package dsale.dhi.com.order;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import dsale.dhi.com.ariesdsale.R;

public class DailySalesReportActivity extends AppCompatActivity {

    private boolean openedItemLayout = false;
    private LinearLayout salesReportLayout ;
    private LinearLayout itemListLayout ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_daily_sales_report);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);


        salesReportLayout = (LinearLayout)findViewById(R.id.salesReportLayout);
        itemListLayout = (LinearLayout)findViewById(R.id.salesItemSummaryLayout);
        openedItemLayout = false;
    }

    public void expandSalesItems(View v){

       // Toast.makeText(getApplicationContext(),"Expand Clicked :"+openedItemLayout,Toast.LENGTH_LONG).show();
        ImageView view = (ImageView) v;
        if(!openedItemLayout){

            salesReportLayout.setVisibility(View.GONE);
            itemListLayout.setVisibility(View.VISIBLE);
            openedItemLayout =true;


            view.setImageResource(R.drawable.ic_expand_more_black_24dp);

        }else{

            salesReportLayout.setVisibility(View.VISIBLE);
            itemListLayout.setVisibility(View.GONE);
            openedItemLayout =false ;


            view.setImageResource(R.drawable.ic_expand_less_black_24dp);
        }

    }
}
