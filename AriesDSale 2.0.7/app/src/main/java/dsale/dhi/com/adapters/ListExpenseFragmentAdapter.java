package dsale.dhi.com.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.FourStrings;

@SuppressLint("ValidFragment")
public class ListExpenseFragmentAdapter extends Fragment {
    private static Activity activity;
    private View mView;
    private Context context ;
    private static DatabaseHandler db;
    ExpenseListRecyclerViewAdapter adapter;

    public ListExpenseFragmentAdapter(Activity activity, Context context) {
        this.activity = activity;
        db = new DatabaseHandler(activity);
        this.context =context ;
    }

    @Override
       public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_expenselist, container, false);
        listExpenses();
        return mView;
       }
    private void listExpenses(){
        HashMap<String, FourStrings> expHashMap = db.getExpenses();
        RecyclerView recyclerView = (RecyclerView) mView.findViewById(R.id.ExpenseRecylerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity().getApplicationContext()));
        adapter = new ExpenseListRecyclerViewAdapter(this.getActivity().getApplicationContext(), expHashMap);
        recyclerView.setAdapter(adapter);
    }
}
