package dsale.dhi.com.invoice;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;


import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.ariesdsale.ShopActivity;
import dsale.dhi.com.customer.EditCustomerActivity;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.order.OrderTakingActivity;

public class InvoicePaymentApproveActivity extends AppCompatActivity implements View.OnClickListener {

    private String scheduledetailid = "";
    private static String sid;
    private String custid;
    private String mob;
    private String OTP;
    private boolean enable_signature;
    private boolean enable_otp;
    PopupWindow popupWindow;
    InvoicePaymentApproveAdapter adapter;
    private ImageView imgNavigationcustdtl3;
    private ImageView imgNavigationcustdtl1;
    private ImageView imgNavigationcustdtl4;

    private int roundoffPrecision ;
    private String currencySymbol ;
    private String currencyCode ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_payment_approve);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        imgNavigationcustdtl3 = (ImageView) findViewById(R.id.imgNavigationcustdtl3);
        imgNavigationcustdtl3.setOnClickListener(this);
        imgNavigationcustdtl1 =(ImageView) findViewById(R.id.imgNavigationcustdtl1);
        imgNavigationcustdtl1.setOnClickListener(this);
        imgNavigationcustdtl4 =(ImageView) findViewById(R.id.imgNavigationcustdtl4);
        imgNavigationcustdtl4.setOnClickListener(this);


        Bundle bundle = getIntent().getExtras();
        sid = (String) bundle.get("scheduledetailid");

        SharedPreferences pref =  getSharedPreferences("Config",InvoiceMakePaymentActivity.MODE_PRIVATE);
        roundoffPrecision = pref.getInt("RoundOffPrecision",0);
        currencyCode=pref.getString("CurrencyCode","");
        currencySymbol =pref.getString("CurrencySymbol","");

        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        JSONArray payments = new JSONArray();
        JSONObject otpdata = new JSONObject();try {
            payments = db.getpaymentstodisplay(sid);
            otpdata = db.get_otp_request_data(sid);

        } catch (Exception e) {
            e.printStackTrace();
        }
        mob = otpdata.optString("mobile", "");
        custid = otpdata.optString("customerid", "");
        OTP = otpdata.optString("otp", "");
        enable_signature = db.isserviceexist("DigitalSignature");
        enable_otp = db.isserviceexist("OTP");
        final JSONArray pay = payments;
//        double toatal_amount = 0.0;
        BigDecimal toatal_amount =new BigDecimal(0) ;

        for (int i = 0; i < pay.length(); i++) {
            try {
                JSONObject dataobj = pay.getJSONObject(i);
                String paymethode = dataobj.getString("payentmethode");
                String amount = dataobj.getString("amount");

                if (!paymethode.equals("Return")
                        && !paymethode.equals("Credit")) {
                    JSONArray inputdata = new JSONArray();
                    inputdata = dataobj.optJSONArray("invoicedata");

                    if (inputdata.length() != 0)
                    {
//                        toatal_amount += Double.parseDouble(amount);
                        toatal_amount = toatal_amount.add(new BigDecimal(amount)) ;
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        Boolean disableaproov = true;
        try {

            for (int i = 0; i < pay.length(); i++) {
                JSONObject dataobj = pay.getJSONObject(i);
                String aproove = dataobj.optString("aprovelag", "0");
                if (aproove.equals("0")) {
                    disableaproov = false;
                    break;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (disableaproov) {
            Button submit = (Button) findViewById(R.id.paymentapprove);
            submit.setText(getResources().getString(R.string.approvedbtn));
            submit.setEnabled(false);
        }
        if (pay.length() > 0) {
            TextView currencySymbolTv = (TextView) findViewById(R.id.currencySymbolTv) ;
            currencySymbolTv.setText(currencySymbol);

            toatal_amount = toatal_amount.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/) ;
            TextView total_amount = (TextView) findViewById(R.id.subtotal);
            total_amount.setText(String.valueOf(toatal_amount));//toatal_amount
            Button approve = (Button) findViewById(R.id.paymentapprove);
            final LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.popuplayout);
            approve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LayoutInflater layoutInflater = (LayoutInflater) InvoicePaymentApproveActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View customView = layoutInflater.inflate(R.layout.popuplayout, null);
                    TextView header =customView.findViewById(R.id.header);
                    TextView msg =(TextView) customView.findViewById(R.id.message);
                    Button yes =(Button) customView.findViewById(R.id.yes);
                    Button cancel = (Button) customView.findViewById(R.id.close);
                    header.setText(getResources().getString(R.string.approve_payments));
                    msg.setText(getResources().getString(R.string.approve_payment_msg));
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("Config",MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putBoolean("gotosecondtab", true);
                            editor.commit();
                            DatabaseHandler db = new DatabaseHandler(getApplicationContext());

                            Calendar c1 = Calendar.getInstance();
                            DateFormat dateTimeFormat2 = new SimpleDateFormat("yyyyMMddHHmmss");
                            String createtime = dateTimeFormat2.format(c1.getTime());
                            String fileId = UUID.randomUUID().toString();
                            //==========without signin person
                            fileId = "0";
                            String singingPerson ="";
                            String verificationcode_entered = "";
                            try {
                                db.aproovepayments(pay, createtime,
                                        singingPerson, fileId,
                                        verificationcode_entered);
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
//                            if (!db.isphonenoavailable(sid)) {
//                                Global.Toast(mAct,"Customer phone number is not available. Please update.",Toast.LENGTH_LONG, Font.Regular);
//                                Intent intent = new Intent(mAct,EditLocationNewActivity.class);
//                                // CustomerEditActivity.class);
//                                intent.putExtra("scheduledetailid", sid);
//                                startActivity(intent);
//                            } else {
                                Intent intent = new Intent(InvoicePaymentApproveActivity.this, CustomerListActivity.class);
                                intent.putExtra("scheduledetailid", sid);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                finish();
                                startActivity(intent);
                                overridePendingTransition(0, 0);
//                            }
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                        }
                    });

                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
                    popupWindow = new PopupWindow(customView, width, height);
                    popupWindow.setFocusable(true);

                    //display the popup window
                    popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);
                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));

                }
            });
        }

        int len =pay.length();
        JSONArray payed =new JSONArray();
        for (int i=0;i<len;i++)
        {
            final JSONObject temp = pay.optJSONObject(i);
            JSONArray inputdata = new JSONArray();
            inputdata = temp.optJSONArray("invoicedata");

            if (inputdata.length() != 0)
            {
                payed.put(temp);
            }
        }
        if(payed.length() ==0)
        {
            Button approve = (Button) findViewById(R.id.paymentapprove);
            approve.setTextColor(getResources().getColor(R.color.textcolor1));
            approve.setEnabled(false);
        }
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new InvoicePaymentApproveAdapter(this,sid,payed);
        recyclerView.setAdapter(adapter);
        Button back=(Button) findViewById(R.id.cancelpayment);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InvoicePaymentApproveActivity.this, InvoiceMakePaymentActivity.class);
                intent.putExtra("scheduledetailid",sid);
                intent.putExtra("back",true);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(InvoicePaymentApproveActivity.this, InvoiceMakePaymentActivity.class);
        i.putExtra("scheduledetailid",sid);
        i.putExtra("back",true);
        finish();
        startActivity(i);
        overridePendingTransition(0,0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imgNavigationcustdtl3:
                Intent i = new Intent(InvoicePaymentApproveActivity.this, EditCustomerActivity.class);

                i.putExtra("scheduledetailid", scheduledetailid);
                finish();
                startActivity(i);
                overridePendingTransition(0, 0);
                break;
            case R.id.imgNavigationcustdtl4:
                i = new Intent(InvoicePaymentApproveActivity.this, OrderTakingActivity.class);

                i.putExtra("status", "");
                i.putExtra("scheduledetailid", scheduledetailid);
                finish();
                overridePendingTransition(0, 0);
                startActivity(i);
                break;
            case R.id.imgNavigationcustdtl1:
                i = new Intent(InvoicePaymentApproveActivity.this, ShopActivity.class);
                i.putExtra("scheduledetailid", scheduledetailid);
                finish();
                startActivity(i);
                overridePendingTransition(0, 0);
                break;

//            case R.id.showAllBtn:
//                showFilterPopup("All", v);
//
//                break;
//
//            case R.id.brandBtn:
//
//                showFilterPopup("Brand", v);
//                break;
//
//            case R.id.categoryBtn:
//                showFilterPopup("Category", v);
//                break;
//
//            case R.id.formBtn:
//                showFilterPopup("Form", v);
//                break;


        }
    }
}
