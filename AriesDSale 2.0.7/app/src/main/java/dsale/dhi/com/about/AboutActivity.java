package dsale.dhi.com.about;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.customerlist.CustomerListActivity;

public class AboutActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_activity);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);



        PackageInfo pinfo;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            int versionNumber = pinfo.versionCode;
            String versionName = pinfo.versionName;
            TextView versioninfo = (TextView) findViewById(R.id.versionlabel);
            versioninfo.setText(" DSale " + versionName);

            TextView helpperson = (TextView) findViewById(R.id.personname);
            helpperson.setText(pref.getString("HelpLinePerson", ""));

            TextView helpmail = (TextView) findViewById(R.id.Email);
            helpmail.setText(pref.getString("HelpLineEmail", ""));

            TextView helmob = (TextView) findViewById(R.id.phone);
            helmob.setText(pref.getString("HelpLinePhone", "")
                    .contentEquals("") ? "" : "Phone: "
                    + pref.getString("HelpLinePhone", ""));

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

   /* public void onBackPressed() {

        finish();
        overridePendingTransition(0, 0);
        return;
    }*/

    @Override
    public void onBackPressed() {

        Intent intent=new Intent(AboutActivity.this, CustomerListActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

}
