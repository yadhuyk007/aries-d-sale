package dsale.dhi.com.order;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.DateError;
import dsale.dhi.com.objects.webconfigurration;

public class ItemListActivity extends AppCompatActivity implements ItemListAdapter.ItemClickListener{

    private static final int REQUEST_CODE_DATE_SETTING =0 ;
    //  HashMap<String, FourStrings> itemListData ;
    ItemListAdapter adapter ;
    // private String Rupeessymbol = "\u20B9";
    RecyclerView itemListRecyclerView ;
    boolean openedCustomerDetails ;
    LinearLayout customerDetailsLayout;
    ItemListAdapter.ItemClickListener itemClickListener ;

    TextView invoiceNoTv ;
    TextView complimentaryInvoiceNoTv ;
    TextView customerNameTv ;
    TextView gstTv ;
    TextView dateTv ;
    TextView gstTvLabel ;
    Button deleteAllBtn;
    Button paymentBtn;


    private String scheduleLineId;
    private String invoiceNo;
    private DatabaseHandler db ;
    private String orderId;
    private String complimentOrderId;
    private boolean addComplimentary = false;
    private boolean approved;
    private ImageButton itemcompliment;
    private HashMap<String, OrderLine> orderLineHashMap ;

    private SharedPreferences pref;
    private BigDecimal totamnt;
    private String currencySymbol ;
    private String currencyCode ;
    private int roundoffPrecision ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_item_list);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        pref = getSharedPreferences("Config", MODE_PRIVATE);
        customerNameTv = (TextView) findViewById(R.id.custname);
        gstTv = (TextView) findViewById(R.id.gstin);
        gstTvLabel  = (TextView) findViewById(R.id.gstinLabel);
        dateTv = (TextView) findViewById(R.id.invoidedate);
        deleteAllBtn = (Button)findViewById(R.id.deleteall);
        paymentBtn = (Button)findViewById(R.id.payment);
        itemcompliment = (ImageButton) findViewById(R.id.complimentbtn);

        invoiceNoTv = (TextView) findViewById(R.id.invoiceno);
        complimentaryInvoiceNoTv = (TextView) findViewById(R.id.compliment_invoiceno);
        customerDetailsLayout =(LinearLayout) findViewById(R.id.customerDetailsLayout);
        itemListRecyclerView = (RecyclerView)findViewById(R.id.itemlistRecyclerView);
        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        itemListRecyclerView.setLayoutManager(layoutManager);

        openedCustomerDetails =true;

        Bundle bundle = getIntent().getExtras();
        scheduleLineId = bundle.getString("scheduledetailid");


//        invoiceNo = bundle.getString("invoiceno");

        db = new DatabaseHandler(getApplicationContext());

        orderId = db.getorderid(scheduleLineId);
        complimentOrderId = db.getComplimentaryOrderId(scheduleLineId, orderId);
//        complimentOrderId = db.getComplimentaryOrderId(scheduleLineId, invoiceNo);
        addComplimentary = db.addComplimentaryItems(orderId);
        invoiceNo = db.getInvoiceNo(orderId);
        if (invoiceNo.length() == 0){
            invoiceNoTv.setText("New");
        }else {
            invoiceNoTv.setText(invoiceNo);
        }
        if (!complimentOrderId.contentEquals("0")) {
            complimentaryInvoiceNoTv.setText(db.getInvoiceNo(complimentOrderId));
        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(
                "dd/MM/yyyy");
        String createdtime = df.format(c.getTime());
        dateTv.setText(createdtime);

        gstTv.setText(db.getgstin(scheduleLineId));
        if(db.getgstin(scheduleLineId).equals("") || db.getgstin(scheduleLineId) == null){
            gstTvLabel.setVisibility(View.GONE);
            gstTv.setVisibility(View.GONE);
        }


        customerNameTv.setText(db.getcustName(scheduleLineId));

        if (!addComplimentary) {
            if (!complimentOrderId.contentEquals("0")) {
                try {

//					db.deleteComplimentaryInvoice(complimentOrderId    );
                    df = new SimpleDateFormat("yyyyMMddHHmmss");
                    createdtime = df.format(c.getTime());
                    db.cancelInvoice(complimentOrderId, createdtime, scheduleLineId);

                } catch (DateError e) {
                    callDateTimeSetting();
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

        }


        if (orderId.contentEquals("0")) {

            approved = false;
            orderLineHashMap = new HashMap<>();

        } else {
            approved = db.isorderapproved(orderId);


          /*  orderLineHashMap = db.getSavedOrderLineData(orderId);
            HashMap< String, OrderLine > temp = db.getSavedOrderLineData(complimentOrderId);

            orderLineHashMap.putAll(temp);

            if (!complimentOrderId.contentEquals("0")) {

                complimentaryInvoiceNoTv .setText(db.getComplimentaryInvoiceNo(complimentOrderId));
            }*/
        }


        if (db.isPaymentApproved(scheduleLineId) || db.isPaymentCancelled(scheduleLineId)) {
            deleteAllBtn.setText(getResources().getString(R.string.cancelbtn));
        }




        if(approved){


            String invdate ="";
            try {
                invdate = db.getInvoiceDate(invoiceNo);
            } catch (ParseException e) {

                e.printStackTrace();
            }

            dateTv.setText(invdate);

            deleteAllBtn.setEnabled(false);
            deleteAllBtn.setBackgroundResource(R.color.dark_gray);

        }


        if (db.isWithoutInventory()) {

            Button payment=(Button)findViewById(R.id.payment);
            payment.setText(getResources().getString(R.string.approve));

            if(approved){
                payment.setEnabled(false);

                itemListRecyclerView.setClickable(false);
            }

        }

        loadListItems();
        itemcompliment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addComplimentaryItems();
//                addComplimentaryItems(invoiceNo);
            }
        });

    }

    private void loadListItems() {

        try {

            webconfigurration c = new webconfigurration(getApplicationContext());

            boolean gstRegistered = true ;
            if(db.getgstin(scheduleLineId).equals("") || db.getgstin(scheduleLineId) == null){
                gstRegistered =false ;
            }
            String msg = db.applyTax(orderId,c.taxType,gstRegistered,scheduleLineId,false);

            if (msg.length() > 0){
                Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                paymentBtn.setEnabled(false);


            }

            orderLineHashMap = new HashMap<>();
            orderLineHashMap = db.getSavedOrderLineData(orderId);


            if (!complimentOrderId.contentEquals("0")) {
                HashMap< String, OrderLine > temp = db.getSavedOrderLineData(complimentOrderId);

                orderLineHashMap.putAll(temp);

                db.applyTax(complimentOrderId,c.taxType,false,scheduleLineId,true);
//                complimentaryInvoiceNoTv .setText(db.getComplimentaryInvoiceNo(complimentOrderId));
            }

            if (orderLineHashMap.size() <= 0){
                Toast.makeText(getApplicationContext(),R.string.No_items_selected,
                        Toast.LENGTH_LONG).show();
                db.updateschedulestatuspending(scheduleLineId, "order");
                finish();
                overridePendingTransition(0, 0);
                Intent i=new Intent(ItemListActivity.this,OrderTakingActivity.class);

                i.putExtra("complimentary",false);
                i.putExtra("parentOrderId","");
                i.putExtra("scheduledetailid",scheduleLineId);

                startActivity(i);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        itemClickListener =this ;
        itemListRecyclerView.setHasFixedSize(true);

        adapter = new ItemListAdapter(ItemListActivity.this,orderLineHashMap,itemClickListener);

        itemListRecyclerView.setAdapter(adapter);

        setSummaryText();
    }

    private void setSummaryText()/*throws  Exception*/ {

        pref = getSharedPreferences("Config", MODE_PRIVATE);
        currencyCode=pref.getString("CurrencyCode","");
        currencySymbol =pref.getString("CurrencySymbol","");
        roundoffPrecision = pref.getInt("RoundOffPrecision",0);

        int subtotalPrecision = 2 ;//subtotal decimal points set as 3 default.
        if(subtotalPrecision < roundoffPrecision){
            subtotalPrecision = roundoffPrecision ;
        }
       /* double taxtot = db.getTotalTaxAmount(orderId);
        double tottaxableamnt = db.getTotalTaxableAmnt(orderId);
        double totcgst = db.getTotalCgst(orderId);
        double totsgst = db.getTotalSgst(orderId);
        double totvat = db.getTotalVat(orderId) ;
        double totDisc = db.getTotalDiscountAmount(orderId);
        totamnt = taxtot+tottaxableamnt;

        DecimalFormat twoDForm = new DecimalFormat("#.##");*/

        BigDecimal taxtot = db.getTotalTaxAmount(orderId,roundoffPrecision);
        BigDecimal tottaxableamnt = db.getTotalTaxableAmnt(orderId,roundoffPrecision);
        BigDecimal totcgst = db.getTotalCgst(orderId,roundoffPrecision);
        BigDecimal totsgst = db.getTotalSgst(orderId,roundoffPrecision);
//        BigDecimal totvat = db.getTotalVat(orderId,roundoffPrecision);
        BigDecimal totDisc = db.getTotalDiscountAmount(orderId,roundoffPrecision);
        BigDecimal totCess = db.getTotalCess(orderId,roundoffPrecision);

        totamnt = taxtot.add(tottaxableamnt);

        if (roundoffPrecision == 0){
            totamnt = totamnt.setScale(subtotalPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
        }

        totamnt = totamnt.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);



        TextView txt=(TextView)findViewById(R.id.totamnt);
        TextView inrTv=(TextView) findViewById(R.id.inrTv1) ;
        TextView taxtotRsTv=(TextView) findViewById(R.id.taxtotRsTv) ;
        TextView sgstRsTv=(TextView) findViewById(R.id.sgstRsTv);
        TextView invoicetotRsTv=(TextView) findViewById(R.id.invoicetotRsTv);
        TextView cgstRsTv=(TextView) findViewById(R.id.cgstRsTv);
        TextView cessRsTv=(TextView) findViewById(R.id.cessRsTv);
        TextView totDiscRsTv=(TextView) findViewById(R.id.totDiscRsTv);

      /*  if(!currencyType.equalsIgnoreCase("inr"))
            Rupeessymbol=currencyType;*/
        inrTv.setText(currencySymbol);
        taxtotRsTv.setText(currencySymbol);
        sgstRsTv.setText(currencySymbol);
        invoicetotRsTv.setText(currencySymbol);
        cgstRsTv.setText(currencySymbol);
        totDiscRsTv.setText(currencySymbol);
        cessRsTv.setText(currencySymbol);

//        totamnt = Math.round(totamnt);
        //  totamnt =Double.parseDouble(twoDForm.format(totamnt));
        txt.setText(totamnt+"");

//        totDisc = Double.valueOf(twoDForm.format(totDisc));
        totDisc = totDisc.setScale(subtotalPrecision,BigDecimal.ROUND_HALF_UP);
        txt=(TextView)findViewById(R.id.totDisc);
        txt.setText(String.valueOf(totDisc));


        txt=(TextView)findViewById(R.id.taxtot);
        taxtot = taxtot.setScale(subtotalPrecision,BigDecimal.ROUND_HALF_UP);
//        taxtot = Double.valueOf(twoDForm .format(taxtot));
        txt.setText(String.valueOf(taxtot));

//        if(totcgst> 0 && totsgst >0 ) {
        if(totcgst.compareTo(new BigDecimal(0))==1 && totsgst.compareTo(new BigDecimal(0))==1 ) {
           /* totcgst = Double.valueOf(twoDForm.format(totcgst));
            totsgst = Double.valueOf(twoDForm.format(totsgst));*/
            totsgst = totsgst.setScale(subtotalPrecision,BigDecimal.ROUND_HALF_UP);
            totcgst = totcgst.setScale(subtotalPrecision,BigDecimal.ROUND_HALF_UP);
            txt = (TextView) findViewById(R.id.cgst);
            txt.setText(String.valueOf(totcgst));
            txt = (TextView) findViewById(R.id.sgst);
            txt.setText(String.valueOf(totsgst));

            totCess= totCess.setScale(subtotalPrecision,BigDecimal.ROUND_HALF_UP);
            txt = (TextView) findViewById(R.id.cess);
            txt.setText(String.valueOf(totCess));

        }else{
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.c3b);
            linearLayout.setVisibility(View.GONE);
            linearLayout = (LinearLayout) findViewById(R.id.c3c);
            linearLayout.setVisibility(View.GONE);
            linearLayout = (LinearLayout) findViewById(R.id.c3f);
            linearLayout.setVisibility(View.GONE);
        }

        txt=(TextView)findViewById(R.id.invoicetot);

        tottaxableamnt = tottaxableamnt.setScale(subtotalPrecision,BigDecimal.ROUND_HALF_UP);
//        tottaxableamnt = Double.valueOf(twoDForm.format(tottaxableamnt));
        txt.setText(String.valueOf(tottaxableamnt));


    }

//    public void hideCustomerDetails(View v){
//
//        LinearLayout listViewLayout = (LinearLayout) findViewById(R.id.itemListLayout) ;
//        ImageView view = (ImageView) v;
//        if(openedCustomerDetails){
//            customerDetailsLayout.setVisibility(View.GONE);
//            openedCustomerDetails =false;
//            view.setImageResource(R.drawable.expand);
//            ((LinearLayout.LayoutParams) listViewLayout.getLayoutParams()).weight = 53;
//        }else{
//            customerDetailsLayout.setVisibility(View.VISIBLE);
//            openedCustomerDetails =true ;
//            view.setImageResource(R.drawable.shrink);
//            ((LinearLayout.LayoutParams) listViewLayout.getLayoutParams()).weight = 47;
//        }
//
//    }

    @Override
    public void onRecyclerViewItemClick(View view,final OrderLine data, int position  ) {
//        Toast.makeText(getApplicationContext(),"Item Qty : "+data.getQty(),Toast.LENGTH_LONG).show();

        approved = db.isorderapproved(orderId);
        if(approved){
            Toast.makeText(getApplicationContext(),R.string.editing_is_not_allowed,Toast.LENGTH_LONG).show();
            return ;
        }
        LayoutInflater layoutInflater = getLayoutInflater();
        final View popup = layoutInflater.inflate(R.layout.popup_order_itemlist_edit, null);
        Button deleteBtn = (Button) popup.findViewById(R.id.deleteBtn);
        Button updateBtn = (Button) popup.findViewById(R.id.updateBtn);

        TextView txt = (TextView) popup .findViewById(R.id.titlePrdName);

        txt.setText(data.getProductName());
        EditText txtt = (EditText) popup .findViewById(R.id.prdqty);

        ((TextView) popup.findViewById(R.id.qtyTv))
                .setText(getResources().getString(R.string.QTY_Package_Type) + data.getPackageTypeCode()    + ")");
        txtt.setText(data.getQty()+"");

        EditText rateEditText = (EditText) popup .findViewById(R.id.prdRate);
        rateEditText.setText(data.getRate()+"");

        //  if (data.getRate() == 0) {
        if (data.isComplimentFlag()) {
            rateEditText.setEnabled(false);
        }

        //instantiate popup window

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(popup, width, height);
        popupWindow.setFocusable(true);

        //display the popup window
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //close the popup window on button click
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{

                    if(db.isPaymentApproved(scheduleLineId) ){
                        AlertDialog.Builder builder = new AlertDialog.Builder( ItemListActivity.this);
                        builder.setTitle(R.string.Already_Payment_Collected);
                        builder.setMessage(R.string.cancel_payment_to_continue);
                        builder.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });

                        builder.show();
                    }else{
                        webconfigurration web = new webconfigurration(getApplicationContext());
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat(
                                "yyyyMMddHHmmss");
                        String createdtime = df.format(c.getTime());
                        db.deleteproduct(data.getOrderId()+"", data.getProductId()+"",
                                createdtime,scheduleLineId,data.isComplimentFlag(),web.taxType);
                        Toast.makeText(getApplicationContext(),R.string.Deleted,Toast.LENGTH_LONG).show();
                        popupWindow.dismiss();
                        finish();
                        startActivity(getIntent());
                        overridePendingTransition(0, 0);
                    }



                }catch(DateError e){
                    callDateTimeSetting();
                }


            }
        });
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(db.isPaymentApproved(scheduleLineId) ){

                    AlertDialog.Builder builder = new AlertDialog.Builder( ItemListActivity.this);
                    builder.setTitle(R.string.Already_Payment_Collected);
                    builder.setMessage(R.string.cancel_payment_to_continue);
                    builder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                    builder.show();

                }else{

                    EditText pricetxt = (EditText)popup.findViewById(R.id.prdRate);
                    String price = pricetxt.getText().toString();

                    if (price.contentEquals("")) {
                        Toast.makeText(getApplicationContext(),R.string.Please_enter_rate,Toast.LENGTH_SHORT).show();
                        return ;
                    }

                    EditText txt = (EditText) popup .findViewById(R.id.prdqty);
                    String quantityString = txt.getText().toString();
                    quantityString = quantityString.contentEquals("") ? "0"
                            : quantityString;

                    double quantity = Double.parseDouble(quantityString);
                    double totalQty = quantity ;
                    double availableqty =   db.getavailableqty( data.getProductId()+"",pref.getString("tmsOrgId", ""),
                            "" );

                    OrderLine line  = new OrderLine();

                    if ( data.complimentFlag ){
                        line  = orderLineHashMap.get( data.getProductId()+"" );
                    }else {
                        line  = orderLineHashMap.get(data.getProductId()+"_C");
                    }

                    if (line != null){
                        totalQty = totalQty + line.getQty();
                    }

                    if ( availableqty < totalQty && !db.isWithoutInventory() ) {

                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.Maximum_available_stock_for)
                                        +" "+data.getProductName()+" "
                                        +getResources().getString(R.string.is)+" "+availableqty ,
                                Toast.LENGTH_LONG).show();

                        return ;

                    }

                    webconfigurration web = new webconfigurration(getApplicationContext());
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    String createdtime = df.format(c.getTime());
                    try{

                        db.updateproduct(data.getOrderId()+"", data.getProductId()+"", quantity+"",price,
                                createdtime,scheduleLineId,data.isComplimentFlag(),web.taxType);

                        Toast.makeText(getApplicationContext(),R.string.Updated,Toast.LENGTH_LONG).show();
                        popupWindow.dismiss();
                        finish();
                        startActivity(getIntent());
                        overridePendingTransition(0, 0);

                    }catch(DateError e){
                        callDateTimeSetting();
                    }



                }
            }
        });

    }

    private void callDateTimeSetting() {

        Toast.makeText(getApplicationContext(),R.string.correct_mob_date_time,
                Toast.LENGTH_LONG).show();

        Intent intent = new Intent(android.provider.Settings.ACTION_DATE_SETTINGS);
        startActivityForResult(intent, REQUEST_CODE_DATE_SETTING);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_DATE_SETTING) {
            finish();
            startActivity(getIntent());
            overridePendingTransition(0, 0);
        }


    }

    private void turnGPSOn() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!statusOfGPS) {


          /*  AlertDialog.Builder builder = Global.Alert(VanSalesInvoiceGenerator.this,
                    "Switch On GPS", "Currently gps of the device is off.");
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            startActivityForResult(
                                    new Intent(
                                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                    0);
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
            Global.setDialogFont(VanSalesInvoiceGenerator.this, dialog);*/
        }

    }


    public void paymentBtnClick(View v){

        if (orderLineHashMap.isEmpty()) {
            Toast.makeText(getApplicationContext(),R.string.No_items_selected,
                    Toast.LENGTH_LONG).show();

        } else {
            //gps check
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean statusOfGPS = manager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (!statusOfGPS) {
                turnGPSOn();
                // uncomment before moving to production
                //  return;
            }
            if (db.isWithoutInventory()) {

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        ItemListActivity.this);
                builder.setTitle(R.string.want_to_approve);
                builder.setMessage(R.string.editing_is_not_allowed);
                builder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                if (db.isInvoiceNoGenTableEmpty()){
                                    Toast.makeText(getApplicationContext(),
                                            getResources().getString(R.string.invoice_no_not_configured),
                                            Toast.LENGTH_LONG).show();
                                    return ;
                                }


                                String invoiceNo = db.invoicenogenerator(orderId);

                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat df = new SimpleDateFormat(
                                        "yyyyMMddHHmmss");
                                String createdtime = df.format(c.getTime());

                                DateFormat dateTimeFormat_payment = new SimpleDateFormat(
                                        "dd/MM/yyyy");


                                final String invDate = dateTimeFormat_payment
                                        .format(c.getTime());
                                try {
                                    db.approveorder(scheduleLineId,createdtime);

                                    db.insertInvoice(db.getcustomerid(scheduleLineId), invoiceNo, String.valueOf(totamnt),
                                            String.valueOf(totamnt), invDate, "0");
                                    Toast.makeText(getApplicationContext(),R.string.approved,
                                            Toast.LENGTH_SHORT).show();

                                    finish();
                                    overridePendingTransition(0, 0);
                                    Intent intent=new Intent(ItemListActivity.this,CustomerListActivity.class);

                                    startActivity(intent);


                                } catch (DateError e) {
                                    callDateTimeSetting();
                                }catch (JSONException e) {

                                    e.printStackTrace();
                                }
                            }
                        });

                builder.setNeutralButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                builder.show();

            }else{
                Intent intent=new Intent(ItemListActivity.this,OrderPaymentActivity.class);
                intent.putExtra("scheduledetailid", scheduleLineId);
                intent.putExtra("invoiceamnt",String.valueOf(totamnt));
//                intent.putExtra("invoiceno",invoiceNo);
                finish();
                startActivity(intent);
            }
        }

    }


    public void cancelBtnClick(View view) {

        String alertMsg="";
        String alertTitle ="";
        final boolean paymentApproved = db.isPaymentApproved(scheduleLineId);
        final boolean paymentCancelled = db.isPaymentCancelled(scheduleLineId) ;


        approved = db.isorderapproved(orderId);
        if (approved) {
            Toast.makeText(getApplicationContext(),R.string.Cannot_Delete_After_Approved,
                    Toast.LENGTH_SHORT).show();
          /*  Global.Toast(ItemListActivity.this, "Cannot Delete After Approved !",
                    Toast.LENGTH_SHORT, Font.Regular);*/
            return ;
        }


        if (paymentApproved || paymentCancelled  ) {

            alertMsg =getResources().getString(R.string.cancel_this_Order_and_Payment);
            alertTitle =getResources().getString(R.string.cancelbtn);

        }else{

            alertMsg =getResources().getString(R.string.delete_all_items);
            alertTitle =getResources().getString(R.string.DELETE_ALL);

        }

        AlertDialog.Builder builder = new AlertDialog.Builder(
                ItemListActivity.this);
        builder.setMessage(alertMsg);
        builder.setTitle(alertTitle);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                    String createdtime = df.format(c.getTime());
                    db.cancelInvoice(orderId, createdtime,scheduleLineId);

                    db.deleteallproducts(orderId, createdtime);

                    if ( !complimentOrderId.contentEquals("0")  ) {

                        db.cancelInvoice(complimentOrderId, createdtime,scheduleLineId);
                        db.deleteallproducts(complimentOrderId , createdtime);

                    }


                    if (paymentApproved) {

                        db.cancelPayment(scheduleLineId, orderId,createdtime);


                    }

                    Toast.makeText(getApplicationContext(),R.string.cancelled,
                            Toast.LENGTH_SHORT).show();
                  /*  Global.Toast(VanSalesInvoiceGenerator.this, "Cancelled !!",
                            Toast.LENGTH_SHORT, Font.Regular);*/


                    Intent intent = new Intent(ItemListActivity.this, CustomerListActivity.class);
                    finish();
                    overridePendingTransition(0, 0);
                    //intent.putExtra("scheduledetailid", schedulelineid);
                    startActivity(intent);


                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (DateError e) {
                    callDateTimeSetting();
                }
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();



    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_add_complimentary, menu);
//        //		  menu.findItem(R.id.addComplimentItem).setTitle(Html.fromHtml("<font color='#FFFFFFFF'>FREE ITEMS</font>"));
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.addComplimentItem :
                addComplimentaryItems();
//                addComplimentaryItems(invoiceNo);
                return true;

            default :
                return super.onOptionsItemSelected(item);
        }

    }

    private void addComplimentaryItems(/*String parentInvoice*/) {

        if (db.isPaymentApproved(scheduleLineId)) {

            Toast.makeText(ItemListActivity.this, R.string.Payment_approved,
                    Toast.LENGTH_SHORT).show();
            return;

        }
        if (addComplimentary) {

            Intent intent=new Intent(ItemListActivity.this,OrderTakingActivity.class);
            intent.putExtra("scheduledetailid", scheduleLineId);
            intent.putExtra("complimentary",true);
            intent.putExtra("parentOrderId",orderId);
//            intent.putExtra("parentInvoiceNo",invoiceNo);
            finish();
            startActivity(intent);

        }else{
            Toast.makeText(ItemListActivity.this, R.string.Cannot_add_Complimentary_Products,
                    Toast.LENGTH_SHORT).show();

        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*Intent i=new Intent(ItemListActivity.this,OrderTakingActivity.class);

        i.putExtra("complimentary",false);
        i.putExtra("parentInvoiceNo","");
        i.putExtra("scheduledetailid",scheduleLineId);
        finish();
        overridePendingTransition(0,0);
        startActivity(i);*/
        finish();
        overridePendingTransition(0,0);
        Intent i=new Intent(ItemListActivity.this, CustomerListActivity.class);
        startActivity(i);



    }
}
