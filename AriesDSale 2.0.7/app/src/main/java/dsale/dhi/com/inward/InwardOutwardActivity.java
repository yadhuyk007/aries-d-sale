package dsale.dhi.com.inward;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.ariesdsale.R;

public class InwardOutwardActivity extends AppCompatActivity {

SharedPreferences pref;
//    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inward_outward_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        //=======================Table Layout
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.inward));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.outward));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        final PagerAdapter adapter = new TabAdapterInwardOrOutward
                (getApplicationContext(),getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        pref = getSharedPreferences("Config", MODE_PRIVATE);
        String TabState=pref.getString("TabState", "Inward");
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        if(TabState.equals("Inward") )
        {
            viewPager.setCurrentItem(0);
        }
        else
        {
            viewPager.setCurrentItem(1);
        }
//========================

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Intent i=new Intent(InwardOutwardActivity.this,CustomerListActivity.class);
        finish();
        startActivity(i);
        overridePendingTransition(0,0);
    }
}
