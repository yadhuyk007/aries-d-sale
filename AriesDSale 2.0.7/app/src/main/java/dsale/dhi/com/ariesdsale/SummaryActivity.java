package dsale.dhi.com.ariesdsale;

import android.animation.ObjectAnimator;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import dsale.dhi.com.database.DatabaseHandler;

public class SummaryActivity extends AppCompatActivity {
    private DatabaseHandler db;
    private String arcollected;
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        db = new DatabaseHandler(getApplicationContext());
        pref = getApplicationContext()
                .getSharedPreferences("Config", MODE_PRIVATE);
        try {
            setvalues();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setvalues() throws ParseException {
        JSONObject summarydata = new JSONObject();
        try {
            summarydata = db.getworksummary();
        } catch (Exception e) {
            e.printStackTrace();
            summarydata = new JSONObject();
        }
        String totalstores = String.valueOf(summarydata
                .optInt("tottalnumberofstores", 0));
        String visited = String.valueOf(summarydata
                .optInt("visitedstores", 0));
        String productive = String.valueOf(summarydata
                .optInt("productivestores", 0));
        String artarget = "";
        String arcollected = "";
        ((TextView) findViewById(R.id.totstoresbval)).setText(totalstores);
        ((TextView) findViewById(R.id.visitedstoresval)).setText(visited);
        ((TextView) findViewById(R.id.productiveval)).setText(productive);
        ((TextView) findViewById(R.id.artargetval)).setText(artarget);
        ((TextView) findViewById(R.id.arcollectval)).setText(arcollected);
        String scheduledate = pref.getString("Schbegintime", "");
        Date datsched = new Date();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date curr = new Date();
        long diff = 0;
        long hours_tottal = 0;
        long mins_tottal = 0;
        int progress1;
        ProgressBar mProgress1=(ProgressBar) findViewById(R.id.performance_progress_bar);
        if (!scheduledate.contentEquals("")) {
            datsched = dateTimeFormat.parse(scheduledate);
            diff = curr.getTime() - datsched.getTime();
            hours_tottal = diff / (60 * 60 * 1000);
            mins_tottal = diff / (60 * 1000) % 60;
        }
        String sched_time = String.valueOf(hours_tottal) + "h "
                + String.valueOf(mins_tottal) + "min";
        ((TextView) findViewById(R.id.worktime)).setText(sched_time);

        // updating working hours in progrss bar and textview
        final String scheduledate_final = pref
                .getString("Schbegintime", "");
        double worktimemax = (((hours_tottal * 60) + mins_tottal) / (8.0 * 60.0)) * 100.0;
        progress1 = (int) worktimemax;
        animateProgress(mProgress1, progress1);
    }
    private void animateProgress(ProgressBar mProgress, int i) {
        ObjectAnimator animation = ObjectAnimator.ofInt(mProgress, "progress",
                0, i);
        animation.setDuration(800);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }
}
