package dsale.dhi.com.ariesdsale;

public class CustomerVisibility {

    public  String id ="";
    public  String createtime ="";
    public String scheduleLineId ="";
    public String photoUUID ="";
    public  String latitude ="";
    public String longtitude ="";
    public String accuracy ="";

    public CustomerVisibility(){}
    public CustomerVisibility(String createtime, String scheduleLineId,
                              String photoUUID, String latitude,
                              String longtitude, String accuracy) {
        this.createtime = createtime;
        this.scheduleLineId = scheduleLineId;
        this.photoUUID = photoUUID;
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.accuracy = accuracy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getScheduleLineId() {
        return scheduleLineId;
    }

    public void setScheduleLineId(String scheduleLineId) {
        this.scheduleLineId = scheduleLineId;
    }

    public String getPhotoUUID() {
        return photoUUID;
    }

    public void setPhotoUUID(String photoUUID) {
        this.photoUUID = photoUUID;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }
}
