package dsale.dhi.com.ariesdsale;


import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.DateError;


public class CompleteScheduleActivity extends AppCompatActivity implements View.OnClickListener {
    private boolean schSmryFlag = true;
    private boolean denomSmryFlag = false;
    private LinearLayout scheduleSmryHdr;
    private LinearLayout denominatnSmryHdr;
    private LinearLayout scheduleSmryDtl;
    private LinearLayout denominatnSmryDtl;
    private ImageView schIcon;
    private ImageView denmIcon;
    TabHost host;
    DatabaseHandler db ;
    private static final int REQUEST_CODE_DATE_SETTING = 0;
    private JSONArray denominationdata = new JSONArray();
    private String denomdata = "";
    private String schedulestatus = "";
    private String[] denomcategorise;
    private String[] denomcategoryids;
    private Spinner spinner;
    private TextView Cashcollected;
    private TextView tottalstores;
    private TextView visitedstores;
    private TextView productivestores;
    private TextView marketordersum;
    private TextView artarget;
    private TextView arcollected;
    private TextView arpending;
    private TextView returnamount;
    private TextView creditamount;
    private TextView cheaquecollected;
    private TextView cheaquecollectedpdc;
    private TextView Grandtottal;

    private TextView Totalreturnamount;
    private String currencySymbol  ;
    private SharedPreferences pref ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.completeschedule);
    }/*{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.completeschedule);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
      //  currencyCode=pref.getString("CurrencyCode","");
        currencySymbol =pref.getString("CurrencySymbol","");
        scheduleSmryDtl = (LinearLayout)findViewById(R.id.scheduleSmryDtl);
        denominatnSmryDtl = (LinearLayout)findViewById(R.id.denominatnSmryDtl);
        schIcon=(ImageView)findViewById(R.id.schIcon);
        denmIcon=(ImageView)findViewById(R.id.denmIcon);
        schIcon.setOnClickListener(this);
        denmIcon.setOnClickListener(this);
        //for tabs in denominatnSmryDtl
        host = (TabHost) findViewById(R.id.tabHost);
        host.setup();
        // tab1
        TabHost.TabSpec spec = host.newTabSpec("Cash");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Cash");
        host.addTab(spec);
        // Tab 2 OLD Cheque
        spec = host.newTabSpec("CDC");
        spec.setContent(R.id.tab2);
        spec.setIndicator("CDC");
        host.addTab(spec);
        // Tab 3 pdc
        spec = host.newTabSpec("PDC");
        spec.setContent(R.id.tab4);
        spec.setIndicator("PDC");
        host.addTab(spec);

        // Tab 4
        spec = host.newTabSpec("Return");
        spec.setContent(R.id.tab3);
        spec.setIndicator("Return");
        host.addTab(spec);

        // Tab 5
        spec = host.newTabSpec("Credit");
        spec.setContent(R.id.tab5);
        spec.setIndicator("Credit");
        host.addTab(spec);

        db = new DatabaseHandler(getApplicationContext());
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss",
                Locale.US);
        String createttime = dateTimeFormat.format(c.getTime());
        if (!db.validateCreateTime(createttime) ) {
            callDateTimeSetting();
        }
        JSONObject schedulesummary = new JSONObject();
        JSONArray chequedetails = new JSONArray();
        JSONArray Returndetails = new JSONArray();
        JSONArray creditdetails = new JSONArray();
        JSONArray chequedetailspdc = new JSONArray();
        try {
            denominationdata = db.getdenominationdata();
            schedulesummary = db.Getschedulesummary();
            denomdata = schedulesummary.getString("denomdata");
            schedulestatus = schedulesummary.getString("schedulestatus");
            chequedetails = schedulesummary.getJSONArray("chequescollected");
            chequedetailspdc = schedulesummary
                    .getJSONArray("chequescollectedpdc");
            Returndetails = schedulesummary.getJSONArray("returndata");
            creditdetails = schedulesummary.getJSONArray("creditdata");
        } catch (JSONException e) { // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            intialisecashcounttable();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            intialisespinner();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Cashcollected = (TextView) findViewById(R.id.cashcollected);
        String ordamt = "0";
        double taxableAmnt = db.getTotalTaxableAmount();
        double taxAmnt = db.getTotalTaxAmount();
        double ordAmnt = taxableAmnt+taxAmnt ;
        ordAmnt = Math.round(ordAmnt);
        ordamt = ordAmnt+"";
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        try {
            Cashcollected.setText(schedulesummary.getString("total_cashamount"));
            tottalstores = (TextView) findViewById(R.id.storecount);
            tottalstores.setText(schedulesummary.getString("tottalnumberofstores"));
            visitedstores = (TextView) findViewById(R.id.visitedcount);
            visitedstores.setText(schedulesummary.getString("visitedstores"));
            productivestores = (TextView) findViewById(R.id.prdctvcount);
            productivestores.setText(schedulesummary.getString("productivestores"));
            marketordersum = (TextView) findViewById(R.id.mktordercount);
            marketordersum.setText(currencySymbol + " " + schedulesummary.getString("Tottalorderamount"));
            marketordersum.setVisibility(View.INVISIBLE);
            artarget = (TextView) findViewById(R.id.totInvCount);
            artarget.setText(currencySymbol + " "+twoDForm.format(Math.round(Double.parseDouble(ordamt))));
            arcollected = (TextView) findViewById(R.id.colectdAmnt);
            arcollected.setText(currencySymbol + " " + schedulesummary.getString("Tottalamountcollected"));
            returnamount = (TextView) findViewById(R.id.RtnAmnt);
            returnamount.setText(currencySymbol + " " + schedulesummary.getString("total_return"));
            double ar_balance = 0.0;
            try {
                double trgt = Double.parseDouble(schedulesummary.getString("artarget"));
                double clcted = Double.parseDouble(schedulesummary.getString("Tottalamountcollected"));
                ar_balance = Math.round(Double.parseDouble(ordamt)) - clcted;
            } catch (Exception e) {
                e.printStackTrace();
            }
            arpending = (TextView) findViewById(R.id.balanceamnt);
            arpending.setText(currencySymbol + " " + String.valueOf(ar_balance));
            cheaquecollected = (TextView) findViewById(R.id.chequeamount);
            cheaquecollected.setText(" " + currencySymbol + " " + schedulesummary.getString("total_chequeamount"));
            setcheckcollectedvalues(chequedetails);
            cheaquecollectedpdc = (TextView) findViewById(R.id.chequeamountPDC);
            cheaquecollectedpdc.setText(" " + currencySymbol + " "+ schedulesummary.getString("total_chequeamount_pdc"));
            setcheckcollectedvaluespdc(chequedetailspdc);
            returnamount = (TextView) findViewById(R.id.returnamount);
            returnamount.setText(" " + currencySymbol + " " + schedulesummary.getString("total_return"));
            setreturndata(Returndetails);

            creditamount = (TextView) findViewById(R.id.creditamount);
            creditamount.setText(" " + currencySymbol + " "
                    + schedulesummary.getString("total_credit"));
            setcreditdata(creditdetails);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Grandtottal = (TextView) findViewById(R.id.finaltotal);
        if (denomdata.length() > 0 && schedulestatus.equals("Completed")) {
            setsavedvalues_and_hidnewbutton();
        }
        ImageView adddenom = (ImageView) findViewById(R.id.adddenomination);
        adddenom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                int selectedposition = spinner.getSelectedItemPosition();
                int denomid = Integer.parseInt(denomcategoryids[selectedposition]);
                int denomamount = 1;
                try {
                    denomamount = Integer.parseInt(denomcategorise[selectedposition]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                EditText count = (EditText) findViewById(R.id.notecount);
                if (count.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Count needed",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                int num = Integer.parseInt(count.getText().toString());
                int subtotal = denomamount * num;
                TextView counttoupdate = (TextView) findViewById(
                        R.id.denominationmainlayer).findViewWithTag(
                        denomid + "_count");
                TextView subtotaltoupdate = (TextView) findViewById(
                        R.id.denominationmainlayer).findViewWithTag(
                        denomid + "_subttal");
                counttoupdate.setText(String.valueOf(num));
                subtotaltoupdate.setText(String.valueOf(subtotal));
                count.setText("");
                updatefinaltotal();
            }
        });
        Button submit = (Button) findViewById(R.id.submitschedule);
        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                getuserconfirmationandsave();
            }
        });
    }*/

    @Override
    public void onClick( View v )
    {
        if ( v == schIcon )
        {
            if(schSmryFlag){
                scheduleSmryDtl.setVisibility(View.GONE);
                schSmryFlag=false;
                schIcon.setImageResource(R.drawable.ic_expand_more_black_24dp);

            }
            else
            {
                scheduleSmryDtl.setVisibility(View.VISIBLE);
                schSmryFlag=true;
                schIcon.setImageResource(R.drawable.ic_expand_less_black_24dp);
                hideDenominatnSmryDtl();
                //hideAddDenominatnDtl();
            }
        }
        else if(v == denmIcon){
            if(denomSmryFlag){
                denominatnSmryDtl.setVisibility(View.GONE);
                denomSmryFlag=false;
                denmIcon.setImageResource(R.drawable.ic_expand_more_black_24dp);

            }
            else
            {
                denominatnSmryDtl.setVisibility(View.VISIBLE);
                denomSmryFlag=true;
                denmIcon.setImageResource(R.drawable.ic_expand_less_black_24dp);
                //hideAddDenominatnDtl();
                hideScheduleSmryDtl();
            }
        }
        /*else if(v == addDenmIcon){
            if(addDenomFlag){
                addDenominatnDtl.setVisibility(View.GONE);
                addDenomFlag=false;
                addDenmIcon.setImageResource(R.drawable.ic_expand_more_black_24dp);

            }
            else
            {
                addDenominatnDtl.setVisibility(View.VISIBLE);
                addDenomFlag=true;
                addDenmIcon.setImageResource(R.drawable.ic_expand_less_black_24dp);
                hideScheduleSmryDtl();
                hideDenominatnSmryDtl();
            }
        }*/
    }
    public  void hideDenominatnSmryDtl()
    {
        denominatnSmryDtl.setVisibility(View.GONE);
        denomSmryFlag=false;
        denmIcon.setImageResource(R.drawable.ic_expand_more_black_24dp);
    }
    public void hideScheduleSmryDtl()
    {
        scheduleSmryDtl.setVisibility(View.GONE);
        schSmryFlag=false;
        schIcon.setImageResource(R.drawable.ic_expand_more_black_24dp);
    }

    private void callDateTimeSetting() {
        Toast.makeText(CompleteScheduleActivity.this, "Please correct Mobile Date and Time!!",
                Toast.LENGTH_SHORT).show();
        Intent dateSetttingIntent = new Intent(android.provider.Settings.ACTION_DATE_SETTINGS);
        startActivityForResult(dateSetttingIntent, REQUEST_CODE_DATE_SETTING);
    }

    public void intialisecashcounttable() throws JSONException {

        /*
         * try {
         *
         * JSONObject obj1 = new JSONObject(); obj1.put("amount", "2000");
         * obj1.put("id", "1"); denominationdata.put(obj1);
         *
         * JSONObject obj2 = new JSONObject(); obj2.put("amount", "1000");
         * obj2.put("id", "2"); denominationdata.put(obj2);
         *
         * JSONObject obj3 = new JSONObject(); obj3.put("amount", "500");
         * obj3.put("id", "3"); denominationdata.put(obj3);
         *
         * JSONObject obj4 = new JSONObject(); obj4.put("amount", "100");
         * obj4.put("id", "4"); denominationdata.put(obj4);
         *
         * JSONObject obj5 = new JSONObject(); obj5.put("amount", "50");
         * obj5.put("id", "5"); denominationdata.put(obj5);
         *
         * JSONObject obj6 = new JSONObject(); obj6.put("amount", "20");
         * obj6.put("id", "6"); denominationdata.put(obj6);
         *
         * JSONObject obj7 = new JSONObject(); obj7.put("amount", "10");
         * obj7.put("id", "7"); denominationdata.put(obj7);
         *
         * JSONObject obj8 = new JSONObject(); obj8.put("amount", "5");
         * obj8.put("id", "8"); denominationdata.put(obj8);
         *
         * JSONObject obj9 = new JSONObject(); obj9.put("amount", "2");
         * obj9.put("id", "9"); denominationdata.put(obj9);
         *
         * JSONObject obj10 = new JSONObject(); obj10.put("amount", "1");
         * obj10.put("id", "10"); denominationdata.put(obj10);
         *
         * } catch (JSONException e) { e.printStackTrace(); }
         */

        final LinearLayout lm = (LinearLayout) findViewById(R.id.denominationmainlayer);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);


        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lparams.weight=0.5f;

        LinearLayout horiz = new LinearLayout(this);
        horiz.setOrientation(LinearLayout.HORIZONTAL);
        horiz.setPadding(0, 20, 0, 20);
        //horiz.setBackground(ContextCompat.getDrawable(CompleteScheduleActivity.this,R.drawable.rounded_corner));
        horiz.setWeightSum(1);
        LinearLayout v1 = new LinearLayout(this);
        v1.setOrientation(LinearLayout.VERTICAL);
        //v1.setPadding(40, 10, 0, 0);
        v1.setPadding(0, 10, 0, 0);
        //v1.setBackground(ContextCompat.getDrawable(CompleteScheduleActivity.this,R.drawable.table_row_border));
        v1.setLayoutParams(lparams);


        LinearLayout v2 = new LinearLayout(this);
        v2.setOrientation(LinearLayout.VERTICAL);
        //v2.setPadding(40, 10, 0, 0);
        v2.setPadding(0, 10, 0, 0);
        //v2.setBackground(ContextCompat.getDrawable(CompleteScheduleActivity.this,R.drawable.table_row_border));
        v2.setLayoutParams(lparams);

        for (int i = 0; i < denominationdata.length(); i++) {
            // Create LinearLayout

            LinearLayout l3 = new LinearLayout(this);
            l3.setOrientation(LinearLayout.HORIZONTAL);
            //l3.setPadding(0, 0, 0, 20);
            l3.setPadding(20, 20, 20, 20);
            l3.setBackground(ContextCompat.getDrawable(CompleteScheduleActivity.this,R.drawable.table_row_border));

            JSONObject dataobj = denominationdata.getJSONObject(i);

            String amout = dataobj.getString("amount");
            String id = dataobj.getString("id");

            TextView amount = new TextView(this);
            amount.setText(amout + " x ");
            amount.setLayoutParams(params);
            amount.setTextColor(Color.parseColor("#5fa2d7"));
            l3.addView(amount);

            TextView count = new TextView(this);
            count.setText("0");
            count.setLayoutParams(params);
            count.setTag(id + "_count");
            l3.addView(count);

            TextView equl = new TextView(this);
            equl.setText(" = ");
            equl.setLayoutParams(params);
            l3.addView(equl);

            TextView sub_total = new TextView(this);
            sub_total.setText("0");
            sub_total.setLayoutParams(params);
            sub_total.setTag(id + "_subttal");
            l3.addView(sub_total);
            if (i % 2 == 0)
                v1.addView(l3);
            else
                v2.addView(l3);

            // Create TextView

        }

        horiz.addView(v1);
        horiz.addView(v2);
        lm.addView(horiz);

    }
    public void intialisespinner() throws JSONException {
        denomcategorise = new String[denominationdata.length()];
        denomcategoryids = new String[denominationdata.length()];
        for (int i = 0; i < denominationdata.length(); i++) {
            String amout = denominationdata.getJSONObject(i)
                    .getString("amount");
            String id = denominationdata.getJSONObject(i).getString("id");
            denomcategorise[i] = amout;
            denomcategoryids[i] = id;
        }
        spinner = (Spinner) findViewById(R.id.denomdropdown);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, denomcategorise);
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    public void setcheckcollectedvaluespdc(JSONArray chequedata)
            throws JSONException {

        String rupeessymbol = "\u20B9";
        final LinearLayout lm = (LinearLayout) findViewById(R.id.chequedetailsPDC);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams params_innerlinear = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params_innerlinear.setMargins(0, 5, 0, 5);
        for (int i = 0; i < chequedata.length(); i++) {
            // Create LinearLayout

            LinearLayout l3 = new LinearLayout(this);
            l3.setOrientation(LinearLayout.VERTICAL);
            l3.setLayoutParams(params_innerlinear);
            l3.setBackgroundColor(Color.parseColor("#f2f2f2"));
            l3.setPadding(10, 10, 0, 20);

            JSONObject dataobj = chequedata.getJSONObject(i);

            String amout = dataobj.getString("amount");
            String cheque_date = dataobj.getString("chequedate");
            String chequenumber = dataobj.getString("chequenumber");
            String bank = dataobj.getString("bankname");
            String chequemode = dataobj.getString("chequemode");

            TextView amount = new TextView(this);
            amount.setText(rupeessymbol + " " + amout);
            amount.setLayoutParams(params);
            // amount.setTextColor(Color.parseColor("#42f4c2"));
            amount.setPadding(0, 5, 0, 5);
            l3.addView(amount);

            TextView chequedt = new TextView(this);
            chequedt.setText("Date : " + cheque_date);
            chequedt.setLayoutParams(params);
            chequedt.setPadding(0, 0, 0, 5);
            l3.addView(chequedt);

            TextView cheq_type = new TextView(this);
            cheq_type.setText("Type : " + chequemode);
            cheq_type.setLayoutParams(params);
            cheq_type.setPadding(0, 0, 0, 5);
            l3.addView(cheq_type);

            TextView bnk = new TextView(this);
            bnk.setText("Bank : " + bank);
            bnk.setLayoutParams(params);
            bnk.setPadding(0, 0, 0, 5);
            l3.addView(bnk);

            TextView chequno = new TextView(this);
            chequno.setText("No : " + chequenumber);
            chequno.setLayoutParams(params);
            chequno.setPadding(0, 0, 0, 5);
            l3.addView(chequno);
            lm.addView(l3);
        }

    }

    public void setcheckcollectedvalues(JSONArray chequedata)
            throws JSONException {

        String rupeessymbol = "\u20B9";
        final LinearLayout lm = (LinearLayout) findViewById(R.id.chequedetails);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams params_innerlinear = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params_innerlinear.setMargins(0, 5, 0, 5);
        for (int i = 0; i < chequedata.length(); i++) {
            // Create LinearLayout

            LinearLayout l3 = new LinearLayout(this);
            l3.setOrientation(LinearLayout.VERTICAL);
            l3.setLayoutParams(params_innerlinear);
            l3.setBackgroundColor(Color.parseColor("#f2f2f2"));
            l3.setPadding(10, 10, 0, 20);

            JSONObject dataobj = chequedata.getJSONObject(i);

            String amout = dataobj.getString("amount");
            String cheque_date = dataobj.getString("chequedate");
            String chequenumber = dataobj.getString("chequenumber");
            String bank = dataobj.getString("bankname");
            String chequemode = dataobj.getString("chequemode");

            TextView amount = new TextView(this);
            amount.setText(rupeessymbol + " " + amout);
            amount.setLayoutParams(params);
            // amount.setTextColor(Color.parseColor("#42f4c2"));
            amount.setPadding(0, 5, 0, 5);
            l3.addView(amount);

            TextView chequedt = new TextView(this);
            chequedt.setText("Date : " + cheque_date);
            chequedt.setLayoutParams(params);
            chequedt.setPadding(0, 0, 0, 5);
            l3.addView(chequedt);

            TextView cheq_type = new TextView(this);
            cheq_type.setText("Type : " + chequemode);
            cheq_type.setLayoutParams(params);
            cheq_type.setPadding(0, 0, 0, 5);
            l3.addView(cheq_type);

            TextView bnk = new TextView(this);
            bnk.setText("Bank : " + bank);
            bnk.setLayoutParams(params);
            bnk.setPadding(0, 0, 0, 5);
            l3.addView(bnk);

            TextView chequno = new TextView(this);
            chequno.setText("No : " + chequenumber);
            chequno.setLayoutParams(params);
            chequno.setPadding(0, 0, 0, 5);
            l3.addView(chequno);
            lm.addView(l3);
        }

    }

    // set return values
    public void setreturndata(JSONArray returndata) throws JSONException {

        String rupeessymbol = "\u20B9";
        final LinearLayout lm = (LinearLayout) findViewById(R.id.returndetails);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams params_innerlinear = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params_innerlinear.setMargins(0, 5, 0, 5);
        for (int i = 0; i < returndata.length(); i++) {
            // Create LinearLayout

            LinearLayout l3 = new LinearLayout(this);
            l3.setOrientation(LinearLayout.VERTICAL);
            l3.setLayoutParams(params_innerlinear);
            l3.setBackgroundColor(Color.parseColor("#f2f2f2"));
            l3.setPadding(10, 10, 0, 20);

            JSONObject dataobj = returndata.getJSONObject(i);
            String amout = dataobj.getString("amount");
            String cust_name = dataobj.getString("custname");
            String cust_adress = dataobj.getString("custadress");
            String cust_cat = dataobj.getString("custcat");
            String cust_code = dataobj.getString("custcode");

            TextView amount = new TextView(this);
            amount.setText(rupeessymbol + " " + amout);
            amount.setLayoutParams(params);
            // amount.setTextColor(Color.parseColor("#42f4c2"));
            amount.setPadding(0, 5, 0, 5);
            l3.addView(amount);

            TextView cstname = new TextView(this);
            cstname.setText(cust_name);
            cstname.setLayoutParams(params);
            cstname.setPadding(0, 0, 0, 5);
            l3.addView(cstname);

            TextView cstadd = new TextView(this);
            cstadd.setText(cust_adress);
            cstadd.setLayoutParams(params);
            cstadd.setPadding(0, 0, 0, 5);
            l3.addView(cstadd);

            TextView cstct = new TextView(this);
            cstct.setText(cust_cat);
            cstct.setLayoutParams(params);
            cstct.setPadding(0, 0, 0, 5);
            l3.addView(cstct);

            TextView cstcd = new TextView(this);
            cstcd.setText("Code : " + cust_code);
            cstcd.setLayoutParams(params);
            cstcd.setPadding(0, 0, 0, 5);
            l3.addView(cstcd);

            lm.addView(l3);
        }

    }

    // set return values
    public void setcreditdata(JSONArray creditdata) throws JSONException {

        String rupeessymbol = "\u20B9";
        final LinearLayout lm = (LinearLayout) findViewById(R.id.creditdetails);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams params_innerlinear = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params_innerlinear.setMargins(0, 5, 0, 5);
        for (int i = 0; i < creditdata.length(); i++) {
            // Create LinearLayout

            LinearLayout l3 = new LinearLayout(this);
            l3.setOrientation(LinearLayout.VERTICAL);
            l3.setLayoutParams(params_innerlinear);
            l3.setBackgroundColor(Color.parseColor("#f2f2f2"));
            l3.setPadding(10, 10, 0, 20);

            JSONObject dataobj = creditdata.getJSONObject(i);
            String amout = dataobj.getString("amount");
            String cust_name = dataobj.getString("custname");
            String cust_adress = dataobj.getString("custadress");
            String cust_cat = dataobj.getString("custcat");
            String cust_code = dataobj.getString("custcode");

            TextView amount = new TextView(this);
            amount.setText(rupeessymbol + " " + amout);
            amount.setLayoutParams(params);
            // amount.setTextColor(Color.parseColor("#42f4c2"));
            amount.setPadding(0, 5, 0, 5);
            l3.addView(amount);

            TextView cstname = new TextView(this);
            cstname.setText(cust_name);
            cstname.setLayoutParams(params);
            cstname.setPadding(0, 0, 0, 5);
            l3.addView(cstname);

            TextView cstadd = new TextView(this);
            cstadd.setText(cust_adress);
            cstadd.setLayoutParams(params);
            cstadd.setPadding(0, 0, 0, 5);
            l3.addView(cstadd);

            TextView cstct = new TextView(this);
            cstct.setText(cust_cat);
            cstct.setLayoutParams(params);
            cstct.setPadding(0, 0, 0, 5);
            l3.addView(cstct);

            TextView cstcd = new TextView(this);
            cstcd.setText("Code : " + cust_code);
            cstcd.setLayoutParams(params);
            cstcd.setPadding(0, 0, 0, 5);
            l3.addView(cstcd);

            lm.addView(l3);
        }

    }

    public void setsavedvalues_and_hidnewbutton() {
        String[] denom_saved = denomdata.split(",");
        int grantotal = 0;
        for (int i = 0; i < denom_saved.length; i++) {
            String id = denom_saved[i].split("#")[0];
            String count = denom_saved[i].split("#")[1];
            String total = denom_saved[i].split("#")[2];

            TextView counttoupdate = (TextView) findViewById(
                    R.id.denominationmainlayer).findViewWithTag(id + "_count");
            TextView subtotaltoupdate = (TextView) findViewById(
                    R.id.denominationmainlayer).findViewWithTag(id + "_subttal");

            counttoupdate.setText(count);
            subtotaltoupdate.setText(total);
            grantotal += Integer.parseInt(total);

        }
        TextView adddenomtex = (TextView) findViewById(R.id.adddenomtext);
        adddenomtex.setVisibility(View.GONE);

        Button submit = (Button) findViewById(R.id.submitschedule);
        submit.setText("COMPLETED");
        submit.setEnabled(false);

        LinearLayout addinglayer = (LinearLayout) findViewById(R.id.addinglayer);
        addinglayer.setVisibility(View.GONE);

        Grandtottal.setText(String.valueOf(grantotal));

    }

    public void updatefinaltotal() {
        int total = 0;
        for (int i = 0; i < denomcategoryids.length; i++) {
            TextView subcurrent = (TextView) findViewById(
                    R.id.denominationmainlayer).findViewWithTag(
                    denomcategoryids[i] + "_subttal");
            total += Integer.parseInt(subcurrent.getText().toString());
        }
        TextView fintotal = (TextView) findViewById(R.id.finaltotal);
        fintotal.setText(String.valueOf(total));
        double diff = 0.0;
        double coll = 0.0;
        try {
            coll = Double.parseDouble(Cashcollected.getText().toString());
            TextView difference = (TextView) findViewById(R.id.difference);
            diff = coll - (double) total;
            difference.setText(String.valueOf(diff));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getuserconfirmationandsave() {
        TextView fintotal = (TextView) findViewById(R.id.finaltotal);
        String finaltotal = fintotal.getText().toString().replace(".0", "");
        String cashcollected = Cashcollected.getText().toString()
                .replace(".0", "");
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss",
                Locale.US);
        final String createttime = dateTimeFormat.format(c.getTime());
        if (!finaltotal.equals(cashcollected)) {
            Toast.makeText(getApplicationContext(), "Amount not matched",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(
                CompleteScheduleActivity.this);
        builder.setTitle("Complete Todays Work");
        builder.setMessage("Verify Data Before submission");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                try {
                    saveandcompleteschedule(createttime);
                    Toast.makeText(getApplicationContext(), "Submitted",
                            Toast.LENGTH_SHORT).show();
                } catch (DateError e) {
                    callDateTimeSetting();
                }
            }
        });

        builder.setNeutralButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        builder.show();
    }

    public void saveandcompleteschedule(String createttime) throws DateError {
        String denomdata = "";
        for (int i = 0; i < denomcategoryids.length; i++) {
            TextView count = (TextView) findViewById(R.id.denominationmainlayer)
                    .findViewWithTag(denomcategoryids[i] + "_count");
            TextView subtotal = (TextView) findViewById(
                    R.id.denominationmainlayer).findViewWithTag(
                    denomcategoryids[i] + "_subttal");
            denomdata += denomcategoryids[i] + "#" + count.getText().toString()
                    + "#" + subtotal.getText().toString() + ",";
        }
        if (denomdata.length() > 0)
            denomdata = denomdata.substring(0, denomdata.length() - 1);
        Calendar c = Calendar.getInstance();
		DateFormat dateTimeFormat_readable = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss", Locale.US);
        String readabletime = dateTimeFormat_readable.format(c.getTime());
        String scheduleids = db.getactualscheduleid();
        String begintime = db.getscheduleheaderbegintime(scheduleids);
        if(begintime == null)
            begintime="";
        //GPS

        /*PlayLocation tracker  = new PlayLocation();
        Double latt = tracker.getlattitude();
        Double longi = tracker.getlongitude();
        String latitude = String.valueOf(latt);
        String longitude = String.valueOf(longi);*/
        String actualscheduleid = db.getactualscheduleid();

        String personid = pref.getString("personid", "0");
        String uniqueid = UUID.randomUUID().toString();
        String tottalcash = Grandtottal.getText().toString();

       /* db.saveschedulecompletiondata(actualscheduleid, denomdata, personid,
                createttime, readabletime, latitude, longitude, uniqueid,
                tottalcash, scheduleid, createttime, begintime);*/

        db.updatescheduleheaderstatus(createttime);
        db.approveAllOrder(createttime);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Schendtime", createttime);
        editor.commit();
        Intent intentent = getIntent();
        finish();
        overridePendingTransition(0, 0);
        startActivity(intentent);
    }












}
