package dsale.dhi.com.invoice;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.database.DatabaseHandler;

import static android.content.Context.MODE_PRIVATE;

public class InvoiceMakePaymentAdapter extends RecyclerView.Adapter<InvoiceMakePaymentAdapter.ViewHolder>{

    private List<String> mData;
    private List<String> mData1;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Button lin;
    private SharedPreferences pref;
    Spinner batchspinner;
    Spinner packagespinner;
    PopupWindow popupWindow;
    LinearLayout cdc;
    LinearLayout editpoup;
    LinearLayout cdcpopup;
    Button cancel_payment;
    Button edit_payment;
    HashMap<Integer, String> selItems = new HashMap<Integer, String>();
    EditText dat;
    static String sid;
    Context mcontext;
    DatabaseHandler db;
    String[] amount;
    String[] balance;
    String[] invoicenumber;
    String[] customerid;
    String[] invoicedate;
    String[] status;
    String[] invoiceid;
  /*  Double subtotal = 0.0;
    Double paidamount = 0.0;
    Double amountdue = 0.0;
    Double chtotal =0.0;
    Double chbalance=0.0;
    Double chpend=0.0;*/

//    private BigDecimal subtotal = new BigDecimal(0);
//    private BigDecimal paidamount = new BigDecimal(0);
//    private BigDecimal amountdue =new BigDecimal(0);
    private BigDecimal chtotal = new BigDecimal(0);
    private BigDecimal chbalance = new BigDecimal(0);
    private BigDecimal chpend = new BigDecimal(0);

    private int roundoffPrecision ;
    private String currencySymbol ;

    // data is passed into the constructor
    public InvoiceMakePaymentAdapter(Context context,String sid) {
        this.mInflater = LayoutInflater.from(context);
        Log log;
        this.mcontext = context;
        this.sid=sid;
        this.invoicenumber =invoicenumber;

        pref = mcontext.getSharedPreferences("Config", MODE_PRIVATE);

         roundoffPrecision = pref.getInt("RoundOffPrecision",0);
         currencySymbol =pref.getString("CurrencySymbol","");


    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_invoice_make_payment, parent, false); // make_payment_row payment_approve_row
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        String animal = mData.get(position);
//        String status = mData1.get(position);
        //lin = itemView.findViewById(R.id.cashbtn);
        db = new DatabaseHandler(mcontext);

        JSONArray inputdata = new JSONArray();
        try {
            inputdata = db.getallinvoicedata(sid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        amount = new String[inputdata.length()];
        balance = new String[inputdata.length()];
        invoicenumber = new String[inputdata.length()];
        customerid = new String[inputdata.length()];
        invoicedate = new String[inputdata.length()];
        status = new String[inputdata.length()];
        invoiceid = new String[inputdata.length()];
        for (int i = 0; i < inputdata.length(); i++) {
            amount[i] = inputdata.optJSONObject(i).optString("amount");
            balance[i] = inputdata.optJSONObject(i).optString("balance");
            invoicenumber[i] = inputdata.optJSONObject(i).optString(
                    "invoicenumber");
            customerid[i] = inputdata.optJSONObject(i).optString("customerid");
            invoicedate[i] = inputdata.optJSONObject(i)
                    .optString("invoicedate");
            status[i] = inputdata.optJSONObject(i).optString("status");
            invoiceid[i] = inputdata.optJSONObject(i).optString("invoiceid");
            try {
               /* Double amt = Double.parseDouble(amount[i]);
                Double bal = Double.parseDouble(balance[i]);
                Double paid = amt - bal;
                subtotal = subtotal + amt;
                paidamount = paidamount + paid;
                amountdue = amountdue + bal;*/

              /*  BigDecimal amt =  new BigDecimal(amount[i]);
                BigDecimal bal = new BigDecimal(balance[i]);
                BigDecimal paid = amt.subtract(bal) ;
                subtotal = subtotal.add(amt) ;
                paidamount = paidamount.add(paid);
                amountdue = amountdue.add(bal);

                amountdue = amountdue.setScale(roundoffPrecision,BigDecimal. ROUND_HALF_EVEN ) ;*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        for (int i = 0; i < invoicenumber.length; i++) {
            try {
                Calendar c1 = Calendar.getInstance();
                DateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy");
                String paymentdate = dateTimeFormat1.format(c1.getTime());
                Date paymentdateformat = dateTimeFormat1.parse(paymentdate);
                DateFormat dateTimeFormat2 = new SimpleDateFormat("MM/dd/yyyy");
                Date invoicedateformat = dateTimeFormat2.parse(invoicedate[position]);
                long diff = paymentdateformat.getTime()
                        - invoicedateformat.getTime();
                long diffDays = diff / (24 * 60 * 60 * 1000);
                BigDecimal amt =  new BigDecimal(amount[position] );
                amt = amt.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
                holder.invoiceamt.setText(currencySymbol +" "+amt.toString());
                holder.myTextView.setText(" #" + invoicenumber[position]);
                holder.invdate.setText(invoicedate[position]);// + " (A/R DAYS : " + diffDays + ")");

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            /*Double paid = 0.0;
            Double amt = 0.0;
            Double bal = 0.0;*/
        BigDecimal paid =  new BigDecimal(0);
        BigDecimal amt =  new BigDecimal(0);
        BigDecimal bal = new BigDecimal(0);

            try {
              /*  amt = Double.parseDouble(amount[position]);
                bal = Double.parseDouble(balance[position]);
                paid = amt - bal;*/

                  amt =  new BigDecimal(amount[position]);
                  bal = new BigDecimal(balance[position]);
                  paid = amt.subtract(bal) ;

                bal = bal.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);

            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.balance.setText(bal.toString());
            String statusStr = mcontext.getResources().getString(R.string.pending);
             if (paid.compareTo(new BigDecimal(0)) == 1) { /* if (paid > 0) {*/
                statusStr = mcontext.getResources().getString(R.string.partial);
                holder.status.setCompoundDrawablesWithIntrinsicBounds( R.drawable.single_dot_partial, 0, 0, 0);
//                Drawable img = mcontext.getResources().getDrawable( R.drawable.single_dot_partial );
//                img.setBounds( 0, 0, 60, 60 );
//                holder.status.setCompoundDrawables( img, null, null, null );
            }
            holder.status.setText(statusStr);
            final int id = position;
            if (!selItems.containsKey(id))selItems.put(id, "");
            holder.amount.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    selItems.put(id, s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                  /*  chtotal = Double.parseDouble(amount[position]);
                    chbalance = Double.parseDouble(balance[position]);*/


                   /* Double pendbal=0.0;
                    pendbal=chtotal-chbalance;
                    Double act=0.0;
                    String paidamtstr=holder.amount.getText().toString();
                    if(paidamtstr.isEmpty()){
                        chpend=0.0;
                        act=chbalance-chpend;
                    }
                    else{
                        chpend=Double.parseDouble(paidamtstr);
                        act=chbalance-chpend;
                    }

                     DecimalFormat twoDForm = new DecimalFormat("#.##");
                    act = Double.valueOf(twoDForm
                            .format(act));
                    String pendingamtstr=Double.toString(act);
                    */

                    chtotal = new BigDecimal(amount[position]);
                    chbalance = new BigDecimal(balance[position]);
                    BigDecimal pendbal= new BigDecimal(0);
                    pendbal=chtotal.subtract(chbalance);
                    BigDecimal act= new BigDecimal(0);
                    String paidamtstr=holder.amount.getText().toString();
                    if(paidamtstr.isEmpty()){
                        chpend=  new BigDecimal(0);
                    } else{
                        chpend=new BigDecimal(paidamtstr);
                    }

                    act=chbalance.subtract(chpend);

                    act = act.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);

                    holder.balance.setText(act.toString());

                }
            });
//        }
    }

    public  HashMap<Integer,String> retitem()
    {
        return selItems;
    }
    @Override
    public int getItemCount() {

        db = new DatabaseHandler(mcontext);

        JSONArray inputdata = new JSONArray();
        try {
            inputdata = db.getallinvoicedata(sid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return inputdata.length();
    }




    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        TextView invoiceamt;
        TextView invdate;
        TextView balance;
        EditText amount;
        Button status;


        ViewHolder(final View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.invoiceno);
            invoiceamt = itemView.findViewById(R.id.invoiceamt);
            invdate = itemView.findViewById(R.id.date);
            amount = itemView.findViewById(R.id.amount);
            status = itemView.findViewById(R.id.status);
            balance = itemView.findViewById(R.id.balance);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
