package dsale.dhi.com.customer;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.ariesdsale.ShopActivity;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.gps.GPSAccuracy;
import dsale.dhi.com.gps.PlayTracker;
import dsale.dhi.com.invoice.InvoiceMakePaymentActivity;
import dsale.dhi.com.objects.Global;
import dsale.dhi.com.objects.Scheduledetail;
import dsale.dhi.com.objects.Stacklogger;
import dsale.dhi.com.objects.webconfigurration;
import dsale.dhi.com.order.OrderTakingActivity;


public class EditCustomerActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_CODE_GPS_ACCURACY = 1 ;
    private static DatabaseHandler db ;
    String scheduledetailid;
    String[] townname;
    String[] townids;
    private static HashMap<String, String> townmap;
    private static final String GSTTYPE_UNREGISTERED = "Unregistered";
    private static final String GSTTYPE_REGISTERED = "Registered";
    private static final String GSTTYPE_COMPOSITION = "Composition";
    List<String> days = new ArrayList<String>();
    List<String> gsttypes = new ArrayList<String>();
    private String ini_CustAddress;
    private String ini_CustPhone;
    private String ini_gst;
    private String ini_contactperson;
    private String ini_town;
    private String ini_closingday;
    private String ini_gstType;
    private String ini_fssi;
    ImageView img;
    private static final int CAMERA_REQUEST = 1888;
    private static Uri mImageUri;
    private String imageType;
    private ProgressBar progress;
    private Bitmap customerphoto;
    private String latitude = "0";
    private String longitude = "0";
    private ImageView imgNavigationcustdtl4;
    private ImageView imgNavigationcustdtl2;
    private ImageView imgNavigationcustdtl1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_customer);

        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);


        imgNavigationcustdtl2=(ImageView)findViewById(R.id.imgNavigationcustdtl2);
        imgNavigationcustdtl2.setOnClickListener(this);
        imgNavigationcustdtl1=(ImageView)findViewById(R.id.imgNavigationcustdtl1);
        imgNavigationcustdtl1.setOnClickListener(this);
        imgNavigationcustdtl4=(ImageView)findViewById(R.id.imgNavigationcustdtl4);
        imgNavigationcustdtl4.setOnClickListener(this);

        /**
         * Check whether GPS
         */
        turnOnGPS();
        db = new DatabaseHandler(getApplicationContext());
        progress = (ProgressBar) findViewById(R.id.progress);
        img= (ImageView) findViewById(R.id.addcust);
        Bundle bundle = getIntent().getExtras();
        scheduledetailid = (String) bundle.get("scheduledetailid");
        try {
            intialisetowns();
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line, townname){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(9);
                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            EditCustomerActivity.this.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        AutoCompleteTextView TownAuto = (AutoCompleteTextView) findViewById(R.id.TownAuto);
        TownAuto.setAdapter(adapter);
        //gst type dropdown
        gsttypes.add(getResources().getString(R.string.GSTTYPE_UNREGISTERED));
        gsttypes.add(getResources().getString(R.string.GSTTYPE_REGISTERED));
        gsttypes.add(getResources().getString(R.string.GSTTYPE_COMPOSITION));
        Spinner gsttypeSpinner = (Spinner) findViewById(R.id.spinnergsttype);
        ArrayAdapter<String> gsttypedataAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, gsttypes) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(10);
                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            EditCustomerActivity.this.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        gsttypedataAdapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        gsttypeSpinner.setAdapter(gsttypedataAdapter);

       // gsttypeSpinner.setSelection(0);//set default selection as unregistered.
        //store closed days
        days.add(getResources().getString(R.string.select));
        days.add(getResources().getString(R.string.Sunday));
        days.add(getResources().getString(R.string.Monday));
        days.add(getResources().getString(R.string.Tuesday));
        days.add(getResources().getString(R.string.Wednesday));
        days.add(getResources().getString(R.string.Thursday));
        days.add(getResources().getString(R.string.Friday));
        days.add(getResources().getString(R.string.Saturday));
        Spinner closedday = (Spinner) findViewById(R.id.spinnerclosing_day);
        ArrayAdapter<String> closeddaydataAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, days) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(12);
                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            EditCustomerActivity.this.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        closeddaydataAdapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        closedday.setAdapter(closeddaydataAdapter);


        setcurrentdata();
        Button updatecustmr=(Button) findViewById(R.id.updatecustmr);
        updatecustmr.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                validateAndCaptureGPS();
//                updatelocation();

            }
        });

        //take photo
        ImageButton photo_camera=(ImageButton) findViewById(R.id.photo_camera);
        img= (ImageView) findViewById(R.id.addcust);
        photo_camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(EditCustomerActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(EditCustomerActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},CAMERA_REQUEST);
                }
                else
                {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File photo;
                    try {
                        photo = createTemporaryFile("picture", ".jpg");
                        photo.delete();

                    } catch (Exception e) {
                        Toast.makeText(EditCustomerActivity.this, R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mImageUri = Uri.fromFile(photo);
                    // mImageUri= FileProvider.getUriForFile(NewCustomerActivity.this, "dsale.dhi.com.ariesdsale.fileprovider", photo);


                    //intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                    //intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    startActivityForResult(intent, CAMERA_REQUEST);

                }
            }
        });
    }
    private void    setcurrentdata() {
        try {
            JSONObject cust = db.getallcustdata(scheduledetailid);
            String code = cust.optString("CustCode", "");
            String CustName = cust.optString("CustName", "");
            //String ini_CustCode = cust.optString("AlternateCustcode", "");
            ini_CustAddress = cust.optString("CustAddress", "");
            ini_CustPhone = cust.optString("CustPhone", "");
            ini_gst = cust.optString("CustGst", "");

            ini_contactperson = cust.optString("contactperson", "");

            String cat = cust.optString("CustCat", "");
            String photouuid = cust.optString("PhotoUUID", "");
            ini_town = cust.optString("town", "");
            ini_closingday = cust.optString("closingday", "");

            ini_gstType = cust.optString("gstType", "");
            ini_fssi = cust.optString("fssi", "");

            // String selprodcats = cust.optString("productcats", "");

            EditText text = (EditText) findViewById(R.id.firstname);
            text.setEnabled(false);
            text.setText(CustName);
            text.setTextColor(Color.parseColor("#a7a7a7"));

            text = (EditText) findViewById(R.id.address);
            text.requestFocus();
            text.setText(ini_CustAddress);

            text = (EditText) findViewById(R.id.catEdit);
            text.setText(cat);
            text.setEnabled(false);
            text.setTextColor(Color.parseColor("#a7a7a7"));

            text = (EditText) findViewById(R.id.contactnumber);
            text.setText(ini_CustPhone);
            // text.setEnabled(false);
           /* ((Button) mView.findViewById(R.id.verify)).setVisibility(View.GONE);*/

            text = (EditText) findViewById(R.id.gstin);
            text.setText(ini_gst);


            text = (EditText) findViewById(R.id.contactperson);
            text.setText(ini_contactperson);


            Spinner spinn = (Spinner) findViewById(R.id.spinnerclosing_day);
            spinn.setSelection(getpos(ini_closingday));

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditCustomerActivity.this,
                    android.R.layout.simple_dropdown_item_1line, townname);
            AutoCompleteTextView TownAuto = (AutoCompleteTextView) findViewById(R.id.TownAuto);
            TownAuto.setAdapter(adapter);

            AutoCompleteTextView textTown = (AutoCompleteTextView) findViewById(R.id.TownAuto);
            textTown.setText(townmap.get(ini_town));

            text = (EditText) findViewById(R.id.fssi);
            text.setText(ini_fssi);

            Spinner spin = (Spinner) findViewById(R.id.spinnergsttype);
            spin.setSelection(getposGsttype(ini_gstType));

            // if (!selprodcats.contentEquals("")) {
            // String[] cats = selprodcats.split(",");
            // for (int i = 0; i < categorycode.length; i++) {
            // String temp = categorycode[i];
            // for (String key : cats) {
            // if (temp.contentEquals(key))
            // selectedarray[i] = true;
            // }
            // }
            // }



            //for image
            if (!photouuid.contentEquals("")) {
                File rootsd = Environment.getExternalStorageDirectory();
                File direct = new File(rootsd.getAbsolutePath() + "/Aries");
                String filename = "photo_" + photouuid + ".png";
                File file = new File(direct, filename);
                Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
                if (bmp != null) {
                    img.setImageBitmap(bmp);
                    img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            //GPS part

                            imageType = "photo";
                            if (ContextCompat.checkSelfPermission(EditCustomerActivity.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions(EditCustomerActivity.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},CAMERA_REQUEST);
                            }
                            else
                            {
                                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                                File photo;
                                try {
                                    photo = createTemporaryFile("picture", ".jpg");
                                    photo.delete();

                                } catch (Exception e) {
                                    Toast.makeText(EditCustomerActivity.this, R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                mImageUri = Uri.fromFile(photo);
                                // mImageUri= FileProvider.getUriForFile(NewCustomerActivity.this, "dsale.dhi.com.ariesdsale.fileprovider", photo);


                                //intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                                //intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                startActivityForResult(intent, CAMERA_REQUEST);

                            }


                        }
                    });

                }else{
                    img.setImageResource(R.drawable.ic_dhi_photodownload);
                    LinearLayout imglayout=(LinearLayout)findViewById(R.id.imglayout);
                    imglayout.setBackgroundResource(R.drawable.dhi_square);

                    img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                searchForImage();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }

            }else{
                img.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        //GPS


                        imageType = "photo";



                        if (ContextCompat.checkSelfPermission(EditCustomerActivity.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(EditCustomerActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},CAMERA_REQUEST);
                        }
                        else
                        {
                            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                            File photo;
                            try {
                                photo = createTemporaryFile("picture", ".jpg");
                                photo.delete();

                            } catch (Exception e) {
                                Toast.makeText(EditCustomerActivity.this, R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                                return;
                            }
                            mImageUri = Uri.fromFile(photo);
                            // mImageUri= FileProvider.getUriForFile(NewCustomerActivity.this, "dsale.dhi.com.ariesdsale.fileprovider", photo);


                            //intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                            //intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            startActivityForResult(intent, CAMERA_REQUEST);

                        }




                    }
                });

            }



        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private int getpos(String day) {

        if(day.equals(getResources().getString(R.string.Sunday)))
            return 1;
        else if(day.equals(getResources().getString(R.string.Monday)))
            return 2;
        else if(day.equals(getResources().getString(R.string.Tuesday)))
            return  3;
        else if(day.equals(getResources().getString(R.string.Wednesday)))
            return  4;
        else if(day.equals(getResources().getString(R.string.Thursday)))
            return  5;
        else if(day.equals(getResources().getString(R.string.Friday)))
            return  6;
        else if(day.equals(getResources().getString(R.string.Saturday)))
            return  7;
        else
            return 0;
       /* switch (day) {
            case sunday :
                return 1;
            case "Monday":
                return 2;
            case "Tuesday":
                return 3;
            case "Wednesday":
                return 4;
            case "Thursday":
                return 5;
            case "Friday":
                return 6;
            case "Saturday":
                return 7;
            default:
                return 0;
        }*/
    }
    private void intialisetowns() throws JSONException {
        townmap = new HashMap<>();
        DatabaseHandler db = new DatabaseHandler(EditCustomerActivity.this);
        SharedPreferences pref = EditCustomerActivity.this.getSharedPreferences("Config",
                Context.MODE_PRIVATE);
        JSONArray towns = db.gettown();
        townname = new String[towns.length()];
        townids = new String[towns.length()];

        for (int i = 0; i < towns.length(); i++) {
            JSONObject obj = towns.getJSONObject(i);
            townname[i] = obj.getString("townname");
            townids[i] = obj.getString("townid");
            townmap.put(obj.getString("townid"), obj.getString("townname"));
        }
    }
    private int getposGsttype(String type) {
        if(type.equals(getResources().getString(R.string.GSTTYPE_UNREGISTERED)))
            return 0;
        else if(type.equals(getResources().getString(R.string.GSTTYPE_REGISTERED)))
            return 1;
        else if(type.equals(getResources().getString(R.string.GSTTYPE_COMPOSITION)))
            return 2;
        else
            return 0;
        /*switch (type) {
            case GSTTYPE_UNREGISTERED:
                return 0;
            case GSTTYPE_REGISTERED:
                return 1;
            case GSTTYPE_COMPOSITION:
                return 2;

            default:
                return 0;
        }*/
    }
    private void updatelocation(double lat,double longi,float accuracy,String provider) {
        String address = ((EditText) findViewById(R.id.address)).getText().toString();
        String contactnumber = ((EditText) findViewById(R.id.contactnumber)).getText().toString();
        String gstin = ((EditText) findViewById(R.id.gstin)).getText().toString();
        String town = ((AutoCompleteTextView) findViewById(R.id.TownAuto)).getText().toString();
        String contactperson = ((EditText) findViewById(R.id.contactperson)).getText().toString();
        String closingday = ((Spinner) findViewById(R.id.spinnerclosing_day)).getSelectedItem().toString();
        String gstType = ((Spinner) findViewById(R.id.spinnergsttype)).getSelectedItem().toString();
        String fssi = ((EditText) findViewById(R.id.fssi)).getText().toString();
        closingday = closingday.contentEquals(getResources().getString(R.string.SELECT)) ? "" : closingday;
        String isvalid = valid(address, gstin, town, contactnumber,contactperson,gstType);
        if (!isvalid.contentEquals("TRUE")) {
            Toast.makeText(EditCustomerActivity.this, isvalid, Toast.LENGTH_SHORT).show();
            return;
        }
        if (!town.equals(""))
            town = db.gettownId(town);

        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String crtime = dateTimeFormat.format(c.getTime());
        if (!(address.contentEquals(ini_CustAddress))
                || !(contactnumber.contentEquals(ini_CustPhone))
                || !(gstin.contentEquals(ini_gst))
                || !(town.contentEquals(ini_town))
                || !(contactperson.contentEquals(ini_contactperson))
                || !(closingday.contentEquals(ini_closingday))
                || !gstType.contentEquals(ini_gstType)
                || !fssi.contentEquals(ini_fssi)) {
            Scheduledetail loc = new Scheduledetail();
            loc.locationadress = address;
            loc.mobilenumber = contactnumber;
            loc.tinnumber = gstin;
            loc.town = town;
            loc.contactperson = contactperson;
            loc.Closingday = closingday;
            loc.gsttype = gstType ;
            loc.fssi = fssi ;

            loc.latitude = lat+"";
            loc.longitude = longi+"";
            loc.locaccuracy = accuracy+"" ;
            loc.locprovider = provider ;


            db.editCustomer(scheduledetailid, loc, crtime);
        }

        if (customerphoto != null) {
            String uuid = UUID.randomUUID().toString();
            String filename = uuid + ".png";
            db.editCustomerPhoto(scheduledetailid, uuid, crtime, latitude,
                    longitude);
            createDirectoryAndsavetempfile(customerphoto, "photo_" + filename);
        }


        /*if (photos != null && photos.size() > 0) {
            for (String key : photos.keySet()) {
                try {
                    Bitmap bitmap = photos.get(key);
                    String filename = key + ".png";
                    createDirectoryAndsavetempfile(bitmap, "document_"
                            + filename);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
*/
        Toast.makeText(EditCustomerActivity.this, R.string.changes_saved, Toast.LENGTH_SHORT).show();
        Intent i = new Intent(EditCustomerActivity.this, CustomerListActivity.class);
        // i.putExtra("scheduledetailid",scheduledetailid);
        startActivity(i);
        finish();
        overridePendingTransition(0,0);

    }
    private String valid(String address, String gstin, String town,String contctno,String contctper, String gstType) {
        String temp = "TRUE";
        if (address.contentEquals(""))
            return getResources().getString(R.string.addr_required);
        if (contctno.contentEquals(""))
            return getResources().getString(R.string.number_required);
        if (contctper.contentEquals(""))
            return getResources().getString(R.string.contact_required);
        if (!gstType.equals(getResources().getString(R.string.GSTTYPE_UNREGISTERED)) && gstin.length() == 0 ) {
            return getResources().getString(R.string.select_gstin);
        }

        if (gstType.equals(getResources().getString(R.string.GSTTYPE_UNREGISTERED)) && gstin.length() > 1 ) {
            return getResources().getString(R.string.chk_gsttype);
        }

        if (gstin.length() != 0 && gstin.length() != 15)
            return getResources().getString(R.string.gstin_validation);
        return temp;
    }
    private File createTemporaryFile(String part, String ext) throws Exception {

        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    private void searchForImage() throws Exception {
        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = new JSONObject(inputParamsStr);

        JSONObject updatestatus = null;
        try {
            webconfigurration.status = "ImageDownload";
            updatestatus = getstatusobject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputParamObj.put("updatestatus", updatestatus);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", Context.MODE_PRIVATE);
        JSONObject data = new JSONObject();
        data.put("operation", "DownloadImages")
                .put("slineid", scheduledetailid);
        inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        inputParamObj.getJSONObject("data")
                .put("personid", pref.getString("personid", ""))
                .put("PersonOrg", pref.getString("tmsOrgId", ""));
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        inputParamObj.put("auth", auth);
        new ImageDownloadOperation().execute(inputParamObj);
    }

    private JSONObject getstatusobject() throws Exception {
        JSONObject temp = new JSONObject();
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);

        String person = "";
        String lat = "";
        String lng = "";
        String operation = "";
        String version = "";
        String createdon = "";
        String uuid = "";
        String appname = "";
        String versioncode = "";
        person = pref.getString("personid", "0");

        operation = webconfigurration.status;
        appname = webconfigurration.appname;
        lat = webconfigurration.lat;
        lng = webconfigurration.lng;

        PackageInfo pInfo = getPackageManager().getPackageInfo(
                getPackageName(), 0);
        version = pInfo.versionName;
        versioncode = String.valueOf(pInfo.versionCode);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        createdon = df.format(c.getTime());

        uuid = UUID.randomUUID().toString();

        temp.put("person", person).put("lat", lat).put("lng", lng)
                .put("operation", operation).put("version", version)
                .put("versioncode", versioncode).put("appname", appname)
                .put("createdon", createdon).put("uuid", uuid);
        return temp;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgNavigationcustdtl4:
                Intent i =new Intent(EditCustomerActivity.this, OrderTakingActivity.class);
                i.putExtra("complimentary",false);
                i.putExtra("parentInvoiceNo","");
                i.putExtra("scheduledetailid",scheduledetailid);
                finish();
                startActivity(i);
                overridePendingTransition(0,0);
                break;
            case R.id.imgNavigationcustdtl2:
                i =new Intent(EditCustomerActivity.this, InvoiceMakePaymentActivity.class);
                i.putExtra("complimentary",false);
                i.putExtra("parentInvoiceNo","");
                i.putExtra("scheduledetailid",scheduledetailid);
                finish();
                overridePendingTransition(0,0);
                startActivity(i);
                break;
            case R.id.imgNavigationcustdtl1:
                i =new Intent(EditCustomerActivity.this, ShopActivity.class);
                i.putExtra("complimentary",false);
                i.putExtra("parentInvoiceNo","");
                i.putExtra("scheduledetailid",scheduledetailid);
                finish();
                startActivity(i);
                overridePendingTransition(0,0);
                break;

        }
    }

    private class ImageDownloadOperation extends
            AsyncTask<JSONObject, Void, String> {
        protected void onPreExecute() {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    progress.setVisibility(View.VISIBLE);
                    img.setVisibility(View.GONE);
                }
            });
        };

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.url;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setConnectTimeout(100000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(200000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responsefrom;
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
            Global.onSync = false;
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Log.e("reply", result);
                        JSONObject reply = new JSONObject(result);
                        if (reply.getString("Status").contentEquals("Success")) {
                            String photoString = reply.getString("photo");

                            byte[] decodedString = Base64.decode(photoString,
                                    Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(
                                    decodedString, 0, decodedString.length);
                            img.setImageBitmap(decodedByte);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progress.setVisibility(View.GONE);
                                img.setVisibility(View.VISIBLE);
                            }
                        });

                    }
                }
            });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {

            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == CAMERA_REQUEST
                    && resultCode == Activity.RESULT_OK) {
                Bundle extras =data.getExtras();
                Bitmap mImageBitmap=(Bitmap)extras.get("data");
                ImageView proof=(ImageView)findViewById(R.id.addcust);
                customerphoto=getResizedBitmap(mImageBitmap,500,500);
                proof.setImageBitmap(customerphoto);

            }else if (requestCode == REQUEST_CODE_GPS_ACCURACY){

                if (resultCode == Activity.RESULT_OK) {
                    Double latitude = data.getDoubleExtra("latit", 0);
                    Double longitude = data.getDoubleExtra("longit", 0);
                    Float accuracy = data.getFloatExtra("minacc", 0);
                    String provider = data.getStringExtra("prov");
                    String operation = data.getStringExtra("operation");

                    if (latitude > 0 && longitude > 0)
                        updatelocation(latitude, longitude, accuracy, provider);
                    else if (!webconfigurration.GPSMandatory){
                        updatelocation(latitude, longitude, accuracy, "");
                    }else
                        Toast.makeText(EditCustomerActivity.this,
                                R.string.location_not_found,Toast.LENGTH_SHORT).show();


                }
                if (locationaccuracy()!=3) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(EditCustomerActivity.this);
                    builder.setTitle(R.string.enable_high_loc_accuracy);
                    builder.setMessage(R.string.loc_acc_low);


                    builder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            });

                    builder.show();

                    return;
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(EditCustomerActivity.this,
                            R.string.cancelled,Toast.LENGTH_SHORT).show();

                    // Write your code if there's no result
                }
            }

        } catch (Exception e) {
            Toast.makeText(EditCustomerActivity.this,R.string.turn_off_high_quality_img,Toast.LENGTH_SHORT).show();
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        return Bitmap
                .createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }

    private void createDirectoryAndsavetempfile(Bitmap imageToSave,
                                                String fileName) {

        File rootsd = Environment.getExternalStorageDirectory();
        File direct = new File(rootsd.getAbsolutePath() + "/Aries");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        File file = new File(direct, fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 75, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(EditCustomerActivity.this,CustomerListActivity.class);
        finish();
        startActivity(i);
        overridePendingTransition(0,0);
    }

    private void validateAndCaptureGPS() {

        turnOnGPS();

        String address = ((EditText) findViewById(R.id.address)).getText().toString();
        String contactnumber = ((EditText) findViewById(R.id.contactnumber)).getText().toString();
        String gstin = ((EditText) findViewById(R.id.gstin)).getText().toString();
        String town = ((AutoCompleteTextView) findViewById(R.id.TownAuto)).getText().toString();
        String contactperson = ((EditText) findViewById(R.id.contactperson)).getText().toString();
        String closingday = ((Spinner) findViewById(R.id.spinnerclosing_day)).getSelectedItem().toString();
        String gstType = ((Spinner) findViewById(R.id.spinnergsttype)).getSelectedItem().toString();
        String fssi = ((EditText) findViewById(R.id.fssi)).getText().toString();
        closingday = closingday.contentEquals(getResources().getString(R.string.SELECT)) ? "" : closingday;
        String isvalid = valid(address, gstin, town, contactnumber,contactperson,gstType);


        if (!isvalid.contentEquals("TRUE")) {
            Toast.makeText(EditCustomerActivity.this, isvalid, Toast.LENGTH_SHORT).show();
            return;
        }


        if (!PlayTracker.intime()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(EditCustomerActivity.this);
            builder.setTitle(R.string.gps_unavailable);
            builder.setMessage(R.string.check_your_gps);

            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

            AlertDialog dialog = builder.create();
            builder.show();

            return;
        }
        if (locationaccuracy()!=3) {
            AlertDialog.Builder builder = new AlertDialog.Builder(EditCustomerActivity.this);
            builder.setTitle(R.string.enable_high_loc_accuracy);
            builder.setMessage(R.string.loc_acc_low);

            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    });

            builder.show();

            return;
        }

        Intent intent = new Intent(EditCustomerActivity.this,
                GPSAccuracy.class);
        intent.putExtra("operation", "addlocation");
        startActivityForResult(intent, REQUEST_CODE_GPS_ACCURACY);
        overridePendingTransition(0, 0);

    }

    private void turnOnGPS() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!statusOfGPS) {

            AlertDialog.Builder builder = new AlertDialog.Builder(EditCustomerActivity.this);
            builder.setTitle(R.string.switch_on_gps);
            builder.setMessage(R.string.gps_off_alert_message);


            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            startActivityForResult(
                                    new Intent(
                                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                    0);
                        }
                    });


            builder.show();

            return;
        }
    }


    private int locationaccuracy(){
        int locationMode=-1;
        try {
            locationMode = Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {

            e.printStackTrace();
        }
        return locationMode;
    }
}
