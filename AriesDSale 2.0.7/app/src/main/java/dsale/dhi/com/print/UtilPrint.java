/**
 * 
 */
package dsale.dhi.com.print;

/**
 * @author dhisigma
 *
 */
public class UtilPrint {

	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;
	
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";
	
	public static final String PRINTER_NAME="SILBT";
	
	/**
	 * 
	 * @param actualString
	 * @param length
	 * @param alignment - 0 for left,1 for right,2 for middle -used when actual length smaller than defined length
	 * @return formatted string
	 */
	public static String getPrintFormat (String actualString,int length,int alignment){

		String formated = "";

		if(actualString.length() == length){
			formated = actualString ;
		}else if (actualString.length() > length) {
			formated = actualString.substring(length);
			formated =	actualString.substring(0, length-3) + ".. ";



		}else if (actualString.length() < length) {
			if (alignment == 0) {//left alignmnt - right padding
				formated =	String.format("%-"+length+"s", actualString) ;

			}else if (alignment == 1) {//right alignmnt -left  padding


				formated =	String.format("%"+length+"s", actualString) ;

			}else if (alignment == 2) { //middle -padding for both sides.
				/*formated =	String.format("%-"+length/2+"s", actualString) ; //.replace(' ', '*')
				formated =	String.format("%"+length+"s", formated) ; */

				int before = (length - actualString.length())/2;
				if (before == 0)
					return String.format("%-" + length + "s", actualString);
				int rest = length - before;
				return String.format("%" + before + "s%-" + rest + "s", "", actualString); 
			}

		}


		return formated;
	}
	
	 
}
