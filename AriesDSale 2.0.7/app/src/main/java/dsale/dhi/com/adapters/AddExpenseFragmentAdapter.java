package dsale.dhi.com.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.List;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.database.DatabaseHandler;

@SuppressLint("ValidFragment")
public class AddExpenseFragmentAdapter extends Fragment {
    private Activity activity;
    private View mView;
    private static DatabaseHandler db;

    @SuppressLint("ValidFragment")
    public AddExpenseFragmentAdapter(Activity activity) {
        this.activity =activity ;
        db = new DatabaseHandler(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_expense, container, false);
        Spinner expenseType = (Spinner) mView.findViewById(R.id.expenseTypesSpinner);
        SpinnerAdapter expenseTypesAdapter = initializeExpenseTypes();
        expenseType.setAdapter(expenseTypesAdapter );
        return mView;
    }
    private SpinnerAdapter initializeExpenseTypes() {
        List<String> expenseTypes = db.getExpenseTypes(getResources().getString(R.string.dropdown_select));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_item, expenseTypes) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#606060"));
                    tv.setTextSize(12);
                    String fontPath = "assets/font/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            activity.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        dataAdapter .setDropDownViewResource(R.layout.spinner_additem_dropdown);
        return dataAdapter;
    }
}
