package dsale.dhi.com.Reports;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.Global;

public class DailySalesReportActivity extends AppCompatActivity {

    private boolean openedItemLayout = false;
    private LinearLayout salesReportLayout ;
    private LinearLayout itemListLayout ;
    private DatabaseHandler db;
    private SharedPreferences pref;

    private TextView dateTV;
    private TextView orgTV;
    private TextView salesPersonTV;
    private TextView distributorTV;
    private TextView routeTV;
    private TextView newShopsTV;
    private TextView shopVisitedTV;
    private TextView billedShopsTV;
    private TextView saleValueTV;
    private TextView distanceTravelledTV;
    private TextView branchTV;
    private TextView branchLabelTv;
    private TextView cashTV;
    private TextView creditTV;
    private TextView bankdepoTV;
    private TextView billopeningTV;
    private TextView billclosingTV;
    private TextView opstockTV;
    private TextView materialIssueTV;
    private TextView damageQtyTV;
    private TextView saleQtyTV;
    private TextView clStockQtyTV;
    private TextView creditCollectedTV;
    private TextView totCollectionTV;
    private TextView expenseTV;
    private TextView netCashTV;
    private TextView cashinHandTV;
    private  TextView totalQtyTv;
    private HashMap<String, JSONObject> hashmap;
    private TableLayout list;
    private TableRow row;
    private TableRow.LayoutParams lp;
    private TextView closingStockTV;
    DecimalFormat twoDForm ;
    private int roundoffPrecision ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_sales_report);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        salesReportLayout = (LinearLayout)findViewById(R.id.salesReportLayout);
        itemListLayout = (LinearLayout)findViewById(R.id.salesItemSummaryLayout);
        totalQtyTv= (TextView) findViewById(R.id.totalQtyTv);
        JSONObject summarydata = new JSONObject();
        db = new DatabaseHandler(getApplicationContext());
        openedItemLayout = false;
        //=====================================================
        pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        roundoffPrecision = pref.getInt("RoundOffPrecision",0);

        list = (TableLayout) findViewById(R.id.dsrList);
//        lp = new LayoutParams(LayoutParams.MATCH_PARENT,
//                LayoutParams.WRAP_CONTENT);
        twoDForm = new DecimalFormat("#.##");
        dateTV = (TextView) findViewById(R.id.dateTv);
        orgTV = (TextView) findViewById(R.id.orgTv);
        salesPersonTV = (TextView) findViewById(R.id.dseNameTv);
        routeTV = (TextView) findViewById(R.id.routeTv);
        newShopsTV=(TextView)findViewById(R.id.newShopsTv);
        shopVisitedTV= (TextView) findViewById(R.id.visitedTv);
        billedShopsTV = (TextView) findViewById(R.id.billedShopsTv);
        saleValueTV = (TextView) findViewById(R.id.saleValTv);
        branchTV=(TextView)findViewById(R.id.branchTv);
        branchLabelTv=(TextView) findViewById(R.id.branchLabelTv);
        cashTV=(TextView)findViewById(R.id.cashSaleTv);
        creditTV=(TextView)findViewById(R.id.creditSaleTv);
        billopeningTV=(TextView)findViewById(R.id.billOpenTv);
        billclosingTV=(TextView)findViewById(R.id.billClosingTv);
        opstockTV=(TextView)findViewById(R.id.OpStockTv);
        materialIssueTV=(TextView)findViewById(R.id.materialIssuedTv);
        closingStockTV=(TextView)findViewById(R.id.closingStockTv) ;
//        damageQtyTV=(TextView)findViewById(R.id.dsrDamageQtyvalue);
//        clStockQtyTV=(TextView)findViewById(R.id.dsrClStockQtyvalue);
//        bankdepoTV=(TextView)findViewById(R.id.dsrBankDepovalue);
//        distanceTravelledTV = (TextView) findViewById(R.id.dsrDistancevalue);
        //distributorTV = (TextView) findViewById(R.id.dsrDistributorvalue);
        saleQtyTV=(TextView)findViewById(R.id.saleQtyTv);
        creditCollectedTV=(TextView)findViewById(R.id.creditCollectedTv);
        totCollectionTV=(TextView)findViewById(R.id.totalCollectionTv);
        expenseTV=(TextView)findViewById(R.id.expenseTv);
        netCashTV=(TextView)findViewById(R.id.netCashTv);
        cashinHandTV=(TextView)findViewById(R.id.cashInHandTv);
        //=====================================================
        try{
            String scheduleheaderid = db.getscheduleheaderid();
            String scheduedetailid=db.getscheduledetailid(scheduleheaderid);
            if (db.isserviceexist("Van Sales")) {

                branchTV.setVisibility(View.GONE);
                branchLabelTv.setVisibility(View.GONE);
                billopeningTV.setText( db.billOpenigClosing("ASC"));
                billclosingTV.setText( db.billOpenigClosing("DESC"));
                String cashamt=db.getpaymentmodeval("Cash");
                BigDecimal cashCollected = new  BigDecimal(cashamt);
                cashCollected = cashCollected.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
//                cashTV.setText(  twoDForm.format(Math.round(Double.parseDouble(cashamt))));

                cashTV.setText(cashCollected.toString());

              /*  double taxableAmnt = db.getTotalTaxableAmount();
                double taxAmnt = db.getTotalTaxAmount();

                double ordAmnt = taxableAmnt+taxAmnt ;
                ordAmnt = Math.round(ordAmnt);

                double creditamt= ordAmnt - Double.parseDouble(cashamt);*/



                BigDecimal ordAmnt = db.getTotalOrderAmount(roundoffPrecision) ;


//                double creditamt= ordAmnt - Double.parseDouble(cashamt);
                BigDecimal creditamt= ordAmnt.subtract(cashCollected);
                if (roundoffPrecision == 0){
                    creditamt = creditamt.setScale(2,BigDecimal.ROUND_HALF_UP  );
                }
                creditamt = creditamt.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP  );

                saleValueTV.setText(ordAmnt.toString());
                creditTV.setText( creditamt.toString() );
                String creditcollected="0";// db.getcreditcollected();
                creditCollectedTV.setText( creditcollected);
//                Double totcollection=Double.parseDouble(cashamt)+ Double.parseDouble(creditcollected);
                BigDecimal totcollection= cashCollected.add(new BigDecimal(creditcollected));
                totcollection = totcollection.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
                totCollectionTV.setText( totcollection.toString());

                BigDecimal expense=db.getTotalExpense();
                expense = expense.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
                expenseTV.setText( expense.toString());

//                BigDecimal netcash=totcollection-expense;
                BigDecimal netcash=totcollection.subtract( expense) ;
                netcash = netcash.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
                netCashTV.setText( netcash.toString());
//                Double bankdeposit=0.0;
//                bankdepoTV.setText( String.valueOf(bankdeposit));
//                cashinHandTV.setText( String.valueOf(netcash-bankdeposit));
                cashinHandTV.setText( netcash.toString());
                //	SALE QTY:
                HashMap<String, JSONObject> hashmap=new HashMap<>();
                hashmap =db.getDailySalesReport();

                JSONArray listArray = new JSONArray();
                for (String key : hashmap.keySet()) {
                    listArray.put(hashmap.get(key));
                }

                double totalQty = 0.0;

                for (int i = 0; i < listArray.length(); i++) {
                    JSONObject item = listArray.optJSONObject(i);
                    if (item != null) {
                        String category = item.optString("Category", "");
                        String qtyString = item.optString("Qty", "0");


                        try {
                            double qty = Double.parseDouble(qtyString );

                            totalQty += qty;
                        }
                        catch(Exception e){
                            e.printStackTrace();
                        }
                    }}
                //saleQtyTV.setText( twoDForm.format(totalQty));
                double damageqty=0.0;
//                damageQtyTV.setText( String.valueOf(damageqty));
                String orgid=pref.getString("tmsOrgId", "");
                double salesQtyFrmInvntry=db.getSalesQty(orgid);
                saleQtyTV.setText( twoDForm.format(salesQtyFrmInvntry));
                double opStock =  db.getOpStock(orgid,"opstockqty");
                double matIssuedStock=db.getMatIssued(orgid,"opstockqty");
                double totalOutward=db.getTotalOutward(orgid,"opstockqty");
                opstockTV.setText(String.valueOf(twoDForm.format(opStock)));
                materialIssueTV.setText( String.valueOf(twoDForm.format(matIssuedStock)));
                double closingStockVal=(opStock+matIssuedStock)-(salesQtyFrmInvntry+totalOutward);
                closingStockTV.setText(String.valueOf(twoDForm.format(closingStockVal)));

                //			cl stock qty
                try {
                    hashmap=new HashMap<>();
                    hashmap = db.getvansalesstockreport(orgid);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Double totavailableqty=0.0;

                for (Map.Entry<String, JSONObject> entry : hashmap.entrySet()) {

                    JSONObject value = entry.getValue();

                    String itemname = value.optString("productname", "");
                    String availableqty = value.optString("availableqty", "0");

                    totavailableqty +=Double.parseDouble(availableqty);

                }
//                clStockQtyTV.setText( String.valueOf(totavailableqty));
                newShopsTV.setText( db.getNewShops());

            }
            else{
                String brnchName=db.getcustomerbranch(scheduedetailid);
                branchTV.setText( db.getcustomerbranch(scheduedetailid));
                cashTV.setVisibility(View.GONE);
                creditTV.setVisibility(View.GONE);
                billopeningTV.setVisibility(View.GONE);
                billclosingTV.setVisibility(View.GONE);
                opstockTV.setVisibility(View.GONE);
                materialIssueTV.setVisibility(View.GONE);
                saleQtyTV.setVisibility(View.GONE);
                creditCollectedTV.setVisibility(View.GONE);
                totCollectionTV.setVisibility(View.GONE);
                netCashTV.setVisibility(View.GONE);
                cashinHandTV.setVisibility(View.GONE);
//                damageQtyTV.setVisibility(View.GONE);
//                bankdepoTV.setVisibility(View.GONE);
//                clStockQtyTV.setVisibility(View.GONE);

//                TextView cashTVLabel=(TextView)findViewById(R.id.dsrCashSale);
//                TextView creditTVLabel=(TextView)findViewById(R.id.dsrCreditSale);
//                TextView bankdepoTVLabel=(TextView)findViewById(R.id.dsrBankDepo);
//                TextView billopeningTVLabel=(TextView)findViewById(R.id.dsrBillOpen);
//                TextView billclosingTVLabel=(TextView)findViewById(R.id.dsrBillClose);
//                TextView opstockTVLabel=(TextView)findViewById(R.id.dsrOpStock);
//                TextView materialIssueTVLabel=(TextView)findViewById(R.id.dsrMaterialIssue);
//                TextView damageQtyTVLabel=(TextView)findViewById(R.id.dsrDamageQty);
//                TextView saleQtyTVLabel=(TextView)findViewById(R.id.dsrSaleQty);
//                TextView clStockQtyTVLabel=(TextView)findViewById(R.id.dsrClStockQty);
//                TextView creditCollectedTVLabel=(TextView)findViewById(R.id.dsrCreditCollect);
//                TextView totCollectionTVLabel=(TextView)findViewById(R.id.dsrTotCollect);
//                TextView netCashTVLabel=(TextView)findViewById(R.id.dsrnetCash);
//                TextView cashinHandTVLabel=(TextView)findViewById(R.id.dsrCashinHand);

//                cashTVLabel.setVisibility(View.GONE);
//                creditTVLabel.setVisibility(View.GONE);
//                bankdepoTVLabel.setVisibility(View.GONE);
//                billopeningTVLabel.setVisibility(View.GONE);
//                billclosingTVLabel.setVisibility(View.GONE);
//                opstockTVLabel.setVisibility(View.GONE);
//                materialIssueTVLabel.setVisibility(View.GONE);
//                damageQtyTVLabel.setVisibility(View.GONE);
//                saleQtyTVLabel.setVisibility(View.GONE);
//                clStockQtyTVLabel.setVisibility(View.GONE);
//                creditCollectedTVLabel.setVisibility(View.GONE);
//                totCollectionTVLabel.setVisibility(View.GONE);
//                netCashTVLabel.setVisibility(View.GONE);
//                cashinHandTVLabel.setVisibility(View.GONE);

                BigDecimal expense=db.getTotalExpense();
                expense = expense.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
                expenseTV.setText( String.valueOf(expense));

                String ordamt = db.getorderamttot();
                saleValueTV.setText( ordamt);
            }
            orgTV.setText( pref.getString("pesronorg", ""));
            String spName=pref.getString("personname", "");
            salesPersonTV.setText(spName);

            summarydata = db.getworksummary();
            shopVisitedTV.setText( String.valueOf(summarydata .getInt("visitedstores")));

            billedShopsTV.setText( String.valueOf(summarydata .getInt("productivestores")));



            double distance_current = (double) (pref.getFloat(
                    "distancetravelled", 0) / 1000);
//            distanceTravelledTV.setText( String.valueOf(round(distance_current, 2)) + " Km ");



            String scheduledate =  pref.getString("Schbegintime", "");
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

            DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");

            Date temp = dateTimeFormat.parse(scheduledate);
            dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy");

            dateTV.setText( dateTimeFormat.format(temp));

            setProductSaleGrid(db);

            JSONObject data = db.getRouteAndDistributor();
            String actschdlId=db.getactualscheduleid();
            String schdlName=db.grtSchdlNameFrmschdlId(actschdlId);
            //routeTV.setText( data.optString("RouteId","0"));
            routeTV.setText(schdlName);
            //distributorTV.setText("DISTRIBUTOR : "+data.optString("Distributor","NA"));

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void expandSalesItems(View v){

       // Toast.makeText(getApplicationContext(),"Expand Clicked :"+openedItemLayout,Toast.LENGTH_LONG).show();
        ImageView view = (ImageView) v;
        if(!openedItemLayout){

            salesReportLayout.setVisibility(View.GONE);
            itemListLayout.setVisibility(View.VISIBLE);
            openedItemLayout =true;


            view.setImageResource(R.drawable.ic_expand_more_black_24dp);

        }else{

            salesReportLayout.setVisibility(View.VISIBLE);
            itemListLayout.setVisibility(View.GONE);
            openedItemLayout =false ;


            view.setImageResource(R.drawable.ic_expand_less_black_24dp);
        }

    }

    private void setProductSaleGrid(DatabaseHandler dbHandler) throws Exception {


        hashmap =dbHandler.getDailySalesReport();

        JSONArray listArray = new JSONArray();
        for (String key : hashmap.keySet()) {
            listArray.put(hashmap.get(key));
        }
        Log.d("result :", ""+hashmap.size());
        double totalQty = 0.0;
        for (int i = 0; i < listArray.length(); i++) {
            JSONObject item = listArray.optJSONObject(i);
            if (item != null) {
                String category = item.optString("Category", "");
                String qtyString = item.optString("Qty", "0");
                String packageType = item.optString("PackageType", "kg");

                double qty = 0;

                try {
                    qty = Double.parseDouble(qtyString );

                    totalQty += qty;


                } catch (Exception e) {
                    e.printStackTrace();

                }

                row = new TableRow(DailySalesReportActivity.this);
                row.setWeightSum(10);
                row.setBackgroundResource(R.color.darkgray);
                row.addView(addtext(category, i,6));
                row.addView(addtext(String.valueOf(twoDForm.format(qty)), i,2.3f));
                row.addView(addtext(packageType, i,2));
                list.addView(row);
            }
        }
        totalQtyTv.setText(String.valueOf(twoDForm.format(totalQty)));

//        a = new Font(DailySalesReportActivity.this, (ViewGroup) findViewById(android.R.id.content), Font.Regular);

    }
    private TextView addtext(String string, int i,float weight) {
        TextView text = new TextView(DailySalesReportActivity.this);
        TableRow.LayoutParams lp=new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 0.6f);
        lp.weight=weight;
        text.setLayoutParams(lp);
        text.setText(string);
        if (i % 2 == 0)
            text.setBackgroundResource(R.color.white);
        else
            text.setBackgroundResource(R.color.gray3);
        text.setPadding(10, 10, 10, 10);
//        a = new Font(DailySalesReportActivity.this, text, Font.Regular);
        return text;
    }
    public static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        String l = Global.setLastTime();
    }

    //Added By Jithu on 17/05/2019 for back press button redirect to customer list page
    @Override
    public void onBackPressed() {

        Intent intent=new Intent(DailySalesReportActivity.this, CustomerListActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }
}
