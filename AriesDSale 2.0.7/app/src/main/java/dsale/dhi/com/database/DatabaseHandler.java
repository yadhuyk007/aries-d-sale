package dsale.dhi.com.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import dsale.dhi.com.ariesdsale.Product;
import dsale.dhi.com.customer.Locations;
import dsale.dhi.com.objects.DateError;
import dsale.dhi.com.objects.FourStrings;
import dsale.dhi.com.objects.Global;
import dsale.dhi.com.objects.PlayLocation;
import dsale.dhi.com.objects.Scheduledata;
import dsale.dhi.com.objects.Scheduledetail;
import dsale.dhi.com.objects.Vehicledata;
import dsale.dhi.com.objects.product;
import dsale.dhi.com.order.OrderLine;
import dsale.dhi.com.ariesdsale.CustomerVisibility;

/**
 * Created by hp on 03/04/2019.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    public boolean checkWithSyncTime = true;


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Vehiclenavigationdb";

    private static final String TABLE_NAVIGATIONDATA = "navigationdatatable";
    private static final String KEY_NAVIGATIONID = "navigationid";
    private static final String KEY_PLANID = "planid";
    private static final String KEY_VEHICLEID = "vehicleid";
    private static final String KEY_DEVICETIME = "devicetime";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";
    private static final String KEY_SPEED = "speed";
    private static final String KEY_PROVIDER = "provider";
    private static final String KEY_BEARING = "bearing";
    private static final String KEY_ACCURACY = "accuracy";
    private static final String KEY_DISTANCE = "distance";
    private static final String NAVIGATIONDATA_UPFLAG = "upflag";
    private static final String KEY_CUSTOMER = "customerid";
    private static final String KEY_TIMEDIFF = "timediff";
    private static final String KEY_MONGODT = "mongodt";
    private static final String KEY_INACTIVE = "inactive";

    // private static final String TABLE_JSONS2 = "jsons2";
    // private static final String JSONS_ID2 = "id";
    // private static final String JSONS_TITLE2 = "title";
    // private static final String JSONS_DATA2 = "data";
    // private static final String JSONS_DATE2 = "curDate";

    private static final String TABLE_JSONS = "jsons";
    private static final String JSONS_ID = "id";
    private static final String JSONS_TITLE = "title";
    private static final String JSONS_DATA = "data";
    private static final String JSONS_DATE = "curDate";

    // mobile services
    private static final String TABLE_MOBSERVICES = "mobileServices";
    private static final String MOBILESERVICE_ID = "id";
    private static final String MOBILESERVICE_NAME = "title";
    private static final String MOBILESERVICE_CODE = "code";

    private static final String MOBILESERVICE_DISABLE_INVENTORY = "inventoryFlag";

    // schedule header
    private static final String TABLE_SCHEDULE_HEADER = "scheduleheader";
    private static final String SCHEDULE_HEADER_PKID = "scheduleheaderpkid";
    private static final String SCHEDULE_ID = "scheduleid";
    private static final String SCHEDULE_NAME = "schedulename";
    private static final String SALESPERSON_ID = "salespersonid";
    private static final String SALESPERSON_NAME = "salespersonname";
    private static final String SALEROUTE_ID = "salesrouteid";
    private static final String SCHEDULE_DATE = "scheduledate";
    private static final String HEADER_STATUS = "headerstatus";
    private static final String ROUTENETWORK_ID = "routenetworkid";
    private static final String SCHEDULE_COMPLETION_STATUS = "cmpletionstatus";
    private static final String SCHEDULE_BEGIN_TIME = "begintime";
    private static final String SCHEDULE_COMPLETION_TIME = "cmpletiontime";

    // scheduled detail
    private static final String TABLE_SCHEDULE_DETAIL = "scheduledetail";
    private static final String SCHEDULE_DETAIL_PKID = "scheduledetailpkid";
    private static final String SCHEDULE_DETAIL_ID = "scheduledetailid";
    private static final String SCHEDULE_HEADER_ID = "scheduleheadid";
    private static final String LOCATION_ID = "locationid";
    private static final String LOCATION_NAME = "locationname";
    private static final String LOCATION_ADRESS = "locationadress";
    private static final String SEQNO = "sequencenumber";
    private static final String DETAIL_STATUS = "status";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String SCHEDULEDETAIL_CREATETIME = "createtime";
    private static final String LATITUDE_NEW = "latitudenew";
    private static final String LONGITUDE_NEW = "longitudeNEW";
    private static final String CUSTOMERCATOGORY = "customercatogory";
    private static final String RETAILPRICE = "retailprice";
    private static final String INVOCENUMBER = "invoicenumber";
    private static final String PICKLISTNUMBER = "picklistnumber";
    private static final String STOREVISIT_REASON = "reason";
    private static final String STOREVISIT_LATTITUDE = "storevisitlatitude";
    private static final String STOREVISIT_LONGITUDE = "storevisitlongitude";
    private static final String STOREVISIT_TIME = "storevisittime";
    private static final String SCHED_CUSTOMERCODE = "customercode";
    private static final String SCHED_LOCUPDATETIME = "locupdatetime";
    private static final String LOCATIONLOCK = "locationlock";
    private static final String CLOSING_DAY = "closingday";
    private static final String CLOSING_DAY_UPDATETIME = "closingdayupdatetime";
    private static final String SCHEDULE_DETAIL_PRODUCTIVE = "productive";
    private static final String SCHEDULE_DETAIL_VISITED = "visited";
    private static final String CUSTOMERCATOGORYID = "customercatogoryid";
    private static final String SCHEDULEDETAIL_MOBILE = "mobilenumber";
    private static final String SCHEDULEDETAIL_OTP = "otpnumber";
    private static final String SCHEDULEDETAIL_TINNUMBER = "tinnumber";
    private static final String SCHEDULEDETAIL_PAN = "pan";
    private static final String SCHEDULEDETAIL_EDITTIME = "edittime";
    private static final String SCHEDULEDETAIL_PHOTOUUID = "photouuid";
    private static final String SCHEDULEDETAIL_IMGLAT = "imglat";
    private static final String SCHEDULEDETAIL_IMGLONG = "imglong";
    private static final String SCHEDULEDETAIL_LANDMARK = "landmark";
    private static final String SCHEDULEDETAIL_TOWN = "town";
    private static final String SCHEDULEDETAIL_CONTACTPERSON = "contactperson";
    private static final String SCHEDULEDETAIL_OTHERCOMPANY = "othercompany";
    private static final String SCHEDULEDETAIL_CRATES = "crates";
    private static final String SCHEDULEDETAIL_OTPVERIFIED = "otpverified";
    private static final String SCHEDULEDETAIL_LOCPROVIDER = "locprovider";
    private static final String SCHEDULEDETAIL_LOCACCURACY = "accuracy";
    private static final String SCHEDULEDETAIL_EMAIL = "email";
    private static final String SCHEDULEDETAIL_DIRECTCOV = "directcov";
    private static final String SCHEDULEDETAIL_SELPRODCAT = "selprodcat";
    private static final String SCHEDULEDETAIL_STORECLOSED = "storeclosed";
    private static final String SCHEDULEDETAIL_PNGCOVERED = "pngcovered";
    private static final String SCHEDULEDETAIL_BRANCH = "branch";
    private static final String SCHEDULEDETAIL_PASSBY = "passby";
    private static final String SCHEDULEDETAIL_PASSBYCRTIME = "passcrtime";
    private static final String SCHEDULEDETAIL_ALTERCUSTCODE = "AlternateCustcode";
    private static final String SCHEDULEDETAIL_LATTITUDE = "storereasonlatitude";
    private static final String SCHEDULEDETAIL_LONGITUDE = "storereasonlongitude";
    private static final String SCHEDULEDETAIL_CUSTTYPE = "custtype";

    private static final String SCHEDULEDETAIL_TARGET_TYPE = "targetType";
    private static final String SCHEDULEDETAIL_TARGET_VALUE = "targetVal";
//    private static final String SCHEDULEDETAIL_CREDIT_LIMIT = "creditLimit";
//    private static final String SCHEDULEDETAIL_CREDIT_BALANCE = "creditBal";

    private static final String SCHEDULEDETAIL_GSTTYPE = "gsttype";
    private static final String SCHEDULEDETAIL_FSSI = "fssi";
    private static final String SCHEDULEDETAIL_STATE_CODE = "stateCode";

    // order header
    private static final String TABLE_ORDER_HEADER = "orderheader";
    private static final String ORDER_HEADER_PKID = "orderheaderpkid";
    private static final String ORDER_SCHEDULE_DETAIL_ID = "scheduledetailid";
    private static final String ORDER_STATUS = "status";
    private static final String ORDERBEGIN_TIME = "orderbegintime";
    private static final String ORDERSUBMIT_TIME = "ordersubmittime";
    private static final String ORDER_LATITUDE = "latitude";
    private static final String ORDER_LONGITUDE = "longitude";
    private static final String ORDER_SALESPERSONID = "salespersonid";
    private static final String ORDER_ROUTENETWORKID = "routenetworkid";
    private static final String ORDER_LOCATIONID = "locationid";

    // order detail
    private static final String TABLE_ORDER_DETAIL = "orderdetail";
    private static final String TABLE_ORDERDETAIL_PKID = "orderheaddetailpkid";
    private static final String ORDER_HEAD_ID = "orderheaderid";
    private static final String PRODUCTID = "productid";
    private static final String QUANTITY = "quantity";
    private static final String DELIVERYDATE = "deliverydate";
    private static final String CREATETIME = "creattime";
    private static final String DELETEFLAG = "deleteflag";
    private static final String RATE = "rate";
    private static final String PRODUCT_TAX = "tax";
    private static final String TOTTAL_AMOUNT = "tottalamount";

    // Sync table
    private static final String TABLE_SYNCTABLE = "syncronisingtable";
    private static final String INDEXPKID = "indexpkid";
    private static final String TABLEINSYNC = "syncingtable";
    private static final String SYNCTIME = "synctime";

    // Location table
    private static final String TABLE_LOCATIONS = "loactiontable";
    private static final String LOCATION_PKID = "locationpkid";
    private static final String LOCATIONID = "locationid";
    private static final String NAME = "name";
    private static final String ADDRESS = "adress";
    private static final String LOCATION_LATITUDE = "latitude";
    private static final String LOCATION_LONGITUDE = "longitude";
    private static final String LOCATION_CREATETIME = "createtime";
    private static final String CUSTOMER_CODE = "costomercode";
    private static final String CUSTOMER_CATOGORY = "customercatogory";
    private static final String TIN_NUMBER = "tinnumber";
    private static final String NEAREST_LANDMARK = "nearestlandmark";
    private static final String CONTACT_PERSONNAME = "contactpersonname";
    private static final String CONTACT_PERSON_NUMBER = "contactnumber";
    private static final String ANT_OTHERCOMPANYVALUE = "anyothercompany";
    private static final String TOWNCLASS = "townclass";
    private static final String SELECTED_PRODUCT_CATEGORIES = "selectedproductcategories";
    private static final String CUSTOMERCATEGOY_CODE = "customercategorycode";
    private static final String CUSTOMER_SCHEDULELINE_GUID = "schedulelineguid";
    private static final String LOCATION_PAN = "Pannumber";
    private static final String LOCATION_OTPVERIFIED = "otpverified";
    private static final String LOCATION_LOCPROVIDER = "locprovider";
    private static final String LOCATION_LOCACCURACY = "locaccuracy";
    private static final String LOCATION_EMAIL = "email";
    private static final String LOCATION_PHOTOUUID = "photouuid";
    private static final String LOCATION_DIRECTCOV = "directcov";
    private static final String LOCATION_CLOSING_DAY = "closingday";
    private static final String LOCATION_STOREISCLOSED = "storeclosed";
    private static final String LOCATION_PNGCOVERED = "pngcovered";
    private static final String LOCATION_BRANCH = "branch";
    private static final String LOCATION_DRUG = "drug";
    private static final String LOCATION_LOCALITY = "localty";
    private static final String LOCATION_CITY = "city";
    private static final String LOCATION_STATE = "state";
    private static final String LOCATION_PIN = "pin";
    private static final String LOCATION_COVDAY = "covday";
    private static final String LOCATION_WEEK1 = "week1";
    private static final String LOCATION_WEEK2 = "week2";
    private static final String LOCATION_WEEK3 = "week3";
    private static final String LOCATION_WEEK4 = "week4";
    private static final String LOCATION_VISTFRQ = "vistfrq";
    private static final String LOCATION_TYPE = "typee";
    private static final String LOCATION_WHOLSAL = "wholsal";
    private static final String LOCATION_METRO = "metro";
    private static final String LOCATION_CLASSIF = "classif";
    private static final String LOCATION_LATITUDEPHOTO = "latitudePhoto";
    private static final String LOCATION_LONGITUDEPHOTO = "longitudePhoto";
    private static final String LOCATION_REMARKS = "remarks";
    private static final String LOCATION_GSTTYPE = "gstType";
    private static final String LOCATION_FSSI = "fssi";

    // Lpreviousorderscachetable
    private static final String TABLE_PREVIOUSORDERS = "previousorders";
    private static final String PREVIOUSORDERS_PKID = "previousorderspkid";
    private static final String PREVIOUSORDERS_LOCATIONID = "locationid";
    private static final String PREVIOUSORDERS_SCHEDULEDETAILID = "scheduledetailid";
    private static final String DATA = "data";

    // delivery payment table
    private static final String TABLE_PAYMENTS = "payments";
    private static final String PAYMENT_PKID = "Pymentid";
    private static final String PAYMENT_SCHEDULEDETAILID = "scheduledetailid";
    private static final String PAYMENT_METHOD = "paymentmethode";
    private static final String AMOUNT = "amount";
    private static final String CHEQUENUMBER = "chequenumber";
    private static final String IFSC = "ifsc";
    private static final String PAYMENT_CREATETIME = "createtime";
    private static final String DELETE_FLAG = "deleteflag";
    private static final String PAYMENTGUID = "paymentid";
    private static final String SALESPERSONID = "salespersonid";
    private static final String PAYMENT_LOCATIONID = "locationid";
    private static final String CHEQE_DATE = "chequedate";
    private static final String PAYMENT_DATE = "paymentdate";
    private static final String PAYMENT_INVOCENUMBER = "invoicenumber";
    private static final String PAYMENT_PICKLISTNUMBER = "picklistnumber";
    private static final String PAYMENT_LATTITUDE = "lattitude";
    private static final String PAYMENT_LONGITUDE = "longitude";
    private static final String PAYMENT_ACTUALMODE = "actualmode";
    private static final String PAYMENT_BANKCODE = "bankcode";
    private static final String PAYMENT_APROOVEFLAG = "aprooveflag";
    private static final String PAYMENT_SIGNING_PERSON = "signingperson";
    private static final String PAYMENT_FILEID = "fileid";
    private static final String PAYMENT_VERIFICATIONCODE = "verificationcode";
    private static final String PAYMENT_STATUS = "Status";

    // Pricetable
    private static final String TABLE_PRICEDETAILS = "pricelist";
    private static final String PRICEDETAILS_PKID = "pricetableid";
    private static final String PRICEDETAILS_PRODUCTID = "productid";
    private static final String PRICEDETAILS_CUSTOMERCATEGORY = "customercategory";
    private static final String PRICE = "price";
    private static final String VALIDFROM = "validfrom";
    private static final String VALIDTO = "validto";
    private static final String MRP = "mrp";

    // invoice table
    private static final String TABLE_INVOICE = "invoice";
    private static final String INVOICE_PKID = "invoicepkid";
    private static final String INVOICE_ID = "invoiceid";
    private static final String INVOICE_NO = "invoicenumber";
    private static final String INVOICE_CUSTOMERID = "customerid";
    private static final String INVOICE_TOTTALAMOUNT = "tottalamount";
    private static final String INVOICE_BALANCEAMOUNT = "balanceamount";
    private static final String INVOICE_STATUS = "status";
    private static final String INVOICE_DATE = "invoicedate";
    private static final String INVOICE_STATICBALANCE = "staticbalance";

    private static final String INVOICE_ORDERUUID = "orderUUID";

    // invoice payment table
    private static final String TABLE_INVOICE_PAYMENTS = "invoicepayments";
    private static final String INVOICEPAYMENT_PKID = "invoicepaymentpkid";
    private static final String INVOICEPAYMENT_HEADERID = "paymentheaderid";
    private static final String INVOICEPAYMENT_ID = "invoiceid";
    private static final String INVOICEPAYMENT_AMOUNTPAYED = "amount";
    private static final String INVOICEPAYMENT_BALANCE = "balanceamount";
    private static final String INVOICEPAYMENT_INVOICENO = "invoiceno";
    private static final String INVOICEPAYMENT_DELETEFLAG = "deleteflag";

    // tottal order amount table
    private static final String TABLE_ORDERAMOUNTDATA = "orderamountfromstore";
    private static final String ORDERAMOUNTDATA_PKID = "orderamountfromstorpkid";
    private static final String ORDERAMOUNTDATA_AMOUNT = "amount";
    private static final String ORDERAMOUNTDATA_SCHEDULEDETAILID = "scheduledetailid";
    private static final String ORDERAMOUNTDATA_CUSTOMERID = "customerid";
    private static final String ORDERAMOUNTDATA_PERSONID = "personid";
    private static final String ORDERAMOUNTDATA_TIME = "time";
    private static final String ORDERAMOUNTDATA_CREATETIME = "createtime";
    private static final String ORDERAMOUNTDATA_LATTITUDE = "lattitude";
    private static final String ORDERAMOUNTDATA_LONGITUDE = "longitude";
    private static final String ORDERAMOUNTDATA_GUID = "amountguid";

    // reason table
    private static final String TABLE_REASONS = "reasons";
    private static final String REASONS_PKID = "reasonpkid";
    private static final String REASONS_CODE = "reasoncode";
    private static final String REASON_DETAIL = "reasondetail";

    // customer serarch table
    private static final String TABLE_SEARCHCUSTOMERS = "customersearch";
    private static final String SEARCHCUSTOMERS_PKID = "pkid";
    private static final String SEARCHCUSTOMERS_ID = "custid";
    private static final String SEARCHCUSTOMERS_NAME = "customername";
    private static final String SEARCHCUSTOMERS_ADRESS = "adress";
    private static final String SEARCHCUSTOMERS_CODE = "customercode";
    private static final String SEARCHCUSTOMERS_LATTITUDE = "lattitude";
    private static final String SEARCHCUSTOMERS_LONGITUDE = "longitude";
    private static final String SEARCHCUSTOMERS_CATEGORY = "custcategory";
    private static final String SEARCHCUSTOMERS_LOCATIONLOCK = "locationlock";
    private static final String SEARCHCUSTOMERS_CLOSINGDAY = "closingday";
    private static final String SEARCHCUSTOMERS_MOBILENUMBER = "mobilenumber";
    private static final String SEARCHCUSTOMERS_CUSTTYPE = "custtype";

    // caustomercategorylist
    private static final String TABLE_CUSTOMERCATEGORIES = "customercategorylist";
    private static final String CUSTOMERCATEGORIES_PKID = "id";
    private static final String CUSTOMERCATEGORIES_CODE = "categorycode";
    private static final String CUSTOMERCATEGORIES_NAME = "name";
    private static final String CUSTOMERCATEGORIES_TYPE = "cattype";

    // productchecklist
    private static final String TABLE_PRODUCTCHECKLIST = "CHECKLIST";
    private static final String PRODUCTCHECKLIST_PKID = "id";
    private static final String PRODUCTCHECKLIST_CODE = "checklistcode";
    private static final String PRODUCTCHECKLIST_NAME = "checklistname";
    private static final String PRODUCTCHECKLIST_SEQNO = "seqno";

    // townlist
    private static final String TABLE_TOWN = "towns";
    private static final String TOWN_PKID = "id";
    private static final String TOWN_CODE = "towncode";
    private static final String TOWN_NAME = "townname";
    private static final String TOWN_ORG = "townorg";

    // denominationdata
    private static final String TABLE_DENOMINATIONS = "denominationdata";
    private static final String DENOMINATIONS_PKID = "id";
    private static final String DENOMINATIONS_CODE = "denominationcode";
    private static final String DENOMINATIONS_AMOUNT = "denominationamount";

    // create schedukesummary and cash denomination data
    private static final String TABLE_SCHEDULESUMMARY = "schedulesummary";
    private static final String SCHEDULESUMMARY_PKID = "id";
    private static final String SCHEDULESUMMARY_PERSONID = "personid";
    private static final String SCHEDULESUMMARY_CREATETIME = "createtime";
    private static final String SCHEDULESUMMARY_SUBMITTIME = "submittime";
    private static final String SCHEDULESUMMARY_LAT = "lattitude";
    private static final String SCHEDULESUMMARY_LONGI = "longitude";
    private static final String SCHEDULESUMMARY_UNIQUEID = "uniqueid";
    private static final String SCHEDULESUMMARY_DENOMEDATA = "denominationdata";
    private static final String SCHEDULESUMMARY_TOTTALCASH = "tottalcashamount";
    private static final String SCHEDULESUMMARY_SCHEDULEID = "scheduleid";
    private static final String SCHEDULESUMMARY_SCHEDULEIDLOCAL = "scheduleidlocal";
    private static final String SCHEDULESUMMARY_COMPLETION_TIME = "completiontime";
    private static final String SCHEDULESUMMARY_BEGIN_TIME = "begintime";

    // createbank master table

    private static final String TABLE_BANKMASTER = "bankmaster";
    private static final String BANKMASTER_PKID = "bankmasterpkid";
    private static final String BANKMASTER_CODE = "bankmastercode";
    private static final String BANKMASTER_NAME = "bankmastername";
    private static final String BANKMASTER_BRANCHNAME = "branchname";

    private static final String TABLE_ORDERHDR = "tableorderhdr";
    private static final String ORDERHDR_PKID = "id";
    private static final String ORDERHDR_SCHLINEID = "slineid";
    private static final String ORDERHDR_CUSTOMER = "customer";
    private static final String ORDERHDR_PERSONID = "personid";
    private static final String ORDERHDR_CUSTID = "custid";
    private static final String ORDERHDR_CREATETIME = "creattime";
    private static final String ORDERHDR_APPROVEFLAG = "approveflag";
    private static final String ORDERHDR_LATITUDE = "latitude";
    private static final String ORDERHDR_LONGITUDE = "longitude";
    private static final String ORDERHDR_NOTE = "notes";
    private static final String ORDERHDR_INVOICENO = "invoiceno";
    private static final String ORDERHDR_STATUS = "Status";
    private static final String ORDERHDR_PARENT_INVOICENO = "parentInvoiceno";
    private static final String ORDERHDR_PARENT_ORDERID = "parentOrderId";

    private static final String TABLE_ORDERLINE = "tableorderline";
    private static final String ORDERLINE_PKID = "id";
    private static final String ORDERLINE_HEADID = "headid";
    private static final String ORDERLINE_PRODID = "prodid";
    private static final String ORDERLINE_QTY = "qty";
    private static final String ORDERLINE_MRP = "mrp";
    private static final String ORDERLINE_RATE = "rate";
    private static final String ORDERLINE_CREATETIME = "creattime";
    private static final String ORDERLINE_DELETEFLAG = "deleteflag";
    /*	private static final String ORDERLINE_UOM = "uom";
    private static final String ORDERLINE_UPC = "upc";*/
    private static final String ORDERLINE_PM = "pm";
    private static final String ORDERLINE_TAX = "tax";
    private static final String ORDERLINE_BATCH = "batch";
    private static final String ORDERLINE_DISCOUNT = "discount";
    private static final String ORDERLINE_DISCOUNTPER = "discountper";
    private static final String ORDERLINE_PACKAGE_TYPE = "packagetype";
    private static final String ORDERLINE_CANCELFLAG = "cancelflag";
    private static final String ORDERLINE_COMPLIMENT = "complimentFlag";

    // product table
    private static final String TABLE_PRODUCT = "producttable";
    private static final String PRODUCT_PKID = "productid";
    private static final String PRODUCT_ID = "productidunique";
    private static final String PRODUCT_NAME = "productname";
    private static final String PRODUCT_CODE = "productcode";
    private static final String PRODUCT_CAT = "productcat";
    private static final String PRODUCT_BRAND = "productbrand";
    private static final String PRODUCT_FORM = "productform";
    private static final String PRODUCT_HSN = "hsn";

    private static final String TABLE_MRP = "mrptable";
    private static final String MRP_PKID = "id";
    private static final String MRP_ID = "mrpid";
    private static final String MRP_PRODUCT = "product";
    private static final String MRP_VALUE = "value";
    private static final String MRP_FROM = "fromdat";
    private static final String MRP_TO = "todat";
    private static final String MRP_BATCH = "batch";
    private static final String MRP_CONSOFF = "consumeroff";
    private static final String MRP_FLATOFF = "flatoff";
    private static final String MRP_TAX = "tax";

    private static final String TABLE_PARTYMARG = "partymarg";
    private static final String PARTYMARG_PKID = "id";
    private static final String PARTYMARG_ID = "partyid";
    private static final String PARTYMARG_PRODUCT = "product";
    private static final String PARTYMARG_CAT = "cat";
    private static final String PARTYMARG_VALUE = "value";
    private static final String PARTYMARG_FROM = "fromdat";
    private static final String PARTYMARG_TO = "todat";
    private static final String PARTYMARG_CUSTOMER = "customer";

    private static final String TABLE_ADDLDISC = "addinltable";
    private static final String ADDLDISC_PKID = "id";
    private static final String ADDLDISC_ID = "discid";
    private static final String ADDLDISC_CAT = "cat";
    private static final String ADDLDISC_VALUE = "value";
    private static final String ADDLDISC_FROM = "fromdat";
    private static final String ADDLDISC_TO = "todat";

    private static final String TABLE_FOCUSEDPRDS = "focusedprods";
    private static final String FOCUSEDPRDS_PKID = "id";
    private static final String FOCUSEDPRDS_ID = "uiqueid";
    private static final String FOCUSEDPRDS_PRODUCT = "product";
    private static final String FOCUSEDPRDS_FROM = "fromdat";
    private static final String FOCUSEDPRDS_TO = "todat";
    private static final String FOCUSEDPRDS_CUSTCAT = "custcat";
    private static final String FOCUSEDPRDS_FLAG = "flag";
    private static final String FOCUSEDPRDS_MINQTY = "minqty";

    private static final String TABLE_PRODUCTCAT = "productcat";
    private static final String PRODUCTCAT_PKID = "id";
    private static final String PRODUCTCAT_ID = "prodid";
    private static final String PRODUCTCAT_NAME = "name";

    private static final String TABLE_BRAND = "brand";
    private static final String BRAND_PKID = "id";
    private static final String BRAND_ID = "brandid";
    private static final String BRAND_NAME = "name";

    private static final String TABLE_FORM = "form";
    private static final String FORM_PKID = "id";
    private static final String FORM_ID = "formid";
    private static final String FORM_NAME = "name";

    private static final String TABLE_INVENTORY = "inventorytable";
    private static final String INVENTORY_PKID = "id";
    private static final String INVENTORY_ID = "uniqeid";
    private static final String INVENTORY_PRODUCT = "invproduct";
    private static final String INVENTORY_QTY = "invqty";

    private static final String TABLE_SALESRETURN = "salesreturntable";
    private static final String SALESRETURN_PKID = "id";
    private static final String SALESRETURN_SCHLINEID = "slineid";
    private static final String SALESRETURN_PERSONID = "personid";
    private static final String SALESRETURN_CUSTID = "custid";
    private static final String SALESRETURN_CREATETIME = "creattime";

    private static final String TABLE_SALERTNLINE = "tablesalesrtnline";
    private static final String SALERTNLINE_PKID = "id";
    private static final String SALERTNLINE_HEADID = "headid";
    private static final String SALERTNLINE_PRODID = "prodid";
    private static final String SALERTNLINE_QTY = "qty";
    private static final String SALERTNLINE_MRP = "mrp";
    private static final String SALERTNLINE_INVOICE = "invoiceno";
    private static final String SALERTNLINE_CREATETIME = "creattime";
    private static final String SALERTNLINE_DELETEFLAG = "deleteflag";

    private static final String TABLE_CONSTANTS = "tableconstants";
    private static final String CONSTANTS_PKID = "id";
    private static final String CONSTANTS_KEY = "constkeys";
    private static final String CONSTANTS_VALUE = "constvalues";

    private static final String TABLE_CREDITNOTES = "tablecreditnote";
    private static final String CREDITNOTE_PKID = "id";
    private static final String CREDITNOTE_SCHDLELINEID = "schdlineid";
    private static final String CREDITNOTE_AMOUNT = "amount";
    private static final String CREDITNOTE_REASON = "reason";
    private static final String CREDITNOTE_DESCRIPTION = "description";
    private static final String CREDITNOTE_CREATEDTIME = "createdtime";
    private static final String CREDITNOTE_DELETEFLAG = "deleteflag";

    private static final String TABLE_CREDITREASONS = "tablecreditnotereasons";
    private static final String CREDITREASONS_PKID = "id";
    private static final String CREDITREASONS_ID = "uniqueid";
    private static final String CREDITREASONS_REASON = "reason";

    private static final String TABLE_CATEGORYPHOTOS = "tablecatphotos";
    private static final String CATEGORYPHOTOS_PKID = "id";
    private static final String CATEGORYPHOTOS_CUSTID = "custid";
    private static final String CATEGORYPHOTOS_CATEGORY = "category";
    private static final String CATEGORYPHOTOS_UUID = "uuid";
    private static final String CATEGORYPHOTOS_CREATETIME = "createtime";
    private static final String CATEGORYPHOTOS_UPDATEFLAG = "upflag";

    private static final String TABLE_DOCUMENTPHOTOS = "tabledocphotos";
    private static final String DOCUMENTPHOTOS_PKID = "id";
    private static final String DOCUMENTPHOTOS_CUSTID = "custid";
    private static final String DOCUMENTPHOTOS_DOCUMENT = "document";
    private static final String DOCUMENTPHOTOS_UUID = "uuid";
    private static final String DOCUMENTPHOTOS_CREATETIME = "createtime";
    private static final String DOCUMENTPHOTOS_UPDATEFLAG = "upflag";

    private static final String TABLE_CRATES = "tablecrates";
    private static final String CRATES_PKID = "id";
    private static final String CRATES_SLINEID = "slineid";
    private static final String CRATES_IN = "inval";
    private static final String CRATES_OUT = "outval";
    private static final String CRATES_CRTIME = "crtime";
    private static final String CREATES_UUID = "uuid";
    private static final String CRATES_CRTIMESYNC = "synctime";

    private static final String TABLE_OTHERCOMPANY = "tableothercomp";
    private static final String OTHERCOMPANY_PKID = "id";
    private static final String OTHERCOMPANY_ID = "otherid";
    private static final String OTHERCOMPANY_NAME = "name";

    private static final String TABLE_BRANCHES = "tablebranches";
    private static final String BRANCHES_PKID = "id";
    private static final String BRANCHES_ID = "orgid";
    private static final String BRANCHES_NAME = "orgname";

    private static final String TABLE_UNITS = "tableunits";
    private static final String UNITS_PKID = "id";
    private static final String UNITS_ID = "unitid";
    private static final String UNITS_ABBRV = "abbrv";
    private static final String UNITS_NAME = "name";

	/*	private static final String TABLE_UNITSMAP = "tableunitmap";
    private static final String UNITSMAP_PKID = "id";
	private static final String UNITSMAP_PRODUCT = "product";
	private static final String UNITSMAP_UOM = "uom";
	private static final String UNITSMAP_UPC = "upc";*/

    private static final String TABLE_EXCUST = "tableexistingc";
    private static final String EXCUST_PKID = "id";
    private static final String EXCUST_ID = "custid";
    private static final String EXCUST_CODE = "code";
    private static final String EXCUST_NAME = "name";
    private static final String EXCUST_ADDRESS = "address";

    private static final String TABLE_MINQTY = "tableminqty";
    private static final String MINQTY_PKID = "id";
    private static final String MINQTY_CUSTCAT = "custcat";
    private static final String MINQTY_PRODUCT = "product";
    private static final String MINQTY_QUANTITY = "minqty";

    private static final String TABLE_CUSTOMERTYPE = "tablecusttypes";
    private static final String CUSTOMERTYPE_PKID = "id";
    private static final String CUSTOMERTYPE_ID = "realid";
    private static final String CUSTOMERTYPE_NAME = "name";

    private static final String TABLE_REGIONS = "regions";
    private static final String REGIONS_PKID = "id";
    private static final String REGIONS_NAME = "name";

    /*
     * Added by Reny For Van Sales Inventory management. May 22 2018
     */
    private static final String TABLE_PRODUCTBATCH = "productbatch";
    private static final String PRODUCTBATCH_PKID = "id";
    private static final String PRODUCTBATCH_ID = "productbatchid";
    private static final String PRODUCTBATCH_BATCH = "Batch";

    private static final String TABLE_PRODUCTLINE = "productline";
    private static final String PRODUCTLINE_PKID = "id";
    private static final String PRODUCTLINE_ID = "productlineid";
    private static final String PRODUCTLINE_PRODUCT = "Product";
    private static final String PRODUCTLINE_BATCH = "BatchId";

    /*
     * private static final String TABLE_PRODUCTMRP ="mrp"; private static final
     * String PRODUCTMRP_PKID = "id"; private static final String PRODUCTMRP_ID
     * = "mrpid"; private static final String PRODUCTMRP_PRODUCT = "Product" ;
     * private static final String PRODUCTMRP_BATCH = "ProductBatch"; private
     * static final String PRODUCTMRP_MRP = "MRP"; private static final String
     * PRODUCTMRP_KEY = "ProductBatchKey"; private static final String
     * PRODUCTMRP_UOM = "UOM"; private static final String PRODUCTMRP_UPC =
     * "UPC"; private static final String PRODUCTMRP_TAX = "TAX";
     */

    private static final String TABLE_INVENTORYACCOUNT = "inventoryaccount";
    private static final String INVENTORYACCOUNT_PKID = "id";
    private static final String INVENTORYACCOUNT_ID = "inventoryaccountid";
    private static final String INVENTORYACCOUNT_NAME = "AccountName";

    private static final String TABLE_INVENTORYJRNLHDR = "inventoryjournalhdr";
    private static final String INVENTORYJRNLHDR_PKID = "id";
    private static final String INVENTORYJRNLHDR_ID = "inventoryjournalid";
    private static final String INVENTORYJRNLHDR_NO = "JournalNo";
    private static final String INVENTORYJRNLHDR_DATE = "JournalDate";
    private static final String INVENTORYJRNLHDR_PRODUCT = "Product";
    private static final String INVENTORYJRNLHDR_ORG = "Org";
    private static final String INVENTORYJRNLHDR_SOURCE = "Source";
    private static final String INVENTORYJRNLHDR_SOURCEID = "SourceID";
    private static final String INVENTORYJRNLHDR_BATCH = "BatchNo";
    private static final String INVENTORYJRNLHDR_UOM = "UOM";

    private static final String TABLE_INVENTORYJRNLLINE = "inventoryjournalline";
    private static final String INVENTORYJRNLLINE_PKID = "id";
    private static final String INVENTORYJRNLLINE_ID = "inventoryjournallineid";
    private static final String INVENTORYJRNLLINE_HDR = "JournalHdr";
    private static final String INVENTORYJRNLLINE_AMNTCREDIT = "AmountCredited";
    private static final String INVENTORYJRNLLINE_AMNTDEBIT = "AmountDebited";
    private static final String INVENTORYJRNLLINE_ACCOUNT = "InventoryAccount";
    private static final String INVENTORYJRNLLINE_QTYCREDIT = "QuantityCredited";
    private static final String INVENTORYJRNLLINE_QTYDEBIT = "QuantityDebited";

    private static final String TABLE_INVOICENOGEN = "invoicenogen";
    private static final String INVOICENOGEN_PKID = "id";
    private static final String INVOICENOGEN_ACTIVE = "Active";
    private static final String INVOICENOGEN_CURRVAL = "CurrVal";
    private static final String INVOICENOGEN_MAXVAL = "MaxVal";
    private static final String INVOICENOGEN_MINVAL = "MinVal";
    // private static final String INVOICENOGEN_ORG = "Organization";
    private static final String INVOICENOGEN_ORGCODE = "OrgCode";

    private static final String TABLE_TAXLINE = "taxline";
    private static final String TAXLINE_PKID = "id";
    private static final String TAXLINE_ID = "taxlineid";
    private static final String TAXLINE_DESCRIP = "description";
    private static final String TAXLINE_RATE = "rate";
    private static final String TAXLINE_TYPE = "taxtype";
    private static final String TAXLINE_NAME = "taxlinename";
    private static final String TAXLINE_TAX = "taxid";
    // private static final String TAXLINE_MINAMNT = "MinimumAmount";
    // private static final String TAXLINE_OUTPUTACC = "OutputAccount";
    // private static final String TAXLINE_INPUTACC = "InputAccount";
    // private static final String TAXLINE_TAXCODE = "TaxCode";
    // private static final String TAXLINE_VALIDFROM = "ValidFrom";
    // private static final String TAXLINE_VALIDTO = "ValidTo";

    private static final String TABLE_TAX = "tax";
    private static final String TAX_PKID = "id";
    private static final String TAX_ID = "taxid";
    private static final String TAX_NAME = "taxname";
    // private static final String TAX_PRDCTCAT = "ProductCategory";

    private static final String TABLE_ORDERTAXLINE = "ordertaxline";
    private static final String ORDERTAXLINE_PKID = "id";
    private static final String ORDERTAXLINE_TAXLINEID = "taxlineid";
    private static final String ORDERTAXLINE_AMOUNT = "taxableamount";
    private static final String ORDERTAXLINE_TAXAMOUNT = "taxamount";
    private static final String ORDERTAXLINE_CODE = "taxcode";
    private static final String ORDERTAXLINE_RATE = "rate";
    private static final String ORDERTAXLINE_ORDRDETAILID = "orderdetailid";
    private static final String ORDERTAXLINE_ORDRDID = "orderid";
    private static final String ORDERTAXLINE_DELETEFLAG = "deleteflag";

    /**
     * FOR DISCOUNT IMPLEMENTATION
     */
    private static final String TABLE_OFFERS = "offers";
    private static final String OFFERS_PKID = "id";
    private static final String OFFERS_ID = "offerId";
    private static final String OFFERS_FREEPRODUCT = "FreeProduct";
    private static final String OFFERS_MINQTY = "MinQty";
    private static final String OFFERS_FREEITEMSNO = "NoOfFreeItems";
    private static final String OFFERS_OFFTYPE = "OfferType";
    private static final String OFFERS_OFFVALUE = "OffValue";
    //	private static final String OFFERS_PRODUCT = "ProductName";
    //	private static final String OFFERS_PRODUCTCATEGORY = "ProductCategory";
    private static final String OFFERS_RATETYPE = "RateType";
    private static final String OFFERS_UOM = "UOM";
    private static final String OFFERS_INDIVIDUAL_FLAG = "individualFlag";
    private static final String OFFERS_CUST_CATEGORY = "CustomerCategory";

    private static final String TABLE_OFFERS_DETAILS = "offersdtls";
    private static final String OFFERS_DETAILS_PKID = "id";
    private static final String OFFERS_DETAILS_ID = "offerDtlsId";
    private static final String OFFERS_DETAILS_OFFER = "offer";
    private static final String OFFERS_DETAILS_PRODUCT = "ProductName";
    private static final String OFFERS_DETAILS_PRODUCTCATEGORY = "ProductCategory";
    /**
     * EXPENSE
     */
    private static final String TABLE_EXPENSETYPE = "expensetype";
    private static final String EXPENSETYPE_PKID = "id";
    private static final String EXPENSETYPE_ID = "expensetypeid";
    private static final String EXPENSETYPE_NAME = "Name";

    private static final String TABLE_EXPENSE = "expenses";
    private static final String EXPENSE_PKID = "id";
    private static final String EXPENSE_AMOUNT = "Amount";
    private static final String EXPENSE_DESCRIPTION = "Description";
    private static final String EXPENSE_EXPENSETYPE = "ExpenseType";
    private static final String EXPENSE_CREATETIME = "createdOn";

    /**
     * IMPLEMENT PACKAGE TYPE
     */

    private static final String TABLE_PACKAGE_TYPE = "packagetype";
    private static final String PACKAGE_TYPE_PKID = "id";
    private static final String PACKAGE_TYPE_ID = "packagetypeId";
    private static final String PACKAGE_TYPE_CODE = "code";
    private static final String PACKAGE_TYPE_NAME = "name";

    private static final String TABLE_PRODUCT_PACKAGE_MAPPING = "productpackagemapping";
    private static final String PRODUCT_PACKAGE_MAP_PKID = "id";
    private static final String PRODUCT_PACKAGE_MAP_ID = "packagemapId";
    private static final String PRODUCT_PACKAGE_MAP_PACKAGE_TYPE = "packagetype";
    private static final String PRODUCT_PACKAGE_MAP_PRODUCT = "product";
    private static final String PRODUCT_PACKAGE_MAP_UOM = "UOM";
    private static final String PRODUCT_PACKAGE_MAP_SELLABLE = "sellable";
    private static final String PRODUCT_PACKAGE_MAP_STOCKABLE = "stockable";
    private static final String PRODUCT_PACKAGE_MAP_BASE_UNIT = "baseunit";
    private static final String PRODUCT_PACKAGE_MAP_CONVERSION_FACTOR = "conversionfactor";


    private static final String TABLE_CUSTOMER_PHOTO_VISIBILITY = "custphotovisibility";
    private static final String CUSTOMER_PHOTO_VISIBILITY_PKID = "id";
    private static final String CUSTOMER_PHOTO_VISIBILITY_SCHEDULE_LINE = "schedulelineid";
    private static final String CUSTOMER_PHOTO_VISIBILITY_PHOTO_ID = "photoid";
    private static final String CUSTOMER_PHOTO_VISIBILITY_CREATETIME = "createtime";
    private static final String CUSTOMER_PHOTO_VISIBILITY_LAT = "latitude";
    private static final String CUSTOMER_PHOTO_VISIBILITY_LONG = "longitude";
    private static final String CUSTOMER_PHOTO_VISIBILITY_ACCURACY = "accuracy";
    private static final String CUSTOMER_PHOTO_VISIBILITY_DELETEFLAG = "deleteflag";

    private static final String TABLE_SCHEDULE_LINE_CREDIT = "scheduleLineCredit";
    private static final String SCHEDULE_LINE_CREDIT_PKID = "id";
    private static final String SCHEDULE_LINE_CREDIT_SCHEDULE_LINE = "schedulelineid";
    private static final String SCHEDULE_LINE_CREDIT_LIMIT = "creditLimit";
    private static final String SCHEDULE_LINE_CREDIT_BALANCE = "creditBalance";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //Log.e("On Create DB start","------------------------------------------------------");
        // create categorylist
        String CREATE_TABLE_SCHEDULE_LINE_CREDIT = "CREATE TABLE "
                + TABLE_SCHEDULE_LINE_CREDIT + "(" + SCHEDULE_LINE_CREDIT_PKID
                + " INTEGER PRIMARY KEY,"
                + SCHEDULE_LINE_CREDIT_SCHEDULE_LINE + " TEXT,"
                + SCHEDULE_LINE_CREDIT_LIMIT + " TEXT,"
                + SCHEDULE_LINE_CREDIT_BALANCE + " TEXT)";
        db.execSQL(CREATE_TABLE_SCHEDULE_LINE_CREDIT);

        String CREATE_TABLE_CUSTOMER_PHOTO_VISIBILITY = "CREATE TABLE " + TABLE_CUSTOMER_PHOTO_VISIBILITY
                + "(" + CUSTOMER_PHOTO_VISIBILITY_PKID + " INTEGER PRIMARY KEY,"
                + CUSTOMER_PHOTO_VISIBILITY_SCHEDULE_LINE + " TEXT,"
                + CUSTOMER_PHOTO_VISIBILITY_PHOTO_ID + " TEXT,"
                + CUSTOMER_PHOTO_VISIBILITY_LAT + " TEXT,"
                + CUSTOMER_PHOTO_VISIBILITY_LONG + " TEXT,"
                + CUSTOMER_PHOTO_VISIBILITY_ACCURACY + " TEXT,"
                + CUSTOMER_PHOTO_VISIBILITY_DELETEFLAG+ " TEXT,"
                + CUSTOMER_PHOTO_VISIBILITY_CREATETIME + " TEXT )";
        db.execSQL(CREATE_TABLE_CUSTOMER_PHOTO_VISIBILITY);


        String CREATE_TABLE_PACKAGE_TYPE = "CREATE TABLE " + TABLE_PACKAGE_TYPE
                + "(" + PACKAGE_TYPE_PKID + " INTEGER PRIMARY KEY,"
                + PACKAGE_TYPE_ID + " TEXT,"
                + PACKAGE_TYPE_NAME + " TEXT,"
                + PACKAGE_TYPE_CODE + " TEXT )";
        db.execSQL(CREATE_TABLE_PACKAGE_TYPE);


        String CREATE_TABLE_PRODUCT_PACKAGE_MAPPING = "CREATE TABLE " + TABLE_PRODUCT_PACKAGE_MAPPING
                + "(" + PRODUCT_PACKAGE_MAP_PKID + " INTEGER PRIMARY KEY,"
                + PRODUCT_PACKAGE_MAP_ID + " TEXT,"
                + PRODUCT_PACKAGE_MAP_PACKAGE_TYPE + " TEXT,"
                + PRODUCT_PACKAGE_MAP_PRODUCT + " TEXT,"
                + PRODUCT_PACKAGE_MAP_SELLABLE + " TEXT,"
                + PRODUCT_PACKAGE_MAP_STOCKABLE + " TEXT,"
                + PRODUCT_PACKAGE_MAP_BASE_UNIT + " TEXT,"
                + PRODUCT_PACKAGE_MAP_CONVERSION_FACTOR + " TEXT,"
                + PRODUCT_PACKAGE_MAP_UOM + " TEXT )";
        db.execSQL(CREATE_TABLE_PRODUCT_PACKAGE_MAPPING);

        String CREATE_TABLE_EXPENSETYPE = "CREATE TABLE " + TABLE_EXPENSETYPE
                + "(" + EXPENSETYPE_PKID + " INTEGER PRIMARY KEY,"
                + EXPENSETYPE_ID + " TEXT,"
                + EXPENSETYPE_NAME + " TEXT )";
        db.execSQL(CREATE_TABLE_EXPENSETYPE);

        String CREATE_TABLE_EXPENSE = "CREATE TABLE " + TABLE_EXPENSE
                + "(" + EXPENSE_PKID + " INTEGER PRIMARY KEY,"
                + EXPENSE_AMOUNT + " TEXT,"
                + EXPENSE_EXPENSETYPE + " TEXT,"
                + EXPENSE_CREATETIME + " TEXT,"
                + EXPENSE_DESCRIPTION + " TEXT )";
        db.execSQL(CREATE_TABLE_EXPENSE);

        String CREATE_TABLE_OFFERS = "CREATE TABLE " + TABLE_OFFERS
                + "(" + OFFERS_PKID + " INTEGER PRIMARY KEY,"
                + OFFERS_ID + " TEXT,"
                + OFFERS_FREEPRODUCT + " TEXT,"
                + OFFERS_MINQTY + " TEXT,"
                + OFFERS_FREEITEMSNO + " TEXT,"
                + OFFERS_OFFTYPE + " TEXT,"
                + OFFERS_OFFVALUE + " TEXT,"
                //				+ OFFERS_PRODUCT + " TEXT,"
                + OFFERS_RATETYPE + " TEXT,"
                + OFFERS_UOM + " TEXT ,"
                + OFFERS_CUST_CATEGORY + " TEXT ,"
                + OFFERS_INDIVIDUAL_FLAG + " TEXT )";
        db.execSQL(CREATE_TABLE_OFFERS);

        String CREATE_TABLE_OFFERS_DETAILS = "CREATE TABLE " + TABLE_OFFERS_DETAILS
                + "(" + OFFERS_DETAILS_PKID + " INTEGER PRIMARY KEY,"
                + OFFERS_DETAILS_ID + " TEXT,"
                + OFFERS_DETAILS_OFFER + " TEXT,"
                + OFFERS_DETAILS_PRODUCT + " TEXT,"
                + OFFERS_DETAILS_PRODUCTCATEGORY + " TEXT )";
        db.execSQL(CREATE_TABLE_OFFERS_DETAILS);

        String CREATE_ORDERTAXLINE_TABLE = "CREATE TABLE " + TABLE_ORDERTAXLINE
                + "(" + ORDERTAXLINE_PKID + " INTEGER PRIMARY KEY,"
                + ORDERTAXLINE_TAXLINEID + " TEXT," + ORDERTAXLINE_AMOUNT
                + " TEXT," + ORDERTAXLINE_TAXAMOUNT + " TEXT,"
                + ORDERTAXLINE_CODE + " TEXT," + ORDERTAXLINE_RATE + " TEXT,"
                + ORDERTAXLINE_ORDRDETAILID + " TEXT,"
                + ORDERTAXLINE_DELETEFLAG + " TEXT,"
                + ORDERTAXLINE_ORDRDID + " TEXT )";
        db.execSQL(CREATE_ORDERTAXLINE_TABLE);

        String CREATE_TAX_TABLE = "CREATE TABLE " + TABLE_TAX + "(" + TAX_PKID
                + " INTEGER PRIMARY KEY," + TAX_ID + " TEXT," + TAX_NAME
                + " TEXT )";
        db.execSQL(CREATE_TAX_TABLE);

        String CREATE_TAXLINE_TABLE = "CREATE TABLE " + TABLE_TAXLINE + "("
                + TAXLINE_PKID + " INTEGER PRIMARY KEY," + TAXLINE_ID
                + " TEXT," + TAXLINE_DESCRIP + " TEXT," + TAXLINE_RATE
                + " TEXT," + TAXLINE_TYPE + " TEXT," + TAXLINE_NAME + " TEXT,"
                + TAXLINE_TAX + " TEXT )";
        db.execSQL(CREATE_TAXLINE_TABLE);

        String CREATE_INVOICENOGEN_TABLE = "CREATE TABLE " + TABLE_INVOICENOGEN
                + "(" + INVOICENOGEN_PKID + " INTEGER PRIMARY KEY,"
                + INVOICENOGEN_ACTIVE + " TEXT," + INVOICENOGEN_CURRVAL
                + " TEXT," + INVOICENOGEN_MAXVAL + " TEXT,"
                + INVOICENOGEN_MINVAL + " TEXT," + INVOICENOGEN_ORGCODE
                + " TEXT)";
        db.execSQL(CREATE_INVOICENOGEN_TABLE);
        // new
        String CREATE_ORDERHRD_TABLE = "CREATE TABLE " + TABLE_ORDERHDR + "("
                + ORDERHDR_PKID + " INTEGER PRIMARY KEY," + ORDERHDR_SCHLINEID
                + " TEXT," + ORDERHDR_PERSONID + " TEXT," + ORDERHDR_CUSTID
                + " TEXT," + ORDERHDR_CREATETIME + " TEXT,"
                + ORDERHDR_APPROVEFLAG + " TEXT," + ORDERHDR_LATITUDE
                + " TEXT," + ORDERHDR_LONGITUDE + " TEXT," + ORDERHDR_CUSTOMER
                + " TEXT," + ORDERHDR_INVOICENO + " TEXT," + ORDERHDR_STATUS + " TEXT,"
                + ORDERHDR_NOTE + " TEXT," + ORDERHDR_PARENT_INVOICENO + " TEXT,"
                +ORDERHDR_PARENT_ORDERID+" TEXT )";
        db.execSQL(CREATE_ORDERHRD_TABLE);

        String CREATE_TABLE_ORDERLINE = "CREATE TABLE " + TABLE_ORDERLINE + "("
                + ORDERLINE_PKID + " INTEGER PRIMARY KEY," + ORDERLINE_HEADID
                + " TEXT," + ORDERLINE_PRODID + " TEXT," + ORDERLINE_QTY
                + " TEXT," + ORDERLINE_MRP + " TEXT," + ORDERLINE_RATE
                + " TEXT," + ORDERLINE_CREATETIME + " TEXT,"
                + ORDERLINE_DELETEFLAG + " TEXT," + ORDERLINE_PACKAGE_TYPE + " TEXT,"
                /*+ ORDERLINE_UPC + " TEXT,"*/ + ORDERLINE_TAX + " TEXT,"
                + ORDERLINE_PM + " TEXT," + ORDERLINE_BATCH + " TEXT,"
                + ORDERLINE_DISCOUNT + " TEXT," + ORDERLINE_CANCELFLAG + " TEXT,"
                + ORDERLINE_COMPLIMENT + " TEXT," + ORDERLINE_DISCOUNTPER + " TEXT)";
        db.execSQL(CREATE_TABLE_ORDERLINE);

        String CREATE_TABLE_INVENTORY = "CREATE TABLE " + TABLE_INVENTORY + "("
                + INVENTORY_PKID + " INTEGER PRIMARY KEY," + INVENTORY_ID
                + " TEXT," + INVENTORY_PRODUCT + " TEXT," + INVENTORY_QTY
                + " TEXT)";
        db.execSQL(CREATE_TABLE_INVENTORY);

        String CREATE_PRODUCT_TABLE = "CREATE TABLE " + TABLE_PRODUCT + "("
                + PRODUCT_PKID + " INTEGER PRIMARY KEY," + PRODUCT_ID
                + " TEXT," + PRODUCT_NAME + " TEXT," + PRODUCT_CODE + " TEXT,"
                + PRODUCT_CAT + " TEXT," + PRODUCT_BRAND + " TEXT,"
                + PRODUCT_FORM + " TEXT," + PRODUCT_HSN + " TEXT)";
        db.execSQL(CREATE_PRODUCT_TABLE);

        String CREATE_TABLE_MRP = "CREATE TABLE IF NOT EXISTS " + TABLE_MRP
                + "(" + MRP_PKID + " INTEGER PRIMARY KEY," + MRP_ID + " TEXT,"
                + MRP_PRODUCT + " TEXT," + MRP_VALUE + " TEXT," + MRP_FROM
                + " TEXT," + MRP_TO + " TEXT," + MRP_BATCH + " TEXT,"
                + MRP_CONSOFF + " TEXT," + MRP_FLATOFF + " TEXT," + MRP_TAX
                + " TEXT)";
        db.execSQL(CREATE_TABLE_MRP);

        String CREATE_TABLE_PARTYMARG = "CREATE TABLE IF NOT EXISTS "
                + TABLE_PARTYMARG + "(" + PARTYMARG_PKID
                + " INTEGER PRIMARY KEY," + PARTYMARG_ID + " TEXT,"
                + PARTYMARG_PRODUCT + " TEXT," + PARTYMARG_CAT + " TEXT,"
                + PARTYMARG_VALUE + " TEXT," + PARTYMARG_FROM + " TEXT,"
                + PARTYMARG_CUSTOMER + " TEXT," + PARTYMARG_TO + " TEXT)";
        db.execSQL(CREATE_TABLE_PARTYMARG);

        String CREATE_TABLE_ADDLDISC = "CREATE TABLE IF NOT EXISTS "
                + TABLE_ADDLDISC + "(" + ADDLDISC_PKID
                + " INTEGER PRIMARY KEY," + ADDLDISC_ID + " TEXT,"
                + ADDLDISC_CAT + " TEXT," + ADDLDISC_VALUE + " TEXT,"
                + ADDLDISC_FROM + " TEXT," + ADDLDISC_TO + " TEXT)";
        db.execSQL(CREATE_TABLE_ADDLDISC);

        String CREATE_TABLE_FOCUSEDPRDS = "CREATE TABLE " + TABLE_FOCUSEDPRDS
                + "(" + FOCUSEDPRDS_PKID + " INTEGER PRIMARY KEY,"
                + FOCUSEDPRDS_ID + " TEXT," + FOCUSEDPRDS_PRODUCT + " TEXT,"
                + FOCUSEDPRDS_FROM + " TEXT," + FOCUSEDPRDS_TO + " TEXT,"
                + FOCUSEDPRDS_CUSTCAT + " TEXT," + FOCUSEDPRDS_FLAG + " TEXT,"
                + FOCUSEDPRDS_MINQTY + " TEXT)";
        db.execSQL(CREATE_TABLE_FOCUSEDPRDS);

        String CREATE_TABLE_PRODUCTCAT = "CREATE TABLE " + TABLE_PRODUCTCAT
                + "(" + PRODUCTCAT_PKID + " INTEGER PRIMARY KEY,"
                + PRODUCTCAT_ID + " TEXT," + PRODUCTCAT_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_PRODUCTCAT);

        String CREATE_TABLE_BRAND = "CREATE TABLE " + TABLE_BRAND + "("
                + BRAND_PKID + " INTEGER PRIMARY KEY," + BRAND_ID + " TEXT,"
                + BRAND_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_BRAND);

        String CREATE_TABLE_FORM = "CREATE TABLE " + TABLE_FORM + "("
                + FORM_PKID + " INTEGER PRIMARY KEY," + FORM_ID + " TEXT,"
                + FORM_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_FORM);

        // END /////////////////////////////////////

        // For saving mobile services
        String CREATE_MOBSERVICES_TABLE = "CREATE TABLE IF NOT EXISTS "
                + TABLE_MOBSERVICES + "(" + MOBILESERVICE_ID
                + " INTEGER PRIMARY KEY," + MOBILESERVICE_NAME + " TEXT,"
                + MOBILESERVICE_DISABLE_INVENTORY + " TEXT,"
                + MOBILESERVICE_CODE + " TEXT)";
        db.execSQL(CREATE_MOBSERVICES_TABLE);

        // String CREATE_JSON_TABLE2 = "CREATE TABLE " + TABLE_JSONS2 + "("
        // + JSONS_ID2 + " INTEGER PRIMARY KEY," + JSONS_TITLE2 + " TEXT,"
        // + JSONS_DATA2 + " TEXT, " + JSONS_DATE2 + " TEXT)";
        // db.execSQL(CREATE_JSON_TABLE2);

        // For Saving Reports data like Coverage, productivity, etc.
        String CREATE_JSON_TABLE = "CREATE TABLE " + TABLE_JSONS + "("
                + JSONS_ID + " INTEGER PRIMARY KEY," + JSONS_TITLE + " TEXT,"
                + JSONS_DATA + " TEXT, " + JSONS_DATE + " TEXT)";
        db.execSQL(CREATE_JSON_TABLE);

        // Navigation table creation
        String CREATE_NAVIGATION_TABLE = "CREATE TABLE " + TABLE_NAVIGATIONDATA
                + "(" + KEY_NAVIGATIONID + " INTEGER PRIMARY KEY," + KEY_PLANID
                + " TEXT," + KEY_VEHICLEID + " TEXT," + KEY_DEVICETIME
                + " TEXT," + KEY_LATITUDE + " TEXT," + KEY_LONGITUDE + " TEXT,"
                + KEY_SPEED + " TEXT," + KEY_PROVIDER + " TEXT," + KEY_BEARING
                + " TEXT," + KEY_ACCURACY + " TEXT," + KEY_DISTANCE + " TEXT,"
                + NAVIGATIONDATA_UPFLAG + " TEXT," + KEY_CUSTOMER + " TEXT,"
                + KEY_TIMEDIFF + " TEXT," + KEY_MONGODT + " TEXT,"
                + KEY_INACTIVE + " TEXT)";
        db.execSQL(CREATE_NAVIGATION_TABLE);

        // schedule header table creation
        String CREATE_SCHEDULEHEADER_TABLE = "CREATE TABLE "
                + TABLE_SCHEDULE_HEADER + "(" + SCHEDULE_HEADER_PKID
                + " INTEGER PRIMARY KEY," + SCHEDULE_ID + " TEXT,"
                + SCHEDULE_NAME + " TEXT," + SALESPERSON_ID + " TEXT,"
                + SALESPERSON_NAME + " TEXT," + SALEROUTE_ID + " TEXT,"
                + SCHEDULE_DATE + " TEXT," + HEADER_STATUS + " TEXT,"
                + ROUTENETWORK_ID + " TEXT," + SCHEDULE_COMPLETION_STATUS
                + " TEXT," + SCHEDULE_BEGIN_TIME + " TEXT,"
                + SCHEDULE_COMPLETION_TIME + " TEXT)";
        db.execSQL(CREATE_SCHEDULEHEADER_TABLE);

        // schedule detail table creation
        String CREATE_SCHEDULEDETAIL_TABLE = "CREATE TABLE "
                + TABLE_SCHEDULE_DETAIL + "(" + SCHEDULE_DETAIL_PKID
                + " INTEGER PRIMARY KEY," + SCHEDULE_DETAIL_ID + " TEXT,"
                + SCHEDULE_HEADER_ID + " TEXT," + LOCATION_ID + " TEXT,"
                + LOCATION_NAME + " TEXT," + LOCATION_ADRESS + " TEXT," + SEQNO
                + " TEXT," + DETAIL_STATUS + " TEXT," + LATITUDE + " TEXT,"
                + LONGITUDE + " TEXT," + SCHEDULEDETAIL_CREATETIME + " TEXT,"
                + LATITUDE_NEW + " TEXT," + LONGITUDE_NEW + " TEXT,"
                + CUSTOMERCATOGORY + " TEXT," + SCHEDULE_DETAIL_VISITED
                + " TEXT," + RETAILPRICE + " TEXT," + INVOCENUMBER + " TEXT,"
                + PICKLISTNUMBER + " TEXT," + STOREVISIT_REASON + " TEXT,"
                + STOREVISIT_LATTITUDE + " TEXT," + STOREVISIT_LONGITUDE
                + " TEXT," + STOREVISIT_TIME + " TEXT," + SCHED_CUSTOMERCODE
                + " TEXT," + SCHED_LOCUPDATETIME + " TEXT," + LOCATIONLOCK
                + " TEXT," + CLOSING_DAY + " TEXT," + CLOSING_DAY_UPDATETIME
                + " TEXT," + SCHEDULE_DETAIL_PRODUCTIVE + " TEXT,"
                + CUSTOMERCATOGORYID + " TEXT," + SCHEDULEDETAIL_MOBILE
                + " TEXT," + SCHEDULEDETAIL_OTP + " TEXT,"
                + SCHEDULEDETAIL_TINNUMBER + " TEXT," + SCHEDULEDETAIL_PAN
                + " TEXT," + SCHEDULEDETAIL_EDITTIME + " TEXT,"
                + SCHEDULEDETAIL_PHOTOUUID + " TEXT," + SCHEDULEDETAIL_IMGLAT
                + " TEXT," + SCHEDULEDETAIL_IMGLONG + " TEXT,"
                + SCHEDULEDETAIL_LANDMARK + " TEXT," + SCHEDULEDETAIL_TOWN
                + " TEXT," + SCHEDULEDETAIL_CONTACTPERSON + " TEXT,"
                + SCHEDULEDETAIL_OTHERCOMPANY + " TEXT,"
                + SCHEDULEDETAIL_CRATES + " TEXT," + SCHEDULEDETAIL_OTPVERIFIED
                + " TEXT," + SCHEDULEDETAIL_LOCPROVIDER + " TEXT,"
                + SCHEDULEDETAIL_LOCACCURACY + " TEXT," + SCHEDULEDETAIL_EMAIL
                + " TEXT," + SCHEDULEDETAIL_DIRECTCOV + " TEXT,"
                + SCHEDULEDETAIL_SELPRODCAT + " TEXT,"
                + SCHEDULEDETAIL_STORECLOSED + " TEXT,"
                + SCHEDULEDETAIL_PNGCOVERED + " TEXT," + SCHEDULEDETAIL_BRANCH
                + " TEXT," + SCHEDULEDETAIL_PASSBY + " TEXT,"
                + SCHEDULEDETAIL_PASSBYCRTIME + " TEXT,"
                + SCHEDULEDETAIL_ALTERCUSTCODE + " TEXT,"
                + SCHEDULEDETAIL_LATTITUDE + " TEXT,"
                + SCHEDULEDETAIL_TARGET_TYPE + " TEXT,"
                + SCHEDULEDETAIL_TARGET_VALUE + " TEXT,"
//                + SCHEDULEDETAIL_CREDIT_LIMIT + " TEXT,"
//                + SCHEDULEDETAIL_CREDIT_BALANCE + " TEXT,"
                + SCHEDULEDETAIL_GSTTYPE + " TEXT,"
                + SCHEDULEDETAIL_FSSI + " TEXT,"
                + SCHEDULEDETAIL_STATE_CODE+ " TEXT,"
                + SCHEDULEDETAIL_LONGITUDE + " TEXT," + SCHEDULEDETAIL_CUSTTYPE
                + " TEXT)";
        db.execSQL(CREATE_SCHEDULEDETAIL_TABLE);

        // ORDER HEADER table creation
        String CREATE_ORDERHEADER_TABLE = "CREATE TABLE " + TABLE_ORDER_HEADER
                + "(" + ORDER_HEADER_PKID + " INTEGER PRIMARY KEY,"
                + ORDERBEGIN_TIME + " TEXT," + ORDERSUBMIT_TIME + " TEXT,"
                + ORDER_SCHEDULE_DETAIL_ID + " TEXT," + ORDER_STATUS + " TEXT,"
                + ORDER_LATITUDE + " TEXT," + ORDER_LONGITUDE + " TEXT,"
                + ORDER_SALESPERSONID + " TEXT," + ORDER_ROUTENETWORKID
                + " TEXT," + ORDER_LOCATIONID + " TEXT)";

        db.execSQL(CREATE_ORDERHEADER_TABLE);

        // create categorylist
        String CREATE_CATOGORYTABLE = "CREATE TABLE "
                + TABLE_CUSTOMERCATEGORIES + "(" + CUSTOMERCATEGORIES_PKID
                + " INTEGER PRIMARY KEY," + CUSTOMERCATEGORIES_CODE + " TEXT,"
                + CUSTOMERCATEGORIES_NAME + " TEXT," + CUSTOMERCATEGORIES_TYPE
                + " TEXT)";
        db.execSQL(CREATE_CATOGORYTABLE);

        // ORDER HEADER DETAIL table creation
        String CREATE_TABLE_ORDER_DETAIL = "CREATE TABLE " + TABLE_ORDER_DETAIL
                + "(" + TABLE_ORDERDETAIL_PKID + " INTEGER PRIMARY KEY,"
                + ORDER_HEAD_ID + " TEXT," + PRODUCTID + " TEXT," + QUANTITY
                + " TEXT," + DELIVERYDATE + " TEXT," + CREATETIME + " TEXT,"
                + DELETEFLAG + " TEXT," + RATE + " TEXT," + PRODUCT_TAX
                + " TEXT," + TOTTAL_AMOUNT + " TEXT)";

        db.execSQL(CREATE_TABLE_ORDER_DETAIL);

        // PRODUCT table creation
        // String CREATE_TABLE_PRODUCT = "CREATE TABLE " + PRODUCT_TABLE + "("
        // + PRODUCT_PKID + " INTEGER PRIMARY KEY," + PRODUCT_ID
        // + " TEXT," + PRODUCT_NAME + " TEXT," + PRODUCT_CODE + " TEXT,"
        // + PRODUCT_DISCRIP + " TEXT," + WEIGHT + " TEXT," + VOLUME
        // + " TEXT," + TAX + " TEXT," + UNITRATE + " TEXT)";
        //
        // db.execSQL(CREATE_TABLE_PRODUCT);

        // sync table
        String CREATE_SYNCTABLE = "CREATE TABLE " + TABLE_SYNCTABLE + "("
                + INDEXPKID + " INTEGER PRIMARY KEY," + TABLEINSYNC + " TEXT,"
                + SYNCTIME + " TEXT)";

        db.execSQL(CREATE_SYNCTABLE);

        // location table creation

        // String CREATE_LOCATIONTABLE = "CREATE TABLE " + TABLE_LOCATIONS + "("
        // + LOCATION_PKID + " INTEGER PRIMARY KEY," + LOCATIONID
        // + " TEXT," + NAME + " TEXT," + ADDRESS + " TEXT,"
        // + LOCATION_LATITUDE + " TEXT," + LOCATION_LONGITUDE + " TEXT,"
        // + LOCATION_CREATETIME + " TEXT," + CUSTOMER_CODE + " TEXT,"
        // + CUSTOMER_CATOGORY + " TEXT," + TIN_NUMBER + " TEXT)";
        String CREATE_LOCATIONTABLE = "CREATE TABLE " + TABLE_LOCATIONS + "("
                + LOCATION_PKID + " INTEGER PRIMARY KEY," + LOCATIONID
                + " TEXT," + NAME + " TEXT," + ADDRESS + " TEXT,"
                + LOCATION_LATITUDE + " TEXT," + LOCATION_LONGITUDE + " TEXT,"
                + LOCATION_CREATETIME + " TEXT," + CUSTOMER_CODE + " TEXT,"
                + CUSTOMER_CATOGORY + " TEXT," + TIN_NUMBER + " TEXT,"
                + NEAREST_LANDMARK + " TEXT," + CONTACT_PERSONNAME + " TEXT,"
                + CONTACT_PERSON_NUMBER + " TEXT," + ANT_OTHERCOMPANYVALUE
                + " TEXT," + TOWNCLASS + " TEXT," + SELECTED_PRODUCT_CATEGORIES
                + " TEXT," + CUSTOMERCATEGOY_CODE + " TEXT,"
                + CUSTOMER_SCHEDULELINE_GUID + " TEXT," + LOCATION_PAN
                + " TEXT," + LOCATION_OTPVERIFIED + " TEXT,"
                + LOCATION_LOCPROVIDER + " TEXT," + LOCATION_LOCACCURACY
                + " TEXT," + LOCATION_EMAIL + " TEXT," + LOCATION_PHOTOUUID
                + " TEXT," + LOCATION_DIRECTCOV + " TEXT,"
                + LOCATION_CLOSING_DAY + " TEXT," + LOCATION_STOREISCLOSED
                + " TEXT," + LOCATION_PNGCOVERED + " TEXT," + LOCATION_BRANCH
                + " TEXT," + LOCATION_DRUG + " TEXT," + LOCATION_LOCALITY
                + " TEXT," + LOCATION_CITY + " TEXT," + LOCATION_STATE
                + " TEXT," + LOCATION_PIN + " TEXT," + LOCATION_COVDAY
                + " TEXT," + LOCATION_WEEK1 + " TEXT," + LOCATION_WEEK2
                + " TEXT," + LOCATION_WEEK3 + " TEXT," + LOCATION_WEEK4
                + " TEXT," + LOCATION_VISTFRQ + " TEXT," + LOCATION_TYPE
                + " TEXT," + LOCATION_WHOLSAL + " TEXT," + LOCATION_METRO
                + " TEXT," + LOCATION_CLASSIF + " TEXT,"
                + LOCATION_GSTTYPE + " TEXT,"
                + LOCATION_FSSI + " TEXT,"
                + LOCATION_LATITUDEPHOTO + " TEXT," + LOCATION_LONGITUDEPHOTO
                + " TEXT," + LOCATION_REMARKS + " TEXT)";

        db.execSQL(CREATE_LOCATIONTABLE);

        // /previoussorders table ceation

        String CREATE_PREVIOUSORDERTABLE = "CREATE TABLE "
                + TABLE_PREVIOUSORDERS + "(" + PREVIOUSORDERS_PKID
                + " INTEGER PRIMARY KEY," + PREVIOUSORDERS_LOCATIONID
                + " TEXT," + PREVIOUSORDERS_SCHEDULEDETAILID + " TEXT,"
                + ADDRESS + " TEXT," + DATA + " TEXT)";

        db.execSQL(CREATE_PREVIOUSORDERTABLE);

        // create towntable
        String CREATE_TOWNTABLE = "CREATE TABLE " + TABLE_TOWN + "("
                + TOWN_PKID + " INTEGER PRIMARY KEY," + TOWN_CODE + " TEXT,"
                + TOWN_NAME + " TEXT," + TOWN_ORG + " TEXT)";
        db.execSQL(CREATE_TOWNTABLE);

        // create checklist table
        String CREATE_CHECKLISTTABLE = "CREATE TABLE " + TABLE_PRODUCTCHECKLIST
                + "(" + PRODUCTCHECKLIST_PKID + " INTEGER PRIMARY KEY,"
                + PRODUCTCHECKLIST_CODE + " TEXT," + PRODUCTCHECKLIST_NAME
                + " TEXT," + PRODUCTCHECKLIST_SEQNO + " TEXT)";
        db.execSQL(CREATE_CHECKLISTTABLE);

        // createpayment table

        String CREATE_PAYMENTTABLE = "CREATE TABLE " + TABLE_PAYMENTS + "("
                + PAYMENT_PKID + " INTEGER PRIMARY KEY,"
                + PAYMENT_SCHEDULEDETAILID + " TEXT," + PAYMENT_METHOD
                + " TEXT," + AMOUNT + " TEXT," + CHEQUENUMBER + " TEXT," + IFSC
                + " TEXT," + PAYMENT_CREATETIME + " TEXT," + DELETE_FLAG
                + " TEXT," + PAYMENTGUID + " TEXT," + SALESPERSONID + " TEXT,"
                + PAYMENT_LOCATIONID + " TEXT," + CHEQE_DATE + " TEXT,"
                + PAYMENT_DATE + " TEXT," + PAYMENT_INVOCENUMBER + " TEXT,"
                + PAYMENT_PICKLISTNUMBER + " TEXT," + PAYMENT_LATTITUDE
                + " TEXT," + PAYMENT_LONGITUDE + " TEXT," + PAYMENT_ACTUALMODE
                + " TEXT," + PAYMENT_BANKCODE + " TEXT," + PAYMENT_APROOVEFLAG
                + " TEXT," + PAYMENT_SIGNING_PERSON + " TEXT, " + PAYMENT_STATUS + " TEXT, "
                + PAYMENT_FILEID + " TEXT," + PAYMENT_VERIFICATIONCODE
                + " TEXT)";

        db.execSQL(CREATE_PAYMENTTABLE);

        // createprice table
        String CREATE_PRICETABLE = "CREATE TABLE " + TABLE_PRICEDETAILS + "("
                + PRICEDETAILS_PKID + " INTEGER PRIMARY KEY,"
                + PRICEDETAILS_PRODUCTID + " TEXT,"
                + PRICEDETAILS_CUSTOMERCATEGORY + " TEXT," + PRICE + " TEXT,"
                + VALIDFROM + " TEXT," + VALIDTO + " TEXT," + MRP + " TEXT)";
        db.execSQL(CREATE_PRICETABLE);

        // create invoice table
        String CREATE_INVOICETABLE = "CREATE TABLE " + TABLE_INVOICE + "("
                + INVOICE_PKID + " INTEGER PRIMARY KEY," + INVOICE_ID
                + " TEXT," + INVOICE_NO + " TEXT," + INVOICE_CUSTOMERID
                + " TEXT," + INVOICE_TOTTALAMOUNT + " TEXT,"
                + INVOICE_BALANCEAMOUNT + " TEXT," + INVOICE_STATUS + " TEXT,"
                + INVOICE_DATE + " TEXT," + INVOICE_STATICBALANCE + " TEXT,"
                + INVOICE_ORDERUUID + " TEXT )";
        db.execSQL(CREATE_INVOICETABLE);

        // create invoice paymenttable

        String CREATE_INVOICEPAYMENT_TABLE = "CREATE TABLE "
                + TABLE_INVOICE_PAYMENTS + "(" + INVOICEPAYMENT_PKID
                + " INTEGER PRIMARY KEY," + INVOICEPAYMENT_HEADERID + " TEXT,"
                + INVOICEPAYMENT_ID + " TEXT," + INVOICEPAYMENT_AMOUNTPAYED
                + " TEXT," + INVOICEPAYMENT_INVOICENO + " TEXT,"
                + INVOICEPAYMENT_DELETEFLAG + " TEXT,"
                + INVOICEPAYMENT_BALANCE + " TEXT)";
        db.execSQL(CREATE_INVOICEPAYMENT_TABLE);

        // create order amount table(capturing amount from store for delivery))

        String CREATE_ORDERAMOUNTTABLE = "CREATE TABLE "
                + TABLE_ORDERAMOUNTDATA + "(" + ORDERAMOUNTDATA_PKID
                + " INTEGER PRIMARY KEY," + ORDERAMOUNTDATA_AMOUNT + " TEXT,"
                + ORDERAMOUNTDATA_SCHEDULEDETAILID + " TEXT,"
                + ORDERAMOUNTDATA_CUSTOMERID + " TEXT,"
                + ORDERAMOUNTDATA_PERSONID + " TEXT," + ORDERAMOUNTDATA_TIME
                + " TEXT," + ORDERAMOUNTDATA_CREATETIME + " TEXT,"
                + ORDERAMOUNTDATA_LATTITUDE + " TEXT,"
                + ORDERAMOUNTDATA_LONGITUDE + " TEXT," + ORDERAMOUNTDATA_GUID
                + " TEXT)";
        db.execSQL(CREATE_ORDERAMOUNTTABLE);

        // create reson table

        String CREATE_REASONTABLE = "CREATE TABLE " + TABLE_REASONS + "("
                + REASONS_PKID + " INTEGER PRIMARY KEY," + REASONS_CODE
                + " TEXT," + REASON_DETAIL + " TEXT)";
        db.execSQL(CREATE_REASONTABLE);

        // customer search table
        String CUSTOMER_SEARCHTABLE = "CREATE TABLE " + TABLE_SEARCHCUSTOMERS
                + "(" + SEARCHCUSTOMERS_PKID + " INTEGER PRIMARY KEY,"
                + SEARCHCUSTOMERS_ID + " TEXT," + SEARCHCUSTOMERS_NAME
                + " TEXT," + SEARCHCUSTOMERS_ADRESS + " TEXT,"
                + SEARCHCUSTOMERS_CODE + " TEXT," + SEARCHCUSTOMERS_LATTITUDE
                + " TEXT," + SEARCHCUSTOMERS_LONGITUDE + " TEXT,"
                + SEARCHCUSTOMERS_CATEGORY + " TEXT,"
                + SEARCHCUSTOMERS_LOCATIONLOCK + " TEXT,"
                + SEARCHCUSTOMERS_CLOSINGDAY + " TEXT,"
                + SEARCHCUSTOMERS_MOBILENUMBER + " TEXT,"
                + SEARCHCUSTOMERS_CUSTTYPE + " TEXT)";
        db.execSQL(CUSTOMER_SEARCHTABLE);

        // create table denomination
        String CREATE_DENOMINATIONTABLE = "CREATE TABLE " + TABLE_DENOMINATIONS
                + "(" + DENOMINATIONS_PKID + " INTEGER PRIMARY KEY,"
                + DENOMINATIONS_CODE + " TEXT," + DENOMINATIONS_AMOUNT
                + " TEXT)";
        db.execSQL(CREATE_DENOMINATIONTABLE);

        // create table schedulsummary and cash denomination
        String SCHEDULE_SUMMARY = "CREATE TABLE " + TABLE_SCHEDULESUMMARY + "("
                + SCHEDULESUMMARY_PKID + " INTEGER PRIMARY KEY,"
                + SEARCHCUSTOMERS_ID + " TEXT," + SCHEDULESUMMARY_PERSONID
                + " TEXT," + SCHEDULESUMMARY_CREATETIME + " TEXT,"
                + SCHEDULESUMMARY_SUBMITTIME + " TEXT," + SCHEDULESUMMARY_LAT
                + " TEXT," + SCHEDULESUMMARY_LONGI + " TEXT,"
                + SCHEDULESUMMARY_UNIQUEID + " TEXT,"
                + SCHEDULESUMMARY_DENOMEDATA + " TEXT,"
                + SCHEDULESUMMARY_TOTTALCASH + " TEXT,"
                + SCHEDULESUMMARY_SCHEDULEID + " TEXT,"
                + SCHEDULESUMMARY_SCHEDULEIDLOCAL + " TEXT,"
                + SCHEDULESUMMARY_COMPLETION_TIME + " TEXT,"
                + SCHEDULESUMMARY_BEGIN_TIME + " TEXT)";
        db.execSQL(SCHEDULE_SUMMARY);

        // create bank master table

        String CREATE_BANKMASTERTABLE = "CREATE TABLE " + TABLE_BANKMASTER
                + "(" + BANKMASTER_PKID + " INTEGER PRIMARY KEY,"
                + BANKMASTER_CODE + " TEXT," + BANKMASTER_NAME + " TEXT,"
                + BANKMASTER_BRANCHNAME + " TEXT)";
        db.execSQL(CREATE_BANKMASTERTABLE);

        // Sales Return Header Table
        String CREATE_TABLE_SALESRETURN = "CREATE TABLE " + TABLE_SALESRETURN
                + "(" + SALESRETURN_PKID + " INTEGER PRIMARY KEY,"
                + SALESRETURN_SCHLINEID + " TEXT," + SALESRETURN_PERSONID
                + " TEXT," + SALESRETURN_CUSTID + " TEXT,"
                + SALESRETURN_CREATETIME + " TEXT)";
        db.execSQL(CREATE_TABLE_SALESRETURN);

        // SalesReturn Line Table
        String CREATE_TABLE_SALERTNLINE = "CREATE TABLE " + TABLE_SALERTNLINE
                + "(" + SALERTNLINE_PKID + " INTEGER PRIMARY KEY,"
                + SALERTNLINE_HEADID + " TEXT," + SALERTNLINE_PRODID + " TEXT,"
                + SALERTNLINE_QTY + " TEXT," + SALERTNLINE_MRP + " TEXT,"
                + SALERTNLINE_INVOICE + " TEXT," + SALERTNLINE_CREATETIME
                + " TEXT," + SALERTNLINE_DELETEFLAG + " TEXT)";
        db.execSQL(CREATE_TABLE_SALERTNLINE);

        String CREATE_TABLE_CONSTANTS = "CREATE TABLE " + TABLE_CONSTANTS + "("
                + CONSTANTS_PKID + " INTEGER PRIMARY KEY," + CONSTANTS_KEY
                + " TEXT," + CONSTANTS_VALUE + " TEXT)";
        db.execSQL(CREATE_TABLE_CONSTANTS);

        String CREATE_TABLE_CREDITNOTES = "CREATE TABLE " + TABLE_CREDITNOTES
                + "(" + CREDITNOTE_PKID + " INTEGER PRIMARY KEY,"
                + CREDITNOTE_SCHDLELINEID + " TEXT," + CREDITNOTE_AMOUNT
                + " TEXT," + CREDITNOTE_REASON + " TEXT,"
                + CREDITNOTE_DESCRIPTION + " TEXT," + CREDITNOTE_CREATEDTIME
                + " TEXT," + CREDITNOTE_DELETEFLAG + " TEXT)";
        db.execSQL(CREATE_TABLE_CREDITNOTES);

        String CREATE_TABLE_CREDITREASONS = "CREATE TABLE "
                + TABLE_CREDITREASONS + "(" + CREDITREASONS_PKID
                + " INTEGER PRIMARY KEY," + CREDITREASONS_ID + " TEXT,"
                + CREDITREASONS_REASON + " TEXT)";
        db.execSQL(CREATE_TABLE_CREDITREASONS);

        String CREATE_TABLE_CATEGORYPHOTOS = "CREATE TABLE "
                + TABLE_CATEGORYPHOTOS + "(" + CATEGORYPHOTOS_PKID
                + " INTEGER PRIMARY KEY," + CATEGORYPHOTOS_CUSTID + " TEXT,"
                + CATEGORYPHOTOS_CATEGORY + " TEXT," + CATEGORYPHOTOS_UUID
                + " TEXT," + CATEGORYPHOTOS_CREATETIME + " TEXT,"
                + CATEGORYPHOTOS_UPDATEFLAG + " TEXT)";
        db.execSQL(CREATE_TABLE_CATEGORYPHOTOS);

        String CREATE_TABLE_DOCUMENTPHOTOS = "CREATE TABLE "
                + TABLE_DOCUMENTPHOTOS + "(" + DOCUMENTPHOTOS_PKID
                + " INTEGER PRIMARY KEY," + DOCUMENTPHOTOS_CUSTID + " TEXT,"
                + DOCUMENTPHOTOS_DOCUMENT + " TEXT," + DOCUMENTPHOTOS_UUID
                + " TEXT," + DOCUMENTPHOTOS_CREATETIME + " TEXT,"
                + CATEGORYPHOTOS_UPDATEFLAG + " TEXT)";
        db.execSQL(CREATE_TABLE_DOCUMENTPHOTOS);

        String CREATE_TABLE_CRATES = "CREATE TABLE " + TABLE_CRATES + "("
                + CRATES_PKID + " INTEGER PRIMARY KEY," + CRATES_SLINEID
                + " TEXT," + CRATES_IN + " TEXT," + CRATES_OUT + " TEXT,"
                + CREATES_UUID + " TEXT," + CRATES_CRTIME + " TEXT,"
                + CRATES_CRTIMESYNC + " TEXT)";
        db.execSQL(CREATE_TABLE_CRATES);

        String CREATE_TABLE_OTHERCOMPANY = "CREATE TABLE " + TABLE_OTHERCOMPANY
                + "(" + OTHERCOMPANY_PKID + " INTEGER PRIMARY KEY,"
                + OTHERCOMPANY_ID + " TEXT," + OTHERCOMPANY_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_OTHERCOMPANY);

        String CREATE_TABLE_BRANCHES = "CREATE TABLE " + TABLE_BRANCHES + "("
                + BRANCHES_PKID + " INTEGER PRIMARY KEY," + BRANCHES_ID
                + " TEXT," + BRANCHES_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_BRANCHES);

        String CREATE_TABLE_UNITS = "CREATE TABLE " + TABLE_UNITS + "("
                + UNITS_PKID + " INTEGER PRIMARY KEY," + UNITS_ID + " TEXT,"
                + UNITS_ABBRV + " TEXT," + UNITS_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_UNITS);

		/*	String CREATE_TABLE_UNITSMAP = "CREATE TABLE " + TABLE_UNITSMAP + "("
                + UNITSMAP_PKID + " INTEGER PRIMARY KEY," + UNITSMAP_PRODUCT
				+ " TEXT," + UNITSMAP_UOM + " TEXT," + UNITSMAP_UPC + " TEXT)";
		db.execSQL(CREATE_TABLE_UNITSMAP);*/

        String CREATE_TABLE_EXCUST = "CREATE TABLE IF NOT EXISTS "
                + TABLE_EXCUST + "(" + EXCUST_PKID + " INTEGER PRIMARY KEY,"
                + EXCUST_ID + " TEXT," + EXCUST_CODE + " TEXT," + EXCUST_NAME
                + " TEXT," + EXCUST_ADDRESS + " TEXT)";
        db.execSQL(CREATE_TABLE_EXCUST);

        String CREATE_TABLE_MINQTY = "CREATE TABLE IF NOT EXISTS "
                + TABLE_MINQTY + "(" + MINQTY_PKID + " INTEGER PRIMARY KEY,"
                + MINQTY_CUSTCAT + " TEXT," + MINQTY_PRODUCT + " TEXT,"
                + MINQTY_QUANTITY + " TEXT)";
        db.execSQL(CREATE_TABLE_MINQTY);

        String CREATE_TABLE_CUSTOMERTYPE = "CREATE TABLE IF NOT EXISTS "
                + TABLE_CUSTOMERTYPE + "(" + CUSTOMERTYPE_PKID
                + " INTEGER PRIMARY KEY," + CUSTOMERTYPE_ID + " TEXT,"
                + CUSTOMERTYPE_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_CUSTOMERTYPE);

        String CREATE_TABLE_REGIONS = "CREATE TABLE IF NOT EXISTS "
                + TABLE_REGIONS + "(" + REGIONS_PKID + " INTEGER PRIMARY KEY,"
                + REGIONS_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_REGIONS);

        /*
         * Added by Reny For Van Sales Inventory management. May 22 2018
         */

        String CREATE_TABLE_PRODUCTBATCH = "CREATE TABLE IF NOT EXISTS "
                + TABLE_PRODUCTBATCH + "(" + PRODUCTBATCH_PKID
                + " INTEGER PRIMARY KEY," + PRODUCTBATCH_ID + " TEXT,"
                + PRODUCTBATCH_BATCH + " TEXT)";
        db.execSQL(CREATE_TABLE_PRODUCTBATCH);

        String CREATE_TABLE_PRODUCTLINE = "CREATE TABLE IF NOT EXISTS "
                + TABLE_PRODUCTLINE + " ( " + PRODUCTLINE_PKID
                + " INTEGER PRIMARY KEY,  " + PRODUCTLINE_ID + " TEXT,"
                + PRODUCTLINE_PRODUCT + " TEXT,  " + PRODUCTLINE_BATCH
                + " TEXT)";

        db.execSQL(CREATE_TABLE_PRODUCTLINE);

        /*
         * String CREATE_TABLE_PRODUCTMRP ="CREATE TABLE IF NOT EXISTS "+
         * TABLE_PRODUCTMRP +" ( "+ PRODUCTMRP_PKID +" INTEGER PRIMARY KEY,  "+
         * PRODUCTMRP_ID +" TEXT,"+ PRODUCTMRP_MRP +" TEXT,"+ PRODUCTMRP_PRODUCT
         * +" TEXT,"+ PRODUCTMRP_BATCH +" TEXT,"+ PRODUCTMRP_KEY +" TEXT,"+
         * PRODUCTMRP_UOM +" TEXT,"+ PRODUCTMRP_UPC +" TEXT,"+ PRODUCTMRP_TAX
         * +" TEXT)" ;
         *
         * db.execSQL(CREATE_TABLE_PRODUCTMRP);
         */

        String CREATE_TABLE_INVENTORYACCOUNT = "CREATE TABLE IF NOT EXISTS "
                + TABLE_INVENTORYACCOUNT + " ( " + INVENTORYACCOUNT_PKID
                + " INTEGER PRIMARY KEY,  " + INVENTORYACCOUNT_ID + " TEXT,"
                + INVENTORYACCOUNT_NAME + " TEXT )";

        db.execSQL(CREATE_TABLE_INVENTORYACCOUNT);

        String CREATE_TABLE_INVENTORYJRNLHDR = "CREATE TABLE IF NOT EXISTS "
                + TABLE_INVENTORYJRNLHDR + " ( " + INVENTORYJRNLHDR_PKID
                + " INTEGER PRIMARY KEY,  " + INVENTORYJRNLHDR_ID + " TEXT,"
                + INVENTORYJRNLHDR_NO + " TEXT," + INVENTORYJRNLHDR_DATE
                + " TEXT," + INVENTORYJRNLHDR_PRODUCT + " TEXT,"
                + INVENTORYJRNLHDR_UOM + " TEXT,"
                + INVENTORYJRNLHDR_ORG + " TEXT," + INVENTORYJRNLHDR_SOURCE
                + " TEXT," + INVENTORYJRNLHDR_SOURCEID + " TEXT," + CREATETIME
                + " TEXT," + INVENTORYJRNLHDR_BATCH + " TEXT )";

        db.execSQL(CREATE_TABLE_INVENTORYJRNLHDR);

        String CREATE_TABLE_INVENTORYJRNLLINE = "CREATE TABLE IF NOT EXISTS "
                + TABLE_INVENTORYJRNLLINE + " ( " + INVENTORYJRNLLINE_PKID
                + " INTEGER PRIMARY KEY,  " + INVENTORYJRNLLINE_ID + " TEXT,"
                + INVENTORYJRNLLINE_HDR + " TEXT,"
                + INVENTORYJRNLLINE_AMNTCREDIT + " TEXT,"
                + INVENTORYJRNLLINE_AMNTDEBIT + " TEXT,"
                + INVENTORYJRNLLINE_ACCOUNT + " TEXT,"
                + INVENTORYJRNLLINE_QTYCREDIT + " TEXT," + CREATETIME
                + " TEXT," + INVENTORYJRNLLINE_QTYDEBIT + " TEXT )";

        db.execSQL(CREATE_TABLE_INVENTORYJRNLLINE);


        //Log.e("On Create DB end","------------------------------------------------------");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //Log.e("onUpgrade start","------------------------------------------------------");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERHDR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERLINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTCAT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BRAND);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FOCUSEDPRDS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MRP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARTYMARG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDLDISC);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVENTORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALESRETURN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALERTNLINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONSTANTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREDITNOTES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREDITREASONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAVIGATIONDATA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULE_HEADER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULE_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDER_HEADER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDER_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNCTABLE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PREVIOUSORDERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAYMENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRICEDETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVOICE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVOICE_PAYMENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERAMOUNTDATA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REASONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEARCHCUSTOMERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMERCATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DENOMINATIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULESUMMARY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BANKMASTER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_JSONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTCHECKLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOWN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORYPHOTOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DOCUMENTPHOTOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CRATES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OTHERCOMPANY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BRANCHES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UNITS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MINQTY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMERTYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REGIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTBATCH);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTLINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVENTORYACCOUNT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVENTORYJRNLHDR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVENTORYJRNLLINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVOICENOGEN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAX);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAXLINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERTAXLINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFERS_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSETYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PACKAGE_TYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT_PACKAGE_MAPPING);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER_PHOTO_VISIBILITY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULE_LINE_CREDIT);

        if (Global.force) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_MOBSERVICES);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXCUST);
            Global.force = false;
        }

        //Log.e("onUpgrade end","------------------------------------------------------");
        onCreate(db);


    }

    public void Deletetabledata() {
        //Log.e("Deletetabledata","------------------------------------------------------");
        SQLiteDatabase db = this.getWritableDatabase();
        onUpgrade(db, 1, 1);
    }

    public String getConstantValue(String key) throws Exception {
        String temp = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + CONSTANTS_VALUE + " FROM " + TABLE_CONSTANTS
                + " WHERE " + CONSTANTS_KEY + "='" + key + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            temp = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return temp.contentEquals("") && key.contentEquals("passbyRange") ? "25"
                : temp;
    }

    public boolean isserviceexist(String servcname) {
        Boolean result = false;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String count = "SELECT * FROM " + TABLE_MOBSERVICES + " WHERE "
                    + MOBILESERVICE_NAME + "='" + servcname + "'";
            Cursor cursor = db.rawQuery(count, null);
            if (cursor.moveToFirst()) {
                result = true;
            }
            if (cursor != null)
                cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean excustneeded() {
        boolean temp = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "Select " + EXCUST_ID + " from " + TABLE_EXCUST;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            temp = false;
        }
        return temp;
    }

    public void updatescheduleheaderbegintime(String begintime) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SCHEDULE_BEGIN_TIME, begintime);
        db.update(TABLE_SCHEDULE_HEADER, values, "", new String[]{});

    }

    public JSONArray getscheduledata(String text) throws Exception {

        JSONArray group = new JSONArray();
        String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE_HEADER;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                String schedulepkid = cursor.getString(0);
                obj.put("schedulepkid", schedulepkid);
                obj.put("scheduleid", cursor.getString(1));
                obj.put("schedulename", cursor.getString(2));
                obj.put("salespersonid", cursor.getString(3));
                obj.put("salespersonname", cursor.getString(4));
                obj.put("salesrouteid", cursor.getString(5));
                obj.put("scheduledate", cursor.getString(6));
                obj.put("headerstatus", cursor.getString(7));
                obj.put("routenetworkid", cursor.getString(8));
                obj.put("schedulecopletionstatus", cursor.getString(9));
                obj.put("coustemerlist", getchid(schedulepkid, text));
                group.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return group;
    }

    public JSONArray getchid(String schedulepkid, String text) throws Exception {
        text = text.replaceAll("'", "\'");
        JSONArray childs = new JSONArray();
        String selectQuery = "SELECT sl.*,count(pt." + PAYMENT_PKID
                + ") as Approve,count(oln." + ORDERLINE_PKID
                + ") as ApproveOrder FROM " + TABLE_SCHEDULE_DETAIL
                + " sl left join " + TABLE_PAYMENTS + " pt on pt."
                + PAYMENT_SCHEDULEDETAILID + "=sl." + SCHEDULE_DETAIL_ID
                //+" and pt."+PAYMENT_STATUS+" != 'Cancelled' "
                + " and pt." + PAYMENT_APROOVEFLAG + "=0 and pt." + DELETE_FLAG

                + "=0  left join " + TABLE_ORDERHDR + " ot on ot."
                + ORDERHDR_SCHLINEID + "=sl." + SCHEDULE_DETAIL_ID + " and ( ot."
                + ORDERHDR_APPROVEFLAG + " is null OR " + ORDERHDR_APPROVEFLAG + " = '0' )  left join "
                + TABLE_ORDERLINE + " oln on oln."
                + ORDERLINE_HEADID
                + "=ot."
                + ORDERHDR_PKID
                // NA Changes
                + " and oln." + ORDERLINE_DELETEFLAG + "=0 "

                + " WHERE " + SCHEDULE_HEADER_ID
                + "="
                + schedulepkid

                // + " AND oln." + ORDERLINE_DELETEFLAG + "=0"

                + " AND (" + LOCATION_NAME + " like '%" + text + "%' OR "
                + SCHED_CUSTOMERCODE + " LIKE '%" + text + "%' )"
                + " group by sl." + SCHEDULE_DETAIL_ID + " ORDER BY " + SEQNO
                + "* 1 ASC ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("scheduledetailpkid", cursor.getString(0));
                obj.put("scheduledetailid", cursor.getString(1));
                obj.put("scheduleheaderid", cursor.getString(2));
                obj.put("locationid", cursor.getString(3));
                obj.put("locationname", cursor.getString(4));
                obj.put("locationadress", cursor.getString(5));
                obj.put("seqno", cursor.getString(6));
                obj.put("status", cursor.getString(7));
                obj.put("latitude", cursor.getString(8));
                obj.put("longitude", cursor.getString(9));
                obj.put("Customercategory", cursor.getString(13));
                obj.put("customercode", cursor.getString(cursor
                        .getColumnIndex(SCHED_CUSTOMERCODE)));
                obj.put("locationlock", cursor.getString(24));
                obj.put("Approve",
                        cursor.getString(cursor.getColumnIndex("Approve")));
                obj.put("ApproveOrder",
                        cursor.getString(cursor.getColumnIndex("ApproveOrder")));

                childs.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return childs;
    }

    public String GetCurrentScheduledate() {
        boolean temp = false;
        String schdate = "";

        try {
            String sql = "select " + SCHEDULE_DATE + " from "
                    + TABLE_SCHEDULE_HEADER;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                do {
                    schdate = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return schdate;
    }

    public String getscheduleheaderbegintime(String scheduleid) {
        // TODO Auto-generated method stub
        String Schedulebegintime = "";
        String get_schedulebegin = "SELECT " + SCHEDULE_BEGIN_TIME + " FROM "
                + TABLE_SCHEDULE_HEADER + " WHERE  " + SCHEDULE_ID + "='"
                + scheduleid + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(get_schedulebegin, null);
        if (cursor.moveToFirst()) {

            Schedulebegintime = cursor.getString(0);
        }

        // if (Schedulebegintime == null)
        // Schedulebegintime = "";
        if (cursor != null)
            cursor.close();
        return Schedulebegintime;

    }

    public String getactualscheduleid() {

        String ServerScheduleid = "0";

        String selectQuery = "SELECT " + SCHEDULE_ID + " FROM "
                + TABLE_SCHEDULE_HEADER + " ORDER BY " + SCHEDULE_HEADER_PKID
                + " DESC LIMIT 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                ServerScheduleid = cursor.getString(0);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return ServerScheduleid;

    }



    public void insertservices(JSONArray services) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();

            for (int i = 0; i < services.length(); i++) {
                JSONObject temp = services.optJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(MOBILESERVICE_CODE, temp.optString("_id"));
                values.put(MOBILESERVICE_NAME, temp.optString("srvcname"));
                db.insert(TABLE_MOBSERVICES, null, values);
            }

            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        }

        finally {
            db.endTransaction();
            db.close();
        }


    }

    public void insertintoregions(JSONArray customertypes)  {
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            db.beginTransaction();
            for (int i = 0; i < customertypes.length(); i++) {
                JSONObject categoryobj = customertypes.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(REGIONS_NAME, categoryobj.getString("Polygon"));
                db.insert(TABLE_REGIONS, null, values);
            }

            db.setTransactionSuccessful();
        }catch(Exception e){
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
            db.close();
        }
        //  db.close();
    }


    public void insertintocutomertype(JSONArray customertypes)  {

        SQLiteDatabase db = this.getWritableDatabase();
        try {

            db.beginTransaction();
            for (int i = 0; i < customertypes.length(); i++) {
                JSONObject categoryobj = customertypes.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(CUSTOMERTYPE_ID, categoryobj.getString("sid"));
                values.put(CUSTOMERTYPE_NAME, categoryobj.getString("name"));
                db.insert(TABLE_CUSTOMERTYPE, null, values);
            }
            db.setTransactionSuccessful();
        }catch(Exception e){
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
            db.close();
        }
        // db.close();
    }

    public void insertintoproductcats(JSONArray productcats) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < productcats.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(PRODUCTCAT_ID, productcats.getJSONObject(i)
                        .optString("sid", ""));
                values.put(PRODUCTCAT_NAME, productcats.getJSONObject(i)
                        .optString("ProdCat", ""));
                long id = db.insert(TABLE_PRODUCTCAT, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }


    }

    public void insertintobrands(JSONArray brands) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.beginTransaction();
            for (int i = 0; i < brands.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(BRAND_ID,
                        brands.getJSONObject(i).optString("sid", ""));
                values.put(BRAND_NAME,
                        brands.getJSONObject(i).optString("Name", ""));
                long id = db.insert(TABLE_BRAND, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }



    }


    public void insertintoforms(JSONArray forms) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.beginTransaction();
            for (int i = 0; i < forms.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(FORM_ID, forms.getJSONObject(i).optString("sid", ""));
                values.put(FORM_NAME,
                        forms.getJSONObject(i).optString("Name", ""));
                long id = db.insert(TABLE_FORM, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }


    }

    public void insertintoproducts(JSONArray products) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + PRODUCT_ID + " FROM " + TABLE_PRODUCT;
        Cursor cursor = db.rawQuery(query, null);
        HashSet<String> ids = new HashSet<String>();
        if (cursor.moveToFirst()) {
            do {
                ids.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        try {
            db.beginTransaction();
            for (int i = 0; i < products.length(); i++) {
                if (ids.contains(products.getJSONObject(i).optString("sid", "")))
                    continue;
                ContentValues values = new ContentValues();
                values.put(PRODUCT_ID,
                        products.getJSONObject(i).optString("sid", ""));
                values.put(PRODUCT_NAME,
                        products.getJSONObject(i).optString("ProductName", ""));
                values.put(PRODUCT_CODE,
                        products.getJSONObject(i).optString("ProductCode", ""));
                values.put(
                        PRODUCT_CAT,
                        products.getJSONObject(i).optString("ProductCat",
                                "Other"));
                values.put(PRODUCT_BRAND,
                        products.getJSONObject(i).optString("Brand", "Other"));
                values.put(PRODUCT_FORM,
                        products.getJSONObject(i).optString("Form", "Other"));

                values.put(PRODUCT_HSN,
                        products.getJSONObject(i).optString("HSN", "NA"));
                long id = db.insert(TABLE_PRODUCT, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

    }

    public void insertintofocsdproducts(JSONArray focusedprds) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < focusedprds.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(FOCUSEDPRDS_ID, focusedprds.getJSONObject(i)
                        .optString("sid", ""));
                values.put(FOCUSEDPRDS_PRODUCT, focusedprds.getJSONObject(i)
                        .optString("product", ""));

                values.put(FOCUSEDPRDS_CUSTCAT, focusedprds.getJSONObject(i)
                        .optString("CustCategory", ""));
                values.put(FOCUSEDPRDS_FLAG, focusedprds.getJSONObject(i)
                        .optString("FocussedProduct", ""));
                values.put(FOCUSEDPRDS_MINQTY, focusedprds.getJSONObject(i)
                        .optString("MinQuantity", ""));

                String from = focusedprds.getJSONObject(i).optString(
                        "StartDate", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(from);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    from = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String to = focusedprds.getJSONObject(i).optString("EndDate",
                        "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(to);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    to = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                values.put(FOCUSEDPRDS_FROM, from);
                values.put(FOCUSEDPRDS_TO, to);
                long id = db.insert(TABLE_FOCUSEDPRDS, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }


    }

    public void insertintoinventory(JSONArray inventory) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < inventory.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(INVENTORY_ID,
                        inventory.getJSONObject(i).getString("sid"));
                values.put(INVENTORY_PRODUCT, inventory.getJSONObject(i)
                        .getString("product"));
                values.put(INVENTORY_QTY,
                        inventory.getJSONObject(i).getString("qty"));
                long id = db.insert(TABLE_INVENTORY, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

    }

    public void insertintoparty(JSONArray party) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + PARTYMARG_ID + " FROM " + TABLE_PARTYMARG;
        Cursor cursor = db.rawQuery(query, null);
        HashSet<String> ids = new HashSet<String>();
        if (cursor.moveToFirst()) {
            do {
                ids.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        try {
            db.beginTransaction();
            for (int i = 0; i < party.length(); i++) {
                if (ids.contains(party.getJSONObject(i).optString("sid", "")))
                    continue;
                ContentValues values = new ContentValues();
                values.put(PARTYMARG_ID,
                        party.getJSONObject(i).optString("sid", ""));
                values.put(PARTYMARG_PRODUCT,
                        party.getJSONObject(i).optString("Product", ""));
                values.put(PARTYMARG_VALUE,
                        party.getJSONObject(i).optString("Value", ""));
                values.put(PARTYMARG_CAT,
                        party.getJSONObject(i).optString("Cat", ""));
                values.put(PARTYMARG_CUSTOMER,
                        party.getJSONObject(i).optString("Customer", ""));

                String from = party.getJSONObject(i).optString("From", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(from);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    from = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String to = party.getJSONObject(i).optString("To", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(to);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    to = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                values.put(PARTYMARG_FROM, from);
                values.put(PARTYMARG_TO, to);

                long id = db.insert(TABLE_PARTYMARG, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
        //  db.close();

    }

    public void insertintoaddis(JSONArray adldisc) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + ADDLDISC_ID + " FROM " + TABLE_ADDLDISC;
        Cursor cursor = db.rawQuery(query, null);
        HashSet<String> ids = new HashSet<String>();
        if (cursor.moveToFirst()) {
            do {
                ids.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        try {
            db.beginTransaction();
            for (int i = 0; i < adldisc.length(); i++) {
                if (ids.contains(adldisc.getJSONObject(i).optString("sid", "")))
                    continue;
                ContentValues values = new ContentValues();
                values.put(ADDLDISC_ID,
                        adldisc.getJSONObject(i).optString("sid", ""));
                values.put(ADDLDISC_VALUE,
                        adldisc.getJSONObject(i).optString("Value", ""));
                values.put(ADDLDISC_CAT,
                        adldisc.getJSONObject(i).optString("Cat", ""));

                String from = adldisc.getJSONObject(i).optString("From", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(from);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    from = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String to = adldisc.getJSONObject(i).optString("To", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(to);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    to = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                values.put(ADDLDISC_FROM, from);
                values.put(ADDLDISC_TO, to);
                long id = db.insert(TABLE_ADDLDISC, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
        // db.close();

    }

    public void insertintomrp(JSONArray mrp) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + MRP_ID + " FROM " + TABLE_MRP;
        Cursor cursor = db.rawQuery(query, null);
        HashSet<String> ids = new HashSet<String>();
        if (cursor.moveToFirst()) {
            do {
                ids.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        try {
            db.beginTransaction();
            for (int i = 0; i < mrp.length(); i++) {
                if (ids.contains(mrp.getJSONObject(i).optString("sid", "")))
                    continue;
                ContentValues values = new ContentValues();
                values.put(MRP_ID, mrp.getJSONObject(i).optString("sid", ""));
                values.put(MRP_PRODUCT,
                        mrp.getJSONObject(i).optString("Product", ""));
                values.put(MRP_VALUE,
                        mrp.getJSONObject(i).optString("Value", ""));
                String from = mrp.getJSONObject(i).optString("From", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(from);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    from = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String to = mrp.getJSONObject(i).optString("To", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(to);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    to = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                values.put(MRP_FROM, from);
                values.put(MRP_TO, to);
                values.put(MRP_BATCH,
                        mrp.getJSONObject(i).optString("ProductBatch", ""));
                values.put(MRP_CONSOFF,
                        mrp.getJSONObject(i).optString("SellingCO", ""));
                values.put(MRP_FLATOFF,
                        mrp.getJSONObject(i).optString("SellingFD", ""));
                values.put(MRP_TAX, mrp.getJSONObject(i).optString("TAX", ""));
                long id = db.insert(TABLE_MRP, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
        // db.close();

    }

    public void insertintounits(JSONArray units) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            db.beginTransaction();
            for (int i = 0; i < units.length(); i++) {
                JSONObject obj = units.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(UNITS_ID, obj.optString("sid", ""));
                values.put(UNITS_NAME, obj.optString("UnitName", ""));
                values.put(UNITS_ABBRV, obj.optString("Abbreviation", ""));
                long id = db.insert(TABLE_UNITS, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }


    }

    public void insertintocreditnotereasons(JSONArray creditnotereasons) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < creditnotereasons.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(CREDITREASONS_ID, creditnotereasons.getJSONObject(i)
                        .optString("sid", ""));
                values.put(CREDITREASONS_REASON, creditnotereasons
                        .getJSONObject(i).optString("reason", ""));
                long id = db.insert(TABLE_CREDITREASONS, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }


    }

    public void insertintoconstants(JSONArray getMapUrl) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < getMapUrl.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(CONSTANTS_KEY,
                        getMapUrl.getJSONObject(i).optString("keys", ""));
                values.put(CONSTANTS_VALUE, getMapUrl.getJSONObject(i)
                        .optString("value", ""));
                long id = db.insert(TABLE_CONSTANTS, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

    }

    public void insertintoexcust(JSONArray getMapUrl) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < getMapUrl.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(EXCUST_ID,
                        getMapUrl.getJSONObject(i).optString("sid", ""));
                values.put(EXCUST_CODE,
                        getMapUrl.getJSONObject(i).optString("CustCode", ""));
                values.put(EXCUST_NAME,
                        getMapUrl.getJSONObject(i).optString("CustName", ""));
                values.put(EXCUST_ADDRESS, getMapUrl.getJSONObject(i)
                        .optString("Address", ""));
                long id = db.insert(TABLE_EXCUST, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

    }

    public void insertintominqty(JSONArray minqty) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < minqty.length(); i++) {
                ContentValues values = new ContentValues();
                // values.put(MINQTY_PKID,
                // minqty.getJSONObject(i).optString("sid", ""));
                values.put(MINQTY_PRODUCT,
                        minqty.getJSONObject(i).optString("product", ""));
                values.put(MINQTY_CUSTCAT,
                        minqty.getJSONObject(i).optString("CustCategory", ""));
                values.put(MINQTY_QUANTITY,
                        minqty.getJSONObject(i).optString("MinQuantity", ""));
                long id = db.insert(TABLE_MINQTY, null, values);
                System.out.println(id);
            }

            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

    }

    public void loadProductBatch(JSONArray batches)  {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();

            for (int i = 0; i < batches.length(); i++) {
                JSONObject obj = batches.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(PRODUCTBATCH_ID, obj.optString("productbatchid", ""));
                values.put(PRODUCTBATCH_BATCH, obj.optString("Batch", ""));
                db.insert(TABLE_PRODUCTBATCH, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public boolean isProductBatchTableEmpty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_PRODUCTBATCH;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    public boolean isInventoryAccountTableEmpty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_INVENTORYACCOUNT;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    public void loadInventoryAccount(JSONArray input)  {
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.beginTransaction();

            for (int i = 0; i < input.length(); i++) {
                JSONObject obj = input.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(INVENTORYACCOUNT_ID,
                        obj.optString("inventoryaccountid", ""));
                values.put(INVENTORYACCOUNT_NAME, obj.optString("AccountName", ""));

                db.insert(TABLE_INVENTORYACCOUNT, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public boolean isTaxTableEmpty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_TAX;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    public void loadTaxTables(JSONArray input) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.beginTransaction();

            HashMap<String, String> taxMap = new HashMap<>();

            for (int i = 0; i < input.length(); i++) {

                JSONObject obj = input.getJSONObject(i);
                String taxId = obj.getString("taxid");
                String taxName = obj.getString("taxname");

                String taxlinename = obj.getString("taxlinename");
                String description = obj.getString("description");
                String rate = obj.getString("rate");
                String type = obj.getString("taxtype");
                String taxlineid = obj.getString("taxlineid");

                if (!taxMap.isEmpty()) {

                    if (!taxMap.containsKey(taxId)) {
                        insertTax(taxId, taxName, db);
                        taxMap.put(taxId, taxName);
                    }

                } else {// 1st row

                    insertTax(taxId, taxName, db);
                    taxMap.put(taxId, taxName);

                }

                insertTaxLine(taxId, taxlineid, taxlinename, description, rate,
                        type, db);

            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public void insertTaxLine(String taxId, String taxlineId,
                              String taxlinename, String description, String rate,
                              String taxtype, SQLiteDatabase db) throws JSONException {

        ContentValues values = new ContentValues();
        values.put(TAXLINE_ID, taxlineId);
        values.put(TAXLINE_DESCRIP, description);
        values.put(TAXLINE_RATE, rate);
        values.put(TAXLINE_TAX, taxId);
        values.put(TAXLINE_NAME, taxlinename);
        values.put(TAXLINE_TYPE, taxtype);

        db.insert(TABLE_TAXLINE, null, values);

    }

    public void insertTax(String taxId, String taxName, SQLiteDatabase db)
            throws JSONException {

        ContentValues values = new ContentValues();
        values.put(TAX_ID, taxId);
        values.put(TAX_NAME, taxName);

        db.insert(TABLE_TAX, null, values);

    }

    public boolean isInvoiceNoGenTableEmpty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_INVOICENOGEN;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    public void loadInvoiceNoGen(JSONArray input)  {
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.beginTransaction();

            for (int i = 0; i < input.length(); i++) {
                JSONObject obj = input.getJSONObject(i);
                ContentValues values = new ContentValues();
                // values.put(INVOICENOGEN_ACTIVE, obj.optString("Active", ""));
                values.put(INVOICENOGEN_CURRVAL, obj.optString("CurrVal", ""));
                values.put(INVOICENOGEN_MAXVAL, obj.optString("MaxVal", ""));
                values.put(INVOICENOGEN_MINVAL, obj.optString("MinVal", ""));
                values.put(INVOICENOGEN_ORGCODE, obj.optString("OrgCode", ""));

                db.insert(TABLE_INVOICENOGEN, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public boolean isInvJrnlTableEmpty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_INVENTORYJRNLLINE;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    public void loadAvailableStock(JSONArray input) throws JSONException {

        JSONObject journalData = new JSONObject();
        JSONArray journalDataArray = new JSONArray();

        String Source = "SYNC";
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -1);
        DateFormat dateTimeFormat = new SimpleDateFormat("M/d/yyyy");

        String currenttime = dateTimeFormat.format(c.getTime());


        for (int i = 0; i < input.length(); i++) {

            String jrnlHdr = UUID.randomUUID().toString();
            JSONObject invObject = input.getJSONObject(i);
            String product = invObject.getString("ProductId");
            String batch = invObject.getString("BatchId");
            double qty = Double.parseDouble(invObject
                    .getString("Available  Quantity"));
            double mrp = Double.parseDouble(invObject.getString("MRP"));

            double amount = qty * mrp;

            JSONObject invJrnlHdr = new JSONObject().put("JrnlHdr", jrnlHdr)
                    .put("JournalNo", Source + currenttime)
                    .put("JournalDate", currenttime).put("Product", product)
                    .put("Source", Source).put("SourceID", currenttime)
                    .put("BatchNo", batch).put("UOM", invObject.optString("UOM", "")); // .put("Organization", value);
            invObject.put("InvJrnlHdr", invJrnlHdr);

            JSONArray InvJournalLines = new JSONArray();

            JSONObject availableStockObject = new JSONObject()
                    .put("JournalHdr", jrnlHdr)
                    .put("AmountCredited", "0")
                    .put("AmountDebited", amount + "")
                    .put("InventoryAccount",
                            getInventoryAccountId("Available Stock"))
                    .put("QuantityCredited", "0")
                    .put("QuantityDebited", qty + "");
            InvJournalLines.put(availableStockObject);

            /*
             * JSONObject goodsIssueObject = new JSONObject() .put("JournalHdr",
             * jrnlHdr) .put("AmountCredited","0") .put("AmountDebited",
             * amount+"" ) .put("InventoryAccount",
             * getInventoryAccountId("Goods Issued"))
             * .put("QuantityCredited","0") .put("QuantityDebited", qty+"");
             * InvJournalLines.put(goodsIssueObject);
             */
            invObject.put("InvJournalLines", InvJournalLines);

            journalDataArray.put(invObject);
        }

        journalData.put("JournalData", journalDataArray);

        insertInventoryJrnl(journalData);

    }

    public Boolean insertInventoryJrnl(JSONObject webresp) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();



        try {
            JSONArray JournalData = webresp.getJSONArray("JournalData");

            for (int i = 0; i < JournalData.length(); i++) {
                ContentValues InvJrnlHdrval = new ContentValues();
                JSONObject jdobj = JournalData.getJSONObject(i);
                JSONObject jhobj = jdobj.getJSONObject("InvJrnlHdr");

                InvJrnlHdrval.put(INVENTORYJRNLHDR_ID,
                        jhobj.getString("JrnlHdr"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_NO,
                        jhobj.getString("JournalNo"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_DATE,
                        jhobj.getString("JournalDate"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_PRODUCT,
                        jhobj.getString("Product"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_ORG,
                        jhobj.optString("Organization", "0"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_SOURCE,
                        jhobj.getString("Source"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_SOURCEID,
                        jhobj.getString("SourceID"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_BATCH,
                        jhobj.getString("BatchNo"));

                InvJrnlHdrval.put(INVENTORYJRNLHDR_UOM,
                        jhobj.getString("UOM"));

                long id = db
                        .insert(TABLE_INVENTORYJRNLHDR, null, InvJrnlHdrval);

                Log.d("inserted1", "insert=" + id);

                JSONArray JournaLine = jdobj.getJSONArray("InvJournalLines");
                for (int j = 0; j < JournaLine.length(); j++) {
                    ContentValues InvJrnlLineval = new ContentValues();

                    JSONObject jlobj = JournaLine.getJSONObject(j);
                    InvJrnlLineval.put(INVENTORYJRNLLINE_ID, "0");
                    InvJrnlLineval.put(INVENTORYJRNLLINE_HDR,
                            jlobj.getString("JournalHdr"));
                    InvJrnlLineval.put(INVENTORYJRNLLINE_AMNTCREDIT,
                            jlobj.getString("AmountCredited"));
                    InvJrnlLineval.put(INVENTORYJRNLLINE_AMNTDEBIT,
                            jlobj.getString("AmountDebited"));
                    // InvJrnlLineval.put(INVENTORYJRNLLINE_ACCOUNT
                    // ,getInventoryAccountId(jlobj.getString("InventoryAccount")));

                    InvJrnlLineval.put(INVENTORYJRNLLINE_ACCOUNT,
                            jlobj.getString("InventoryAccount"));
                    InvJrnlLineval.put(INVENTORYJRNLLINE_QTYCREDIT,
                            jlobj.getString("QuantityCredited"));
                    InvJrnlLineval.put(INVENTORYJRNLLINE_QTYDEBIT,
                            jlobj.getString("QuantityDebited"));

                    id = db.insert(TABLE_INVENTORYJRNLLINE, null,
                            InvJrnlLineval);
                    Log.d("inserted2", "insert=" + id);
                }

            }
        } catch (Exception e) {
            e.getStackTrace();
            throw e;
        }
        // return null;
        Boolean result = true;
        // String count = "SELECT * FROM " + TABLE_INVENTORYJRNLHDR;
        String count = "SELECT * FROM " + TABLE_INVENTORYJRNLLINE;

        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null) {
            cursor.close();
        }
        return result;
    }

    public String getInventoryAccountId(String accountName) {

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + INVENTORYACCOUNT_ID + " FROM "
                + TABLE_INVENTORYACCOUNT + " WHERE " + INVENTORYACCOUNT_NAME
                + " = '" + accountName + "'";

        String id = "0";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            id = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();

        // db.close();
        return id;

    }

    public void loadOffers(JSONArray offersArray) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();

            HashMap<String, String> hashMap = new HashMap<>();

            for (int i = 0; i < offersArray.length(); i++) {

                JSONObject obj = offersArray.getJSONObject(i);
                String offerId = obj.optString(OFFERS_ID, "");
                String NoOfFreeItems = obj.optString(OFFERS_FREEITEMSNO, "");
                String FreeProduct = obj.optString(OFFERS_FREEPRODUCT, "");
                String MinQty = obj.optString(OFFERS_MINQTY, "");
                String OffType = obj.optString(OFFERS_OFFTYPE, "");
                String OffValue = obj.optString(OFFERS_OFFVALUE, "");
                String RateType = obj.optString(OFFERS_RATETYPE, "");
                String UOM = obj.optString(OFFERS_UOM, "");
                String individualFlag = obj.optString("ApplyOnIndividualItem", "");
                String custCat = obj.optString("CustomerCategory", "0");

                String offerDtlId = obj.optString(OFFERS_DETAILS_ID, "");
                String product = obj.optString("ProductName", "");
                String productCategory = obj.optString("ProductCategory", "");

                if (!hashMap.isEmpty()) {

                    if (!hashMap.containsKey(offerId)) {

                        insertOffer(offerId, NoOfFreeItems, FreeProduct, MinQty, OffType,
                                OffValue, RateType, UOM, individualFlag, custCat, db);
                        hashMap.put(offerId, MinQty);
                    }

                } else {// 1st row

                    insertOffer(offerId, NoOfFreeItems, FreeProduct, MinQty, OffType,
                            OffValue, RateType, UOM, individualFlag, custCat, db);
                    hashMap.put(offerId, MinQty);
                }


                insertOfferDtls(offerId, offerDtlId, product, productCategory, db);
            }
            db.setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }

        finally {
            db.endTransaction();
            db.close();
        }

    }

    public void insertOffer(String offerId, String NoOfFreeItems, String FreeProduct, String MinQty,
                            String OffType, String OffValue, String RateType, String UOM, String individualFlag, String custCategory, SQLiteDatabase db)
            throws JSONException {

        ContentValues values = new ContentValues();
        values.put(OFFERS_ID, offerId);
        values.put(OFFERS_FREEITEMSNO, NoOfFreeItems);
        values.put(OFFERS_FREEPRODUCT, FreeProduct);
        values.put(OFFERS_MINQTY, MinQty);
        values.put(OFFERS_OFFTYPE, OffType);
        values.put(OFFERS_OFFVALUE, OffValue);
        values.put(OFFERS_RATETYPE, RateType);
        values.put(OFFERS_UOM, UOM);
        values.put(OFFERS_INDIVIDUAL_FLAG, individualFlag);
        values.put(OFFERS_CUST_CATEGORY, custCategory);
        long id = db.insert(TABLE_OFFERS, null, values);

    }

    public void insertOfferDtls(String offerId, String offerDtlId,
                                String product, String productCategory, SQLiteDatabase db) throws JSONException {

        ContentValues values = new ContentValues();
        values.put(OFFERS_DETAILS_ID, offerDtlId);
        values.put(OFFERS_DETAILS_OFFER, offerId);
        values.put(OFFERS_DETAILS_PRODUCT, product);
        values.put(OFFERS_DETAILS_PRODUCTCATEGORY, productCategory);

        db.insert(TABLE_OFFERS_DETAILS, null, values);

    }

    public void loadExpenseTypes(JSONArray inputArray) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + EXPENSETYPE_ID + " FROM " + TABLE_EXPENSETYPE;
        Cursor cursor = db.rawQuery(query, null);
        HashSet<String> ids = new HashSet<String>();
        if (cursor.moveToFirst()) {
            do {
                ids.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        try {
            db.beginTransaction();
            for (int i = 0; i < inputArray.length(); i++) {
                if (ids.contains(inputArray.getJSONObject(i).optString("_id", "")))
                    continue;
                ContentValues values = new ContentValues();
                values.put(EXPENSETYPE_ID, inputArray.getJSONObject(i).optString("_id", ""));
                values.put(EXPENSETYPE_NAME, inputArray.getJSONObject(i).optString("Expense Type", ""));

                long id = db.insert(TABLE_EXPENSETYPE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }


    }

    public void loadPackageTypes(JSONArray input) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < input.length(); i++) {
                JSONObject obj = input.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(PACKAGE_TYPE_ID, obj.optString("TypeId", ""));
                values.put(PACKAGE_TYPE_CODE, obj.optString("Code", ""));
                values.put(PACKAGE_TYPE_NAME, obj.optString("Name", ""));
                long id = db.insert(TABLE_PACKAGE_TYPE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();

        }


    }

    public void loadPackageTypesMap(JSONArray input) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            db.beginTransaction();
            for (int i = 0; i < input.length(); i++) {
                JSONObject obj = input.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(PRODUCT_PACKAGE_MAP_PRODUCT, obj.optString("Product", ""));
                values.put(PRODUCT_PACKAGE_MAP_UOM, obj.optString("UOM", ""));
                values.put(PRODUCT_PACKAGE_MAP_PACKAGE_TYPE, obj.optString("PackageType", ""));
                values.put(PRODUCT_PACKAGE_MAP_BASE_UNIT, obj.optString("BaseUnitFlag", ""));
                values.put(PRODUCT_PACKAGE_MAP_SELLABLE, obj.optString("SellableFlag", ""));
                values.put(PRODUCT_PACKAGE_MAP_STOCKABLE, obj.optString("StockableFlag", ""));
                values.put(PRODUCT_PACKAGE_MAP_CONVERSION_FACTOR, obj.optString("ConversionFactor", ""));
                values.put(PRODUCT_PACKAGE_MAP_ID, obj.optString("PPMId", ""));
                //				values.put(UNITSMAP_UPC, obj.optString("UPC", ""));
                long id = db.insert(TABLE_PRODUCT_PACKAGE_MAPPING, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }


    }

    public void deleteService(String services) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String count = "DELETE FROM " + TABLE_MOBSERVICES + " WHERE "
                    + MOBILESERVICE_NAME + "='" + services + "'";
            db.execSQL(count);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createNewSchedule(Scheduledata header) {

        long key = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            db.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(SCHEDULE_ID, header.Scheduleid);
            values.put(SCHEDULE_NAME, header.Schedulename);
            values.put(SALESPERSON_ID, header.Salespersonid);
            values.put(SALESPERSON_NAME, header.salespersonname);
            values.put(SALEROUTE_ID, header.Salesrouteid);
            values.put(SCHEDULE_DATE, header.Scheduledate);
            values.put(HEADER_STATUS, header.Headerstatus);
            values.put(ROUTENETWORK_ID, header.Routenetworkid);
            values.put(SCHEDULE_BEGIN_TIME, header.begintime);
            key = db.insert(TABLE_SCHEDULE_HEADER, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public void intialisesynctable(String scheduleBeginTime) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {

            db.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(TABLEINSYNC, TABLE_ORDER_DETAIL);
            values.put(SYNCTIME, scheduleBeginTime);
            db.insert(TABLE_SYNCTABLE, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
        locintialise(scheduleBeginTime);
        expenseSyncInitialise(scheduleBeginTime);
    }

    public void expenseSyncInitialise(String scheduleBeginTime) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            db.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(TABLEINSYNC, TABLE_EXPENSE);
            values.put(SYNCTIME, scheduleBeginTime);
            db.insert(TABLE_SYNCTABLE, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public void locintialise(String scheduleBeginTime) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            db.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(TABLEINSYNC, TABLE_LOCATIONS);
            values.put(SYNCTIME, scheduleBeginTime);
            db.insert(TABLE_SYNCTABLE, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public Boolean ispricetableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_PRICEDETAILS;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;

    }

    public void loadpricetable(JSONArray pricedata) throws JSONException {

        SQLiteDatabase db = this.getWritableDatabase();
        try {

            db.beginTransaction();
            for (int i = 0; i < pricedata.length(); i++) {
                JSONObject rateobject = pricedata.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(PRICEDETAILS_PRODUCTID,
                        String.valueOf(rateobject.getInt("productId")));
                values.put(PRICEDETAILS_CUSTOMERCATEGORY,
                        rateobject.getString("CustomerCategory"));
                values.put(PRICE, rateobject.getString("Rate"));
                values.put(VALIDFROM, rateobject.getString("ValidFrom"));
                values.put(VALIDTO, rateobject.getString("ValidTo"));
                values.put(MRP, rateobject.optString("mrp", "0.0"));
                db.insert(TABLE_PRICEDETAILS, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public Boolean categorytableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_CUSTOMERCATEGORIES;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return result;

    }

    public void loadcustomercategorys(JSONArray categorydata)
            throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        for (int i = 0; i < categorydata.length(); i++) {
            JSONObject categoryobj = categorydata.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(CUSTOMERCATEGORIES_CODE, categoryobj.getString("id"));
            values.put(CUSTOMERCATEGORIES_NAME,
                    categoryobj.getString("CustCat"));
            values.put(CUSTOMERCATEGORIES_TYPE,
                    categoryobj.optString("Type", ""));
            db.insert(TABLE_CUSTOMERCATEGORIES, null, values);
        }

        db.close();
    }

    public Boolean isinvoicetableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_INVOICE;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;

    }

    public void loadinvoicetable(JSONArray invoicedata)   {

        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.beginTransaction();
            for (int i = 0; i < invoicedata.length(); i++) {
                JSONObject invoiceobject = invoicedata.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(INVOICE_ID, String.valueOf(invoiceobject.getInt("_id")));
                values.put(INVOICE_NO, invoiceobject.getString("InvoiceNo"));
                values.put(INVOICE_CUSTOMERID, invoiceobject.getString("cusid"));
                values.put(INVOICE_TOTTALAMOUNT,
                        invoiceobject.getString("invototal"));
                values.put(INVOICE_BALANCEAMOUNT,
                        invoiceobject.getString("balance"));
                values.put(INVOICE_STATUS, invoiceobject.getString("Status"));
                values.put(INVOICE_DATE, invoiceobject.optString("InvoiceDate"));
                values.put(INVOICE_STATICBALANCE,
                        invoiceobject.getString("balance"));
                db.insert(TABLE_INVOICE, null, values);

            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public boolean isbranchestableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_BRANCHES;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }

        if (cursor != null)
            cursor.close();
        return result;

    }

    public void loadbranches(JSONArray branches) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.beginTransaction();
            for (int i = 0; i < branches.length(); i++) {
                JSONObject branchesobj = branches.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(BRANCHES_ID, branchesobj.optString("sid", ""));
                values.put(BRANCHES_NAME, branchesobj.optString("orgname", ""));
                db.insert(TABLE_BRANCHES, null, values);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
        }
    }

    public Boolean isreasontableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_REASONS;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;

    }

    public void loadresons(JSONArray resondata) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < resondata.length(); i++) {
            JSONObject reasonobj = resondata.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(REASONS_CODE, reasonobj.getString("Id"));
            values.put(REASON_DETAIL, reasonobj.getString("Reason"));
            db.insert(TABLE_REASONS, null, values);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void updateInventoryFlag(String flag) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MOBILESERVICE_DISABLE_INVENTORY, flag);

        int id = db.update(TABLE_MOBSERVICES, values, "", new String[]{});


    }

    public Boolean bankmastertableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_BANKMASTER;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }

        if (cursor != null)
            cursor.close();
        return result;

    }

    public void loadbankmaster(JSONArray bankmasterdata) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < bankmasterdata.length(); i++) {
            JSONObject reasonobj = bankmasterdata.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(BANKMASTER_CODE, reasonobj.getString("id"));
            values.put(BANKMASTER_NAME, reasonobj.getString("bankname"));
            values.put(BANKMASTER_BRANCHNAME,
                    reasonobj.getString("baranchname"));
            db.insert(TABLE_BANKMASTER, null, values);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public Boolean ischecklisttableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_PRODUCTCHECKLIST;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }

        if (cursor != null)
            cursor.close();
        return result;

    }

    public void loadchecklist(JSONArray checklist) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < checklist.length(); i++) {
            JSONObject checklistobj = checklist.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(PRODUCTCHECKLIST_CODE, checklistobj.getString("id"));
            values.put(PRODUCTCHECKLIST_NAME,
                    checklistobj.getString("CustomerChecklist"));
            values.put(PRODUCTCHECKLIST_SEQNO, checklistobj.getString("SeqNo"));
            db.insert(TABLE_PRODUCTCHECKLIST, null, values);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public Boolean istowntableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_TOWN;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }

        if (cursor != null)
            cursor.close();
        return result;

    }

    public void loadtowns(JSONArray towns) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ;
        for (int i = 0; i < towns.length(); i++) {
            JSONObject obj = towns.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(TOWN_CODE, obj.optString("townid", ""));
            values.put(TOWN_NAME, obj.optString("townname", ""));
            values.put(TOWN_ORG, obj.optString("organization", ""));
            db.insert(TABLE_TOWN, null, values);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public boolean isothercompanytableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_OTHERCOMPANY;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    public void loadothercompany(JSONArray othercompany) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ;
        for (int i = 0; i < othercompany.length(); i++) {
            JSONObject obj = othercompany.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(OTHERCOMPANY_ID, obj.getString("othercompanyid"));
            values.put(OTHERCOMPANY_NAME, obj.getString("othercompanyname"));
            db.insert(TABLE_OTHERCOMPANY, null, values);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public Boolean iscustomersearchtableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_SEARCHCUSTOMERS;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;

    }

    public void loadcustomersearchtable(JSONArray customerdata)
            throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();

            for (int i = 0; i < customerdata.length(); i++) {
                JSONObject custobj = customerdata.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(SEARCHCUSTOMERS_ID, custobj.getString("custid"));
                values.put(SEARCHCUSTOMERS_NAME, custobj.optString("custname", ""));
                values.put(SEARCHCUSTOMERS_ADRESS,
                        custobj.optString("custadress", ""));
                values.put(SEARCHCUSTOMERS_CODE, custobj.optString("custcode", ""));
                values.put(SEARCHCUSTOMERS_LATTITUDE,
                        custobj.optString("custlat", "0"));
                values.put(SEARCHCUSTOMERS_LONGITUDE,
                        custobj.optString("custlong", "0"));
                values.put(SEARCHCUSTOMERS_CATEGORY,
                        custobj.optString("custcategory", ""));
                values.put(SEARCHCUSTOMERS_LOCATIONLOCK,
                        custobj.optString("locationverified", "0"));
                values.put(SEARCHCUSTOMERS_CLOSINGDAY,
                        custobj.optString("Closingday", ""));
                values.put(SEARCHCUSTOMERS_MOBILENUMBER,
                        custobj.optString("mobilenumber", ""));

                values.put(SEARCHCUSTOMERS_MOBILENUMBER,
                        custobj.optString("mobilenumber", ""));
                values.put(SEARCHCUSTOMERS_MOBILENUMBER,
                        custobj.optString("mobilenumber", ""));
                values.put(SEARCHCUSTOMERS_MOBILENUMBER,
                        custobj.optString("mobilenumber", ""));
                db.insert(TABLE_SEARCHCUSTOMERS, null, values);

            }

            db.setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
            db.close();
        }

    }

    public Boolean denomtableableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_DENOMINATIONS;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;

    }

    public void loaddenomdata(JSONArray denomdata) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        for (int i = 0; i < denomdata.length(); i++) {
            JSONObject denomobj = denomdata.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(DENOMINATIONS_CODE, denomobj.getString("id"));
            values.put(DENOMINATIONS_AMOUNT, denomobj.optString("amount", "0"));
            db.insert(TABLE_DENOMINATIONS, null, values);

        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void insertscheduledata(List<Scheduledata> Scheduldat) {
        Long key;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        Log.e("insertscheduledata",Scheduldat.size() + "------------------------------------------");
        for (int i = 0; i < Scheduldat.size(); i++) {

            ContentValues values = new ContentValues();
            values.put(SCHEDULE_ID, Scheduldat.get(i).Scheduleid);
            values.put(SCHEDULE_NAME, Scheduldat.get(i).Schedulename);
            values.put(SALESPERSON_ID, Scheduldat.get(i).Salespersonid);
            values.put(SALESPERSON_NAME, Scheduldat.get(i).salespersonname);
            values.put(SALEROUTE_ID, Scheduldat.get(i).Salesrouteid);
            values.put(SCHEDULE_DATE, Scheduldat.get(i).Scheduledate);
            values.put(HEADER_STATUS, Scheduldat.get(i).Headerstatus);
            values.put(ROUTENETWORK_ID, Scheduldat.get(i).Routenetworkid);
            values.put(SCHEDULE_COMPLETION_STATUS,
                    Scheduldat.get(i).completionstatus);
            values.put(SCHEDULE_BEGIN_TIME, Scheduldat.get(i).begintime);
            values.put(SCHEDULE_COMPLETION_TIME,
                    Scheduldat.get(i).completiontime);
            key = db.insert(TABLE_SCHEDULE_HEADER, null, values);
            for (int j = 0; j < Scheduldat.get(i).Scheduledetails.size(); j++) {

                ContentValues values2 = new ContentValues();
                values2.put(
                        SCHEDULE_DETAIL_ID,
                        Scheduldat.get(i).Scheduledetails.get(j).Scheduledetailid);
                values2.put(SCHEDULE_HEADER_ID, String.valueOf(key));
                values2.put(LOCATION_ID,
                        Scheduldat.get(i).Scheduledetails.get(j).locationid);
                values2.put(LOCATION_NAME,
                        Scheduldat.get(i).Scheduledetails.get(j).locationname);
                values2.put(LOCATION_ADRESS,
                        Scheduldat.get(i).Scheduledetails.get(j).locationadress);
                values2.put(SEQNO,
                        Scheduldat.get(i).Scheduledetails.get(j).sequencenumber);
                values2.put(DETAIL_STATUS,
                        Scheduldat.get(i).Scheduledetails.get(j).status);
                values2.put(LATITUDE,
                        Scheduldat.get(i).Scheduledetails.get(j).latitude);
                values2.put(LONGITUDE,
                        Scheduldat.get(i).Scheduledetails.get(j).longitude);
                values2.put(LATITUDE_NEW, "0");
                values2.put(LONGITUDE_NEW, "0");
                values2.put(SCHEDULEDETAIL_CREATETIME, "0");
                values2.put(CLOSING_DAY_UPDATETIME, "0");
                values2.put(
                        CUSTOMERCATOGORY,
                        Scheduldat.get(i).Scheduledetails.get(j).Customercatogory);
                values2.put(
                        CUSTOMERCATOGORYID,
                        Scheduldat.get(i).Scheduledetails.get(j).CustomercatogoryId);
                values2.put(RETAILPRICE,
                        Scheduldat.get(i).Scheduledetails.get(j).Rettail);
                values2.put(INVOCENUMBER,
                        Scheduldat.get(i).Scheduledetails.get(j).invoicenumber);
                values2.put(PICKLISTNUMBER,
                        Scheduldat.get(i).Scheduledetails.get(j).picklistnumber);
                values2.put(SCHED_CUSTOMERCODE,
                        Scheduldat.get(i).Scheduledetails.get(j).customercode);
                values2.put(LOCATIONLOCK,
                        Scheduldat.get(i).Scheduledetails.get(j).locationlock);
                values2.put(CLOSING_DAY,
                        Scheduldat.get(i).Scheduledetails.get(j).Closingday);
                values2.put(SCHEDULEDETAIL_MOBILE,
                        Scheduldat.get(i).Scheduledetails.get(j).mobilenumber);
                values2.put(SCHEDULEDETAIL_OTP,
                        Scheduldat.get(i).Scheduledetails.get(j).OTP);
                values2.put(SCHEDULEDETAIL_TINNUMBER,
                        Scheduldat.get(i).Scheduledetails.get(j).tinnumber);
                values2.put(SCHEDULEDETAIL_PAN,
                        Scheduldat.get(i).Scheduledetails.get(j).pan);
                values2.put(SCHEDULEDETAIL_PHOTOUUID,
                        Scheduldat.get(i).Scheduledetails.get(j).photoUUID);
                values2.put(SCHEDULEDETAIL_LANDMARK,
                        Scheduldat.get(i).Scheduledetails.get(j).landmark);
                values2.put(SCHEDULEDETAIL_TOWN,
                        Scheduldat.get(i).Scheduledetails.get(j).town);
                values2.put(SCHEDULEDETAIL_CONTACTPERSON,
                        Scheduldat.get(i).Scheduledetails.get(j).contactperson);
                values2.put(SCHEDULEDETAIL_OTHERCOMPANY,
                        Scheduldat.get(i).Scheduledetails.get(j).othercompany);
                values2.put(SCHEDULEDETAIL_CRATES,
                        Scheduldat.get(i).Scheduledetails.get(j).crates);
                values2.put(
                        SCHEDULEDETAIL_ALTERCUSTCODE,
                        Scheduldat.get(i).Scheduledetails.get(j).alternatecustcode);
                values2.put(SCHEDULEDETAIL_CUSTTYPE,
                        Scheduldat.get(i).Scheduledetails.get(j).custtype);


                values2.put(SCHEDULEDETAIL_TARGET_TYPE,
                        Scheduldat.get(i).Scheduledetails.get(j).targetType);
                values2.put(SCHEDULEDETAIL_TARGET_VALUE,
                        Scheduldat.get(i).Scheduledetails.get(j).targetValue);
                /*values2.put(SCHEDULEDETAIL_CREDIT_BALANCE,
                        Scheduldat.get(i).Scheduledetails.get(j).creditBalance);*/
                /*values2.put(SCHEDULEDETAIL_CREDIT_LIMIT,
                        Scheduldat.get(i).Scheduledetails.get(j).creditLimit);*/

                values2.put(SCHEDULEDETAIL_GSTTYPE,
                        Scheduldat.get(i).Scheduledetails.get(j).gsttype);
                values2.put(SCHEDULEDETAIL_FSSI,
                        Scheduldat.get(i).Scheduledetails.get(j).fssi);
                values2.put(SCHEDULEDETAIL_STATE_CODE,
                        Scheduldat.get(i).Scheduledetails.get(j).stateCode);


                /*
                 * values2.put( SCHEDULEDETAIL_ALTERCUSTCODE,
                 * Scheduldat.get(i).Scheduledetails.get(j).alternatecustcode);
                 * values2.put( SCHEDULEDETAIL_ALTERCUSTCODE,
                 * Scheduldat.get(i).Scheduledetails.get(j).alternatecustcode);
                 */

                db.insert(TABLE_SCHEDULE_DETAIL, null, values2);

            }

        }

        // 2nd argument is String containing nullColumnHack
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public Boolean isproducttableempty() {
        Boolean result = true;
        // SQLiteDatabase db = this.getWritableDatabase();
        // String count = "SELECT * FROM " + PRODUCT_TABLE;
        // Cursor cursor = db.rawQuery(count, null);
        // if (cursor.moveToFirst()) {
        // result = false;
        // }

        return result;

    }


    public void insertproducts(List<product> products) {


    }


    public JSONObject getworksummary() throws Exception {

        int numberofstores = 0;
        int numberofvisitedstores = 0;
        int numberofproductivestores = 0;
        double artarget = 0.0;

        Double amount_collected = 0.0;
        Double ordeamount_enterred = 0.0;
        int orderstorescount = 0;
        JSONObject summarydata = new JSONObject();

        SQLiteDatabase db = this.getWritableDatabase();
        String getstore_count = "SELECT COUNT(*) FROM " + TABLE_SCHEDULE_DETAIL;
        Cursor cursor = db.rawQuery(getstore_count, null);
        if (cursor.moveToFirst()) {

            numberofstores = cursor.getInt(0);
        }

        String get_visited_store_count = "SELECT COUNT(*) FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_VISITED
                + "=" + "1";
        cursor = db.rawQuery(get_visited_store_count, null);
        if (cursor.moveToFirst()) {

            numberofvisitedstores = cursor.getInt(0);
        }

        /*
         * String get_productive_store_count = "SELECT COUNT(*) FROM " +
         * TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_PRODUCTIVE + "="
         * + "1";
         */

        // String get_productive_store_count = "SELECT COUNT(*) FROM "
        String get_productive_store_count = "SELECT count(DISTINCT CASE WHEN "
                + PAYMENT_PKID + " is not null or " + ORDERHDR_PKID
                + " is not null or " + ORDERAMOUNTDATA_PKID
                + " is not null THEN sl." + SCHEDULE_DETAIL_PKID
                + " ELSE null END) FROM " + TABLE_SCHEDULE_DETAIL
                + " sl LEFT JOIN " + TABLE_PAYMENTS + " pt ON pt. "
                + PAYMENT_SCHEDULEDETAILID + " =sl." + SCHEDULE_DETAIL_ID
                + " AND pt. " + PAYMENT_APROOVEFLAG + " =1 " + " LEFT JOIN "
                + TABLE_ORDERHDR + " ot ON ot. " + ORDERHDR_SCHLINEID
                + " =sl. " + SCHEDULE_DETAIL_ID + " AND ot. "
                + ORDERHDR_APPROVEFLAG + " =1 " + " LEFT JOIN "
                + TABLE_ORDERAMOUNTDATA + " oamt ON oamt. "
                + ORDERAMOUNTDATA_SCHEDULEDETAILID + " =sl. "
                + SCHEDULE_DETAIL_ID + " WHERE " + SCHEDULE_DETAIL_PRODUCTIVE
                + " =1";
        cursor = db.rawQuery(get_productive_store_count, null);
        if (cursor.moveToFirst()) {

            numberofproductivestores = cursor.getInt(0);
        }

        String get_tottalcollectedamount = "SELECT SUM(AMOUNT) FROM "
                + TABLE_PAYMENTS + " WHERE " + PAYMENT_METHOD
                + "!= 'Return' AND " + PAYMENT_METHOD + "!= 'Credit'" + " AND "
                + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG + "=1";
        ;
        cursor = db.rawQuery(get_tottalcollectedamount, null);
        if (cursor.moveToFirst()) {

            amount_collected = cursor.getDouble(0);
        }

        String get_tottal_orderamount = "SELECT SUM(" + ORDERAMOUNTDATA_AMOUNT
                + ") FROM " + TABLE_ORDERAMOUNTDATA;
        cursor = db.rawQuery(get_tottal_orderamount, null);
        if (cursor.moveToFirst()) {

            ordeamount_enterred = cursor.getDouble(0);
        }

        String marketorder_count = "SELECT COUNT(*) FROM "
                + TABLE_ORDERAMOUNTDATA;
        cursor = db.rawQuery(marketorder_count, null);
        if (cursor.moveToFirst()) {

            orderstorescount = cursor.getInt(0);
        }

        // ar target

        // getting AR target for the schedule
        String get_artarget = "select sum (inv.`staticbalance`) from invoice inv join scheduledetail schdtl on  schdtl.`locationid`=inv.`customerid`";

        cursor = db.rawQuery(get_artarget, null);
        if (cursor.moveToFirst()) {

            artarget = cursor.getDouble(0);
        }

        summarydata.put("tottalnumberofstores", numberofstores);
        summarydata.put("visitedstores", numberofvisitedstores);
        summarydata.put("Tottalamountcollected", amount_collected);
        summarydata.put("Tottalorderamount", ordeamount_enterred);
        summarydata.put("tottalmarketorders", orderstorescount);
        summarydata.put("productivestores", numberofproductivestores);
        summarydata.put("artarget", artarget);
        if (cursor != null)
            cursor.close();
        return summarydata;

    }

    public HashMap<String, String> getCustInvoice() {
        HashMap<String, String> temp = new HashMap<>();
        String query = "Select " + INVOICE_CUSTOMERID +
                ", " + INVOICE_NO + ", " + INVOICE_TOTTALAMOUNT + " from "
                + TABLE_INVOICE + " where " + INVOICE_STATUS + " != 'Cancelled' group by " + INVOICE_CUSTOMERID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put(cursor.getString(0), "Invoice: " + cursor.getString(1)
                        + ", Amount: " + cursor.getDouble(2));
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getmapurl() {
        String temp = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + CONSTANTS_VALUE + " FROM " + TABLE_CONSTANTS
                + " WHERE " + CONSTANTS_KEY + "='mapURL'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            temp = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getmapurloptions() {
        String temp = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + CONSTANTS_VALUE + " FROM " + TABLE_CONSTANTS
                + " WHERE " + CONSTANTS_KEY + "='mapURLOptions'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            temp = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public List<String> getExpenseTypes(String defaultSelect) {
        // Reny
        List<String> temp = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        temp.add(defaultSelect);
        String sql = "select " + EXPENSETYPE_NAME + " from " + TABLE_EXPENSETYPE;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                temp.add(cursor.getString(cursor.getColumnIndex(EXPENSETYPE_NAME)));
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getExpenseTypeId(String expenseTypeString) {
        String temp = "";
        expenseTypeString = expenseTypeString.replaceAll("'", "\'");
        String selectQuery = "SELECT " + EXPENSETYPE_ID + " FROM "
                + TABLE_EXPENSETYPE + " WHERE " + EXPENSETYPE_NAME + "= '" + expenseTypeString + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public long saveExpense(ContentValues values) {
        SQLiteDatabase db = this.getWritableDatabase();
        long id = 0;
        try {

            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat(
                    "yyyyMMddHHmmss");
            String createdtime = df.format(c.getTime());

            ContentValues insertValues = new ContentValues(values);
            insertValues.put(EXPENSE_CREATETIME, createdtime);
            id = db.insert(TABLE_EXPENSE, null, insertValues);

        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();

        return id;
    }

    public void deleteExpense(String expenseId) {
        SQLiteDatabase db = this.getWritableDatabase();

        if (!expenseId.contentEquals("0")) {
            String[] whereArgs = new String[]{expenseId};
            db.delete(TABLE_EXPENSE, EXPENSE_PKID + "=?", whereArgs);
        } else {
            String[] whereArgs = new String[]{};

            db.delete(TABLE_EXPENSE, null, whereArgs);
        }
        db.close();
    }

    public HashMap<String, FourStrings> getExpenses() {

        HashMap<String, FourStrings> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();
        /*	+ " WHERE oln." + ORDERLINE_CREATETIME + ">'" + lastsyc
        + "' AND " + ORDERHDR_APPROVEFLAG + "='1'";*/

        String sql = "select e." + EXPENSE_CREATETIME + ",e." + EXPENSE_EXPENSETYPE + ",e." + EXPENSE_AMOUNT + ",e." +
                EXPENSE_DESCRIPTION + ",e." + EXPENSE_PKID + ",et." + EXPENSETYPE_NAME + " from " + TABLE_EXPENSE + " e " +
                "JOIN " + TABLE_EXPENSETYPE + " et ON et." + EXPENSETYPE_ID + " = e." + EXPENSE_EXPENSETYPE;
        //	+" WHERE e."+EXPENSE_CREATETIME +">'"+lastsyc+"'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                FourStrings strings =
                        new FourStrings(cursor.getString(cursor.getColumnIndex(EXPENSE_EXPENSETYPE)),
                                cursor.getString(cursor.getColumnIndex(EXPENSE_AMOUNT)),
                                cursor.getString(cursor.getColumnIndex(EXPENSE_DESCRIPTION)) == null ? ""
                                        : cursor.getString(cursor.getColumnIndex(EXPENSE_DESCRIPTION)),
                                //	cursor.getString(cursor.getColumnIndex(EXPENSE_DESCRIPTION)),
                                cursor.getString(cursor.getColumnIndex(EXPENSETYPE_NAME)),
                                cursor.getString(cursor.getColumnIndex(EXPENSE_CREATETIME)) == null ? ""
                                        : cursor.getString(cursor.getColumnIndex(EXPENSE_CREATETIME)), "", "");
                temp.put(cursor.getString(cursor.getColumnIndex(EXPENSE_PKID)), strings);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;

    }


    public void updateExpense(String expenseId, String amount) {
        SQLiteDatabase db = this.getWritableDatabase();

        if (!expenseId.contentEquals("0")) {
            String where = EXPENSE_PKID + "=?";
            String[] whereArgs = new String[]{expenseId};

            ContentValues values = new ContentValues();
            if (Double.parseDouble(amount) > 0) {

                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                String createdtime = df.format(c.getTime());
                values.put(EXPENSE_CREATETIME, createdtime);
                values.put(EXPENSE_AMOUNT, amount);
                db.update(TABLE_EXPENSE, values, where, whereArgs);

            } else {
                db.delete(TABLE_EXPENSE, where, whereArgs);
            }

        }
        db.close();
    }

    public long AddNavigationdata(Vehicledata vehdata) {
        SQLiteDatabase db = this.getWritableDatabase();
        long id = 0;
        try{

            db.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(KEY_PLANID, vehdata.PlanId);
            values.put(KEY_VEHICLEID, vehdata.VehicleId);
            values.put(KEY_DEVICETIME, vehdata.DeviceTime);
            values.put(KEY_LATITUDE, vehdata.Latitude);
            values.put(KEY_LONGITUDE, vehdata.Longitude);
            values.put(KEY_SPEED, vehdata.Speed);
            values.put(KEY_PROVIDER, vehdata.provider);
            values.put(KEY_BEARING, vehdata.bearing);
            values.put(KEY_ACCURACY, vehdata.accuracy);
            values.put(KEY_DISTANCE, vehdata.distance);
            values.put(KEY_CUSTOMER, vehdata.customer);
            values.put(NAVIGATIONDATA_UPFLAG, "0");
            values.put(KEY_TIMEDIFF, vehdata.timediff);
            values.put(KEY_MONGODT, vehdata.mongoDate);
            values.put(KEY_INACTIVE, vehdata.inactive);

            // Inserting Row
            id = db.insert(TABLE_NAVIGATIONDATA, null, values);
            db.setTransactionSuccessful();

        }catch(Exception e){
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
            db.close();
        }
        db.close();

        return id;
    }

    public JSONArray getallcustomerlatlongs() throws Exception {
        JSONArray array = new JSONArray();
        String sql = "select " + LOCATION_ID + "," + LATITUDE + "," + LONGITUDE
                + " from " + TABLE_SCHEDULE_DETAIL;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                if (!cursor.getString(0).contains("-")
                        && Double.parseDouble(cursor.getString(1)) > 0) {
                    JSONObject obj = new JSONObject();
                    obj.put("customer", cursor.getString(0));
                    obj.put("lat", cursor.getString(1));
                    obj.put("long", cursor.getString(2));
                    array.put(obj);
                }
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return array;
    }

    public JSONArray getallcustomerlatlongsnotpass() throws Exception {
        JSONArray array = new JSONArray();
        String sql = "select " + SCHEDULE_DETAIL_PKID + "," + LATITUDE + ","
                + LONGITUDE + " from " + TABLE_SCHEDULE_DETAIL + " where "
                + SCHEDULEDETAIL_PASSBY + "!='1' or " + SCHEDULEDETAIL_PASSBY
                + " is null";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                if (!cursor.getString(0).contains("-")
                        && Double.parseDouble(cursor.getString(1)) > 0) {
                    JSONObject obj = new JSONObject();
                    obj.put("slineid", cursor.getString(0));
                    obj.put("lat", cursor.getString(1));
                    obj.put("long", cursor.getString(2));
                    array.put(obj);
                }
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return array;
    }

    public int NavDatacounttoSync() {
        int temp = 0;
        String countQuery = "SELECT count(1) FROM " + TABLE_NAVIGATIONDATA
                + " where " + NAVIGATIONDATA_UPFLAG + "!='1' or "
                + NAVIGATIONDATA_UPFLAG + " is null";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        if (cursor.moveToFirst()) {
            temp = cursor.getInt(0);
        }
        if (cursor != null)
            cursor.close();

        return temp;
    }

    public void updateSlinePassby(String slineid, String crtime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SCHEDULEDETAIL_PASSBY, "1");
        values.put(SCHEDULEDETAIL_PASSBYCRTIME, crtime);
        db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_PKID + "=?",
                new String[]{slineid});
    }

    public List<Vehicledata> Getallnavigationdata() {

        List<Vehicledata> vehicledatalist = new ArrayList<Vehicledata>();
        try {
            // Select All Query
            String selectQuery = "SELECT * FROM " + TABLE_NAVIGATIONDATA
                    + " where " + NAVIGATIONDATA_UPFLAG + "!='1' or "
                    + NAVIGATIONDATA_UPFLAG + " is null";
            //Log.e("In DB", selectQuery);
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list

            if (cursor.moveToFirst()) {
                do {
                    Vehicledata vehdata = new Vehicledata();
                    vehdata.pkid = cursor.getString(0);
                    vehdata.PlanId = cursor.getString(1);
                    vehdata.VehicleId = cursor.getString(2);
                    vehdata.DeviceTime = cursor.getString(3);
                    vehdata.Latitude = cursor.getString(4);
                    vehdata.Longitude = cursor.getString(5);
                    vehdata.Speed = cursor.getString(6);
                    vehdata.provider = cursor.getString(7);
                    vehdata.bearing = cursor.getString(8);
                    vehdata.accuracy = cursor.getString(9);
                    vehdata.distance = cursor.getString(10);
                    vehdata.customer = cursor.getString(cursor
                            .getColumnIndex(KEY_CUSTOMER));
                    vehdata.timediff = cursor.getString(cursor
                            .getColumnIndex(KEY_TIMEDIFF));
                    vehdata.mongoDate = cursor.getString(cursor
                            .getColumnIndex(KEY_MONGODT));
                    vehdata.inactive = cursor.getString(cursor
                            .getColumnIndex(KEY_INACTIVE));
                    vehicledatalist.add(vehdata);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicledatalist;

    }

    public void cleartable(String pkid) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "update " + TABLE_NAVIGATIONDATA + " set "
                + NAVIGATIONDATA_UPFLAG + "='1' where " + KEY_NAVIGATIONID
                + " in " + pkid;
        db.execSQL(query);

    }

    public JSONArray getalllatlongs() throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        String id = largetVistedSeq(db);
        JSONArray alldata = new JSONArray();

        String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE_DETAIL;

        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                JSONObject latlongdata = new JSONObject();
                if (cursor.getString(0).contentEquals(id))
                    latlongdata.put("status", "Next");
                else
                    latlongdata.put("status", cursor.getString(7));
                latlongdata.put("latitude_current", cursor.getString(8));
                latlongdata.put("longitude_current", cursor.getString(9));
                latlongdata.put("storename", cursor.getString(4));
                latlongdata.put("adress", cursor.getString(5));
                latlongdata.put("seq", cursor.getString(6));
                latlongdata.put("provider", "gps");
                latlongdata.put("cat", cursor.getString(13));

                alldata.put(latlongdata);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();

        return alldata;

    }

    private String largetVistedSeq(SQLiteDatabase db) {
        String id = "0";
        String sql = "select " + SCHEDULE_DETAIL_PKID + " from "
                + TABLE_SCHEDULE_DETAIL + " where " + SEQNO + "*1>(select "
                + SEQNO + " from " + TABLE_SCHEDULE_DETAIL + " where "
                + DETAIL_STATUS + "='Completed' order by " + SEQNO
                + "*1 desc limit 1)*1 order by " + SEQNO + "*1 asc limit 1";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            id = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        if (id.contentEquals("0")) {
            sql = "select " + SCHEDULE_DETAIL_PKID + " from "
                    + TABLE_SCHEDULE_DETAIL + " order by " + SEQNO
                    + "*1 limit 1";
            Cursor cursor1 = db.rawQuery(sql, null);
            if (cursor1.moveToFirst()) {
                id = cursor1.getString(0);
            }
            if (cursor1 != null)
                cursor1.close();
        }
        return id;
    }

    public void updateinvoicetableonpaymentdeletion(JSONArray inviceupdatedata)
            throws JSONException {

        SQLiteDatabase db = this.getWritableDatabase();

        for (int i = 0; i < inviceupdatedata.length(); i++) {
            ContentValues values = new ContentValues();
            JSONObject updateobject = inviceupdatedata.getJSONObject(i);
            String id = updateobject.getString("invoicepkid");
            double amount_payed = Double.parseDouble(updateobject
                    .getString("amountpayed"));
            double balance = Double.parseDouble(updateobject
                    .getString("invoicebalance"));
            double new_balance = amount_payed + balance;

            values.put(INVOICE_BALANCEAMOUNT, String.valueOf(new_balance));
            values.put(INVOICE_STATUS, "Not Paid");
            db.update(TABLE_INVOICE, values, INVOICE_PKID + "=?",
                    new String[]{id});

        }
    }

    // display invoices
    public JSONArray getpaymentstodisplay(String Scheduledetailid)
            throws Exception {
        Scheduledetailid = "'" + Scheduledetailid + "'";
        JSONArray paymentlist = new JSONArray();
        String selectQuery = "SELECT * FROM " + TABLE_PAYMENTS + " WHERE "
                + PAYMENT_SCHEDULEDETAILID + " = " + Scheduledetailid + " AND "
                + DELETE_FLAG + "=0";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();

                String paymentid = cursor.getString(0);
                obj.put("paymentheaderid", paymentid);
                obj.put("scheduledetailid", cursor.getString(1));
                obj.put("payentmethode", cursor.getString(17));
                obj.put("amount", cursor.getString(3));
                obj.put("chequenumber", cursor.getString(4));
                obj.put("ifsc", cursor.getString(5));
                obj.put("deleteflag", cursor.getString(7));
                obj.put("paymentid", cursor.getString(8));
                obj.put("salespersonid", cursor.getString(9));
                obj.put("locationid", cursor.getString(10));
                obj.put("chequedate", cursor.getString(11));
                obj.put("paymentdate", cursor.getString(12));
                obj.put("invoicenumber", cursor.getString(13));
                obj.put("picklistnumber", cursor.getString(14));
                obj.put("latitude", cursor.getString(15));
                obj.put("longitude", cursor.getString(16));
                obj.put("aprovelag", cursor.getString(19));
                obj.put("invoicedata", getinvoicedetailstodisplay(paymentid));
                paymentlist.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return paymentlist;

    }


    // getinvoicedetails
    public JSONArray getinvoicedetailstodisplay(String paymentheaderid)
            throws Exception {

        JSONArray allinvoicedata = new JSONArray();

        String selectQuery = "SELECT " + "INVP." + INVOICEPAYMENT_AMOUNTPAYED
                + ",INVP." + INVOICEPAYMENT_BALANCE + ",INV."
                + INVOICE_TOTTALAMOUNT + ",INV." + INVOICE_STATUS + ",INV."
                + INVOICE_DATE + ",INV." + INVOICE_NO + ",INVP."
                + INVOICEPAYMENT_PKID + ",INV." + INVOICE_PKID + ",INV."
                + INVOICE_BALANCEAMOUNT + ",INV." + INVOICE_CUSTOMERID
                + ",INV." + INVOICE_ID + " FROM " + TABLE_INVOICE_PAYMENTS
                + " INVP Left JOIN " + TABLE_INVOICE + " INV" + " ON " + "INVP."
                + INVOICEPAYMENT_ID + "=" + "INV." + INVOICE_ID + " WHERE "
                + "INVP." + INVOICEPAYMENT_HEADERID + "=" + paymentheaderid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                JSONObject invoiceobject = new JSONObject();
                invoiceobject.put("amountpayed", cursor.getString(0));
                invoiceobject.put("balance", cursor.getString(1));
                invoiceobject.put("invoicetottal", cursor.getString(2));
                invoiceobject.put("status", cursor.getString(3));
                invoiceobject.put("date", cursor.getString(4));
                invoiceobject.put("invoiceno", cursor.getString(5));
                invoiceobject.put("paymentlineid", cursor.getString(6));
                invoiceobject.put("invoicepkid", cursor.getString(7));
                invoiceobject.put("invoicebalance", cursor.getString(8));
                invoiceobject.put("customerid", cursor.getString(9));
                invoiceobject.put("invoiceid", cursor.getString(10));

                allinvoicedata.put(invoiceobject);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return allinvoicedata;
    }


    public void deletpaymentitem(String pkid) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DELETE_FLAG, "1");
        db.update(TABLE_PAYMENTS, values, PAYMENT_PKID + "=?",
                new String[]{pkid});

    }


    public String getreasoncodechoosen(String scheduledetailid) {
        scheduledetailid = "'" + scheduledetailid + "'";
        String reason = "";
        String selectQuery = "SELECT " + STOREVISIT_REASON + " FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_ID + "="
                + scheduledetailid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {

            reason = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return reason;

    }


    // getinvoicedata
    public JSONArray getallinvoicedata(String Scheduledetailid)
            throws JSONException {
        Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, -1);
        DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy");
        String daybefore = dateTimeFormat.format(c.getTime());
        String customerid = getlocationid(Scheduledetailid);
        customerid = "'" + customerid + "'";
        JSONArray allinvoicedata = new JSONArray();

        String selectQuery = "SELECT " + INVOICE_ID + "," + INVOICE_NO + ","
                + INVOICE_CUSTOMERID + "," + INVOICE_TOTTALAMOUNT + ","
                + INVOICE_BALANCEAMOUNT + "," + INVOICE_STATUS + ","
                + INVOICE_DATE + " FROM " + TABLE_INVOICE + " WHERE "
                + INVOICE_CUSTOMERID + "=" + customerid+" AND "+INVOICE_DATE+" NOT LIKE '"+daybefore+"%'";
//                + " AND "
//                + INVOICE_BALANCEAMOUNT + "+0" + ">0";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                JSONObject invoiceobject = new JSONObject();
                invoiceobject.put("invoiceid", cursor.getString(0));
                invoiceobject.put("invoicenumber", cursor.getString(1));
                invoiceobject.put("customerid", cursor.getString(2));
                invoiceobject.put("amount", cursor.getString(3));
                invoiceobject.put("balance", cursor.getString(4));
                invoiceobject.put("status", cursor.getString(5));
                invoiceobject.put("invoicedate", cursor.getString(6));
                allinvoicedata.put(invoiceobject);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return allinvoicedata;
    }


    //=============================delivery payment operations//////////////////
    public void insertpayments(String scheduledetailid, String paymentmethode,
                               String amount, String chequenumber, String ifscno,
                               String createtime, String paymentid, String Checkdate,
                               String paymentdate, String latitude, String longitude,
                               JSONArray invoicedata, String actualmode, String banknamecode,
                               boolean updatebalance) throws JSONException {
        String locationid = getcoustomername(scheduledetailid).getString(
                "locationid");
        String salespersonid = String.valueOf(getsalespersonid());
        JSONObject invoicedat = getinvoiceandpicklistnumber(scheduledetailid);

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PAYMENT_SCHEDULEDETAILID, scheduledetailid);
        values.put(PAYMENT_METHOD, paymentmethode);
        values.put(AMOUNT, amount);
        values.put(CHEQUENUMBER, chequenumber);
        values.put(IFSC, ifscno);
        values.put(PAYMENT_CREATETIME, createtime);
        values.put(DELETE_FLAG, "0");
        values.put(PAYMENTGUID, paymentid);
        values.put(SALESPERSONID, salespersonid);
        values.put(PAYMENT_LOCATIONID, locationid);
        values.put(CHEQE_DATE, Checkdate);
        values.put(PAYMENT_DATE, paymentdate);
        values.put(PAYMENT_INVOCENUMBER, invoicedat.getString("invoicenumber"));
        values.put(PAYMENT_PICKLISTNUMBER,
                invoicedat.getString("picklistnumber"));
        values.put(PAYMENT_LATTITUDE, latitude);
        values.put(PAYMENT_LONGITUDE, longitude);
        values.put(PAYMENT_ACTUALMODE, actualmode);
        values.put(PAYMENT_BANKCODE, banknamecode);
        values.put(PAYMENT_APROOVEFLAG, "0");
        long header = db.insert(TABLE_PAYMENTS, null, values);
        insertintoinvoicepayment(String.valueOf(header), invoicedata);
        if (updatebalance)
            updateinvoicetable(invoicedata);

    }


    // =============update invoice table on submission
    public void updateinvoicetable(JSONArray inviceupdatedata)
            throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        for (int i = 0; i < inviceupdatedata.length(); i++) {
            ContentValues values = new ContentValues();
            JSONObject updateobject = inviceupdatedata.getJSONObject(i);
//            			String invoiceid = updateobject.getString("invoiceid"); 
            String invoiceno = updateobject.getString("invoicenumber");
            values.put(INVOICE_BALANCEAMOUNT, updateobject.getString("balance"));
            values.put(INVOICE_STATUS, updateobject.getString("status"));
            db.update(TABLE_INVOICE, values, INVOICE_NO + "=?",
                    new String[]{invoiceno});

        }
    }

    // insertinto invoicetable on paymentsubmission
    public void insertintoinvoicepayment(String paymentheaderid,
                                         JSONArray invoicepaymentdata) throws JSONException {

        SQLiteDatabase db = this.getWritableDatabase();

        for (int i = 0; i < invoicepaymentdata.length(); i++) {
            JSONObject invoiceobject = invoicepaymentdata.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(INVOICEPAYMENT_HEADERID, paymentheaderid);
            values.put(INVOICEPAYMENT_ID, invoiceobject.optString("invoiceid","0"));
            values.put(INVOICEPAYMENT_INVOICENO, invoiceobject.getString("invoicenumber"));

            values.put(INVOICEPAYMENT_AMOUNTPAYED,
                    invoiceobject.getString("amout"));
            values.put(INVOICEPAYMENT_BALANCE,
                    invoiceobject.getString("balance"));
            values.put(INVOICEPAYMENT_DELETEFLAG, "0");
            long m = db.insert(TABLE_INVOICE_PAYMENTS, null, values);
            String val=Long.toString(m);

        }

    }

    public void updateschedulestatus(String scheduledetailid, String operation)
            throws JSONException {

        // scheduledetailid=","+scheduledetailid+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DETAIL_STATUS, "Completed");
        if (validatesubmissionloaction(scheduledetailid)) {
            values.put(SCHEDULE_DETAIL_VISITED, "1");
            if (!operation.equals("status")) {
                values.put(SCHEDULE_DETAIL_PRODUCTIVE, "1");
            }

        } else if (!operation.equals("status")) {
            values.put(SCHEDULE_DETAIL_PRODUCTIVE, "1");
        }
        db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_ID + "=?",
                new String[]{scheduledetailid});
    }

    // ==================================validate whether gps location is currect or not
    public boolean validatesubmissionloaction(String scheduledetailid)
            throws JSONException {

        JSONObject latdata = getupdatedlatlong(scheduledetailid);
        String store_lat = latdata.optString("latitude_current", "0.0");
        String stor_lng = latdata.optString("longitude_current", "0.0");

        double store_lat_val = 0.0;
        double store_lng_val = 0.0;

        try {
            store_lat_val = Double.parseDouble(store_lat);
            store_lng_val = Double.parseDouble(stor_lng);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //		Vehicletracker tracker = new Vehicletracker();
        PlayLocation tracker = new PlayLocation();
        double current_lat = tracker.getlattitude();
        double current_long = tracker.getlongitude();

        if (store_lat_val == 0 || store_lng_val == 0 || current_lat == 0
                || current_long == 0) {
            return false;
        }

        float distance = distance(current_lat, current_long, store_lat_val, store_lng_val);

        if (distance < 70) {

            return true;
        } else {

            return false;
        }

    }


    public String getlocationid(String scheduledetailid) {

        scheduledetailid = "'" + scheduledetailid + "'";
        String locid = "";
        String selectQuery = "SELECT " + LOCATION_ID + " FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_ID + "="
                + scheduledetailid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                locid = cursor.getString(0);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return locid;
    }


    //==================================getting invoice and picklist number

    public JSONObject getinvoiceandpicklistnumber(String scheduledetailid)
            throws JSONException {
        scheduledetailid = "'" + scheduledetailid + "'";
        JSONObject invoicedata = new JSONObject();

        SQLiteDatabase db = this.getWritableDatabase();
        String selectquery = "SELECT " + PAYMENT_INVOCENUMBER + ","
                + PAYMENT_PICKLISTNUMBER + " FROM " + TABLE_SCHEDULE_DETAIL
                + " WHERE " + SCHEDULE_DETAIL_ID + "=" + scheduledetailid;
        Cursor cursor = db.rawQuery(selectquery, null);

        if (cursor.moveToFirst()) {

            invoicedata.put("invoicenumber", cursor.getString(0));
            invoicedata.put("picklistnumber", cursor.getString(1));

        }
        if (cursor != null)
            cursor.close();
        return invoicedata;


    }

    public String getscheduleheaderid() {

        String scheduleheaderid = "0";

        String selectQuery = "SELECT " + SCHEDULE_HEADER_PKID + " FROM "
                + TABLE_SCHEDULE_HEADER + " ORDER BY " + SCHEDULE_HEADER_PKID
                + " DESC LIMIT 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                scheduleheaderid = cursor.getString(0);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return scheduleheaderid;
    }

    public String getscheduledetailid(String scheduleheaderid) {
        String scheduledetailid = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + SCHEDULE_DETAIL_ID + " from " + TABLE_SCHEDULE_DETAIL + " where " +
                SCHEDULE_HEADER_ID + "= '" + scheduleheaderid + "'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            scheduledetailid = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return scheduledetailid;
    }


    public String billOpenigClosing(String orderby) {
        String invoiceno = " ";
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String currentdate = df.format(c.getTime());
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "SELECT " + INVOICE_NO + " FROM " + TABLE_INVOICE + " WHERE " +
                INVOICE_DATE + " LIKE '" + currentdate + "%' ORDER BY " + INVOICE_NO + " " + orderby + " LIMIT 1";

        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            invoiceno = cursor.getString(0);

        }
        if (cursor != null)
            cursor.close();
        return invoiceno;
    }

    public String getpaymentmodeval(String mode) {
        String totcreditpayment = "";
        SQLiteDatabase db = this.getWritableDatabase();
        //		String sql= "select SUM("+AMOUNT+") from "+TABLE_PAYMENTS+ " where "
        //					+ DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG + "=1 group by "+PAYMENT_METHOD+"='"+mode+"' ";
        String sql = "select SUM(" + AMOUNT + ") from " + TABLE_PAYMENTS + " where "
                + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG + "=1 AND " + PAYMENT_METHOD + "='" + mode + "' ";

        Cursor cursor = db.rawQuery(sql, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            totcreditpayment = cursor.getString(0);
            if (totcreditpayment == null) {
                totcreditpayment = "0.0";
            }


        }
        if (cursor != null)
            cursor.close();
        return totcreditpayment;

    }


    public BigDecimal getTotalExpense() throws JSONException {

        SQLiteDatabase db = this.getWritableDatabase();
//        double expense = 0.0;

        BigDecimal expense = new BigDecimal(0);
        String sql = "select SUM(" + EXPENSE_AMOUNT
                + ") as expense FROM " + TABLE_EXPENSE;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {

            expense =   new BigDecimal(cursor.getString(0) == null ? "0" :cursor.getString(0 ));
//            expense = cursor.getDouble(0);
        }
        if (cursor != null)
            cursor.close();
        db.close();

       /* DecimalFormat twoDForm = new DecimalFormat("#.#");
        expense = Double.valueOf(twoDForm.format(expense));*/
        return expense;


    }


    public HashMap<String, JSONObject> getDailySalesReport() throws Exception {

        HashMap<String, JSONObject> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "Select pt. " + PACKAGE_TYPE_CODE + ", pc." + PRODUCTCAT_NAME + " , sum(ol."
                + ORDERLINE_QTY + ") " + "from " + TABLE_ORDERLINE + " ol "
                + "join " + TABLE_PRODUCT + " p on p." + PRODUCT_ID + " = ol."
                + ORDERLINE_PRODID +
                " join "+TABLE_ORDERHDR+" oh on oh."+ORDERHDR_PKID+" = ol."+ORDERLINE_HEADID+
                " join " + TABLE_PRODUCTCAT + " pc on pc." + PRODUCTCAT_ID + " = p." + PRODUCT_CAT +
                " left join " + TABLE_PACKAGE_TYPE + " pt on pt." + PACKAGE_TYPE_ID + " = ol." + ORDERLINE_PACKAGE_TYPE +
                " where ol." + ORDERLINE_DELETEFLAG + " =0 " +
                " and (oh."+ORDERHDR_APPROVEFLAG+" = '1' OR oh."+ORDERHDR_APPROVEFLAG+" = '-1') and oh."+ORDERHDR_STATUS+" != 'Cancelled'"+
                " group by pc."+PRODUCTCAT_NAME;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject tempObj = new JSONObject();
                tempObj.put("PackageType", cursor.getString(0));
                tempObj.put("Category", cursor.getString(1));
                tempObj.put("Qty", cursor.getString(2));

                temp.put(cursor.getString(1), tempObj);
            } while (cursor.moveToNext());
        }

        if (cursor != null)
            cursor.close();
        return temp;
    }
    //added by jithu on 14/05/2019 for getting sum of total inward quantity
    public double getMatIssued(String orgId,String condition){
        Double totavailableqty = 0.0;
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("M/d/yyyy");
        String daybefore = dateTimeFormat.format(c.getTime());
        SQLiteDatabase db = this.getWritableDatabase();
        String sql="SELECT (SUM(jl."+INVENTORYJRNLLINE_QTYDEBIT +")) as TotalDebit"+
                " FROM " +TABLE_INVENTORYJRNLLINE+ " jl"+
                " JOIN "+TABLE_INVENTORYJRNLHDR+" jh ON jh."+INVENTORYJRNLHDR_ID+" = jl."+INVENTORYJRNLLINE_HDR +
                " JOIN "+TABLE_INVENTORYACCOUNT+" ia ON ia."+INVENTORYACCOUNT_ID+" = jl."+INVENTORYJRNLLINE_ACCOUNT +
                " WHERE  jh."+INVENTORYJRNLHDR_SOURCE+" ='STG'"+
                " AND  ia."+INVENTORYACCOUNT_NAME+" ='Available Stock'"+
                " AND jh."+INVENTORYJRNLHDR_DATE+" LIKE '"+daybefore+"%'"+
                " AND (jh."+INVENTORYJRNLHDR_ORG+" = '" +orgId+ "' OR jh."+INVENTORYJRNLHDR_ORG+" =0)";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                totavailableqty= cursor.getDouble(cursor
                        .getColumnIndex("TotalDebit"));
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return totavailableqty;
    }

    //added by jithu on 21/05/2019 for getting sum of total inward quantity
    public double getTotalOutward(String orgId,String condition){
        Double totavailableqty = 0.0;
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("M/d/yyyy");
        String daybefore = dateTimeFormat.format(c.getTime());
        SQLiteDatabase db = this.getWritableDatabase();
        String sql="SELECT (SUM(jl."+INVENTORYJRNLLINE_QTYCREDIT +")) as TotalDebit"+
                " FROM " +TABLE_INVENTORYJRNLLINE+ " jl"+
                " JOIN "+TABLE_INVENTORYJRNLHDR+" jh ON jh."+INVENTORYJRNLHDR_ID+" = jl."+INVENTORYJRNLLINE_HDR +
                " JOIN "+TABLE_INVENTORYACCOUNT+" ia ON ia."+INVENTORYACCOUNT_ID+" = jl."+INVENTORYJRNLLINE_ACCOUNT +
                " WHERE  jh."+INVENTORYJRNLHDR_SOURCE+" ='STG'"+
                " AND  ia."+INVENTORYACCOUNT_NAME+" ='Available Stock'"+
                " AND jh."+INVENTORYJRNLHDR_DATE+" LIKE '"+daybefore+"%'"+
                " AND (jh."+INVENTORYJRNLHDR_ORG+" = '" +orgId+ "' OR jh."+INVENTORYJRNLHDR_ORG+" =0)";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                totavailableqty= cursor.getDouble(cursor
                        .getColumnIndex("TotalDebit"));
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return totavailableqty;
    }

    //added by jithu on 14/05/2019 for getting sales quantity from inventory
    public double getSalesQty(String orgId) {
        Double totSaleQty = 0.0;
        Calendar c = Calendar.getInstance();
        /*if (condition.equalsIgnoreCase("opstockqty"))*/
        c.add(Calendar.DATE, -0);
        DateFormat dateTimeFormat = new SimpleDateFormat("M/d/yyyy");
        String daybefore = dateTimeFormat.format(c.getTime());
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "SELECT  p." + PRODUCT_NAME + ",  (SUM(jl." + INVENTORYJRNLLINE_QTYDEBIT +
                ")) as debit,(SUM(jl." + INVENTORYJRNLLINE_QTYCREDIT + ") ) AS credit" +
                " FROM " + TABLE_INVENTORYJRNLHDR + " j " + " JOIN " + TABLE_INVENTORYJRNLLINE +
                " jl ON j." + INVENTORYJRNLHDR_ID + " = jl." + INVENTORYJRNLLINE_HDR +
                " JOIN " + TABLE_INVENTORYACCOUNT + " a ON a." + INVENTORYACCOUNT_ID +
                " = jl." + INVENTORYJRNLLINE_ACCOUNT + " Join " + TABLE_PRODUCT + " p on p."
                + PRODUCT_ID + "=   j." + INVENTORYJRNLHDR_PRODUCT +
                " WHERE  a." + INVENTORYACCOUNT_NAME + "='Goods Issued' and ( j." + INVENTORYJRNLHDR_ORG
                + " = '" + orgId + "' OR  j." + INVENTORYJRNLHDR_ORG + " = 0 )"+
                " AND j."+INVENTORYJRNLHDR_SOURCE+" ='INVOICE'"
                + " GROUP BY j." + INVENTORYJRNLHDR_PRODUCT + " ORDER BY p." + PRODUCT_NAME;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                String product = cursor.getString(cursor.getColumnIndex(PRODUCT_NAME));
                String debitQty = cursor.getString(cursor
                        .getColumnIndex("debit"));
                String creditQty = cursor.getString(cursor
                        .getColumnIndex("credit"));


                totSaleQty +=
                        Double.parseDouble(debitQty) - Double.parseDouble(creditQty);
                int i=0;

            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return totSaleQty;
    }

    //Updated By Jithu on 14/05/2019 for getting opening stock with source SYNC
    public double getOpStock(String orgId, String condition) {
        Double totavailableqty = 0.0;
        Calendar c = Calendar.getInstance();
        if (condition.equalsIgnoreCase("opstockqty"))
            c.add(Calendar.DATE, -1);
        DateFormat dateTimeFormat = new SimpleDateFormat("M/d/yyyy");

        String daybefore = dateTimeFormat.format(c.getTime());

        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "SELECT  p." + PRODUCT_NAME + ",  (SUM(jl." + INVENTORYJRNLLINE_QTYDEBIT +
                ")) as debit,(SUM(jl." + INVENTORYJRNLLINE_QTYCREDIT + ") ) AS credit" +
                " FROM " + TABLE_INVENTORYJRNLHDR + " j " + " JOIN " + TABLE_INVENTORYJRNLLINE +
                " jl ON j." + INVENTORYJRNLHDR_ID + " = jl." + INVENTORYJRNLLINE_HDR +
                " JOIN " + TABLE_INVENTORYACCOUNT + " a ON a." + INVENTORYACCOUNT_ID +
                " = jl." + INVENTORYJRNLLINE_ACCOUNT + " Join " + TABLE_PRODUCT + " p on p."
                + PRODUCT_ID + "=   j." + INVENTORYJRNLHDR_PRODUCT +
                " WHERE  a." + INVENTORYACCOUNT_NAME + "='Available Stock' and j." +
                INVENTORYJRNLHDR_DATE + " LIKE '" + daybefore + "%' and ( j." + INVENTORYJRNLHDR_ORG
                + " = '" + orgId + "' OR  j." + INVENTORYJRNLHDR_ORG + " = 0 )"+
                " AND j."+INVENTORYJRNLHDR_SOURCE+" ='SYNC'"
                + " GROUP BY j." + INVENTORYJRNLHDR_PRODUCT + " ORDER BY p." + PRODUCT_NAME;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                String product = cursor.getString(cursor.getColumnIndex(PRODUCT_NAME));
                String debitQty = cursor.getString(cursor
                        .getColumnIndex("debit"));
                String creditQty = cursor.getString(cursor
                        .getColumnIndex("credit"));


                totavailableqty +=
                        Double.parseDouble(debitQty) - Double.parseDouble(creditQty);

            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return totavailableqty;
    }

    public HashMap<String, JSONObject> getvansalesstockreport(String orgId) throws Exception {
        HashMap<String, JSONObject> temp = new HashMap<>();
        int i = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "SELECT u." + UNITS_ABBRV + " as unit , p." + PRODUCT_NAME + ",  (SUM(jl." + INVENTORYJRNLLINE_QTYDEBIT + ")) as debit,(SUM(jl." + INVENTORYJRNLLINE_QTYCREDIT + ") ) AS credit" +
                " FROM " + TABLE_INVENTORYJRNLHDR + " j " +

                " LEFT JOIN " + TABLE_UNITS + " u ON u." + UNITS_ID + " = j." + INVENTORYJRNLHDR_UOM +
                " JOIN " + TABLE_INVENTORYJRNLLINE + " jl ON j." + INVENTORYJRNLHDR_ID + " = jl." + INVENTORYJRNLLINE_HDR +
                " JOIN " + TABLE_INVENTORYACCOUNT + " a ON a." + INVENTORYACCOUNT_ID + " = jl." + INVENTORYJRNLLINE_ACCOUNT

                + " Join " + TABLE_PRODUCT + " p on p." + PRODUCT_ID + "=   j." + INVENTORYJRNLHDR_PRODUCT +
                " WHERE  a." + INVENTORYACCOUNT_NAME + "='Available Stock' " +

                " and ( j." + INVENTORYJRNLHDR_ORG + " = '" + orgId + "' OR  j." + INVENTORYJRNLHDR_ORG + " = 0 )"
                + " GROUP BY j." + INVENTORYJRNLHDR_PRODUCT + " ORDER BY p." + PRODUCT_NAME;

        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                i++;

                String unit = cursor.getString(cursor
                        .getColumnIndex("unit"));

                String debitQty = cursor.getString(cursor
                        .getColumnIndex("debit"));
                String creditQty = cursor.getString(cursor
                        .getColumnIndex("credit"));


                JSONObject tempObj = new JSONObject();
                //				tempObj.put("productname", cursor.getString(0));
                //				tempObj.put("availableqty", cursor.getString(1));
                tempObj.put(
                        "productname",
                        cursor.getString(cursor
                                .getColumnIndex(PRODUCT_NAME)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(PRODUCT_NAME)));
                tempObj.put(
                        "availableqty",
                        Double.parseDouble(debitQty) - Double.parseDouble(creditQty));

                tempObj.put("unit", unit);


                temp.put(String.valueOf(i), tempObj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getNewShops() {
        Calendar c = Calendar.getInstance();

        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMdd");

        String currntime = dateTimeFormat.format(c.getTime());

        String countQuery = "SELECT * FROM " + TABLE_LOCATIONS + " WHERE "
                + LOCATION_CREATETIME + " LIKE '" + currntime + "%'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        String count = String.valueOf(cursor.getCount());
        if (cursor != null)
            cursor.close();

        return count;
    }


    public String getcustomerbranch(String scheduledetailid) {
        String branchid = "";
        String branchname = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String getbranchid = "select " + LOCATION_BRANCH + " from " + TABLE_LOCATIONS + " where " +
                CUSTOMER_SCHEDULELINE_GUID + "= '" + scheduledetailid + "'";
        Cursor cursor = db.rawQuery(getbranchid, null);
        if (cursor.moveToFirst()) {
            branchid = cursor.getString(0);
        }
        String getbranchname = "select " + BRANCHES_NAME + " from " + TABLE_BRANCHES + " where " +
                BRANCHES_ID + "= '" + branchid + "'";
        cursor = db.rawQuery(getbranchname, null);
        if (cursor.moveToFirst()) {
            branchname = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();


        return branchname;
    }


    public String getorderamttot() {
        String temp = "0";
        SQLiteDatabase db = this.getWritableDatabase();
        // String query = "select o." + ORDERLINE_QTY + ",o." + ORDERLINE_RATE
        // + ",mp." + UNITSMAP_UPC + " from " + TABLE_ORDERLINE
        // + " o join " + TABLE_UNITSMAP + " mp on mp." + UNITSMAP_UOM
        // + "=o." + ORDERLINE_UPC + " and mp." + UNITSMAP_PRODUCT + "=o."
        // + ORDERLINE_PRODID + " where o." + ORDERLINE_HEADID + "='"
        // + orderid + "' and " + ORDERLINE_DELETEFLAG + "!='1'";
        // String query = "SELECT SUM(" + ORDERLINE_QTY + "*" + ORDERLINE_RATE
        // + " ) FROM " + TABLE_ORDERLINE + " WHERE " + ORDERLINE_HEADID
        // + "='" + orderid + "' AND " + ORDERLINE_DELETEFLAG + "!='1'";

        String query = "SELECT " + ORDERLINE_QTY + "," + ORDERLINE_RATE + ", "
                + ORDERLINE_PRODID + ", " + ORDERLINE_PACKAGE_TYPE + ", " + ORDERLINE_DISCOUNT + " FROM "
                + TABLE_ORDERLINE + " ol join " + TABLE_ORDERHDR + " oh on oh."
                + ORDERHDR_PKID + "=ol." + ORDERLINE_HEADID + " WHERE ol."
                + ORDERLINE_DELETEFLAG + "!='1' and oh." + ORDERHDR_APPROVEFLAG
                + ">'0'";
        double amt = 0;
        String a = "";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                double qty = cursor.getDouble(0);
                double rate = cursor.getDouble(1);
                String product = cursor.getString(2);
                //				String upc = cursor.getString(3);
                double discountAmnt = 0;
                discountAmnt = cursor.getDouble(4);
                //				double valupc = getupcofprod(db, product, upc);
                //				double total = qty * rate * valupc;

                double total = qty * rate;
                total = total - discountAmnt;
                a += String.valueOf(qty) + "*" + String.valueOf(rate) +
                        "=" + String.valueOf(total)
                        + "\n";
                amt += (total);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        DecimalFormat twoDForm = new DecimalFormat("#.#");
        amt = Double.valueOf(twoDForm.format(amt));
        return String.valueOf(amt);
    }

    //Updated by Jithu for syntax error correction of order by on 15/065/2019
    public JSONObject getRouteAndDistributor() throws Exception {
        JSONObject temp = new JSONObject();
        SQLiteDatabase db = this.getWritableDatabase();
        /*
         * String sql ="SELECT s." + SCHEDULE_ID +
         * ",s."+ROUTENETWORK_ID+",o."+BRANCHES_NAME+" FROM " +
         * TABLE_SCHEDULE_HEADER + " s " +
         * "left join "+TABLE_SCHEDULE_DETAIL+" sd on sd."
         * +SCHEDULE_HEADER_ID+"=s."+SCHEDULE_ID+
         * " left join "+TABLE_LOCATIONS+" l on l."
         * +LOCATION_ID+" = sd."+LOCATION_ID+
         * " left join "+TABLE_BRANCHES+" o on l."
         * +LOCATION_BRANCH+"=o."+BRANCHES_ID+ " ORDER BY " +
         * SCHEDULE_HEADER_PKID + " DESC LIMIT 1" ;
         */

        String sql = "SELECT  o." + BRANCHES_NAME + " FROM " + TABLE_LOCATIONS
                + " l " + " left join " + TABLE_BRANCHES + " o on l."
                + LOCATION_BRANCH + "= o." + BRANCHES_ID

                + "  LIMIT 1";

        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {

                /*
                 * temp.put("ScheduleId", cursor.getString(0));
                 * temp.put("RouteId", cursor.getString(1));
                 */
                temp.put("Distributor", cursor.getString(0));

            } while (cursor.moveToNext());
        }

        if (cursor != null)
            cursor.close();
        return temp;

    }


    public HashMap<String, String> getPackageTypes() {
        HashMap<String, String> temp = new HashMap<String, String>();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + PACKAGE_TYPE_CODE + ", " + PACKAGE_TYPE_ID + " from "
                + TABLE_PACKAGE_TYPE + " order by " + PACKAGE_TYPE_CODE;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put(cursor.getString(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return temp;
    }

    public HashMap<String, String> getBatches() {
        HashMap<String, String> temp = new HashMap<String, String>();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + PRODUCTBATCH_BATCH + ", " + PRODUCTBATCH_ID
                + " from " + TABLE_PRODUCTBATCH + " order by "
                + PRODUCTBATCH_BATCH;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put(cursor.getString(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        return temp;
    }


    public HashMap<String, String> getproductFormMap(String selectedCategory,
                                                     String selectedBrand) {
        HashMap<String, String> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "";
        if (selectedCategory.contentEquals("All")
                && selectedBrand.contentEquals("All"))
            query = "SELECT " + FORM_ID + ", " + FORM_NAME + " FROM "
                    + TABLE_FORM;
        else
            query = "select productform,(select form.name from form where form.formid=productform) as formname from producttable where (productcat='"
                    + selectedCategory
                    + "' or 'All'='"
                    + selectedCategory
                    + "') and (productbrand='"
                    + selectedBrand
                    + "' or 'All'='"
                    + selectedBrand + "') group by producttable.productform";

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put(cursor.getString(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public List<String> getallproductids(String cat, String brand, String form,
                                         String text, int selection) {
        List<String> temp = new ArrayList<String>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "";
        switch (selection) {
            case 0:
                query = "SELECT " + PRODUCT_ID + " FROM " + TABLE_PRODUCT
                        + " WHERE ('" + cat + "'=" + PRODUCT_CAT + " OR '" + cat
                        + "'='All')" + "AND ('" + brand + "'=" + PRODUCT_BRAND
                        + " OR '" + brand + "'='All')" + "AND ('" + form + "'="
                        + PRODUCT_FORM + " OR '" + form + "'='All')" + "AND ("
                        + PRODUCT_NAME + " LIKE '%" + text + "%' OR ''='" + text
                        + "') ORDER BY " + PRODUCT_NAME;
                break;
            default:
                query = "SELECT " + PRODUCT_ID + " FROM " + TABLE_PRODUCT
                        + " ORDER BY " + PRODUCT_NAME;
                break;
        }
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }


    public String getZeroTax() {
        SQLiteDatabase db = this.getWritableDatabase();
        String taxname = "TAX@0";
        String query = "SELECT " + TAX_ID + " FROM " + TABLE_TAX + " WHERE "
                + TAX_NAME + " = '" + taxname + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {

            taxname = cursor.getString(0);
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return taxname;
    }


    // getdenomination data
    public JSONArray getdenominationdata() throws JSONException {

        JSONArray denomdata = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String selectquery = "SELECT " + DENOMINATIONS_CODE + ","
                + DENOMINATIONS_AMOUNT + " FROM " + TABLE_DENOMINATIONS
                + " ORDER BY " + DENOMINATIONS_AMOUNT + "+0" + " DESC";

        Cursor cursor = db.rawQuery(selectquery, null);

        if (cursor.moveToFirst()) {
            do {
                JSONObject denomobj = new JSONObject();
                denomobj.put("id", cursor.getString(0));
                denomobj.put("amount", cursor.getString(1));
                denomdata.put(denomobj);
            } while (cursor.moveToNext());

        }

        if (cursor != null)
            cursor.close();
        return denomdata;

    }
// /get summary data in schedule wise //

    public JSONObject Getschedulesummary() throws JSONException {

        int numberofstores = 0;
        int numberofvisitedstores = 0;
        int numberofproductivestores = 0;
        Double amount_collected = 0.0;
        Double ordeamount_enterred = 0.0;
        int orderstorescount = 0;
        JSONObject summarydata = new JSONObject();

        Double total_cashamount = 0.0;
        Double total_chequeamount = 0.0;
        Double total_chequeamount_pdc = 0.0;
        Double total_return = 0.0;
        Double total_credit = 0.0;
        Double artarget = 0.0;
        String denomdata = "";
        String Schedulestatus = "";

        SQLiteDatabase db = this.getWritableDatabase();

        // total stores
        String getstore_count = "SELECT COUNT(*) FROM " + TABLE_SCHEDULE_DETAIL;
        Cursor cursor = db.rawQuery(getstore_count, null);
        if (cursor.moveToFirst()) {

            numberofstores = cursor.getInt(0);
        }

        // visited stores
        String get_visited_store_count = "SELECT COUNT(*) FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_VISITED
                + "=" + "1";
        cursor = db.rawQuery(get_visited_store_count, null);
        if (cursor.moveToFirst()) {

            numberofvisitedstores = cursor.getInt(0);
        }

        // productivestores
        String get_productive_store_count = "SELECT COUNT(*) FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE "
                + SCHEDULE_DETAIL_PRODUCTIVE + "=" + "1";
        cursor = db.rawQuery(get_productive_store_count, null);
        if (cursor.moveToFirst()) {

            numberofproductivestores = cursor.getInt(0);
        }

        // total collected amount
        String get_tottalcollectedamount = "SELECT SUM(AMOUNT) FROM "
                + TABLE_PAYMENTS + " WHERE " + PAYMENT_METHOD
                + "!= 'Return' AND " + PAYMENT_METHOD + "!='Credit' " + " AND "
                + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG + "=1";
        ;
        cursor = db.rawQuery(get_tottalcollectedamount, null);
        if (cursor.moveToFirst()) {

            amount_collected = cursor.getDouble(0);
        }

        String get_tottal_orderamount = "SELECT SUM(" + ORDERAMOUNTDATA_AMOUNT
                + ") FROM " + TABLE_ORDERAMOUNTDATA;
        cursor = db.rawQuery(get_tottal_orderamount, null);
        if (cursor.moveToFirst()) {

            ordeamount_enterred = cursor.getDouble(0);
        }

        String marketorder_count = "SELECT COUNT(orderamountfromstorpkid) FROM "
                + TABLE_ORDERAMOUNTDATA;
        cursor = db.rawQuery(marketorder_count, null);
        if (cursor.moveToFirst()) {

            orderstorescount = cursor.getInt(0);
        }

        // tottal cash payment

        String get_tottalcollectedamount_cash = "SELECT SUM(AMOUNT) FROM "
                + TABLE_PAYMENTS + " WHERE " + PAYMENT_METHOD + "= 'Cash'"
                + " AND " + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG
                + "=1";
        ;
        cursor = db.rawQuery(get_tottalcollectedamount_cash, null);
        if (cursor.moveToFirst()) {

            total_cashamount = cursor.getDouble(0);
        }

        // tottal cheque amount cdc
        String get_tottalcollectedamount_cheque = "SELECT SUM(AMOUNT) FROM "
                + TABLE_PAYMENTS + " WHERE " + PAYMENT_METHOD + "= 'Cheque'"
                + " AND " + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG
                + "=1 AND " + PAYMENT_ACTUALMODE + "='CDC'";
        ;
        cursor = db.rawQuery(get_tottalcollectedamount_cheque, null);
        if (cursor.moveToFirst()) {

            total_chequeamount = cursor.getDouble(0);
        }

        // tottal cheque amount pdc
        String get_tottalcollectedamount_cheque_pdc = "SELECT SUM(AMOUNT) FROM "
                + TABLE_PAYMENTS
                + " WHERE "
                + PAYMENT_METHOD
                + "= 'Cheque'"
                + " AND "
                + DELETE_FLAG
                + "=0"
                + " AND "
                + PAYMENT_APROOVEFLAG
                + "=1 AND " + PAYMENT_ACTUALMODE + "='PDC'";
        ;
        cursor = db.rawQuery(get_tottalcollectedamount_cheque_pdc, null);
        if (cursor.moveToFirst()) {

            total_chequeamount_pdc = cursor.getDouble(0);
        }

        // total credit amount
        String get_tottalcollectedamount_credit = "SELECT SUM(AMOUNT) FROM "
                + TABLE_PAYMENTS + " WHERE " + PAYMENT_METHOD + "= 'Credit'"
                + " AND " + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG
                + "=1";
        ;
        cursor = db.rawQuery(get_tottalcollectedamount_credit, null);
        if (cursor.moveToFirst()) {

            total_credit = cursor.getDouble(0);
        }

        // tottal return amount
        String get_tottalcollectedamount_Return = "SELECT SUM(AMOUNT) FROM "
                + TABLE_PAYMENTS + " WHERE " + PAYMENT_METHOD + "= 'Return'"
                + " AND " + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG
                + "=1";
        ;
        cursor = db.rawQuery(get_tottalcollectedamount_Return, null);
        if (cursor.moveToFirst()) {

            total_return = cursor.getDouble(0);
        }

        // get schedule denominationdata

        String get_denominationdata = "SELECT " + SCHEDULESUMMARY_DENOMEDATA
                + " FROM " + TABLE_SCHEDULESUMMARY;
        cursor = db.rawQuery(get_denominationdata, null);
        if (cursor.moveToFirst()) {

            denomdata = cursor.getString(0);
        }

        // get schedule status

        String get_schedulestatus = "SELECT " + SCHEDULE_COMPLETION_STATUS
                + " FROM " + TABLE_SCHEDULE_HEADER;
        cursor = db.rawQuery(get_schedulestatus, null);
        if (cursor.moveToFirst()) {

            Schedulestatus = cursor.getString(0);
        }

        // getting AR target for the schedule
        String get_artarget = "select sum (inv.`staticbalance`) from invoice inv join scheduledetail schdtl on  schdtl.`locationid`=inv.`customerid`";

        cursor = db.rawQuery(get_artarget, null);
        if (cursor.moveToFirst()) {

            artarget = cursor.getDouble(0);
        }

        // cheque payments --cheque data
        JSONArray chequedetails = new JSONArray();
        String get_chequedetails = "SELECT sum(amount),chequenumber,ifsc,chequedate,actualmode FROM "
                + TABLE_PAYMENTS
                + " WHERE "
                + PAYMENT_METHOD
                + "= 'Cheque'"
                + " AND "
                + PAYMENT_ACTUALMODE
                + "='CDC' AND "
                + DELETE_FLAG
                + "=0"
                + " AND "
                + PAYMENT_APROOVEFLAG
                + "=1"
                + " group by chequenumber";
        cursor = db.rawQuery(get_chequedetails, null);
        if (cursor.moveToFirst()) {

            do {
                JSONObject dataobj = new JSONObject();
                dataobj.put("amount", cursor.getString(0));
                dataobj.put("chequenumber", cursor.getString(1));
                dataobj.put("bankname", cursor.getString(2));
                dataobj.put("chequedate", cursor.getString(3));
                dataobj.put("chequemode", cursor.getString(4));
                chequedetails.put(dataobj);
            } while (cursor.moveToNext());
        }

        // cheque payments --cheque data_PDC
        JSONArray chequedetails_PDC = new JSONArray();
        String get_chequedetails_PDC = "SELECT sum(amount),chequenumber,ifsc,chequedate,actualmode FROM "
                + TABLE_PAYMENTS
                + " WHERE "
                + PAYMENT_METHOD
                + "= 'Cheque'"
                + " AND "
                + PAYMENT_ACTUALMODE
                + "='PDC' AND "
                + DELETE_FLAG
                + "=0"
                + " AND "
                + PAYMENT_APROOVEFLAG
                + "=1"
                + " group by chequenumber";
        cursor = db.rawQuery(get_chequedetails_PDC, null);
        if (cursor.moveToFirst()) {

            do {
                JSONObject dataobj = new JSONObject();
                dataobj.put("amount", cursor.getString(0));
                dataobj.put("chequenumber", cursor.getString(1));
                dataobj.put("bankname", cursor.getString(2));
                dataobj.put("chequedate", cursor.getString(3));
                dataobj.put("chequemode", cursor.getString(4));
                chequedetails_PDC.put(dataobj);
            } while (cursor.moveToNext());
        }

        // stores with return
        JSONArray Returndata = new JSONArray();
        String get_Returndata = "select sum (pay.amount),schdtl.locationname,schdtl.locationadress,schdtl.customercatogory,schdtl.customercode  from "
                + "payments pay join scheduledetail schdtl on schdtl.scheduledetailid=pay.scheduledetailid where pay.paymentmethode = 'Return' and pay.deleteflag=0 and pay.aprooveflag=1 group by pay.scheduledetailid  ";
        cursor = db.rawQuery(get_Returndata, null);
        if (cursor.moveToFirst()) {

            do {
                JSONObject dataobj = new JSONObject();
                dataobj.put("amount", cursor.getString(0));
                dataobj.put("custname", cursor.getString(1));
                dataobj.put("custadress", cursor.getString(2));
                dataobj.put("custcat", cursor.getString(3));
                dataobj.put("custcode", cursor.getString(4));
                Returndata.put(dataobj);
            } while (cursor.moveToNext());
        }

        // stores with credit
        JSONArray creditdata = new JSONArray();
        String get_creditdata = "select sum (pay.amount),schdtl.locationname,schdtl.locationadress,schdtl.customercatogory,schdtl.customercode  from "
                + "payments pay join scheduledetail schdtl on schdtl.scheduledetailid=pay.scheduledetailid where pay.paymentmethode = 'Credit' and pay.deleteflag=0 and pay.aprooveflag=1 group by pay.scheduledetailid  ";
        cursor = db.rawQuery(get_creditdata, null);
        if (cursor.moveToFirst()) {

            do {
                JSONObject dataobj = new JSONObject();
                dataobj.put("amount", cursor.getString(0));
                dataobj.put("custname", cursor.getString(1));
                dataobj.put("custadress", cursor.getString(2));
                dataobj.put("custcat", cursor.getString(3));
                dataobj.put("custcode", cursor.getString(4));
                creditdata.put(dataobj);
            } while (cursor.moveToNext());
        }

        summarydata.put("creditdata", creditdata);
        summarydata.put("returndata", Returndata);
        summarydata.put("chequescollected", chequedetails);
        summarydata.put("chequescollectedpdc", chequedetails_PDC);
        summarydata.put("artarget", artarget + "");
        summarydata.put("tottalnumberofstores", numberofstores + "");
        summarydata.put("visitedstores", numberofvisitedstores + "");
        summarydata.put("Tottalamountcollected", amount_collected + "");
        summarydata.put("Tottalorderamount", ordeamount_enterred + "");
        summarydata.put("tottalmarketorders", orderstorescount + "");
        summarydata.put("total_cashamount", total_cashamount + "");
        summarydata.put("total_chequeamount", total_chequeamount + "");
        summarydata.put("total_chequeamount_pdc", total_chequeamount_pdc + "");
        summarydata.put("total_return", total_return + "");
        summarydata.put("total_credit", total_credit + "");
        summarydata.put("denomdata", denomdata);
        summarydata.put("schedulestatus", Schedulestatus);
        summarydata.put("productivestores", numberofproductivestores + "");
        if (cursor != null)
            cursor.close();
        return summarydata;

    }

   /* public BigDecimal getTotalTaxableAmount() {
        SQLiteDatabase db = this.getWritableDatabase();
//        double taxableamt = 0.0;
        BigDecimal taxableamt =  new BigDecimal(0);
        String sql = "select SUM(" + ORDERTAXLINE_AMOUNT
                + ") as taxableamt FROM " + TABLE_ORDERTAXLINE +
                " otln JOIN "+TABLE_ORDERHDR+" oh on oh."+ORDERHDR_PKID+" = otln."+ORDERTAXLINE_ORDRDID+
                " where oh."+ORDERHDR_STATUS+" != 'Cancelled' and ( otln."
                + ORDERTAXLINE_CODE + " LIKE 'CGST%' OR otln." +
                ORDERTAXLINE_CODE+" NOT LIKE '%GST%' ) AND otln." + ORDERTAXLINE_DELETEFLAG + " != '1'"
                +"AND (oh."+ORDERHDR_APPROVEFLAG+" = '1' OR oh."+ORDERHDR_APPROVEFLAG+" = '-1')";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
//            taxableamt = cursor.getDouble(0);
            taxableamt = new BigDecimal(cursor.getString(0) == null ? "0" :cursor.getString(0 ));
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return taxableamt;
    }

    public BigDecimal getTotalTaxAmount() {
        SQLiteDatabase db = this.getWritableDatabase();
//        Double taxtot = 0.0;
        BigDecimal taxtot = new BigDecimal(0);
        String sql = "select SUM(" + ORDERTAXLINE_TAXAMOUNT
                + ") as taxamount FROM " + TABLE_ORDERTAXLINE +
                " otln JOIN "+TABLE_ORDERHDR+" oh on oh."+ORDERHDR_PKID+" = otln."+ORDERTAXLINE_ORDRDID+
                " where oh."+ORDERHDR_STATUS+" != 'Cancelled' "+
                //	+ ORDERTAXLINE_ORDRDID + " ='" + orderid + "' and "
                " AND otln." + ORDERTAXLINE_DELETEFLAG + " != '1'"
                +"AND (oh."+ORDERHDR_APPROVEFLAG+" = '1' OR oh."+ORDERHDR_APPROVEFLAG+" ='-1')";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {

//            taxtot = cursor.getDouble(0);
            taxtot = new BigDecimal(cursor.getString(0) == null ? "0" :cursor.getString(0 ));
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return taxtot;
    }*/

    // update schedule status when schedule completed
    public void updatescheduleheaderstatus(String createttime) throws DateError {

        if (!validateCreateTime(createttime)) {
            throw new DateError();
        }

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SCHEDULE_COMPLETION_STATUS, "Completed");
        values.put(SCHEDULE_COMPLETION_TIME, createttime);
        db.update(TABLE_SCHEDULE_HEADER, values, "", new String[]{});

    }

    public void approveAllOrder(String approveTime) throws DateError {

        if (!validateCreateTime(approveTime)) {
            throw new DateError();
        }

        SQLiteDatabase db = this.getWritableDatabase();

        String schLineId = "";
        String sql = "select " + ORDERHDR_SCHLINEID
                + "  FROM " + TABLE_ORDERHDR + " WHERE " + ORDERHDR_APPROVEFLAG + " = '-1' ";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {

            schLineId = cursor.getString(0);

            ContentValues values = new ContentValues();

            values.put(PAYMENT_CREATETIME, approveTime);

            db.update(TABLE_PAYMENTS, values, PAYMENT_SCHEDULEDETAILID + "= ?",
                    new String[]{schLineId});
        }

        if (cursor != null)
            cursor.close();

        ContentValues values = new ContentValues();

        values.put(ORDERHDR_APPROVEFLAG, "1");
        values.put(ORDERHDR_CREATETIME, approveTime);
        db.update(TABLE_ORDERHDR, values, ORDERHDR_APPROVEFLAG + "= ?",
                new String[]{"-1"});


        db.close();
    }


    // get data request for OTP
    public JSONObject get_otp_request_data(String sid) throws JSONException {

        sid = "'" + sid + "'";
        JSONObject data = new JSONObject();
        String selectQuery = "SELECT " + LOCATION_ID + ","
                + SCHEDULEDETAIL_MOBILE + "," + SCHEDULEDETAIL_OTP + " FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_ID + "="
                + sid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            data.put("customerid", cursor.getString(0));
            data.put("mobile", cursor.getString(1));
            data.put("otp", cursor.getString(2));
        }

        if (cursor != null)
            cursor.close();
        return data;
    }

    // update aprrove flag of payments on aproove
    public void aproovepayments(JSONArray aprovepayments, String createtime,
                                String singingPerson, String fileId, String verificationcodeentred)
            throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();

        for (int i = 0; i < aprovepayments.length(); i++) {
            ContentValues values = new ContentValues();
            JSONObject updateobject = aprovepayments.getJSONObject(i);
            String aprooveflag = updateobject.optString("aprovelag", "0");
            if (aprooveflag.equals("1"))
                continue;
            String id = updateobject.getString("paymentheaderid");

            values.put(PAYMENT_APROOVEFLAG, "1");
            values.put(PAYMENT_CREATETIME, createtime);

            values.put(PAYMENT_SIGNING_PERSON, singingPerson);
            values.put(PAYMENT_FILEID, fileId);
            values.put(PAYMENT_VERIFICATIONCODE, verificationcodeentred);
            db.update(TABLE_PAYMENTS, values, PAYMENT_PKID + "=?",
                    new String[]{id});

        }
    }


    public boolean addComplimentaryItems(String OrderId) {

        boolean complimentary = false;

        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select ordl.tax as tax,ordl.lineid as lineid,ordl.qty as qty," +
                "ordl.rate as rate, " +
                "ordl.Product as Product,offer.OffValue as discPer,pc.CatQtySum as CatQtySum " +
                "  from " + TABLE_ORDERHDR + " ord " +
                "join (Select ol." + ORDERLINE_TAX + " , ol." + ORDERLINE_PKID + " as lineid," +
                "ol." + ORDERLINE_HEADID + ",ol." + ORDERLINE_QTY + "," +
                "p." + PRODUCT_CAT + ",p." + PRODUCT_ID + " as Product,ol." + ORDERLINE_RATE + " , "
                + " ol." + ORDERLINE_QTY + " as Total from " + TABLE_ORDERLINE + " ol " +
                "join " + TABLE_PRODUCT + " p on p." + PRODUCT_ID + " = ol." + ORDERLINE_PRODID +
                " where ol." + ORDERLINE_HEADID + "='" + OrderId +
                "' and ol." + ORDERLINE_DELETEFLAG + "!= '1' )ordl on " +
                "ordl." + ORDERLINE_HEADID + " = ord." + ORDERHDR_PKID +
                " join (Select ol." + ORDERLINE_HEADID + ",SUM(ol." + ORDERLINE_QTY + "* 1 ) as CatQtySum " +
                "FROM  " + TABLE_ORDERLINE + " ol " +
                "join " + TABLE_PRODUCT + " p on p." + PRODUCT_ID + " = ol." + ORDERLINE_PRODID +
                " where p." + PRODUCT_CAT + "  in ( Select " + OFFERS_DETAILS_PRODUCTCATEGORY +
                " from " + TABLE_OFFERS_DETAILS +
                " ) and ol." + ORDERLINE_HEADID + " ='" + OrderId + "' and ol." + ORDERLINE_DELETEFLAG + "!= '1'" +
                " group by ol." + ORDERLINE_HEADID + " ) pc on pc." + ORDERLINE_HEADID + " =  ord." + ORDERHDR_PKID +
                " join (SELECT od." + OFFERS_DETAILS_PRODUCTCATEGORY + ",o." + OFFERS_MINQTY + ",o." + OFFERS_OFFVALUE +
                " ,o." + OFFERS_ID + " FROM  " + TABLE_OFFERS_DETAILS + " od " +
                "join " + TABLE_OFFERS + " o on o." + OFFERS_ID + " = od." + OFFERS_DETAILS_OFFER +
                " where  o." + OFFERS_INDIVIDUAL_FLAG + " = 0 AND o." + OFFERS_OFFTYPE + " = 'Free' " +
                //					" group by od."+OFFERS_DETAILS_ID+"  order by o."+OFFERS_MINQTY+"*1 desc limit 1 ) "+
                " group by od." + OFFERS_DETAILS_ID + "   ) " +
                "offer on ( (  offer." + OFFERS_DETAILS_PRODUCTCATEGORY + " = ordl." + PRODUCT_CAT + ") " +
                "and ((offer." + OFFERS_MINQTY + "*1) <= (pc.CatQtySum*1) )" +
                ") where ord." + ORDERHDR_PKID + " ='" + OrderId + "'  group by ordl.lineid ";
        //		 		"having max(offer."+OFFERS_OFFVALUE+"*1)  ";


        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {

            complimentary = true;
        }

        return complimentary;

    }

  /*  public String getComplimentaryOrderId(String schedulelineid, String parentInvoiceNo) {
        String temp = "0";
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + ORDERHDR_PKID + " FROM " + TABLE_ORDERHDR
                + " WHERE " + ORDERHDR_SCHLINEID + "='" + schedulelineid + "' AND " + ORDER_STATUS + " != 'Cancelled' AND " +
                ORDERHDR_CUSTOMER + "='0' AND " + ORDERHDR_PARENT_INVOICENO + " = '" + parentInvoiceNo + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }



    public void updateParentInvoiceNo(String orderId, String invoiceNo) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values = new ContentValues();
        values.put(ORDERHDR_PARENT_INVOICENO, invoiceNo);
        String where = ORDERHDR_PKID + "=?";
        db.update(TABLE_ORDERHDR, values, where,
                new String[]{orderId});

    }*/

    public void updateComplimentaryFlag(String orderId) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ORDERLINE_COMPLIMENT, "1");
        //		values.put(ORDERLINE_CREATETIME, createdtime);
        String where = ORDERLINE_HEADID + "=?";
        String[] whereArgs = new String[]{orderId};
        db.update(TABLE_ORDERLINE, values, where, whereArgs);

    }

    public HashMap<String, String> getComplimentQtyMap(String orderid) {
        HashMap<String, String> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + ORDERLINE_PRODID + ", " + ORDERLINE_QTY
                + " FROM " + TABLE_ORDERLINE + " WHERE " + ORDERLINE_HEADID
                + "='" + orderid + "' AND " + ORDERLINE_DELETEFLAG + "='0'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put(cursor.getString(0) + "_C", cursor.getString(1));
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public HashMap<String, FourStrings> getComplimentOrderLine(String orderid) {
        HashMap<String, FourStrings> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + ORDERLINE_PRODID + ", " + ORDERLINE_QTY
                + ", " + ORDERLINE_MRP + ", " + ORDERLINE_RATE + ", " +
                /*+ ORDERLINE_UOM + ", " + ORDERLINE_UPC*/
                ORDERLINE_PACKAGE_TYPE + ", " + ORDERLINE_TAX
                + ", " + ORDERLINE_DISCOUNT + ", " + ORDERLINE_DISCOUNTPER
                + " FROM " + TABLE_ORDERLINE + "  WHERE " + ORDERLINE_HEADID
                + "='" + orderid + "' AND " + ORDERLINE_DELETEFLAG + "='0'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                FourStrings t = new FourStrings(cursor.getString(1),
                        cursor.getString(2), cursor.getString(3),
                        cursor.getString(4),  //cursor.getString(5),
                        cursor.getString(5), cursor.getString(6), cursor.getString(7));
                temp.put(cursor.getString(0) + "_C", t);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getInvoiceNo(String orderId) {
        String temp = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT ifnull(" + ORDERHDR_INVOICENO + ",'') FROM " + TABLE_ORDERHDR
                + " WHERE " + ORDERHDR_PKID + "='" + orderId + "'  ";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }


    public void deleteComplimentaryInvoiceNotUsed(String orderId) throws DateError {

        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_ORDERHDR, ORDERHDR_PKID + "=?",
                new String[]{orderId});

        db.delete(TABLE_ORDERLINE, ORDERLINE_HEADID + "=?",
                new String[]{orderId});

        db.delete(TABLE_ORDERTAXLINE, ORDERTAXLINE_ORDRDID + "=?",
                new String[]{orderId});

		/*String query = "SELECT " + INVOICENOGEN_CURRVAL +  " FROM " + TABLE_INVOICENOGEN;


		int curr = 0;
		Cursor cursor = db.rawQuery(query, null);
		if (cursor.moveToFirst()) {

			curr = Integer.parseInt(cursor.getString(0));

		}

		curr = curr-1;
		if (cursor != null)
			cursor.close();


		db.close();
		updateinvoicegen(curr+"");*/


    }


    public String getrecentsynctime() {
        String lastsynctime = "0";
        String selectQuery = "SELECT * FROM " + TABLE_SYNCTABLE + " WHERE "
                + TABLEINSYNC + "=" + "'orderdetail'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                lastsynctime = cursor.getString(2);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return lastsynctime;

    }

    public boolean isWithoutInventory() {
        boolean result = false;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String count = "SELECT " + MOBILESERVICE_DISABLE_INVENTORY + " FROM " + TABLE_MOBSERVICES;
            Cursor cursor = db.rawQuery(count, null);

            if (cursor.moveToFirst()) {

                if (Integer.parseInt(cursor.getString(0)) == 1) {
                    result = true;
                }

            }
            if (cursor != null)
                cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void insertInvoice(String customer, String invoiceno, String totinvamt, String pendingamt, String paymentdate, String paidamntval) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues invoicetabledata = new ContentValues();
        invoicetabledata.put(INVOICE_CUSTOMERID, customer);
        invoicetabledata.put(INVOICE_NO, invoiceno);
        invoicetabledata.put(INVOICE_TOTTALAMOUNT, totinvamt);
        invoicetabledata.put(INVOICE_BALANCEAMOUNT, pendingamt);
        invoicetabledata.put(INVOICE_DATE, paymentdate);

        Double balance = 0.0;
        Double totamnt = Double.valueOf(totinvamt);
        Double paidamnt = Double.valueOf(paidamntval);
        String status = "";


        if (balance <= 0) {
            status = "paid";

        } else if (balance == totamnt) {


            status = "not paid";

        } else {
            status = "partiallypaid";
        }
        invoicetabledata.put(INVOICE_STATUS, status);

        long n = db.insert(TABLE_INVOICE, null, invoicetabledata);
        String val=Long.toString(n);
    }


    public String getInvoiceDate(String invNo) throws ParseException {

        String paymentdate = "";
        invNo = "'" + invNo + "'";

        String selectQuery = "SELECT " + INVOICE_DATE + " FROM "
                + TABLE_INVOICE + " WHERE " + INVOICE_NO + "="
                + invNo;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {

            paymentdate = cursor.getString(0);

        }
        if (cursor != null)
            cursor.close();

        return paymentdate;


    }


    public boolean isAnyPreviousPaymentPending(String schLineId) {
        boolean temp = false;
        String lastupdate = getrecentsynctime();
        String selectQuery = "SELECT ol.id FROM " + TABLE_ORDERLINE
                + " ol join " + TABLE_ORDERHDR + " orh on orh." + ORDERHDR_PKID
                + "=ol." + ORDERLINE_HEADID + " WHERE ol."
                + ORDERLINE_CREATETIME + " > " + lastupdate + " AND ol."
                + ORDERLINE_DELETEFLAG + "=0" + " and (orh."
                + ORDERHDR_APPROVEFLAG + "= '0' or orh." + ORDERHDR_APPROVEFLAG
                + " is null) AND   orh." + ORDERHDR_SCHLINEID + " != '" + schLineId + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public double getTargetVal() {
        SQLiteDatabase db = this.getWritableDatabase();
        double amnt = 0.0;
        String sql = "select SUM(" + SCHEDULEDETAIL_TARGET_VALUE
                + ") as target FROM " + TABLE_SCHEDULE_DETAIL + " where "
                + SCHEDULEDETAIL_TARGET_TYPE + " ='Amount'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {

            amnt = cursor.getDouble(0);
            DecimalFormat twoDForm = new DecimalFormat("#.##");
            amnt = Double.parseDouble(twoDForm.format(amnt));
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return amnt;
    }


    // ////////NEW ORDERTAKING CHANGES
    public String getorderid(String schedulelineid) {
        String temp = "0";
        try {

            SQLiteDatabase db = this.getWritableDatabase();
            String query = "SELECT o." + ORDERHDR_PKID + " FROM " + TABLE_ORDERHDR
                    + " o JOIN " + TABLE_ORDERLINE + " ol ON ol." + ORDERLINE_HEADID + " = o." + ORDERHDR_PKID +
                    " WHERE o." + ORDERHDR_SCHLINEID + "='" + schedulelineid + "' AND o." + ORDER_STATUS + " != 'Cancelled' " +
                    "AND ol." + ORDERLINE_COMPLIMENT + " != '1' OR ol." + ORDERLINE_COMPLIMENT + " is null ";
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    temp = cursor.getString(0);
                } while (cursor.moveToNext());
            }
            db.close();
            if (cursor != null)
                cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return temp;
    }

    public String getcustcat(String schedulelineid) {
        String temp = "0";
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + CUSTOMERCATOGORYID + " FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_ID
                + "= '" + schedulelineid + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getcustomerid(String schedulelineid) {
        String temp = "0";
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + LOCATION_ID + " FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_ID
                + "= '" + schedulelineid + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getcusttype(String schedulelineid) {
        String temp = "0";
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + SCHEDULEDETAIL_CUSTTYPE + " FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_ID
                + "= '" + schedulelineid + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getcustdetails(String schedulelineid) {
        String temp = "";
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "SELECT " + LOCATION_NAME + " , " + LOCATION_ADRESS
                + " FROM " + TABLE_SCHEDULE_DETAIL + " WHERE "
                + SCHEDULE_DETAIL_ID + " ='" + schedulelineid + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0) + "\n" + cursor.getString(1);

            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public HashMap<String, String> getqtymap(String orderid) {
        HashMap<String, String> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + ORDERLINE_PRODID + ", " + ORDERLINE_QTY
                + " FROM " + TABLE_ORDERLINE + " WHERE " + ORDERLINE_HEADID
                + "='" + orderid + "' AND " + ORDERLINE_DELETEFLAG + "='0'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put(cursor.getString(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public HashMap<String, JSONArray> getqtybatchmap(String orderid)
            throws JSONException {
        HashMap<String, JSONArray> temp = new HashMap<>();

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + ORDERLINE_PRODID + ", " + ORDERLINE_BATCH
                + ", " + ORDERLINE_QTY + " FROM " + TABLE_ORDERLINE + " WHERE "
                + ORDERLINE_HEADID + "='" + orderid + "' AND "
                + ORDERLINE_DELETEFLAG + "='0'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String productId = cursor.getString(0);
                String batch = cursor.getString(1);
                String qty = cursor.getString(2);

                JSONObject obj = new JSONObject().put("Qty", qty).put("Batch",
                        batch);

                JSONArray array = new JSONArray();
                if (temp.containsKey(productId)) {
                    array = temp.get(productId);
                }

                array.put(obj);

                temp.put(productId, array);

            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public HashMap<String, String> getcasemap(String orderid) {
        HashMap<String, String> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT o." + ORDERLINE_PRODID + ", p." + PACKAGE_TYPE_CODE
                + " FROM " + TABLE_ORDERLINE + " o " +
                "JOIN " + TABLE_PACKAGE_TYPE + " p ON p." + PACKAGE_TYPE_ID + "= o." + ORDERLINE_PACKAGE_TYPE
                + " WHERE o." + ORDERLINE_HEADID
                + "='" + orderid + "' AND o." + ORDERLINE_DELETEFLAG + "='0'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put(cursor.getString(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }


    public String getPackageTypeCode(String packageTypeId) {

        SQLiteDatabase db = this.getWritableDatabase();
        String code = "";
        String query = "SELECT   " + PACKAGE_TYPE_CODE + " FROM " + TABLE_PACKAGE_TYPE + " WHERE "
                + PACKAGE_TYPE_ID + " = '" + packageTypeId + "' ";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {

            code = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        //		db.close();
        return code;

    }

    public JSONObject getBaseUnitQty(String product, String packageTypeId, double packageQty)
            throws JSONException {

        JSONObject returnObject = new JSONObject();
        SQLiteDatabase db = this.getWritableDatabase();
        double conversionFactor = 1;
        String uom = "";
        String query = "SELECT " + PRODUCT_PACKAGE_MAP_CONVERSION_FACTOR + ", " + PRODUCT_PACKAGE_MAP_UOM +
                " FROM " + TABLE_PRODUCT_PACKAGE_MAPPING + " WHERE "
                + PRODUCT_PACKAGE_MAP_PRODUCT + " = " + product + " and " + PRODUCT_PACKAGE_MAP_BASE_UNIT + " = "
                + "'1'  and " + PRODUCT_PACKAGE_MAP_PACKAGE_TYPE + " = '" + packageTypeId + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            conversionFactor = cursor.getDouble(0);
            uom = cursor.getString(1);
        }

        double qty = conversionFactor * packageQty;
        returnObject.put("Qty", qty).put("UOM", uom).put("ConversionFactor", conversionFactor);
        if (cursor != null)
            cursor.close();
        //		db.close();
        return returnObject;
    }

    public boolean isorderapproved(String orderid) {
        boolean temp = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + ORDERHDR_PKID + " from " + TABLE_ORDERHDR
                + " where " + ORDERHDR_PKID + "='" + orderid + "' and "
                + ORDERHDR_APPROVEFLAG + ">'0'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public void approveorder(String scheduleLineId, String approveTime) throws DateError, JSONException {

        if (!validateCreateTime(approveTime)) {
            throw new DateError();
        }

        SQLiteDatabase db = this.getWritableDatabase();

        if (isserviceexist("Van Sales")) {

            if (isWithoutInventory()) {

                ContentValues values = new ContentValues();
                values.put(ORDERHDR_APPROVEFLAG, "1");
                values.put(ORDERHDR_CREATETIME, approveTime);
                db.update(TABLE_ORDERHDR, values, ORDERHDR_SCHLINEID + "=?",
                        new String[]{scheduleLineId});
            } else {

                String schLineId = "";
                String sql = "select " + ORDERHDR_SCHLINEID
                        + "  FROM " + TABLE_ORDERHDR + " WHERE " + ORDERHDR_APPROVEFLAG + " = '-1' ";
                Cursor cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {

                    schLineId = cursor.getString(0);

                    ContentValues values = new ContentValues();

                    values.put(PAYMENT_CREATETIME, approveTime);

                    db.update(TABLE_PAYMENTS, values, PAYMENT_SCHEDULEDETAILID + "= ?",
                            new String[]{schLineId});
                }

                if (cursor != null)
                    cursor.close();

                ContentValues values = new ContentValues();
                values.put(ORDERHDR_APPROVEFLAG, "-1");
                db.update(TABLE_ORDERHDR, values, ORDERHDR_SCHLINEID + "=? and " + ORDERHDR_STATUS + " != 'Cancelled' ",
                        new String[]{scheduleLineId});

                values.put(ORDERHDR_APPROVEFLAG, "1");
                values.put(ORDERHDR_CREATETIME, approveTime);
                db.update(TABLE_ORDERHDR, values, ORDERHDR_SCHLINEID + "!=? and " + ORDERHDR_APPROVEFLAG + " = '-1' ",
                        new String[]{scheduleLineId});

            }

        } else {


            ContentValues values = new ContentValues();
            values.put(ORDERHDR_APPROVEFLAG, "1");
            values.put(ORDERHDR_CREATETIME, approveTime);
            db.update(TABLE_ORDERHDR, values, ORDERHDR_SCHLINEID + "=?",
                    new String[]{scheduleLineId});

        }

        updateschedulestatus(scheduleLineId, "order");

    }


    // validate whether gps location is currect or not
  /*  public boolean validatesubmissionloaction(String scheduledetailid)
            throws JSONException {

        JSONObject latdata = getupdatedlatlong(scheduledetailid);
        String store_lat = latdata.optString("latitude_current", "0.0");
        String stor_lng = latdata.optString("longitude_current", "0.0");

        double store_lat_val = 0.0;
        double store_lng_val = 0.0;

        try {
            store_lat_val = Double.parseDouble(store_lat);
            store_lng_val = Double.parseDouble(stor_lng);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //		Vehicletracker tracker = new Vehicletracker();
        PlayLocation tracker  = new PlayLocation();
        double current_lat = tracker.getlattitude();
        double current_long = tracker.getlongitude();

        if (store_lat_val == 0 || store_lng_val == 0 || current_lat == 0
                || current_long == 0) {
            return false;
        }

        float distance = distance(current_lat, current_long, store_lat_val,
                store_lng_val);

        if (distance < 70) {

            return true;
        }

        else {

            return false;
        }

    }*/

    public JSONObject getupdatedlatlong(String scheduledetailid)
            throws JSONException {
        scheduledetailid = "'" + scheduledetailid + "'";
        JSONObject latlongdata = new JSONObject();
        String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE_DETAIL
                + " WHERE " + SCHEDULE_DETAIL_ID + " = " + scheduledetailid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            latlongdata.put("latitude_new", cursor.getString(11));
            latlongdata.put("longitude_new", cursor.getString(12));
            latlongdata.put("latitude_current", cursor.getString(8));
            latlongdata.put("longitude_current", cursor.getString(9));

        }
        if (cursor != null)
            cursor.close();
        return latlongdata;

    }

    // calculate distance
    public static float distance(double lat1, double lng1, double lat2,
                                 double lng2) {
        double earthRadius = 6371000; // meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

    public boolean validateCreateTime(String createTime) {


        if (checkWithSyncTime) {

            String lastSyncTime = getrecentsynctime();

            SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");

            Date lastSync = new Date();
            Date createDateTime = new Date();
            try {
                lastSync = df.parse(lastSyncTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {
                createDateTime = df.parse(createTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (lastSync.before(createDateTime)) {

                long diff = createDateTime.getTime() - lastSync.getTime();
                long diffInHours = TimeUnit.MILLISECONDS.toHours(diff);

                if (diffInHours > 48) {//two days
                    return false;
                } else {
                    return true;
                }

            } else {
                return false;
            }

            //return lastSync.before(createDateTime);

        } else {

            return true;
        }

    }

    public HashMap<String, Product> getproductmap(String cat,
                                                  String brand, String form, String text, int selection,
                                                  String currentDate, String custCat, String custtype, String customer) {
        HashMap<String, Product> temp = new HashMap<String, Product>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "";
        switch (selection) {
            case 0:
                // query =
                // "select p.productidunique, productname, case when f.product is
                // null then '0' else '1' end as foc, mp.value as mrp, round( case
                // when flatoff>0 then mp.value-(mp.value*flatoff/100) else
                // (mp.value*(100-consumeroff)*(100-round(case when(al.value is
                // null) then 0 else al.value end,2)))/((100+round(case
                // when(py.value is null) then 0 else py.value end ,2))*100) end,2)
                // as rate FROM producttable p left join focusedprods f on
                // f.product=p.productidunique AND f.fromdat<='"
                // date validation removed by ansar

                query = "select p.productidunique,  productname,  "
                        + "case when f.flag is null then '0' else '1' end as foc,  "
                        + "mp.value as mrp, "
					/*+ "round( case when flatoff>0  then mp.value-(mp.value*flatoff/100)  "
					+ "else (mp.value*(100-consumeroff)*(100-round(case when(al.value is null) "
					+ "then 0 else al.value end ,2)))/((100+round(case when(py.value is null) "
					+ "then 0 else py.value end ,2))*100) end,2) as rate ," +*/
                        + "mp.value as rate,"
                        + "py.value as pm,mp."
                        + MRP_BATCH
                        + ", mi.minqty, case when f.flag is null then '0' else '1' end "
                        + ", mp."
                        + MRP_TAX
                        + " as tax"
                        + " FROM producttable"
                        + " p  left join focusedprods f on f.product=p.productidunique"

                        + " and f.custcat='"
                        + custtype
                        + "'"

                        +
                        // " AND f.fromdat<='"+ currentDate+ "' AND f.todat>='"+
                        // currentDate + "'" +// date validation removed by ansar
                        "    left join mrptable mp on mp.product=p.productidunique"
                        +
                        // " and mp.fromdat<='"
                        // + currentDate
                        // + "' and mp.todat>='"
                        // + currentDate
                        // + "'" +// date validation removed by ansar
                        "   left join partymarg py on py.product=mp.product and ( py.cat='"
                        + custCat
                        + "' or py." + PARTYMARG_CUSTOMER + " = '" + customer + "' )"
                        // " and py.fromdat<='"
                        // + currentDate
                        // + "'   and py.todat>='"
                        // + currentDate
                        // + "'"// date validation removed by ansar
                        + " left join addinltable al on al.cat='"
                        + custCat
                        + "'"

                        + " left join "
                        + TABLE_MINQTY
                        + " mi on mi."
                        + MINQTY_PRODUCT
                        + "=p.productidunique and mi."
                        + MINQTY_CUSTCAT
                        + "='"
                        + custtype
                        + "'"

                        +
                        // " and al.fromdat<='"
                        // + currentDate
                        // + "'   and al.todat>='"
                        // + currentDate
                        // + "'" +// date validation removed by ansar
                        "  WHERE ('"
                        + cat
                        + "'=productcat OR '"
                        + cat
                        + "'='All') AND  ('"
                        + brand
                        + "'=productbrand OR '"
                        + brand
                        + "'='All')  AND   ('"
                        + form
                        + "'=productform OR '"
                        + form
                        + "'='All')  AND (productname LIKE '%"
                        + text
                        // + "%' OR ''='" + text + "') ORDER BY " + PRODUCT_NAME;
                        + "%' OR ''='"
                        + text
                        + "') ORDER BY f."
                        + FOCUSEDPRDS_FLAG
                        + " DESC," + PRODUCT_NAME + " ASC";
                // + "%' OR ''='" + text + "') ORDER BY f." + FOCUSEDPRDS_FLAG +
                // " DESC";
                break;

            default:
                break;
        }
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Product p = new Product(
                        cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3) == null ? "0" : cursor.getString(3),
                        cursor.getString(4) == null ? "0" : cursor.getString(4),
                        cursor.getString(5) == null ? "0" : cursor.getString(5),
                        cursor.getString(6) == null ? "0" : cursor.getString(6),
                        cursor.getString(7) == null ? "0" : cursor.getString(7),
                        cursor.getString(8) == null ? "0" : cursor.getString(8),
                        cursor.getString(9) == null ? getZeroTax() : cursor.getString(9));

                temp.put(cursor.getString(0), p);

            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();

        return temp;
    }



   /* public HashMap<String, OrderLine> getSavedOrderLineData(String orderid)
            throws JSONException {
        HashMap<String, OrderLine> temp = new HashMap<>();

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT o." + ORDERLINE_PRODID + ", o." + ORDERLINE_BATCH
                + ", o." + ORDERLINE_QTY + ", p." + PACKAGE_TYPE_CODE+" FROM " + TABLE_ORDERLINE +
                	" o JOIN "+TABLE_PACKAGE_TYPE+" p ON p."+PACKAGE_TYPE_ID+"= o."+ORDERLINE_PACKAGE_TYPE+
               //  " JOIN "+TABLE_Pro+" p ON p."+PACKAGE_TYPE_ID+"= o."+ORDERLINE_PACKAGE_TYPE+
                 " WHERE o."
                + ORDERLINE_HEADID + "='" + orderid + "' AND o."
                + ORDERLINE_DELETEFLAG + "='0'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String productId = cursor.getString(0);
                String batch = cursor.getString(1);
                String qty = cursor.getString(2);
                String packageType = cursor.getString(3);


                JSONObject obj = new JSONObject().put("Qty", qty).put("Batch",
                        batch).put("PackageType",packageType);


                temp.put(productId, obj);

            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }*/

    public String isinvoicenogenerated(String orderid) {
        String invoiceno = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + ORDERHDR_INVOICENO + " from " + TABLE_ORDERHDR
                + " where " + ORDERHDR_PKID + " = " + orderid;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            invoiceno = cursor.getString(cursor.getColumnIndex(ORDERHDR_INVOICENO)) == null ? ""
                    : cursor.getString(cursor.getColumnIndex(ORDERHDR_INVOICENO));
        }
        if (cursor != null)
            cursor.close();
        return invoiceno;
    }


    public double getavailableqty(String product, String orgId, String invoiceNo) {
        Double availabeqty = 0.0;
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "";
        if (invoiceNo.length() == 0) {

            query = "SELECT SUM(" + INVENTORYJRNLLINE_QTYDEBIT + "),SUM("
                    + INVENTORYJRNLLINE_QTYCREDIT + ")  AS QtyAvailable" + " FROM "
                    + TABLE_INVENTORYJRNLHDR + " j " + " JOIN "
                    + TABLE_INVENTORYJRNLLINE + " jl ON j." + INVENTORYJRNLHDR_ID
                    + " = jl." + INVENTORYJRNLLINE_HDR + " JOIN "
                    + TABLE_INVENTORYACCOUNT + " a ON a." + INVENTORYACCOUNT_ID
                    + " = jl." + INVENTORYJRNLLINE_ACCOUNT + " WHERE  a."
                    + INVENTORYACCOUNT_NAME + "='Available Stock' " + " and j."
                    + INVENTORYJRNLHDR_PRODUCT + " = " + product + " and ( j."
                    + INVENTORYJRNLHDR_ORG + " = " + orgId + " OR  j."
                    + INVENTORYJRNLHDR_ORG + " = 0 ) "
                    + " GROUP BY j." + INVENTORYJRNLHDR_PRODUCT;

        } else {

            query = "SELECT SUM(" + INVENTORYJRNLLINE_QTYDEBIT + "),SUM("
                    + INVENTORYJRNLLINE_QTYCREDIT + ")  AS QtyAvailable" + " FROM "
                    + TABLE_INVENTORYJRNLHDR + " j " + " JOIN "
                    + TABLE_INVENTORYJRNLLINE + " jl ON j." + INVENTORYJRNLHDR_ID
                    + " = jl." + INVENTORYJRNLLINE_HDR + " JOIN "
                    + TABLE_INVENTORYACCOUNT + " a ON a." + INVENTORYACCOUNT_ID
                    + " = jl." + INVENTORYJRNLLINE_ACCOUNT + " WHERE  a."
                    + INVENTORYACCOUNT_NAME + "='Available Stock' " + " and j."
                    + INVENTORYJRNLHDR_PRODUCT + " = " + product + " and ( j."
                    + INVENTORYJRNLHDR_ORG + " = " + orgId + " OR  j."
                    + INVENTORYJRNLHDR_ORG + " = 0 ) " +
                    " and ( j." + INVENTORYJRNLHDR_SOURCEID + " != '" + invoiceNo + "' )"
                    + " GROUP BY j." + INVENTORYJRNLHDR_PRODUCT;

        }

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String debitQty = cursor.getString(0);
                String creditQty = cursor.getString(1);

                availabeqty =
                        Double.parseDouble(debitQty)
                                - Double.parseDouble(creditQty);

            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return availabeqty;
    }


    public String getProductName(String key) {
        String temp = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + PRODUCT_NAME + " FROM " + TABLE_PRODUCT
                + " WHERE " + PRODUCT_ID + "='" + key + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    /**
     * Insert Van Sales Order
     */

    public void insertorder(
            String scheduleLineId, String personId, String createdtime,
            HashMap<String, Product> productmap,
            HashMap<String, OrderLine> savedData, HashMap<String, String> packageTypeMap,
            String lat, String lng, String customerId, String parentInvoiceNo,String taxType,
            String parentOrderId)
            throws DateError, JSONException {

        if (!validateCreateTime(createdtime)) {
            throw new DateError();
        }

        //  boolean needinsert = false;

        //   if (needinsert) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        values.put(ORDERHDR_SCHLINEID, scheduleLineId);
        values.put(ORDERHDR_CUSTOMER, customerId);
        values.put(ORDERHDR_PERSONID, personId);
        values.put(ORDERHDR_CREATETIME, createdtime);
        values.put(ORDERHDR_LATITUDE, lat);
        values.put(ORDERHDR_LONGITUDE, lng);
        values.put(ORDERHDR_STATUS, "Approved");
        values.put(ORDERHDR_PARENT_ORDERID, parentOrderId);
//        values.put(ORDERHDR_PARENT_INVOICENO, parentInvoiceNo);
        long headerid = db.insert(TABLE_ORDERHDR, null, values);


        insertorderline(db, createdtime, headerid, productmap,
                savedData, packageTypeMap, scheduleLineId,taxType);

        db.close();
        // }
    }

    private void insertorderline(SQLiteDatabase db,
                                 String createdtime, long headerid,
                                 HashMap<String, Product> productmap,
                                 HashMap<String, OrderLine> savedData,
                                 HashMap<String, String> packageTypeMap, String scheduleLineId,String taxType) throws DateError {


        if (!validateCreateTime(createdtime)) {
            throw new DateError();
        }

        for (Map.Entry<String, OrderLine> entry : savedData.entrySet()) {

            String tempProductId = entry.getKey();
            OrderLine line = entry.getValue();

            if(line.isComplimentFlag()){
                tempProductId =  entry.getKey().split("_")[0];
            }


            final String productId =tempProductId;
            double qty = line.getQty();
            String packageTypeCode = line.getPackageTypeCode();
            if (qty > 0) {

//                double baseunitRate = line.getRate();
                BigDecimal baseunitRate =  line.getRate();
                BigDecimal rate = baseunitRate;
                try {
                    JSONObject baseunitqtyObject = getBaseUnitQty(productId, packageTypeMap.get(packageTypeCode), qty);

                    double conFactor = baseunitqtyObject.optDouble("ConversionFactor", 1);

//                    rate = baseunitRate * conFactor;
                    rate = baseunitRate.multiply(new BigDecimal(conFactor)) ;

                } catch (JSONException e) {

                    e.printStackTrace();
                }
				/*String upc = units.get(casemap.get(key));
				double valupc = getupcofprod(db, key, upc);
				double total = qty * rate * valupc;

				double qtyUpc = 0;
				qtyUpc = qty * valupc;*/

				/*	double discountPer = getDiscountPercentage(key,qtyUpc+"");

				double discountAmnt = 0;

				if(discountPer >0){
					discountAmnt =  (total * discountPer) /100 ;
				}*/
                ContentValues values = new ContentValues();
                values.put(ORDERLINE_CREATETIME, createdtime);
                values.put(ORDERLINE_DELETEFLAG, "0");
                values.put(ORDERLINE_HEADID, String.valueOf(headerid));
                values.put(ORDERLINE_MRP, productmap.get(productId).mrp);
                values.put(ORDERLINE_PRODID, productId);
                values.put(ORDERLINE_QTY, qty);
                values.put(ORDERLINE_RATE, rate.toString());
				/*values.put(ORDERLINE_UOM, casemap.get(key));
				values.put(ORDERLINE_UPC,upc);*/
                values.put(ORDERLINE_PACKAGE_TYPE, packageTypeMap.get(packageTypeCode));

                values.put(ORDERLINE_PM, productmap.get(productId).pm);
                values.put(ORDERLINE_BATCH, productmap.get(productId).batch);
                values.put(ORDERLINE_TAX, productmap.get(productId).tax);
                values.put(ORDERLINE_CANCELFLAG, "0");
                values.put(ORDERLINE_COMPLIMENT, line.isComplimentFlag()?"1":"0");
				/*values.put(ORDERLINE_DISCOUNTPER, discountPer);
				values.put(ORDERLINE_DISCOUNT, discountAmnt);*/
                long id = db.insert(TABLE_ORDERLINE, null, values);
                // insertordertaxline();
            }
        }

        applyDiscountForOrder(headerid + "", scheduleLineId,taxType);

    }

    /**
     *
     *
     * @param OrderId
     * @param scheduleLineId
     * @param taxType
     */
    public void applyDiscountForOrder(String OrderId, String scheduleLineId,String taxType) {


        try {

            String custCategory = getcustcat(scheduleLineId);

            SQLiteDatabase db = this.getWritableDatabase();

            String sql = "select ifnull(py.value,0) as partymargin  ,ordl.tax as tax,ordl.lineid as lineid,ordl.qty as qty," +
                    "ordl.rate as rate, " +
                    "ordl.Product as Product,offer.OffValue as discPer,pc.CatQtySum as CatQtySum " +
                    "  from " + TABLE_ORDERHDR + " ord " +
                    "join (Select ol." + ORDERLINE_TAX + " , ol." + ORDERLINE_PKID + " as lineid," +
                    "ol." + ORDERLINE_HEADID + ",ol." + ORDERLINE_QTY + "," +
                    "p." + PRODUCT_CAT + ",p." + PRODUCT_ID + " as Product,ol." + ORDERLINE_RATE + " , "
                    + " ol." + ORDERLINE_QTY + " as Total from " + TABLE_ORDERLINE + " ol " +
                    "join " + TABLE_PRODUCT + " p on p." + PRODUCT_ID + " = ol." + ORDERLINE_PRODID +
                    " where ol." + ORDERLINE_HEADID + "='" + OrderId +
                    "' and ol." + ORDERLINE_DELETEFLAG + "!= '1' )ordl on " +
                    "ordl." + ORDERLINE_HEADID + " = ord." + ORDERHDR_PKID +
                    " left join (Select pmap." + PRODUCT_PACKAGE_MAP_UOM + " as baseunit, ol." + ORDERLINE_HEADID + ",SUM(ol." + ORDERLINE_QTY + "* pmap." + PRODUCT_PACKAGE_MAP_CONVERSION_FACTOR + "* 1 ) as CatQtySum " +
                    "FROM  " + TABLE_ORDERLINE + " ol " +
                    "join " + TABLE_PRODUCT + " p on p." + PRODUCT_ID + " = ol." + ORDERLINE_PRODID +
                    " join " + TABLE_PRODUCT_PACKAGE_MAPPING + " pmap " +
                    "on pmap." + PRODUCT_PACKAGE_MAP_PRODUCT + " = ol." + ORDERLINE_PRODID +
                    " AND pmap." + PRODUCT_PACKAGE_MAP_PACKAGE_TYPE + " = ol." + ORDERLINE_PACKAGE_TYPE +
                    " AND pmap." + PRODUCT_PACKAGE_MAP_BASE_UNIT + " = '1'" +
                    " where p." + PRODUCT_CAT + "  in ( Select " + OFFERS_DETAILS_PRODUCTCATEGORY +
                    " from " + TABLE_OFFERS_DETAILS +
                    " ) and ol." + ORDERLINE_HEADID + " ='" + OrderId + "' and ol." + ORDERLINE_DELETEFLAG + "!= '1'" +
                    " group by ol." + ORDERLINE_HEADID + " ) pc on pc." + ORDERLINE_HEADID + " =  or" +
                    "d." + ORDERHDR_PKID +
                    " left join (SELECT o." + OFFERS_UOM + ",od." + OFFERS_DETAILS_PRODUCTCATEGORY + ",o." + OFFERS_MINQTY + ",o." + OFFERS_OFFVALUE +
                    " ,o." + OFFERS_ID + " FROM  " + TABLE_OFFERS_DETAILS + " od " +
                    "join " + TABLE_OFFERS + " o on o." + OFFERS_ID + " = od." + OFFERS_DETAILS_OFFER +
                    " where  o." + OFFERS_INDIVIDUAL_FLAG + " = 0 AND o." + OFFERS_OFFTYPE + " = 'Discount' " +
                    " AND ( o." + OFFERS_CUST_CATEGORY + " = '" + custCategory + "' OR o." + OFFERS_CUST_CATEGORY + " = '0' )" +
                    //					" group by od."+OFFERS_DETAILS_ID+"  order by o."+OFFERS_MINQTY+"*1 desc limit 1 ) "+
                    " group by od." + OFFERS_DETAILS_ID + "   ) " +
                    "offer on ( (  offer." + OFFERS_DETAILS_PRODUCTCATEGORY + " = ordl." + PRODUCT_CAT + ") " +
                    "and ((offer." + OFFERS_MINQTY + "*1) <= (pc.CatQtySum*1) )" +
                    "and pc.baseunit = offer." + OFFERS_UOM + " ) " +
                    " left join partymarg py on py.product=ordl.Product and ( py.cat='" + custCategory + "'   )" +
                    " where ord." + ORDERHDR_PKID + " ='" + OrderId + "'  group by ordl.lineid " +
                    "having max(offer." + OFFERS_OFFVALUE + "*1) || py.value > 0 ";


            Cursor cursor = db.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                do {


                    String lineId = cursor.getString(cursor.getColumnIndex("lineid"));

                    //						String headid =cursor.getString(cursor.getColumnIndex("headid"));

                    String CatQtySum = cursor.getString(cursor.getColumnIndex("CatQtySum"));
                    String qty = cursor.getString(cursor.getColumnIndex("qty"));

                    String product = cursor.getString(cursor.getColumnIndex("Product"));
                    String rate = cursor.getString(cursor.getColumnIndex("rate"));
                    //					String upc =cursor.getString(cursor.getColumnIndex("upc"));

                    //					double valupc = getupcofprod(db, product, upc);

                    String discPerString = cursor.getString(cursor.getColumnIndex("discPer"));

                    String tax = cursor.getString(cursor.getColumnIndex("tax"));

                    String partymargin = cursor.getString(cursor.getColumnIndex("partymargin"));

                    /**
                     * Converting double to big decimal to handle rounding issues.
                     */
                  /*  double total = 0;
                    double discPer = 0;
                    double taxable = 0;
                    double taxRate = getTaxRate(tax,taxType );
                    double discount = 0;*/

                    BigDecimal total = new BigDecimal(0);
                    BigDecimal discPer = new BigDecimal(0);
                    BigDecimal taxable = new BigDecimal(0);
                    BigDecimal taxRate = getTaxRate(tax,taxType );
                    BigDecimal discount = new BigDecimal(0);

                    try {
//                        discPer = Double.parseDouble(discPerString)  ;
                        discPer = new BigDecimal(discPerString);
                    } catch (Exception e) {
                        Log.e("Parse Error", e.getMessage());
                    }

                    try {
//                        discPer = discPer+ Double.parseDouble(partymargin);
                        discPer = discPer.add(new BigDecimal(partymargin));

                    } catch (Exception e) {
                        Log.e("Parse Error", e.getMessage());
                    }

                    try {

//                        total = Double.parseDouble(qty) * Double.parseDouble(rate);
                        total = new BigDecimal(qty).multiply(new BigDecimal(rate));
                    } catch (Exception e) {
                        Log.e("Parse Error", e.getMessage());
                    }

//                    taxable = (total * 100) / (100 + taxRate);
                    BigDecimal temp1 = total.multiply(new BigDecimal(100));
                    BigDecimal temp2 = taxRate.add(new BigDecimal(100));

                    taxable =  temp1.divide(temp2,5,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);



                    if (discPer.compareTo(new BigDecimal(0)) == 1 ) {

                      //  discount = (taxable * discPer) / 100;
                        temp1 = taxable.multiply(discPer);
                        discount = temp1.divide(new BigDecimal(100),5,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/) ;

                    }

                    //					double rateAfter =
                    Log.d(" lineId,discPerString", lineId + "-" + discPerString);
                    Log.d(" total,discount ", total + "-" + discount);


                    //					db.beginTransaction();
                    ContentValues values = new ContentValues();
                    values.put(ORDERLINE_DISCOUNTPER, discPer.toString());
                    values.put(ORDERLINE_DISCOUNT, discount.toString());
                    String where = ORDERLINE_PKID + "=?";
                    String[] whereArgs = new String[]{lineId};
                    int i = db.update(TABLE_ORDERLINE, values, where, whereArgs);

					/*	db.setTransactionSuccessful();
					db.endTransaction();*/
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
            //Log.e("Error", e.getMessage());
        }

        //	return discPer;


    }


    public void updateorder(String orderid,
                            String createdtime, HashMap<String, Product> productmap,
                            HashMap<String, OrderLine> saveData, HashMap<String, String> packageTypeMap,
                            String scheduleLineId,String taxType) throws DateError {

        if (!validateCreateTime(createdtime)) {
            throw new DateError();
        }

        SQLiteDatabase db = this.getWritableDatabase();
        for (Map.Entry<String, OrderLine> entry : saveData.entrySet()) {

            String tempProductId = entry.getKey();
            OrderLine line = entry.getValue();

            if(line.isComplimentFlag()){
                tempProductId =  entry.getKey().split("_")[0];
            }


            final String productId =tempProductId;

            double qty = line.getQty();
            String packageTypeCode = line.getPackageTypeCode();


           /* String key = entry.getKey();
            String value = entry.getValue();
            value = value.contentEquals("") ? "0" : value;*/
            String lineid = getlineidofprod(db, orderid, productId);
            if (!lineid.contentEquals("0")) {
                ContentValues values = new ContentValues();
                if (qty > 0) {


                  /*  double baseunitRate = Double.parseDouble(productmap.get(productId).rate);
                    double rate = baseunitRate;*/

                    BigDecimal baseunitRate =  line.getRate();
                    BigDecimal rate = baseunitRate;

                    try {
                        JSONObject baseunitqtyObject = getBaseUnitQty(productId, packageTypeMap.get(packageTypeCode), qty);

                        double conFactor = baseunitqtyObject.optDouble("ConversionFactor", 1);

//                        rate = baseunitRate * conFactor;
                        rate = baseunitRate.multiply(new BigDecimal(conFactor)) ;

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }



                    values.put(ORDERLINE_QTY, qty + "");

                    values.put(ORDERLINE_PACKAGE_TYPE, packageTypeMap.get(line.getPackageTypeCode()));

                    values.put(ORDERLINE_DELETEFLAG, "0");
                    values.put(ORDERLINE_RATE, rate.toString());


                    try {
                        values.put(ORDERLINE_TAX,
                                productmap.get(productId).tax == null ? "0"
                                        : productmap.get(productId).tax);
                    } catch (Exception e) {
                        values.put(ORDERLINE_TAX, "0");
                    }
                } else {
                    String[] orderTaxLineWhereArgs = new String[]{lineid};
                    db.delete(TABLE_ORDERTAXLINE, ORDERTAXLINE_ORDRDETAILID + "=?",
                            orderTaxLineWhereArgs);//Added By Jithu on 07/06/2019 for delete ordertaxline entity when order quantity of an item change to zero
                    values.put(ORDERLINE_DELETEFLAG, "1");
                }
                values.put(ORDERLINE_CREATETIME, createdtime);
                String where = ORDERLINE_PKID + "=?";
                String[] whereArgs = new String[]{lineid};
                int i = db.update(TABLE_ORDERLINE, values, where, whereArgs);

                values = new ContentValues();
                values.put(ORDERHDR_CREATETIME, createdtime);
                where = ORDERHDR_PKID + "=?";
                whereArgs = new String[]{lineid};
                db.update(TABLE_ORDERHDR, values, where, whereArgs);

            } else {
                if (qty > 0) {

                    BigDecimal baseunitRate =  line.getRate();
                    BigDecimal rate = baseunitRate;
                    try {
                        JSONObject baseunitqtyObject = getBaseUnitQty(productId, packageTypeMap.get(packageTypeCode), qty);

                        double conFactor = baseunitqtyObject.optDouble("ConversionFactor", 1);

//                        rate = baseunitRate * conFactor;
                        rate = baseunitRate.multiply(new BigDecimal(conFactor)) ;
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
					/*String upc = units.get(casemap.get(key));
					double valupc = getupcofprod(db, key, upc);
					double total = qty * rate * valupc;


					double qtyUpc = 0;
					qtyUpc = qty * valupc;*/

					/*double discountPer = getDiscountPercentage(key,qtyUpc+"");
					double discountAmnt = 0;
					if(discountPer >0){
						discountAmnt =  (total * discountPer) /100 ;
					}*/

                    ContentValues values = new ContentValues();
                    values.put(ORDERLINE_CREATETIME, createdtime);
                    values.put(ORDERLINE_DELETEFLAG, "0");
                    values.put(ORDERLINE_HEADID, orderid);
                    values.put(ORDERLINE_MRP, productmap.get(productId).mrp);
                    values.put(ORDERLINE_PRODID, productId);
                    values.put(ORDERLINE_QTY, line.getQty());
                    values.put(ORDERLINE_RATE, rate.toString());
					/*values.put(ORDERLINE_UOM, casemap.get(key));
					values.put(ORDERLINE_UPC, units.get(casemap.get(key)));*/
                    values.put(ORDERLINE_PACKAGE_TYPE, packageTypeMap.get(line.getPackageTypeCode()));
                    values.put(ORDERLINE_PM, productmap.get(productId).pm);
                    values.put(ORDERLINE_BATCH, productmap.get(productId).batch);
                    values.put(ORDERLINE_TAX, productmap.get(productId).tax);
                    values.put(ORDERLINE_CANCELFLAG, "0");
                    values.put(ORDERLINE_COMPLIMENT, line.isComplimentFlag()?"1":"0");
					/*	values.put(ORDERLINE_DISCOUNTPER, discountPer);
					values.put(ORDERLINE_DISCOUNT, discountAmnt);*/

                    long id = db.insert(TABLE_ORDERLINE, null, values);
                    values = new ContentValues();
                    values.put(ORDERHDR_CREATETIME, createdtime);
                    String where = ORDERHDR_PKID + "=?";
                    String[] whereArgs = new String[]{lineid};
                    db.update(TABLE_ORDERHDR, values, where, whereArgs);
                }
            }
        }
        db.close();

        applyDiscountForOrder(orderid, scheduleLineId,taxType);
    }


    public BigDecimal getTaxRate(String taxId, String taxType) throws Exception {

        SQLiteDatabase db = this.getWritableDatabase();

        String rate = "0";
        BigDecimal rateDecimal = new BigDecimal(rate);
        String query = "SELECT SUM(" + TAXLINE_RATE
                + ") as rate " + "FROM " + TABLE_TAXLINE + " WHERE "
                + TAXLINE_TAX + " = " + taxId + " and " + TAXLINE_TYPE + " = "
                + "'"+ taxType +"'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {

            rate = cursor.getString(0);
            rateDecimal = new BigDecimal(rate);
        }
        if (cursor != null)
            cursor.close();
        //		db.close();
        return rateDecimal;
    }

    private String getlineidofprod(SQLiteDatabase db, String orderid, String key) {
        String temp = "0";
        String query = "SELECT " + ORDERLINE_PKID + " FROM " + TABLE_ORDERLINE
                + " WHERE " + ORDERLINE_HEADID + "='" + orderid + "' AND "
                + ORDERLINE_PRODID + "='" + key + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String invoicenogenerator(String orderid) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + INVOICENOGEN_CURRVAL + " , "
                + INVOICENOGEN_MAXVAL + " , " + INVOICENOGEN_MINVAL + " , "
                + INVOICENOGEN_ORGCODE + " FROM " + TABLE_INVOICENOGEN;

        String invoiceno = "";
        String currstr = "";
        int curr = 0;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            String currval = cursor.getString(0);
            String maxval = cursor.getString(1);
            String minval = cursor.getString(2);
            String orgcode = cursor.getString(3);
            curr = Integer.parseInt(currval);
            int min = Integer.parseInt(minval);
            //	int max = Integer.parseInt(maxval);
            if (currval != null) {
                //if ((min <= curr) && (max >= curr)) {
                if (min <= curr) {
                    curr = curr + 1;
                } else {
                    curr = min;
                }
            } else {
                curr = min;
            }
            currstr = Integer.toString(curr);
            // left-padding currentval with zeroes to make 6char long
            String currpad = String.format("%6s", currstr).replace(' ', '0');

            invoiceno = orgcode + currpad;
        }

        if (cursor != null)
            cursor.close();

        // db.setTransactionSuccessful();
        // db.endTransaction();
        //  currstr="0001";
//        invoiceno ="L0001";
        db.close();
        updateinvoicegen(currstr);
        updateTableOrderHdr(orderid, invoiceno);
        return invoiceno;

    }


    public void updateinvoicegen(String currval) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "update " + TABLE_INVOICENOGEN + " set "
                + INVOICENOGEN_CURRVAL + " = " + currval;
        db.execSQL(query);

        db.close();
    }


    public void updateTableOrderHdr(String orderid, String invoiceno) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "update " + TABLE_ORDERHDR + " set "
                + ORDERHDR_INVOICENO + " = '" + invoiceno + "' where "
                + ORDERHDR_PKID + " = " + orderid;

        db.execSQL(query);

        db.close();
    }

    public String getgstin(String scheduledetailid) {
        scheduledetailid = "'" + scheduledetailid + "'";
        String gstin = "";
        String selectQuery = "SELECT " + SCHEDULEDETAIL_TINNUMBER + " FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_ID
                + " = " + scheduledetailid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            gstin = cursor.getString(0) == null ? "" : cursor.getString(0);
            // gstin=cursor.getString(0);

        }
        if (cursor != null)
            cursor.close();
        return gstin;

    }

    public void deleteallproducts(String orderid, String createdtime) throws DateError {

        if (!validateCreateTime(createdtime)) {
            throw new DateError();
        }
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ORDERLINE_DELETEFLAG, "1");
        values.put(ORDERLINE_CREATETIME, createdtime);
        String where = ORDERLINE_HEADID + "=?";
        String[] whereArgs = new String[]{orderid};
        db.update(TABLE_ORDERLINE, values, where, whereArgs);
		/*db.delete(TABLE_ORDERTAXLINE, ORDERTAXLINE_ORDRDID + "=?",
				new String[] { orderid });*/


        values = new ContentValues();
        values.put(ORDERTAXLINE_DELETEFLAG, "1");

        String where1 = ORDERTAXLINE_ORDRDID + "=?";
        String[] whereArgs1 = new String[]{orderid};
        db.update(TABLE_ORDERTAXLINE, values, where1, whereArgs1);

        db.close();
    }

    public void cancelInvoice(String orderid, String createdtime, String scheduleDetailId) throws JSONException, DateError {

        if (!validateCreateTime(createdtime)) {
            throw new DateError();
        }
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ORDERLINE_DELETEFLAG, "1");
        values.put(ORDERLINE_CANCELFLAG, "1");
        values.put(ORDERLINE_CREATETIME, createdtime);
        String where = ORDERLINE_HEADID + "=?";
        String[] whereArgs = new String[]{orderid};
        db.update(TABLE_ORDERLINE, values, where, whereArgs);

        values = new ContentValues();

        values.put(ORDERHDR_STATUS, "Cancelled");
        values.put(ORDERHDR_CREATETIME, createdtime);
        where = ORDERHDR_PKID + "=?";
        whereArgs = new String[]{orderid};
        db.update(TABLE_ORDERHDR, values, where, whereArgs);


        values = new ContentValues();

        values.put(ORDERTAXLINE_DELETEFLAG, "1");
        where = ORDERTAXLINE_ORDRDID + "=?";
        whereArgs = new String[]{orderid};
        db.update(TABLE_ORDERTAXLINE, values, where, whereArgs);
        db.close();

        updateschedulestatuspending(scheduleDetailId, "order");
    }

    public void cancelPayment(String scheduleDetailId,  String orderId, String createdtime) throws JSONException, DateError {


        if (!validateCreateTime(createdtime)) {
            throw new DateError();
        }

        String invoiceNo = getInvoiceNo(orderId);
        String complimentOrderId = getComplimentaryOrderId(scheduleDetailId, orderId);
        String complimentInvNo = getInvoiceNo(complimentOrderId);

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ORDERHDR_APPROVEFLAG, "0");
        db.update(TABLE_ORDERHDR, values, ORDERHDR_SCHLINEID + "=?",
                new String[]{scheduleDetailId});

        values = new ContentValues();
        values.put(DELETE_FLAG, "1");
        values.put(PAYMENT_STATUS, "Cancelled");
        values.put(PAYMENT_CREATETIME, createdtime);
        String where = PAYMENT_SCHEDULEDETAILID + "=?";
        String[] whereArgs = new String[]{scheduleDetailId};
        db.update(TABLE_PAYMENTS, values, where, whereArgs);


        values = new ContentValues();

        values.put(INVOICE_STATUS, "Cancelled");
        where = INVOICE_NO + "=?";
        whereArgs = new String[]{invoiceNo};
        db.update(TABLE_INVOICE, values, where, whereArgs);


        values = new ContentValues();

        values.put(INVOICEPAYMENT_DELETEFLAG, "1");
        where = INVOICEPAYMENT_INVOICENO + "=?";
        whereArgs = new String[]{invoiceNo};
        db.update(TABLE_INVOICE_PAYMENTS, values, where, whereArgs);

        if (!isWithoutInventory()) {


            String journalId = "";
		/*	String sql = "SELECT  "+INVENTORYJRNLHDR_ID+" FROM " + TABLE_INVENTORYJRNLHDR
					+" WHERE " +INVENTORYJRNLHDR_SOURCE+"= 'Invoice' and ( "+INVENTORYJRNLHDR_SOURCEID+" = '"+invoiceNo+"' OR "
					+INVENTORYJRNLHDR_SOURCEID+ " = '"+complimentInvNo+"' )";*/

            String sql = "SELECT  " + INVENTORYJRNLHDR_ID + " FROM " + TABLE_INVENTORYJRNLHDR
                    + " WHERE " + INVENTORYJRNLHDR_SOURCE + "= 'INVOICE' " +
                    "and  " + INVENTORYJRNLHDR_SOURCEID + " in ( '" + invoiceNo + "' ,  '" + complimentInvNo + "' )";
            //	" and "+INVENTORYJRNLHDR_SOURCEID+" = '"+invoiceNo+"'";

            Cursor cursor = db.rawQuery(sql, null);
            if (cursor.moveToFirst()) {

                do {

                    journalId = cursor.getString(0);
                    Log.e("111111", "**********************************************************");
                    Log.e("Deleting Journals",journalId+"");
                    Log.e("111111", "**********************************************************");
                    db.delete(TABLE_INVENTORYJRNLHDR, INVENTORYJRNLHDR_ID + "=?",
                            new String[]{journalId});

                    db.delete(TABLE_INVENTORYJRNLLINE, INVENTORYJRNLLINE_HDR + "=?",
                            new String[]{journalId});

                } while (cursor.moveToNext());

            }

		/*	if (journalId.length() >0) {

				db.delete(TABLE_INVENTORYJRNLHDR,  INVENTORYJRNLHDR_ID + "=?",
						new String[] { journalId });

				db.delete(TABLE_INVENTORYJRNLLINE,  INVENTORYJRNLLINE_HDR + "=?",
						new String[] { journalId });
			} */

            if (cursor != null)
                cursor.close();
        }

        db.close();


    }

    public void updateschedulestatuspending(String scheduledetailid, String operation)
            throws JSONException {

        // scheduledetailid=","+scheduledetailid+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DETAIL_STATUS, "Pending");
        if (validatesubmissionloaction(scheduledetailid)) {
            values.put(SCHEDULE_DETAIL_VISITED, "1");
            if (!operation.equals("status")) {
                values.put(SCHEDULE_DETAIL_PRODUCTIVE, "0");
            }

        } else if (!operation.equals("status")) {
            values.put(SCHEDULE_DETAIL_PRODUCTIVE, "0");
        }
        db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_ID + "=?",
                new String[]{scheduledetailid});
    }


    public boolean isPaymentApproved(String scheduleDetailId) {
        boolean temp = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + PAYMENT_PKID + " from " + TABLE_PAYMENTS
                + " where " + PAYMENT_SCHEDULEDETAILID + "='" + scheduleDetailId + "' and "
                + PAYMENT_APROOVEFLAG + "> '0'  AND " + PAYMENT_STATUS + " != 'Cancelled'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public boolean isPaymentCancelled(String scheduleDetailId) {
        boolean temp = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + PAYMENT_PKID + " from " + TABLE_PAYMENTS
                + " where " + PAYMENT_SCHEDULEDETAILID + "='" + scheduleDetailId + "' and "
                /*+  PAYMENT_APROOVEFLAG + "> '0'  AND "*/ + PAYMENT_STATUS + " = 'Cancelled'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public HashMap<String, OrderLine> getSavedOrderLineData(String orderid) {
        HashMap<String, OrderLine> temp = new HashMap<>();

        SQLiteDatabase db = this.getWritableDatabase();


        String query = "select l." + ORDERLINE_PM + ",l." + ORDERLINE_COMPLIMENT + ",l."
                + ORDERLINE_TAX + ",l." + ORDERLINE_PRODID +
                ",p." + PRODUCT_NAME + ",  l."
                + ORDERLINE_QTY + ", l." + ORDERLINE_PACKAGE_TYPE + ", l."
                + ORDERLINE_RATE + ", l." + ORDERLINE_DISCOUNT
                + ", l." + ORDERLINE_DISCOUNTPER
                +  ", otl."
                + ORDERTAXLINE_TAXAMOUNT + " as otherTamnt, otl."
                + ORDERTAXLINE_RATE
                + " as otherTrate, ifnull(ctl." + ORDERTAXLINE_AMOUNT + ",otl."+ORDERTAXLINE_AMOUNT+") as taxableamount, ctl."
                + ORDERTAXLINE_TAXAMOUNT + " as CGSTamnt, stl."
                + ORDERTAXLINE_TAXAMOUNT + " as SGSTamnt, ctl."
                + ORDERTAXLINE_RATE + " as CGSTrate, stl." + ORDERTAXLINE_RATE
                + " as SGSTrate, ( l." + ORDERLINE_RATE + "*" + ORDERLINE_QTY
                + ") AS actualtotal, ifnull((stl." + ORDERTAXLINE_TAXAMOUNT + "+ ctl."
                + ORDERTAXLINE_TAXAMOUNT  + " + ifnull(otl.taxamount,0) )  ,otl." + ORDERTAXLINE_TAXAMOUNT + ") as taxtotal, " +
                "ifnull(( ( stl.taxamount+ ctl.taxamount + ifnull(otl.taxamount,0) )+ ctl.taxableamount ),(otl.taxamount + otl.taxableamount )) as total " +
                " from "
                + TABLE_ORDERLINE
                + " l left join "
                + TABLE_PRODUCT
                + " p on l."
                + ORDERLINE_PRODID
                + "=p."
                + PRODUCT_ID
                // + " left join " + TABLE_UNITS + " u on u." + UNITS_ID + "=l."
                // + ORDERLINE_UOM
                + " left join " + TABLE_ORDERTAXLINE + " ctl on ctl."
                + ORDERTAXLINE_ORDRDETAILID + "=l." + ORDERLINE_PKID
                + " and ctl." + ORDERTAXLINE_CODE + " LIKE '%cgst%' "
                + " left join " + TABLE_ORDERTAXLINE + " otl on otl."
                + ORDERTAXLINE_ORDRDETAILID + "=l." + ORDERLINE_PKID
                + " and otl." + ORDERTAXLINE_CODE + " NOT LIKE '%gst%' " +
                " left JOIN "
                + TABLE_ORDERTAXLINE + " stl on stl."
                + ORDERTAXLINE_ORDRDETAILID + "=l." + ORDERLINE_PKID
                + " and stl." + ORDERTAXLINE_CODE + " LIKE '%sgst%' where l."
                + ORDERLINE_HEADID + "=" + orderid +   " and l."+ORDERLINE_DELETEFLAG+" ='0' " +
                "and l."+ORDERLINE_CANCELFLAG+" ='0' GROUP BY l."
                + ORDERLINE_PRODID;


        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {


                String prdId = cursor.getString(cursor.getColumnIndex(ORDERLINE_PRODID)) == null ? ""
                        : cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PRODID));


                OrderLine orderLine = new OrderLine();
                orderLine.setOrderId(Integer.parseInt(orderid));
                orderLine.setProductId(Integer.parseInt(prdId));
                orderLine.setProductName(cursor.getString(cursor.getColumnIndex(PRODUCT_NAME)) == null ? ""
                        : cursor.getString(cursor
                        .getColumnIndex(PRODUCT_NAME)));
                orderLine.setQty(cursor.getDouble(cursor.getColumnIndex(ORDERLINE_QTY)) == 0 ? 0
                        : cursor.getDouble(cursor
                        .getColumnIndex(ORDERLINE_QTY)));

                boolean compliment = cursor.getDouble(cursor
                        .getColumnIndex(ORDERLINE_COMPLIMENT)) == 1;

                orderLine.setComplimentFlag(compliment);

              /*  orderLine.setRate(cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_RATE)));*/

                orderLine.setRate(new BigDecimal(cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_RATE))));

                String packageTypeId = cursor.getString(cursor.getColumnIndex(ORDERLINE_PACKAGE_TYPE)) == null ? ""
                        : cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PACKAGE_TYPE));

                String packageTypeCode = getPackageTypeCode(packageTypeId);

                orderLine.setPackageTypeCode(packageTypeCode);
                orderLine.setPackageTypeId(Integer.parseInt(packageTypeId));

                if (compliment){

                    prdId = prdId + "_C";

                }


               /* orderLine.setTaxableAmount(cursor.getDouble(cursor
                        .getColumnIndex(ORDERTAXLINE_AMOUNT)));
                orderLine.setCgstTaxAmount(cursor.getDouble(cursor
                        .getColumnIndex("CGSTamnt")));
                orderLine.setSgstTaxAmount(cursor.getDouble(cursor
                        .getColumnIndex("SGSTamnt")));

                orderLine.setOtherTaxAmount(cursor.getDouble(cursor
                        .getColumnIndex("otherTamnt")));*/
               /* double sgstTaxRate = cursor.getDouble(cursor
                        .getColumnIndex("SGSTrate"));*/

                orderLine.setTaxableAmount(
                        cursor.getString(cursor.getColumnIndex(ORDERTAXLINE_AMOUNT)) == null ?
                                new BigDecimal(0)
                                : new BigDecimal(cursor.getString(cursor
                                .getColumnIndex(ORDERTAXLINE_AMOUNT))));
                orderLine.setCgstTaxAmount(
                        cursor.getString(cursor.getColumnIndex("CGSTamnt")) == null ?
                                new BigDecimal(0)
                                : new BigDecimal(cursor.getString(cursor
                                .getColumnIndex("CGSTamnt"))));
                orderLine.setSgstTaxAmount(
                        cursor.getString(cursor.getColumnIndex("SGSTamnt")) == null ?
                                new BigDecimal(0)
                                : new BigDecimal(cursor.getString(cursor
                                .getColumnIndex("SGSTamnt"))));

                orderLine.setOtherTaxAmount(
                        cursor.getString(cursor.getColumnIndex("otherTamnt")) == null ?
                                new BigDecimal(0)
                                : new BigDecimal(cursor.getString(cursor
                                .getColumnIndex("otherTamnt"))));

              /*  double sgstTaxRate = cursor.getString(cursor.getColumnIndex("SGSTrate")) == null ? 0
                        : cursor.getDouble(cursor
                        .getColumnIndex("SGSTrate"));*/
                BigDecimal sgstTaxRate = cursor.getString(cursor.getColumnIndex("SGSTrate")) == null ?
                        new BigDecimal(0)
                        : new BigDecimal(cursor.getString(cursor
                        .getColumnIndex("SGSTrate")));

                BigDecimal cgstTaxRate = cursor.getString(cursor.getColumnIndex("CGSTrate")) == null ?
                        new BigDecimal(0)
                        : new BigDecimal(cursor.getString(cursor
                        .getColumnIndex("CGSTrate")));

                BigDecimal otherTaxRate = cursor.getString(cursor.getColumnIndex("otherTrate")) == null ?
                        new BigDecimal(0)
                        : new BigDecimal(cursor.getString(cursor
                        .getColumnIndex("otherTrate")));

                BigDecimal taxRate = sgstTaxRate.add(cgstTaxRate).add(otherTaxRate);
               /* if(sgstTaxRate.compareTo(new BigDecimal(0)) == 1)
                    orderLine.setTaxRate(sgstTaxRate.multiply(new BigDecimal(2)));
                else
                    orderLine.setTaxRate(
                            cursor.getString(cursor.getColumnIndex("otherTrate")) == null ?
                                    new BigDecimal(0)
                                    : new BigDecimal(cursor.getString(cursor
                                    .getColumnIndex("otherTrate"))));*/
                /*    orderLine.setTaxRate(cursor.getDouble(cursor
                            .getColumnIndex("otherTrate")));*/

                orderLine.setTaxId(cursor.getInt(cursor
                        .getColumnIndex(ORDERLINE_TAX)) );

                orderLine.setTotalAmount(
                        cursor.getString(cursor.getColumnIndex("total")) == null ?
                                new BigDecimal(0)
                                : new BigDecimal(cursor.getString(cursor
                                .getColumnIndex("total"))));

                orderLine.setTotalTaxAmount(
                        cursor.getString(cursor.getColumnIndex("taxtotal")) == null ?
                                new BigDecimal(0)
                                : new BigDecimal(cursor.getString(cursor
                                .getColumnIndex("taxtotal"))));

                orderLine.setDiscountAmount(
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_DISCOUNT)) == null ?
                                new BigDecimal(0)
                                : new BigDecimal(cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DISCOUNT))));

                orderLine.setDiscountPer(
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_DISCOUNTPER)) == null ?
                                new BigDecimal(0)
                                : new BigDecimal(cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DISCOUNTPER))));

               /* orderLine.setTotalAmount(cursor.getDouble(cursor
                        .getColumnIndex("total")));

                orderLine.setTotalTaxAmount(cursor.getDouble(cursor
                        .getColumnIndex("taxtotal")));

                orderLine.setDiscountAmount(cursor.getDouble(cursor
                        .getColumnIndex(ORDERLINE_DISCOUNT)));

                orderLine.setDiscountPer(cursor.getDouble(cursor
                        .getColumnIndex(ORDERLINE_DISCOUNTPER)));*/

                String partymarg = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PM)) == null ? "" : cursor
                        .getString(cursor.getColumnIndex(ORDERLINE_PM));

//                orderLine.setPartyMargin(Double.parseDouble(partymarg));

                orderLine.setPartyMargin(new BigDecimal(partymarg));

                temp.put(prdId, orderLine);

            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getcustName(String schedulelineid) {
        String temp = "";
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "SELECT " + LOCATION_NAME + " FROM " + TABLE_SCHEDULE_DETAIL + " WHERE "
                + SCHEDULE_DETAIL_ID + " ='" + schedulelineid + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);

            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }


    private boolean isOrderTaxlineExist(String lineid, String key) {

        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select * from " + TABLE_ORDERTAXLINE + " where "
                + ORDERTAXLINE_ORDRDETAILID + " ='" + lineid + "' and "
                + ORDERTAXLINE_TAXLINEID + " = '" + key + "'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return result;

    }


    public void insertOrderTaxline(String taxLineId, BigDecimal taxableAmnt,
                                   BigDecimal taxamt, String taxcode, String taxrate, String orderlineid,
                                   String orderid) {
        Boolean check = isOrderTaxlineExist(orderlineid, taxLineId);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ORDERTAXLINE_TAXLINEID, taxLineId);
        values.put(ORDERTAXLINE_AMOUNT, taxableAmnt.toString());
        values.put(ORDERTAXLINE_TAXAMOUNT, taxamt.toString());
        values.put(ORDERTAXLINE_CODE, taxcode);
        values.put(ORDERTAXLINE_RATE, taxrate);
        values.put(ORDERTAXLINE_ORDRDETAILID, orderlineid);
        values.put(ORDERTAXLINE_ORDRDID, orderid);
        values.put(ORDERTAXLINE_DELETEFLAG, "0");
        if (check) {

            long i = db.insert(TABLE_ORDERTAXLINE, null, values);

        } else {

            long j = db.delete(TABLE_ORDERTAXLINE, ORDERTAXLINE_ORDRDETAILID
                    + "=? and " + ORDERTAXLINE_TAXLINEID + "=?", new String[]{
                    orderlineid, taxLineId});
            long i = db.insert(TABLE_ORDERTAXLINE, null, values);
            Log.d("d-insert i=", String.valueOf(i));
        }

    }

    public HashMap<String, JSONObject> getTax(String taxid, String productId,
                                              String orderid,String taxType) throws JSONException {

        HashMap<String, JSONObject> temp = new HashMap<>();

        SQLiteDatabase db = this.getWritableDatabase();
        String lineid = getlineidofprod(db, orderid, productId);
        String query = "SELECT * " + "FROM " + TABLE_TAXLINE + " WHERE "
                + TAXLINE_TAX + " = " + taxid + " and " + TAXLINE_TYPE + " = "
                + "'"+taxType+"'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {

            do {
                String taxlinepkid = cursor.getString(0);
                String taxlineid = cursor.getString(1);
                String taxdescrip = cursor.getString(2);
                String taxrate = cursor.getString(3);
                String taxtype = cursor.getString(4);
                String taxname = cursor.getString(5);

                JSONObject obj = new JSONObject()
                        .put("taxlinepkid", taxlinepkid)
                        .put("taxdescrip", taxdescrip).put("taxrate", taxrate)
                        .put("taxtype", taxtype).put("taxname", taxname)
                        .put("lineid", lineid);

                temp.put(taxlineid, obj);

            } while (cursor.moveToNext());
        }

        db.close();
        if (cursor != null)
            cursor.close();

        return temp;
    }

    public String applyTax (HashMap<String, OrderLine> orderLineHashMap, String orderId,String taxType) throws Exception {

        String returnString = "";
        for (Map.Entry<String, OrderLine> entry : orderLineHashMap.entrySet()) {

            final String productId = entry.getKey();
            OrderLine line = entry.getValue();
            String taxId = line.getTaxId() + "";


           /* double qty = line.getQty();
            double rate = line.getRate();
            double total = rate * qty;
            double totalTaxRate = getTaxRate(taxId,taxType );

            double taxableamnt = (total * 100) / (100 + totalTaxRate);*/

            BigDecimal qty = new BigDecimal(line.getQty());
            BigDecimal rate =   line.getRate();
            BigDecimal total =qty.multiply(rate);
            BigDecimal totalTaxRate = getTaxRate(taxId,taxType );
            BigDecimal taxableamnt = new BigDecimal(0);
            BigDecimal discAmnt = new BigDecimal(0);

            BigDecimal temp1 = total.multiply(new BigDecimal(100));
            BigDecimal temp2 = totalTaxRate.add(new BigDecimal(100));
            taxableamnt =temp1.divide(temp2,5,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);

            /**
             * Discount amount calculation
             */

            BigDecimal discPer =  line.getDiscountPer();


//            double discAmnt = 0;

            if (discPer.compareTo(new BigDecimal(0)) == 1) {

                //Discount amount calculated on taxable amount

                discAmnt = line.getDiscountAmount();

//                taxableamnt = taxableamnt - discAmnt;
                taxableamnt = taxableamnt.subtract(discAmnt) ;
            }

            HashMap<String, JSONObject> taxLineDetails = getTax(taxId, productId, orderId,taxType);

            if (taxLineDetails.size() == 0) {
                returnString = "Tax Not Available for " + line.getProductName();

            }

            for (Map.Entry<String, JSONObject> entry1 : taxLineDetails.entrySet()) {

                String taxLineId = entry1.getKey();
                JSONObject taxLineObject = entry1.getValue();
                String taxrate = taxLineObject.optString("taxrate", "nil");
                String taxdescrip = taxLineObject.optString("taxdescrip", "nil");
//                double taxRate = Double.parseDouble(taxrate);
                BigDecimal taxRate = new BigDecimal(taxrate);
                String taxcode = taxLineObject.optString("taxname", "nil");

                String taxtype = taxLineObject.optString("taxtype", "nil");
                String orderLineId = taxLineObject.optString("lineid", "nil");

//                double taxAmount = taxableamnt * (taxRate / 100);
                temp1 = taxableamnt.multiply(taxRate);
                BigDecimal taxAmount =temp1.divide(new BigDecimal(100),5,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/) ;

              /*  DecimalFormat twoDForm = new DecimalFormat("#.##");
                taxAmount = Double.valueOf(twoDForm.format(taxAmount));*/

                taxAmount = taxAmount.setScale(5,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);

                insertOrderTaxline(taxLineId, taxableamnt, taxAmount, taxcode, taxrate, orderLineId, orderId);

            }


        }

        return returnString;


    }


    public BigDecimal getTotalTaxAmount(String orderid,int precision) {
        SQLiteDatabase db = this.getWritableDatabase();

        BigDecimal taxtot = new BigDecimal(0);
//        double  taxtot = 0.0;
        String sql = "select SUM(" + ORDERTAXLINE_TAXAMOUNT
                + ") as taxamount FROM " + TABLE_ORDERTAXLINE + " where "
                + ORDERTAXLINE_ORDRDID + " ='" + orderid + "' and " + ORDERTAXLINE_DELETEFLAG + " = '0'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {

           /* taxtot = cursor.getDouble(0);
            DecimalFormat twoDForm = new DecimalFormat("#.##");
            taxtot = Double.parseDouble(twoDForm.format(taxtot));*/

            taxtot = new BigDecimal(cursor.getString(0) == null ? "0" :cursor.getString(0 ));
          //  taxtot = taxtot.setScale(precision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return taxtot;
    }

    public BigDecimal getTotalTaxableAmnt(String orderid,int precision) {
        SQLiteDatabase db = this.getWritableDatabase();
//        double taxableamt = 0.0;
        BigDecimal taxableamt = new BigDecimal(0);
        String sql = "select SUM(" + ORDERTAXLINE_AMOUNT
                + ") as taxableamt FROM " + TABLE_ORDERTAXLINE + " where "
                + ORDERTAXLINE_ORDRDID + " ='" + orderid + "' and ("
                + ORDERTAXLINE_CODE + " LIKE 'CGST%' OR "+ORDERTAXLINE_CODE+" LIKE 'VAT%'  ) and "
                + ORDERTAXLINE_DELETEFLAG + " = '0'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
//            taxableamt = cursor.getDouble(0);
            taxableamt = new BigDecimal(cursor.getString(0) == null ? "0" :cursor.getString(0 ));
        //    taxableamt = taxableamt.setScale(precision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return taxableamt;
    }

    public BigDecimal getTotalCgst(String orderid,int precision) {
        SQLiteDatabase db = this.getWritableDatabase();
//        Double cgst = 0.0;
        BigDecimal cgst = new BigDecimal(0);

        String sql = "select SUM(" + ORDERTAXLINE_TAXAMOUNT
                + ") as taxamount FROM " + TABLE_ORDERTAXLINE + " where "
                + ORDERTAXLINE_ORDRDID + " ='" + orderid + "' and "
                + ORDERTAXLINE_CODE + " LIKE 'CGST%'  and " + ORDERTAXLINE_DELETEFLAG + " = '0'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
//            cgst = cursor.getDouble(0);
            cgst = new BigDecimal( cursor.getString(0) == null ? "0" :cursor.getString(0 ) );
      //      cgst = cgst.setScale(precision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return cgst;
    }

    public BigDecimal getTotalSgst(String orderid,int precision) {
        SQLiteDatabase db = this.getWritableDatabase();
//        Double sgst = 0.0;
        BigDecimal sgst = new BigDecimal(0);
        String sql = "select SUM(" + ORDERTAXLINE_TAXAMOUNT
                + ") as taxamount FROM " + TABLE_ORDERTAXLINE + " where "
                + ORDERTAXLINE_ORDRDID + " ='" + orderid + "' and "
                + ORDERTAXLINE_CODE + " LIKE 'SGST%'  and " + ORDERTAXLINE_DELETEFLAG + " = '0'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
//            sgst = cursor.getDouble(0);
            sgst = new BigDecimal(cursor.getString(0) == null ? "0" :cursor.getString(0 ));
          //  sgst = sgst.setScale(precision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return sgst;
    }

    public BigDecimal getTotalDiscountAmount(String orderid,int precision) {
        SQLiteDatabase db = this.getWritableDatabase();
//        Double sgst = 0.0;
        BigDecimal discAmnt = new BigDecimal(0);
        String sql = "select SUM(" + ORDERLINE_DISCOUNT
                + ") as amount FROM " + TABLE_ORDERLINE + " where "
                + ORDERLINE_HEADID + " ='" + orderid + "' and "
                + ORDERLINE_DELETEFLAG + " = '0'";

        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
//            sgst = cursor.getDouble(0);

            discAmnt = new BigDecimal(cursor.getString(0) == null ? "0" :cursor.getString(0 ));
       //   discAmnt = discAmnt.setScale(precision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return discAmnt;
    }

    public List<String> getallproductcats() {
        List<String> temp = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + PRODUCTCAT_ID + " FROM " + TABLE_PRODUCTCAT;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public HashMap<String, String> getproductCatMap() {
        HashMap<String, String> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + PRODUCTCAT_ID + ", " + PRODUCTCAT_NAME
                + " FROM " + TABLE_PRODUCTCAT;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put(cursor.getString(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public List<String> getallproductbrands(String selectedCategory) {
        List<String> temp = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "";
        if (selectedCategory.contentEquals("All"))
            query = "SELECT " + BRAND_ID + " FROM " + TABLE_BRAND;
        else
            query = "select productbrand from producttable where productcat='"
                    + selectedCategory + "' group by producttable.productbrand";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public HashMap<String, String> getproductBrandMap(String selectedCategory) {
        HashMap<String, String> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "";
        if (selectedCategory.contentEquals("All"))
            query = "SELECT " + BRAND_ID + ", " + BRAND_NAME + " FROM "
                    + TABLE_BRAND;
        else
            query = "select productbrand,(select brand.name from brand where brand.brandid=productbrand) as brandname from producttable where productcat='"
                    + selectedCategory
                    + "'  group by producttable.productbrand";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put(cursor.getString(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public List<String> getallproductforms(String selectedCategory,
                                           String selectedBrand) {
        List<String> temp = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "";
        if (selectedCategory.contentEquals("All")
                && selectedBrand.contentEquals("All"))
            query = "SELECT " + FORM_ID + " FROM " + TABLE_FORM;
        else
            query = "select productform from producttable where (productcat='"
                    + selectedCategory + "' or 'All'='" + selectedCategory
                    + "') and (productbrand='" + selectedBrand + "' or 'All'='"
                    + selectedBrand + "') group by producttable.productform";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }


    public void deleteproduct(String orderid, String key, String createdtime, String schedulelineid,
                               boolean compliment, String taxType) throws DateError {

        if (!validateCreateTime(createdtime)) {
            throw new DateError();
        }

        SQLiteDatabase db = this.getWritableDatabase();
        String lineid = getlineidofprod(db, orderid, key);
        if (!lineid.contentEquals("0")) {
            ContentValues values = new ContentValues();
            values.put(ORDERLINE_DELETEFLAG, "1");
            values.put(ORDERLINE_CREATETIME, createdtime);
            String where = ORDERLINE_PKID + "=?";
            String[] whereArgs = new String[]{lineid};
            db.update(TABLE_ORDERLINE, values, where, whereArgs);

            //Bug fix - Missing Invoice if compliment have no products.
            if(!compliment) {
                db.delete(TABLE_ORDERTAXLINE, ORDERTAXLINE_ORDRDETAILID + "=?",
                        whereArgs);

                applyDiscountForOrder(orderid, schedulelineid,taxType);
            }else{

                values = new ContentValues();
                values.put(ORDERTAXLINE_DELETEFLAG, "1");

                  where = ORDERTAXLINE_ORDRDETAILID + "=?";
                 whereArgs = new String[]{lineid};
                db.update(TABLE_ORDERTAXLINE, values, where, whereArgs);
            }
        }
        db.close();
    }

    public void updateproduct(String orderid, String key, String quantity, String rate,
                              String createdtime, String scheduleLineId,boolean compliment,
                              String taxType) throws DateError {

        if (!validateCreateTime(createdtime)) {
            throw new DateError();
        }
        SQLiteDatabase db = this.getWritableDatabase();
        String lineid = getlineidofprod(db, orderid, key);
        if (!lineid.contentEquals("0")) {
            ContentValues values = new ContentValues();
            if (Double.parseDouble(quantity) > 0) {
                values.put(ORDERLINE_DISCOUNT, 0);
                values.put(ORDERLINE_DISCOUNTPER, 0);
                values.put(ORDERLINE_QTY, quantity);
                values.put(ORDERLINE_RATE, rate);
                values.put(ORDERLINE_DELETEFLAG, "0");
            } else
                values.put(ORDERLINE_DELETEFLAG, "1");
            values.put(ORDERLINE_CREATETIME, createdtime);
            String where = ORDERLINE_PKID + "=?";
            String[] whereArgs = new String[]{lineid};
            db.update(TABLE_ORDERLINE, values, where, whereArgs);
           /* db.delete(TABLE_ORDERTAXLINE, ORDERTAXLINE_ORDRDETAILID + "=?",
                    whereArgs);
            applyDiscountForOrder(orderid, scheduleLineId);*/

           //Bug fix - Missing Invoice if compliment have no products.
            if(!compliment) {
                db.delete(TABLE_ORDERTAXLINE, ORDERTAXLINE_ORDRDETAILID + "=?",
                        whereArgs);

                applyDiscountForOrder(orderid, scheduleLineId,taxType);
            }else{

                values = new ContentValues();
                values.put(ORDERTAXLINE_DELETEFLAG, "1");

                where = ORDERTAXLINE_ORDRDETAILID + "=?";
                whereArgs = new String[]{lineid};
                db.update(TABLE_ORDERTAXLINE, values, where, whereArgs);
            }
        }
        db.close();
    }

    public JSONArray getlocationdata() throws Exception {

        String lastsynctime = lastlocatiosynctime();
        JSONArray loclist = new JSONArray();

        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_LOCATIONS + " WHERE "
                + LOCATION_CREATETIME + ">" + lastsynctime;
        ;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {

                JSONObject obj = new JSONObject();

                obj.put("Locationid", cursor.getString(1));
                obj.put("Locationname", cursor.getString(2));
                obj.put("Locationadress", cursor.getString(3));
                obj.put("Latitude", cursor.getString(4));
                obj.put("Longitude", cursor.getString(5));
                obj.put("CustCode", cursor.getString(7));
                obj.put("Customercategory", cursor.getString(8));
                obj.put("Tinnumber", cursor.getString(9));
                obj.put("nearestlandmark", cursor.getString(10));
                obj.put("contactpersonname", cursor.getString(11));
                obj.put("contactnumber", cursor.getString(12));
                obj.put("anyothercompany", cursor.getString(13));
                obj.put("townclass", cursor.getString(14));
                obj.put("selectedproductcategories", cursor.getString(15));
                obj.put("customercategorycode", cursor.getString(16));
                obj.put("schlineguid", cursor.getString(17));
                obj.put("Salespersonid", getsalespersonid());
                obj.put("Pan",
                        cursor.getString(cursor.getColumnIndex(LOCATION_PAN)));
                obj.put("category",
                        getProductCats(db, cursor.getString(1), lastsynctime));
                obj.put("document",
                        getDocuments(db, cursor.getString(1), lastsynctime));
                obj.put("email",
                        cursor.getString(cursor.getColumnIndex(LOCATION_EMAIL)));
                obj.put("photouuid", cursor.getString(cursor
                        .getColumnIndex(LOCATION_PHOTOUUID)));
                obj.put("directcov", cursor.getString(cursor
                        .getColumnIndex(LOCATION_DIRECTCOV)));
                obj.put("locprovider", cursor.getString(cursor
                        .getColumnIndex(LOCATION_LOCPROVIDER)));
                obj.put("locaccuracy", cursor.getString(cursor
                        .getColumnIndex(LOCATION_LOCACCURACY)));
                obj.put("otpverified", cursor.getString(cursor
                        .getColumnIndex(LOCATION_OTPVERIFIED)));
                obj.put("closingday", cursor.getString(cursor
                        .getColumnIndex(LOCATION_CLOSING_DAY)));
                obj.put("storeclosed", cursor.getString(cursor
                        .getColumnIndex(LOCATION_STOREISCLOSED)));
                obj.put("pngcovered", cursor.getString(cursor
                        .getColumnIndex(LOCATION_PNGCOVERED)));
                obj.put("branch", cursor.getString(cursor
                        .getColumnIndex(LOCATION_BRANCH)));

                obj.put("drugLicienceNo",
                        cursor.getString(cursor.getColumnIndex(LOCATION_DRUG)));
                obj.put("locality", cursor.getString(cursor
                        .getColumnIndex(LOCATION_LOCALITY)));
                obj.put("city",
                        cursor.getString(cursor.getColumnIndex(LOCATION_CITY)));
                obj.put("state",
                        cursor.getString(cursor.getColumnIndex(LOCATION_STATE)));
                obj.put("pin",
                        cursor.getString(cursor.getColumnIndex(LOCATION_PIN)));
                obj.put("coverageDay", cursor.getString(cursor
                        .getColumnIndex(LOCATION_COVDAY)));
                obj.put("week1",
                        cursor.getString(cursor.getColumnIndex(LOCATION_WEEK1)));
                obj.put("week2",
                        cursor.getString(cursor.getColumnIndex(LOCATION_WEEK2)));
                obj.put("week3",
                        cursor.getString(cursor.getColumnIndex(LOCATION_WEEK3)));
                obj.put("week4",
                        cursor.getString(cursor.getColumnIndex(LOCATION_WEEK4)));
                obj.put("visitFrequency", cursor.getString(cursor
                        .getColumnIndex(LOCATION_VISTFRQ)));
                obj.put("type",
                        cursor.getString(cursor.getColumnIndex(LOCATION_TYPE)));
                obj.put("wholeSale", cursor.getString(cursor
                        .getColumnIndex(LOCATION_WHOLSAL)));
                obj.put("metro",
                        cursor.getString(cursor.getColumnIndex(LOCATION_METRO)));
                obj.put("classification", cursor.getString(cursor
                        .getColumnIndex(LOCATION_CLASSIF)));

                obj.put("latitudePhoto", cursor.getString(cursor
                        .getColumnIndex(LOCATION_LATITUDEPHOTO)));
                obj.put("longitudePhoto", cursor.getString(cursor
                        .getColumnIndex(LOCATION_LONGITUDEPHOTO)));
                obj.put("remarks", cursor.getString(cursor
                        .getColumnIndex(LOCATION_REMARKS)));
                obj.put("SchDate", cursor.getString(cursor
                        .getColumnIndex(LOCATION_CREATETIME)));

                obj.put("GSTType", cursor.getString(cursor
                        .getColumnIndex(LOCATION_GSTTYPE)));
                obj.put("FSSINumber", cursor.getString(cursor
                        .getColumnIndex(LOCATION_FSSI)));
                loclist.put(obj);

            } while (cursor.moveToNext());

        }
        for (int i = 0; i < loclist.length(); i++) {
            String filename = loclist.getJSONObject(i).getString("Locationid")
                    + ".png";
            loclist.getJSONObject(i).put("idproof",
                    readFromFile("idproof_" + filename));// .replace("\n",
            // "").replace("\r",
            // ""));
            loclist.getJSONObject(i).put("photo",
                    readFromFile("photo_" + filename));
            loclist.getJSONObject(i).put("sales",
                    readFromFile("sales_" + filename));
        }
        if (cursor != null)
            cursor.close();
        return loclist;

    }

    private JSONArray getDocuments(SQLiteDatabase db, String string,
                                   String lastsynctime) throws JSONException {
        JSONArray temp = new JSONArray();
        String sql = "Select " + DOCUMENTPHOTOS_DOCUMENT + ","
                + DOCUMENTPHOTOS_UUID + " from " + TABLE_DOCUMENTPHOTOS
                + " where " + DOCUMENTPHOTOS_CUSTID + "='" + string + "'  AND "
                + DOCUMENTPHOTOS_CREATETIME + ">" + lastsynctime + " AND "
                + DOCUMENTPHOTOS_UPDATEFLAG + "!='1'";
        ;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("name", cursor.getString(0));
                obj.put("uuid", cursor.getString(1));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    private JSONArray getProductCats(SQLiteDatabase db, String string,
                                     String lastsynctime) throws JSONException {
        JSONArray temp = new JSONArray();
        String sql = "Select " + CATEGORYPHOTOS_CATEGORY + ","
                + CATEGORYPHOTOS_UUID + " from " + TABLE_CATEGORYPHOTOS
                + " where " + CATEGORYPHOTOS_CUSTID + "='" + string + "' AND "
                + CATEGORYPHOTOS_CREATETIME + ">" + lastsynctime + " AND "
                + CATEGORYPHOTOS_UPDATEFLAG + "!='1'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("name", cursor.getString(0));
                obj.put("uuid", cursor.getString(1));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    // /////////reading customer proof image from local file
    private String readFromFile(String filename) {

        String stringifiedbitmap = "";
        try {
            File rootsd = Environment.getExternalStorageDirectory();
            String path = rootsd.getAbsolutePath() + "/Dhirouter/" + filename;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(path, options);

            stringifiedbitmap = getStringFromBitmap(bitmap);
        } catch (Exception e) {
            stringifiedbitmap = "";
            e.printStackTrace();

        }
        return stringifiedbitmap;
        // Bitmap decodedbitmap=getBitmapFromString(stringifiedbitmap);

    }

    // convert bitmap image to string
    private String getStringFromBitmap(Bitmap bitmapPicture) {
        /*
         * This functions converts Bitmap picture to a string which can be
         * JSONified.
         */
        final int COMPRESSION_QUALITY = 75;
        String encodedImage;
        ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
        bitmapPicture.compress(Bitmap.CompressFormat.JPEG, COMPRESSION_QUALITY,
                byteArrayBitmapStream);
        byte[] b = byteArrayBitmapStream.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    // get unvisited stores to sync
    public JSONArray getunvisitedstores(boolean backup) throws JSONException {

        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }


        //		String lastupdate = getrecentsynctime();
        String salespersonid = String.valueOf(getsalespersonid());
        JSONArray unvisitedstores = new JSONArray();
        try {
            String selectQuery = new StringBuilder().append("SELECT ")
                    .append(STOREVISIT_REASON).append(",").append(STOREVISIT_TIME).append(",")
                    .append(STOREVISIT_LATTITUDE).append(",").append(STOREVISIT_LONGITUDE).append(",")
                    .append(SCHEDULE_DETAIL_ID).append(",").append(LOCATION_ID).append(",")
                    .append(SCHEDULEDETAIL_LATTITUDE).append(",").append(SCHEDULEDETAIL_LONGITUDE)
                    .append(" FROM ").append(TABLE_SCHEDULE_DETAIL).append(" WHERE ")
                    .append(SCHEDULEDETAIL_CREATETIME).append(" > ")
                    .append(lastupdate).toString();


            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    JSONObject obj = new JSONObject();
                    String reason = cursor.getString(0);
                    obj.put("reason", cursor.getString(0));
                    obj.put("visittime", cursor.getString(1));
                    obj.put("lattitude", cursor.getString(2));
                    obj.put("longitude", cursor.getString(3));
                    obj.put("scheduledetailid", cursor.getString(4));
                    obj.put("customerid", cursor.getString(5));
                    obj.put("personid", salespersonid);
                    String lat = cursor.getString(cursor
                            .getColumnIndex(SCHEDULEDETAIL_LATTITUDE));
                    String lng = cursor.getString(cursor
                            .getColumnIndex(SCHEDULEDETAIL_LONGITUDE));

                    obj.put("reasonlatitude", lat == null ? "" : lat);
                    obj.put("reasonlongitude", lng == null ? "" : lng);

                    if (reason != null && !reason.equals("null")
                            && !reason.equals("")) {
                        unvisitedstores.put(obj);
                    }

                } while (cursor.moveToNext());

            }
            if (cursor != null)
                cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return unvisitedstores;
    }

    // /get the last synctime for location
    public String lastlocatiosynctime() throws Exception {

        String lastsynctime = "0";
        String selectQuery = "SELECT * FROM " + TABLE_SYNCTABLE + " WHERE "
                + TABLEINSYNC + "=" + "'loactiontable'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                lastsynctime = cursor.getString(2);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return lastsynctime;
    }

    // ///getsalesperson name
    public int getsalespersonid() {

        int salepersonid = 0;
        String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE_HEADER;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String salepid = cursor.getString(3);
                salepersonid = Integer.parseInt(salepid);
                return salepersonid;

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return salepersonid;

    }


    // get closing day update data for sync
    // get unvisited stores to sync
    public JSONArray getclosingdayupdates(boolean backup) throws JSONException {


        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }


        //		String lastupdate = getrecentsynctime();
        String salespersonid = String.valueOf(getsalespersonid());
        JSONArray closingdayupdates = new JSONArray();
        String selectQuery = new StringBuilder().append("SELECT ").append(CLOSING_DAY).append(",").append(SCHEDULE_DETAIL_ID).append(",").append(LOCATION_ID).append(" FROM ").append(TABLE_SCHEDULE_DETAIL).append(" WHERE ").append(CLOSING_DAY_UPDATETIME).append(" > ").append(lastupdate).toString();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("closingday", cursor.getString(0));
                obj.put("scheduledetailid", cursor.getString(1));
                obj.put("customerid", cursor.getString(2));
                obj.put("personid", salespersonid);
                closingdayupdates.put(obj);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return closingdayupdates;
    }


    // get all denomination data to sync
    public JSONArray getdenominationdatatatosync(boolean backup) throws JSONException {
        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }

        //		String lastsyc = getrecentsynctime();

        JSONArray denomdata = new JSONArray();

        String selectQuery = new StringBuilder().append("SELECT ").append(SCHEDULESUMMARY_SUBMITTIME).append(",").append(SCHEDULESUMMARY_PERSONID).append(",").append(SCHEDULESUMMARY_LAT).append(",").append(SCHEDULESUMMARY_LONGI).append(",").append(SCHEDULESUMMARY_UNIQUEID).append(",").append(SCHEDULESUMMARY_DENOMEDATA).append(",").append(SCHEDULESUMMARY_TOTTALCASH).append(",").append(SCHEDULESUMMARY_SCHEDULEID).append(",").append(SCHEDULESUMMARY_COMPLETION_TIME).append(",").append(SCHEDULESUMMARY_BEGIN_TIME).append(" FROM ").append(TABLE_SCHEDULESUMMARY).append(" WHERE ").append(SCHEDULESUMMARY_CREATETIME).append(" > ").append(lastsyc).toString();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();

                obj.put("Submittime", cursor.getString(0));
                obj.put("personid", cursor.getString(1));
                obj.put("lattitude", cursor.getString(2));
                obj.put("longitude", cursor.getString(3));
                obj.put("uniqueid", cursor.getString(4));
                obj.put("denomedata", cursor.getString(5));
                obj.put("totalcash", cursor.getString(6));
                obj.put("scheduleid", cursor.getString(7));
                obj.put("completiontime", cursor.getString(8));
                obj.put("begintime", cursor.getString(9));

                denomdata.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return denomdata;

    }

    public JSONArray getupdatecustomerstosync(boolean backup) throws JSONException {

        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        //		String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = new StringBuilder().append("select * from ").append(TABLE_SCHEDULE_DETAIL).append(" where ").append(SCHEDULEDETAIL_EDITTIME).append(">").append(lastupdate).toString();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("detailid", cursor.getString(cursor
                        .getColumnIndex(SCHEDULE_DETAIL_ID)));
                obj.put("Address", cursor.getString(cursor
                        .getColumnIndex(LOCATION_ADRESS)));
                obj.put("Phone", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_MOBILE)));
                obj.put("Gst", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_TINNUMBER)));
                obj.put("Pan", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PAN)));
                obj.put("PhotoUUID", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PHOTOUUID)));
                obj.put("ImgLat", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_IMGLAT)));
                obj.put("ImgLong", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_IMGLONG)));
                obj.put("landmark", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_LANDMARK)));
                obj.put("town", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_TOWN)));
                obj.put("contactperson", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_CONTACTPERSON)));
                obj.put("othercompany", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_OTHERCOMPANY)));
                obj.put("closingday",
                        cursor.getString(cursor.getColumnIndex(CLOSING_DAY)));
                obj.put("selectedproductcat", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_SELPRODCAT)));
                obj.put("AlternateCustCode", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_ALTERCUSTCODE)));

                obj.put("GSTType", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_GSTTYPE)));
                obj.put("FSSINumber", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_FSSI)));


                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONArray getphotostosync(boolean backup) throws JSONException {

        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        //		String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = new StringBuilder().append("select * from ").append(TABLE_DOCUMENTPHOTOS).append(" where ").append(DOCUMENTPHOTOS_CREATETIME).append(">").append(lastupdate).append(" and ").append(DOCUMENTPHOTOS_UPDATEFLAG).append("='1'").toString();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("type", "document");
                obj.put("slineid", cursor.getString(cursor
                        .getColumnIndex(DOCUMENTPHOTOS_CUSTID)));
                obj.put("name", cursor.getString(cursor
                        .getColumnIndex(DOCUMENTPHOTOS_DOCUMENT)));
                obj.put("catid", "");
                obj.put("uuid", cursor.getString(cursor
                        .getColumnIndex(DOCUMENTPHOTOS_UUID)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        sql = new StringBuilder().append("select * from ").append(TABLE_CATEGORYPHOTOS).append(" where ").append(CATEGORYPHOTOS_CREATETIME).append(">").append(lastupdate).append(" and ").append(CATEGORYPHOTOS_UPDATEFLAG).append("='1'").toString();
        cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("type", "category");
                obj.put("slineid", cursor.getString(cursor
                        .getColumnIndex(CATEGORYPHOTOS_CUSTID)));
                String cat = cursor.getString(cursor
                        .getColumnIndex(CATEGORYPHOTOS_CATEGORY));
                obj.put("name", cat);
                String catid = getchecklistids(cat, db);
                obj.put("catid", catid);
                obj.put("uuid", cursor.getString(cursor
                        .getColumnIndex(CATEGORYPHOTOS_UUID)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getchecklistids(String names, SQLiteDatabase db) {
        String temp = "";
        String selectQuery = "SELECT " + PRODUCTCHECKLIST_CODE + " FROM "
                + TABLE_PRODUCTCHECKLIST + " WHERE " + PRODUCTCHECKLIST_NAME
                + "='" + names + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            temp = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONArray getcratestosync(boolean backup) throws JSONException {

        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        //		String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = new StringBuilder().append("select * from ").append(TABLE_CRATES).append(" where ").append(CRATES_CRTIMESYNC).append(">").append(lastupdate).toString();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("detailid",
                        cursor.getString(cursor.getColumnIndex(CRATES_SLINEID)));
                obj.put("inval",
                        cursor.getString(cursor.getColumnIndex(CRATES_IN)));
                obj.put("outval",
                        cursor.getString(cursor.getColumnIndex(CRATES_OUT)));
                obj.put("crtime",
                        cursor.getString(cursor.getColumnIndex(CRATES_CRTIME)));
                obj.put("uuid",
                        cursor.getString(cursor.getColumnIndex(CREATES_UUID)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONArray getpassbyupdatedsync(boolean backup) throws JSONException {

        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        //		String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = new StringBuilder().append("select ").append(SCHEDULE_DETAIL_ID).append(",").append(LOCATION_ID).append(" from ").append(TABLE_SCHEDULE_DETAIL).append(" where ").append(SCHEDULEDETAIL_PASSBYCRTIME).append(">").append(lastupdate).toString();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("detailid", cursor.getString(cursor
                        .getColumnIndex(SCHEDULE_DETAIL_ID)));
                obj.put("custid",
                        cursor.getString(cursor.getColumnIndex(LOCATION_ID)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public HashMap<String, FourStrings> getExpensesForSync(boolean backup) {


        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }

        HashMap<String, FourStrings> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();

        //		String lastsyc = getrecentsynctime();
        String sql = "select e." + EXPENSE_CREATETIME + ",e." + EXPENSE_EXPENSETYPE + ",e." + EXPENSE_AMOUNT + ",e." +
                EXPENSE_DESCRIPTION + ",e." + EXPENSE_PKID + ",et." + EXPENSETYPE_NAME + " from " + TABLE_EXPENSE + " e " +
                "JOIN " + TABLE_EXPENSETYPE + " et ON et." + EXPENSETYPE_ID + " = e." + EXPENSE_EXPENSETYPE
                + " WHERE e." + EXPENSE_CREATETIME + ">'" + lastsyc + "'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                String c = cursor.getString(cursor.getColumnIndex(EXPENSE_CREATETIME));
                FourStrings strings =
                        new FourStrings(cursor.getString(cursor.getColumnIndex(EXPENSE_EXPENSETYPE)),
                                cursor.getString(cursor.getColumnIndex(EXPENSE_AMOUNT)),
                                cursor.getString(cursor.getColumnIndex(EXPENSE_DESCRIPTION)) == null ? ""
                                        : cursor.getString(cursor.getColumnIndex(EXPENSE_DESCRIPTION)),
                                //	cursor.getString(cursor.getColumnIndex(EXPENSE_DESCRIPTION)),
                                cursor.getString(cursor.getColumnIndex(EXPENSETYPE_NAME)),
                                cursor.getString(cursor.getColumnIndex(EXPENSE_CREATETIME)) == null ? ""
                                        : cursor.getString(cursor.getColumnIndex(EXPENSE_CREATETIME)), "", "");
                temp.put(cursor.getString(cursor.getColumnIndex(EXPENSE_PKID)), strings);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;

    }

    public JSONArray getExpenseDataForSync(boolean backup) {


        String expenseDt = "";
        JSONArray expensesArray = new JSONArray();


        try {

            HashMap<String, FourStrings> expHashMap = getExpensesForSync(backup);

            if (expHashMap.size() > 0) {

                JSONObject expenses = new JSONObject();
                expenses.put("PaymentMode", "Cash Payment");

                String scheduleId = getactualscheduleid();
                expenses.put("scheduleId", scheduleId);
                JSONArray expenseDtls = new JSONArray();

                for (Map.Entry<String, FourStrings> entry : expHashMap.entrySet()) {

                    String expenseId = entry.getKey();
                    FourStrings value = entry.getValue();

                    JSONObject expObject = new JSONObject()
                            .put("amount", value.mrp)
                            .put("expenseTypeId", value.qty)
                            .put("description", value.rate);
					/*if (value.upc == null) {

					}*/
                    expenseDt = value.taxid;
                    expenseDtls.put(expObject);
                }

                expenses.put("expenseDtls", expenseDtls).put("expenseDate", expenseDt);
                expensesArray.put(expenses);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }
        return expensesArray;

    }

    public JSONArray getvansalesdatatosync(boolean backup) throws JSONException {


        String lastsyc = getrecentsynctime();

        JSONArray orderdata = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "SELECT hrd." + ORDERHDR_CREATETIME + " as hdrtime,hrd.approveflag ," +
                "hrd." + ORDERHDR_PARENT_INVOICENO + ", hrd." + ORDERHDR_STATUS +
                ", oln.*,hrd." + ORDERHDR_SCHLINEID + ", hrd."
                + ORDERHDR_CUSTOMER + ", hrd." + ORDERHDR_INVOICENO + ", hrd."
                + ORDERHDR_LATITUDE + ", hrd." + ORDERHDR_LONGITUDE + " , hrd."
                + ORDERHDR_NOTE + ",SUM(tl." + ORDERTAXLINE_TAXAMOUNT
                + ") as taxamount" + ",SUM(tl." + ORDERTAXLINE_RATE
                + ") as taxrate,tl." + ORDERTAXLINE_AMOUNT
                + " as taxableamount, (SUM(tl." + ORDERTAXLINE_TAXAMOUNT
                + ")+tl." + ORDERTAXLINE_AMOUNT + ") as totalamount, " + "oln."
                + ORDERLINE_TAX + " FROM " + TABLE_ORDERLINE + " oln JOIN "
                + TABLE_ORDERHDR + " hrd on hrd." + ORDERHDR_PKID + "=oln."
                + ORDERLINE_HEADID + " JOIN " + TABLE_SCHEDULE_DETAIL
                + " sln on hrd." + ORDERHDR_SCHLINEID + "=sln."
                + SCHEDULE_DETAIL_ID + " JOIN " + TABLE_ORDERTAXLINE
                + " tl on oln." + ORDERLINE_PKID + "=tl."
                + ORDERTAXLINE_ORDRDETAILID + " WHERE (oln."
                + ORDERLINE_CREATETIME + " >'" + lastsyc + "' OR hrd." + ORDERHDR_CREATETIME + " > '" + lastsyc + "' )"
                //+" AND (hrd." + ORDERHDR_APPROVEFLAG + ">'0' )"
                + " AND ( hrd." + ORDERHDR_APPROVEFLAG + ">'0' " +
                "OR ( hrd." + ORDERHDR_STATUS + "= 'Cancelled' " +
                "AND ( hrd." + ORDERHDR_APPROVEFLAG + " = '0' OR hrd." + ORDERHDR_APPROVEFLAG + " is null ) ) )"
                + " AND  ( hrd." + ORDERHDR_INVOICENO + " is not null OR hrd."+ ORDERHDR_INVOICENO + " = '' )"
            //    +"AND tl."+ORDERTAXLINE_DELETEFLAG + " = '0'"
                + " group by oln." + ORDERLINE_PKID;

		/*String query ="SELECT   oln.*,hrd."+ORDERHDR_CREATETIME+" as hdrtime,hrd."+ORDERHDR_APPROVEFLAG+" FROM   tableorderline oln " +
				"JOIN tableorderhdr hrd ON hrd.id = oln.headid WHERE  " +
				"( oln.creattime > '" + lastsyc + "' OR hrd."+ORDERHDR_CREATETIME+"  > '"+lastsyc+"'  ) "+
				"AND ( hrd.approveflag >'0' OR ( hrd.Status = 'Cancelled' AND ( hrd.approveflag = '0' OR hrd.approveflag is null ) ) )";
*/
        if (backup) {

            query = "SELECT hrd." + ORDERHDR_CREATETIME + " as hdrtime,hrd.approveflag ," +
                    "hrd." + ORDERHDR_PARENT_INVOICENO + ", hrd." + ORDERHDR_STATUS +
                    ", oln.*,hrd." + ORDERHDR_SCHLINEID + ", hrd."
                    + ORDERHDR_CUSTOMER + ", hrd." + ORDERHDR_INVOICENO + ", hrd."
                    + ORDERHDR_LATITUDE + ", hrd." + ORDERHDR_LONGITUDE + " , hrd."
                    + ORDERHDR_NOTE + ",SUM(tl." + ORDERTAXLINE_TAXAMOUNT
                    + ") as taxamount" + ",SUM(tl." + ORDERTAXLINE_RATE
                    + ") as taxrate,tl." + ORDERTAXLINE_AMOUNT
                    + " as taxableamount, ( SUM(tl." + ORDERTAXLINE_TAXAMOUNT
                    + ")+tl." + ORDERTAXLINE_AMOUNT + ") as totalamount, " + "oln."
                    + ORDERLINE_TAX + " FROM " + TABLE_ORDERLINE + " oln JOIN "
                    + TABLE_ORDERHDR + " hrd on hrd." + ORDERHDR_PKID + "=oln."
                    + ORDERLINE_HEADID + " JOIN " + TABLE_SCHEDULE_DETAIL
                    + " sln on hrd." + ORDERHDR_SCHLINEID + "=sln."
                    + SCHEDULE_DETAIL_ID + " JOIN " + TABLE_ORDERTAXLINE
                    + " tl on oln." + ORDERLINE_PKID + "=tl."
                    + ORDERTAXLINE_ORDRDETAILID
                   // +" WHERE tl."+ORDERTAXLINE_DELETEFLAG + " = '0'"
                    + " group by oln." + ORDERLINE_PKID;
        }
		/*String query = "SELECT oln.*,hrd." + ORDERHDR_SCHLINEID + ", hrd."
				+ ORDERHDR_CUSTOMER + ", hrd." + ORDERHDR_INVOICENO + ", hrd."
				+ ORDERHDR_LATITUDE + ", hrd." + ORDERHDR_LONGITUDE + " , hrd."
				+ ORDERHDR_NOTE + ",SUM(tl." + ORDERTAXLINE_TAXAMOUNT
				+ ") as taxamount" + ",SUM(tl." + ORDERTAXLINE_RATE
				+ ") as taxrate,tl." + ORDERTAXLINE_AMOUNT
				+ " as taxableamount, (SUM(tl." + ORDERTAXLINE_TAXAMOUNT
				+ ")+tl." + ORDERTAXLINE_AMOUNT + ") as totalamount, " + "oln."
				+ ORDERLINE_TAX + " FROM " + TABLE_ORDERLINE + " oln JOIN "
				+ TABLE_ORDERHDR + " hrd on hrd." + ORDERHDR_PKID + "=oln."
				+ ORDERLINE_HEADID + " JOIN " + TABLE_SCHEDULE_DETAIL
				+ " sln on hrd." + ORDERHDR_SCHLINEID + "=sln."
				+ SCHEDULE_DETAIL_ID + " JOIN " + TABLE_ORDERTAXLINE
				+ " tl on oln." + ORDERLINE_PKID + "=tl."
				+ ORDERTAXLINE_ORDRDETAILID + " WHERE (oln."
				+ ORDERLINE_CREATETIME + ">'" + lastsyc + "' OR hrd."+ORDERHDR_CREATETIME+"> '"+lastsyc+"'  )"
				+" AND hrd." + ORDERHDR_APPROVEFLAG + ">'0'"
				+" group by oln." + ORDERLINE_PKID;*/


        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject tempObj = new JSONObject();
                tempObj.put(
                        "Status",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_STATUS)) == null ? "Approved"
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_STATUS)));
                tempObj.put(
                        "invoiceNo",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_INVOICENO)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_INVOICENO)));
                tempObj.put(
                        "schedulelineid",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_SCHLINEID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_SCHLINEID)));
                tempObj.put(
                        "TaxableAmount",
                        cursor.getString(cursor.getColumnIndex("taxableamount")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("taxableamount")));
                tempObj.put(
                        "TaxAmount",
                        cursor.getString(cursor.getColumnIndex("taxamount")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("taxamount")));
                tempObj.put(
                        "NetAmount",
                        cursor.getString(cursor.getColumnIndex("totalamount")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("totalamount")));
                tempObj.put(
                        "TaxRate",
                        cursor.getString(cursor.getColumnIndex("taxrate")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("taxrate")));

                tempObj.put(
                        "product",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_PRODID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_PRODID)));
                tempObj.put(
                        "qty",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_QTY)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_QTY)));
                tempObj.put(
                        "MRP",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_MRP)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_MRP)));
                tempObj.put(
                        "UnitRate",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_RATE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_RATE)));
                tempObj.put(
                        "deleteflag",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DELETEFLAG)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DELETEFLAG)));
                tempObj.put(
                        "Discount",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_DISCOUNT)) == null ? "0"
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DISCOUNT)));
                tempObj.put(
                        "DiscountPer",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_DISCOUNTPER)) == null ? "0"
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DISCOUNTPER)));

                String dateString = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_CREATETIME));

                tempObj.put("createtime", dateString);

                tempObj.put("Hdr_createtime", cursor.getString(cursor
                        .getColumnIndex("hdrtime")) == null ? ""
                        : cursor.getString(cursor
                        .getColumnIndex("hdrtime")));
                tempObj.put("approveflag", cursor.getString(cursor
                        .getColumnIndex(ORDERHDR_APPROVEFLAG)) == null ? ""
                        : cursor.getString(cursor
                        .getColumnIndex(ORDERHDR_APPROVEFLAG)));

                tempObj.put(
                        "latitude",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LATITUDE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LATITUDE)));
                tempObj.put(
                        "longitude",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LONGITUDE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LONGITUDE)));

                tempObj.put(
                        "TaxId",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_TAX)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_TAX)));

                String partymarg = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PM)) == null ? "" : cursor
                        .getString(cursor.getColumnIndex(ORDERLINE_PM));

                String packageType = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PACKAGE_TYPE)) == null ? "" : cursor
                        .getString(cursor.getColumnIndex(ORDERLINE_PACKAGE_TYPE));


                String Batch = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_BATCH)) == null ? "" : cursor
                        .getString(cursor.getColumnIndex(ORDERLINE_BATCH));
                tempObj.put("Batch", Batch);
                tempObj.put("TaxType", "Intrastate");
                tempObj.put("PackageType", packageType);
                tempObj.put("PartyMargin", partymarg.contentEquals("") ? "0" : partymarg);

                //				tempObj.put("TotalQty", String.valueOf(totalqty));
                tempObj.put("note",
                        cursor.getString(cursor.getColumnIndex(ORDERHDR_NOTE)) == null ? "" :
                                cursor.getString(cursor.getColumnIndex(ORDERHDR_NOTE)));

                if (tempObj.getString("Status").equalsIgnoreCase("Cancelled")) {
                    tempObj.put("deleteflag", "0");
                }

                tempObj.put("ParentInvoice",
                        cursor.getString(cursor.getColumnIndex(ORDERHDR_PARENT_INVOICENO)) == null ? "" :
                                cursor.getString(cursor.getColumnIndex(ORDERHDR_PARENT_INVOICENO)));
                tempObj.put("ComplimentaryFlag",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_COMPLIMENT)) == null ? "" :
                                cursor.getString(cursor.getColumnIndex(ORDERLINE_COMPLIMENT)));

                orderdata.put(tempObj);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return orderdata;
    }

    public JSONArray getvansalespaymentstosync(boolean backup) throws JSONException {

        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        JSONArray paymentlist = new JSONArray();
        // boolean enabledigital_signature = isserviceexist("DigitalSignature");
        // JSONArray fileData = new JSONArray();
        // HashSet<String> unique_fileid = new HashSet<String>();

        String selectQuery = new StringBuilder().append("SELECT * FROM ").append(TABLE_PAYMENTS).append(" WHERE ").append(PAYMENT_CREATETIME).append(" > ").append(lastupdate).append(" AND ( ").append(DELETE_FLAG).append("=0").append(" OR (").append(DELETE_FLAG).append("= '1' AND ").append(PAYMENT_STATUS).append(" = 'Cancelled' ) )").append(" AND ").append(PAYMENT_APROOVEFLAG).append("=1 ORDER BY ").append(PAYMENT_CREATETIME).append(" DESC").toString();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                String paymentid = cursor.getString(cursor.getColumnIndex(PAYMENT_PKID));//cursor.getString(0);
                String schduleline_id = cursor.getString(1);
                obj.put("paymentid", UUID.randomUUID().toString());
                obj.put("scheduledetailid", schduleline_id);
                // obj.put("paymentmethod", cursor.getString(17));// 17 for
                // pdc/cdc


                String orderid = getorderid(schduleline_id);

                if (isorderapproved(orderid) || backup) {

                    String mode = cursor.getString(cursor.getColumnIndex(PAYMENT_METHOD));
                    String status = cursor.getString(cursor.getColumnIndex(PAYMENT_STATUS));

                    if (mode.equalsIgnoreCase("cash")) {
                        obj.put("paymentmethod", "Cash Payment");
                        obj.put("Status", "Approved");
                    } else if (mode.equalsIgnoreCase("credit")) {
                        obj.put("paymentmethod", "Credit");
                        obj.put("Status", "Approved");
                    } else if (mode.equalsIgnoreCase("Cheque")) {
                        obj.put("paymentmethod", "By Cheque");
                        obj.put("Status", "Draft");
                    }

                    if (status.equalsIgnoreCase("Cancelled")) {
                        obj.put("Status", "Cancelled");
                    }
                    // else if (cursor.getString(17).equalsIgnoreCase("return")) {
                    // obj.put("paymentmethod", "Return");
                    // }
                    obj.put("amount", cursor.getString(cursor.getColumnIndex(AMOUNT)));
                    obj.put("chequenumber", cursor.getString(cursor.getColumnIndex(CHEQUENUMBER)));
                    // obj.put("ifsc", cursor.getString(5));
                    // obj.put("deleteflag", cursor.getString(7));
                    // obj.put("paymentid", cursor.getString(8));
                    // obj.put("salespersonid", cursor.getString(9));
                    obj.put("locationid", cursor.getString(cursor.getColumnIndex(PAYMENT_LOCATIONID)));

                    // DateFormat dateTimeFormat = new SimpleDateFormat(
                    // "yyyyMMddHHmmss");

                    // obj.put("paymentdate",
                    // dateTimeFormat.format(cursor.getString(12)));

                    String paymentdateString = "";
                    String chequeDateString = "";
                    DateFormat dateTimeFormat = new SimpleDateFormat(
                            "yyyyMMddHHmmss");

                    DateFormat dateTimeFormat_readable1 = new SimpleDateFormat(
                            "dd/MM/yyyy HH:mm:ss");
                    DateFormat dateTimeFormat_readable2 = new SimpleDateFormat(
                            "dd/MM/yyyy");
                    try {

						/*	paymentdateString = dateTimeFormat
								.format(dateTimeFormat_readable1.parse(cursor
										.getString(12)));*/

                        paymentdateString = dateTimeFormat
                                .format(dateTimeFormat_readable1.parse
                                        (cursor.getString(cursor.getColumnIndex(PAYMENT_DATE))));

                    } catch (ParseException e) {

                        e.printStackTrace();
                    }

                    try {
						/*chequeDateString = dateTimeFormat
								.format(dateTimeFormat_readable2.parse(cursor
										.getString(11)));*/
                        chequeDateString = dateTimeFormat
                                .format(dateTimeFormat_readable2.parse(cursor.getString(cursor.getColumnIndex(CHEQE_DATE))));

                    } catch (ParseException e) {

                        e.printStackTrace();
                    }
                    obj.put("paymentdate", paymentdateString);

                    obj.put("chequedate", chequeDateString);
                    // obj.put("chequedate", cursor.getString(11));

					/*obj.put("latitude", cursor.getString(15));
					obj.put("longitude", cursor.getString(16));
					obj.put("banknamecode", cursor.getString(18));*/
                    obj.put("latitude", cursor.getString(cursor.getColumnIndex(PAYMENT_LATTITUDE)));
                    obj.put("longitude", cursor.getString(cursor.getColumnIndex(PAYMENT_LONGITUDE)));
                    obj.put("banknamecode", cursor.getString(cursor.getColumnIndex(PAYMENT_BANKCODE)));
                    // obj.put("verificationcode", cursor.getString(22));
                    obj.put("verificationcode", " ");
                    obj.put("signingperson", " ");
                    obj.put("Otp", " ");
                    obj.put("signaturedata", " ");

                    obj.put("invoicedata", getinvoicedatatosync(paymentid));
                    paymentlist.put(obj);
                }
            } while (cursor.moveToNext());


        }

        return paymentlist;

    }

    public String getcurrinvoiceval() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + INVOICENOGEN_CURRVAL + " FROM "
                + TABLE_INVOICENOGEN;

        String currval = new String();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            currval = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();

        // db.close();
        return currval;

    }

    // getting invoicedata on sync

    public JSONArray getinvoicedatatosync(String paymentheaderid)
            throws JSONException {

        JSONArray allinvoicedata = new JSONArray();

        String selectQuery = "SELECT " + INVOICEPAYMENT_INVOICENO + ","
                + INVOICEPAYMENT_AMOUNTPAYED + "," + INVOICEPAYMENT_BALANCE + "," + INVOICEPAYMENT_DELETEFLAG
                + " FROM " + TABLE_INVOICE_PAYMENTS + " WHERE "
                + INVOICEPAYMENT_HEADERID + "='" + paymentheaderid + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {

                String status = "";

                if (cursor.getString(3).equals("1")) {
                    status = "Cancelled";
                }

                JSONObject invoiceobject = new JSONObject();
                invoiceobject.put("invoiceid", cursor.getString(0));
                invoiceobject.put("amount", cursor.getString(1));
                invoiceobject.put("balance", cursor.getString(2));
                invoiceobject.put("Status", status);
                allinvoicedata.put(invoiceobject);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return allinvoicedata;

    }

// //getupdatedlatlongs

    public JSONArray getupdatedlaatlong(boolean backup) throws JSONException {
        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }
        //		String lastsyc = getrecentsynctime();

        JSONArray childs = new JSONArray();

        String selectQuery = new StringBuilder().append("SELECT ").append(LOCATION_ID).append(",").append(LATITUDE_NEW).append(",").append(LONGITUDE_NEW).append(",").append(SCHED_LOCUPDATETIME).append(" FROM ").append(TABLE_SCHEDULE_DETAIL).append(" WHERE ").append(SCHEDULEDETAIL_CREATETIME).append(" > ").append(lastsyc).toString();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                String lat = cursor.getString(1);
                obj.put("locationid", cursor.getString(0));
                obj.put("latitude", cursor.getString(1));
                obj.put("longitude", cursor.getString(2));
                obj.put("Salespersonid", getsalespersonid());
                obj.put("updatetime", cursor.getString(3));
                if (lat != null && !lat.equals("0")) {
                    childs.put(obj);
                }

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return childs;

    }

    public boolean getpendingNotApproved() throws JSONException {
        boolean temp = false;
        String lastupdate = getrecentsynctime();

        String selectQuery = "SELECT * FROM " + TABLE_PAYMENTS + " WHERE "
                + PAYMENT_CREATETIME + " > " + lastupdate + " AND "
                + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG
                + "!=1 ORDER BY " + PAYMENT_CREATETIME + " DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public boolean getpendingNotApprovedOrders() throws Exception {
        boolean temp = false;
        String lastupdate = getrecentsynctime();
        String selectQuery = "SELECT ol.id FROM " + TABLE_ORDERLINE
                + " ol join " + TABLE_ORDERHDR + " orh on orh." + ORDERHDR_PKID
                + "=ol." + ORDERLINE_HEADID + " WHERE "
                //			    + "ol." + ORDERLINE_CREATETIME + " > " + lastupdate + " AND "
                + "ol." + ORDERLINE_DELETEFLAG + "=0" + " and (orh."
                + ORDERHDR_APPROVEFLAG + "!= '1'  or orh." + ORDERHDR_APPROVEFLAG
                + " is null )";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public Boolean anypendingorderexist() {

        Boolean reuslt = false;
        SQLiteDatabase db = this.getWritableDatabase();

        String lastupdate = getrecentsynctime();

        String selectQuery = "SELECT * FROM " + TABLE_ORDER_DETAIL + " WHERE "
                + CREATETIME + ">" + lastupdate;

        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            reuslt = true;
            return reuslt;
        }
        if (cursor != null)
            cursor.close();

        return reuslt;
    }

    public boolean getPendingOrderPayment() throws JSONException {
        boolean temp = false;
        String lastupdate = getrecentsynctime();
        String selectQuery = "SELECT ol.id FROM " + TABLE_ORDERLINE
                + " ol join " + TABLE_ORDERHDR + " orh on orh." + ORDERHDR_PKID
                + "=ol." + ORDERLINE_HEADID + " WHERE ol."
                + ORDERLINE_CREATETIME + " > " + lastupdate + " AND ol."
                + ORDERLINE_DELETEFLAG + "=0" + " and (orh."
                + ORDERHDR_APPROVEFLAG + "= '0' or orh." + ORDERHDR_APPROVEFLAG
                + " is null)";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public void updatesynctable(String synctime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SYNCTIME, synctime);
        db.update(TABLE_SYNCTABLE, values, TABLEINSYNC + "=?",
                new String[]{TABLE_ORDER_DETAIL});

    }

    // /update synctable for locations
    public void updatesynctablelocationsync(String synctime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SYNCTIME, synctime);
        db.update(TABLE_SYNCTABLE, values, TABLEINSYNC + "=?",
                new String[]{TABLE_LOCATIONS});

    }

    /**
     * Order Payment DB Services
     */

    public String getpaidamnt(String scheduledetailid) {

        String paidamnt = "";
        scheduledetailid = "'" + scheduledetailid + "'";

        String selectQuery = "SELECT " + AMOUNT + " FROM " + TABLE_PAYMENTS
                + " WHERE " + PAYMENT_SCHEDULEDETAILID + "=" + scheduledetailid
                + " AND " + DELETE_FLAG + "=0";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            paidamnt = cursor.getString(0);

        }
        if (cursor != null)
            cursor.close();

        return paidamnt;

    }

    public String getpaymentdate(String scheduledetailid) {

        String paymentdate = "";
        scheduledetailid = "'" + scheduledetailid + "'";

        String selectQuery = "SELECT " + PAYMENT_DATE + " FROM "
                + TABLE_PAYMENTS + " WHERE " + PAYMENT_SCHEDULEDETAILID + "="
                + scheduledetailid + " AND " + DELETE_FLAG + "=0";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            paymentdate = cursor.getString(0);

        }
        if (cursor != null)
            cursor.close();

        return paymentdate;

    }

    public String getactualmode(String scheduledetailid) {

        String actualmode = "";
        scheduledetailid = "'" + scheduledetailid + "'";

        String selectQuery = "SELECT " + PAYMENT_ACTUALMODE + " FROM "
                + TABLE_PAYMENTS + " WHERE " + PAYMENT_SCHEDULEDETAILID + "="
                + scheduledetailid + " AND " + DELETE_FLAG + "=0";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            actualmode = cursor.getString(0);

        }
        if (cursor != null)
            cursor.close();

        return actualmode;

    }

    // get bank data from cache
    public JSONArray getbankdata() throws JSONException {
        JSONArray bankdata = new JSONArray();
        String selectQuery = "SELECT " + BANKMASTER_CODE + ","
                + BANKMASTER_NAME + "," + BANKMASTER_BRANCHNAME + " FROM "
                + TABLE_BANKMASTER + " ORDER BY " + BANKMASTER_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("bankcode", cursor.getString(0));
                obj.put("bankname", cursor.getString(1));
                obj.put("branchname", cursor.getString(2));
                bankdata.put(obj);
            } while (cursor.moveToNext());

        }

        if (cursor != null)
            cursor.close();
        return bankdata;
    }

    public JSONObject getcoustomername(String scheduledetailid)
            throws JSONException {

        scheduledetailid = "'" + scheduledetailid + "'";
        JSONObject retJson = new JSONObject();
        String coustemername = "";
        String CustomerAddress = "";
        String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE_DETAIL
                + " WHERE " + SCHEDULE_DETAIL_ID + "=" + scheduledetailid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                coustemername = cursor.getString(4);
                CustomerAddress = cursor.getString(5);
                retJson.put("CustName", coustemername);
                retJson.put("CustAddress", CustomerAddress);
                retJson.put("locationid", cursor.getString(3));
                retJson.put("CustPhone", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_MOBILE)));
                retJson.put("PhotoUUID", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PHOTOUUID)));
                return retJson;

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return retJson;

    }

    public String insertvansalespayment(String scheduledetailid,
                                        String paymentmode, String paidamntval, String checknumbervalue,
                                        String banknamevalue, String createtime2, String checkdatevalue,
                                        String paymentdate, String invoiceno, Double lat, Double lng,
                                        String actualmode, String banknamecode, Double pendingamt,
                                        String totinvamt) throws DateError, JSONException {


        if (!validateCreateTime(createtime2)) {
            throw new DateError();
        }

        String locationid = getcoustomername(scheduledetailid).getString(
                "locationid");
        String salespersonid = String.valueOf(getsalespersonid());
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(PAYMENT_SCHEDULEDETAILID, scheduledetailid);
        values.put(PAYMENT_METHOD, paymentmode);
        values.put(AMOUNT, paidamntval);
        values.put(CHEQUENUMBER, checknumbervalue);
        values.put(IFSC, banknamevalue);
        values.put(PAYMENT_CREATETIME, createtime2);
        values.put(DELETE_FLAG, "0");
        // values.put(PAYMENTGUID, paymentid);
        values.put(SALESPERSONID, salespersonid);
        values.put(PAYMENT_LOCATIONID, locationid);
        values.put(CHEQE_DATE, checkdatevalue);
        values.put(PAYMENT_DATE, paymentdate);
        values.put(PAYMENT_INVOCENUMBER, invoiceno);
        /*
         * values.put(PAYMENT_PICKLISTNUMBER,
         * invoicedat.getString("picklistnumber"));
         */
        values.put(PAYMENT_LATTITUDE, lat);
        values.put(PAYMENT_LONGITUDE, lng);
        values.put(PAYMENT_ACTUALMODE, actualmode);
        values.put(PAYMENT_BANKCODE, banknamecode);
        values.put(PAYMENT_APROOVEFLAG, "0");
        values.put(PAYMENT_STATUS, "Pending");

        ContentValues invoicetabledata = new ContentValues();
        invoicetabledata.put(INVOICE_CUSTOMERID, locationid);
        invoicetabledata.put(INVOICE_NO, invoiceno);
        invoicetabledata.put(INVOICE_TOTTALAMOUNT, totinvamt);
        invoicetabledata.put(INVOICE_BALANCEAMOUNT, pendingamt);
        invoicetabledata.put(INVOICE_DATE, paymentdate);

        Double balance = 0.0;
        Double totamnt = Double.valueOf(totinvamt);
        Double paidamnt = Double.valueOf(paidamntval);
        String status = "";
        if (paymentmode.contentEquals("Credit"))
            balance = round((totamnt), 2);
        else
            balance = round((totamnt - paidamnt), 2);

        if (balance <= 0) {
            status = "paid";

        } else if (balance == totamnt) {


            status = "not paid";

        } else {
            status = "partiallypaid";
        }
        invoicetabledata.put(INVOICE_STATUS, status);

        long n = db.insert(TABLE_INVOICE, null, invoicetabledata);

        long header = db.insert(TABLE_PAYMENTS, null, values);
        insertvansaleinvoicepayment(String.valueOf(header), paidamntval,
                pendingamt.toString(), invoiceno, scheduledetailid);
        // if (updatebalance)
        // updateinvoicetable(invoicedata);

        return String.valueOf(header);

    }

    public void insertvansaleinvoicepayment(String paymentheaderid,
                                            String paidamntval, String pendingamt, String invoiceNo,
                                            String scheduledetailid) throws JSONException {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(INVOICEPAYMENT_HEADERID, paymentheaderid);
        values.put(INVOICEPAYMENT_INVOICENO, invoiceNo);
        values.put(INVOICEPAYMENT_AMOUNTPAYED, paidamntval);
        values.put(INVOICEPAYMENT_BALANCE, pendingamt);
        values.put(INVOICEPAYMENT_DELETEFLAG, "0");
        long m = db.insert(TABLE_INVOICE_PAYMENTS, null, values);

        if (!isWithoutInventory()) {
            inventoryJournalPosting(scheduledetailid);
        }


    }

    private void inventoryJournalPosting(String scheduledetailid) throws JSONException {

        //String orderId = getorderid(scheduledetailid);

        //		String complimentaryOrderId = getComplimentaryOrderId(scheduledetailid, parentInvoiceNo)
        JSONObject journalData = new JSONObject();
        JSONArray journalDataArray = new JSONArray();
        String query = "SELECT ol." + ORDERLINE_PRODID + ",ol." + ORDERLINE_BATCH
                + ",ol." + ORDERLINE_QTY + ",ol." + ORDERLINE_MRP + ", o."
                + ORDERHDR_INVOICENO + ",o." + ORDERHDR_CREATETIME + ",ol." + ORDERLINE_PACKAGE_TYPE + " FROM "
                + TABLE_ORDERLINE + " ol JOIN " + TABLE_ORDERHDR + " o ON o."
                + ORDERHDR_PKID + " = ol." + ORDERLINE_HEADID + " WHERE o."
                + ORDERHDR_SCHLINEID + " = '" + scheduledetailid + "' " +
                "AND " + ORDERHDR_STATUS + " != 'Cancelled' " +
                "AND " + ORDERLINE_DELETEFLAG + " != '1'";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String jrnlHdr = UUID.randomUUID().toString();

                JSONObject invObject = new JSONObject();
                String product = cursor.getString(0);
                String batch = cursor.getString(1);
                double packQty = Double.parseDouble(cursor.getString(2));
                double mrp = Double.parseDouble(cursor.getString(3));
                String invNo = cursor.getString(4);
                String invDate = cursor.getString(5);
                String packageTypeId = cursor.getString(6);
                JSONObject qtyObject = getBaseUnitQty(product, packageTypeId, packQty);
                double qty = qtyObject.optDouble("Qty", 0);

                double amount = qty * mrp;


                JSONObject invJrnlHdr = new JSONObject()
                        .put("JrnlHdr", jrnlHdr).put("JournalNo", invNo)
                        .put("JournalDate", invDate).put("Product", product)
                        .put("Source", "INVOICE").put("SourceID", invNo)
                        .put("UOM", qtyObject.optString("UOM"))
                        .put("PackageType", qtyObject.optString("UOM"))
                        .put("BatchNo", batch); // .put("Organization", value);
                invObject.put("InvJrnlHdr", invJrnlHdr);

                JSONArray InvJournalLines = new JSONArray();

                JSONObject availableStockObject = new JSONObject()
                        .put("JournalHdr", jrnlHdr)
                        .put("AmountCredited", amount + "")
                        .put("AmountDebited", "0")
                        .put("InventoryAccount",
                                getInventoryAccountId("Available Stock"))
                        .put("QuantityCredited", qty + "")
                        .put("QuantityDebited", "0");
                InvJournalLines.put(availableStockObject);

                JSONObject goodsIssueObject = new JSONObject()
                        .put("JournalHdr", jrnlHdr)
                        .put("AmountCredited", "0")
                        .put("AmountDebited", amount + "")
                        .put("InventoryAccount",
                                getInventoryAccountId("Goods Issued"))
                        .put("QuantityCredited", "0")
                        .put("QuantityDebited", qty + "");
                InvJournalLines.put(goodsIssueObject);
                invObject.put("InvJournalLines", InvJournalLines);

                journalDataArray.put(invObject);
            } while (cursor.moveToNext());

        }
        db.close();
        if (cursor != null)
            cursor.close();

        journalData.put("JournalData", journalDataArray);

        insertInventoryJrnl(journalData);
    }


    public double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
   /* public void approveorder(String scheduleLineId,String approveTime ) throws DateError, JSONException {

        if (!validateCreateTime(approveTime)) {
            throw new DateError();
        }

        SQLiteDatabase db = this.getWritableDatabase();

        if ( isserviceexist("Van Sales") ) {

            if (isWithoutInventory()) {

                ContentValues values = new ContentValues();
                values.put(ORDERHDR_APPROVEFLAG, "1");
                values.put(ORDERHDR_CREATETIME, approveTime);
                db.update(TABLE_ORDERHDR, values, ORDERHDR_SCHLINEID + "=?",
                        new String[] { scheduleLineId });
            }else{

                String schLineId = "";
                String sql = "select " + ORDERHDR_SCHLINEID
                        + "  FROM " + TABLE_ORDERHDR+" WHERE "+ ORDERHDR_APPROVEFLAG+" = '-1' " ;
                Cursor cursor = db.rawQuery(sql, null);
                if (cursor.moveToFirst()) {

                    schLineId = cursor.getString(0);

                    ContentValues values = new ContentValues();

                    values.put(PAYMENT_CREATETIME, approveTime);

                    db.update(TABLE_PAYMENTS, values, PAYMENT_SCHEDULEDETAILID + "= ?",
                            new String[] { schLineId });
                }

                if (cursor != null)
                    cursor.close();

                ContentValues values = new ContentValues();
                values.put(ORDERHDR_APPROVEFLAG, "-1");
                db.update(TABLE_ORDERHDR, values, ORDERHDR_SCHLINEID + "=? and "+ORDERHDR_STATUS+" != 'Cancelled' ",
                        new String[] { scheduleLineId });

                values.put(ORDERHDR_APPROVEFLAG, "1");
                values.put(ORDERHDR_CREATETIME, approveTime);
                db.update(TABLE_ORDERHDR, values, ORDERHDR_SCHLINEID + "!=? and "+ORDERHDR_APPROVEFLAG+" = '-1' ",
                        new String[] { scheduleLineId });

            }

        }else{


            ContentValues values = new ContentValues();
            values.put(ORDERHDR_APPROVEFLAG, "1");
            values.put(ORDERHDR_CREATETIME, approveTime);
            db.update(TABLE_ORDERHDR, values, ORDERHDR_SCHLINEID + "=?",
                    new String[] { scheduleLineId });

        }

        updateschedulestatus(scheduleLineId, "order");

    }*/

    public void approvepayment(String headerid, String createtime) throws DateError {

        if (!validateCreateTime(createtime)) {
            throw new DateError();
        }

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PAYMENT_APROOVEFLAG, "1");
        values.put(PAYMENT_CREATETIME, createtime);
        db.update(TABLE_PAYMENTS, values, PAYMENT_PKID + "=?",
                new String[]{headerid});

		/*	values.put(PAYMENT_APROOVEFLAG, "1");
		db.update(TABLE_PAYMENTS, values, PAYMENT_PKID + "!= ? and "+PAYMENT_APROOVEFLAG+" = '-1'",
				new String[] { headerid });*/


    }

    public JSONArray getInvoiceDetailsForPrint(String orderid)
            throws JSONException {
        JSONArray orderdata = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
		/*	String sql = "select p." + PRODUCT_NAME + ", p." + PRODUCT_HSN + ", l."
				+ ORDERLINE_QTY + ", l." + ORDERLINE_PACKAGE_TYPE + ", l."
				+ ORDERLINE_RATE +  ", l." + ORDERLINE_DISCOUNT
				+ ", ctl." + ORDERTAXLINE_AMOUNT + ", ctl."
				+ ORDERTAXLINE_TAXAMOUNT + " as CGSTamnt, stl."
				+ ORDERTAXLINE_TAXAMOUNT + " as SGSTamnt, ctl."
				+ ORDERTAXLINE_RATE + " as CGSTrate, stl." + ORDERTAXLINE_RATE
				+ " as SGSTrate, ( (l." + ORDERLINE_MRP + "*" + ORDERLINE_QTY
				+ ") - l." + ORDERLINE_DISCOUNT
				+") AS total, (stl." + ORDERTAXLINE_TAXAMOUNT + "+ ctl."
				+ ORDERTAXLINE_TAXAMOUNT
				+ " ) as taxtotal from "
				+ TABLE_ORDERLINE
				+ " l left join "
				+ TABLE_PRODUCT
				+ " p on l."
				+ ORDERLINE_PRODID
				+ "=p."
				+ PRODUCT_ID
				// + " left join " + TABLE_UNITS + " u on u." + UNITS_ID + "=l."
				// + ORDERLINE_UOM
				+ " left join " + TABLE_ORDERTAXLINE + " ctl on ctl."
				+ ORDERTAXLINE_ORDRDETAILID + "=l." + ORDERLINE_PKID
				+ " and ctl." + ORDERTAXLINE_CODE + " LIKE '%cgst%' JOIN "
				+ TABLE_ORDERTAXLINE + " stl on stl."
				+ ORDERTAXLINE_ORDRDETAILID + "=l." + ORDERLINE_PKID
				+ " and stl." + ORDERTAXLINE_CODE + " LIKE '%sgst%' where l."
				+ ORDERLINE_HEADID + "=" + orderid + " GROUP BY l."
				+ ORDERLINE_PRODID;*/

        String sql = "select p." + PRODUCT_NAME + ", p." + PRODUCT_HSN + ", l."
                + ORDERLINE_QTY + ", l." + ORDERLINE_PACKAGE_TYPE + ", l."
                + ORDERLINE_RATE + ", l." + ORDERLINE_DISCOUNT
                + ", l." + ORDERLINE_DISCOUNTPER
                + ", ctl." + ORDERTAXLINE_AMOUNT + ", ctl."
                + ORDERTAXLINE_TAXAMOUNT + " as CGSTamnt, stl."
                + ORDERTAXLINE_TAXAMOUNT + " as SGSTamnt, ctl."
                + ORDERTAXLINE_RATE + " as CGSTrate, stl." + ORDERTAXLINE_RATE
                + " as SGSTrate, ( l." + ORDERLINE_RATE + "*" + ORDERLINE_QTY
                + ") AS actualtotal, (stl." + ORDERTAXLINE_TAXAMOUNT + "+ ctl."
                + ORDERTAXLINE_TAXAMOUNT
                + "+ ifnull(otl.taxamount,0) ) as taxtotal," +
                " ( ( stl.taxamount+ ctl.taxamount + ifnull(otl.taxamount,0) ) + ctl.taxableamount ) as total ," +
                 " otl." + ORDERTAXLINE_TAXAMOUNT + " as otherTamnt, " +
                "otl." + ORDERTAXLINE_RATE + " as otherTrate "+
                " from "
                + TABLE_ORDERLINE
                + " l left join "
                + TABLE_PRODUCT
                + " p on l."
                + ORDERLINE_PRODID
                + "=p."
                + PRODUCT_ID
                // + " left join " + TABLE_UNITS + " u on u." + UNITS_ID + "=l."
                // + ORDERLINE_UOM
                + " left join " + TABLE_ORDERTAXLINE + " ctl on ctl."
                + ORDERTAXLINE_ORDRDETAILID + "=l." + ORDERLINE_PKID
                + " and ctl." + ORDERTAXLINE_CODE + " LIKE '%cgst%' " +
                "left JOIN "
                + TABLE_ORDERTAXLINE + " stl on stl."
                + ORDERTAXLINE_ORDRDETAILID + "=l." + ORDERLINE_PKID
                + " and stl." + ORDERTAXLINE_CODE + " LIKE '%sgst%' "
                + " left join " + TABLE_ORDERTAXLINE + " otl on otl."
                + ORDERTAXLINE_ORDRDETAILID + "=l." + ORDERLINE_PKID
                + " and otl." + ORDERTAXLINE_CODE + "  LIKE '%CESS%' " +

                "where l."
                + ORDERLINE_HEADID + "=" + orderid + " GROUP BY l."
                + ORDERLINE_PRODID;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {

            do {

                JSONObject tempObj = new JSONObject();
                tempObj.put(
                        "productname",
                        cursor.getString(cursor.getColumnIndex(PRODUCT_NAME)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(PRODUCT_NAME)));
                tempObj.put(
                        "HSNCode",
                        cursor.getString(cursor.getColumnIndex(PRODUCT_HSN)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(PRODUCT_HSN)));
                tempObj.put(
                        "Qty",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_QTY)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_QTY)));
				/*tempObj.put(
						"uom",
						cursor.getString(cursor.getColumnIndex(ORDERLINE_UOM)) == null ? ""
								: cursor.getString(cursor
										.getColumnIndex(ORDERLINE_UOM)));*/
                tempObj.put(
                        "mrp",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_RATE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_RATE)));
                tempObj.put(
                        "taxableamnt",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERTAXLINE_AMOUNT)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERTAXLINE_AMOUNT)));
                tempObj.put(
                        "CGSTamnt",
                        cursor.getString(cursor.getColumnIndex("CGSTamnt")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("CGSTamnt")));
                tempObj.put(
                        "SGSTamnt",
                        cursor.getString(cursor.getColumnIndex("SGSTamnt")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("SGSTamnt")));
                tempObj.put(
                        "CGSTrate",
                        cursor.getString(cursor.getColumnIndex("CGSTrate")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("CGSTrate")));
                tempObj.put(
                        "SGSTrate",
                        cursor.getString(cursor.getColumnIndex("SGSTrate")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("SGSTrate")));

                tempObj.put(
                        "actualtotal",
                        cursor.getString(cursor.getColumnIndex("actualtotal")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("actualtotal")));
                tempObj.put(
                        "total",
                        cursor.getString(cursor.getColumnIndex("total")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("total")));

                tempObj.put(
                        "taxtotal",
                        cursor.getString(cursor.getColumnIndex("taxtotal")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("taxtotal")));
                tempObj.put(
                        "discount",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_DISCOUNT)) == null ? "0"
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DISCOUNT)));

                tempObj.put(
                        "discountPer",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_DISCOUNTPER)) == null ? "0"
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DISCOUNTPER)));

                String packageTypeId = cursor.getString(cursor.getColumnIndex(ORDERLINE_PACKAGE_TYPE)) == null ? ""
                        : cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PACKAGE_TYPE));
                String packageTypeCode = getPackageTypeCode(packageTypeId);

                tempObj.put("uom", packageTypeCode);

                tempObj.put(
                        "otherTrate",
                        cursor.getString(cursor.getColumnIndex("otherTrate")) == null ? "0"
                                : cursor.getString(cursor
                                .getColumnIndex("otherTrate")));
                tempObj.put(
                        "otherTamnt",
                        cursor.getString(cursor.getColumnIndex("otherTamnt")) == null ? "0"
                                : cursor.getString(cursor
                                .getColumnIndex("otherTamnt")));
                orderdata.put(tempObj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return orderdata;

    }

    public JSONArray getpaymentstosync(boolean backup) throws JSONException {
        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }
        //		String lastupdate = getrecentsynctime();
        JSONArray paymentlist = new JSONArray();
        boolean enabledigital_signature = isserviceexist("DigitalSignature");
        JSONArray fileData = new JSONArray();
        HashSet<String> unique_fileid = new HashSet<String>();

        String selectQuery = "SELECT * FROM " + TABLE_PAYMENTS + " WHERE "
                + PAYMENT_CREATETIME + " > " + lastupdate + " AND "
                + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG
                + "=1 ORDER BY " + PAYMENT_CREATETIME + " DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                String paymentid = cursor.getString(0);
                String schduleline_id = cursor.getString(1);
                obj.put("scheduledetailid", schduleline_id);
                obj.put("payentmethode", cursor.getString(17));// 17 for pdc/cdc
                obj.put("amount", cursor.getString(3));
                obj.put("chequenumber", cursor.getString(4));
                obj.put("ifsc", cursor.getString(5));
                obj.put("deleteflag", cursor.getString(7));
                obj.put("paymentid", cursor.getString(8));
                obj.put("salespersonid", cursor.getString(9));
                obj.put("locationid", cursor.getString(10));
                obj.put("chequedate", cursor.getString(11));
                obj.put("paymentdate", cursor.getString(12));
                obj.put("invoicenumber", cursor.getString(13));
                obj.put("picklistnumber", cursor.getString(14));
                obj.put("latitude", cursor.getString(15));
                obj.put("longitude", cursor.getString(16));
                obj.put("banknamecode", cursor.getString(18));
                obj.put("verificationcode", cursor.getString(22));
                if (enabledigital_signature) {
                    obj.put("signingperson", cursor.getString(20));
                    String fileid = cursor.getString(21);
                    obj.put("signaturefileid", fileid);

                    if (!unique_fileid.contains(fileid)) {
                        JSONObject imageobject = new JSONObject();
                        imageobject.put("signmappingid", fileid);
                        imageobject.put("signmappingimage", readFromFile(fileid
                                + "_signature.png"));
                        fileData.put(imageobject);
                        unique_fileid.add(fileid);
                    }
                }

                obj.put("invoicedata", getinvoicedatatosync(paymentid));
                paymentlist.put(obj);

            } while (cursor.moveToNext());

            if (paymentlist.length() > 0 && enabledigital_signature) {
                paymentlist.getJSONObject(0).put("signaturedata", fileData);
            }

        }

        return paymentlist;

    }

    // /get order amount to sync
    public JSONArray getorderpaymentstosync(boolean backup) throws JSONException {

        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        //		String lastupdate = getrecentsynctime();
        JSONArray orederamountdata = new JSONArray();
        String selectQuery = "SELECT * FROM " + TABLE_ORDERAMOUNTDATA
                + " WHERE " + ORDERAMOUNTDATA_CREATETIME + " > " + lastupdate;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();

                obj.put("amount", cursor.getString(1));
                obj.put("scheduledetailid", cursor.getString(2));
                obj.put("customerid", cursor.getString(3));
                obj.put("personid", cursor.getString(4));
                obj.put("time", cursor.getString(5));
                obj.put("lattitude", cursor.getString(7));
                obj.put("longitude", cursor.getString(8));
                obj.put("guid", cursor.getString(9));
                orederamountdata.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return orederamountdata;

    }

    public JSONArray getorderdatatosync(boolean backup) throws JSONException {

        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }

        //		String lastsyc = getrecentsynctime();
        JSONArray orderdata = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT oln.*,hrd." + ORDERHDR_SCHLINEID + ", hrd."
                + ORDERHDR_CUSTOMER + ", hrd." + ORDERHDR_LATITUDE + ", hrd."
                + ORDERHDR_LONGITUDE + " , hrd." + ORDERHDR_NOTE + " FROM "
                + TABLE_ORDERLINE + " oln JOIN " + TABLE_ORDERHDR
                + " hrd on hrd." + ORDERHDR_PKID + "=oln." + ORDERLINE_HEADID
                + " JOIN " + TABLE_SCHEDULE_DETAIL + " sln on hrd."
                + ORDERHDR_SCHLINEID + "=sln." + SCHEDULE_DETAIL_ID
                + " WHERE oln." + ORDERLINE_CREATETIME + ">'" + lastsyc
                + "' AND " + ORDERHDR_APPROVEFLAG + "='1'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject tempObj = new JSONObject();
                tempObj.put(
                        "schedulelineid",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_SCHLINEID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_SCHLINEID)));
                tempObj.put(
                        "product",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_PRODID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_PRODID)));
                tempObj.put(
                        "qty",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_QTY)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_QTY)));
                tempObj.put(
                        "mrp",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_MRP)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_MRP)));
                tempObj.put(
                        "rate",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_RATE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_RATE)));
                tempObj.put(
                        "deleteflag",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DELETEFLAG)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DELETEFLAG)));
                tempObj.put(
                        "createtime",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_CREATETIME)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_CREATETIME)));
                tempObj.put(
                        "latitude",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LATITUDE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LATITUDE)));
                tempObj.put(
                        "longitude",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LONGITUDE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LONGITUDE)));
                tempObj.put(
                        "customer",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_CUSTOMER)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_CUSTOMER)));

                String product = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PRODID)) == null ? ""
                        : cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PRODID));
                String partymarg = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PM)) == null ? "" : cursor
                        .getString(cursor.getColumnIndex(ORDERLINE_PM));
				/*String upc = cursor.getString(cursor
						.getColumnIndex(ORDERLINE_PRODID)) == null ? ""
								: cursor.getString(cursor.getColumnIndex(ORDERLINE_UPC));
				 	tempObj.put("uom", upc);
				double valupc = getupcofprod(db, product, upc);
				tempObj.put("upc", String.valueOf(valupc));
				 */
                String packagetype = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PACKAGE_TYPE)) == null ? ""
                        : cursor.getString(cursor.getColumnIndex(ORDERLINE_PACKAGE_TYPE));

                tempObj.put("PackageType", packagetype);
                tempObj.put("pm", partymarg.contentEquals("") ? "0" : partymarg);
                String batch = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_BATCH)) == null ? "" : cursor
                        .getString(cursor.getColumnIndex(ORDERLINE_BATCH));
                tempObj.put("batch", batch);

                tempObj.put(
                        "note",
                        cursor.getString(cursor.getColumnIndex(ORDERHDR_NOTE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_NOTE)));

                orderdata.put(tempObj);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return orderdata;
    }

    // /geting order data for upload
    public JSONArray getorders(boolean backup) throws JSONException {
        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        //		String lastupdate = getrecentsynctime();
        JSONArray orders = new JSONArray();
        String selectQuery = "SELECT * FROM " + TABLE_ORDER_HEADER + " WHERE "
                + ORDERSUBMIT_TIME + " IS NOT NULL ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                String orderheadid = cursor.getString(0);
                obj.put("orderbegintime", cursor.getString(1));
                obj.put("ordersubmittime", cursor.getString(2));
                obj.put("scheduledetailid", cursor.getString(3));
                obj.put("latitude", cursor.getString(5));
                obj.put("longitude", cursor.getString(6));
                obj.put("salespersonid", cursor.getString(7));
                obj.put("routenetworkid", cursor.getString(8));
                obj.put("locationid", cursor.getString(9));
                obj.put("products", getorderdetails(orderheadid, lastupdate));
                orders.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return orders;

    }

    public JSONArray getorderdetails(String orderhead, String lastupdate) throws JSONException {

        JSONArray orderdetail = new JSONArray();
        String selectQuery = "SELECT * FROM " + TABLE_ORDER_DETAIL + " WHERE "
                + ORDER_HEAD_ID + "=" + orderhead + " AND " + CREATETIME + ">"
                + lastupdate;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();

                obj.put("productid", cursor.getString(2));
                obj.put("quantity", cursor.getString(3));
                obj.put("deliverydate", cursor.getString(4));
                obj.put("DeleteFlag", cursor.getString(6));
                obj.put("rate", cursor.getString(7));
                obj.put("PackageType", cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PACKAGE_TYPE)));
                String product = cursor.getString(2);
				/*	String upc = cursor.getString(cursor
						.getColumnIndex(ORDERLINE_UPC));
				obj.put("uom", upc);
				double valupc = getupcofprod(db, product, upc);
				obj.put("upc", String.valueOf(valupc));*/
                orderdetail.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return orderdetail;
    }

    public JSONArray getsalesreturndatatosync(boolean backup) throws JSONException {

        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }
        //		String lastsyc = getrecentsynctime();
        JSONArray orderdata = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT srn.*,hrd." + SALESRETURN_SCHLINEID + " FROM "
                + TABLE_SALERTNLINE + " srn JOIN " + TABLE_SALESRETURN
                + " hrd on hrd." + SALESRETURN_PKID + "=srn."
                + SALERTNLINE_HEADID + " JOIN " + TABLE_SCHEDULE_DETAIL
                + " sln on hrd." + SALESRETURN_SCHLINEID + "=sln."
                + SCHEDULE_DETAIL_ID + " WHERE srn." + SALERTNLINE_CREATETIME
                + ">'" + lastsyc + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject tempObj = new JSONObject();
                tempObj.put(
                        "schedulelineid",
                        cursor.getString(cursor
                                .getColumnIndex(SALESRETURN_SCHLINEID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALESRETURN_SCHLINEID)));
                tempObj.put(
                        "product",
                        cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_PRODID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_PRODID)));
                tempObj.put(
                        "qty",
                        cursor.getString(cursor.getColumnIndex(SALERTNLINE_QTY)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_QTY)));
                tempObj.put(
                        "mrp",
                        cursor.getString(cursor.getColumnIndex(SALERTNLINE_MRP)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_MRP)));
                tempObj.put(
                        "invoice",
                        cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_INVOICE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_INVOICE)));
                tempObj.put(
                        "deleteflag",
                        cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_DELETEFLAG)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_DELETEFLAG)));
                tempObj.put(
                        "createtime",
                        cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_CREATETIME)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_CREATETIME)));
                orderdata.put(tempObj);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return orderdata;
    }

    public JSONArray getcreditnotestosync(boolean backup) throws JSONException {

        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }

        //		String lastsyc = getrecentsynctime();
        JSONArray orderdata = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_CREDITNOTES + " WHERE "
                + CREDITNOTE_CREATEDTIME + ">'" + lastsyc + "'";

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject tempObj = new JSONObject();
                tempObj.put(
                        "schedulelineid",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_SCHDLELINEID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_SCHDLELINEID)));
                tempObj.put(
                        "amount",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_AMOUNT)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_AMOUNT)));
                tempObj.put(
                        "reason",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_REASON)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_REASON)));
                tempObj.put(
                        "description",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_DESCRIPTION)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_DESCRIPTION)));
                tempObj.put(
                        "deleteflag",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_DELETEFLAG)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_DELETEFLAG)));
                tempObj.put(
                        "createtime",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_CREATEDTIME)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_CREATEDTIME)));
                orderdata.put(tempObj);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return orderdata;
    }


    // get category data from cache
    public JSONArray getcustomercategories(String personrole)
            throws JSONException {
        JSONArray resons = new JSONArray();
        String selectQuery = "";
        if (personrole.contentEquals("Combing User"))
            selectQuery = "SELECT " + CUSTOMERCATEGORIES_CODE + ","
                    + CUSTOMERCATEGORIES_NAME + " FROM "
                    + TABLE_CUSTOMERCATEGORIES + " where "
                    + CUSTOMERCATEGORIES_TYPE + "='Combing'";
        else
            selectQuery = "SELECT " + CUSTOMERCATEGORIES_CODE + ","
                    + CUSTOMERCATEGORIES_NAME + " FROM "
                    + TABLE_CUSTOMERCATEGORIES + " where "
                    + CUSTOMERCATEGORIES_TYPE + "!='Combing'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("catcode", cursor.getString(0));
                obj.put("catname", cursor.getString(1));
                resons.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return resons;
    }
    // get towndata data
    public JSONArray gettown() throws JSONException {
        JSONArray towns = new JSONArray();
        String selectQuery = "SELECT " + TOWN_CODE + "," + TOWN_NAME + " FROM "
                + TABLE_TOWN + " ORDER BY " + TOWN_NAME + " ASC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("townid",
                        cursor.getString(cursor.getColumnIndex(TOWN_CODE)));
                obj.put("townname",
                        cursor.getString(cursor.getColumnIndex(TOWN_NAME)));
                towns.put(obj);
            } while (cursor.moveToNext());

        }

        if (cursor != null)
            cursor.close();
        return towns;
    }
    public HashMap<String, FourStrings> getexcustomers() throws Exception {
        HashMap<String, FourStrings> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + EXCUST_CODE + "," + EXCUST_NAME + ","
                + EXCUST_ADDRESS + " from " + TABLE_EXCUST;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                String key = cursor.getString(0);
                FourStrings value = new FourStrings(cursor.getString(1),
                        cursor.getString(2), "", "", "", "", "");
                temp.put(key, value);
            } while (cursor.moveToNext());
        }
        return temp;
    }

    public boolean isvalidtown(String name) {
        boolean temp = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + TOWN_PKID + " from " + TABLE_TOWN + " where "
                + TOWN_NAME + "='" + name + "'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }
    public String getcustomercategoriesId(String string) {
        String temp = "";
        string = string.replaceAll("'", "\'");
        String selectQuery = "SELECT " + CUSTOMERCATEGORIES_CODE + " FROM "
                + TABLE_CUSTOMERCATEGORIES + " WHERE "
                + CUSTOMERCATEGORIES_NAME + "= '" + string + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }
    public String gettownId(String string) {
        String temp = "";
        string = string.replaceAll("'", "\'");
        String selectQuery = "SELECT " + TOWN_CODE + " FROM " + TABLE_TOWN
                + " WHERE " + TOWN_NAME + "= '" + string + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }
// schedule begin status

    public String getschedulebegintime() {

        String Schedulsttatus = "";
        String get_schedulestatus = "SELECT " + SCHEDULE_BEGIN_TIME + " FROM "
                + TABLE_SCHEDULE_HEADER + " WHERE  " + SCHEDULE_BEGIN_TIME
                + " IS NOT NULL";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(get_schedulestatus, null);
        if (cursor.moveToFirst()) {

            Schedulsttatus = cursor.getString(0);
        }

        if (Schedulsttatus == null)
            Schedulsttatus = "";
        if (cursor != null)
            cursor.close();
        return Schedulsttatus;
    }
    // //adding single location
    public void addnewlocation(Locations loc) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LOCATIONID, loc.Locationid);
        values.put(NAME, loc.Locationname);
        values.put(ADDRESS, loc.Locationadress);
        values.put(LOCATION_LATITUDE, loc.Latitude);
        values.put(LOCATION_LONGITUDE, loc.Longitude);
        values.put(LOCATION_CREATETIME, loc.createtime);
        //values.put(CUSTOMER_CODE, loc.costomercode);
        values.put(CUSTOMER_CATOGORY, loc.customercategory);
        values.put(TIN_NUMBER, loc.tinnumber);
        values.put(CONTACT_PERSONNAME, loc.contactpersonvalue);
        values.put(CONTACT_PERSON_NUMBER, loc.Contactpersonnumbervalue);
        values.put(TOWNCLASS, loc.townclassvalue);
        values.put(CUSTOMER_SCHEDULELINE_GUID, loc.schdulineguid);
        values.put(LOCATION_OTPVERIFIED, loc.otpverified);
        values.put(LOCATION_LOCPROVIDER, loc.locprovider);
        values.put(LOCATION_LOCACCURACY, loc.locaccuracy);
        values.put(LOCATION_PHOTOUUID, loc.photouuid);
        values.put(LOCATION_CLOSING_DAY, loc.closingday);
        values.put(LOCATION_LATITUDEPHOTO, loc.latitudePhoto);
        values.put(LOCATION_LONGITUDEPHOTO, loc.longitudePhoto);
        values.put(LOCATION_REMARKS, loc.remarks);
        values.put(LOCATION_GSTTYPE, loc.gstType);
        values.put(LOCATION_FSSI, loc.fssi);
        values.put(CUSTOMERCATEGOY_CODE, loc.customercatogorycode);
        db.insert(TABLE_LOCATIONS, null, values);

    }
    // new locationscheduleexist
    public String isnewlocationsexist() throws JSONException {

        String result = "0";

        String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE_HEADER
                + " WHERE " + SCHEDULE_NAME + "=" + "'New Locations'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            result = cursor.getString(0);

        }
        if (cursor != null)
            cursor.close();
        return result;
    }
    public void firstlocationadd(Scheduledata header, Scheduledetail dettail)

    {

        Long key;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SCHEDULE_ID, header.Scheduleid);
        values.put(SCHEDULE_NAME, header.Schedulename);
        values.put(SALESPERSON_ID, header.Salespersonid);
        values.put(SALESPERSON_NAME, header.salespersonname);
        values.put(SALEROUTE_ID, header.Salesrouteid);
        values.put(SCHEDULE_DATE, header.Scheduledate);
        values.put(HEADER_STATUS, header.Headerstatus);
        values.put(ROUTENETWORK_ID, header.Routenetworkid);
        values.put(SCHEDULE_BEGIN_TIME, header.begintime);
        key = db.insert(TABLE_SCHEDULE_HEADER, null, values);

        ContentValues values2 = new ContentValues();
        values2.put(SCHEDULE_DETAIL_ID, dettail.Scheduledetailid);
        values2.put(SCHEDULE_HEADER_ID, String.valueOf(key));
        values2.put(LOCATION_ID, dettail.locationid);
        values2.put(LOCATION_NAME, dettail.locationname);
        values2.put(LOCATION_ADRESS, dettail.locationadress);
        values2.put(SEQNO, dettail.sequencenumber);
        values2.put(DETAIL_STATUS, dettail.status);
        values2.put(LATITUDE, dettail.latitude);
        values2.put(LONGITUDE, dettail.longitude);
        values2.put(LATITUDE_NEW, "0");
        values2.put(LONGITUDE_NEW, "0");
        values2.put(SCHEDULEDETAIL_CREATETIME, "0");
        values2.put(CLOSING_DAY_UPDATETIME, "0");
        values2.put(CUSTOMER_CATOGORY, dettail.Customercatogory);
        values2.put(CUSTOMERCATOGORYID, dettail.CustomercatogoryId);
        values2.put(INVOCENUMBER, dettail.invoicenumber);
        values2.put(PICKLISTNUMBER, dettail.picklistnumber);
        values2.put(SCHED_CUSTOMERCODE, dettail.customercode);
        values2.put(LOCATIONLOCK, dettail.locationlock);
        values2.put(CLOSING_DAY, dettail.Closingday);
        values2.put(SCHEDULEDETAIL_MOBILE, dettail.mobilenumber);
        values2.put(SCHEDULEDETAIL_TINNUMBER, dettail.tinnumber);

        values2.put(SCHEDULEDETAIL_TOWN, dettail.town);
        values2.put(SCHEDULEDETAIL_CONTACTPERSON, dettail.contactperson);
        values2.put(SCHEDULEDETAIL_OTHERCOMPANY, dettail.othercompany);
        values2.put(SCHEDULEDETAIL_OTPVERIFIED, dettail.otpverified);
        values2.put(SCHEDULEDETAIL_LOCPROVIDER, dettail.locprovider);
        values2.put(SCHEDULEDETAIL_LOCACCURACY, dettail.locaccuracy);
        values2.put(SCHEDULEDETAIL_PHOTOUUID, dettail.photoUUID);

        values2.put(CLOSING_DAY, dettail.Closingday);

        values2.put(SCHEDULEDETAIL_GSTTYPE, dettail.gsttype);
        values2.put(SCHEDULEDETAIL_FSSI, dettail.fssi);
        values2.put(SCHEDULEDETAIL_STATE_CODE, dettail.stateCode);
        db.insert(TABLE_SCHEDULE_DETAIL, null, values2);

    }
    public void addtoexistingnewlocations(String scheduleheadid,
                                          Scheduledetail dettail) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values2 = new ContentValues();
        values2.put(SCHEDULE_DETAIL_ID, dettail.Scheduledetailid);
        values2.put(SCHEDULE_HEADER_ID, scheduleheadid);
        values2.put(LOCATION_ID, dettail.locationid);
        values2.put(LOCATION_NAME, dettail.locationname);
        values2.put(LOCATION_ADRESS, dettail.locationadress);
        values2.put(SEQNO, dettail.sequencenumber);
        values2.put(DETAIL_STATUS, dettail.status);
        values2.put(LATITUDE, dettail.latitude);
        values2.put(LONGITUDE, dettail.longitude);
        values2.put(LATITUDE_NEW, "0");
        values2.put(LONGITUDE_NEW, "0");
        values2.put(SCHEDULEDETAIL_CREATETIME, "0");
        values2.put(CLOSING_DAY_UPDATETIME, "0");
        values2.put(CUSTOMER_CATOGORY, dettail.Customercatogory);
        values2.put(CUSTOMERCATOGORYID, dettail.CustomercatogoryId);
        values2.put(INVOCENUMBER, dettail.invoicenumber);
        values2.put(PICKLISTNUMBER, dettail.picklistnumber);
        values2.put(SCHED_CUSTOMERCODE, dettail.customercode);
        values2.put(LOCATIONLOCK, dettail.locationlock);
        values2.put(CLOSING_DAY, dettail.Closingday);
        values2.put(SCHEDULEDETAIL_MOBILE, dettail.mobilenumber);
        values2.put(SCHEDULEDETAIL_TINNUMBER, dettail.tinnumber);

        values2.put(SCHEDULEDETAIL_TOWN, dettail.town);
        values2.put(SCHEDULEDETAIL_CONTACTPERSON, dettail.contactperson);
        values2.put(SCHEDULEDETAIL_OTHERCOMPANY, dettail.othercompany);
        values2.put(SCHEDULEDETAIL_OTPVERIFIED, dettail.otpverified);
        values2.put(SCHEDULEDETAIL_LOCPROVIDER, dettail.locprovider);
        values2.put(SCHEDULEDETAIL_LOCACCURACY, dettail.locaccuracy);
        values2.put(SCHEDULEDETAIL_PHOTOUUID, dettail.photoUUID);



        values2.put(SCHEDULEDETAIL_GSTTYPE, dettail.gsttype);
        values2.put(SCHEDULEDETAIL_FSSI, dettail.fssi);
        values2.put(SCHEDULEDETAIL_STATE_CODE, dettail.stateCode);
        long id = db.insert(TABLE_SCHEDULE_DETAIL, null, values2);

        System.out.println("id:"+id);

    }

    public void editCustomerPhoto(String scheduledetailid, String uuid,
                                  String time, String imglat, String imglong) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SCHEDULEDETAIL_EDITTIME, time);
        values.put(SCHEDULEDETAIL_PHOTOUUID, uuid);
        values.put(SCHEDULEDETAIL_IMGLAT, imglat);
        values.put(SCHEDULEDETAIL_IMGLONG, imglong);
        db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_ID + "=?",
                new String[] { scheduledetailid });
    }

    public JSONObject getallcustdata(String scheduledetailid) throws Exception {
        JSONObject temp = new JSONObject();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select * from " + TABLE_SCHEDULE_DETAIL + " where "
                + SCHEDULE_DETAIL_ID + "='" + scheduledetailid + "'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put("CustCode", cursor.getString(cursor
                        .getColumnIndex(SCHED_CUSTOMERCODE)));
                temp.put("AlternateCustcode", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_ALTERCUSTCODE)));
                temp.put("AlternateCustCode", cursor.getString(cursor
                        .getColumnIndex(SCHED_CUSTOMERCODE)));
                temp.put("CustName",
                        cursor.getString(cursor.getColumnIndex(LOCATION_NAME)));
                temp.put("CustAddress", cursor.getString(cursor
                        .getColumnIndex(LOCATION_ADRESS)));
                temp.put("CustPhone", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_MOBILE)));
                temp.put("CustGst", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_TINNUMBER)));
                temp.put("CustCat", cursor.getString(cursor
                        .getColumnIndex(CUSTOMERCATOGORY)));
                temp.put("CustPan", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PAN)));
                temp.put("PhotoUUID", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PHOTOUUID)));
                temp.put("landmark", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_LANDMARK)));
                temp.put("town", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_TOWN)));
                temp.put("contactperson", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_CONTACTPERSON)));
                temp.put("othercompany", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_OTHERCOMPANY)));
                temp.put("closingday",
                        cursor.getString(cursor.getColumnIndex(CLOSING_DAY)));
                temp.put("gstType",
                        cursor.getString(cursor.getColumnIndex(SCHEDULEDETAIL_GSTTYPE)));
                temp.put("fssi",
                        cursor.getString(cursor.getColumnIndex(SCHEDULEDETAIL_FSSI)));
                temp.put("status",
                        cursor.getString(cursor.getColumnIndex(DETAIL_STATUS)));

            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }
    public void editCustomer(String scheduledetailid, Scheduledetail loc,
                             String crtime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LOCATION_ADRESS, loc.locationadress);
        values.put(SCHEDULEDETAIL_MOBILE, loc.mobilenumber);
        values.put(SCHEDULEDETAIL_TINNUMBER, loc.tinnumber);
        values.put(SCHEDULEDETAIL_PAN, loc.pan);
        values.put(SCHEDULEDETAIL_LANDMARK, loc.landmark);
        values.put(SCHEDULEDETAIL_TOWN, loc.town);
        values.put(SCHEDULEDETAIL_CONTACTPERSON, loc.contactperson);
        values.put(SCHEDULEDETAIL_OTHERCOMPANY, loc.othercompany);
        values.put(CLOSING_DAY, loc.Closingday);
        values.put(SCHEDULEDETAIL_SELPRODCAT, loc.selectedproductcategories);
        values.put(SCHEDULEDETAIL_ALTERCUSTCODE, loc.alternatecustcode);
        values.put(SCHEDULEDETAIL_EDITTIME, crtime);

        values.put(SCHEDULEDETAIL_GSTTYPE, loc.gsttype);
        values.put(SCHEDULEDETAIL_FSSI, loc.fssi);

        db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_ID + "=?",
                new String[] { scheduledetailid });
    }

    public HashMap<String, JSONObject> getVansalesSaleReportData() throws Exception{

        HashMap<String, JSONObject> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "Select  pt. "+PACKAGE_TYPE_CODE+",  p." + PRODUCT_NAME + " , sum(ol." + ORDERLINE_QTY + "*1 ) "
                + "from " + TABLE_ORDERLINE + " ol "
               + "join " + TABLE_ORDERHDR + " h on h." + ORDERHDR_PKID + " = ol." + ORDERLINE_HEADID
                + " join " + TABLE_PRODUCT + " p on p." + PRODUCT_ID + " = ol." + ORDERLINE_PRODID
                +" left join " + TABLE_PACKAGE_TYPE + " pt on pt." + PACKAGE_TYPE_ID + " = ol." + ORDERLINE_PACKAGE_TYPE
                +" WHERE ol."+ORDERLINE_DELETEFLAG+"= 0 and h."+ ORDERHDR_APPROVEFLAG + "='1' and h."
                +ORDERHDR_STATUS + " != 'Cancelled' "
                +" group by p." + PRODUCT_NAME;


        Cursor cursor = db.rawQuery(sql, null);
        int i =0 ;


        if (cursor.moveToFirst()) {
            do {
                i++;
                JSONObject tempObj = new JSONObject();
                tempObj.put("unit", cursor.getString(0));
                tempObj.put("productname", cursor.getString(1));
                tempObj.put("availableqty", cursor.getString(2));
                temp.put(String.valueOf(i), tempObj);

            } while (cursor.moveToNext());
        }

        if (cursor != null)
            cursor.close();
        return temp;
    }

    // get schedule status
    public String getschedulestatus() {

        String Schedulsttatus = "";
        String get_schedulestatus = "SELECT " + SCHEDULE_COMPLETION_STATUS
                + " FROM " + TABLE_SCHEDULE_HEADER + " WHERE  "
                + SCHEDULE_COMPLETION_STATUS + " ='Completed'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(get_schedulestatus, null);
        if (cursor.moveToFirst()) {

            Schedulsttatus = cursor.getString(0);
        }

        if (Schedulsttatus == null)
            Schedulsttatus = "";
        if (cursor != null)
            cursor.close();
        return Schedulsttatus;
    }

    public String gettaxname(String taxid) {
        SQLiteDatabase db = this.getWritableDatabase();
        String taxname = new String();
        String query = "SELECT " + TAX_NAME + " FROM " + TABLE_TAX + " WHERE "
                + TAX_ID + " = " + taxid;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {

            taxname = cursor.getString(0);
        }

        db.close();
        if (cursor != null)
            cursor.close();
        return taxname;
    }

    public JSONObject getproductratedetailsWithInventory(String product,
                                                         String custCat, String currentDate, String orgId,String customer,String invoiceNo) throws JSONException {

        JSONObject temp = new JSONObject();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + CUSTOMERCATEGORIES_NAME + " FROM "
                + TABLE_CUSTOMERCATEGORIES + " WHERE "
                + CUSTOMERCATEGORIES_CODE + "='" + custCat + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put("catname", cursor.getString(0));
            } while (cursor.moveToNext());
        }
        query = "SELECT " + PRODUCT_NAME + ", pct." + PRODUCTCAT_NAME
                + ", bnd." + BRAND_NAME + ", fm." + FORM_NAME + " FROM "
                + TABLE_PRODUCT + " p LEFT JOIN " + TABLE_PRODUCTCAT
                + " pct on p." + PRODUCT_CAT + "=pct." + PRODUCTCAT_ID
                + " LEFT JOIN " + TABLE_BRAND + " bnd ON bnd." + BRAND_ID
                + "=p." + PRODUCT_BRAND + " LEFT JOIN " + TABLE_FORM
                + " fm ON fm." + FORM_ID + "=p." + PRODUCT_FORM + " WHERE "
                + PRODUCT_ID + "='" + product + "'";
        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put("prdname", cursor.getString(0));
                temp.put("prdcat", cursor.getString(1));
                temp.put("brand", cursor.getString(2));
                temp.put("form", cursor.getString(3));
            } while (cursor.moveToNext());
        }
        query = " SELECT " + MRP_VALUE + ", " + MRP_CONSOFF + ", "
                + MRP_FLATOFF + ", " + MRP_TAX + " FROM " + TABLE_MRP
                + " WHERE " + MRP_PRODUCT + "='" + product + "'";
        // AND " + MRP_FROM + "<='" + currentDate + "' AND " + MRP_TO + ">='" +
        // currentDate + "'";// date validation removed by ansar
        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put("mrp", cursor.getString(0));
                temp.put("consoff", cursor.getString(1));
                temp.put("flat", cursor.getString(2));
                temp.put("tax", cursor.getString(3));
            } while (cursor.moveToNext());
        }
        query = " SELECT " + PARTYMARG_VALUE + " FROM " + TABLE_PARTYMARG
                + " WHERE " + PARTYMARG_PRODUCT + "='" + product + "' AND "
                // + PARTYMARG_FROM + "<='" + currentDate + "' AND "
                // + PARTYMARG_TO + ">='" + currentDate + "' AND "
                // date validation removed by ansar
                + PARTYMARG_CAT + "='" + custCat +
                "' or "+PARTYMARG_CUSTOMER+"='"+customer+"'";
        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put("party", cursor.getString(0));
            } while (cursor.moveToNext());
        }
        query = " SELECT " + ADDLDISC_VALUE + " FROM " + TABLE_ADDLDISC
                + " WHERE "
                // + MRP_FROM + "<='" + currentDate + "' AND " + MRP_TO + ">='"
                // + currentDate + "' AND " // date validation removed by ansar
                + ADDLDISC_CAT + "='" + custCat + "'";
        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put("adldis", cursor.getString(0));
            } while (cursor.moveToNext());
        }

        double discPer = 0;

		/*
		 * OFFER STRUCTURE CHANGED----------
		 * try{
			String sql="select "+OFFERS_OFFVALUE+ " from "+TABLE_OFFERS+
					" off join "+TABLE_PRODUCT+" p on p."+PRODUCT_CAT+"= off."+OFFERS_PRODUCTCATEGORY+"    where off."+
					OFFERS_PRODUCT+"= '"+product+"'  OR  p."+PRODUCT_ID+" = '"+product+"'";

			//Log.d("Discount sql", sql) ;
			cursor =db.rawQuery(sql, null);
			if(cursor.moveToFirst()){
				discPer=Double.parseDouble(cursor.getString(0));
			}

		}catch(Exception e) {
			//Log.e("Discount error", e.getMessage()) ;
		}
		 */
        temp.put("discountper", discPer) ;
        /*
         * query = " SELECT " + INVENTORY_QTY + " FROM " + TABLE_INVENTORY +
         * " WHERE " + INVENTORY_PRODUCT + "='" + product + "'";
         */

        if (invoiceNo.length() == 0) {


            query = "SELECT SUM(" + INVENTORYJRNLLINE_QTYDEBIT + "),SUM("
                    + INVENTORYJRNLLINE_QTYCREDIT + ")  AS QtyAvailable"
                    + ",u."+UNITS_NAME+" as unit FROM "
                    + TABLE_INVENTORYJRNLHDR + " j " + " JOIN "
                    + TABLE_INVENTORYJRNLLINE + " jl ON j." + INVENTORYJRNLHDR_ID
                    + " = jl." + INVENTORYJRNLLINE_HDR + " JOIN "
                    + TABLE_INVENTORYACCOUNT + " a ON a." + INVENTORYACCOUNT_ID
                    + " = jl." + INVENTORYJRNLLINE_ACCOUNT
                    +" LEFT JOIN "+TABLE_UNITS+" u ON u."+UNITS_ID+" = j."+INVENTORYJRNLHDR_UOM
                    +" WHERE  a."
                    + INVENTORYACCOUNT_NAME + "='Available Stock' " + " and j."
                    + INVENTORYJRNLHDR_PRODUCT + " = " + product + " and ( j."
                    + INVENTORYJRNLHDR_ORG + " = " + orgId + " OR  j."
                    + INVENTORYJRNLHDR_ORG + " = 0 )" + " GROUP BY j."
                    + INVENTORYJRNLHDR_PRODUCT;

        }else{
            query = "SELECT SUM(" + INVENTORYJRNLLINE_QTYDEBIT + "),SUM("
                    + INVENTORYJRNLLINE_QTYCREDIT + ")  AS QtyAvailable"
                    + ",u."+UNITS_NAME+" as unit FROM "
                    + TABLE_INVENTORYJRNLHDR + " j " + " JOIN "
                    + TABLE_INVENTORYJRNLLINE + " jl ON j." + INVENTORYJRNLHDR_ID
                    + " = jl." + INVENTORYJRNLLINE_HDR + " JOIN "
                    + TABLE_INVENTORYACCOUNT + " a ON a." + INVENTORYACCOUNT_ID
                    + " = jl." + INVENTORYJRNLLINE_ACCOUNT
                    +" LEFT JOIN "+TABLE_UNITS+" u ON u."+UNITS_ID+" = j."+INVENTORYJRNLHDR_UOM+" WHERE  a."
                    + INVENTORYACCOUNT_NAME + "='Available Stock' " + " and j."
                    + INVENTORYJRNLHDR_PRODUCT + " = " + product + " and ( j."
                    + INVENTORYJRNLHDR_ORG + " = " + orgId + " OR  j."
                    + INVENTORYJRNLHDR_ORG + " = 0 ) " +
                    " and (  j."+INVENTORYJRNLHDR_SOURCEID+" != '"+invoiceNo+"' )"
                    + " GROUP BY j." + INVENTORYJRNLHDR_PRODUCT;
        }

        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String debitQty = cursor.getString(0);
                String creditQty = cursor.getString(1);
                String unit = cursor.getString(2);
                temp.put(
                        "inventory",
                        Double.parseDouble(debitQty)
                                - Double.parseDouble(creditQty));
                temp.put("unit",unit);
                // temp.put("inventory", cursor.getString(0));
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;

    }

    public String applyTax ( String orderId, String taxType,boolean gstRegistered,String scheduleLineId,boolean complimentFlag)throws  Exception{

        HashMap<String, OrderLine> orderLineHashMap = getSavedOrderLineData(orderId);
        String returnString="";
        for (Map.Entry<String, OrderLine> entry : orderLineHashMap.entrySet()) {

            String tempProductId = entry.getKey();
            OrderLine line = entry.getValue();

            if(line.isComplimentFlag()){
                tempProductId =  entry.getKey().split("_")[0];
            }

            final String productId =tempProductId;
            String taxId = line.getTaxId()+"";

            BigDecimal qty = new BigDecimal(line.getQty());
            BigDecimal rate =   line.getRate();
            BigDecimal total =qty.multiply(rate);
            BigDecimal totalTaxRate = getTaxRate(taxId,taxType );
            BigDecimal taxableamnt = new BigDecimal(0);
            BigDecimal discAmnt = new BigDecimal(0);

            BigDecimal temp1 = total.multiply(new BigDecimal(100));
            BigDecimal temp2 = totalTaxRate.add(new BigDecimal(100));
            taxableamnt =temp1.divide(temp2,5,BigDecimal.ROUND_HALF_UP  /*ROUND_HALF_EVEN*/ ) ;

            /**
             * Discount amount calculation
             */
            BigDecimal discPer =  line.getDiscountPer();


            if (discPer.compareTo(new BigDecimal(0)) == 1) {

                //Discount amount calculated on taxable amount

                discAmnt = line.getDiscountAmount();

//                taxableamnt = taxableamnt - discAmnt;
                taxableamnt = taxableamnt.subtract(discAmnt) ;
            }

            HashMap<String,JSONObject> taxLineDetails = getTax(taxId,productId,orderId,taxType);

            if(taxLineDetails.size() == 0){
                returnString="Tax Not Available for "+line.getProductName();

            }

            for (Map.Entry<String, JSONObject> entry1 : taxLineDetails.entrySet()) {

                String taxLineId = entry1.getKey();
                JSONObject taxLineObject = entry1.getValue();
                String taxrate=taxLineObject.optString("taxrate","nil");
                String taxdescrip=taxLineObject.optString("taxdescrip","nil");

             /*   if (gstRegistered && taxdescrip.toUpperCase().contains("CESS")){
                    taxrate ="0" ;
                    continue;
                }*/

                BigDecimal taxRate = new BigDecimal(taxrate);
                String  taxcode=taxLineObject.optString("taxname", "nil");

                String  taxtype=taxLineObject.optString("taxtype","nil");
                String orderLineId=taxLineObject.optString("lineid","nil");


                temp1 = taxableamnt.multiply(taxRate);
                BigDecimal taxAmount =temp1.divide(new BigDecimal(100),5,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/) ;


                taxAmount = taxAmount.setScale(5,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);

                if (gstRegistered && taxdescrip.toUpperCase().contains("CESS")){
                    /*Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    String createdtime = df.format(c.getTime());
                    BigDecimal taxPerUnit = taxAmount.divide(qty);
                    rate = rate.subtract(taxPerUnit) ;
                    updateproduct(orderId,productId,qty.toString(),rate.toString(),createdtime,scheduleLineId,complimentFlag,taxtype);*/
                }else{
                    insertOrderTaxline(taxLineId,taxableamnt,taxAmount,taxcode,taxrate,orderLineId,orderId);
                }


            }



        }

        return  returnString ;


    }

    // get customer search data
    public JSONArray getcustomersearchdata(String customername)
            throws JSONException {

        JSONArray customerdata = new JSONArray();
        String selectQuery = "SELECT " + "CS." + SEARCHCUSTOMERS_ID + ","
                + "CS." + SEARCHCUSTOMERS_NAME + "," + "CS."
                + SEARCHCUSTOMERS_ADRESS + "," + "CS." + SEARCHCUSTOMERS_CODE
                + "," + "CS." + SEARCHCUSTOMERS_LATTITUDE + "," + "CS."
                + SEARCHCUSTOMERS_LONGITUDE + "," + "CS."
                + SEARCHCUSTOMERS_CATEGORY + "," + "INV." + INVOICE_NO + ","
                + "CS." + SEARCHCUSTOMERS_LOCATIONLOCK + "," + "CS."
                + SEARCHCUSTOMERS_CLOSINGDAY + "," + "CS."
                + SEARCHCUSTOMERS_MOBILENUMBER + ", CS."
                + SEARCHCUSTOMERS_CUSTTYPE + " FROM " + TABLE_SEARCHCUSTOMERS
                + " CS LEFT JOIN " + TABLE_INVOICE + " INV ON  INV."
                + INVOICE_CUSTOMERID + "=" + "CS." + SEARCHCUSTOMERS_ID
                + " WHERE " + SEARCHCUSTOMERS_NAME + " LIKE '%" + customername
                + "%'" + " OR " + SEARCHCUSTOMERS_CODE + " LIKE '%"
                + customername + "%'" + " GROUP BY CS." + SEARCHCUSTOMERS_ID
                + " ORDER BY " + "INV." + INVOICE_NO + " DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();

                obj.put("custid", cursor.getString(0));
                obj.put("custname", cursor.getString(1));
                obj.put("adress", cursor.getString(2));
                obj.put("custcode", cursor.getString(3));
                obj.put("custlat", cursor.getString(4));
                obj.put("custlong", cursor.getString(5));
                obj.put("custcategory", cursor.getString(6));
                obj.put("invoicestatus", cursor.getString(7));
                obj.put("locationlock", cursor.getString(8));
                obj.put("closingday", cursor.getString(9));
                obj.put("mobile", cursor.getString(10));
                obj.put("custtype", cursor.getString(11));
                customerdata.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();

        return customerdata;

    }
// ///adding new location////////////

    public HashSet<String> getcurrentlocationids() {
        HashSet<String> ids = new HashSet<String>();
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT " + LOCATION_ID + " FROM "
                + TABLE_SCHEDULE_DETAIL;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {

                ids.add(cursor.getString(0));
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return ids;

    }
    // new locationwebexist
    public String isnewlocationsexistwebsearch() throws JSONException {

        String result = "0";

        String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE_HEADER
                + " WHERE " + SCHEDULE_NAME + "="
                + "'New Locations (Off Route Customers)'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            result = cursor.getString(0);

        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    /**
     * Shop Activity
     */

    public JSONObject getCustomerSummary(String scheduledetailid)
            throws Exception {
        JSONObject temp = new JSONObject();
        double collected = 0.0;
        double target = 0.0;
        try {
            SQLiteDatabase db = getWritableDatabase();
            String get_tottalcollectedamount = "SELECT SUM(" + AMOUNT
                    + ") FROM " + TABLE_PAYMENTS + " WHERE " + PAYMENT_METHOD
                    + "!= 'Return' AND " + PAYMENT_METHOD + "!= 'Credit'"
                    + " AND " + DELETE_FLAG + "=0" + " AND "
                    + PAYMENT_APROOVEFLAG + "=1 " + " AND "
                    + PAYMENT_SCHEDULEDETAILID + "= '" + scheduledetailid + "'";

            Cursor cursor = db.rawQuery(get_tottalcollectedamount, null);
            if (cursor.moveToFirst()) {
                temp.put("collected", cursor.getString(0));
            }
            String get_artarget = "select sum (inv.`staticbalance`) from invoice inv join scheduledetail schdtl on  schdtl.`locationid`=inv.`customerid` "
                    + " AND schdtl."
                    + SCHEDULE_DETAIL_ID
                    + " ='"
                    + scheduledetailid + "'";
            cursor = db.rawQuery(get_artarget, null);
            if (cursor.moveToFirst()) {
                temp.put("target", cursor.getString(0));
            }

			/*String count = "SELECT (SUM(" + ORDERLINE_QTY + "*"
					+ ORDERLINE_RATE + "* (select mp." + UNITSMAP_UPC
					+ " from " + TABLE_UNITSMAP + " mp where mp."
					+ UNITSMAP_PRODUCT + "=OL." + ORDERLINE_PRODID + " and mp."
					+ UNITSMAP_UOM + "=OL." + ORDERLINE_UPC
					+ " limit 1))), COUNT(1) FROM " + TABLE_ORDERLINE
					+ " OL JOIN " + TABLE_ORDERHDR + " HD ON HD."
					+ ORDERHDR_PKID + "=OL." + ORDERLINE_HEADID + " WHERE HD."
					+ ORDERHDR_SCHLINEID + "='" + scheduledetailid + "' AND "
					+ ORDERLINE_DELETEFLAG + "!=1 and " + ORDERHDR_APPROVEFLAG
					+ ">'0'";*/
            String count = "SELECT (SUM(" + ORDERLINE_QTY + "*"
                    + ORDERLINE_RATE + ") ), COUNT(1) FROM " + TABLE_ORDERLINE
                    + " OL JOIN " + TABLE_ORDERHDR + " HD ON HD."
                    + ORDERHDR_PKID + "=OL." + ORDERLINE_HEADID + " WHERE HD."
                    + ORDERHDR_SCHLINEID + "='" + scheduledetailid + "' AND "
                    + ORDERLINE_DELETEFLAG + "!=1 ";
            //and " + ORDERHDR_APPROVEFLAG
            //       + ">'0'";

            cursor = db.rawQuery(count, null);
            if (cursor.moveToFirst()) {
                temp.put("orderamt", cursor.getString(0));
                temp.put("ordercount", cursor.getString(1));
            }
            if (cursor != null)
                cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }
   /* public Double gettaxableamnt(String orderid) {
        SQLiteDatabase db = this.getWritableDatabase();
        double taxableamt = 0.0;
        String sql = "select SUM(" + ORDERTAXLINE_AMOUNT
                + ") as taxableamt FROM " + TABLE_ORDERTAXLINE + " where "
                + ORDERTAXLINE_ORDRDID + " ='" + orderid + "' and "
                + ORDERTAXLINE_CODE + " LIKE 'CGST%'  and "+ORDERTAXLINE_DELETEFLAG +" = '0'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            taxableamt = cursor.getDouble(0);
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return taxableamt;
    }

    public Double gettaxtot(String orderid) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        double taxtot = 0.0;
        String sql = "select SUM(" + ORDERTAXLINE_TAXAMOUNT
                + ") as taxamount FROM " + TABLE_ORDERTAXLINE + " where "
                + ORDERTAXLINE_ORDRDID + " ='" + orderid + "' and "+ORDERTAXLINE_DELETEFLAG +" = '0'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {

            taxtot = cursor.getDouble(0);
            DecimalFormat twoDForm = new DecimalFormat("#.##");
            taxtot = Double.parseDouble(twoDForm.format(taxtot)) ;
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return taxtot;
    }*/
    // get reson data from cache
    public JSONArray getreasons() throws JSONException {
        JSONArray resons = new JSONArray();
        String selectQuery = "SELECT " + REASONS_CODE + "," + REASON_DETAIL
                + " FROM " + TABLE_REASONS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("reasoncode", cursor.getString(0));
                obj.put("resondetail", cursor.getString(1));
                resons.put(obj);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return resons;
    }

    // /update schedule line with no visit status
    public void updatenovisitstatus(String scheduledetailid, String reason,
                                    String time, String createtime, String lattitude, String longitude,
                                    String latreason, String lngreason) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(STOREVISIT_REASON, reason);
        values.put(STOREVISIT_TIME, time);
        values.put(SCHEDULEDETAIL_CREATETIME, createtime);
        values.put(STOREVISIT_LATTITUDE, lattitude);
        values.put(STOREVISIT_LONGITUDE, longitude);
        values.put(SCHEDULEDETAIL_LATTITUDE, latreason);
        values.put(SCHEDULEDETAIL_LONGITUDE, lngreason);

        db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_ID + "=?",
                new String[] { scheduledetailid });

    }


    public String readordernote(String orderid) {
        // TODO Auto-generated method stub
        String temp = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String query = "SELECT " + ORDERHDR_NOTE + " FROM "
                    + TABLE_ORDERHDR + " WHERE " + ORDERHDR_PKID + " ='"
                    + orderid + "'";
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    temp = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }
    public void addordernote(String orderid, String data_note) {
        // TODO Auto-generated method stub
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ORDERHDR_NOTE, data_note);
        db.update(TABLE_ORDERHDR, values, ORDERHDR_PKID + "=?",
                new String[] { orderid });
        // db.close();
        /*
         * SQLiteDatabase db = this.getWritableDatabase(); ContentValues values
         * = new ContentValues(); values.put(CLOSING_DAY, day);
         * values.put(CLOSING_DAY_UPDATETIME, createtime);
         * db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_ID + "=?",
         * new String[] { scheduledetailid });
         */

    }
    // save denomination details

    public void saveschedulecompletiondata(String scheduleid,
                                           String denominationdata, String personid, String createtime,
                                           String submittime, String lat, String longi, String uniqueid,
                                           String totalamount, String scheduleidlocal, String createttime,
                                           String begintime) throws DateError {

        if (!validateCreateTime(createttime)) {
            throw new DateError();
        }
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SCHEDULESUMMARY_CREATETIME, createtime);
        values.put(SCHEDULESUMMARY_SUBMITTIME, submittime);
        values.put(SCHEDULESUMMARY_PERSONID, personid);
        values.put(SCHEDULESUMMARY_LAT, lat);
        values.put(SCHEDULESUMMARY_LONGI, longi);
        values.put(SCHEDULESUMMARY_UNIQUEID, uniqueid);
        values.put(SCHEDULESUMMARY_DENOMEDATA, denominationdata);
        values.put(SCHEDULESUMMARY_TOTTALCASH, totalamount);
        values.put(SCHEDULESUMMARY_SCHEDULEID, scheduleid);
        values.put(SCHEDULESUMMARY_SCHEDULEIDLOCAL, scheduleidlocal);
        values.put(SCHEDULESUMMARY_COMPLETION_TIME, createttime);
        values.put(SCHEDULESUMMARY_BEGIN_TIME, begintime);
        long insetrtedid = db.insert(TABLE_SCHEDULESUMMARY, null, values);

    }
    public boolean orderDataExist() {

        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERLINE  ;

        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            result = true;
            return result;
        }
        if (cursor != null)
            cursor.close();

        return result;
    }

    // ////get lock status

    public String getlocakstatus(String scheduledetailid) throws JSONException {
        scheduledetailid = "'" + scheduledetailid + "'";

        String locationlock = "0";
        String selectQuery = "SELECT " + LOCATIONLOCK + " FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_ID
                + " = " + scheduledetailid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            locationlock = cursor.getString(0);

        }
        if (cursor != null)
            cursor.close();
        return locationlock;

    }

    /**
     * Called from Shop Activity- Location Update
     * @param scheduledetailid
     * @param latitude
     * @param longitude
     * @param createtime
     * @param redabledate
     */
    public void updatelatlongcoustomertable(String scheduledetailid,
                                            String latitude, String longitude, String createtime,
                                            String redabledate) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LATITUDE_NEW, latitude);
        values.put(LONGITUDE_NEW, longitude);
        values.put(SCHEDULEDETAIL_CREATETIME, createtime);
        values.put(SCHED_LOCUPDATETIME, redabledate);

        db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_ID + "=?",
                new String[] { scheduledetailid });

    }
    //Added By Jithu for getting schedule name from schedule id for daily sales report
    public String grtSchdlNameFrmschdlId(String schdlId){
        String schdlName = "0";
        String get_schedulebegin = "SELECT " + SCHEDULE_NAME + " FROM "
                + TABLE_SCHEDULE_HEADER + " WHERE  " + SCHEDULE_ID + "='"
                + schdlId + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(get_schedulebegin, null);
        if (cursor.moveToFirst()) {
            schdlName = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return schdlName;
    }
    //Added bu jithu on 23/05/2019 for getting target quantity
    public double getTargetQty(){
        SQLiteDatabase db = this.getWritableDatabase();
        double amnt = 0.0;
        String sql = "select SUM(" + SCHEDULEDETAIL_TARGET_VALUE
                + ") as target FROM " + TABLE_SCHEDULE_DETAIL + " where "
                + SCHEDULEDETAIL_TARGET_TYPE  + " ='Quantity'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {

            amnt = cursor.getDouble(0);
            DecimalFormat twoDForm = new DecimalFormat("#.##");
            amnt = Double.parseDouble(twoDForm.format(amnt)) ;
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return amnt;
    }

    public double getdistance() {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select ifnull(sum(" + KEY_DISTANCE + "),0) from "
                + TABLE_NAVIGATIONDATA;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            return (cursor.getDouble(0) / 1000);
        }
        return 0;
    }


    public void saveVisibilityPhoto(CustomerVisibility customerVisibility)
            throws DateError {

        if (!validateCreateTime(customerVisibility.createtime)) {
            throw new DateError();
        }

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CUSTOMER_PHOTO_VISIBILITY_CREATETIME, customerVisibility.createtime);
        values.put(CUSTOMER_PHOTO_VISIBILITY_SCHEDULE_LINE, customerVisibility.scheduleLineId);
        values.put(CUSTOMER_PHOTO_VISIBILITY_PHOTO_ID, customerVisibility.photoUUID);
        values.put(CUSTOMER_PHOTO_VISIBILITY_LAT, customerVisibility.latitude);
        values.put(CUSTOMER_PHOTO_VISIBILITY_LONG, customerVisibility.longtitude);
        values.put(CUSTOMER_PHOTO_VISIBILITY_ACCURACY, customerVisibility.accuracy);
        values.put(CUSTOMER_PHOTO_VISIBILITY_DELETEFLAG, "0");

        long id = db.insert(TABLE_CUSTOMER_PHOTO_VISIBILITY, null, values);

    }

    public String getCustomerVisibilityId(String scheduleLineId){

        String id = "0";
        String qry = "SELECT " + CUSTOMER_PHOTO_VISIBILITY_PKID
                +" FROM "
                + TABLE_CUSTOMER_PHOTO_VISIBILITY +
                " WHERE  " + CUSTOMER_PHOTO_VISIBILITY_SCHEDULE_LINE + "='"
                + scheduleLineId + "'  AND "+CUSTOMER_PHOTO_VISIBILITY_DELETEFLAG+" = '0'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(qry, null);
        if (cursor.moveToFirst()) {
            id = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();

        return  id ;

    }

    public CustomerVisibility getCustomerVisibilityPhoto(String visibilityId){

        CustomerVisibility customerVisibility = new CustomerVisibility();
        String qry = "SELECT " + CUSTOMER_PHOTO_VISIBILITY_CREATETIME + ","
                +CUSTOMER_PHOTO_VISIBILITY_SCHEDULE_LINE+ ","
                +CUSTOMER_PHOTO_VISIBILITY_PHOTO_ID+ ","
                +CUSTOMER_PHOTO_VISIBILITY_LAT+ ","
                +CUSTOMER_PHOTO_VISIBILITY_LONG+ ","
                +CUSTOMER_PHOTO_VISIBILITY_ACCURACY
                +" FROM "
                + TABLE_CUSTOMER_PHOTO_VISIBILITY + " WHERE  "
                + CUSTOMER_PHOTO_VISIBILITY_PKID + "='"
                + visibilityId + "'   AND "+CUSTOMER_PHOTO_VISIBILITY_DELETEFLAG+" = '0'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(qry, null);
        if (cursor.moveToFirst()) {
            customerVisibility = new CustomerVisibility( cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5));
        }
        if (cursor != null)
            cursor.close();
        return customerVisibility;
    }

    public JSONArray getCustomerVisibilityPhotoSyncData(String personId, boolean backup){

        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }

        JSONArray data = new JSONArray();
        try {
            String qry = "SELECT " + CUSTOMER_PHOTO_VISIBILITY_CREATETIME + ","
                    + CUSTOMER_PHOTO_VISIBILITY_SCHEDULE_LINE + ","
                    + CUSTOMER_PHOTO_VISIBILITY_PHOTO_ID + ","
                    + CUSTOMER_PHOTO_VISIBILITY_LAT + ","
                    + CUSTOMER_PHOTO_VISIBILITY_LONG + ","
                    + CUSTOMER_PHOTO_VISIBILITY_ACCURACY
                    + " FROM "
                    + TABLE_CUSTOMER_PHOTO_VISIBILITY
                    + " WHERE " + CUSTOMER_PHOTO_VISIBILITY_CREATETIME + ">'" + lastsyc + "' " +
                    "AND "+CUSTOMER_PHOTO_VISIBILITY_DELETEFLAG +" = '0'" ;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(qry, null);
            if (cursor.moveToFirst()) {

                do {

                    JSONObject tempObject = new JSONObject();
                    tempObject.put("TakenOn", cursor.getString(0));
                    tempObject.put("ScheduleLineId", cursor.getString(1));
                    tempObject.put("ImageId", cursor.getString(2));
                    tempObject.put("TakenBy", personId);
                    data.put(tempObject);
                } while (cursor.moveToNext());

            }
            if (cursor != null)
                cursor.close();

        }catch (JSONException e){
            e.printStackTrace();
        }

        return data ;
    }


    public void deleteVisibilityPhoto(String visibilityId , String deleteTime) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CUSTOMER_PHOTO_VISIBILITY_DELETEFLAG, "1");
        values.put(CUSTOMER_PHOTO_VISIBILITY_CREATETIME,  deleteTime);
         db.update(TABLE_CUSTOMER_PHOTO_VISIBILITY, values, CUSTOMER_PHOTO_VISIBILITY_PKID + "=?",
                new String[] { visibilityId });

    }

    //Added By Jithu for generate audit report om 13/06/2019
    public JSONArray getVanSalesDataForAuditReport() throws JSONException {
        JSONArray orderdata = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
      /*  String query = "SELECT hrd." + ORDERHDR_CREATETIME + " as hdrtime,hrd.approveflag ,hrd." + ORDERHDR_PARENT_INVOICENO + ", hrd." + ORDERHDR_STATUS +
                ", oln.*,hrd." + ORDERHDR_SCHLINEID + ", hrd."
                + ORDERHDR_CUSTOMER + ", hrd." + ORDERHDR_INVOICENO + ", hrd."
                + ORDERHDR_LATITUDE + ", hrd." + ORDERHDR_LONGITUDE + " , hrd."
                + ORDERHDR_NOTE + ",SUM(tl." + ORDERTAXLINE_TAXAMOUNT
                + ") as taxamount" + ",SUM(tl." + ORDERTAXLINE_RATE
                + ") as taxrate,tl." + ORDERTAXLINE_AMOUNT
                + " as taxableamount, (SUM(tl." + ORDERTAXLINE_TAXAMOUNT
                + ")+tl." + ORDERTAXLINE_AMOUNT + ") as totalamount, " + "oln."
                + ORDERLINE_TAX + " FROM " + TABLE_ORDERLINE + " oln JOIN "
                + TABLE_ORDERHDR + " hrd on hrd." + ORDERHDR_PKID + "=oln."
                + ORDERLINE_HEADID + " JOIN " + TABLE_SCHEDULE_DETAIL
                + " sln on hrd." + ORDERHDR_SCHLINEID + "=sln."
                + SCHEDULE_DETAIL_ID + " JOIN " + TABLE_ORDERTAXLINE
                + " tl on oln." + ORDERLINE_PKID + "=tl."
                + ORDERTAXLINE_ORDRDETAILID + " WHERE "
                //+" AND (hrd." + ORDERHDR_APPROVEFLAG + ">'0' )"
                + " ( hrd." + ORDERHDR_APPROVEFLAG + "!=0 OR "+ORDERHDR_APPROVEFLAG +" is not null"+
                " OR ( hrd." + ORDERHDR_STATUS + "= 'Cancelled' " +
                "AND ( hrd." + ORDERHDR_APPROVEFLAG + " = '0' OR hrd." + ORDERHDR_APPROVEFLAG + " is null ) ) )"+
                "AND ( oln."+ORDERLINE_DELETEFLAG+" = '0' OR (hrd."+ORDERHDR_STATUS+" = 'Cancelled' AND oln."+ORDERLINE_DELETEFLAG+"= '1'))"
                + " group by oln." + ORDERLINE_PKID;*/

        String query = "SELECT hrd." + ORDERHDR_CREATETIME + " as hdrtime,hrd.approveflag ,hrd." + ORDERHDR_PARENT_INVOICENO + ", hrd." + ORDERHDR_STATUS +
                ", oln.*,hrd." + ORDERHDR_SCHLINEID + ", hrd."
                + ORDERHDR_CUSTOMER + ", hrd." + ORDERHDR_INVOICENO + ", hrd."
                + ORDERHDR_LATITUDE + ", hrd." + ORDERHDR_LONGITUDE + " , hrd."
                + ORDERHDR_NOTE + ",SUM(tl." + ORDERTAXLINE_TAXAMOUNT
                + ") as taxamount" + ",SUM(tl." + ORDERTAXLINE_RATE
                + ") as taxrate,tl." + ORDERTAXLINE_AMOUNT
                + " as taxableamount, (SUM(tl." + ORDERTAXLINE_TAXAMOUNT
                + ")+tl." + ORDERTAXLINE_AMOUNT + ") as totalamount, " + "oln."
                + ORDERLINE_TAX + " FROM " + TABLE_ORDERLINE + " oln JOIN "
                + TABLE_ORDERHDR + " hrd on hrd." + ORDERHDR_PKID + "=oln."
                + ORDERLINE_HEADID + " JOIN " + TABLE_SCHEDULE_DETAIL
                + " sln on hrd." + ORDERHDR_SCHLINEID + "=sln."
                + SCHEDULE_DETAIL_ID + " JOIN " + TABLE_ORDERTAXLINE
                + " tl on oln." + ORDERLINE_PKID + "=tl."
                + ORDERTAXLINE_ORDRDETAILID + " WHERE "
                //+" AND (hrd." + ORDERHDR_APPROVEFLAG + ">'0' )"
                + " ( hrd." + ORDERHDR_APPROVEFLAG + "!= 0 OR "+ORDERHDR_APPROVEFLAG +" is not null"+
                " OR ( hrd." + ORDERHDR_STATUS + "= 'Cancelled' ))" +
          //      "AND ( hrd." + ORDERHDR_APPROVEFLAG + " = '0' OR hrd." + ORDERHDR_APPROVEFLAG + " is null ) ) )"+
                " AND ( oln."+ORDERLINE_DELETEFLAG+" = '0' OR (hrd."+ORDERHDR_STATUS+" = 'Cancelled' " +
                "AND oln."+ORDERLINE_DELETEFLAG+"= '1' ))"
                + " group by oln." + ORDERLINE_PKID + " ORDER BY  hrd."+ORDERHDR_INVOICENO;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject tempObj = new JSONObject();
                tempObj.put(
                        "Status",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_STATUS)) == null ? "Approved"
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_STATUS)));
                tempObj.put(
                        "invoiceNo",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_INVOICENO)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_INVOICENO)));
                tempObj.put(
                        "schedulelineid",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_SCHLINEID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_SCHLINEID)));
                tempObj.put(
                        "TaxableAmount",
                        cursor.getString(cursor.getColumnIndex("taxableamount")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("taxableamount")));
                tempObj.put(
                        "TaxAmount",
                        cursor.getString(cursor.getColumnIndex("taxamount")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("taxamount")));
                tempObj.put(
                        "NetAmount",
                        cursor.getString(cursor.getColumnIndex("totalamount")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("totalamount")));
                tempObj.put(
                        "TaxRate",
                        cursor.getString(cursor.getColumnIndex("taxrate")) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex("taxrate")));

                tempObj.put(
                        "product",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_PRODID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_PRODID)));
                tempObj.put(
                        "qty",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_QTY)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_QTY)));
                tempObj.put(
                        "MRP",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_MRP)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_MRP)));
                tempObj.put(
                        "UnitRate",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_RATE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_RATE)));
                tempObj.put(
                        "deleteflag",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DELETEFLAG)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DELETEFLAG)));
                tempObj.put(
                        "Discount",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_DISCOUNT)) == null ? "0"
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DISCOUNT)));
                tempObj.put(
                        "DiscountPer",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_DISCOUNTPER)) == null ? "0"
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DISCOUNTPER)));

                String dateString = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_CREATETIME));

                tempObj.put("createtime", dateString);

                tempObj.put("Hdr_createtime", cursor.getString(cursor
                        .getColumnIndex("hdrtime")) == null ? ""
                        : cursor.getString(cursor
                        .getColumnIndex("hdrtime")));
                tempObj.put("approveflag", cursor.getString(cursor
                        .getColumnIndex(ORDERHDR_APPROVEFLAG)) == null ? ""
                        : cursor.getString(cursor
                        .getColumnIndex(ORDERHDR_APPROVEFLAG)));

                tempObj.put(
                        "latitude",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LATITUDE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LATITUDE)));
                tempObj.put(
                        "longitude",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LONGITUDE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LONGITUDE)));

                tempObj.put(
                        "TaxId",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_TAX)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_TAX)));

                String partymarg = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PM)) == null ? "" : cursor
                        .getString(cursor.getColumnIndex(ORDERLINE_PM));

                String packageType = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PACKAGE_TYPE)) == null ? "" : cursor
                        .getString(cursor.getColumnIndex(ORDERLINE_PACKAGE_TYPE));


                String Batch = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_BATCH)) == null ? "" : cursor
                        .getString(cursor.getColumnIndex(ORDERLINE_BATCH));
                tempObj.put("Batch", Batch);
                tempObj.put("TaxType", "Intrastate");
                tempObj.put("PackageType", packageType);
                tempObj.put("PartyMargin", partymarg.contentEquals("") ? "0" : partymarg);

                //				tempObj.put("TotalQty", String.valueOf(totalqty));
                tempObj.put("note",
                        cursor.getString(cursor.getColumnIndex(ORDERHDR_NOTE)) == null ? "" :
                                cursor.getString(cursor.getColumnIndex(ORDERHDR_NOTE)));

                if (tempObj.getString("Status").equalsIgnoreCase("Cancelled")) {
                    tempObj.put("deleteflag", "0");
                }

                tempObj.put("ParentInvoice",
                        cursor.getString(cursor.getColumnIndex(ORDERHDR_PARENT_INVOICENO)) == null ? "" :
                                cursor.getString(cursor.getColumnIndex(ORDERHDR_PARENT_INVOICENO)));
                tempObj.put("ComplimentaryFlag",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_COMPLIMENT)) == null ? "" :
                                cursor.getString(cursor.getColumnIndex(ORDERLINE_COMPLIMENT)));

                orderdata.put(tempObj);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return orderdata;

    }


    public BigDecimal getTotalVat(String orderid,int precision) {
        SQLiteDatabase db = this.getWritableDatabase();
//        Double vat = 0.0;
        BigDecimal vat = new BigDecimal(0);
        String sql = "select SUM(" + ORDERTAXLINE_TAXAMOUNT
                + ") as taxamount FROM " + TABLE_ORDERTAXLINE + " where "
                + ORDERTAXLINE_ORDRDID + " ='" + orderid + "' and "
                + ORDERTAXLINE_CODE + " LIKE 'VAT%'  and " + ORDERTAXLINE_DELETEFLAG + " = '0'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
//            vat = cursor.getDouble(0);

            vat = new BigDecimal( cursor.getString(0) == null ? "0" : cursor.getString(0 ) );
//            vat = vat.setScale( precision , BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/ );
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return vat;
    }

    public void updateCreditBalance(JSONArray dataArray){
        {

            SQLiteDatabase db = this.getWritableDatabase();
            try{
                db.beginTransaction();
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject dataObject = dataArray.getJSONObject(i);

                    String lineId = dataObject.optString("ScheduleLine Id","0");
                    String limit = dataObject.optString("CreditLimit","0");
                    String balance = dataObject.optString("PendingInvoiceAmount","0");


                    ContentValues values = new ContentValues();
                    values.put(SCHEDULE_LINE_CREDIT_SCHEDULE_LINE, lineId );
                    values.put(SCHEDULE_LINE_CREDIT_BALANCE, balance );
                    values.put(SCHEDULE_LINE_CREDIT_LIMIT, limit );
                   long id = db.insert( TABLE_SCHEDULE_LINE_CREDIT,null,values  );



                }
                db.setTransactionSuccessful();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Error",e.getMessage());
            } finally {
                db.endTransaction();
                db.close();
            }
        }
    }
//creditBalance-cashCollectedToday > creditLimit
    public BigDecimal getCreditBalance (String scheduleLineId){

        BigDecimal creditLimit = new BigDecimal(0);
        BigDecimal creditBalance = new BigDecimal(0);
        BigDecimal cashCollectedToday = new BigDecimal(0);
        SQLiteDatabase db = getWritableDatabase();
        try {

            String get_tottalcollectedamount = "SELECT SUM(" + AMOUNT
                    + ") FROM " + TABLE_PAYMENTS + " WHERE " + PAYMENT_METHOD
                    + "!= 'Return' AND " + PAYMENT_METHOD + "!= 'Credit'"
                    + " AND " + DELETE_FLAG + "=0" + " AND "
                    + PAYMENT_APROOVEFLAG + "=1 " + " AND "
                    + PAYMENT_SCHEDULEDETAILID + "= '" + scheduleLineId + "'";

            Cursor cursor = db.rawQuery(get_tottalcollectedamount, null);
            if (cursor.moveToFirst()) {
                String cashCollected = cursor.getString(0) == null ? "0" : cursor.getString(0);
                cashCollectedToday = new BigDecimal(cashCollected);

            }


            if (cursor != null)
                cursor.close();

            String getCreditDetails = "SELECT " + SCHEDULE_LINE_CREDIT_BALANCE  +
                    "," + SCHEDULE_LINE_CREDIT_LIMIT +
                    " FROM "
                    + TABLE_SCHEDULE_LINE_CREDIT+ " WHERE " + SCHEDULE_LINE_CREDIT_SCHEDULE_LINE + "="
                    + scheduleLineId;

            Cursor cursor1 = db.rawQuery(getCreditDetails, null);
            if (cursor1.moveToFirst()) {


                String creditBalanceString =  cursor1.getString(cursor1.getColumnIndex(SCHEDULE_LINE_CREDIT_BALANCE))  == null ? "0" :
                        cursor1.getString(cursor1.getColumnIndex(SCHEDULE_LINE_CREDIT_BALANCE));


                creditBalance = new BigDecimal(creditBalanceString);

                String creditLimitString = cursor1.getString(cursor1.getColumnIndex(SCHEDULE_LINE_CREDIT_LIMIT)) == null ? "0" :
                        cursor1.getString(cursor1.getColumnIndex(SCHEDULE_LINE_CREDIT_LIMIT));
//                        cursor.getString(1) == null ? "0" : cursor.getString(1);

                creditLimit = new BigDecimal(creditLimitString);

            }

            if (cursor1 != null)
                cursor1.close();

            creditBalance = creditBalance.subtract(cashCollectedToday) ;

            if (creditBalance.compareTo(creditLimit) == 1) {
                return creditBalance ;
            }else{
                return  new BigDecimal(0);
            }


        }catch(Exception e){

            e.printStackTrace();
            Log.e("Error",e.getMessage());

        }finally {
            db.close();
        }

        return new BigDecimal(0);
    }


    public BigDecimal getTotalOrderAmount (int roundoffPrecision){

        BigDecimal totalAmount = new BigDecimal(0);
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT  " +
                "   SUM( l.taxamount ) as taxamount, " +
                "     SUM( l.taxrate) as taxrate, " +
                "   SUM(  l.taxableamount) as taxableamount, " +
                "   SUM(  l. totalamount) as totalamount " +
                "FROM " +
                "    tableorderhdr hrd " +

                "  JOIN " +
                " ( SELECT oln.headid, SUM(tl.taxamount) AS taxamount, " +
                "    SUM(tl.rate) AS taxrate, " +
                "    tl.taxableamount AS taxableamount, " +
                "    (SUM(tl.taxamount * 1) + tl.taxableamount) AS totalamount  " +
                "    FROM    " +
                "    tableorderline oln " +
                "    JOIN " +
                "    ordertaxline tl  ON oln.id = tl.orderdetailid  WHERE   (oln.deleteflag = '0') " +
                "        AND (tl.deleteflag = '0')  GROUP BY oln.id ) l ON hrd.id = l.headid " +
                "WHERE " +
                "    (hrd.approveflag != 0 " +
                "        AND approveflag IS NOT NULL) " +
                "        AND (hrd.Status != 'Cancelled') " +

                "GROUP BY hrd.id" ;
      /*  String query = "SELECT  "+
	 "SUM(tl." + ORDERTAXLINE_TAXAMOUNT
                + ") as taxamount" + ",SUM(tl." + ORDERTAXLINE_RATE
                + ") as taxrate,tl." + ORDERTAXLINE_AMOUNT
                + " as taxableamount, "+
                " ( SUM(tl." + ORDERTAXLINE_TAXAMOUNT
                + "*1 )+ tl." + ORDERTAXLINE_AMOUNT + ") as totalamount  FROM " +  TABLE_ORDERHDR+ " hrd JOIN "
                +   TABLE_ORDERLINE+ " oln on hrd." + ORDERHDR_PKID + "=oln."
                + ORDERLINE_HEADID + " JOIN " + TABLE_ORDERTAXLINE
                + " tl on oln." + ORDERLINE_PKID + "=tl."
                + ORDERTAXLINE_ORDRDETAILID + " WHERE "
                + " ( hrd." + ORDERHDR_APPROVEFLAG + "!=0 AND "+ORDERHDR_APPROVEFLAG +" is not null )"+
                " AND ( hrd." + ORDERHDR_STATUS + " != 'Cancelled' )" +
                " AND ( oln."+ORDERLINE_DELETEFLAG+" = '0' )"+
                " AND ( tl."+ORDERTAXLINE_DELETEFLAG+" = '0' )"
                + " group by hrd." + ORDERHDR_INVOICENO ;*/

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
           do {

               String taxableAmountString =  cursor.getString(cursor.getColumnIndex("taxableamount"))  == null ? "0" :
                       cursor.getString(cursor.getColumnIndex("taxableamount"));

               String taxAmountString =  cursor.getString(cursor.getColumnIndex("taxamount"))  == null ? "0" :
                       cursor.getString(cursor.getColumnIndex("taxamount"));

                String amountString =  cursor.getString(cursor.getColumnIndex("totalamount"))  == null ? "0" :
                        cursor.getString(cursor.getColumnIndex("totalamount"));

               BigDecimal amount = new BigDecimal(amountString);

               if (roundoffPrecision == 0){
                   amount = amount.setScale( 2 , BigDecimal.ROUND_HALF_UP );
               }

               amount = amount.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP );

               totalAmount = totalAmount.add(amount);

            } while (cursor.moveToNext());
        }

        db.close();

        if (cursor != null)
            cursor.close();

        if (roundoffPrecision == 0){
            totalAmount = totalAmount.setScale( 2 , BigDecimal.ROUND_HALF_UP );
        }

        totalAmount = totalAmount.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP );

        return totalAmount ;
    }

    public BigDecimal getTotalCess(String orderid,int precision) {
        SQLiteDatabase db = this.getWritableDatabase();
//        Double vat = 0.0;
        BigDecimal cess = new BigDecimal(0);
        String sql = "select SUM(" + ORDERTAXLINE_TAXAMOUNT
                + ") as taxamount FROM " + TABLE_ORDERTAXLINE + " where "
                + ORDERTAXLINE_ORDRDID + " ='" + orderid + "' and "
                + ORDERTAXLINE_CODE + " LIKE '%CESS%'  and " + ORDERTAXLINE_DELETEFLAG + " = '0'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
//            vat = cursor.getDouble(0);

            cess = new BigDecimal( cursor.getString(0) == null ? "0" : cursor.getString(0 ) );
//            vat = vat.setScale( precision , BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/ );
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return cess;
    }

    public String getStateCode(String scheduledetailid) {
        scheduledetailid = "'" + scheduledetailid + "'";
        String code = "";
        String selectQuery = "SELECT " + SCHEDULEDETAIL_STATE_CODE + " FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_ID
                + " = " + scheduledetailid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            code = cursor.getString(0) == null ? "" : cursor.getString(0);
            // gstin=cursor.getString(0);

        }
        if (cursor != null)
            cursor.close();
        return code;

    }

    public String getComplimentaryOrderId(String schedulelineid, String parentOrderId) {
        String temp = "0";
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + ORDERHDR_PKID + " FROM " + TABLE_ORDERHDR
                + " WHERE " + ORDERHDR_SCHLINEID + "='" + schedulelineid + "' AND " + ORDER_STATUS + " != 'Cancelled' AND " +
                ORDERHDR_CUSTOMER + "='0' AND " + ORDERHDR_PARENT_ORDERID+ " = '" + parentOrderId + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }



    public void updateParentInvoiceNo(String orderId, String parentInvoiceNo) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values = new ContentValues();
        values.put(ORDERHDR_PARENT_INVOICENO, parentInvoiceNo);
        String where = ORDERHDR_PKID + "=?";
        db.update(TABLE_ORDERHDR, values, where,
                new String[]{orderId});

    }
}
