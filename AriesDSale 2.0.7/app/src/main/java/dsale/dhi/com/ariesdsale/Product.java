package dsale.dhi.com.ariesdsale;

public class Product {

	public String name;
	public String id;
	public String focused;
	public String rate;
	public String mrp;
	public String pm;
	public String batch;
	public String minqty;
	public String flag;
	public String tax;

	public Product(String id, String name, String focused, String mrp,
			String rate, String pm, String batch, String minqty,String flag,String tax) {
		this.id = id;
		this.name = name;
		this.focused = focused;
		this.mrp = mrp;
		this.rate = rate;
		this.pm = pm;
		this.batch = batch;
		this.minqty = minqty;
		this.flag = flag;
		this.tax=tax;
	}
}
