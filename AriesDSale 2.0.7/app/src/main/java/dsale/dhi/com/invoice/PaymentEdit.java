package dsale.dhi.com.invoice;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.database.DatabaseHandler;

public class PaymentEdit extends AppCompatActivity {
    private static String sid;
    private JSONObject payadata = new JSONObject();
    private static String checknumbervalue = "";
    private static String banknamevalue = "";
    private static String checkdatevalue = "";
    private static String banknamecode = "";
    private static String paymentmode = "";
    private static String pymentheaderid = "0";
    String[] banknames;
    String[] bankcodes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_payment);
        Bundle bundle = getIntent().getExtras();
        sid = (String) bundle.get("sid");
        try {
            payadata = new JSONObject(bundle.getString("paymentdetails"));
            checkdatevalue = payadata.optString("chequedate", "");
            checknumbervalue = payadata.optString("chequenumber", "");
            paymentmode = payadata.optString("payentmethode", "");
            banknamevalue = payadata.optString("ifsc");
            pymentheaderid = payadata.optString("paymentheaderid", "0");

        } catch (JSONException e3) {
            // TODO Auto-generated catch block
            e3.printStackTrace();
        }
        // intialise bank master spinner
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        JSONArray bankdata;
        try {
            bankdata = db.getbankdata();
            bankcodes = new String[bankdata.length() + 1];
            banknames = new String[bankdata.length() + 1];

            bankcodes[0] = "0";
            banknames[0] = "SELECT";
            for (int i = 0; i < bankdata.length(); i++) {
                JSONObject dataobj = bankdata.getJSONObject(i);
                bankcodes[i + 1] = dataobj.getString("bankcode");
                banknames[i + 1] = dataobj.getString("bankcode") + "- "
                        + dataobj.getString("bankname");

            }

        } catch (JSONException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
    }
}
