package dsale.dhi.com.ariesdsale;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

import dsale.dhi.com.adapters.AddExpenseFragmentAdapter;
import dsale.dhi.com.adapters.CustomPageAdapter;
import dsale.dhi.com.adapters.CustomTabFactory;
import dsale.dhi.com.adapters.ListExpenseFragmentAdapter;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.Global;
import dsale.dhi.com.objects.Stacklogger;

public class ExpenseActivity extends AppCompatActivity implements OnTabChangeListener, OnPageChangeListener {
	CustomPageAdapter pageAdapter;
	ViewPager mViewPager;
	TabHost mTabHost;
	TabWidget mTabWidget;
	 
	public static final String EXPENSE_AMOUNT = "Amount";
	public static final String EXPENSE_DESCRIPTION = "Description";
	public static final String EXPENSE_EXPENSETYPE = "ExpenseType"; 
 	DatabaseHandler db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.expense_main);
		Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));

		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);


		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"Config", MODE_PRIVATE);
 		db = new DatabaseHandler(getApplicationContext());
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabWidget = (TabWidget) findViewById(android.R.id.tabs);
		mViewPager = (ViewPager) findViewById(R.id.viewpager);
		mViewPager.addOnPageChangeListener(this);
		initialiseTabHost();
		List<Fragment> fragments = getFragments();
		pageAdapter = new CustomPageAdapter(getSupportFragmentManager(), fragments);
		mViewPager.setAdapter(pageAdapter);
		SharedPreferences pref2 = getApplicationContext().getSharedPreferences( "Config", MODE_PRIVATE);
		boolean setsecontab = pref2.getBoolean("gotosecondtab", false);
		if (setsecontab) {
			mTabHost.setCurrentTab(1);
			Editor editor = pref2.edit();
			editor.putBoolean("gotosecondtab", false);
			editor.commit();
		}
	}

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		String l = Global.setLastTime();
	}
	public void onTabChanged(String tag) {
		int pos = this.mTabHost.getCurrentTab();
		setBackgroundColor();
		this.mViewPager.setCurrentItem(pos);
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"Config", MODE_PRIVATE);
		boolean needtorefresh = pref.getBoolean("needrefresh", false);
		if (needtorefresh) {
			Editor editor = pref.edit();
			editor.putBoolean("needrefresh", false);
			editor.commit();
			finish();
			startActivity(getIntent());
			overridePendingTransition(0, 0);
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		int pos = this.mViewPager.getCurrentItem();
		this.mTabHost.setCurrentTab(pos);
	}

	@Override
	public void onPageSelected(int arg0) {
	}

	private static void AddTab(ExpenseActivity MainActivity,
			TabHost tabHost, TabHost.TabSpec tabSpec) {
		tabSpec.setContent(new CustomTabFactory(MainActivity));
		tabHost.addTab(tabSpec);
	}

	private void initialiseTabHost() {
		mTabHost.setup();
		try {
			mTabHost.clearAllTabs();
		} catch (Exception e) {
		}
		ExpenseActivity.AddTab(this, this.mTabHost, this.mTabHost
				.newTabSpec("Tab1").setIndicator(getResources().getString(R.string.new_expense)));
		ExpenseActivity.AddTab(this, this.mTabHost, this.mTabHost
				.newTabSpec("Tab2").setIndicator(getResources().getString(R.string.expenses)));
		setBackgroundColor();
		mTabHost.setOnTabChangedListener(this);
	}

	private void setBackgroundColor() {
		switch (mTabHost.getCurrentTab()) {
			case 0:
				TextView tv = (TextView) mTabWidget.getChildAt(0).findViewById(
						android.R.id.title);
				tv.setTextColor(Color.parseColor("#ffffff"));
				tv.setSingleLine(true);
				tv = (TextView) mTabWidget.getChildAt(1).findViewById(
						android.R.id.title);
				mTabWidget.getChildAt(0).setBackgroundResource(R.drawable.leftclicked);
				tv.setTextColor(Color.parseColor("#aeaeae"));
				tv.setSingleLine(true);
				String fontPath = "assets/font/segoeui.ttf";
				mTabWidget.getChildAt(1).setBackgroundResource(R.drawable.rightunclicked);
				break;
			case 1:
				tv = (TextView) mTabWidget.getChildAt(0).findViewById(
						android.R.id.title);
				tv.setTextColor(Color.parseColor("#aeaeae"));
				tv.setSingleLine(true);
				mTabWidget.getChildAt(0).setBackgroundResource(R.drawable.leftunclicked);
				tv = (TextView) mTabWidget.getChildAt(1).findViewById(
						android.R.id.title);
				tv.setTextColor(Color.parseColor("#ffffff"));
				tv.setSingleLine(true);
				mTabWidget.getChildAt(1).setBackgroundResource(R.drawable.rightclicked);
				fontPath = "assets/font/segoeui.ttf";
				break;
		}
	}

	private List<Fragment> getFragments() {
		List<Fragment> fList = new ArrayList<Fragment>();
		AddExpenseFragmentAdapter f1 = new AddExpenseFragmentAdapter(ExpenseActivity.this );
		ListExpenseFragmentAdapter f2 = new ListExpenseFragmentAdapter(ExpenseActivity.this,this) ;
		fList.add(f1);
		fList.add(f2);
		return fList;
	}

	//Added By Jithu On 17/05/2019 for back button redirect to customer list
	@Override
	public void onBackPressed() {

		Intent intent=new Intent(ExpenseActivity.this, CustomerListActivity.class);
		startActivity(intent);
		finish();
		super.onBackPressed();
	}

	@Override
	protected void onRestart() {
		finish();
		startActivity(getIntent());
		overridePendingTransition(0, 0);
		super.onRestart();
	}

	public void deleteAllExpense(View v) {
		db.deleteExpense("0");
		startActivity(getIntent());
		overridePendingTransition(0, 0);
		finish();
	}
	public void saveExpense(View v){
		View mView = mViewPager.getChildAt(0);
		String expenseTypesName = ((Spinner) mView.findViewById(R.id.expenseTypesSpinner))
				.getSelectedItem().toString();
		String amount = ((EditText) mView.findViewById(R.id.expnsAmnt))
				.getText().toString();
		String desc = ((EditText) mView.findViewById(R.id.expsDesc))
				.getText().toString();
		String isValid =validate(expenseTypesName, amount, desc);
		if (isValid.length() > 0) {
    		Toast.makeText(v.getContext(), isValid, Toast.LENGTH_SHORT).show();
			return;
		}
		String expenseTypeId = db.getExpenseTypeId(expenseTypesName);
		ContentValues values = new ContentValues();
		values.put(ExpenseActivity.EXPENSE_EXPENSETYPE, expenseTypeId);
		values.put(ExpenseActivity.EXPENSE_AMOUNT, amount);
		values.put(ExpenseActivity.EXPENSE_DESCRIPTION, desc);
		long id = db.saveExpense(values);
		if(id > 0){
			Toast.makeText(v.getContext(), R.string.expense_msg_saved, Toast.LENGTH_SHORT).show();
			clearFields();
		}
	}

private String validate(String expenseTypesName,String amountString,String desc){
		String valid = "";
		if (expenseTypesName.contentEquals(getResources().getString(R.string.dropdown_select)))
			return getResources().getString(R.string.err_valid_exp_type);
		try{
			double amount = Double.parseDouble(amountString);
			if(amount <= 0){
				return getResources().getString(R.string.err_valid_exp_amt);
			}
		}catch(Exception e){
			return getResources().getString(R.string.err_valid_exp_amt);
		}
		return valid;
	}

public void clearFields(){
		View mView = mViewPager.getChildAt(0);
		((EditText) mView.findViewById(R.id.expnsAmnt)).setText("");
	    ((EditText) mView.findViewById(R.id.expsDesc)).setText("");
	    ((Spinner) mView.findViewById(R.id.expenseTypesSpinner)).setSelection(0) ;
	    startActivity(getIntent());
	    overridePendingTransition(0, 0);
	    finish();
   }

}
