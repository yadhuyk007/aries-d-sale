package dsale.dhi.com.print;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Iterator;
import java.util.Set;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.customerlist.CustomerListActivity;

public class PrintActivity extends AppCompatActivity {
    private BluetoothAdapter mBluetoothAdapter = null;
    private long mLastClickTime = 0;
    private static final int REQUEST_ENABLE_BT = 3;
    private static final String TAG = "TestPrint";
    public static final String TOAST = "toast";
    public static final String DEVICE_NAME = "device_name";
    private BluetoothChatService mChatService = null;
    private String mConnectedDeviceName = null;
    boolean ifbattery = false;
    private TextView printTv;
    private Button printBtn;
    private Button connectbtn;
    private Button printCompanyCopyBtn;
    private Button printComplimentBtn;
    private LinearLayout layoutConPrinter;//added by jithu for set button visiblity on 15/05/2019
    private LinearLayout layoutCustPrint;//added by jithu for set button visiblity on 15/05/2019
    private LinearLayout layoutComplimentoryPrint;//added by jithu for set button visiblity on 15/05/2019
    private LinearLayout layoutCompanyPrint;//added by jithu for set button visiblity on 15/05/2019
    String comparemsg = "";
    String receivedmsg = "";
    private boolean landscapeMode;
    private TextView noteTv;
    private static String printmsg1 = "Test printing1...";
    private static String printmsg2 = "Test printing2...";
    private static String printmsg3 = "Test printing3...";

    private final Handler mHandler = new Handler() {

        @SuppressLint("HandlerLeak")
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case UtilPrint.MESSAGE_STATE_CHANGE:
                    Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothChatService.STATE_CONNECTED:
                            printTv.setText("Connected to " + mConnectedDeviceName);
                            try {
                                Thread.sleep(1050);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            printBtn.setEnabled(true);
                            printCompanyCopyBtn.setEnabled(true);
                            printComplimentBtn.setEnabled(true);
                            break;
                        case BluetoothChatService.STATE_CONNECTING:
                            printTv.setText("Connecting... ");
                            printBtn.setEnabled(false);
                            printCompanyCopyBtn.setEnabled(false);
                            printComplimentBtn.setEnabled(false);
                            break;
                        case BluetoothChatService.STATE_LISTEN:
                        case BluetoothChatService.STATE_NONE:
                            printTv.setText("Not Connected... ");
                            printBtn.setEnabled(false);
                            printCompanyCopyBtn.setEnabled(false);
                            printComplimentBtn.setEnabled(false);
                            break;
                    }
                    break;
                case UtilPrint.MESSAGE_WRITE:
                    break;
                case UtilPrint.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    comparemsg = comparemsg + readMessage;
                    Log.e("ChargeMEssage: ", comparemsg);
                    if (comparemsg.length() >= 5 || !comparemsg.trim().isEmpty() ) {
                        comparemsg = "".trim();
                        readMessage = readMessage.trim().replace("BL=", "");
                        String chargeValue = readMessage.replaceAll("[^0-9]", "");
                        if (chargeValue.equals("")) {
                            chargeValue = "1";
                        }
                        if (chargeValue.equals("0")) {
                            printTv.setText("No Charge in Printer ");
                        } else {
                            ifbattery = true;
                            PrintActivity.this.printMessage(receivedmsg);
                        }
                    } else {

                    }
                    break;
                case UtilPrint.MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    printTv.setText("Connected to " + mConnectedDeviceName);
                    Toast.makeText(getApplicationContext(),
                            "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT)
                            .show();
                    printTv.setEnabled(true);
                    break;
                case UtilPrint.MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(),
                            msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        printTv = (TextView) findViewById(R.id.printerStatus);
        printBtn = (Button) findViewById(R.id.customercopy);
        layoutConPrinter=(LinearLayout) findViewById(R.id.layout_Printer_Btn);//added by jithu for taking layout on 15/05/2019
        layoutCustPrint=(LinearLayout) findViewById(R.id.layout_Customer_Btn);//added by jithu for taking layout on 15/05/2019
        layoutComplimentoryPrint=(LinearLayout) findViewById(R.id.layout_Compliment_Btn);//added by jithu for taking layout on 15/05/2019
        layoutCompanyPrint=(LinearLayout) findViewById(R.id.layout_Company_btn);//added by jithu for taking layout on 15/05/2019
        //layoutCmplmntPrint=
        connectbtn = (Button) findViewById(R.id.connectpntr);
        printCompanyCopyBtn = (Button) findViewById(R.id.companycopy);
        printComplimentBtn = (Button) findViewById(R.id.compliment);
        noteTv = (TextView) findViewById(R.id.printerinfo);
        noteTv.setSelected(true);
        Bundle bundle=getIntent().getExtras();
        printmsg1=bundle.getString("message1");
        printmsg2=bundle.getString("message2");
        printmsg3=bundle.getString("message3");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (printmsg2.length() == 0) {
            /*printComplimentBtn.setVisibility(View.GONE);
            printCompanyCopyBtn.setVisibility(View.GONE);*/
            LinearLayout.LayoutParams lp=new LinearLayout.LayoutParams(0,120);//added by jithu for set button visiblitu on 15/05/2019
            LinearLayout.LayoutParams lp1=new LinearLayout.LayoutParams(0,120);//added by jithu for set button visiblity on 15/05/2019
            lp.weight = 1;//added by jithu for set button visiblity on 15/05/2019
            lp1.weight = 1;//added by jithu for set button visiblity on 15/05/2019
            //lp1.topMargin=-15;
            layoutComplimentoryPrint.setVisibility(View.GONE);//added by jithu for set button visiblity on 15/05/2019
            layoutCompanyPrint.setVisibility(View.GONE);//added by jithu for set button visiblity on 15/05/2019
            layoutConPrinter.setLayoutParams(lp);//added by jithu for set button visiblity on 15/05/2019
            layoutCustPrint.setLayoutParams(lp1);//added by jithu for set button visiblity on 15/05/2019


            printBtn.setText("Print");
           /* ((LinearLayout.LayoutParams) connectbtn.getLayoutParams()).weight = 2;
            ((LinearLayout.LayoutParams) printBtn.getLayoutParams()).weight = 2;*/
        }
        if (mBluetoothAdapter == null) {
            printBtn.setEnabled(false);
            printCompanyCopyBtn.setEnabled(false);
            printComplimentBtn.setEnabled(false);
            Toast.makeText(PrintActivity.this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
        }
        connectbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectPrinter();
            }
        });


    }

    public void printMessage(String message) {
        try{
            Log.e("" + message.length(), "printing: " + message);
            Toast.makeText(PrintActivity.this, "  Please wait Printing is in Progress ", Toast.LENGTH_LONG).show();
            byte[] m2 = new byte[3];
            m2[0] = (byte) 0x1b;
            m2[1] = (byte) 0x21;
            m2[2] = (byte) 0x00;
            mChatService.write(m2);
            if (landscapeMode) {
                byte[] m = new byte[3];
                m[0] = (byte) 0x1b;
                m[1] = (byte) 0X13;//Landscape mode
                m[2] = (byte) 0X90;//144
                mChatService.write(m);
            }else{
                byte[] m = new byte[3];
                m[0] = (byte) 0x1b;
                m[1] = (byte) 0x09;//Portrait	mode
                m[2] = (byte) 0X90;
                mChatService.write(m);
            }
            byte[] messageByte = message.getBytes();
            Log.d("message length","length :"+ messageByte.length);
            mChatService.write(messageByte);
            byte cmd = (byte) 0x0A;
            mChatService.write(cmd);
        }catch (Exception e) {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
    private boolean connectPrinter( ) {
        Set<BluetoothDevice> pairedDevices =   mBluetoothAdapter.getBondedDevices();
        boolean printerNotFound =true;
        for (Iterator iterator = pairedDevices.iterator(); iterator.hasNext();) {
            BluetoothDevice bluetoothDevice = (BluetoothDevice) iterator.next();
            String deviceName =bluetoothDevice.getName().toUpperCase();
            if (deviceName.startsWith(UtilPrint.PRINTER_NAME)) {
                printerNotFound=false;
                mChatService.connect(bluetoothDevice);
                break ;
            }
        }
        if (pairedDevices.size() ==0) {
            Toast.makeText(PrintActivity.this, "No Paired Device found", Toast.LENGTH_LONG).show();
        }
        if (printerNotFound) {
            Toast.makeText(PrintActivity.this, "No Printer found", Toast.LENGTH_LONG).show();
        }
        return printerNotFound;
    }
    @Override
    public void onStart() {
        super.onStart();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            if (mChatService == null) {
                setup();
            }
        }
    }

    private void setup(){
        printBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                landscapeMode =true;
                if (printmsg2.length() == 0) {
                    landscapeMode =false;
                }
                printBtn.setEnabled(false);
                printCompanyCopyBtn.setEnabled(false);
                printComplimentBtn.setEnabled(false);
                ifbattery = false;
                if (SystemClock.elapsedRealtime() - mLastClickTime < 3000) {
                    return;
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        printBtn.setEnabled(true);
                        printCompanyCopyBtn.setEnabled(true);
                        printComplimentBtn.setEnabled(true);

                        finish();
                        startActivity(getIntent());
                    }
                }, 3000);
                mLastClickTime = SystemClock.elapsedRealtime();
                Log.e("click", "count");
//				sendMessageToPrinter(printmsg);

                printMessage(printmsg1);
            }
        });

        printCompanyCopyBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                landscapeMode =false;
                printBtn.setEnabled(false);
                printCompanyCopyBtn.setEnabled(false);
                printComplimentBtn.setEnabled(false);
                ifbattery = false;
                if (SystemClock.elapsedRealtime() - mLastClickTime < 3000) {
                    return;
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        printBtn.setEnabled(true);
                        printCompanyCopyBtn.setEnabled(true);
                        printComplimentBtn.setEnabled(true);
                    }
                }, 3000);
                mLastClickTime = SystemClock.elapsedRealtime();
                printMessage(printmsg2);
            }
        });

        printComplimentBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (printmsg3.length() < 0) {
                    Toast.makeText(PrintActivity.this, " No Compliment Items Added.. ", Toast.LENGTH_LONG).show();
                    return ;
                }
                landscapeMode =false;
                printBtn.setEnabled(false);
                printCompanyCopyBtn.setEnabled(false);
                printComplimentBtn.setEnabled(false);
                ifbattery = false;
                if (SystemClock.elapsedRealtime() - mLastClickTime < 3000) {
                    return;
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        printBtn.setEnabled(true);
                        printCompanyCopyBtn.setEnabled(true);
                        printComplimentBtn.setEnabled(true);
                    }
                }, 3000);
                mLastClickTime = SystemClock.elapsedRealtime();
                printMessage(printmsg3);
            }
        });
        mChatService = new BluetoothChatService(this, mHandler);
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        printBtn.setEnabled(true);
        printCompanyCopyBtn.setEnabled(true);
        printComplimentBtn.setEnabled(true);
        if (mChatService != null) {
            if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
                mChatService.start();
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                printBtn.setEnabled(true);
                printCompanyCopyBtn.setEnabled(true);
                printComplimentBtn.setEnabled(true);
            } else {
                Toast.makeText(PrintActivity.this,"Bluetooth was not enabled..", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mChatService != null)
            mChatService.stop();
        Log.e(TAG, "--- ON DESTROY ---");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent i=new Intent(PrintActivity.this, CustomerListActivity.class);
        startActivity(i);
        overridePendingTransition(0,0);
    }
}
