package dsale.dhi.com.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.FourStrings;

public class ExpenseListRecyclerViewAdapter extends RecyclerView.Adapter<ExpenseListRecyclerViewAdapter.ViewHolder>{
    private HashMap<String, FourStrings> expHashMap;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context context;
    private static DatabaseHandler db;

    // data is passed into the constructor
    ExpenseListRecyclerViewAdapter(Context context, HashMap<String, FourStrings> expHashMap) {
        this.mInflater = LayoutInflater.from(context);
        this.expHashMap = expHashMap;
        this.context=context;
        db = new DatabaseHandler(context);
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view = mInflater.inflate(R.layout.recyclerview_row_expenselist, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String lastsyc = db.getrecentsynctime();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date lastSyncDateTime = new Date();
        try {
                lastSyncDateTime = dateTimeFormat.parse(lastsyc);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Map.Entry<String, FourStrings> entry = (Map.Entry<String, FourStrings>) expHashMap.entrySet().toArray()[position];
        final String expenseId = entry.getKey();
        final FourStrings value = entry.getValue();
        final String amount = value.mrp;
        final String desc = value.rate;
        final String expenseTypeName = value.packageType;
        String createTime = value.taxid;
        Date createOn = new Date();
        try {
              createOn = dateTimeFormat.parse(createTime);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        holder.expenseTypeName.setText(expenseTypeName);
        holder.expenseDesc.setText(desc);
        holder.amount.setText(amount);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return expHashMap.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView expenseTypeName;
        TextView expenseDesc;
        TextView amount;
        RecyclerView ExpenseRecylerview;

        ViewHolder(View itemView) {
            super(itemView);
            expenseTypeName = itemView.findViewById(R.id.expenseTypeName);
            expenseDesc = itemView.findViewById(R.id.expenseDesc);
            amount = itemView.findViewById(R.id.amount);
            itemView.setOnClickListener(this);
            ExpenseRecylerview=itemView.findViewById(R.id.ExpenseRecylerview);
        }

        @Override
        public void onClick( View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            //Toast.makeText(view.getContext(), "Clicked!", Toast.LENGTH_SHORT).show();
            //call popup to edit the row
            final LinearLayout schbgnpopup= (LinearLayout) view.findViewById(R.id.rcyclerpopup);
            final View customView = mInflater.inflate(R.layout.edit_expense_popup, null);
            Button delete = (Button) customView.findViewById(R.id.close);
            Button update = (Button) customView.findViewById(R.id.yes);
            ImageButton closebtn =(ImageButton) customView.findViewById(R.id.imgclose) ;
            closebtn.setOnClickListener(this);
            //fields in popup
            TextView exptype=(TextView) customView.findViewById(R.id.expType);
            TextView expDesc=(TextView) customView.findViewById(R.id.expDesc);
            TextView expAmnt=(TextView) customView.findViewById(R.id.expAmnt);
            // get position
            int pos = getAdapterPosition();
            Map.Entry<String, FourStrings> hmapentry = (Map.Entry<String, FourStrings>) expHashMap.entrySet().toArray()[pos];
            final String expenseId = hmapentry.getKey();
            FourStrings value = hmapentry.getValue();
            final String amount = value.mrp;
            final String desc = value.rate;
            final String expenseTypeName = value.packageType;
            exptype.setText(expenseTypeName);
            expDesc.setText(desc);
            expAmnt.setText(amount);
            //instantiate popup window
            int width = LinearLayout.LayoutParams.MATCH_PARENT;
            int height = LinearLayout.LayoutParams.MATCH_PARENT;
            final PopupWindow popupWindow = new PopupWindow(customView, width, height);
            popupWindow.setFocusable(true);
            //display the popup window
            new Handler().postDelayed(new Runnable(){
                public void run() {
                   popupWindow.showAtLocation(schbgnpopup, Gravity.CENTER, 0, 0);
                }
            }, 200L);
            popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            //close the popup window on button click
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    String createdtime = df.format(c.getTime());
                    db.deleteExpense(expenseId);
                    Toast.makeText(v.getContext(), R.string.expense_msg_deleted, Toast.LENGTH_SHORT).show();
                    popupWindow.dismiss();
                    expHashMap.remove(expenseId);
                    notifyDataSetChanged();
                }
            });
            update.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    EditText txt = (EditText) customView.findViewById(R.id.expAmnt);
                    String amount = txt.getText().toString();
                    amount = amount.contentEquals("") ? "0"
                            : amount;

										/*Calendar c = Calendar.getInstance();
										SimpleDateFormat df = new SimpleDateFormat(
												"yyyyMMddHHmmss");
										String createdtime = df.format(c.getTime());*/
                    db.updateExpense(expenseId, amount);
                    Toast.makeText(v.getContext(), R.string.expense_msg_updated, Toast.LENGTH_SHORT).show();
                    popupWindow.dismiss();
                    expHashMap = db.getExpenses();
                    notifyDataSetChanged();
                }
            });
            closebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                }
            });
        }
    }

    // convenience method for getting data at click position
    /*String getItem(int id) {
        return expHashMap.get(id);
    }*/

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
