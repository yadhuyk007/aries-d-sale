package dsale.dhi.com.inward;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class TabAdapterInwardOrOutward extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Context mcontext;

    public TabAdapterInwardOrOutward(Context mcontext, FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.mcontext=mcontext;
    }
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabInward tab1 = new TabInward();
                return tab1;
            case 1:
                TabOutward tab2 = new TabOutward();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
