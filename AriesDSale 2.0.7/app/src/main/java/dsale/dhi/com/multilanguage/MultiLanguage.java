package dsale.dhi.com.multilanguage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;
import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.customerlist.CustomerListActivity;

public class MultiLanguage extends AppCompatActivity implements View.OnClickListener {
    TextView tvEng;
    TextView tvHin;
    Locale myLocale;
    String currentLanguage = "en", currentLang;
    private SharedPreferences pref;
    private String appLanguage ;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multilanguage);

        currentLanguage = getIntent().getStringExtra(currentLang);

        tvEng = (TextView)findViewById(R.id.lang1);
        tvHin = (TextView)findViewById(R.id.lang2);

        //List<String> list = new ArrayList<String>();

       // list.add("Select language");
        tvEng.setText("English");
        tvHin.setText("हिंदी");

        tvEng.setOnClickListener(this);
        tvHin.setOnClickListener(this);
        pref = getSharedPreferences("Config",
                MODE_PRIVATE);

        String localeName=pref.getString("language","en");
        int tempid=android.R.color.holo_orange_light;
        if (localeName.equalsIgnoreCase("en")){
            tvEng.setTextColor(getResources().getColor(tempid));
            tvHin.setTextColor(getResources().getColor(android.R.color.black));
        }else {
            tvHin.setTextColor(getResources().getColor(android.R.color.holo_orange_light));
            tvEng.setTextColor(getResources().getColor(android.R.color.black));
        }

       /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        setLocale("en");
                        break;
                    case 2:
                        setLocale("hi");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }
*/}
    public void setLocale(String localeName) {
        if (!localeName.equals(currentLanguage)) {
            myLocale = new Locale(localeName);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);

            pref.edit().putString("language",localeName.toString()).apply();
            Intent refresh = new Intent(this, CustomerListActivity.class);
            refresh.putExtra(currentLang, localeName);
            finish();
            startActivity(refresh);
        } else {
            Toast.makeText(MultiLanguage.this, "Language already selected!", Toast.LENGTH_SHORT).show();
        }
    }
    public void setLanguage(Context context) {
        pref = context.getSharedPreferences("Config",
                        MODE_PRIVATE);
        String localeName=pref.getString("language","en");

        if (!localeName.equals(currentLanguage)) {
            myLocale = new Locale(localeName);
            Resources res = context.getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);


        }
    }


    public void onBackPressed() {

        Intent intent=new Intent(getApplicationContext(), CustomerListActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }


    @SuppressLint("NewApi")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lang1:
                tvEng.setTextColor(getResources().getColor(android.R.color.holo_orange_light));
                tvHin.setTextColor(getResources().getColor(android.R.color.black));
                setLocale("en");
                break;
            case R.id.lang2:
                tvHin.setTextColor(getResources().getColor(android.R.color.holo_orange_light));
                tvEng.setTextColor(getResources().getColor(android.R.color.black));
                setLocale("hi");
                break;

        }
    }
    }
