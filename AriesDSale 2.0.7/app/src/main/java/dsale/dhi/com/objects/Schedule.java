package dsale.dhi.com.objects;
import java.util.List;

import dsale.dhi.com.adapters.ParentListItem;
import dsale.dhi.com.customerlist.CustomerList;

public class Schedule implements ParentListItem {
    private String mRouteName;
    private String mRouteId;
    private String mRouteStartTime;
    private List<CustomerList> mcustlist;

    public Schedule(String name, List<CustomerList> custlist,String RouteId,String RouteStartTime) {
        mRouteName = name;
        mcustlist = custlist;
        mRouteId = RouteId;
        mRouteStartTime = RouteStartTime;
    }

    public String getRouteName() {
        return mRouteName;
    }
    public String getRouteId() {
        return mRouteId;
    }
    public String getRouteStartTime() {
        return mRouteStartTime;
    }

    @Override
    public List<?> getChildItemList() {
        return mcustlist;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
