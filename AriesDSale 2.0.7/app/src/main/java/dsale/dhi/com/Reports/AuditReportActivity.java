package dsale.dhi.com.Reports;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.webconfigurration;
import dsale.dhi.com.print.PrintActivity;
import dsale.dhi.com.print.UtilPrint;

public class AuditReportActivity extends AppCompatActivity {
    private DatabaseHandler db;
    private JSONArray vanSalesData=new JSONArray();
    JSONObject processedOrderData=new JSONObject();
    private TableLayout list;
    private TableRow.LayoutParams lp;
    private TableRow row;
    private TextView summary_total;
    private TextView summary_cancel;
    private TextView summary_complmnt;
    private TextView summary_nettotal;
    private LinearLayout summary_totalll;
    private LinearLayout summary_cancelll;
    private LinearLayout summary_complll;
    private LinearLayout summary_netll;
    private LinearLayout summary_outer;
    private ImageButton prntbtn;
    private SharedPreferences pref;
    private String currencyCode ;
    private String currencySymbol ;
    private int roundoffPrecision ;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_reoport);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        pref = getApplicationContext()
                .getSharedPreferences("Config", MODE_PRIVATE);

        currencyCode=pref.getString("CurrencyCode","");
        currencySymbol =pref.getString("CurrencySymbol","");
        roundoffPrecision = pref.getInt("RoundOffPrecision",0);

        prntbtn = (ImageButton) findViewById(R.id.prntbtn);
        prntbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message1 = null;
                try {
                    message1 = getReportForPrint();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent=new Intent(AuditReportActivity.this, PrintActivity.class);
                intent.putExtra("message1",message1 );
                intent.putExtra("message2","" );
                intent.putExtra("message3","" );
                finish();
                startActivity(intent);
                int i=0;
            }
        });
        SharedPreferences pref = getApplicationContext()
                .getSharedPreferences("Config", MODE_PRIVATE);
        db = new DatabaseHandler(getApplicationContext());
        list = (TableLayout) findViewById(R.id.auditlist);
        lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);

        lp.setMargins(1, 0, 0, 1);


        try {
            vanSalesData=db.getVanSalesDataForAuditReport();
            processedOrderData=processOrderData(vanSalesData);
            int i=0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            generateReport(processedOrderData);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private String getReportForPrint() throws JSONException {
        JSONObject invoiceTotal=processedOrderData.optJSONObject("InvoiceTotal");
        JSONObject invoiceStatus=processedOrderData.optJSONObject("InvoiceStatus");
        JSONObject invoiceType=processedOrderData.optJSONObject("InvoiceType");
        String netTOtal=processedOrderData.optString("NetTotal","0");
        String complimentTotal=processedOrderData.optString("ComplimentTotal","0");
        int actAmt=Integer.parseInt(netTOtal)-Integer.parseInt(complimentTotal);
        String printmsg = "";
        webconfigurration c = new webconfigurration(getApplicationContext());
        String line = UtilPrint.getPrintFormat("------------------------", 24, 2);
        String companyName =c.companyNameShort ;
        String msg = UtilPrint.getPrintFormat(getResources().getString(R.string.auditreport), 24, 2)
                +UtilPrint.getPrintFormat(companyName.toUpperCase(), 24, 0)
                +UtilPrint.getPrintFormat(c.companyAddressShort.toUpperCase(), 24, 0)
                +UtilPrint.getPrintFormat(c.companyGSTIN.toUpperCase(), 24, 0)
                +UtilPrint.getPrintFormat("ST CODE:32", 24, 0)
                +line;
        String productHeads =UtilPrint.getPrintFormat(getResources().getString(R.string.audit_sno),5, 0)
                +UtilPrint.getPrintFormat(getResources().getString(R.string.audit_billNo),8, 0)
                +UtilPrint.getPrintFormat(getResources().getString(R.string.audit_Amount),7, 0)
                +UtilPrint.getPrintFormat(getResources().getString(R.string.audit_sts), 4, 1)
                +line;
        String productString="";
        JSONArray keys =new JSONArray();
        if(invoiceTotal.length()>0)
         keys = invoiceTotal.names ();
        ArrayList<String> sortedKeys=getSortedKeys(keys);
        String slNo="";
        for (int indx = 0; indx < sortedKeys.size (); indx++) {
            int dummyIndx=indx+1;
            if(dummyIndx<10)
                slNo="0"+String.valueOf(dummyIndx);
            else
                slNo=String.valueOf(dummyIndx);
            String key=sortedKeys.get(indx);
            String amount=invoiceTotal.getString(key);
            String invStats=invoiceType.getString(key);
            productString =productString+UtilPrint.getPrintFormat(slNo, 4,0)
                    + UtilPrint.getPrintFormat(key, 20, 0)
                    +UtilPrint.getPrintFormat(amount, 17, 1)
                    +UtilPrint.getPrintFormat(invStats, 7, 1);
        }
        printmsg =  msg+productHeads+productString+line;
        String tottString =UtilPrint.getPrintFormat(getResources().getString(R.string.audit_summary_totatl),12, 0)
                +UtilPrint.getPrintFormat(":",1, 0)
                +UtilPrint.getPrintFormat(netTOtal+".00",10, 1)
                +UtilPrint.getPrintFormat("",1, 1);
        String cancelString =UtilPrint.getPrintFormat(getResources().getString(R.string.audit_summary_cancel),12, 0)
                +UtilPrint.getPrintFormat(":",1, 0)
                +UtilPrint.getPrintFormat("0.00",10, 1)
                +UtilPrint.getPrintFormat("",1, 1);
        String netTotalString =UtilPrint.getPrintFormat(getResources().getString(R.string.audit_summary_net),12, 0)
                +UtilPrint.getPrintFormat(":",1, 0)
                +UtilPrint.getPrintFormat(""+actAmt+".00",10, 1)
                +UtilPrint.getPrintFormat("",1, 1);

        String compTotalString=UtilPrint.getPrintFormat(getResources().getString(R.string.audit_summary_compl),12, 0)
                +UtilPrint.getPrintFormat(":",1, 0)
                +UtilPrint.getPrintFormat(""+complimentTotal+".00",10, 1)
                +UtilPrint.getPrintFormat("",1, 1);

        printmsg =  msg+productHeads+productString+line+tottString+cancelString+compTotalString+netTotalString;
        return printmsg;
    }

    private void generateReport(JSONObject processedOrderData) throws Exception {
        JSONObject invoiceTotal=processedOrderData.optJSONObject("InvoiceTotal");
        JSONObject invoiceStatus=processedOrderData.optJSONObject("InvoiceStatus");
        JSONObject invoiceType=processedOrderData.optJSONObject("InvoiceType");
        String netTOtal=processedOrderData.optString("NetTotal","0");
        String complimentTotal=processedOrderData.optString("ComplimentTotal","0");
//        int actAmt=Integer.parseInt(netTOtal)-Integer.parseInt(complimentTotal);

        BigDecimal actAmt = new BigDecimal(netTOtal).subtract(new BigDecimal(complimentTotal));
        actAmt =actAmt.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP );
        summary_total = (TextView) findViewById(R.id.summary_total);
        summary_cancel = (TextView) findViewById(R.id.summary_cancel);
        summary_complmnt = (TextView) findViewById(R.id.summary_compl);
        summary_nettotal = (TextView) findViewById(R.id.summary_net);
        summary_totalll=(LinearLayout) findViewById(R.id.summary_totalll);
        summary_cancelll=(LinearLayout) findViewById(R.id.summary_cancelll);
        summary_complll=(LinearLayout) findViewById(R.id.summary_complll);
        summary_netll=(LinearLayout) findViewById(R.id.summary_netll);
        //summary_outer=(LinearLayout) findViewById(R.id.summary_outer);

        JSONArray keys = new JSONArray();
        if(invoiceTotal.length()>0)
            keys=invoiceTotal.names ();
        ArrayList<String> sortedKeys=getSortedKeys(keys);
        String slNo="";
        for (int indx = 0; indx < sortedKeys.size (); indx++) {
            int dummyIndx=indx+1;
            if(dummyIndx<10)
                slNo="0"+String.valueOf(dummyIndx);
            else
                slNo=String.valueOf(dummyIndx);
            String key=sortedKeys.get(indx);
            String amount=invoiceTotal.getString(key);
            String invStats=invoiceType.getString(key);
            row = new TableRow(AuditReportActivity.this);
            row.setBackgroundResource(R.drawable.table_row);
            row.addView(addtext(slNo,indx));
            row.addView(addtext(key, indx));
            row.addView(addtext(amount, indx));
            row.addView(addtext(invStats, indx));
            list.addView(row);

        }
        if(keys.length()>0){
            summary_total.setText(currencySymbol+" "+netTOtal);
            summary_cancel.setText(currencySymbol+" "+"0");
            summary_complmnt.setText(currencySymbol+" "+complimentTotal);
            summary_nettotal.setText(currencySymbol+" "+String.valueOf(actAmt));
        }
        else{
            summary_totalll.setVisibility(View.GONE);
            summary_cancelll.setVisibility(View.GONE);
            summary_complll.setVisibility(View.GONE);
            summary_netll.setVisibility(View.GONE);
            list.setVisibility(View.GONE);
            //summary_outer.setVisibility(View.GONE);

        }




    }

    private ArrayList<String> getSortedKeys(JSONArray keys) {
        int arrLen=keys.length();
        ArrayList<String> list = new ArrayList<String>();
        for(int indx=0;indx<arrLen;indx++){
            try {
                String tmpInvNo=keys.getString(indx);
                list.add(tmpInvNo);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Collections.sort(list);
        return list;
    }

    private View addtext(String string, int indx) {
        TextView text = new TextView(AuditReportActivity.this);
        text.setLayoutParams(lp);
        text.setText(string);
//        if (i % 2 == 0)
        text.setBackgroundResource(R.color.white);
//        else
//            text.setBackgroundResource(R.color.gray3);
        text.setPadding(10, 20, 80, 10);
//        a = new Font(VanSalesReport.this, text, Font.Regular);
        return text;
    }

    private JSONObject processOrderData(JSONArray vanSalesData) throws Exception {
        JSONObject invTotalJson=new JSONObject();
        JSONObject statusJson=new JSONObject();
        JSONObject typeJSON=new JSONObject();
        JSONObject outerJson=new JSONObject();
//        long compNet=0;
//        long netTot=0;

        BigDecimal compNet= new BigDecimal(0);
        BigDecimal netTot=  new BigDecimal(0);

        Set<String> hash_Set = new HashSet<String>();
        int ordDatalen=0;
        ordDatalen=vanSalesData.length();
        for(int indx=0;indx<ordDatalen;indx++){
            String invNo= null;
            try {
                invNo = vanSalesData.getJSONObject(indx).getString("invoiceNo");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            hash_Set.add(invNo);
        }
        for(String invoiceNumber : hash_Set){
            String val=invoiceNumber;
            String invStatus="";
            String invType="";
            String singleInvTot="";
//            Double innrTot=0.0;
            BigDecimal innrTot = new BigDecimal(0);
            for(int indx=0;indx<ordDatalen;indx++){
                String innrInvNo=vanSalesData.getJSONObject(indx).getString("invoiceNo");
                String innrInvStat=vanSalesData.getJSONObject(indx).getString("Status");
                String innrCompFlg=vanSalesData.getJSONObject(indx).getString("ComplimentaryFlag");
                if(innrInvNo.equals(val) && innrInvStat.equals("Cancelled")){
                    //invType="CR";
                    if(innrCompFlg.equals("0")){
                        invType="CR";
                    }
                    else{
                        invType="CM";
                    }
                    invStatus="Cancelled";
                    break;
                }
                else if(innrInvNo.equals(val)){
                    if(innrCompFlg.equals("0")){
                        invType="CR";
                    }
                    else{
                        invType="CM";
                    }
                    invStatus=innrInvStat;
//                    innrTot+=vanSalesData.getJSONObject(indx).getDouble("NetAmount");

                    innrTot = innrTot.add(new BigDecimal(vanSalesData.getJSONObject(indx).optDouble("NetAmount",0)));
                }
            }
          /*  long convertedTot=Math.round(innrTot);
            netTot+=convertedTot;
            if(invType.equals("CM"))
                compNet+=convertedTot;*/

            if (roundoffPrecision == 0){
                innrTot = innrTot.setScale(2,BigDecimal.ROUND_HALF_UP  );
            }
            BigDecimal convertedTot =innrTot.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP );

            netTot = netTot.add(convertedTot) ;


            if(invType.equals("CM")){
                compNet= compNet.add(convertedTot);
            }


            invTotalJson.put(val,String.valueOf(convertedTot));
            statusJson.put(val,invStatus);
            typeJSON.put(val,invType);

            int i=0;
        }

        if (roundoffPrecision == 0){
            netTot = netTot.setScale(2,BigDecimal.ROUND_HALF_UP  );
        }
        netTot =netTot.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP );

        if (roundoffPrecision == 0){
            compNet = compNet.setScale(2,BigDecimal.ROUND_HALF_UP  );
        }
        compNet =compNet.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP );

        outerJson.put("InvoiceTotal",invTotalJson)
                 .put("InvoiceStatus",statusJson)
                 .put("InvoiceType",typeJSON)
                 .put("NetTotal",String.valueOf(netTot))
                 .put("ComplimentTotal",String.valueOf(compNet));

        return outerJson;
    }

    @Override
    public void onBackPressed() {

        Intent intent=new Intent(AuditReportActivity.this, CustomerListActivity.class);
        finish();
        startActivity(intent);
        super.onBackPressed();
    }

}
