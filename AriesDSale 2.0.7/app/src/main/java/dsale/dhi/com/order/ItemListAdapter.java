package dsale.dhi.com.order;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import java.util.HashMap;

import java.util.List;


import dsale.dhi.com.ariesdsale.R;


public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ViewHolder> {

    Context context ;
    HashMap<String, OrderLine> dataHashMap = new HashMap<String,OrderLine>();
    List<String> keyList = new ArrayList<String>();
    private ItemClickListener mClickListener;


    public ItemListAdapter(Context context, HashMap<String, OrderLine> dataHashMap,ItemClickListener itemClickListener) {
        this.dataHashMap = dataHashMap ;
        this.context =context;

        keyList = new ArrayList<String>();
        keyList.addAll(dataHashMap.keySet());
       this.mClickListener = itemClickListener ;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem= layoutInflater.inflate(R.layout.adapter_list_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        OrderLine data = dataHashMap.get(keyList.get(position));

        viewHolder.prdNameView.setText(data.getProductName());
        viewHolder.prdRateView.setText(twoDForm .format(data.getRate()) );
        viewHolder.qtyView.setText(data.getQty()+"");
        viewHolder.discountView.setText( twoDForm .format(data.getDiscountAmount()) );
        viewHolder.totalView.setText(twoDForm .format(data.getTotalAmount())  );

        if (data.isComplimentFlag()){
            viewHolder.itemLayout.setBackgroundResource(R.color.light_gray);
            viewHolder.discountView.setText( "0" );
            viewHolder.totalView.setText("0");
        }

    }

    @Override
    public int getItemCount() {

        return dataHashMap.size();
    }

    public OrderLine getItem(int position) {

        String key = keyList.get(position);
        return dataHashMap.get(key);
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout itemLayout;
        public TextView prdNameView;
        public TextView prdRateView;
        public TextView qtyView;
        public TextView discountView;
        public TextView totalView;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            this.prdNameView = (TextView) itemView.findViewById(R.id.prdname);
            this.prdRateView = (TextView) itemView.findViewById(R.id.prdrate);
            this.qtyView = (TextView) itemView.findViewById(R.id.prdqty);
            this.discountView = (TextView) itemView.findViewById(R.id.disc);
            this.totalView = (TextView) itemView.findViewById(R.id.total);
            itemLayout =(LinearLayout)itemView.findViewById(R.id.itemLayout);
            itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onRecyclerViewItemClick(itemView,getItem(getAdapterPosition()),getAdapterPosition() );
                }
            });

        }



    }


    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onRecyclerViewItemClick(View view, OrderLine data, int position );
    }
}
