package dsale.dhi.com.objects;

import android.app.Activity;
import android.content.Context;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import dsale.dhi.com.ariesdsale.R;

/**
 * Created by hp on 04/04/2019.
 */

public class Global {
    public static String date = "";
    public static boolean force = false;
    public static boolean refresh=false;
    public static boolean onSync;
    public static String lastacttime = "";
    public static boolean synnav=false;



    public static void Toast(Activity mAct, String string, int length,
                             String style) {
        Toast toast = Toast.makeText(mAct, string, length);
        // toast.setGravity(Gravity.CENTER, 0, 0);
        LinearLayout tView = (LinearLayout) toast.getView();
        tView.setBackgroundResource(R.color.level1);
        //Font a = new Font(mAct, tView, style);
        toast.show();
    }
    public static String setLastTime() {
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        lastacttime = dateTimeFormat.format(c.getTime());
        return lastacttime;
    }




}
