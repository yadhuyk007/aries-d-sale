package dsale.dhi.com.customerlist;


import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import dsale.dhi.com.Reports.AuditReportActivity;
import dsale.dhi.com.Reports.DailySalesReportActivity;
import dsale.dhi.com.Reports.ProductSalesReportActivity;
import dsale.dhi.com.Reports.StockReportActivity;
import dsale.dhi.com.adapters.ExpandableRecyclerAdapter;
import dsale.dhi.com.adapters.ScheduleAdapter;
import dsale.dhi.com.about.AboutActivity;
import dsale.dhi.com.completeschedule.CompleteScheduleActivity;
import dsale.dhi.com.ariesdsale.ExpenseActivity;
import dsale.dhi.com.ariesdsale.LoginActivity;
import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.dashboard.SummaryActivity;
import dsale.dhi.com.customer.NewCustomerActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.gps.CopyOfVehicletracker;
import dsale.dhi.com.gps.PlayTracker;
import dsale.dhi.com.inward.InwardOutwardActivity;
import dsale.dhi.com.multilanguage.MultiLanguage;
import dsale.dhi.com.objects.Global;
import dsale.dhi.com.objects.Schedule;
import dsale.dhi.com.objects.ScheduledRouteObject;
import dsale.dhi.com.objects.Stacklogger;
import dsale.dhi.com.objects.SupportFunctions;
import dsale.dhi.com.objects.Synchronize;
import dsale.dhi.com.objects.webconfigurration;
import dsale.dhi.com.offroute.SearchCustomerOffRouteActivity;
import dsale.dhi.com.resetpassword.ResetPasswordActivity;

public class CustomerListActivity extends AppCompatActivity implements CustListRecyclerViewAdapter.ItemClickListener, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    private Menu menu;
    private static String salespersonid = "";
    private static String salespersonname = "";
    private static String Scheduledate = "0";
    CustListRecyclerViewAdapter adapter;
    FloatingActionButton fab1;
    LinearLayout fabLayoutNewCust;
    LinearLayout fabLayoutCustSrch;
    private boolean isFABOpen = false;
    private ScheduleAdapter mAdapter;
    private RecyclerView recyclerView;
    private ScheduledRouteObject[] elemts;
    private ScheduledRouteObject[] scheduledata;
    private DatabaseHandler db;
    private TextView searchText;
    private ImageView srchbtn;
    private SharedPreferences pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MultiLanguage multiLanguage=new MultiLanguage();
        multiLanguage.setLanguage(this);
        setContentView(R.layout.activity_drawer_main);

        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        menu = navigationView.getMenu();
        View header = navigationView.getHeaderView(0);
        db = new DatabaseHandler(getApplicationContext());
        pref = getApplicationContext()
                .getSharedPreferences("Config",
                        MODE_PRIVATE);
        String salespersonsync = db.getrecentsynctime();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            Date temp = dateTimeFormat.parse(salespersonsync);
            dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            salespersonsync = dateTimeFormat.format(temp);
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        TextView txt = header.findViewById(R.id.textdsename);
        txt.setText(pref.getString("personname", ""));
        txt = header.findViewById(R.id.texttype);
        txt.setText(pref.getString("personrole", ""));
        txt = header.findViewById(R.id.textcode);
        txt.setText(pref.getString("pesronorg", ""));
        txt = header.findViewById(R.id.textdate);
        txt.setText(salespersonsync);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);



        // The request code used in ActivityCompat.requestPermissions()
// and returned in the Activity's onRequestPermissionsResult()
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CAMERA
        };

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }


        startTimerService();


        String scheduleid = db.getactualscheduleid();
        String beginTime = db.getscheduleheaderbegintime(scheduleid);


        try {
            elemts = getScheduledRoutes("");
            // mExpListAdapter = new ExpListAdapter(getApplicationContext(),
            // elemts, mArrChildelements, this);
            loadCustList(elemts);

            //check gps on
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean statusOfGPS = manager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!statusOfGPS) {

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        CustomerListActivity.this);
                builder.setTitle(R.string.switch_on_gps);
                builder.setMessage(R.string.gps_off_alert_message);
                builder.setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                startActivityForResult(
                                        new Intent(
                                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                        0);
                            }
                        });
                builder.show();
                return;
            }


            if (beginTime == null) {
                String schDate = db.GetCurrentScheduledate();
                Calendar c = Calendar.getInstance();
                dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd",
                        Locale.US);
                String currenttime = dateTimeFormat.format(c.getTime());
                if (currenttime.contentEquals(schDate)) {
                    //final LinearLayout schbgnpopup= (LinearLayout) findViewById(R.id.testid);
                    final DrawerLayout schbgnpopup = (DrawerLayout) findViewById(R.id.drawer_layout);
                    LayoutInflater layoutInflater = (LayoutInflater) CustomerListActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
                    View customView = layoutInflater.inflate(R.layout.popuplayout, null);
                    Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                    Button confirm = (Button) customView.findViewById(R.id.yes);
                    TextView title = (TextView) customView.findViewById(R.id.header);
                    title.setText(R.string.begin_schedule);
                    TextView message = (TextView) customView.findViewById(R.id.message);
                    message.setText(R.string.begin_schedule_message);

                    //instantiate popup window

                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
                    final PopupWindow popupWindow = new PopupWindow(customView, width, height);
                    // popupWindow.showAtLocation(customView , Gravity.CENTER, 0, 0);

                    //display the popup window
                    // popupWindow.showAtLocation(schbgnpopup, Gravity.CENTER, 0, 0);

                    new Handler().postDelayed(new Runnable() {

                        public void run() {
                            //popupWindow.showAtLocation(CustomerListActivity.this.getWindow().getDecorView(), Gravity.CENTER,0,0);
                            popupWindow.showAtLocation(schbgnpopup, Gravity.CENTER, 0, 0);
                        }

                    }, 600L);
                    popupWindow.setFocusable(true);
                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    //schbgnpopup.setAlpha(0.5F);
                    //close the popup window on button click
                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                            return;
                        }
                    });
                    confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                            Calendar c = Calendar.getInstance();
                            DateFormat dateTimeFormat = new SimpleDateFormat(
                                    "yyyyMMddHHmmss", Locale.US);
                            DateFormat dateTimeFormat_readable = new SimpleDateFormat(
                                    "dd/MM/yyyy HH:mm:ss", Locale.US);
                            String currenttime = dateTimeFormat.format(c
                                    .getTime());
                            String readabletime = dateTimeFormat_readable
                                    .format(c.getTime());

                            db.updatescheduleheaderbegintime(currenttime);


                            db.intialisesynctable(currenttime);

                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("Schbegintime", currenttime);
                            editor.commit();
                            Toast.makeText(CustomerListActivity.this,R.string.schedule_started, Toast.LENGTH_SHORT).show();
                            /*Global.Toast(CustomerListActivity.this,
                                    "Schedule Started!",
                                    Toast.LENGTH_SHORT, Font.Regular);*/
                            finish();
                            startActivity(getIntent());
                            overridePendingTransition(0,0);
                            return;



                        }
                    });

                }
            }//beginTime == null end


        } catch (Exception e2) {
            // TODO Auto-generated catch block

            e2.printStackTrace();
        }
        NavigationView mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(this);
        }


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;


        fabLayoutNewCust = (LinearLayout) findViewById(R.id.fabLayoutNewCust);
        fabLayoutCustSrch = (LinearLayout) findViewById(R.id.fabLayoutCustSrch);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseHandler db = new DatabaseHandler(
                        getApplicationContext());
                String status = db.getschedulestatus();
                if (status.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), R.string.schedules_completed, Toast.LENGTH_SHORT).show();
                    return;
                }
                String scheduleid = db.getactualscheduleid();
                String begintime = db.getscheduleheaderbegintime(scheduleid);
                if (begintime == null) {
                    String schDate = db.GetCurrentScheduledate();
                    Calendar c = Calendar.getInstance();
                    DateFormat dateTimeFormat = new SimpleDateFormat(
                            "yyyy-MM-dd", Locale.US);
                    String currenttime = dateTimeFormat.format(c.getTime());
                    if (!currenttime.contentEquals(schDate)) {
                        Toast.makeText(CustomerListActivity.this,
                                R.string.not_allowed_to_start_future_date_schedule, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (!isFABOpen) {
                    showFABMenu();
                } else {
                    closeFABMenu();
                }

            }
        });
        FloatingActionButton fabNewCust = (FloatingActionButton) findViewById(R.id.fabNewCust);
        fabNewCust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                goToActivity(NewCustomerActivity.class);
               /* Intent i = new Intent(CustomerListActivity.this,
                        NewCustomerActivity.class);
                startActivity(i);
                finish();*/
            }
        });
        FloatingActionButton fabCustSrch = (FloatingActionButton) findViewById(R.id.fabCustSrch);
        fabCustSrch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToActivity(SearchCustomerOffRouteActivity.class );
              /*  Intent i = new Intent(CustomerListActivity.this,
                        SearchCustomerOffRouteActivity.class);
                startActivity(i);
                finish();*/
            }
        });
//search fn
        searchText = (TextView) findViewById(R.id.searchText);
        srchbtn = (ImageView) findViewById(R.id.srchbtn);
        srchbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchText.setText("");

            }
        });
        searchText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.length() > 0) {
                    srchbtn.setImageResource(R.drawable.ic_dhi_deletetextn);
                } else {
                    srchbtn.setImageResource(R.drawable.ic_dhi_new_search);
                }
                //populateCustList(s.toString());
                try {
                    elemts = getScheduledRoutes(s.toString());
                    loadCustList(elemts);
                } catch (Exception e2) {
                    e2.printStackTrace();

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        ordertaking();
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.action_create_alarm:
                openorclosenavmenu();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openorclosenavmenu() {

        DrawerLayout DrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
        if (nav.isShown())
            DrawerLayout.closeDrawers();
        else
            DrawerLayout.openDrawer(nav);
    }

    private void showFABMenu() {
        isFABOpen = true;
        fabLayoutNewCust.setVisibility(View.VISIBLE);
        fabLayoutNewCust.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        fabLayoutCustSrch.setVisibility(View.VISIBLE);
        fabLayoutCustSrch.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
    }

    private void closeFABMenu() {
        isFABOpen = false;
        fabLayoutNewCust.animate().translationY(0);
        fabLayoutNewCust.setVisibility(View.GONE);
        fabLayoutCustSrch.animate().translationY(0);
        fabLayoutCustSrch.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(View view, int position) {
        //    Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {


    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        DrawerLayout schbgnpopup = (DrawerLayout) findViewById(R.id.drawer_layout);
        switch (id) {
            case R.id.nav_inoutward:
                goToActivity(InwardOutwardActivity.class);
                schbgnpopup.closeDrawers();
                return true;
            case R.id.nav_expense:
                schbgnpopup.closeDrawers();

                goToActivity(ExpenseActivity.class);
                return true;
            case R.id.nav_dsalesrpt:
                schbgnpopup.closeDrawers();
                goToActivity(DailySalesReportActivity.class);
               /* intent = new Intent(CustomerListActivity.this, DailySalesReportActivity.class);
                finish();
                startActivity(intent);*/
                //Toast.makeText(getApplicationContext(), "nav_dsalesrpt", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.nav_stockrpt:
                /*intent = new Intent(CustomerListActivity.this, StockReportActivity.class);
                startActivity(intent);
                finish();*/
                goToActivity(StockReportActivity.class);
                //Toast.makeText(getApplicationContext(), "nav_stockrpt", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.nav_auditreport:
                /*intent = new Intent(CustomerListActivity.this, AuditReportActivity.class);
                startActivity(intent);
                finish();*/
                goToActivity(AuditReportActivity.class);
                //Toast.makeText(getApplicationContext(), "nav_stockrpt", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.nav_prdctsalesrpt:
                /*intent = new Intent(CustomerListActivity.this, ProductSalesReportActivity.class);
                startActivity(intent);
                finish();*/
                goToActivity(ProductSalesReportActivity.class);
                //Toast.makeText(getApplicationContext(), "nav_prdctsalesrpt", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.nav_dashboard:
                goToActivity(SummaryActivity.class);
               /* intent = new Intent(CustomerListActivity.this, SummaryActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);*/
                return true;
            case R.id.nav_refreshcache:
                refreshcachedata();
                return true;
            case R.id.nav_fclrdata:
                forcecleardata();
                return true;
            case R.id.nav_chkupdate:
                Toast.makeText(getApplicationContext(), R.string.checking_new_version_available, Toast.LENGTH_SHORT).show();

                checkversion();
                return true;
            case R.id.nav_resetpswd:
                /*intent = new Intent(CustomerListActivity.this, ResetPasswordActivity.class);
                startActivity(intent);
                finish();*/
                goToActivity(ResetPasswordActivity.class);
              //  overridePendingTransition(0, 0);
//                Toast.makeText(getApplicationContext(), "nav_resetpswd", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.nav_about:
                goToActivity(AboutActivity.class);
               /* Intent intent2 = new Intent(CustomerListActivity.this, AboutActivity.class);
                startActivity(intent2);
                finish();
                overridePendingTransition(0, 0);*/
                return true;
            case R.id.nav_logout:
                confirnlogout();
                return true;
            case R.id.nav_completeschedl:
                try {
                    String status = db.getschedulestatus();
                    if (scheduledata.length <= 0) {
                        Toast.makeText(CustomerListActivity.this,R.string.no_schedules_available, Toast.LENGTH_SHORT).show();
                        return true;
                    }
                    else if (status.equals("Completed"))
                    {
                        Toast.makeText(CustomerListActivity.this,R.string.schedule_already_complete,Toast.LENGTH_SHORT).show();
                        return true;
                    }
                    completeschedule();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.nav_sync:
                syncData(menuItem);
                return true;

            case R.id.nav_multilingual:
                goToActivity(MultiLanguage.class);
               /* intent = new Intent(CustomerListActivity.this, MultiLanguage.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);*/
//                Toast.makeText(getApplicationContext(), "nav_resetpswd", Toast.LENGTH_SHORT).show();
                return true;

        }
        return false;
    }

    private void goToActivity(Class activityClass) {

        PackageManager manager = getApplicationContext()
                .getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(getApplicationContext()
                    .getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        //if(Global.isNewVersionAvailable(getApplicationContext())) {
        if(info.versionCode < pref.getInt("version",0)){
            Toast.makeText(CustomerListActivity.this,R.string.new_version,Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(CustomerListActivity.this, activityClass);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        }
    }

    public void forcecleardata() {
        NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
        LayoutInflater layoutInflater = (LayoutInflater) CustomerListActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View customView = layoutInflater.inflate(R.layout.forceclr_popuplayout, null);
        Button closePopupBtn = (Button) customView.findViewById(R.id.close);
        Button confirm = (Button) customView.findViewById(R.id.yes);
        TextView title = (TextView) customView.findViewById(R.id.header);
        TextView msg = (TextView) customView.findViewById(R.id.message);
        title.setText(R.string.force_clear_confirmation_title);
        msg.setText(R.string.force_clear_confirmation_message);


        //instantiate popup window

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);

        //display the popup window
        popupWindow.showAtLocation(nav, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //close the popup window on button click
        closePopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                EditText pass = (EditText) customView.findViewById(R.id.forcepass);
                String passwrd = pass.getText().toString();
                SharedPreferences pref = getApplicationContext()
                        .getSharedPreferences("Config", MODE_PRIVATE);
                String orginalpassword = pref.getString("passcode", "");
                if (passwrd.equals(orginalpassword)) {
                    //confirmlogout();
                    backupData();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.wrong_password, Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    public void backupData() {

        JSONObject data = new JSONObject();

        try {

            DatabaseHandler db = new DatabaseHandler(getApplicationContext());

            if (db.isserviceexist("Van Sales")) {

                data = getBackUpDataForVanSales();

            } else {

                data = getBackUpForOrderTaking();

            }

            //			confirmlogout();

            new BackupOperation().execute(data);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();

        }
    }

    // /upload locations
    public JSONArray uploadlocations() {

        String data = "";
        db = new DatabaseHandler(getApplicationContext());

        JSONArray datatoupload = new JSONArray();
        try {
            datatoupload = db.getlocationdata();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return datatoupload;
        // uploadorders(data);

    }

    private JSONObject getBackUpForOrderTaking() throws JSONException {

        boolean backup = true;


        JSONArray uplatlong = new JSONArray();
        JSONArray newcostomerdata = uploadlocations();
        JSONArray datadb = new JSONArray();
        db = new DatabaseHandler(getApplicationContext());

        JSONArray datatoupload = new JSONArray();
        JSONArray paymentlist = db.getpaymentstosync(backup);
        JSONArray orderpayments = db.getorderpaymentstosync(backup);
        JSONArray unvisitedstores = db.getunvisitedstores(backup);
        JSONArray closingdayupdates = db.getclosingdayupdates(backup);
        JSONArray denominationdata = db.getdenominationdatatatosync(backup);
        JSONArray orderdata = db.getorderdatatosync(backup);
        JSONArray salesreturn = db.getsalesreturndatatosync(backup);
        JSONArray creditnote = db.getcreditnotestosync(backup);
        JSONArray updatecustomers = db.getupdatecustomerstosync(backup);
        JSONArray updatephotos = db.getphotostosync(backup);
        JSONArray cratesdata = db.getcratestosync(backup);
        JSONArray passbyupdated = db.getpassbyupdatedsync(backup);

        datatoupload = db.getorders(backup);
        uplatlong = db.getupdatedlaatlong(backup);

        datadb = datatoupload;

        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = new JSONObject(inputParamsStr);

        JSONObject updatestatus = null;
        try {
            webconfigurration.status = "ClearCache";
            updatestatus = getstatusobject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputParamObj.put("updatestatus", updatestatus);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", Context.MODE_PRIVATE);
        JSONObject data = new JSONObject();
        data.put("operation", "PutMobileLog").put("data", datadb)
                .put("customers", newcostomerdata)
                .put("updatelatlong", uplatlong).put("payments", paymentlist)
                .put("reasons", unvisitedstores)
                .put("orderamountfromstores", orderpayments)
                .put("closingdayupdates", closingdayupdates)
                .put("Denominationdata", denominationdata)
                .put("OrderTaking", orderdata).put("SalesReturn", salesreturn)
                .put("CreditNotes", creditnote)
                .put("updatecustomers", updatecustomers)
                .put("updatephotos", updatephotos)
                .put("cratesdata", cratesdata)
                .put("passbyupdated", passbyupdated)
                .put("expenses", db.getExpenseDataForSync(backup));
        inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        inputParamObj.getJSONObject("data")
                .put("personid", pref.getString("personid", ""))
                .put("PersonOrg", pref.getString("tmsOrgId", ""));
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        inputParamObj.put("auth", auth);

        return inputParamObj;

    }

    //===============================================For vansales backup
    private JSONObject getBackUpDataForVanSales() throws JSONException {
        boolean backup = true;
        JSONArray uplatlong = new JSONArray();
        JSONArray newcostomerdata = uploadlocations();
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        JSONArray datadb = new JSONArray();
        JSONArray paymentlist = new JSONArray();
        JSONArray orderpayments = new JSONArray();
        JSONArray unvisitedstores = db.getunvisitedstores(backup);
        JSONArray closingdayupdates = db.getclosingdayupdates(backup);
        JSONArray denominationdata = db.getdenominationdatatatosync(backup);
        JSONArray orderdata = new JSONArray(); //db.getorderdatatosync();
        JSONArray salesreturn = new JSONArray(); //db.getsalesreturndatatosync();
        JSONArray creditnote = new JSONArray(); // db.getcreditnotestosync();
        JSONArray updatecustomers = db.getupdatecustomerstosync(backup);
        JSONArray updatephotos = db.getphotostosync(backup);
        JSONArray cratesdata = db.getcratestosync(backup);
        JSONArray passbyupdated = db.getpassbyupdatedsync(backup);

        JSONArray expenseData = db.getExpenseDataForSync(backup);

        JSONArray orderDataVansales = db.getvansalesdatatosync(backup);
        JSONArray paymentDataVansales = db.getvansalespaymentstosync(backup);
        try {
            //			datatoupload = db.getorders();
            uplatlong = db.getupdatedlaatlong(backup);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\"," +
                "\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\"," +
                "\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = new JSONObject(inputParamsStr);

        JSONObject updatestatus = null;
        try {
            webconfigurration.status = "ClearCache";
            updatestatus = getstatusobject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputParamObj.put("updatestatus", updatestatus);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", Context.MODE_PRIVATE);
        JSONObject data = new JSONObject();
        data.put("operation", "PutMobileLog").put("data", datadb)
                .put("customers", newcostomerdata)
                .put("updatelatlong", uplatlong).put("payments", paymentlist)
                .put("reasons", unvisitedstores)
                .put("orderamountfromstores", orderpayments)
                .put("closingdayupdates", closingdayupdates)
                .put("Denominationdata", denominationdata)
                .put("OrderTaking", orderdata).put("SalesReturn", salesreturn)
                .put("CreditNotes", creditnote)
                .put("updatecustomers", updatecustomers)
                .put("updatephotos", updatephotos)
                .put("cratesdata", cratesdata)
                .put("passbyupdated", passbyupdated)
                .put("expenses", expenseData);

        if (orderDataVansales.length() > 0 || paymentDataVansales.length() > 0) {
            data.put("VanSalesData", getVanSalesDataForSync(orderDataVansales, paymentDataVansales));
        }

        inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        inputParamObj.getJSONObject("data")
                .put("personid", pref.getString("personid", ""))
                .put("PersonOrg", pref.getString("tmsOrgId", ""));
        //		.put("InvNoForUpdate",db.getcurrinvoiceval());
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        inputParamObj.put("auth", auth);

        return inputParamObj;

    }

    private JSONObject getstatusobject() throws Exception {
        JSONObject temp = new JSONObject();
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);

        String person = "";
        String lat = "";
        String lng = "";
        String operation = "";
        String version = "";
        String createdon = "";
        String uuid = "";
        String appname = "";
        String versioncode = "";
        person = pref.getString("personid", "0");

        operation = webconfigurration.status;
        appname = webconfigurration.appname;
        lat = webconfigurration.lat;
        lng = webconfigurration.lng;

        PackageInfo pInfo = getPackageManager().getPackageInfo(
                getPackageName(), 0);
        version = pInfo.versionName;
        versioncode = String.valueOf(pInfo.versionCode);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        createdon = df.format(c.getTime());

        uuid = UUID.randomUUID().toString();

        temp.put("person", person).put("lat", lat).put("lng", lng)
                .put("operation", operation).put("version", version)
                .put("versioncode", versioncode).put("appname", appname)
                .put("createdon", createdon).put("uuid", uuid);
        return temp;
    }

    private JSONObject getVanSalesDataForSync(JSONArray orderDataVansales, JSONArray paymentDataVansales) throws JSONException {
        JSONObject returnObject = new JSONObject();


        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"OrderTakingForVanSales\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        returnObject = new JSONObject(inputParamsStr);

		/*JSONArray orderDataVansales = db.getvansalesdatatosync();
        JSONArray paymentDataVansales =db.getvansalespaymentstosync();*/

        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", Context.MODE_PRIVATE);
        JSONObject data = new JSONObject();

        data.put("orderData", orderDataVansales)
                .put("payments", paymentDataVansales);
        returnObject.getJSONObject("data").getJSONArray("data").put(data);
        returnObject.getJSONObject("data")
                .put("personId", pref.getString("personid", ""))
                .put("personOrg", pref.getString("tmsOrgId", ""))
                .put("InvNoForUpdate", db.getcurrinvoiceval());

        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        returnObject.put("auth", auth);

        Log.d("Data Input:", returnObject.toString());

        return returnObject;
    }

    public void refreshcachedata() {

        NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
        LayoutInflater layoutInflater = (LayoutInflater) CustomerListActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.custom_refreshcache_alert, null);
        Button closePopupBtn = (Button) customView.findViewById(R.id.close);
        Button confirm = (Button) customView.findViewById(R.id.yes);

        //instantiate popup window

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);

        //display the popup window
        popupWindow.showAtLocation(nav, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //close the popup window on button click
        closePopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                if (db.isserviceexist("Van Sales")) {
                    if (db.orderDataExist()) {
                        String status = db.getschedulestatus();
                        if (status.equals("Completed")) {
                            Synchronize synchronize = new Synchronize(CustomerListActivity.this, false,true);
                            try {
                                synchronize.syncData();
                            } catch (Exception e) {

                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), R.string.try_again, Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        else{

//                            Global.Toast(CustomerListActivity.this,"Please complete schedule for Refreshing cache",Toast.LENGTH_SHORT, Font.Regular);
                            Toast.makeText(getApplicationContext(), R.string.complete_schedule_before_refreshing, Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }
                    else{

                        Synchronize synchronize = new Synchronize(CustomerListActivity.this, false,true);
                        try {
                            synchronize.syncData();
                        } catch (Exception e) {

                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), R.string.try_again, Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }
                }

                SharedPreferences pref = getApplicationContext().getSharedPreferences(
                        "Config", MODE_PRIVATE);
                final String loginstring = pref.getString("datafromlogin", "");
                Log.d("prefer", loginstring);
                String user = pref.getString("UserName", "");
                String passcode = pref.getString("passcode", "");
                int verscode=pref.getInt("version",0);

                Log.d("User", pref.getString("User", ""));
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.commit();
                DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                db.Deletetabledata();

                SharedPreferences.Editor editor1 = pref.edit();
                editor1.putString("UserName", user);
                editor1.putString("passcode", passcode);
                editor1.putInt("version", verscode);
                editor1.commit();

                Intent login = new Intent(CustomerListActivity.this, LoginActivity.class);
                login.putExtra("refresh", true);
                login.putExtra("prefer", loginstring);
                login.putExtra("UserName", user);
                startActivity(login);
                finish();
            }
        });


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mAdapter.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mAdapter.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Synchronize synchronize = new Synchronize(CustomerListActivity.this,
                false,true);

        try {
            synchronize.syncerrors();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        try {
            synchronize.syncphotos();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        try {
            synchronize.syncData();
        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    public ScheduledRouteObject[] getScheduledRoutes(String text)
            throws Exception {

        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        JSONArray scheduleDetailDataArry = db.getscheduledata(text);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        salespersonid = pref.getString("personid", "");
        salespersonname = pref.getString("personname", "");

        //turnGPSOn();



		/*if (!isServiceRunning()) {
//			startService(new Intent(this, Vehicletracker.class));
			Log.d("service", "servicestarted");

		}*/

        if (scheduleDetailDataArry.length() > 0) {

            String Scheduledate_temp = scheduleDetailDataArry.getJSONObject(0)
                    .getString("scheduledate");

            DateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy");

            Date temp = new Date();
            try {
                temp = dateTimeFormat1.parse(Scheduledate_temp);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            DateFormat dateTimeFormat2 = new SimpleDateFormat("dd MMM  yyyy");

            salespersonid = pref.getString("personid", "");
            salespersonname = pref.getString("personname", "");
            Scheduledate = dateTimeFormat2.format(temp);

        }

        // /////////////////////////////////////

        /*
         * ScheduledRouteObject[] ObjectItemData = new ScheduledRouteObject[5];
         * JSONArray customerArray=new JSONArray(); JSONObject CustomerObj=new
         * JSONObject(); CustomerObj.put("CustName", "Kavitha Store");
         * CustomerObj.put("CustAddress", "Kaloor,Near Pavakulam temple");
         * CustomerObj.put("Status", "Completed"); CustomerObj.put("Time",
         * "10:00 AM"); customerArray.put(CustomerObj); CustomerObj=new
         * JSONObject(); CustomerObj.put("CustName", "KR Bakers");
         * CustomerObj.put("CustAddress", "Pottakuzhy");
         * CustomerObj.put("Status", "Pending"); CustomerObj.put("Time",
         * "10:30 AM"); customerArray.put(CustomerObj); CustomerObj=new
         * JSONObject(); CustomerObj.put("CustName", "Best Bakers");
         * CustomerObj.put("CustAddress", "Near Jawans Pump");
         * CustomerObj.put("Status", "Pending"); CustomerObj.put("Time",
         * "11:15 AM"); customerArray.put(CustomerObj); ObjectItemData[0] = new
         * ScheduledRouteObject("R001",
         * "Kaloor Route","10:00 AM",customerArray);
         *
         *
         * customerArray=new JSONArray(); CustomerObj=new JSONObject();
         * CustomerObj.put("CustName", "Super Bakery");
         * CustomerObj.put("CustAddress", "Near Stadium");
         * CustomerObj.put("Status", "Pending"); CustomerObj.put("Time",
         * "11:30 AM");
         *
         * customerArray.put(CustomerObj); CustomerObj=new JSONObject();
         * CustomerObj.put("CustName", "Al Maray");
         * CustomerObj.put("CustAddress", "Near Church");
         * CustomerObj.put("Status", "Pending"); CustomerObj.put("Time",
         * "11:45 AM"); customerArray.put(CustomerObj);
         *
         *
         * ObjectItemData[1] = new ScheduledRouteObject("R002",
         * "Deshabhimani Route","11:00 AM",customerArray); ObjectItemData[2] =
         * new ScheduledRouteObject("R003", "Palarivattom Route","2:00 PM",new
         * JSONArray()); ObjectItemData[3] = new ScheduledRouteObject("R004",
         * "PipeLine Route","4:00 PM",new JSONArray()); ObjectItemData[4] = new
         * ScheduledRouteObject("R005", "Vazhakkulam Route","5:00 PM",new
         * JSONArray());
         */

        return getFormattedScheduleRouteData(scheduleDetailDataArry);
    }

    public ScheduledRouteObject[] getFormattedScheduleRouteData(
            JSONArray inputJsonArray) throws JSONException {
        JSONArray OutputJsonArr = new JSONArray();
        JSONObject inputJsonObject = null;
        JSONObject oJsonObject = null;
        ScheduledRouteObject[] ObjectItemData = new ScheduledRouteObject[inputJsonArray
                .length()];
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        HashMap<String, String> invoicehashmap = db.getCustInvoice();

        for (int i = 0; i < inputJsonArray.length(); i++) {
            inputJsonObject = inputJsonArray.getJSONObject(i);

            JSONArray inCustomerListArray = inputJsonObject
                    .getJSONArray("coustemerlist");
            JSONArray outCustomerListArray = new JSONArray();
            for (int j = 0; j < inCustomerListArray.length(); j++) {

                JSONObject CustJSONObj = inCustomerListArray.getJSONObject(j);
                JSONObject CustomerObj = new JSONObject();
                CustomerObj.put("CustName",
                        CustJSONObj.getString("locationname"));
                CustomerObj.put("CustAddress",
                        CustJSONObj.getString("locationadress"));
                CustomerObj.put("Status", CustJSONObj.getString("status"));
                CustomerObj.put("Time", "");
                String custid = CustJSONObj.getString("locationid");
                CustomerObj.put("CustId", custid);
                CustomerObj.put("scheduledetailid",
                        CustJSONObj.getString("scheduledetailid"));
                CustomerObj.put("latitude", CustJSONObj.optString("latitude"));
                CustomerObj
                        .put("longitude", CustJSONObj.optString("longitude"));
                CustomerObj.put("Customercategory",
                        CustJSONObj.optString("Customercategory"));
                CustomerObj.put("customercode",
                        CustJSONObj.optString("customercode"));
                CustomerObj.put("locationlock",
                        CustJSONObj.optString("locationlock", "0"));
                CustomerObj.put("Approve",
                        CustJSONObj.optString("Approve", "0"));
                CustomerObj.put("ApproveOrder",
                        CustJSONObj.optString("ApproveOrder", "0"));
                if (invoicehashmap.containsKey(custid))
                    CustomerObj.put("Invoice", invoicehashmap.get(custid));
                else
                    CustomerObj.put("Invoice", "Invoice: NIL, Amount: 0");
                outCustomerListArray.put(CustomerObj);
            }

            Calendar c = Calendar.getInstance();
            DateFormat dateTimeFormat = new SimpleDateFormat("hh:mm a");
            String formattedDate = dateTimeFormat.format(c.getTime());

            String temp = inputJsonObject.getString("headerstatus");
            ObjectItemData[i] = new ScheduledRouteObject(
                    inputJsonObject.getString("schedulepkid"),
                    inputJsonObject.getString("schedulename"), "",
                    outCustomerListArray,
                    inputJsonObject.getString("headerstatus"),
                    inputJsonObject.optString("schedulecopletionstatus",
                            "incomplete"));
        }
        scheduledata = ObjectItemData;

        return ObjectItemData;

    }

   /* private void turnGPSOn() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!statusOfGPS) {

            AlertDialog.Builder builder = new AlertDialog.Builder(
                    ListSalesLocationActivity.this);
            builder.setTitle("Switch On GPS");
            builder.setMessage("Currently gps of the device is off");
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            startActivityForResult(
                                    new Intent(
                                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                    0);
                        }
                    });
            builder.show();

        }

    }*/

    public void loadCustList(ScheduledRouteObject[] elemts)
            throws Exception {
        final List<Schedule> schedules = new ArrayList<>();
        for (int i = 0; i < elemts.length; i++) {
            List<CustomerList> custlist = new ArrayList<>();
            ;

            try {

                String RouteId = elemts[i].RouteId;
                String RouteName = elemts[i].RouteName;
                String RouteTime = elemts[i].RouteTime;
                //child array list
                JSONArray custtemparr = elemts[i].CustomerList;
                for (int j = 0; j < custtemparr.length(); j++) {
                    JSONObject customerObject = new JSONObject();
                    customerObject = elemts[i].CustomerList
                            .getJSONObject(j);

                    String name = customerObject.getString("CustName");
                    String customercode = customerObject.getString("customercode");
                    String category = customerObject.getString("Customercategory");
                    String adr = customerObject.getString("CustAddress");
                    String invoice = customerObject.getString("Invoice");
                    String scheduledetailid = customerObject.getString("scheduledetailid");
                    String latitude = customerObject.getString("latitude");
                    String longitude = customerObject.getString("longitude");
                    String approve = customerObject.getString("Approve");
                    String approveorder = customerObject.getString("ApproveOrder");
                    String status = customerObject.getString("Status");
                    String locationlock = customerObject.getString("locationlock");
                    CustomerList custitem = new CustomerList(name, customercode, category, adr, invoice, scheduledetailid, latitude, longitude,approve,approveorder,status,locationlock);
                    custlist.add(custitem);

                }
                Schedule schObject = new Schedule(RouteName, custlist, RouteId, RouteTime);
                schedules.add(schObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        // set up the RecyclerView
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        mAdapter = new ScheduleAdapter(CustomerListActivity.this, schedules);

        mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @Override
            public void onListItemExpanded(int position) {
                Schedule expandedCustList = schedules.get(position);

                //// String toastMsg = getResources().getString(R.string.expanded, expandedMovieCategory.getName());
                //Toast.makeText(MainActivity.this,
                //    toastMsg,
                //   Toast.LENGTH_SHORT)
                //  .show();
            }

            @Override
            public void onListItemCollapsed(int position) {
                Schedule collapsedCustList = schedules.get(position);

                // String toastMsg = getResources().getString(R.string.collapsed, collapsedMovieCategory.getName());
                // Toast.makeText(MainActivity.this,
                //         toastMsg,
                //        Toast.LENGTH_SHORT)
                //        .show();
            }
        });
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (elemts.length >= 1) {
            for (int i = 0 ; i< schedules.size() ;i++){
                mAdapter.expandParent(i);
            }

        }
    }

    public void confirnlogout() {
        //====================================================logout data
        Global.force = true;
        JSONArray uplatlong = new JSONArray();
        JSONArray newcostomerdata = uploadlocations();
        JSONArray datadb = new JSONArray();
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        try {
            JSONArray closingdayupdates = db.getclosingdayupdates(false);
            JSONArray unvisitedstores = db.getunvisitedstores(false);
            JSONArray orderpayments = db.getorderpaymentstosync(false);
            JSONArray datatoupload = new JSONArray();

            JSONArray denominationdata = db.getdenominationdatatatosync(false);
            JSONArray orderdata = db.getorderdatatosync(false);
            JSONArray paymentlist = db.getpaymentstosync(false);
            JSONArray salesreturn = db.getsalesreturndatatosync(false);
            JSONArray creditnote = db.getcreditnotestosync(false);

            JSONArray updatecustomers = db.getupdatecustomerstosync(false);
            JSONArray updatephotos = db.getphotostosync(false);
            JSONArray cratesdata = db.getcratestosync(false);
            JSONArray passbyupdated = db.getpassbyupdatedsync(false);

            JSONArray expenses =  db.getExpenseDataForSync(false) ;
            if(db.isserviceexist("Van Sales")){
                orderdata = db.getvansalesdatatosync(false);
                paymentlist = db.getvansalespaymentstosync(false);
            }
            datatoupload = db.getorders(false);
            uplatlong = db.getupdatedlaatlong(false);

            datadb = datatoupload;

            if (db.getpendingNotApproved()) {
               /* Global.Toast(CustomerListActivity.this, R.string.payment_approval_pending,
                        Toast.LENGTH_SHORT, Font.Regular);*/
                Toast.makeText(CustomerListActivity.this, R.string.payment_approval_pending,
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (db.getpendingNotApprovedOrders()) {
                /*Global.Toast(CustomerListActivity.this, "Order Approvals Pending!",
                        Toast.LENGTH_SHORT, Font.Regular);*/

                Toast.makeText(CustomerListActivity.this, R.string.order_approval_pending,
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (expenses.length() > 0 ||db.anypendingorderexist() || uplatlong.length() > 0
                    || newcostomerdata.length() > 0 || paymentlist.length() > 0
                    || orderpayments.length() > 0 || unvisitedstores.length() > 0
                    || closingdayupdates.length() > 0
                    || denominationdata.length() > 0 || orderdata.length() > 0
                    || salesreturn.length() > 0 || creditnote.length() > 0
                    || updatecustomers.length() > 0 || cratesdata.length() > 0
                    || updatephotos.length() > 0 || passbyupdated.length() > 0) {
                // AlertDialog.Builder builder = new AlertDialog.Builder(
                // DashboardActivity.this);
                // builder.setTitle("Sync Data");
                // builder.setMessage("You need to sync your data before logout");
                AlertDialog builder = new AlertDialog.Builder(CustomerListActivity.this)
                        .setTitle(R.string.sync_data)
                        .setMessage( R.string.logout_sync_data_warning_message )
                        .setPositiveButton(
                                android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                    }
                                })
                        /*  .setNegativeButton(
                                  android.R.string.cancel,
                                  new DialogInterface.OnClickListener() {
                                      public void onClick(
                                              DialogInterface dialog,
                                              int which) {
                                      }
                                  })*/
                        .show();

            }
            else {
                if (db.orderDataExist()) {
                    String status = db.getschedulestatus();
                    if (status.equals("Completed")) {
                        logoutPopup();
                    }
                    else{

                        AlertDialog builder = new AlertDialog.Builder(CustomerListActivity.this)
                                .setTitle(R.string.completeShedule)
                                .setMessage( R.string.complete_schedule_before_Logout )
                                .setPositiveButton(
                                        android.R.string.ok,
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                            }
                                        })
                                /*  .setNegativeButton(
                                          android.R.string.cancel,
                                          new DialogInterface.OnClickListener() {
                                              public void onClick(
                                                      DialogInterface dialog,
                                                      int which) {
                                              }
                                          })*/
                                .show();

                        //   Toast.makeText(getApplicationContext(), R.string.complete_schedule_before_Logout, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }else{
                    logoutPopup();
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void logoutPopup(){
        NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
        LayoutInflater layoutInflater = (LayoutInflater) CustomerListActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.popuplayout, null);
        TextView header = (TextView) customView.findViewById(R.id.header);
        header.setText(R.string.logout_confirmation_title);
        TextView msg = (TextView) customView.findViewById(R.id.message);
        msg.setText(R.string.logout_confirmation_message);
        Button closePopupBtn = (Button) customView.findViewById(R.id.close);
        Button confirm = (Button) customView.findViewById(R.id.yes);

        //instantiate popup window

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);

        //display the popup window
        popupWindow.showAtLocation(nav, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //close the popup window on button click
        closePopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                logout();
            }
        });
    }

    public void logout() {

        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = dateTimeFormat.format(c.getTime());

        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        String personid = pref.getString("personid", "0");

        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = null;
        try {
            inputParamObj = new JSONObject(inputParamsStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject updatestatus = null;
        try {

            webconfigurration.status = "Logout";//getstatusobject();//
            updatestatus = SupportFunctions.getstatusobject(getApplicationContext(), "", personid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            inputParamObj.put("updatestatus", updatestatus);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject data = new JSONObject();
        try {
            data.put("operation", "PutMobileLog");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        try {
            inputParamObj.put("auth", auth);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new LongOperation2().execute(inputParamObj);

    }

    private void startTimerService() {
        // Toast.makeText(getApplicationContext(), "startTimerService on sync",
        // Toast.LENGTH_LONG).show();
        // startService(new Intent(this, CopyOfVehicletracker.class));
        // startService(new Intent(this, Vehicletracker.class));
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(CustomerListActivity.this,
                CopyOfVehicletracker.class);
        PendingIntent pintent = PendingIntent.getService(
                CustomerListActivity.this, 0, intent, 0);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                1000, pintent);

        Intent intentn = new Intent(CustomerListActivity.this, PlayTracker.class);
        PendingIntent pintentn = PendingIntent.getService(
                CustomerListActivity.this, 0, intentn, 0);
        AlarmManager alarmn = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmn.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                1000, pintentn);
    }

    private void checkversion() {
        JSONObject params = new JSONObject();
        try {
            params.put("AppName", "dsale");
            params.put("Version", "1");
            params.put("email", "");
            params.put("password", "");
            String auth = webconfigurration.auth;
            auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
            params.put("auth", auth);
            params.put("Operation", "VersionCheck");

            new LongOperation().execute(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void syncData(MenuItem menuItem) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageView iv = (ImageView) inflater.inflate(R.layout.nav_menu_item_sync, null);

        Animation rotation = AnimationUtils.loadAnimation(CustomerListActivity.this, R.anim.rotate);
        rotation.setRepeatCount(Animation.INFINITE);
        iv.startAnimation(rotation);

        menuItem.setActionView(iv);

        Synchronize synchronize = new Synchronize(CustomerListActivity.this, false,false);

        try {
            synchronize.syncData();
        } catch (Exception e) {

            e.printStackTrace();
            Toast.makeText(getApplicationContext(), R.string.try_again, Toast.LENGTH_SHORT).show();
            completeSync();
        }

//        completeSync(menuItem);
    }

    public void completeSync() {

        MenuItem menuItem = menu.findItem(R.id.nav_sync);
        if (menuItem !=null && menuItem.getActionView() != null){
            menuItem.getActionView().clearAnimation();
            menuItem.setActionView(null);
        }

    }

    private class BackupOperation extends AsyncTask<JSONObject, Void, String> {
        final ProgressDialog mProgress = new ProgressDialog(CustomerListActivity.this);

        protected void onPreExecute() {
            try {
                mProgress.setTitle("");
                mProgress.setContentView(R.layout.adapter_loading);
                mProgress.setMessage(getString(R.string.loading));
                mProgress.setCancelable(false);
                mProgress.setIndeterminate(true);
                if (mProgress != null) {
                    mProgress.dismiss();
                }
                mProgress.show();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.backupServiceUrl;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");
                conn.setConnectTimeout(100000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(200000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responsefrom;
        }

        protected void onPostExecute(final String response) {
            if(mProgress !=null){
                mProgress.dismiss();
            }

            try {
                if (response != null) {

                    JSONObject resp = new JSONObject(response);
                    if (resp.optString("msg").contentEquals("success")) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                /*try {
                                    syncmobilelog("ForceClearData");
								} catch (Exception e) {

									e.printStackTrace();
								}*/
                                SharedPreferences pref = getApplicationContext()
                                        .getSharedPreferences("Config",
                                                MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.clear();
                                editor.commit();
                                DatabaseHandler db = new DatabaseHandler(
                                        getApplicationContext());
                                Global.force = true;
                                db.Deletetabledata();
                                Intent intent = new Intent(
                                        CustomerListActivity.this,
                                        LoginActivity.class);
                                // disconnectgoogleclient();
                                finish();
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                                overridePendingTransition(0, 0);
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), R.string.cannot_connect_to_server, Toast.LENGTH_SHORT).show();

                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), R.string.cannot_connect_to_server, Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), R.string.cannot_connect_to_server, Toast.LENGTH_SHORT).show();

                    }
                });
            }
        }
    }

    private class LongOperation2 extends AsyncTask<JSONObject, Void, String> {
        private ProgressDialog mProgress;

        protected void onPreExecute() {
            try {
                mProgress = new ProgressDialog(CustomerListActivity.this);
                mProgress.setTitle("");
                mProgress.setContentView(R.layout.adapter_loading);
                mProgress.setMessage(getString(R.string.loading));
                mProgress.setCancelable(false);
                mProgress.setIndeterminate(true);
                mProgress.show();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.url;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setConnectTimeout(100000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(200000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();

            }
            return responsefrom;
        }

        protected void onPostExecute(final String response) {
            try {
                if (response != null) {

                    JSONObject resp = new JSONObject(response);
                    if (resp.optString("status").contentEquals("success")) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                SharedPreferences pref = getApplicationContext()
                                        .getSharedPreferences("Config",
                                                MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.clear();
                                editor.commit();
                                DatabaseHandler db = new DatabaseHandler(
                                        getApplicationContext());
                                Global.force = true;
                                db.Deletetabledata();
                                Intent intent = new Intent(
                                        CustomerListActivity.this,
                                        LoginActivity.class);
                                finish();
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                                overridePendingTransition(0, 0);
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), R.string.cannot_connect_to_server, Toast.LENGTH_SHORT).show();
                                if (mProgress != null) {
                                    mProgress.dismiss();
                                }
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), R.string.cannot_connect_to_server, Toast.LENGTH_SHORT).show();
                            if (mProgress != null) {
                                mProgress.dismiss();
                            }
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), R.string.cannot_connect_to_server, Toast.LENGTH_SHORT).show();
                        if (mProgress != null) {
                            mProgress.dismiss();
                        }
                    }
                });
            }
        }
    }

    private class LongOperation extends AsyncTask<JSONObject, Void, String> {
        // final Dialog dialog = new Dialog(MainActivity.this);
        protected void onPreExecute() {
            // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            // dialog.getWindow().clearFlags(
            // WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            // dialog.setContentView(R.layout.adapter_progressdialog);
            // TextView txt = (TextView) dialog.findViewById(R.id.text);
            // txt.setText("Please wait..");
            // dialog.getWindow().setBackgroundDrawable(
            // new ColorDrawable(android.graphics.Color.TRANSPARENT));
            // dialog.show();
            // dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.loginurl;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setConnectTimeout(10000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(10000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responsefrom;
        }

        protected void onPostExecute(final String response) {
            // dialog.dismiss();
            if (response != null) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        String replay = "";
                        try {
                            JSONObject resp = new JSONObject(response);
                            if (resp.optString("status").contentEquals(
                                    "success")) {
                                replay = resp.optString("Version","0");
                                int vcode = 0;
                                vcode=Integer.parseInt(replay);
                                int currnt = 0;
                                try {
                                    PackageManager manager = getApplicationContext()
                                            .getPackageManager();
                                    PackageInfo info = manager.getPackageInfo(
                                            getApplicationContext()
                                                    .getPackageName(), 0);
                                    currnt = info.versionCode;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (vcode > currnt) {
                                    Toast.makeText( getApplicationContext(), R.string.new_version,
                                            Toast.LENGTH_LONG).show();

                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putBoolean("versionavailable",true);
                                    editor.putInt("version",vcode);
                                    editor.commit();

                                }/*{
                                    AlertDialog builder = new AlertDialog.Builder(
                                            CustomerListActivity.this)
                                            .setTitle(getString(R.string.alert))
                                            .setMessage(getString(R.string.new_version) )
                                            .setPositiveButton(
                                                    android.R.string.yes,
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(
                                                                DialogInterface dialog,
                                                                int which) {
                                                            updateApp();
                                                        }

                                                        private void updateApp() {
                                                            Intent intent = new Intent(
                                                                    CustomerListActivity.this,
                                                                    DownloadActivity.class);
                                                            startActivity(intent);
                                                        }
                                                    })
                                            .setNegativeButton(
                                                    android.R.string.no,
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(
                                                                DialogInterface dialog,
                                                                int which) {
                                                        }
                                                    })
                                            .setIcon(
                                                    android.R.drawable.ic_dialog_alert)
                                            .show();
                                }*/
                                //}
                            } else
                                replay = resp.optString("Error");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(getApplicationContext(), replay,
                        // Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                // Toast.makeText(getApplicationContext(),
                // "Cannot connect to server", Toast.LENGTH_LONG).show();
            }
        }
    }


    boolean doubleBackToExitPressedOnce = false;


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;


        Toast.makeText( getApplicationContext(), R.string.touch_again_to_exit,
                Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void completeschedule() throws Exception {
        try {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.getpendingNotApproved()) {
                Toast.makeText(CustomerListActivity.this,
                        R.string.payment_approval_pending, Toast.LENGTH_SHORT).show();
            } else if (db.getPendingOrderPayment()) {
                Toast.makeText(CustomerListActivity.this,
                        R.string.pending_payments , Toast.LENGTH_SHORT).show();
            } else {
                goToActivity(CompleteScheduleActivity.class);
               /* Intent intent = new Intent(this, CompleteScheduleActivity.class);
                finish();
                startActivity(intent);
                overridePendingTransition(0,0);*/
            }
        }catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void ordertaking()
    {
        if (db.isWithoutInventory()) {

            MenuItem menuIteminoutward = menu.findItem(R.id.nav_inoutward);
            MenuItem menuItemstockrpt = menu.findItem(R.id.nav_stockrpt);
            MenuItem menuItemprdctsalesrpt = menu.findItem(R.id.nav_prdctsalesrpt);
            menuIteminoutward.setVisible(false);
            menuItemstockrpt.setVisible(false);
            menuItemprdctsalesrpt.setVisible(false);
        }
    }
}



