package dsale.dhi.com.order;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.database.DatabaseHandler;

public class ItemDetailsActivity extends AppCompatActivity {
    private ImageView imgView;
    private ImageView imgView2;
    private ImageView imgView3;
    private TextView productNameTV;
    private static DatabaseHandler db ;
    private SharedPreferences pref;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHandler(getApplicationContext());
        pref = getSharedPreferences("Config", MODE_PRIVATE);
        setContentView(R.layout.activity_itemdetais);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        loadImage();
        imgView2=(ImageView)findViewById(R.id.imageView2) ;
        imgView3=(ImageView)findViewById(R.id.imageView3) ;
        imgView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               showPrevImg();
            }
        });
        imgView3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showNextImg();
            }
        });
    }
    private void showPrevImg(){
        new DownloadImageTask(imgView,getApplicationContext())
                .execute("http://heatmap.dhisigma.com/demo/0002.jpg");
    }
    private void showNextImg(){
        new DownloadImageTask(imgView,getApplicationContext())
                .execute("http://heatmap.dhisigma.com/demo/0006.jpg");
    }
    private void loadImage() {
        imgView=(ImageView) findViewById(R.id.imageView1);
        productNameTV=(TextView) findViewById(R.id.productNameTV);
        TextView stockQty = (TextView) findViewById(R.id.stockQty);
        LinearLayout instock = (LinearLayout) findViewById(R.id.linearstock);
        TextView prdcat = (TextView) findViewById(R.id.prdcat);
        TextView brand = (TextView) findViewById(R.id.brand);
        TextView form = (TextView) findViewById(R.id.form);
        TextView mrp = (TextView) findViewById(R.id.mrp);
        TextView rate = (TextView) findViewById(R.id.rate);
        TextView tax = (TextView) findViewById(R.id.tax);
        Bundle bundle = getIntent().getExtras();
        String scheduleLineId = bundle.getString("scheduledetailid");
        String customerId= bundle.getString("customerId");
        String invoiceno= bundle.getString("invoiceno");
        String mrpVal=bundle.getString("mrp");
        String rateVal=bundle.getString("rate");
        String customerCategoryId = bundle.getString("customerCategoryId");
        Calendar C = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String currentDate = df.format(C.getTime()) + "000000";

        if (db.isWithoutInventory()) {
            instock.setVisibility(View.GONE);
        }
        String productId=bundle.getString("productId");
        String productName=bundle.getString("productName");

        productNameTV.setText(productName);
        new DownloadImageTask(imgView,getApplicationContext())
                .execute("http://heatmap.dhisigma.com/demo/0001.JPG");
        JSONObject data = new JSONObject();
        try {
            data = db.getproductratedetailsWithInventory(productId, customerCategoryId, currentDate,
                    pref.getString("tmsOrgId", ""), customerId, invoiceno);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String currencyCode=pref.getString("CurrencyCode","");

        String taxname = db.gettaxname(data.optString("tax", "NA"));
        tax.setText(taxname);
        rate.setText(rateVal+ " "+ currencyCode);
        stockQty.setText(data.optString("inventory", "Nil")
                + " " + data.optString("unit", ""));
        prdcat.setText(data.optString("prdcat", "Nil"));
        brand.setText(data.optString("brand", "Nil"));
        form.setText(data.optString("form", "Nil"));
        mrp.setText(mrpVal + " "+ currencyCode);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bundle bundle = getIntent().getExtras();
        String scheduleLineId = bundle.getString("scheduledetailid");
        Boolean complimentaryFlag = bundle.getBoolean("complimentary");
//        String parentInvoiceNo = bundle.getString("parentInvoiceNo");
        String parentOrderId =bundle.getString("parentOrderId");
        finish();
        overridePendingTransition(0,0);


        Intent intent=new Intent(ItemDetailsActivity.this, OrderTakingActivity.class);

        intent.putExtra("scheduledetailid",scheduleLineId)
                .putExtra("complimentary",complimentaryFlag)
//                .putExtra("parentInvoiceNo",parentInvoiceNo);
         .putExtra("parentOrderId",parentOrderId);
          startActivity(intent);

    }
}
