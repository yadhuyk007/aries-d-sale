package dsale.dhi.com.offroute;


import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;


import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;

public class SearchCustomerOffRouteActivity extends AppCompatActivity {
    private String searchtext = "";
    private boolean flag_loading = false;
    private boolean nomoredata = false;
    CustSearchOffRouteRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_customer_offroute);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        ImageView search = (ImageView) findViewById(R.id.searchbtn);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText searchquery = (EditText) findViewById(R.id.searchkey);
                searchtext = searchquery.getText().toString();
                if (!flag_loading) {
                    try {
                        flag_loading = true;
                        nomoredata = false;
                        additems(true);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void additems(boolean isnewsearch) throws JSONException {
        if (isnewsearch) {
            }
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        JSONArray data = db.getcustomersearchdata(searchtext);
        addSearchRsltData(data);
    }

    public void addSearchRsltData(JSONArray data) throws JSONException {
        if (data.length() == 0) {
            Toast.makeText(getApplicationContext(), R.string.no_result_found,Toast.LENGTH_SHORT).show();
        }
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.srchCustRecyclerview);
        adapter = new CustSearchOffRouteRecyclerViewAdapter(SearchCustomerOffRouteActivity.this, data);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(SearchCustomerOffRouteActivity.this));
        flag_loading = false;
    }

    public void onBackPressed() {
        Intent intent = new Intent(this, CustomerListActivity.class);
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.rev1, R.anim.rev2);
        return;
    }
}