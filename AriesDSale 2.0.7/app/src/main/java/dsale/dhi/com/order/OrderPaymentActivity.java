package dsale.dhi.com.order;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.PopupWindow.OnDismissListener;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.gps.PlayTracker;
import dsale.dhi.com.objects.DateError;
import dsale.dhi.com.objects.webconfigurration;
import dsale.dhi.com.print.PrintActivity;
import dsale.dhi.com.print.UtilPrint;

public class OrderPaymentActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_DATE_SETTING = 0;
    DatabaseHandler db ;
    private String totinvamnt="";
    private String  scheduledetailid="";
    private String  invoiceNo="";
    private String complimentInvoiceNo ="";
    private String orderid;
    private Button printbtn;
    private Button appr;
    private EditText paidamnt;
    private Spinner spinnerN ;
    private TextView invoiceamnt;
    private TextView rupeeTV;
    private TextView pendingamnt;
    private TextView invoicenotxt;
    private TextView invoicedate;
    private TextView custname;
    private TextView gstin;
    String invoicetime ="";
    private static String paymentmode = "";
    private Double totamnt=0.0;
    private Double pa=0.0;
    Double pendingamt=0.0;
    SparseArray<TextView> array;
    String[] banknames;
    String[] bankcodes;
    private double lat;
    private double lng;
    private static String checknumbervalue = "";
    private static String banknamevalue = "";
    private static String checkdatevalue = "";
    private static String banknamecode = "";
    Calendar myCalendar = Calendar.getInstance();
    String cancelled = "Cancelled";
    private int roundoffPrecision ;
    private int ExceptionFlag=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_payment);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getApplicationContext().startForegroundService(new Intent(getApplicationContext(), OrderPaymentActivity.class));
        } else {
            getApplicationContext().startService(new Intent(getApplicationContext(), OrderPaymentActivity.class));
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);


        db = new DatabaseHandler(getApplicationContext());
        Bundle bundle = getIntent().getExtras();
        totinvamnt = bundle.getString("invoiceamnt");
        scheduledetailid = bundle.getString("scheduledetailid");
        orderid = db.getorderid(scheduledetailid);
        invoiceNo = db.getInvoiceNo(orderid) ;
//        invoiceno = bundle.getString("invoiceno");
        printbtn = (Button) findViewById(R.id.printbtn);
        appr = ((Button) findViewById(R.id.approvebtn));
        paidamnt = (EditText) findViewById(R.id.paidamnt);
        invoiceamnt = (TextView) findViewById(R.id.invoiceamnt);
        rupeeTV=(TextView) findViewById(R.id.TextView02);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        String currencyCode=pref.getString("CurrencyCode","");
        roundoffPrecision = pref.getInt("RoundOffPrecision",0);
        invoiceamnt.setText(totinvamnt );
        rupeeTV.setText(currencyCode);
        pendingamnt = (TextView) findViewById(R.id.balance);
        pendingamnt.setText(totinvamnt);
        custname = (TextView) findViewById(R.id.custname);
        custname.setText(db.getcustName(scheduledetailid));
        gstin=(TextView)findViewById(R.id.gstin);
        String gst = db.getgstin(scheduledetailid);
        gstin.setText(gst);

        if (gst.length() == 0 || gst == null ) {
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.gstLayout);
            linearLayout.setVisibility(View.GONE);
            linearLayout = (LinearLayout) findViewById(R.id.custLayout);
            LinearLayout.LayoutParams lp=new LinearLayout.LayoutParams(0,120);
            lp.weight = 1 ;
        }

        invoicenotxt = (TextView) findViewById(R.id.invoiceno);
        if (invoiceNo.length() == 0){
            invoicenotxt.setText("New");
        }else {

            String complimentOrderId = db.getComplimentaryOrderId(scheduledetailid, orderid);

            if ( !complimentOrderId.contentEquals("0") ) {
                complimentInvoiceNo =  db.getInvoiceNo(complimentOrderId) ;
                invoicenotxt.setText(invoiceNo+"( "+complimentInvoiceNo+" )");
            }else{
                invoicenotxt.setText(invoiceNo);
            }

        }

        invoicedate = (TextView) findViewById(R.id.date);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        if (db.isPaymentApproved(scheduledetailid)) {
            if ((db.isorderapproved(orderid))) {
                appr.setEnabled(false);
                appr.setBackgroundResource(R.color.textDark);
            }
            appr.setText(R.string.cancelbtn);
            printbtn.setClickable(true);
            printbtn.setTextColor(getResources().getColor(R.color.textDark));
            paidamnt.setEnabled(false);
            String invdatetime=db.getpaymentdate(scheduledetailid);
            String[] date=invdatetime.split(" ");
            invoicedate.setText(date[0]);
            invoicetime=date[1];
            paidamnt.setText(db.getpaidamnt(scheduledetailid));
            paymentmode=db.getactualmode(scheduledetailid);
            totamnt=Double.parseDouble(totinvamnt);
            String paidamtstr=paidamnt.getText().toString();
            if(paidamtstr.isEmpty()){
                pa=0.0;
                pendingamt=totamnt-pa;
            }
            else{
                pa=Double.parseDouble(paidamtstr);
                pendingamt=totamnt-pa;
            }
            DecimalFormat twoDForm = new DecimalFormat("#.##");
            pendingamt = Double.valueOf(twoDForm
                    .format(pendingamt));
            String pendingamtstr=Double.toString(pendingamt);
            pendingamnt.setText(pendingamtstr);
        }
        else{
            String createdtime = df.format(c.getTime());
            invoicedate.setText(createdtime);
            invoicetime = new SimpleDateFormat("HH:mm:ss").format(c.getTime());
            paidamnt.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    // TODO Auto-generated method stub
                }
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                    // TODO Auto-generated method stub
                }
                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub
                    totamnt=Double.parseDouble(totinvamnt);
                    String paidamtstr=paidamnt.getText().toString();
                    if(paidamtstr.isEmpty()){
                        pa=0.0;
                        pendingamt=totamnt-pa;
                    }
                    else{
                        pa=Double.parseDouble(paidamtstr);
                        pendingamt=totamnt-pa;
                    }
                    DecimalFormat twoDForm = new DecimalFormat("#.##");
                    pendingamt = Double.valueOf(twoDForm
                            .format(pendingamt));
                    String pendingamtstr=Double.toString(pendingamt);
                    pendingamnt.setText(pendingamtstr);
                }
            });
        }
        try {
            ViewGroup viewGroup = (ViewGroup) findViewById(R.id.parent);
            array = new SparseArray<TextView>();
            findAllEdittexts(viewGroup);
            for (int i = 0; i < array.size(); i++) {
                TextView tempTextView = array.valueAt(i);
                String fontPath = "fonts/segoeui.ttf";
                Typeface m_typeFace = Typeface.createFromAsset(getAssets(),
                        fontPath);
                tempTextView.setTypeface(m_typeFace);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // intialise bank master spinner
        JSONArray bankdata = new JSONArray();
        try {
            bankdata = db.getbankdata();
            bankcodes = new String[bankdata.length() + 1];
            banknames = new String[bankdata.length() + 1];
            bankcodes[0] = "0";
            banknames[0] = getResources().getString(R.string.SELECT);
            for (int i = 0; i < bankdata.length(); i++) {
                JSONObject dataobj = bankdata.getJSONObject(i);
                bankcodes[i + 1] = dataobj.getString("bankcode");
                banknames[i + 1] = dataobj.getString("bankcode") + "- "
                        + dataobj.getString("bankname");
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        String[] valSpinner = { getResources().getString(R.string.select),getResources().getString(R.string.cash),getResources().getString(R.string.CDC), getResources().getString(R.string.PDC), getResources().getString(R.string.Credit)};
        spinnerN = (Spinner) findViewById(R.id.paymentmodespinner);
        SpinnerAdapter spinAdapter = new ArrayAdapter<Object>(
                getApplicationContext(), android.R.layout.simple_list_item_1,
                valSpinner) {

            @Override

            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,parent);
                try {
                    //tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextColor(Color.parseColor("#606060"));
                    tv.setTextSize(12);

                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(getAssets(),
                            fontPath);
                    tv.setTypeface(m_typeFace);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(getAssets(),
                            fontPath);
                    tv.setTypeface(m_typeFace);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        spinnerN.setAdapter(spinAdapter);
        if (paymentmode.equalsIgnoreCase(getResources().getString(R.string.select)))
            spinnerN.setSelection(0);
        else if (paymentmode.equalsIgnoreCase(getResources().getString(R.string.cash)))
            spinnerN.setSelection(1);
        else if (paymentmode.equalsIgnoreCase(getResources().getString(R.string.CDC)))
            spinnerN.setSelection(2);
        else if (paymentmode.equalsIgnoreCase(getResources().getString(R.string.PDC)))
            spinnerN.setSelection(3);
        else if (paymentmode.equalsIgnoreCase(getResources().getString(R.string.Credit)))
            spinnerN.setSelection(4);

        Spinner spinner = (Spinner) findViewById(R.id.paymentmodespinner);
        if(db.isPaymentApproved(scheduledetailid))
        {
            spinner.setEnabled(false);
        }
        else
        {
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1,
                                           int position, long arg3) {
                    if (position == 2 || position == 3) {

                        checknumbervalue = "";
                        banknamevalue = "";
                        checkdatevalue = "";
                        showchequepopup(arg1, position);
                    } else {
                        checknumbervalue = "";
                        banknamevalue = "";
                        checkdatevalue = "";
                    }
                    if(position==4){
                        paidamnt.setText("0.0");
                        paidamnt.setEnabled(false);
                    }
                    else{

                        paidamnt.setEnabled(true);
                    }
                }

                public void showchequepopup(View v, int chequetype) {
                    LayoutInflater layoutInflater = (LayoutInflater) OrderPaymentActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View customView = layoutInflater.inflate(R.layout.cdc_popuplayout, null);
                    final PopupWindow popupWindow = new PopupWindow(customView,
                            LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
                    popupWindow.setFocusable(true);
                    //display  popup window
                    LinearLayout linearLayout1 =(LinearLayout) findViewById(R.id.cdclayout);
                    popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);
                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    TextView productNameTextView = (TextView) popupWindow
                            .getContentView().findViewById(R.id.cdcorpdc);
                    cancelled = "Cancelled";

                    // intialising bank drop down
                    Spinner spinnerbankmaster = (Spinner) popupWindow
                            .getContentView().findViewById(R.id.banknamespinner);
                    SpinnerAdapter spinAdapter_bank = new ArrayAdapter<Object>(
                            OrderPaymentActivity.this, android.R.layout.simple_list_item_1,
                            banknames);
                    spinnerbankmaster.setAdapter(spinAdapter_bank);
                    int selctedpos = -1;
                    for (int i = 0; i < banknames.length; i++) {
                        if (banknames[i].trim().equalsIgnoreCase(
                                banknamevalue.trim())) {
                            selctedpos = i;
                            break;
                        }
                    }
                    if (selctedpos >= 0)
                        spinnerbankmaster.setSelection(selctedpos);

                    if (chequetype == 2)
                        productNameTextView.setText(R.string.Cheque_CDC);

                    else
                        productNameTextView.setText(R.string.Cheque_PDC);

                    final EditText edittext = (EditText) popupWindow
                            .getContentView().findViewById(R.id.checquedate);
                    edittext.setEnabled(false);
                    edittext.setText(checkdatevalue);

                    EditText checknumber = (EditText) popupWindow.getContentView()
                            .findViewById(R.id.checkno);
                    checknumber.setText(checknumbervalue);

                    final ImageView datepick = (ImageView) popupWindow.getContentView()
                            .findViewById(R.id.datepickericon);
                    final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // TODO Auto-generated method stub
                            myCalendar.set(Calendar.YEAR, year);
                            myCalendar.set(Calendar.MONTH, monthOfYear);
                            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            updateLabel();
                        }

                        private void updateLabel() {
                            String myFormat = "dd/MM/yyyy"; // In which you need put
                            // here
                            SimpleDateFormat sdf = new SimpleDateFormat(myFormat,
                                    Locale.US);
                           edittext.setText(sdf.format(myCalendar.getTime()));
                        }
                    };
                    datepick.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            new DatePickerDialog(OrderPaymentActivity.this, date, myCalendar
                                    .get(Calendar.YEAR), myCalendar
                                    .get(Calendar.MONTH), myCalendar
                                    .get(Calendar.DAY_OF_MONTH)).show();
                        }
                    });

                    final EditText chequedate = (EditText) popupWindow
                            .getContentView().findViewById(R.id.checquedate);
                    final int chquetype_choosen = chequetype;
                    if (chquetype_choosen == 2){
                        Calendar cal = Calendar.getInstance();
                        Date dateafter_2_days = cal.getTime();
                        String Cheque_date_choosen = "";
                        DateFormat dateTimeFormat = new SimpleDateFormat(
                                "dd/MM/yyyy");
                        Cheque_date_choosen = dateTimeFormat
                                .format(dateafter_2_days);
                        chequedate.setText(Cheque_date_choosen);
                        chequedate.setEnabled(false);
                        datepick.setEnabled(false);
                    }


                   Button btnasve = (Button) customView.findViewById(R.id.save);
                    btnasve.setOnClickListener(new Button.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                           /* EditText Rate = (EditText) popupWindow.getContentView()
                                    .findViewById(R.id.amount);
                            String amount = Rate.getText().toString();*/

                            EditText checknumber = (EditText) popupWindow
                                    .getContentView().findViewById(
                                            R.id.checkno);
                            String chknbr = checknumber.getText().toString();

                            Spinner bankname = (Spinner) popupWindow
                                    .getContentView().findViewById(
                                            R.id.banknamespinner);
                            String ifsccdval = bankname.getSelectedItem()
                                    .toString();

                            String selecteditemcode = bankcodes[bankname
                                    .getSelectedItemPosition()];

                            String chequedatevalue = chequedate.getText()
                                    .toString();

                            if (chknbr.equals("")) {
                                Toast.makeText(getApplicationContext(),
                                        R.string.Cheque_number_Needed, Toast.LENGTH_SHORT)
                                        .show();
                                return;
                            }
                            if (ifsccdval.equalsIgnoreCase(getResources().getString(R.string.select))) {
                                Toast.makeText(getApplicationContext(),
                                        R.string.Bank_Name_needed, Toast.LENGTH_SHORT)
                                        .show();
                                return;
                            }
                            if (chequedatevalue.equals("")) {
                                Toast.makeText(getApplicationContext(),
                                        R.string.Date_not_valid, Toast.LENGTH_SHORT)
                                        .show();
                                return;

                            }
                            if (chquetype_choosen == 3) {
                                Calendar cal = Calendar.getInstance();
                                Date currdate = cal.getTime();
                                DateFormat dateTimeFormat = new SimpleDateFormat(
                                        "dd/MM/yyyy");
                                Date Cheque_date_choosen = new Date();
                                try {
                                    Cheque_date_choosen = dateTimeFormat
                                            .parse(chequedatevalue);
                                } catch (ParseException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                if (Cheque_date_choosen.compareTo(currdate) <= 0) {
                                    Toast.makeText(getApplicationContext(),
                                            R.string.Invalid_Date, Toast.LENGTH_SHORT)
                                            .show();
                                    return;
                                }

                            }

                            checknumbervalue = chknbr;
                            banknamevalue = ifsccdval;
                            banknamecode = selecteditemcode;
                            checkdatevalue = chequedatevalue;
                            cancelled = "OK";
                            popupWindow.dismiss();

                        }
                    });
                    Button cancel = (Button) customView.findViewById(R.id.cancel);
                    cancel.setClickable(false);
                    cancel.setOnClickListener(new Button.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                    OrderPaymentActivity.this);
                            builder.setTitle(R.string.Changing_Paymentmode);
                            builder.setMessage(R.string.cheque_cancel_warning);
                            builder.setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            Spinner mySpinner = (Spinner) findViewById(R.id.paymentmodespinner);
                                            mySpinner.setSelection(0);
                                            dialog.dismiss();
                                            popupWindow.dismiss();
                                        }
                                    });
                            builder.setNeutralButton("CANCEL",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.dismiss();

                                        }
                                    });
                            builder.show();
                        }
                    });
                    popupWindow.showAsDropDown(v, 50, -30);
                    popupWindow.setOnDismissListener(new OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            if (!(cancelled.contentEquals("OK"))) {
                                Spinner mySpinner = (Spinner) findViewById(R.id.paymentmodespinner);
                                mySpinner.setSelection(0);
                            }
                        }
                    });
                }
                @Override
                public void onNothingSelected(AdapterView<?> arg0) {

                }
            });
        }//end else
    }

    private void findAllEdittexts(ViewGroup viewGroup) {
        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof ViewGroup)
                findAllEdittexts((ViewGroup) view);
            else if (view instanceof TextView) {
                TextView edittext = (TextView) view;
                array.put(edittext.getId(), edittext);
            }
        }

    }

    public void approve(View v){
    int ex=0;
        if(db.isPaymentApproved(scheduledetailid) && (!db.isorderapproved(orderid))){
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    OrderPaymentActivity.this);
            builder.setTitle(R.string.confirm_cancelPayment);
            builder.setMessage(R.string.Current_Payment_will_be_cancelled);
            builder.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                            try {
                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat df = new SimpleDateFormat(
                                        "yyyyMMddHHmmss");
                                String cancelTime = df.format(c.getTime());
                                db.cancelPayment(scheduledetailid, orderid,cancelTime);
                                finish();
                                startActivity(getIntent());
                                overridePendingTransition(0, 0);
                            } catch (DateError e) {
                                callDateTimeSetting();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            builder.setNeutralButton("CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
        }
        else
        {
            final String paidamntval=paidamnt.getText().toString();
            paymentmode=spinnerN.getSelectedItem().toString();
            Calendar c1 = Calendar.getInstance();
            DateFormat dateTimeFormat2 = new SimpleDateFormat(
                    "yyyyMMddHHmmss");

            DateFormat dateTimeFormat_payment = new SimpleDateFormat(
                    "dd/MM/yyyy HH:mm:ss");

            final String createtime = dateTimeFormat2
                    .format(c1.getTime());
            final String paymentdate = dateTimeFormat_payment
                    .format(c1.getTime());
            //gps check
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean statusOfGPS = manager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!statusOfGPS) {

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        OrderPaymentActivity.this);
                builder.setTitle(R.string.switch_on_gps);
                builder.setMessage(R.string.gps_off_alert_message);
                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                startActivityForResult(
                                        new Intent(
                                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                        0);
                            }
                        });
                builder.show();
                return;
            }

            Location loc =null;
            lat=0.0 ;
            lng=0.0 ;
            try {
                if (PlayTracker.getLastLocation() != null) {
                    loc = PlayTracker.getLastLocation();
                    lat =  loc.getLatitude();
                    lng = loc.getLongitude();
                }
            }catch (Exception e){
                e.printStackTrace();
                ExceptionFlag =1;
                if (paymentmode.equals(getResources().getString(R.string.CDC))
                        || paymentmode.equals(getResources().getString(R.string.PDC)))
                    paymentmode = "Cheque";

                final  String actualmode = spinnerN.getSelectedItem().toString();

                if(TextUtils.isEmpty(paidamntval)){
                    paidamnt.setError(getResources().getString(R.string.enter_paid_amount));
                }
                else if (paymentmode.equalsIgnoreCase(getResources().getString(R.string.select))){
                    Toast.makeText(getApplicationContext(),
                            R.string.select_payment_mode, Toast.LENGTH_SHORT)
                            .show();
                }

                else if( (pa<0)){//else if((pa>totamnt)||(pa<0)){

                    paidamnt.setError(getResources().getString(R.string.Invalid_paid_amount));
                }
                else if(!paymentmode.equalsIgnoreCase(getResources().getString(R.string.Credit))&& (pa==0)){
                    paidamnt.setError(getResources().getString(R.string.Invalid_paid_amount));
                }
                else{
                    LinearLayout linearLayout3=(LinearLayout)findViewById(R.id.linearLayout3);
                    LayoutInflater layoutInflatr = (LayoutInflater) OrderPaymentActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View custView = layoutInflatr.inflate(R.layout.popuplayout, null);
                    TextView hdr = (TextView) custView.findViewById(R.id.header);
                    hdr.setText(R.string.confirm_makepayment);
                    TextView msg = (TextView) custView.findViewById(R.id.message);
                    msg.setText(R.string.verify_making_payment);
                    Button closPopupBtn = (Button) custView.findViewById(R.id.close);
                    Button cnfirm = (Button) custView.findViewById(R.id.yes);
                    //instantiate popup window
                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
                    final PopupWindow popupWindw = new PopupWindow(custView, width, height);
                    popupWindw.setFocusable(true);
                    //display the popup window
                    popupWindw.showAtLocation(linearLayout3, Gravity.CENTER, 0, 0);
                    popupWindw.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    //close the popup window on button click
                    closPopupBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindw.dismiss();
                        }
                    });
                    cnfirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindw.dismiss();
                            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                            try {

                                String complimentOrderId = db.getComplimentaryOrderId(scheduledetailid, orderid);

                                if (!complimentOrderId.contentEquals("0")) {
                                    HashMap< String, OrderLine > temp = db.getSavedOrderLineData(complimentOrderId);
                                    /**
                                     * If there is no item in the compliment invoice for this invoice then
                                     *  mark compliment invoice as cancelled other wise invoice number sequence
                                     *  seems to be having gap
                                     */
                                    if(temp.size() == 0){
                                        db.cancelInvoice(complimentOrderId,createtime,scheduledetailid);
                                    }
                                }


                                if (invoiceNo.length() == 0){
                                    invoiceNo = db.invoicenogenerator(orderid);

                                    if ( !complimentOrderId.contentEquals("0")   ) {
                                        if (complimentInvoiceNo.length() == 0){
                                            complimentInvoiceNo = db.invoicenogenerator(complimentOrderId);
                                        }

                                        db.updateParentInvoiceNo(complimentOrderId,invoiceNo);
                                    }

                                }


                                String headerid=db.insertvansalespayment(scheduledetailid,paymentmode,paidamntval,
                                        checknumbervalue,banknamevalue,createtime,checkdatevalue,paymentdate,invoiceNo,
                                        lat,lng,actualmode,banknamecode,pendingamt,totinvamnt);
                                db.approveorder(scheduledetailid,createtime);
                                db.approvepayment(headerid,createtime);
                                printbtn.setClickable(true);
                                //printbtn.setBackgroundResource(R.drawable.dhi_payment_button);
                                printbtn.setTextColor(getResources().getColor(R.color.textDark));
                                if(  (db.isorderapproved(orderid))){

                                    appr.setEnabled(false);
                                    appr.setBackgroundResource(R.color.textDark);
                                }
                                appr.setText(R.string.cancelbtn);
                                paidamnt.setEnabled(false);
                                spinnerN.setEnabled(false);

                                if ( !complimentOrderId.contentEquals("0") ) {
                                    invoicenotxt.setText(invoiceNo+"( "+complimentInvoiceNo+" )");
                                }else{
                                    invoicenotxt.setText(invoiceNo);
                                }

                                Toast.makeText( getApplicationContext(), R.string.approved,
                                        Toast.LENGTH_SHORT).show();
                            }  catch (DateError e) {
                                callDateTimeSetting();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            ex = 1;
                //---
            }
            if (ExceptionFlag!=1){
                if (ex==0){
                    if (paymentmode.equals(getResources().getString(R.string.CDC))
                            || paymentmode.equals(getResources().getString(R.string.PDC)))
                        paymentmode = "Cheque";

                    final  String actualmode = spinnerN.getSelectedItem().toString();

                    if(TextUtils.isEmpty(paidamntval)){
                        paidamnt.setError(getResources().getString(R.string.enter_paid_amount));
                    }
                    else if (paymentmode.equalsIgnoreCase(getResources().getString(R.string.select))){
                        Toast.makeText(getApplicationContext(),
                                R.string.select_payment_mode, Toast.LENGTH_SHORT)
                                .show();
                    }

                    else if( (pa<0)){//else if((pa>totamnt)||(pa<0)){

                        paidamnt.setError(getResources().getString(R.string.Invalid_paid_amount));
                    }
                    else if(!paymentmode.equalsIgnoreCase(getResources().getString(R.string.Credit))&& (pa==0)){
                        paidamnt.setError(getResources().getString(R.string.Invalid_paid_amount));
                    }
                    else{
                        LinearLayout linearLayout3=(LinearLayout)findViewById(R.id.linearLayout3);
                        LayoutInflater layoutInflatr = (LayoutInflater) OrderPaymentActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View custView = layoutInflatr.inflate(R.layout.popuplayout, null);
                        TextView hdr = (TextView) custView.findViewById(R.id.header);
                        hdr.setText(R.string.confirm_makepayment);
                        TextView msg = (TextView) custView.findViewById(R.id.message);
                        msg.setText(R.string.verify_making_payment);
                        Button closPopupBtn = (Button) custView.findViewById(R.id.close);
                        Button cnfirm = (Button) custView.findViewById(R.id.yes);
                        //instantiate popup window
                        int width = LinearLayout.LayoutParams.MATCH_PARENT;
                        int height = LinearLayout.LayoutParams.MATCH_PARENT;
                        final PopupWindow popupWindw = new PopupWindow(custView, width, height);
                        popupWindw.setFocusable(true);
                        //display the popup window
                        popupWindw.showAtLocation(linearLayout3, Gravity.CENTER, 0, 0);
                        popupWindw.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                        //close the popup window on button click
                        closPopupBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                popupWindw.dismiss();
                            }
                        });
                        cnfirm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                popupWindw.dismiss();
                                DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                                try {

                                    String complimentOrderId = db.getComplimentaryOrderId(scheduledetailid, orderid);

                                    if (!complimentOrderId.contentEquals("0")) {
                                        HashMap< String, OrderLine > temp = db.getSavedOrderLineData(complimentOrderId);
                                        /**
                                         * If there is no item in the compliment invoice for this invoice then
                                         *  mark compliment invoice as cancelled other wise invoice number sequence
                                         *  seems to be having gap
                                         */
                                        if(temp.size() == 0){
                                            db.cancelInvoice(complimentOrderId,createtime,scheduledetailid);
                                        }
                                    }


                                    if (invoiceNo.length() == 0){
                                        invoiceNo = db.invoicenogenerator(orderid);

                                        if ( !complimentOrderId.contentEquals("0")   ) {
                                            if (complimentInvoiceNo.length() == 0){
                                                complimentInvoiceNo = db.invoicenogenerator(complimentOrderId);
                                            }

                                            db.updateParentInvoiceNo(complimentOrderId,invoiceNo);
                                        }

                                    }


                                    String headerid=db.insertvansalespayment(scheduledetailid,paymentmode,paidamntval,
                                            checknumbervalue,banknamevalue,createtime,checkdatevalue,paymentdate,invoiceNo,
                                            lat,lng,actualmode,banknamecode,pendingamt,totinvamnt);
                                    db.approveorder(scheduledetailid,createtime);
                                    db.approvepayment(headerid,createtime);
                                    printbtn.setClickable(true);
                                    //printbtn.setBackgroundResource(R.drawable.dhi_payment_button);
                                    printbtn.setTextColor(getResources().getColor(R.color.textDark));
                                    if(  (db.isorderapproved(orderid))){

                                        appr.setEnabled(false);
                                        appr.setBackgroundResource(R.color.textDark);
                                    }
                                    appr.setText(R.string.cancelbtn);
                                    paidamnt.setEnabled(false);
                                    spinnerN.setEnabled(false);

                                    if ( !complimentOrderId.contentEquals("0") ) {
                                        invoicenotxt.setText(invoiceNo+"( "+complimentInvoiceNo+" )");
                                    }else{
                                        invoicenotxt.setText(invoiceNo);
                                    }

                                    Toast.makeText( getApplicationContext(), R.string.approved,
                                            Toast.LENGTH_SHORT).show();
                                }  catch (DateError e) {
                                    callDateTimeSetting();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }


        }//end else
    }

    private void callDateTimeSetting() {
       Toast.makeText(getApplicationContext(),
                R.string.correct_mob_date_time, Toast.LENGTH_SHORT)
                .show();
        Intent dateSetttingIntent = new Intent(android.provider.Settings.ACTION_DATE_SETTINGS);
        startActivityForResult(dateSetttingIntent, REQUEST_CODE_DATE_SETTING);
    }

    public void printinvoice(View v){

        if(db.isPaymentApproved(scheduledetailid) ){
            if (totinvamnt.equals("0")) {
               /* double taxableAmnt = db.getTotalTaxableAmnt(orderid);
                double taxAmnt = 0;
                taxAmnt = db.getTotalTaxAmount(orderid);
                double ordAmnt = taxableAmnt+taxAmnt ;
                totinvamnt = Math.round(ordAmnt)+"";*/

                /**
                 * As of now for print scaling is done for 2 decimal points.
                 */
                BigDecimal taxableAmnt = db.getTotalTaxableAmnt(orderid,2);
                BigDecimal taxAmnt = new BigDecimal(0);
                taxAmnt = db.getTotalTaxAmount(orderid,2);
                BigDecimal ordAmnt = taxableAmnt.add(taxAmnt) ;
                ordAmnt =ordAmnt.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_EVEN);
                totinvamnt = ordAmnt.toString();

            }
            String message1 = "";
            webconfigurration w = new webconfigurration(getApplicationContext() );
            if (/*w.rateWithOutTax &&*/ w.showDiscPercentage ) {
                message1 = getInvoiceCustomerPrintData();
            }else{
                message1 = getInvoiceForPrint();
            }
            String message2 = getMessageToPrint(false);//company copy
            String compliment = getMessageToPrint(true);//compliment invoice print.
            if ( message2.length() > 0) {
                Intent intent=new Intent(OrderPaymentActivity.this, PrintActivity.class);
                intent.putExtra("message1",message1 );
                intent.putExtra("message2",message2 );
                intent.putExtra("message3",compliment );
                intent.putExtra("message1LandscapeMode",true );
                intent.putExtra("message2LandscapeMode",false );
                intent.putExtra("message3LandscapeMode",false );

                finish();
                overridePendingTransition(0, 0);
                startActivity(intent);
            }else {

                Toast.makeText(OrderPaymentActivity.this,R.string.error_printing, Toast.LENGTH_SHORT)
                        .show();
            }

        }else   {

            Toast.makeText(OrderPaymentActivity.this,R.string.approve_bfr_printing, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    /**
     * Written for Sabari
     * Changes  - Rate must be without tax,Disc % column added,Round off Amount shown
     *
     * @return
     */
    private String getInvoiceCustomerPrintData(){

        String invoiceNo = db.getInvoiceNo(orderid) ;
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        String printmsg = "";
        try {
            String custDetails =db.getcustdetails(scheduledetailid).toUpperCase();
            String customer = UtilPrint.getPrintFormat(custDetails.split("\n")[0], 30, 0);
            String customerAddres =UtilPrint.getPrintFormat(custDetails.split("\n")[1], 37, 0);
            String custGST =UtilPrint.getPrintFormat(db.getgstin(scheduledetailid).toUpperCase(), 15, 0);
            String invNo= UtilPrint.getPrintFormat(invoiceNo, 15, 0);
            String invDate=UtilPrint.getPrintFormat(invoicedate.getText()+"",10, 0);
            String invTime =UtilPrint.getPrintFormat(invoicetime+"",8, 0); //"10:06:00";
            String payMode= UtilPrint.getPrintFormat(spinnerN.getSelectedItem().toString().toUpperCase(), 6, 0)  ; //"CHEQUE";

            String paidAmnt = UtilPrint.getPrintFormat(paidamnt.getText().toString(), 8, 0) ;//"10000.00";
            DecimalFormat twoDForm = new DecimalFormat("#.##");
            String grosTax =twoDForm.format(db.getTotalTaxAmount(orderid,2).doubleValue());
            grosTax   =UtilPrint.getPrintFormat(grosTax, 7, 0);//"5000.00";
            String grosamt =UtilPrint.getPrintFormat(totinvamnt, 8, 0) ; //"100000.00";

            double creditAmount = Double.parseDouble(totinvamnt) - Double.parseDouble(paidamnt.getText().toString());
            String credit =UtilPrint.getPrintFormat( (creditAmount<=0) ? "0.00": creditAmount+"", 8, 0) ;//"10000.00";
            String salesperson = UtilPrint.getPrintFormat(pref.getString("personname", ""), 24, 0); //XXXVINEESH TV...........
            String productString ="";

            JSONArray productArray =  db.getInvoiceDetailsForPrint(orderid);


            for (int i = 0; i < productArray.length(); i++) {
                JSONObject product = productArray.getJSONObject(i);
                String unit = product.optString("uom","").length() == 0 ? " ":product.optString("uom","").charAt(0)+"";
                String item = UtilPrint.getPrintFormat((i+1)+"."+product.getString("productname"), 32, 0);
                String hsncode = UtilPrint.getPrintFormat(product.getString("HSNCode"), 9, 0);
                String qty= UtilPrint.getPrintFormat(product.getString("Qty")+"("+unit+")", 8, 2);//" 100(K) " ;//' 100(K) '

                double cgstRate = Double.parseDouble(product.getString("CGSTrate"));
                double sgstRate = Double.parseDouble(product.getString("SGSTrate"));
                double otherTrate = Double.parseDouble(product.optString("otherTrate","0"));
                double otherTamnt = Double.parseDouble(product.optString("otherTamnt","0"));
                double unitRate = Double.parseDouble(product.getString("mrp"));

                double taxRate = cgstRate+sgstRate+otherTrate;

                //				double taxAmount = (unitRate *taxRate)/100 ;

                unitRate = (unitRate*100)/(100+taxRate);
                DecimalFormat twoDFormRate = new DecimalFormat("#.###");
                String rate= UtilPrint.getPrintFormat( twoDFormRate.format(unitRate), 8, 2);

                double grosAmnt = ( Double.parseDouble(product.getString("actualtotal")) * 100)/(100+taxRate);

				/*String actualtotal=getPrintFormat(
								twoDForm.format(Double.parseDouble(product.getString("actualtotal"))), 9, 2); */
                String total=UtilPrint.getPrintFormat(
                        twoDForm.format(grosAmnt), 8, 2);
                String disc= UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("discount"))), 8, 2);
                String txblval=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("taxableamnt"))), 10, 2);
                String cgstper=UtilPrint.getPrintFormat( twoDForm.format(cgstRate), 7,2);
                String cgstamt=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("CGSTamnt"))),10,2);
                String sgstper=UtilPrint.getPrintFormat( twoDForm.format(sgstRate), 7, 2);
                String sgstamt=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("SGSTamnt"))),10, 2);
                String tottax=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("taxtotal"))),7,2);
                String totamnt=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("total"))), 13, 2);

//                String cessper=UtilPrint.getPrintFormat( twoDForm.format(otherTrate), 7, 2);
                String cessamt=UtilPrint.getPrintFormat(
                        twoDForm.format(otherTamnt),10, 2);

                String discper= UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("discountPer"))), 7, 2);
				/*String rate= getPrintFormat(product.getString("mrp"), 8, 2);//" 116.91 " ;
				String total=getPrintFormat(product.getString("total"), 9, 2); //" 1169.10 " ;
				String disc= getPrintFormat(product.getString("discount"), 9, 2);//" 1169.10 " ;
				String txblval=getPrintFormat(product.getString("taxableamnt"), 11, 2); //"  1169.10 " ;
				String cgstper=getPrintFormat(product.getString("CGSTrate"), 7,2); //"  02.50 " ;
				String cgstamt=getPrintFormat(product.getString("CGSTamnt"),11,2);//"  0025.00 " ;
				String sgstper=getPrintFormat(product.getString("SGSTrate"), 7, 2);//"  02.50 " ;
				String sgstamt=getPrintFormat(product.getString("SGSTamnt"),11, 2);//"  0025.00 " ;
				String tottax=getPrintFormat(product.getString("taxtotal"),7,2);//"  50.00  " ;
				String totamnt=getPrintFormat(product.getString("total"), 15, 2);//"   1333.00    " ;
				 */
				/*			String item ="3.TENDER MANGO PIC. POUCH 200gm ";//'2.CORIANDERPOW. 100gm           ' '1.CHILLY POW. 100gm             '
				String hsncode =" 20109000";//' 9109929 ' ' 9042000 '9
				String qty=" 100(K) " ;//' 100(K) '
				String rate=" 116.91 " ;
				String total=" 1169.10 " ;
				String disc=" 1169.10 " ;
				String txblval="  1169.10 " ;
				String cgstper="  02.50 " ;
				 String cgstamt="  0025.00 " ;
				 String sgstper="  02.50 " ;
				 String sgstamt="  0025.00 " ;
				 String tottax="  50.00  " ;
				 String totamnt="   1333.00    " ;*/
                productString =productString+item+hsncode+qty+rate+total+discper+
                        disc+txblval+cgstper+cgstamt+sgstper+sgstamt+tottax+totamnt;
                // StringBuilder strb = new StringBuilder(productString).append(str)
            }

			/*String companyName = "   Greenmount Spices Pvt Ltd,Neriamangalam P.O,Idukki,Kerala   ";//63
			String officeAddress="   Reg. office:Nellikuzhi P.O,686691,Kothamangalam,Ernakulam   ";
			String officeContact="     Phone:0485-2827371,9495930076  GSTIN:32AACCG0501B1ZR      ";*/
            webconfigurration w = new webconfigurration(getApplicationContext() );


            String companyName = UtilPrint.getPrintFormat(w.companyNameLong, 63, 2);//63
            String officeAddress=UtilPrint.getPrintFormat(w.companyAddressLong, 68, 2);
            String officeContact=UtilPrint.getPrintFormat(w.companyContact+" "+w.companyGSTIN+" "+"STATE CODE:32", 75, 2);

            double roundoff =   Double.parseDouble(totinvamnt) - ( db.getTotalTaxAmount(orderid,2).doubleValue() + db.getTotalTaxableAmnt(orderid,2).doubleValue() );
            String roundoffString  = UtilPrint.getPrintFormat( twoDForm.format(roundoff), 7, 0); //00.00;7
                      /* printmsg =
                    //print4: print2 corrected	FINAL, CASH CHANGED TO CHEQUE		  "123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789*123456789*123456789*123456789*"; //"<0x09>" +"" +

                    "BILLED TO:"+customer+"                       TAX INVOICE                                                  INV#:"+invNo+
                            customerAddres+" "+companyName +"                        INV DATE:"+ invDate+
                            "CUST GSTIN:"+custGST+"            "+officeAddress+"                   TIME:"+ invTime+"      "+
                            //					"                                      "+officeContact+"                        PAYMENT MODE:"+payMode+
                            "STATE CODE:32                          "+officeContact+"           PAYMENT MODE:"+payMode+
                            "................................................................................................................................................"+
                            "SL      ITEM			  HSNCODE  QTY(U)   RATE   TOTAL	DIS%    DISC   G.AMNT   CGST%   CGSTAMT   SGST%   SGSTAMT   TOT.TAX  TOT.AMNT "+
                            productString+
                            "................................................................................................................................................"+
                            "CREDIT BAL:"+credit+"    CESS:00.00    ROUNDOFF:"+roundoffString  +"    PRVD BAL:0000.00    PAID AMNT:"+paidAmnt+"     TOT.TAX AMT:"+grosTax+"       BILL AMT:"+grosamt+
                            "........................................................................................SALES:"+salesperson+"..SIGN ...................";
*/
            printmsg =
                    //print4: print2 corrected	FINAL, CASH CHANGED TO CHEQUE		  "123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789*123456789*123456789*123456789*"; //"<0x09>" +"" +

                    getResources().getString(R.string.BILLED_TO)+customer+"                       "+getResources().getString(R.string.TAX_INVOICE)+"                                                  "+getResources().getString(R.string.INV)+invNo+
                            customerAddres+" "+companyName +"                        "+getResources().getString(R.string.INV_DATE)+ invDate+
                            getResources().getString(R.string.CUST_GSTIN)+custGST+"            "+officeAddress+"                   "+getResources().getString(R.string.TIME)+ invTime+"      "+
                            //					"                                      "+officeContact+"                        PAYMENT MODE:"+payMode+
                            getResources().getString(R.string.STATE_CODE)+"                          "+officeContact+"           "+getResources().getString(R.string.PAYMENTMODE)+payMode+
                            "................................................................................................................................................"+
                            getResources().getString(R.string.SL)+"      "+getResources().getString(R.string.ITEM)+"			  "+getResources().getString(R.string.HSNCODE)+"  "+getResources().getString(R.string.QTY_U)+"   "+getResources().getString(R.string.RATE)+"   "+getResources().getString(R.string.TOTAL)+"	"+getResources().getString(R.string.DIS_perc)+"    "+getResources().getString(R.string.DISC)+"   "+getResources().getString(R.string.G_AMNT)+"   "+getResources().getString(R.string.CGST_perc)+"   "+getResources().getString(R.string.CGSTAMT)+"   "+getResources().getString(R.string.SGST_perc)+"   "+getResources().getString(R.string.SGSTAMT)+"   "+getResources().getString(R.string.TOT_TAX)+"  "+getResources().getString(R.string.TOT_AMNT)+" "+
                            " "+productString+
                            "................................................................................................................................................"+
                            getResources().getString(R.string.CREDIT_BAL)+credit+"    "+getResources().getString(R.string.CESS)+"00.00"+"    "+getResources().getString(R.string.ROUNDOFF_label)+roundoffString  +"    "+getResources().getString(R.string.PRVD_BAL)+"    "+getResources().getString(R.string.PAID_AMNT)+paidAmnt+"     "+getResources().getString(R.string.TOT_TAX_AMT)+grosTax+"       "+getResources().getString(R.string.BILL_AMT)+grosamt+
                            "........................................................................................"+getResources().getString(R.string.SALES)+salesperson+".."+getResources().getString(R.string.SIGN)+" ...................";


        } catch ( Exception e) {

            e.printStackTrace();
        }


        return printmsg;

    }

    /**
     * Print Customer Copy Of Invoice for Greenmount
     * @return
     */
    private String getInvoiceForPrint() {
        String invoiceNo = db.getInvoiceNo(orderid) ;
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);

        DecimalFormat twoDForm = new DecimalFormat("#.##");
        String printmsg = "";
        try {
            String custDetails =db.getcustdetails(scheduledetailid).toUpperCase();
            String customer = UtilPrint.getPrintFormat(custDetails.split("\n")[0], 30, 0);
            String customerAddres =UtilPrint.getPrintFormat(custDetails.split("\n")[1], 37, 0);
            String custGST =UtilPrint.getPrintFormat(db.getgstin(scheduledetailid).toUpperCase(), 15, 0);
            String invNo= UtilPrint.getPrintFormat(invoiceNo, 12, 0);
            String invDate=UtilPrint.getPrintFormat(invoicedate.getText()+"",10, 0);
            String invTime =UtilPrint.getPrintFormat(invoicetime+"",8, 0); //"10:06:00";
            String payMode= UtilPrint.getPrintFormat(spinnerN.getSelectedItem().toString().toUpperCase(), 6, 0)  ; //"CHEQUE";

            String paidAmnt = UtilPrint.getPrintFormat(paidamnt.getText().toString(), 8, 0) ;//"10000.00";
            String grosTax =UtilPrint.getPrintFormat(twoDForm.format(db.getTotalTaxAmount(orderid,2)), 7, 0);//"5000.00";
            String grosamt =UtilPrint.getPrintFormat(totinvamnt, 8, 0) ; //"100000.00";

            String cessTotal = UtilPrint.getPrintFormat(twoDForm.format(db.getTotalCess(orderid,2)), 7, 0);//"5000.00";
            double creditAmount = Double.parseDouble(totinvamnt) - Double.parseDouble(paidamnt.getText().toString());
            String credit =UtilPrint.getPrintFormat( (creditAmount<=0) ? "0.00": twoDForm.format(creditAmount), 8, 0) ;//"10000.00";
            String salesperson = UtilPrint.getPrintFormat(pref.getString("personname", ""), 24, 0); //XXXVINEESH TV...........
            String productString ="";

            JSONArray productArray =  db.getInvoiceDetailsForPrint(orderid);


            for (int i = 0; i < productArray.length(); i++) {
                JSONObject product = productArray.getJSONObject(i);
                String unit = product.optString("uom","").length() == 0 ? " ":product.optString("uom","").charAt(0)+"";
                String item = UtilPrint.getPrintFormat((i+1)+"."+product.getString("productname"), 31, 0);
                String hsncode = UtilPrint.getPrintFormat(product.getString("HSNCode"), 9, 0);
                String qty= UtilPrint.getPrintFormat(product.getString("Qty")+"("+unit+")", 8, 2);//" 100(K) " ;//' 100(K) '

                /*Show rate,grossAmt without tax*/
                double cgstRate = Double.parseDouble(product.getString("CGSTrate"));
                double sgstRate = Double.parseDouble(product.getString("SGSTrate"));
                double otherTrate = Double.parseDouble(product.optString("otherTrate","0"));
                double otherTamnt = Double.parseDouble(product.optString("otherTamnt","0"));
                double unitRate = Double.parseDouble(product.getString("mrp"));


                double taxRate = cgstRate+sgstRate+otherTrate;

                //				double taxAmount = (unitRate *taxRate)/100 ;

                unitRate = (unitRate*100)/(100+taxRate);
                DecimalFormat twoDFormRate = new DecimalFormat("#.###");
                String rate= UtilPrint.getPrintFormat( twoDFormRate.format(unitRate), 8, 2);

                double grosAmnt = ( Double.parseDouble(product.getString("actualtotal")) * 100)/(100+taxRate);
                String total=UtilPrint.getPrintFormat(
                        twoDForm.format(grosAmnt), 8, 2);

                /*
                 String rate= UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("mrp"))), 8, 2);
                String total=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("actualtotal"))), 8, 2);*/
                String disc= UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("discount"))), 8, 2);
                String txblval=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("taxableamnt"))), 11, 2);
                String cgstper=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("CGSTrate"))), 7,2);
                String cgstamt=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("CGSTamnt"))),11,2);
                String sgstper=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("SGSTrate"))), 7, 2);
                String sgstamt=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("SGSTamnt"))),11, 2);
                String tottax=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("taxtotal"))),7,2);
                String totamnt=UtilPrint.getPrintFormat(
                        twoDForm.format(Double.parseDouble(product.getString("total"))), 11, 2);

//                double otherTamnt = Double.parseDouble(product.optString("otherTamnt","0"));
                String cessamt=UtilPrint.getPrintFormat( twoDForm.format(otherTamnt) ,7 , 2);

				/*String rate= getPrintFormat(product.getString("mrp"), 8, 2);//" 116.91 " ;
				String total=getPrintFormat(product.getString("total"), 9, 2); //" 1169.10 " ;
				String disc= getPrintFormat(product.getString("discount"), 9, 2);//" 1169.10 " ;
				String txblval=getPrintFormat(product.getString("taxableamnt"), 11, 2); //"  1169.10 " ;
				String cgstper=getPrintFormat(product.getString("CGSTrate"), 7,2); //"  02.50 " ;
				String cgstamt=getPrintFormat(product.getString("CGSTamnt"),11,2);//"  0025.00 " ;
				String sgstper=getPrintFormat(product.getString("SGSTrate"), 7, 2);//"  02.50 " ;
				String sgstamt=getPrintFormat(product.getString("SGSTamnt"),11, 2);//"  0025.00 " ;
				String tottax=getPrintFormat(product.getString("taxtotal"),7,2);//"  50.00  " ;
				String totamnt=getPrintFormat(product.getString("total"), 15, 2);//"   1333.00    " ;
				 */
				/*			String item ="3.TENDER MANGO PIC. POUCH 200gm ";//'2.CORIANDERPOW. 100gm           ' '1.CHILLY POW. 100gm             '
				String hsncode =" 20109000";//' 9109929 ' ' 9042000 '9
				String qty=" 100(K) " ;//' 100(K) '
				String rate=" 116.91 " ;
				String total=" 1169.10 " ;
				String disc=" 1169.10 " ;
				String txblval="  1169.10 " ;
				String cgstper="  02.50 " ;
				 String cgstamt="  0025.00 " ;
				 String sgstper="  02.50 " ;
				 String sgstamt="  0025.00 " ;
				 String tottax="  50.00  " ;
				 String totamnt="   1333.00    " ;*/
                productString =productString+item+hsncode+qty+rate+total+disc+txblval+cgstper+cgstamt+sgstper+sgstamt+cessamt+tottax+totamnt;
                // StringBuilder strb = new StringBuilder(productString).append(str)
            }

			/*String companyName = "   Greenmount Spices Pvt Ltd,Neriamangalam P.O,Idukki,Kerala   ";//63
			String officeAddress="   Reg. office:Nellikuzhi P.O,686691,Kothamangalam,Ernakulam   ";
			String officeContact="     Phone:0485-2827371,9495930076  GSTIN:32AACCG0501B1ZR      ";*/
            webconfigurration w = new webconfigurration(getApplicationContext() );


            String companyName = UtilPrint.getPrintFormat(w.companyNameLong, 63, 2);//63
            String officeAddress=UtilPrint.getPrintFormat(w.companyAddressLong, 68, 2);
            String officeContact=UtilPrint.getPrintFormat(w.companyContact+"  "+w.companyGSTIN, 63, 2);

            /*printmsg =
                    //print4: print2 corrected	FINAL, CASH CHANGED TO CHEQUE		  "123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789*123456789*123456789*123456789*"; //"<0x09>" +"" +

                    "BILLED TO:"+customer+"                       TAX INVOICE                                                   INV NO:"+invNo+
                            customerAddres+" "+companyName +"                        INV DATE:"+ invDate+
                            "CUST GSTIN:"+custGST+"            "+officeAddress+"                   TIME:"+ invTime+"      "+
                            "                                      "+officeContact+"                        PAYMENT MODE:"+payMode+
                            "................................................................................................................................................"+
                            "SL      ITEM			   HSNCODE  QTY(U)  RATE	 TOTAL	    DISC     TXBLVAL   CGST%   CGSTAMT   SGST%   SGSTAMT   TOT.TAX   TOT.AMOUNT "+
                            productString+
                            "................................................................................................................................................"+
                            "CREDIT BAL:"+credit+"    CESS:00.00    ROUNDOFF:00.00      PRVD BAL:0000.00    PAID AMNT:"+paidAmnt+"     TOT.TAX AMT:"+grosTax+"       BILL AMT:"+grosamt+
                            "........................................................................................SALES:"+salesperson+"..SIGN ...................";
*/
            printmsg =
                    //print4: print2 corrected	FINAL, CASH CHANGED TO CHEQUE		  "123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789*123456789*123456789*123456789*"; //"<0x09>" +"" +

                    getResources().getString(R.string.BILLED_TO)+customer+"                       "+getResources().getString(R.string.TAX_INVOICE)+"                                                   "+getResources().getString(R.string.INV_NO)+invNo+
                            customerAddres+" "+companyName +"                        "+getResources().getString(R.string.INV_DATE)+ invDate+
                            getResources().getString(R.string.CUST_GSTIN)+custGST+"            "+officeAddress+"                   "+getResources().getString(R.string.TIME)+ invTime+"      "+
                            "                                      "+officeContact+"                        "+getResources().getString(R.string.PAYMENTMODE)+payMode+
                            "................................................................................................................................................"+
                            getResources().getString(R.string.SL)+"    "+getResources().getString(R.string.ITEM)+"		         "+getResources().getString(R.string.HSNCODE)+"   "+getResources().getString(R.string.QTY_U)+"   "+getResources().getString(R.string.RATE)+"   "+getResources().getString(R.string.TOTAL)+"   "+getResources().getString(R.string.DISC)+"     "+getResources().getString(R.string.TXBLVAL)+"   "+getResources().getString(R.string.CGST_perc)+"   "+getResources().getString(R.string.CGSTAMT)+"   "+getResources().getString(R.string.SGST_perc)+"   "+getResources().getString(R.string.SGSTAMT)+"   "+getResources().getString(R.string.CESSAMT)+"   "+getResources().getString(R.string.TOT_TAX)+"   "+getResources().getString(R.string.TOT_AMOUNT) +
                            /*" "+*/productString+
                            "................................................................................................................................................"+
                            getResources().getString(R.string.CREDIT_BAL)+credit+"   "+getResources().getString(R.string.CESS)+cessTotal+"   "+getResources().getString(R.string.ROUNDOFF)+"      "+getResources().getString(R.string.PRVD_BAL)+"    "+getResources().getString(R.string.PAID_AMNT)+paidAmnt+"     "+getResources().getString(R.string.TOT_TAX_AMT)+grosTax+"       "+getResources().getString(R.string.BILL_AMT)+grosamt+
                            "........................................................................................"+getResources().getString(R.string.SALES)+salesperson+".."+getResources().getString(R.string.SIGN)+" ...................";


        } catch ( Exception e) {

            e.printStackTrace();
        }


        return printmsg;
    }

    /**
     * Compliment Invoice Print.
     * @param compliment
     * @return
     */
    private String getMessageToPrint(boolean compliment) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        String printmsg = "";

        try{
            webconfigurration c = new webconfigurration(getApplicationContext());

            String Rupeessymbol = "\u20B9";
            String custDetails ="";
            String customer = "";
            String custGST = "";
            String payMode= ""  ;
            String paidAmnt = "0";
            double totalDisc = 0;
            double actualAmount = 0 ;
            double balanceAmount = 0;
            String tempOrderId ="";
            String tempInvoiceno="";
            String custStateCode = "32";

            if (compliment) {
                tempOrderId = db.getComplimentaryOrderId(scheduledetailid, orderid);
                tempInvoiceno = db.getInvoiceNo(tempOrderId);
                customer = "COMPLIMENT";
                totalDisc = 0;
                double taxableAmnt = db.getTotalTaxableAmnt(tempOrderId,2).doubleValue();
                double taxAmnt = db.getTotalTaxAmount(tempOrderId,2).doubleValue();
                double ordAmnt = taxableAmnt+taxAmnt ;
                totinvamnt = Math.round(ordAmnt)+"";
                actualAmount =  Double.parseDouble(totinvamnt) ;
                balanceAmount = Double.parseDouble(totinvamnt) ;

            }else{
                tempOrderId = orderid;
                tempInvoiceno = db.getInvoiceNo(orderid);;
                custDetails =db.getcustdetails(scheduledetailid).toUpperCase();
                customer = custDetails.split("\n")[0];
                custGST = db.getgstin(scheduledetailid).toUpperCase();
                payMode= spinnerN.getSelectedItem().toString().toUpperCase()  ;
                paidAmnt = paidamnt.getText().toString();
                totalDisc = db.getTotalDiscountAmount( tempOrderId,2).doubleValue();
                //			 actualAmount = totalDisc+Double.parseDouble(totinvamnt) ;
                balanceAmount = Double.parseDouble(totinvamnt) - Double.parseDouble(paidAmnt);

                custStateCode = db.getStateCode(scheduledetailid);

                if (custStateCode.length() ==0){
                    custStateCode ="32";
                }

            }
			/*	String custDetails =db.getcustdetails(scheduledetailid).toUpperCase();
		String customer = custDetails.split("\n")[0];

		String custGST = db.getgstin(scheduledetailid).toUpperCase();

		String payMode= spinnerN.getSelectedItem().toString().toUpperCase()  ;

		String paidAmnt = paidamnt.getText().toString();

		double totalDisc = db.getTotalDiscountAmnt(orderid);

		double actualAmount = totalDisc+Double.parseDouble(totinvamnt) ;


		double balanceAmount = Double.parseDouble(totinvamnt) - Double.parseDouble(paidamnt.getText().toString());*/

            String salesperson = pref.getString("personname", "");

            DecimalFormat twoDForm = new DecimalFormat("#.##");
            String companyName =c.companyNameShort ;

            String line = UtilPrint.getPrintFormat("-----------------------", 24, 2);
            String msg = UtilPrint.getPrintFormat(getResources().getString(R.string.TAX_INVOICE), 24, 2)
                    +UtilPrint.getPrintFormat(companyName.toUpperCase(), 24, 0)
                    +UtilPrint.getPrintFormat(c.companyAddressShort.toUpperCase(), 24, 0)
                    +UtilPrint.getPrintFormat(c.companyGSTIN.toUpperCase(), 24, 0)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.ST_CODE)+"32", 24, 0)
                    +line
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.INV_NO)+tempInvoiceno, 24,0)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.DT)+invoicedate.getText()+" "+invoicetime, 24, 0)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.BILL_TYPE)+payMode, 24, 0)
                    +line;

            String custDetailsString = UtilPrint.getPrintFormat(getResources().getString(R.string.TO)+customer, 48, 0)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.GSTIN)+custGST, 24, 0)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.ST_CODE)+custStateCode, 24,0)
                    //	+UtilPrint.getPrintFormat("ST CODE:32", 24, 0)
                    +line;

            String productHeads =UtilPrint.getPrintFormat(getResources().getString(R.string.HSN), 12, 0)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.ITEM), 12, 1)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.qty), 8, 0)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.RATE), 8, 0)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.amount), 8, 1)
                    +line;



            JSONArray productArray =  db.getInvoiceDetailsForPrint(tempOrderId);
            //		DecimalFormat twoDForm = new DecimalFormat("#.##");
            String productString ="";

            for (int i = 0; i < productArray.length(); i++) {

                JSONObject product = productArray.getJSONObject(i);
                String unit = product.optString("uom","").length() == 0 ? " ":product.optString("uom","").charAt(0)+"";
                String item =  product.getString("productname") ;
                item = getShortenItemName(item, 14);

                String hsncode =product.getString("HSNCode");
                String qty= product.getString("Qty")+unit;

                String rate= "0";
                String actualtotal ="0";

               // if (c.rateWithOutTax){
                    double cgstRate = Double.parseDouble(product.getString("CGSTrate"));
                    double sgstRate = Double.parseDouble(product.getString("SGSTrate"));
                    double unitRate = Double.parseDouble(product.getString("mrp"));

                    double otherTrate = Double.parseDouble(product.optString("otherTrate","0"));
                    double otherTamnt = Double.parseDouble(product.optString("otherTamnt","0"));

                    double taxRate = cgstRate+sgstRate+otherTrate;

                    //				double taxAmount = (unitRate *taxRate)/100 ;

                    unitRate = (unitRate*100)/(100+taxRate);
                    DecimalFormat twoDFormRate = new DecimalFormat("#.###");
                    rate= UtilPrint.getPrintFormat( twoDFormRate.format(unitRate), 8, 2);

                    double grosAmnt = ( Double.parseDouble(product.getString("actualtotal")) * 100)/(100+taxRate);

                    actualtotal = twoDForm.format(grosAmnt);

                    actualAmount =actualAmount + grosAmnt;

                /*}else{
                    rate=  twoDForm.format(Double.parseDouble(product.getString("mrp")));
                    actualtotal =twoDForm.format(Double.parseDouble(product.getString("actualtotal")));

                    actualAmount =actualAmount+ Double.parseDouble(product.getString("actualtotal"));
                }*/




                productString =productString+UtilPrint.getPrintFormat(hsncode, 10, 0)
                        +UtilPrint.getPrintFormat(item, 14, 0)
                        +UtilPrint.getPrintFormat(qty, 6, 0)
                        +UtilPrint.getPrintFormat(rate, 8, 2)
                        +UtilPrint.getPrintFormat(actualtotal, 10, 1);



            }




            String subtotal =line+ UtilPrint.getPrintFormat(getResources().getString(R.string.SUB_TOTAL), 12, 0)
                    +UtilPrint.getPrintFormat(twoDForm.format(actualAmount), 12, 1)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.DISCOUNT), 12, 0)
                    +UtilPrint.getPrintFormat(totalDisc+"", 12, 1)
                    +line;

            String totaltax =  UtilPrint.getPrintFormat(getResources().getString(R.string.TXBLE_AMNT), 12, 0)
                    +UtilPrint.getPrintFormat(twoDForm.format(db.getTotalTaxableAmnt(tempOrderId,2))+"", 12, 1)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.CGST), 12, 1)
                    +UtilPrint.getPrintFormat(twoDForm.format(db.getTotalCgst(tempOrderId,2))+"", 12, 1)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.SGST), 12, 1)
                    +UtilPrint.getPrintFormat(twoDForm.format(db.getTotalSgst(tempOrderId,2))+"", 12, 1)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.CESS), 12, 1)
                    +UtilPrint.getPrintFormat(twoDForm.format(db.getTotalCess(tempOrderId,2))+"", 12, 1)

                    +line;

            String total = line+UtilPrint.getPrintFormat(getResources().getString(R.string.TOTAL)+" "+Rupeessymbol+":", 12, 0)
                    +UtilPrint.getPrintFormat(totinvamnt, 12, 1)
					/*+UtilPrint.getPrintFormat("RoundOff:", 12, 1)
				+UtilPrint.getPrintFormat("0.01", 12, 1)*/
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.RECEIVED), 12, 0)
                    +UtilPrint.getPrintFormat(paidAmnt, 12, 1)+line
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.balance), 12, 0)
                    +UtilPrint.getPrintFormat(balanceAmount+"", 12, 1)
                    +line;

            String salesman = UtilPrint.getPrintFormat(getResources().getString(R.string.SALESMAN), 12, 0)
                    +UtilPrint.getPrintFormat(salesperson, 12, 1);


            printmsg =  msg+custDetailsString+productHeads+productString+subtotal+totaltax+total+salesman+line;

        } catch ( Exception e) {

            Log.e("Error",e.getMessage()) ;
            e.printStackTrace();
        }

        return printmsg ;
    }

    private String getShortenItemName(String itemName,int possibleLength){
        StringBuilder shortenName = new StringBuilder();

        if (itemName.length()<=possibleLength) {
            return itemName;
        }else {

            String[] itemNameSplited = itemName.split(" ");
            if (itemNameSplited.length > 3) {

                shortenName.append(itemNameSplited[0].length() < 3 ? itemNameSplited[0] : itemNameSplited[0].substring(0, 3));
                shortenName.append(".");
                for (int i = 1; i < itemNameSplited.length-1; i++) {

                    //itemNameSplited[i] = itemNameSplited[i].substring(itemNameSplited.length-2) + ".";
                    shortenName.append(itemNameSplited[i].length() < 1 ? itemNameSplited[i] : itemNameSplited[i].substring(0, 1));
                    shortenName.append(".");
                }
                shortenName.append(itemNameSplited[itemNameSplited.length-1]);
                return shortenName.toString() ;

            }else if (itemNameSplited.length == 3) {

                shortenName.append(itemNameSplited[0].length() < 3 ? itemNameSplited[0] : itemNameSplited[0].substring(0,3));
                shortenName.append(".");
                shortenName.append(itemNameSplited[1].length() < 2 ?
                        itemNameSplited[1] : itemNameSplited[1].substring(0, 2));
                shortenName.append(".");
                shortenName.append(itemNameSplited[itemNameSplited.length-1]);
                return shortenName.toString() ;
            }else if (itemNameSplited.length == 2) {

                shortenName.append(itemNameSplited[0].length() < 4 ? itemNameSplited[0] : itemNameSplited[0].substring(0, 4));
                shortenName.append(".");

                shortenName.append(itemNameSplited[itemNameSplited.length-1]);
                return shortenName.toString() ;
            }else if (itemNameSplited.length == 1) {

                shortenName.append(itemNameSplited[0].length() < possibleLength-2 ? itemNameSplited[0] : itemNameSplited[0].substring(0, possibleLength-2));
                shortenName.append(".");

                //				shortenName.append(itemNameSplited[itemNameSplited.length-1]);
                return shortenName.toString() ;
            }


        }

        return shortenName.toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_DATE_SETTING) {
            finish();
            startActivity(getIntent());
            overridePendingTransition(0, 0);
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
     /*   Intent i=new Intent(OrderPaymentActivity.this,ItemListActivity.class);
        i.putExtra("scheduledetailid", scheduledetailid);
        finish();
        overridePendingTransition(0,0);
        startActivity(i);*/

        finish();
        overridePendingTransition(0,0);
        Intent i=new Intent(OrderPaymentActivity.this, CustomerListActivity.class);
        startActivity(i);


    }
}
