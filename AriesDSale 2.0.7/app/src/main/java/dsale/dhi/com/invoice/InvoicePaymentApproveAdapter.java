package dsale.dhi.com.invoice;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.PlayLocation;

import static android.content.Context.MODE_PRIVATE;

public class InvoicePaymentApproveAdapter extends RecyclerView.Adapter<InvoicePaymentApproveAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    Context mcontext;
    private List<String> mData;
    private InvoiceMakePaymentAdapter.ItemClickListener mClickListener;
    JSONArray pay;
    private String sid;
    int childPosition =0;
    PopupWindow popupWindow;
    PopupWindow popupWindow2;
    PopupWindow popupWindow1;
    Spinner spinner;
    String[] banknames;
    String[] bankcodes;
    Calendar myCalendar = Calendar.getInstance();
    EditText dat;
    LinearLayout linearLayout1;
    private static String checknumbervalue = "";
    private static String banknamevalue = "";
    private static String checkdatevalue = "";
    private static String banknamecode = "";
    private static String paymentmode = "";
    private static String pymentheaderid = "0";
    String cancelled = "Cancelled";
    int len=0;
    String[] amount;
    String[] balance;
    String[] invoicenumber;
    String[] customerid;
    String[] invoicedate;
    String[] status;
    String[] invoiceid;
    String[] Amountpatyedbefore;
    String[] actualinvoicebalance;
    Double subtotal = 0.0;
    Double paidamount = 0.0;
    Double amountdue = 0.0;
    Double chtotal =0.0;
    Double chbalance=0.0;
    Double chpend=0.0;

    // data is passed into the constructor
    public InvoicePaymentApproveAdapter(Context context, String sid,JSONArray pay) {
        this.mInflater = LayoutInflater.from(context);
        Log log;
        this.mcontext = context;
        this.pay = pay;
        this.sid=sid;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.invoice_payment_approve_row, parent, false); // make_payment_row payment_approve_row
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        String animal = mData.get(position);
//        String status = mData1.get(position);
        //lin = itemView.findViewById(R.id.cashbtn);
        final JSONObject temp = pay.optJSONObject(position);
//        JSONObject tep = getChild(position, childPosition);
        final HashMap<Integer, String> selItems = new HashMap<Integer, String>();
        final JSONObject tep = pay.optJSONObject(position).optJSONArray("invoicedata").optJSONObject(childPosition);
        JSONArray inputdata = new JSONArray();
        try {
            inputdata = temp.getJSONArray("invoicedata");
        } catch (JSONException e) {
            e.printStackTrace();
        }

            amount = new String[inputdata.length()];
        balance = new String[inputdata.length()];
        invoicenumber = new String[inputdata.length()];
        customerid = new String[inputdata.length()];
        invoicedate = new String[inputdata.length()];
        status = new String[inputdata.length()];
        invoiceid = new String[inputdata.length()];
        Amountpatyedbefore = new String[inputdata.length()];
        actualinvoicebalance = new String[inputdata.length()];
        for (int i = 0; i < inputdata.length(); i++) {
            amount[i] = inputdata.optJSONObject(i).optString("invoicetottal");

            invoicenumber[i] = inputdata.optJSONObject(i)
                    .optString("invoiceno");
            customerid[i] = inputdata.optJSONObject(i).optString("customerid");
            invoicedate[i] = inputdata.optJSONObject(i).optString("date");
            status[i] = inputdata.optJSONObject(i).optString("status");
            invoiceid[i] = inputdata.optJSONObject(i).optString("invoiceid");
            Amountpatyedbefore[i] = inputdata.optJSONObject(i).optString(
                    "amountpayed");

            balance[i] = inputdata.optJSONObject(i).optString("balance");
            actualinvoicebalance[i] = inputdata.optJSONObject(i).optString(
                    "invoicebalance");
            try {
                Double amt = Double.parseDouble(amount[i]);
                Double bal = Double.parseDouble(actualinvoicebalance[i]);
                Double amtpayed = Double.parseDouble(Amountpatyedbefore[i]);
                if (paymentmode.contentEquals("Credit"))
                    balance[i] = String.valueOf(bal);
                else
                    balance[i] = String.valueOf(bal + amtpayed);

                if (!paymentmode.contentEquals("Credit")) {
                    Double paid = amt - bal;
                    subtotal = subtotal + amt;
                    paidamount = paidamount + paid;
                    amountdue = amountdue + bal;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tep != null) {
            String no = tep.optString("invoiceno", "");
            holder.myTextView.setText(no);
            holder.paidamount.setText(tep.optString("amountpayed", ""));
            holder.totalamt.setText(tep.optString("invoicetottal", ""));

            String method = (String) temp.optString("payentmethode", "");
            String date = (String) temp.optString("paymentdate", "");
            if (method.contentEquals("Cash"))
                holder.cashbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.single_dot_cash, 0, 0, 0);
            else if (method.contentEquals("CDC") || method.equals("PDC"))
                holder.cashbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.single_dot_partial, 0, 0, 0);
            holder.cashbtn.setText(method);
            holder.date.setText(date);
            if (method.contentEquals("CDC") || method.equals("PDC")) {
                String Chequeno = (String) temp.optString("chequenumber", "");
                String bankname = (String) temp.optString("ifsc", "");
                String chequedate = (String) temp.optString("chequedate", "");
                holder.chquedt.setText(chequedate);
                holder.bankname.setText(bankname);
                holder.chequeno.setText(Chequeno);
            } else {
                holder.cdclayout.setVisibility(View.GONE);
            }
            final String aproovesstatus = temp.optString("aprovelag", "0");
            if (aproovesstatus.equals("1")) {
                holder.editpayment.setVisibility(View.GONE);
                holder.deletepayment.setVisibility(View.GONE);
            } else {
                holder.editpayment.setVisibility(View.VISIBLE);
                holder.deletepayment.setVisibility(View.VISIBLE);
            }
            final int grouppos_selected = position;
            holder.editpayment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final View customView = mInflater.inflate(R.layout.edit_payment, null);
                    Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
                    popupWindow2 = new PopupWindow(customView, width, height);
                    Button yes = (Button) customView.findViewById(R.id.yes);
                    TextView header = (TextView) customView.findViewById(R.id.header);
                    header.setText(mcontext.getResources().getString(R.string.edit_payments));
                    TextView invno = (TextView) customView.findViewById(R.id.invoiceno);
                    invno.setText(tep.optString("invoiceno", ""));
                    final TextView curbal = (TextView) customView.findViewById(R.id.curbalance);
                    curbal.setText(tep.optString("invoicebalance", ""));
                    TextView date = (TextView) customView.findViewById(R.id.date);
                    date.setText(temp.optString("paymentdate", ""));
                    final EditText amtt = (EditText) customView.findViewById(R.id.amount);
                    amtt.setText(tep.optString("amountpayed", ""));
                    Double amt = Double.parseDouble(tep.optString("invoicetottal", ""));
                    Double payed = Double.parseDouble(tep.optString("amountpayed", ""));
                    final Double beforebalance = (amt - payed);
                    TextView beforebal = (TextView) customView.findViewById(R.id.balance);
                    beforebal.setText(beforebalance.toString());
                    final int id = position;
                    if (!selItems.containsKey(id))
                        selItems.put(id, "");
                    amtt.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence s, int start,
                                                  int before, int count) {
                            selItems.put(id, s.toString());
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start,
                                                      int count, int after) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                            chtotal = Double.parseDouble(tep.optString("invoicetottal", ""));
                            chbalance = Double.parseDouble(tep.optString("invoicebalance", ""));
                            Double crntblnc= Double.parseDouble(tep.optString("amountpayed", ""));
                            chbalance=chbalance+crntblnc;
                            Double pendbal=0.0;
                            pendbal=chtotal-chbalance;
                            Double act=0.0;
                            String paidamtstr=amtt.getText().toString();
                            if(paidamtstr.isEmpty()){
                                chpend=0.0;
                                act=chbalance-chpend;
                            }
                            else{
                                chpend=Double.parseDouble(paidamtstr);
                                act=chbalance-chpend;
                            }
                            DecimalFormat twoDForm = new DecimalFormat("#.##");
                            act = Double.valueOf(twoDForm
                                    .format(act));
                            String pendingamtstr=Double.toString(act);
                            curbal.setText(pendingamtstr);

                        }
                    });
                    TextView invamt = (TextView) customView.findViewById(R.id.invoiceamt);
                    invamt.setText(tep.optString("invoicetottal", "")+" INR");
                    final Button paymentmode = (Button) customView.findViewById(R.id.type);
                    final String method = (String) temp.optString("payentmethode", "");
                    if (method.contentEquals("Cash"))
                        paymentmode.setCompoundDrawablesWithIntrinsicBounds(R.drawable.single_dot_cash, 0, 0, 0);
                    else if (method.contentEquals("CDC") || method.equals("PDC"))
                        paymentmode.setCompoundDrawablesWithIntrinsicBounds(R.drawable.single_dot_partial, 0, 0, 0);
                    paymentmode.setText(method);
                    spinner = (Spinner) customView.findViewById(R.id.spinner2);
                    SpinnerAdapter adapter1 = initializeExpenseTypes();
                    //ArrayAdapter adapter1 = initializeExpenseTypes();//ArrayAdapter.createFromResource(mcontext, R.array.payment_mode, R.layout.spinner_item_payment);
                    spinner.setAdapter(adapter1);
                    //================================check cdc or pdc=================
                    DatabaseHandler db = new DatabaseHandler(mcontext);
                    JSONArray bankdata;
                    try {
                        bankdata = db.getbankdata();

                        bankcodes = new String[bankdata.length() + 1];
                        banknames = new String[bankdata.length() + 1];

                        bankcodes[0] = "0";
                        banknames[0] = mcontext.getResources().getString(R.string.selectbank);
                        for (int i = 0; i < bankdata.length(); i++) {
                            JSONObject dataobj = bankdata.getJSONObject(i);
                            bankcodes[i + 1] = dataobj.getString("bankcode");
                            banknames[i + 1] = dataobj.getString("bankcode") + "- "
                                    + dataobj.getString("bankname");

                        }

                    } catch (JSONException e2) {
                        // TODO Auto-generated catch block
                        e2.printStackTrace();
                    }
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                            String text = spinner.getSelectedItem().toString();
                            cancelled = "Cancelled";
                            if (text.contentEquals("Cash"))
                                paymentmode.setCompoundDrawablesWithIntrinsicBounds(R.drawable.single_dot_cash, 0, 0, 0);
                            else if (text.contentEquals("CDC") || text.equals("PDC"))
                                paymentmode.setCompoundDrawablesWithIntrinsicBounds(R.drawable.single_dot_partial, 0, 0, 0);
                            paymentmode.setText(text);
                            if (text.equals("CDC")) {
                                LayoutInflater layoutInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View customView2 = layoutInflater.inflate(R.layout.cdc_popup, null);
                                Button closePopupBtn = (Button) customView2.findViewById(R.id.close);
                                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                                popupWindow = new PopupWindow(customView2, width, height);
                                Button yes = (Button) customView2.findViewById(R.id.confirm);
                                TextView cdc = (TextView) customView2.findViewById(R.id.cdcorpdc);
                                ImageButton datp = (ImageButton) customView2.findViewById(R.id.calendar);
                                dat = (EditText) customView2.findViewById(R.id.checquedate);
                                dat.setEnabled(false);
                                cdc.setText(mcontext.getResources().getString(R.string.chequecdc));
                                EditText chno = (EditText) customView2.findViewById(R.id.checkno);
                                String Chequeno = (String) temp.optString("chequenumber", "");
                                String bankname = (String) temp.optString("ifsc", "");
                                String chequedate = (String) temp.optString("chequedate", "");
                                chno.setText(Chequeno);
                                Spinner spinnerbankmaster = (Spinner) customView2.findViewById(R.id.banknamespinner);
                                SpinnerAdapter spinAdapter_bank = new ArrayAdapter<Object>(
                                        mcontext, android.R.layout.simple_list_item_1,
                                        banknames);
                                spinnerbankmaster.setAdapter(spinAdapter_bank);
                                int selctedpos = -1;
                                for (int i = 0; i < banknames.length; i++) {
                                    if (banknames[i].trim().equalsIgnoreCase(
                                            bankname.trim())) {
                                        selctedpos = i;
                                        break;
                                    }
                                }
                                spinnerbankmaster.setSelection(selctedpos);
                                final ImageButton datepick = (ImageButton) customView2.findViewById(R.id.calendar);
                                final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {
                                        // TODO Auto-generated method stub
                                        myCalendar.set(Calendar.YEAR, year);
                                        myCalendar.set(Calendar.MONTH, monthOfYear);
                                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                        updateLabel();
                                    }

                                    private void updateLabel() {
                                        String myFormat = "dd/MM/yyyy"; // In which you need put
                                        // here
                                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat,
                                                Locale.US);
                                        dat.setText(sdf.format(myCalendar.getTime()));
                                    }
                                };
                                datepick.setOnClickListener(new View.OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        new DatePickerDialog(mcontext, date, myCalendar
                                                .get(Calendar.YEAR), myCalendar
                                                .get(Calendar.MONTH), myCalendar
                                                .get(Calendar.DAY_OF_MONTH)).show();
                                    }
                                });

                                Calendar cal = Calendar.getInstance();
                                Date dateafter_2_days = cal.getTime();
                                String Cheque_date_choosen = "";
                                DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy");
                                Cheque_date_choosen = dateTimeFormat.format(dateafter_2_days);
                                dat.setText(Cheque_date_choosen);
                                dat.setEnabled(false);
                                datepick.setEnabled(false);

                                //instantiate popup window

                                popupWindow.setFocusable(true);

                                //display the popup window
                                popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);
                                popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                                //close the popup window on button click
                                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                                mcontext);
                                        builder.setTitle(mcontext.getResources().getString(R.string.change_paymode));
                                        builder.setMessage(mcontext.getResources().getString(R.string.change_paymode_msg));
                                        builder.setPositiveButton(mcontext.getString(R.string.ok),
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int which) {

                                                        spinner.setSelection(0);
                                                        popupWindow.dismiss();
                                                    }
                                                });

                                        builder.setNeutralButton(mcontext.getResources().getString(R.string.CANCEL),
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int which) {

                                                    }
                                                });

                                        builder.show();
                                        //spinner.setSelection(0);
//                                        popupWindow.dismiss();
                                    }
                                });
                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        EditText checknumber = (EditText) customView2.findViewById(R.id.checkno);
                                        String chknbr = checknumber.getText().toString();
                                        Spinner bankname = (Spinner) popupWindow
                                                .getContentView().findViewById(
                                                        R.id.banknamespinner);
                                        String ifsccdval = bankname.getSelectedItem()
                                                .toString();

                                        String selecteditemcode = bankcodes[bankname
                                                .getSelectedItemPosition()];
                                        EditText chequedate = (EditText) customView2.findViewById(R.id.checquedate);
                                        String chequedatevalue = chequedate.getText().toString();
                                        if (chknbr.equals("")) {
                                            Toast.makeText(mcontext, mcontext.getResources().getString(R.string.toastchequenoneeded),
                                                    Toast.LENGTH_SHORT).show();

                                            return;
                                        }

                                        if (ifsccdval.equalsIgnoreCase(mcontext.getResources().getString(R.string.selectbank))) {
                                            Toast.makeText(mcontext, mcontext.getResources().getString(R.string.toastbanknameneeded),
                                                    Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        if (chequedatevalue.equals("")) {
                                            Toast.makeText(mcontext, mcontext.getResources().getString(R.string.toastdatenotvalid),
                                                    Toast.LENGTH_SHORT).show();
                                            return;

                                        }
                                        Calendar cal = Calendar.getInstance();
                                        Date dateafter_2_days = cal.getTime();

                                        DateFormat dateTimeFormat = new SimpleDateFormat(
                                                "dd/MM/yyyy");
                                        Date Cheque_date_choosen = new Date();
                                        try {
                                            Cheque_date_choosen = dateTimeFormat
                                                    .parse(chequedatevalue);
                                        } catch (ParseException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }

                                        if (Cheque_date_choosen.compareTo(dateafter_2_days) > 0) {
                                            Toast.makeText(mcontext, mcontext.getResources().getString(R.string.Invalid_Date), Toast.LENGTH_SHORT).show();
                                            return;
                                        }

                                        checknumbervalue = chknbr;
                                        banknamevalue = ifsccdval;
                                        banknamecode = selecteditemcode;
                                        checkdatevalue = chequedatevalue;
                                        cancelled = "OK";
                                        popupWindow.dismiss();
                                    }
                                });
                                popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                                    @Override
                                    public void onDismiss() {
                                        if (!(cancelled.contentEquals("OK"))) {
                                           // spinner.setSelection(0);
                                            popupWindow.dismiss();
                                        }

                                    }
                                });
                            } else if (text.equals("PDC")) {
                                LayoutInflater layoutInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View customView3 = layoutInflater.inflate(R.layout.cdc_popup, null);
                                Button closePopupBtn = (Button) customView3.findViewById(R.id.close);
                                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                                popupWindow = new PopupWindow(customView3, width, height);
                                popupWindow.setFocusable(true);
                                Button yes = (Button) customView3.findViewById(R.id.confirm);
                                TextView cdc = (TextView) customView3.findViewById(R.id.cdcorpdc);
                                cdc.setText("Cheque (PDC)");
                                EditText chno = (EditText) customView3.findViewById(R.id.checkno);
                                String Chequeno = (String) temp.optString("chequenumber", "");
                                String bankname = (String) temp.optString("ifsc", "");
                                String chequedate = (String) temp.optString("chequedate", "");
                                chno.setText(Chequeno);
                                ImageButton datp = (ImageButton) customView3.findViewById(R.id.calendar);
                                dat = (EditText) customView3.findViewById(R.id.checquedate);
                                dat.setEnabled(false);
                                dat.setText(chequedate);
                                Spinner spinnerbankmaster = (Spinner) customView3.findViewById(R.id.banknamespinner);
                                SpinnerAdapter spinAdapter_bank = new ArrayAdapter<Object>(
                                        mcontext, android.R.layout.simple_list_item_1,
                                        banknames);
                                spinnerbankmaster.setAdapter(spinAdapter_bank);
                                int selctedpos = -1;
                                for (int i = 0; i < banknames.length; i++) {
                                    if (banknames[i].trim().equalsIgnoreCase(
                                            bankname.trim())) {
                                        selctedpos = i;
                                        break;
                                    }
                                }
                                spinnerbankmaster.setSelection(selctedpos);
                                datp.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Calendar calendar = Calendar.getInstance();
                                        int year = calendar.get(Calendar.YEAR);
                                        int month = calendar.get(Calendar.MONTH);
                                        final int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                                        DatePickerDialog datePickerDialog = new DatePickerDialog(mcontext,
                                                new DatePickerDialog.OnDateSetListener() {
                                                    @Override
                                                    public void onDateSet(DatePicker datePicker, final int year, final int month, final int day) {
                                                        Date d = new Date();
                                                        myCalendar.set(Calendar.YEAR, year);
                                                        myCalendar.set(Calendar.MONTH, month);
                                                        myCalendar.set(Calendar.DAY_OF_MONTH, day);
                                                        Date enterdate = myCalendar.getTime();
                                                        if (enterdate.compareTo(d) > 0) {
                                                            AlertDialog builder = new AlertDialog.Builder(mcontext)
                                                                    .setTitle(mcontext.getResources().getString(R.string.alert))
                                                                    .setMessage(
                                                                            mcontext.getResources().getString(R.string.alertfuturedate))
                                                                    .setPositiveButton(
                                                                            android.R.string.yes,
                                                                            new DialogInterface.OnClickListener() {
                                                                                public void onClick(
                                                                                        DialogInterface dialog,
                                                                                        int which) {
                                                                                    updateLabel(day, month, year);
                                                                                }
                                                                            })
                                                                    .setNegativeButton(
                                                                            android.R.string.no,
                                                                            new DialogInterface.OnClickListener() {
                                                                                public void onClick(
                                                                                        DialogInterface dialog,
                                                                                        int which) {
                                                                                }
                                                                            })
                                                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                                                    .show();

                                                        }
                                                        else {
                                                            updateLabel(day, month, year);
                                                        }
                                                    }
                                                }, year, month, dayOfMonth);
                                        datePickerDialog.show();

                                    }

                                    private void updateLabel(int day, int month, int year) {
                                        String myFormat = "dd/MM/yyyy"; // In which you need put
                                        // here
                                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat,
                                                Locale.US);
                                        dat.setText(sdf.format(myCalendar.getTime()));

//                            dat.setText(new StringBuilder().append(day).append("/")
//                                    .append(month).append("/").append(year));
                                    }
                                });


                                //display the popup window
                                popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);
                                popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                                //close the popup window on button click
                                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                                mcontext);
                                        builder.setTitle(mcontext.getResources().getString(R.string.change_paymode));
                                        builder.setMessage(mcontext.getResources().getString(R.string.change_paymode_msg));
                                        builder.setPositiveButton(mcontext.getResources().getString(R.string.ok),
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int which) {

                                                        spinner.setSelection(0);
                                                        popupWindow.dismiss();
                                                    }
                                                });

                                        builder.setNeutralButton(mcontext.getResources().getString(R.string.CANCEL),
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int which) {

                                                    }
                                                });

                                        builder.show();
//                                        popupWindow.dismiss();
                                    }
                                });

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        EditText checknumber = (EditText) customView3.findViewById(R.id.checkno);
                                        String chknbr = checknumber.getText().toString();
                                        Spinner bankname = (Spinner) popupWindow
                                                .getContentView().findViewById(
                                                        R.id.banknamespinner);
                                        String ifsccdval = bankname.getSelectedItem()
                                                .toString();

                                        String selecteditemcode = bankcodes[bankname
                                                .getSelectedItemPosition()];
                                        EditText chequedate = (EditText) customView3.findViewById(R.id.checquedate);
                                        String chequedatevalue = chequedate.getText().toString();
                                        if (chknbr.equals("")) {
                                            Toast.makeText(mcontext, mcontext.getResources().getString(R.string.toastchequenoneeded),
                                                    Toast.LENGTH_SHORT).show();

                                            return;
                                        }

                                        if (ifsccdval.equalsIgnoreCase(mcontext.getResources().getString(R.string.selectbank))) {
                                            Toast.makeText(mcontext, mcontext.getResources().getString(R.string.toastbanknameneeded),
                                                    Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        if (chequedatevalue.equals("")) {
                                            Toast.makeText(mcontext, mcontext.getResources().getString(R.string.Date_not_valid),
                                                    Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        Calendar cal = Calendar.getInstance();

                                        Date currdate = cal.getTime();

                                        DateFormat dateTimeFormat = new SimpleDateFormat(
                                                "dd/MM/yyyy");
                                        Date Cheque_date_choosen = new Date();
                                        try {
                                            Cheque_date_choosen = dateTimeFormat
                                                    .parse(chequedatevalue);
                                        } catch (ParseException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }

                                        if (Cheque_date_choosen.compareTo(currdate) <= 0) {
                                            Toast.makeText(mcontext, mcontext.getResources().getString(R.string.Invalid_Date),
                                                    Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        checknumbervalue = chknbr;
                                        banknamevalue = ifsccdval;
                                        banknamecode = selecteditemcode;
                                        checkdatevalue = chequedatevalue;
                                        cancelled = "OK";
                                        popupWindow.dismiss();
                                    }
                                });

                                popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                                    @Override
                                    public void onDismiss() {
                                        if (!(cancelled.contentEquals("OK"))) {
//                                            spinner.setSelection(0);
                                            popupWindow.dismiss();
                                        }

                                    }
                                });
                            } else {
                                checknumbervalue = "";
                                banknamevalue = "";
                                checkdatevalue = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });

                    if (method.contentEquals("Cash"))
                    {
                        spinner.setSelection(0);
                    }
                    else if(method.contentEquals("CDC"))
                    {
                        spinner.setSelection(1);
                    }
                    else if(method.contentEquals("PDC"))
                    {
                        spinner.setSelection(2);
                    }
                    popupWindow2.setFocusable(true);

                    //display the popup window
                    popupWindow2.showAtLocation(holder.popuplayout, Gravity.CENTER, 0, 0);
                    popupWindow2.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow2.dismiss();
                        }
                    });
                    final LinearLayout llayout = (LinearLayout) customView.findViewById(R.id.layconfirm);
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LayoutInflater layoutInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View newcustomView = layoutInflater.inflate(R.layout.popuplayout_makepayment, null);
                            TextView header = (TextView) newcustomView.findViewById(R.id.header);
                            header.setText(mcontext.getResources().getString(R.string.submitpayment));
                            TextView msg = (TextView) newcustomView.findViewById(R.id.message);
                            Button yes = (Button) newcustomView.findViewById(R.id.yes);
                            Button cancel = (Button) newcustomView.findViewById(R.id.close);
                            double total_payingamount = 0.0;
                            for (int i = 0; i < invoicenumber.length; i++) {
                                String singlebil_amount = selItems.get(position);
                                String editamt=amtt.getText().toString();
                                if (singlebil_amount.equals("")) {
                                    if (editamt.equals("")) {
                                        continue;
                                    }
                                    else
                                    {
                                        singlebil_amount=editamt;
                                    }
                                }

                                try {
                                    total_payingamount += Double
                                            .parseDouble(singlebil_amount);
                                } catch (Exception newx) {
                                }

                            }

                            Spinner spiner_temp = (Spinner) customView.findViewById(R.id.spinner2);
                            String paymentmode = spiner_temp.getSelectedItem()
                                    .toString();

                            String chequenumber_temp = checknumbervalue;
                            String cheuedate_temp = checkdatevalue;
                            String bankname_temp = banknamevalue;
                            String confirmation_string = "";
                            if (paymentmode.equals("CDC") || paymentmode.equals("PDC")) {
                                confirmation_string = mcontext.getResources().getString(R.string.totamt)
                                        + total_payingamount + "\n" + mcontext.getResources().getString(R.string.PAYMENTMODE)
                                        + paymentmode + "\n" + mcontext.getResources().getString(R.string.chno)
                                        + chequenumber_temp + "\n" + mcontext.getResources().getString(R.string.date)
                                        + cheuedate_temp + "\n" + mcontext.getResources().getString(R.string.Bank)
                                        + bankname_temp;

                            } else {
                                confirmation_string = mcontext.getResources().getString(R.string.totamt)
                                        + total_payingamount + "\n" + mcontext.getResources().getString(R.string.PAYMENTMODE)
                                        + paymentmode;
                            }

                            final String confirmation_final = confirmation_string;
                            msg.setText(confirmation_final);
                            cancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    popupWindow1.dismiss();
                                }
                            });
                            yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Double tottal_amount_paid = 0.0;
                                    JSONArray invoicedata = new JSONArray();
                                    for (int i = 0; i < invoicenumber.length; i++) {
                                        String invamount = selItems.get(position);
                                        String editamt=amtt.getText().toString();
                                        if (invamount.equals("")) {
                                            if(editamt.equals("")) {
                                                continue;
                                            }
                                            else
                                            {
                                                invamount=editamt;
                                            }
                                        }

                                        JSONObject obj = new JSONObject();
                                        String amount = invamount;

                                        String id = invoiceid[i];
                                        Double amt = Double.parseDouble(tep.optString("invoicetottal", ""));
                                        Double payed = Double.parseDouble(tep.optString("amountpayed", ""));
                                        Double beforebalance = (amt - payed);
                                        String tottalinvoiceamount = balance[i];
                                        try {
                                            obj.put("amout", amount);
                                            try {
                                                tottal_amount_paid += Double
                                                        .parseDouble(amount);
                                            } catch (Exception newx) {
                                            }
                                            obj.put("invoiceid", id);
                                            obj.put("invoicenumber", invoicenumber[i]);
                                            obj.put("Tottalinvoiceamount",
                                                    tottalinvoiceamount);
                                            Double currentamount_temp = Double
                                                    .parseDouble(amount);
                                            Double tottalamount_temp = Double
                                                    .parseDouble(tottalinvoiceamount);
                                            Double balance;

                                            // new edit need changes
                                            Spinner mySpinner = (Spinner) customView.findViewById(R.id.spinner2);
                                            String paymentmode = mySpinner
                                                    .getSelectedItem()
                                                    .toString();

                                            if (paymentmode
                                                    .contentEquals("Credit"))
                                                balance = round(
                                                        (tottalamount_temp), 2);
                                            else
                                                balance = round(
                                                        (tottalamount_temp - currentamount_temp),
                                                        2);

                                            double currenbalnce = Double
                                                    .parseDouble(actualinvoicebalance[i]);
                                            double amountpayedbefore = Double
                                                    .parseDouble(Amountpatyedbefore[i]);
                                            obj.put("actualbalnce",
                                                    String.valueOf(currenbalnce
                                                            + amountpayedbefore));
                                            String status = "";
                                            if (balance <= 0) {
                                                status = "paid";
                                                obj.put("balance",
                                                        String.valueOf(balance));

                                            } else {
                                                status = "not paid";
                                                obj.put("balance",
                                                        String.valueOf(balance));
                                            }
                                            obj.put("status", status);
                                            invoicedata.put(obj);

                                        } catch (JSONException e) {
                                            e.printStackTrace();

                                        }

                                    }
                                    DatabaseHandler db = new DatabaseHandler(mcontext);
                                    String paymentid = UUID.randomUUID().toString();
                                    Calendar c1 = Calendar.getInstance();
                                    DateFormat dateTimeFormat2 = new SimpleDateFormat(
                                            "yyyyMMddHHmmss");
                                    DateFormat dateTimeFormat_payment = new SimpleDateFormat(
                                            "dd/MM/yyyy HH:mm:ss");

                                    String createtime = dateTimeFormat2.format(c1.getTime());
                                    String paymentdate = dateTimeFormat_payment.format(c1.getTime());

//				    Vehicletracker tracker = new Vehicletracker();
                                    PlayLocation tracker = new PlayLocation();
                                    Double lat = tracker.getlattitude();
                                    Double lng = tracker.getlongitude();

                                    Spinner mySpinner = (Spinner) customView.findViewById(R.id.spinner2);
                                    String paymentmode = mySpinner.getSelectedItem().toString();
                                    if (paymentmode.equals("CDC")
                                            || paymentmode.equals("PDC"))
                                        paymentmode = "Cheque";

                                    String actualmode = mySpinner
                                            .getSelectedItem().toString();

                                    if (invoicedata.length() < 1) {
                                        popupWindow2.dismiss();
                                        popupWindow1.dismiss();
                                        return;
                                    }
                                    try {

                                        String paymentheaderid = pay.getJSONObject(grouppos_selected).getString("paymentheaderid");
                                        String payid=pay.getJSONObject(grouppos_selected).getString("paymentheaderid");
                                        db.deletpaymentitem(paymentheaderid); //pymentheaderid
                                        // db.updateinvoicetableonpaymentedit(invoicedata);

                                        if (!paymentmode.equals("Cheque")) {
                                            checknumbervalue = "";
                                            banknamevalue = "";
                                            checkdatevalue = "";
                                            banknamecode = "";
                                        }

                                        db.insertpayments(
                                                sid,
                                                paymentmode,
                                                String.valueOf(round(
                                                        tottal_amount_paid, 2)),
                                                checknumbervalue,
                                                banknamevalue, createtime,
                                                paymentid, checkdatevalue,
                                                paymentdate, String
                                                        .valueOf(lat), String
                                                        .valueOf(lng),
                                                invoicedata, actualmode,
                                                banknamecode, true);

                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }

                                    SharedPreferences pref = mcontext.getSharedPreferences("Config", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putBoolean("gotosecondtab", true);
                                    editor.commit();
                                    notifyDataSetChanged();
                                    popupWindow1.dismiss();
                                    Intent intent = new Intent(
                                            mcontext,
                                            InvoicePaymentApproveActivity.class);
                                    intent.putExtra("scheduledetailid", sid);
                                    mcontext.startActivity(intent);

                                }
                            });
                            //instantiate popup window
                            int width = LinearLayout.LayoutParams.MATCH_PARENT;
                            int height = LinearLayout.LayoutParams.MATCH_PARENT;
                            popupWindow1 = new PopupWindow(newcustomView, width, height);
                            popupWindow1.setFocusable(true);

                            //display the popup window
                            popupWindow1.showAtLocation(holder.popuplayout, Gravity.CENTER, 0, 0);
                            popupWindow1.setBackgroundDrawable(new ColorDrawable(Color.WHITE));


                        }
                    });


                }

            });
            holder.deletepayment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final View customView = mInflater.inflate(R.layout.popuplayout, null);
                    Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                    Button yes = (Button) customView.findViewById(R.id.yes);
                    TextView header = (TextView) customView.findViewById(R.id.header);
                    header.setText(mcontext.getResources().getString(R.string.deletepayment));
                    TextView msg = (TextView) customView.findViewById(R.id.message);
                    msg.setText(mcontext.getResources().getString(R.string.deletepaymentmsg));
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DatabaseHandler db = new DatabaseHandler(mcontext);
                            try {
                                String paymentheaderid = pay.getJSONObject(grouppos_selected).getString("paymentheaderid");
                                db.deletpaymentitem(paymentheaderid);
                                String paymethod = pay.getJSONObject(grouppos_selected).getString("payentmethode");
                                if (!paymethod.contentEquals("Credit"))
                                    db.updateinvoicetableonpaymentdeletion(pay.getJSONObject(grouppos_selected).getJSONArray("invoicedata"));
                                pay = db.getpaymentstodisplay(sid);
                                double toatal_amount = 0.0;
                                Boolean disableaproov = true;
                                for (int i = 0; i < pay.length(); i++) {
                                    JSONObject dataobj = pay.getJSONObject(i);
                                    String paymethode = dataobj.getString("payentmethode");
                                    String amount = dataobj.getString("amount");

                                    String aproove = dataobj.optString("aprovelag", "0");
                                    if (aproove.equals("0")) {
                                        disableaproov = false;
                                    }
                                    if (!paymethode.equals("Return") || !paymethode.contentEquals("Credit")) {
                                        toatal_amount += Double.parseDouble(amount);
                                    }
                                }
                                notifyItemRemoved(position);
                                Intent intent = new Intent(
                                        mcontext,
                                        InvoicePaymentApproveActivity.class);
                                intent.putExtra("scheduledetailid", sid);
                                mcontext.startActivity(intent);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            @SuppressWarnings("static-access")
                            SharedPreferences pref = mcontext.getSharedPreferences("Config", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putBoolean("needrefresh", true);
                            editor.commit();
                            notifyDataSetChanged();
                            popupWindow.dismiss();

                        }
                    });

                    //instantiate popup window
                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
                    popupWindow = new PopupWindow(customView, width, height);
                    popupWindow.setFocusable(true);

                    //display the popup window
                    popupWindow.showAtLocation(holder.popuplayout, Gravity.CENTER, 0, 0);
                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                        }
                    });

                }
            });

        }


    }



//     total number of rows
    @Override
    public int getItemCount() {
         return pay.length();
    }



    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder  { //implements View.OnClickListener
        TextView myTextView;
        Button cashbtn ;
        TextView date;
        TextView chequeno;
        TextView bankname;
        TextView chquedt;
        LinearLayout cdclayout;
        Button editpayment;
        Button deletepayment;
        LinearLayout popuplayout;
        TextView paidamount;
        TextView totalamt;


        ViewHolder(final View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.invoiceno);
            cashbtn = itemView.findViewById(R.id.cashbtn);
            date = itemView.findViewById(R.id.date);
            chequeno = itemView.findViewById(R.id.chequeno);
            bankname = itemView.findViewById(R.id.bankname);
            chquedt = itemView.findViewById(R.id.chequedt);
            cdclayout = itemView.findViewById(R.id.cdc);
            editpayment = itemView.findViewById(R.id.editpayment);
            deletepayment = itemView.findViewById(R.id.paimentcancel);
            popuplayout = itemView.findViewById(R.id.editpopup);
            paidamount = itemView.findViewById(R.id.paidamount);
            totalamt = itemView.findViewById(R.id.totalamt);
            linearLayout1 = itemView.findViewById(R.id.editpopup);
//            itemView.setOnClickListener(this);
        }

//        @Override
//        public void onClick(View view) {
//            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
//        }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(InvoiceMakePaymentAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
    private SpinnerAdapter initializeExpenseTypes() {

        String[] valSpinner = { mcontext.getResources().getString(R.string.cash), mcontext.getResources().getString(R.string.CDC), mcontext.getResources().getString(R.string.PDC) };
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mcontext,android.R.layout.simple_spinner_item, valSpinner) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#606060"));
                    tv.setTextSize(12);
                    String fontPath = "font/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            mcontext.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        dataAdapter .setDropDownViewResource(R.layout.spinner_item_payment);
        return dataAdapter;
    }
    public double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


}
