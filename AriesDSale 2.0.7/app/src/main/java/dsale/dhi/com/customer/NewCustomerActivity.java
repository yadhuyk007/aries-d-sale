package dsale.dhi.com.customer;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.gps.GPSAccuracy;
import dsale.dhi.com.gps.PlayTracker;
import dsale.dhi.com.objects.Scheduledata;
import dsale.dhi.com.objects.Scheduledetail;
import dsale.dhi.com.objects.webconfigurration;

public class NewCustomerActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_GPS_ACCURACY = 1 ;
    private Activity activity;
    List<String> categories = new ArrayList<String>();
    Hashtable<String, String> customercategorymap = new Hashtable<String, String>();
    private static DatabaseHandler db ;
    String[] townname;
    String[] townids;
    private static final String GSTTYPE_UNREGISTERED = "Unregistered";
    private static final String GSTTYPE_REGISTERED = "Registered";
    private static final String GSTTYPE_COMPOSITION = "Composition";
    List<String> gsttypes = new ArrayList<String>();
    List<String> days = new ArrayList<String>();
    private String latitude = "0";
    private String longitude = "0";
    private String customerid = "";
    private String createtime = "";
    private Bitmap customerphoto;
    //public static boolean OTP_verificationstatus= false;
    public static boolean OTP_verificationstatus= true;
    private static String mobilenumber;
    private static LayoutInflater inflater;
    private static EditText contactnumber;
    private static Button verification;
    private String imageType;
    private String bitmapkey;
    private static final int CAMERA_REQUEST = 1888;
    private static boolean highq = false;
    private static Uri mImageUri;
    ImageView img;
    private String photoPath="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_customer);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        /**
         * Check whether GPS
         */
        turnOnGPS();

       db = new DatabaseHandler(getApplicationContext());
        contactnumber = (EditText) findViewById(R.id.contactnumber);
        verification = (Button) findViewById(R.id.verify);

        try {
            intialisecategories();
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        Spinner spinner = (Spinner) findViewById(R.id.spinnercustcat);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, categories) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(9);

                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            activity.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }

        };
        dataAdapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        spinner.setAdapter(dataAdapter);
        try {
            intialisetowns();
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line, townname){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(9);
                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            activity.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        AutoCompleteTextView TownAuto = (AutoCompleteTextView) findViewById(R.id.TownAuto);
        TownAuto.setAdapter(adapter);
        //gst type dropdown
        /*gsttypes.add(GSTTYPE_UNREGISTERED);
        gsttypes.add(GSTTYPE_REGISTERED);
        gsttypes.add(GSTTYPE_COMPOSITION);*/
        gsttypes.add(getResources().getString(R.string.GSTTYPE_UNREGISTERED));
        gsttypes.add(getResources().getString(R.string.GSTTYPE_REGISTERED));
        gsttypes.add(getResources().getString(R.string.GSTTYPE_COMPOSITION));
        Spinner gsttypeSpinner = (Spinner) findViewById(R.id.spinnergsttype);
        ArrayAdapter<String> gsttypedataAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, gsttypes) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(9);
                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            activity.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        gsttypedataAdapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        gsttypeSpinner.setAdapter(gsttypedataAdapter);
        gsttypeSpinner.setSelection(0);//set select unregistered as default.
        //store closed days
        days.add(getResources().getString(R.string.select));
        days.add(getResources().getString(R.string.Sunday));
        days.add(getResources().getString(R.string.Monday));
        days.add(getResources().getString(R.string.Tuesday));
        days.add(getResources().getString(R.string.Wednesday));
        days.add(getResources().getString(R.string.Thursday));
        days.add(getResources().getString(R.string.Friday));
        days.add(getResources().getString(R.string.Saturday));

        Spinner closedday = (Spinner) findViewById(R.id.spinnerclosing_day);
        ArrayAdapter<String> closeddaydataAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, days) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(12);
                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            activity.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        closeddaydataAdapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        closedday.setAdapter(closeddaydataAdapter);

        //take photo
        ImageButton photo_camera=(ImageButton) findViewById(R.id.photo_camera);
        img= (ImageView) findViewById(R.id.addcust);
        photo_camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(NewCustomerActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(NewCustomerActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},CAMERA_REQUEST);
                }
                else
                {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File photo;
                    try {
                        photo = createTemporaryFile("picture", ".jpg");
                        photo.delete();

                    } catch (Exception e) {
                        Toast.makeText(NewCustomerActivity.this, R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mImageUri = Uri.fromFile(photo);
                   // mImageUri= FileProvider.getUriForFile(NewCustomerActivity.this, "dsale.dhi.com.ariesdsale.fileprovider", photo);


                            //intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                    //intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    startActivityForResult(intent, CAMERA_REQUEST);

                }


            }
        });
        //validate loc and save
        Button savenewcustmr=(Button) findViewById(R.id.savenewcustmr);
        savenewcustmr.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

             validateAndCaptureGPS();
                    //need to write code to validate gps
                  //  saveData();



            }
        });



    }
    private void intialisecategories() throws JSONException {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("Config",
                Context.MODE_PRIVATE);
        String personrole=pref.getString(
                "personrole", "");
        JSONArray categoryarr = db.getcustomercategories(personrole);
        categories.add(getResources().getString(R.string.SELECT));
        for (int i = 0; i < categoryarr.length(); i++) {
            JSONObject obj = categoryarr.optJSONObject(i);
            if (!customercategorymap.containsKey(obj.optString("catname"))) {
                customercategorymap.put(obj.optString("catname"),
                        obj.optString("catcode"));
                categories.add(obj.optString("catname"));
            }
        }
    }
    private void intialisetowns() throws JSONException {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("Config",
                Context.MODE_PRIVATE);
        JSONArray towns = db.gettown();
        townname = new String[towns.length()];
        townids = new String[towns.length()];

        for (int i = 0; i < towns.length(); i++) {
            JSONObject obj = towns.getJSONObject(i);
            townname[i] = obj.getString("townname");
            townids[i] = obj.getString("townid");
        }
    }
    private void saveData(double latitude,double longitude,float accuracy,String provider) {

        String name = ((EditText) findViewById(R.id.firstname)).getText().toString();
        String address = ((EditText) findViewById(R.id.address)).getText().toString();

        //String custcode = ((EditText)findViewById(R.id.custcode)).getText().toString();

        String category = ((Spinner)findViewById(R.id.spinnercustcat)).getSelectedItem().toString();
        String contactnumber = ((EditText) findViewById(R.id.contactnumber)).getText().toString();
        String gstin = ((EditText)findViewById(R.id.gstin)).getText().toString();
        String town = ((AutoCompleteTextView)findViewById(R.id.TownAuto)).getText().toString();
        String contactperson = ((EditText)findViewById(R.id.contactperson)).getText().toString();
        String closingday = ((Spinner)findViewById(R.id.spinnerclosing_day)).getSelectedItem().toString();

        //String branch = ((Spinner)findViewById(R.id.branch)).getSelectedItem().toString();

        String remarks = ((EditText)findViewById(R.id.remarks)).getText().toString();
        String gstType = ((Spinner) findViewById(R.id.spinnergsttype)).getSelectedItem().toString();
        String fssi = ((EditText) findViewById(R.id.fssi)).getText().toString();

        String isvalid = valid(gstType,name, address, category, gstin, town,contactperson,contactnumber);
        if (!isvalid.contentEquals("TRUE")) {
            /*Global.Toast(NewCustomerActivity.this, isvalid,
                    Toast.LENGTH_SHORT, Font.Regular);*/
            Toast.makeText(NewCustomerActivity.this, isvalid, Toast.LENGTH_SHORT).show();
            return;
        }

        boolean otpverified = OTP_verificationstatus;
        String categoryids = "";
        if (!category.equals(""))
            categoryids = db.getcustomercategoriesId(category);
        if (!town.equals(""))
            town = db.gettownId(town);

        String schbegintime =db.getschedulebegintime();
        if(schbegintime == null || schbegintime == "" ){
            Calendar c = Calendar.getInstance();
            DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            schbegintime = dateTimeFormat.format(c.getTime());
            db.updatescheduleheaderbegintime(schbegintime);
            db.intialisesynctable(schbegintime);
            SharedPreferences pref = getApplicationContext().getSharedPreferences(
                    "Config", MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("Schbegintime", schbegintime);
            editor.commit();
        }

        try {
            String uuid = UUID.randomUUID().toString();
            closingday = closingday.contentEquals(getResources().getString(R.string.SELECT)) ? "" : closingday;
            Addlocation(name, address,category,categoryids,gstin,  contactperson, contactnumber,town, otpverified ? "1" : "0",  uuid,
                     closingday,schbegintime, remarks,gstType,fssi,
                    latitude+"",longitude+"",accuracy+"",provider);
            if (customerphoto != null) {
                String filename = uuid + ".png";
                createDirectoryAndsavetempfile(customerphoto, "photo_"
                        + filename);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(NewCustomerActivity.this, R.string.please_try_again, Toast.LENGTH_SHORT).show();

            try {
                File tempDir = Environment.getExternalStorageDirectory();
                tempDir = new File(tempDir.getAbsolutePath() + "/AriesErrors/");
                if (!tempDir.exists()) {
                    tempDir.mkdirs();
                }
                Calendar c = Calendar.getInstance();
                String myFormat = "dd-MM-yyyy HH:mm:ss";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                myFormat = "yyyyMMddHHmmss";
                sdf = new SimpleDateFormat(myFormat, Locale.US);
                String filedate = sdf.format(c.getTime());
                File myExternalFile = new File(tempDir, "errorlog_" + filedate
                        + ".txt");
                String error = filedate + "-----------";
                error += e.toString();
                FileOutputStream fos = new FileOutputStream(myExternalFile);
                OutputStreamWriter osw = new OutputStreamWriter(fos);
                osw.append(error);
                osw.close();
            } catch (IOException ioe) {
                // ...
            }
        }
    }
    private String valid(String gstType,String name, String address, String category,
                          String gstin, String town,String cntactperson,String contactno) {
        String temp = "TRUE";
        if (cntactperson.contentEquals(""))
            return getResources().getString(R.string.contact_required);
        if (contactno.contentEquals(""))
            return getResources().getString(R.string.number_required);
        if (name.contentEquals(""))
            return getResources().getString(R.string.name_required);
        if (address.contentEquals(""))
            return getResources().getString(R.string.addr_required);
        if (category.contentEquals(getResources().getString(R.string.SELECT)))
            return getResources().getString(R.string.select_cat);
       /* if (custcode.length() > 9)
            return "Customer Code must be less than 10 letters!";*/

        if (!gstType.equals(getResources().getString(R.string.GSTTYPE_UNREGISTERED)) && gstin.length() == 0 ) {
            return getResources().getString(R.string.select_gstin);
        }

        if (gstType.equals(getResources().getString(R.string.GSTTYPE_UNREGISTERED)) && gstin.length() > 1 ) {
            return getResources().getString(R.string.chk_gsttype);
        }

        if (gstin.length() != 0 && gstin.length() != 15)
            return getResources().getString(R.string.gstin_validation);
        if (town.contentEquals(""))
            return getResources().getString(R.string.town_required);
        else if (!db.isvalidtown(town))
            return getResources().getString(R.string.valid_town);

        if (customerphoto == null)
            return getResources().getString(R.string.photo_required);

        return temp;
    }
    public void Addlocation(String name, String adress, String customercategory,String customercategorycode,
                            String tinnumber, String contactpersonvalue, String Contactpersonnumbervalue,
                            String townclassvalue,
                            String otpverified, String uuid,
                            String closingday,
                            String schbegintime,
                            String remarks, String gstType, String fssi, String latitude,
                            String longitude, String locaccuracy, String provider)
            throws Exception {
        Locations loctodb = new Locations();
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String crtime = dateTimeFormat.format(c.getTime());

        String schdulineguid = UUID.randomUUID().toString();

        loctodb.Locationname = name;
        loctodb.Locationadress = adress;
        loctodb.Latitude = latitude;
        loctodb.Longitude = longitude;
        loctodb.locaccuracy = locaccuracy ;
        loctodb.locprovider = provider;
        loctodb.Locationid = UUID.randomUUID().toString();
        customerid = loctodb.Locationid;
        loctodb.createtime = crtime;
        createtime = crtime;
        loctodb.customercategory = customercategory;
        loctodb.customercatogorycode = customercategorycode;
        loctodb.tinnumber = tinnumber;

        loctodb.contactpersonvalue = contactpersonvalue;
        loctodb.Contactpersonnumbervalue = Contactpersonnumbervalue;

        loctodb.townclassvalue = townclassvalue;

        loctodb.schdulineguid = schdulineguid;

        loctodb.otpverified = otpverified;


        loctodb.photouuid = uuid;

        loctodb.closingday = closingday;


        loctodb.remarks = remarks;
        loctodb.gstType = gstType;
        loctodb.fssi = fssi;
        db.addnewlocation(loctodb);

        String schedheadid = db.isnewlocationsexist();
        if (schedheadid.equals("0")) {
            Calendar c2 = Calendar.getInstance();
            DateFormat dateTimeFormat2 = new SimpleDateFormat("MM/dd/yyyy");
            String schdate = dateTimeFormat2.format(c2.getTime());
            SharedPreferences pref = getApplicationContext()
                    .getSharedPreferences("Config", MODE_PRIVATE);
            Scheduledata headerdatatodb = new Scheduledata();
            headerdatatodb.Scheduleid = UUID.randomUUID().toString();
            headerdatatodb.Schedulename = "New Locations";
            headerdatatodb.Salespersonid = pref.getString("personid", "0");
            headerdatatodb.salespersonname = pref.getString("personname", "");
            headerdatatodb.Routenetworkid = "0";
            headerdatatodb.Headerstatus = "";
            headerdatatodb.Scheduledate = schdate;
            headerdatatodb.begintime = schbegintime;

            Scheduledetail detail = new Scheduledetail();

            detail.Scheduledetailid = schdulineguid;
            detail.locationid = loctodb.Locationid;
            detail.locationname = loctodb.Locationname;
            detail.locationadress = loctodb.Locationadress;
            detail.sequencenumber = "1";
            detail.status = "pending";
            detail.latitude = loctodb.Latitude;
            detail.longitude = loctodb.Longitude;
            detail.Customercatogory = loctodb.customercategory;
            detail.CustomercatogoryId = db
                    .getcustomercategoriesId(loctodb.customercategory);
            detail.invoicenumber = "";
            detail.picklistnumber = "";
            detail.customercode = loctodb.costomercode;
            detail.locationlock = "0";
            detail.Closingday = "";
            detail.mobilenumber = loctodb.Contactpersonnumbervalue;
            detail.tinnumber = loctodb.tinnumber;

            detail.town = townclassvalue;
            detail.contactperson = loctodb.contactpersonvalue;

            detail.otpverified = otpverified;

            detail.photoUUID = uuid;
            detail.Closingday = closingday;
            detail.gsttype = gstType;
            detail.fssi = fssi;

            db.firstlocationadd(headerdatatodb, detail);
            Intent intent = new Intent(this, CustomerListActivity.class);
            finish();
            overridePendingTransition(0, 0);
            startActivity(intent);
            overridePendingTransition(0, 0);
        }

        else {
            Scheduledetail detail = new Scheduledetail();
            detail.Scheduledetailid = schdulineguid;
            detail.locationid = loctodb.Locationid;
            detail.locationname = loctodb.Locationname;
            detail.locationadress = loctodb.Locationadress;
            detail.sequencenumber = "1";
            detail.status = "pending";
            detail.latitude = loctodb.Latitude;
            detail.longitude = loctodb.Longitude;
            detail.Customercatogory = loctodb.customercategory;
            detail.CustomercatogoryId = db
                    .getcustomercategoriesId(loctodb.customercategory);
            detail.invoicenumber = "";
            detail.picklistnumber = "";
            detail.customercode = loctodb.costomercode;
            detail.locationlock = "0";
            detail.Closingday = "";
            detail.mobilenumber = loctodb.Contactpersonnumbervalue;
            detail.tinnumber = loctodb.tinnumber;

            detail.town = townclassvalue;
            detail.contactperson = loctodb.contactpersonvalue;

            detail.otpverified = otpverified;
            detail.photoUUID = uuid;
            detail.Closingday = closingday;
            detail.gsttype = gstType;
            detail.fssi = fssi;
            db.addtoexistingnewlocations(schedheadid, detail);
            Intent intent = new Intent(this, CustomerListActivity.class);
            finish();
            startActivity(intent);
            overridePendingTransition(R.anim.rev1, R.anim.rev2);
        }

    }

    private JSONObject getstatusobject() throws Exception {
        JSONObject temp = new JSONObject();
        SharedPreferences pref = NewCustomerActivity.this.getSharedPreferences("Config",
                Context.MODE_PRIVATE);

        String person = "";
        String lat = "";
        String lng = "";
        String operation = "";
        String version = "";
        String createdon = "";
        String uuid = "";
        String appname = "";
        String versioncode = "";
        person = pref.getString("personid", "0");

        operation = webconfigurration.status;
        appname = webconfigurration.appname;
        lat = webconfigurration.lat;
        lng = webconfigurration.lng;

        PackageInfo pInfo = NewCustomerActivity.this.getPackageManager().getPackageInfo(
                NewCustomerActivity.this.getPackageName(), 0);
        version = pInfo.versionName;
        versioncode = String.valueOf(pInfo.versionCode);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        createdon = df.format(c.getTime());

        uuid = UUID.randomUUID().toString();

        temp.put("person", person).put("lat", lat).put("lng", lng)
                .put("operation", operation).put("version", version)
                .put("versioncode", versioncode).put("appname", appname)
                .put("createdon", createdon).put("uuid", uuid);
        return temp;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) NewCustomerActivity.this
                .getSystemService(NewCustomerActivity.this.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }




    private void createDirectoryAndsavetempfile(Bitmap imageToSave,
                                                String fileName) {

        File rootsd = Environment.getExternalStorageDirectory();
        File direct = new File(rootsd.getAbsolutePath() + "/Aries");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        File file = new File(direct, fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 75, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        Bitmap rotatedBitmap = image;
       /* try {
            ExifInterface ei = new ExifInterface(photoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

         //   Bitmap rotatedBitmap = null;
            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(image, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(image, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(image, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = image;
            }

        }catch (Exception e){
            e.printStackTrace();
        }*/
        return Bitmap.createScaledBitmap(rotatedBitmap, bitmapWidth, bitmapHeight, true);
    }

    public  Bitmap rotateImage(Bitmap sourceBitmap, float angle) {
            Matrix matrix = new Matrix();
            matrix.postRotate(angle);
            return Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(),
                    matrix, true);
        }
    protected void onActivityResult(int requestCode,int resultCode,Intent data)
    {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                Bundle extras =data.getExtras();
                Bitmap mImageBitmap=(Bitmap)extras.get("data");
                ImageView proof=(ImageView)findViewById(R.id.addcust);
                customerphoto=getResizedBitmap(mImageBitmap,500,500);
                proof.setImageBitmap(customerphoto);
            }else if (requestCode == REQUEST_CODE_GPS_ACCURACY){

                if (resultCode == Activity.RESULT_OK) {
                    Double latitude = data.getDoubleExtra("latit", 0);
                    Double longitude = data.getDoubleExtra("longit", 0);
                    Float accuracy = data.getFloatExtra("minacc", 0);
                    String provider = data.getStringExtra("prov");
                    String operation = data.getStringExtra("operation");

                    if (latitude > 0 && longitude > 0)
                        saveData(latitude, longitude, accuracy, provider);
                    else if (!webconfigurration.GPSMandatory){
                        saveData(latitude, longitude, accuracy, "");
                    }else
                       Toast.makeText(NewCustomerActivity.this,
                               R.string.location_not_found,Toast.LENGTH_SHORT).show();
                }
                if (locationaccuracy()!=3) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(NewCustomerActivity.this);
                    builder.setTitle(R.string.enable_high_loc_accuracy);
                    builder.setMessage(R.string.loc_acc_low);


                    builder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            });

                    builder.show();

                    return;
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(NewCustomerActivity.this,
                            R.string.cancelled,Toast.LENGTH_SHORT).show();

                    // Write your code if there's no result
                }
            }
        }catch (Exception e) {
           Toast.makeText(this, R.string.turn_off_high_quality_img, Toast.LENGTH_SHORT).show();
        }
    }
    private File createTemporaryFile(String part, String ext) throws Exception {

        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }
   /* public void grabImage(ImageView imageView) {
        try {
            this.getContentResolver().notifyChange(mImageUri, null);
            ContentResolver cr = this.getContentResolver();
            Bitmap bitmap;
            customerphoto = getResizedBitmap(MediaStore.Images.Media.getBitmap(cr,mImageUri), 500, 500);
            imageView.setImageBitmap(customerphoto);
        } catch (Exception e) {
            Toast.makeText(this, R.string.failed_to_load, Toast.LENGTH_SHORT).show();
        }
        // System.gc();
    }*/


    private void validateAndCaptureGPS() {

        turnOnGPS();

        String name = ((EditText) findViewById(R.id.firstname)).getText().toString();
        String address = ((EditText) findViewById(R.id.address)).getText().toString();

        //String custcode = ((EditText)findViewById(R.id.custcode)).getText().toString();

        String category = ((Spinner)findViewById(R.id.spinnercustcat)).getSelectedItem().toString();
        String contactnumber = ((EditText) findViewById(R.id.contactnumber)).getText().toString();
        String gstin = ((EditText)findViewById(R.id.gstin)).getText().toString();
        String town = ((AutoCompleteTextView)findViewById(R.id.TownAuto)).getText().toString();
        String contactperson = ((EditText)findViewById(R.id.contactperson)).getText().toString();
        String closingday = ((Spinner)findViewById(R.id.spinnerclosing_day)).getSelectedItem().toString();

        //String branch = ((Spinner)findViewById(R.id.branch)).getSelectedItem().toString();

        String remarks = ((EditText)findViewById(R.id.remarks)).getText().toString();
        String gstType = ((Spinner) findViewById(R.id.spinnergsttype)).getSelectedItem().toString();
        String fssi = ((EditText) findViewById(R.id.fssi)).getText().toString();

        String isvalid = valid(gstType,name, address, category, gstin, town,contactperson,contactnumber);
        if (!isvalid.contentEquals("TRUE")) {
            /*Global.Toast(NewCustomerActivity.this, isvalid,
                    Toast.LENGTH_SHORT, Font.Regular);*/
            Toast.makeText(NewCustomerActivity.this, isvalid, Toast.LENGTH_SHORT).show();
            return;
        }

    //    turnOnGPS();
        if (!PlayTracker.intime()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(NewCustomerActivity.this);
            builder.setTitle(R.string.gps_unavailable);
            builder.setMessage(R.string.check_your_gps);

            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

            AlertDialog dialog = builder.create();
            builder.show();

            return;
        }
        if (locationaccuracy()!=3) {
            AlertDialog.Builder builder = new AlertDialog.Builder(NewCustomerActivity.this);
            builder.setTitle(R.string.enable_high_loc_accuracy);
            builder.setMessage(R.string.loc_acc_low);

            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    });

            builder.show();

            return;
        }

        Intent intent = new Intent(NewCustomerActivity.this,
                GPSAccuracy.class);
        intent.putExtra("operation", "addlocation");
        startActivityForResult(intent, REQUEST_CODE_GPS_ACCURACY);
        overridePendingTransition(0, 0);

    }

    private void turnOnGPS() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!statusOfGPS) {

            AlertDialog.Builder builder = new AlertDialog.Builder(NewCustomerActivity.this);
            builder.setTitle(R.string.switch_on_gps);
            builder.setMessage(R.string.gps_off_alert_message);


            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            startActivityForResult(
                                    new Intent(
                                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                    0);
                        }
                    });


            builder.show();

            return;
        }
    }


    private int locationaccuracy(){
        int locationMode=-1;
        try {
            locationMode = Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {

            e.printStackTrace();
        }
        return locationMode;
    }

}
