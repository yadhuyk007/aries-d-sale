package dsale.dhi.com.objects;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hp on 03/04/2019.
 */

public class webconfigurration {


    /***************************** DMSLite **********************/
    public static String bucketaddress = "dms";
    //public static String mainurl = "http://demotms.dhicubes.com:8080/dms"; //QA

    public static String mainurl = "https://dms.dhisigma.com/dhi";// PRODUCTION


    /**************************** Local ***************************/
//	public static String mainurl = "http://192.168.2.5:8080/EMRCodes";

//	public static String mainurl = "http://192.168.2.170:8080/EMRCodes";

    //public static String mainurl = "http://192.168.1.2:8080/EMRCodes"; //yadhu local

    /********************** do not modify this values **************/
    public static String loginurl = mainurl
            + "/services/dhiservices/userloginsalespro";
    public static String url = mainurl + "/services/dhiservices/service";

    public static String stocktransferUrl = mainurl + "/services/dhiservices/vansalesoperations";

    public static String backupServiceUrl = mainurl + "/services/dhiservices/backup";

    public static String bucketapp = bucketaddress + "/ariesd.apk";
    public static String auth = "-1396247038";
    public static String status = "Refresh";
    public static String appname = "ariesd";
    public static String lat = "";
    public static String lng = "";

    public String bu = "";
    public String orgid = "";
    public String appid = "";
    public String user = "";
    public String dbname = "";

    public String companyNameLong = "";
    public String companyNameShort = "";
    public String companyAddressLong = "";
    public String companyAddressShort = "";
    public String companyContact = "";
    public String companyGSTIN = "";

    public boolean rateWithOutTax = false;
    public boolean showDiscPercentage = false;
    public boolean showProdCatPer = false;

    public String taxType = "Intrastate";

    public static boolean GPSMandatory = false ;

    public webconfigurration(Context c) {
        SharedPreferences pref = c.getSharedPreferences("Config",
                Context.MODE_PRIVATE);
        bu = pref.getString("BussinessUnit", "");
        orgid = pref.getString("orgId", "");
        dbname = pref.getString("orgDb", "");
        user = pref.getString("UserName", "");
        appid = pref.getString("appid", "");


        if (orgid.equals("39")) {//HardCoded for Greenmount

            companyNameLong = "Greenmount Spices Pvt Ltd,Neriamangalam P.O,Idukki,Kerala";
            companyNameShort = "Greenmount Spices Pvt Lt";
            companyAddressLong = "Reg. office:Nellikuzhi P.O,686691,Kothamangalam,Ernakulam";
            companyAddressShort = "Reg Office:Kothamangalam";
            companyContact = "Phone:0485-2827371,9495930076";
            companyGSTIN = "GSTIN:32AACCG0501B1ZR";

            rateWithOutTax = false;
            showDiscPercentage = false;
            showProdCatPer = true;
            taxType = "Intrastate";

        } /*else if (orgid.equals("43")) {//HardCoded for Pipefield

            companyNameLong = "ROOFCO TRADING COMPANY PVT LTD";
            companyNameShort = "ROOFCO TRADING COMPANY PVT LT";
            companyAddressLong = "44/3504,Deshabhimani Road,Kaloor,Kochi-682017";
            companyAddressShort = "Reg.Office: Kaloor";
            companyContact = "Phone:0484 2536244";
            companyGSTIN = "GSTIN:32AACCR8840E1ZK";

        }*/ else if (orgid.equals("47")) {//HardCoded for Sabari

            companyNameLong = "Sabari Distribution Pvt Ltd.";
            companyNameShort = "Sabari Distribution Pvt Ltd.";
            companyAddressLong = "Office: V/950F, Alupuram, Kuttikattukara P.O,683504, Kalamassery";
            companyAddressShort = "Kuttikattukara,Kalamassery";
            companyContact = "FSSAI:11316007001040";
            companyGSTIN = "GSTIN:32AAFCS3552D1ZR";

            rateWithOutTax = true;
            showDiscPercentage = true;
            showProdCatPer = false;
            taxType = "Intrastate";

        } else if(orgid.equals("51")){

            companyNameLong = "Greenmount Spices Pvt Ltd,Neriamangalam P.O,Idukki,Kerala";
            companyNameShort = "Greenmount Spices Pvt Lt";
            companyAddressLong = "Reg. office:Nellikuzhi P.O,686691,Kothamangalam,Ernakulam";
            companyAddressShort = "Reg Office:Kothamangalam";
            companyContact = "Phone:0485-2827371,9495930076";
            companyGSTIN = " ";

            rateWithOutTax = false;
            showDiscPercentage = false;
            showProdCatPer = false;

            taxType = "Interstate";
        }
    }

}
