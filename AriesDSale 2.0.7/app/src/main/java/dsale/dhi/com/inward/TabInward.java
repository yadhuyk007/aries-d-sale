package dsale.dhi.com.inward;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import dsale.dhi.com.ariesdsale.Product;
import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.database.DatabaseHandler;

import static android.content.Context.MODE_PRIVATE;

public class TabInward extends Fragment  implements InwardOutwardAdapter.ItemClickListener {
    InwardOutwardAdapter adapter;

    private DatabaseHandler db;
    private Button category;
    private Button brand;
    private Button form;
    private Button itemlist;
    PopupWindow popupWindow;
    private SharedPreferences pref;
    private String selectedCategory;
    private String selectedBrand;
    private String selectedForm;
    private String searchString;
    private String catid;
    private String state;
    private EditText search;
    private Button all;
    private TextView nos;
    int selection;
    private RecyclerView recyclerView;
    private String cuttype;
    private HashMap<String, Product> productmap;
    private HashMap<String, String> qtymap;
    private HashMap<String, String> casemap;
    private HashMap<String, String> batchmap;
    private JSONArray productListArray ;
    private HashMap<String, String> packageTypeMap;
    private List<String> packageTypeList = new ArrayList<>();
    private HashMap<String, String> batchdb;
    private List<String> batchlist = new ArrayList<>();
    private List<String> productids;
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view =  inflater.inflate(R.layout.inward_or_outward, container, false);
        category =(Button) view.findViewById(R.id.cat);
        final RecyclerView  linearLayout1 = (RecyclerView) view.findViewById(R.id.inward_recycler_view);
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.inward_recycler_view);

        db = new DatabaseHandler(view.getContext());
        productmap = new HashMap<>();
        productListArray = new JSONArray();
        packageTypeMap = db.getPackageTypes();
        for (String key : packageTypeMap.keySet()) {
            packageTypeList.add(key);
        }
        batchdb = db.getBatches();
        for (String key : batchdb.keySet()) {
            batchlist.add(key);
        }
        pref = (view.getContext()).getSharedPreferences("Config", MODE_PRIVATE);
        selectedCategory = "All";
        selectedForm = "All";
        selectedBrand = "All";
        searchString = "";
        selection = 0;
        search = (EditText) view.findViewById(R.id.searchText);
        nos = (TextView) view.findViewById(R.id.nos);
        all=(Button) view.findViewById(R.id.all);
        brand = (Button) view.findViewById(R.id.brand);
        form = (Button) view.findViewById(R.id.form);
        itemlist = (Button) view.findViewById(R.id.itemlist);

        itemlist.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            itemlist1(v);

                                        }
                                    });
        settitle(all, category, brand, form);
        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                searchString = s.toString();
                listPopulate();

                final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.inward_recycler_view);
                recyclerView.setLayoutManager(new LinearLayoutManager(TabInward.this.getActivity().getApplicationContext()));
//                adapter = new InwardOutwardAdapter(TabInward.this.getActivity().getApplicationContext(), productids,productmap,qtymap,view);
                adapter = new InwardOutwardAdapter(TabInward.this.getActivity().getApplicationContext(), productids,productmap,qtymap,view,casemap,batchmap);
                adapter.setClickListener(TabInward.this);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if(pref.getString("InwardOutwardItemlist", "") != null){
            setSelectedProductsHashMap(pref.getString("InwardOutwardItemlist", ""));
            nos.setText(String.valueOf(qtymap.size()) +  getResources().getString(R.string.nos));

        }
        else{
            qtymap = new HashMap<>();
            casemap = new HashMap<>();
            batchmap = new HashMap<>();
        }
        listPopulate();
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity().getApplicationContext()));
        adapter = new InwardOutwardAdapter(TabInward.this.getActivity().getApplicationContext(), productids,productmap,qtymap,view,casemap,batchmap);
//        adapter = new InwardOutwardAdapter(TabInward.this.getActivity().getApplicationContext(), productids,productmap,qtymap,view);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        //==========================
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                LayoutInflater layoutInflaternflater = (LayoutInflater) view.(Context.LAYOUT_INFLATER_SERVICE);
                final View customView = inflater.inflate(R.layout.inward_popup, null);
                TextView header =(TextView) customView.findViewById(R.id.header);
                final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.inward_recycler_view);
                header.setText(R.string.Categories);
                ImageButton closePopupBtn = (ImageButton) customView.findViewById(R.id.close);

                //instantiate popup window
                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                popupWindow = new PopupWindow(customView, width, height);
                popupWindow.setFocusable(true);

                //display the popup window
                popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);
                popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                //close the popup window on button click
                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });


                ListView list = (ListView) customView.findViewById(R.id.list);
                final List<String> cats = db.getallproductcats();
                final HashMap<String, String> catMap = db.getproductCatMap();
                ListAdapter arrayadapter = new BaseAdapter() {

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View mView = inflater.inflate(R.layout.adapter_cat_list, null);
                        final TextView textView = (TextView) mView
                                .findViewById(R.id.textView);
                        textView.setText(getItem(position));
                        textView.setTag(getItemid(position));
                        mView.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // Toast.makeText(getApplicationContext(),
                                // textView.getTag().toString(),
                                // Toast.LENGTH_SHORT).show();
                                selectedCategory = textView.getTag().toString();
                                selectedBrand = "All";
                                selectedForm = "All";
                                selection = 0;
                                listPopulate();
                                popupWindow.dismiss();
                                recyclerView.setLayoutManager(new LinearLayoutManager(TabInward.this.getActivity().getApplicationContext()));
                                adapter = new InwardOutwardAdapter(TabInward.this.getActivity().getApplicationContext(), productids,productmap,qtymap,view,casemap,batchmap);
//                                adapter = new InwardOutwardAdapter(TabInward.this.getActivity().getApplicationContext(), productids,productmap,qtymap,view);
                                adapter.setClickListener(TabInward.this);
                                recyclerView.setAdapter(adapter);
                                category.setBackgroundResource(R.drawable.cansel_corner);
                                settitle(category, all, brand, form);
                            }

                        });
                        return mView;
                    }

                    private String getItemid(int position) {
                        return cats.get(position);
                    }

                    @Override
                    public long getItemId(int position) {
                        return position;
                    }

                    @Override
                    public String getItem(int position) {
                        return catMap.get(cats.get(position));
                    }

                    @Override
                    public int getCount() {
                        return cats == null ? 0 : cats.size();
                    }
                };
                list.setAdapter(arrayadapter);

            }

        });
        brand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                LayoutInflater layoutInflaternflater = (LayoutInflater) view.(Context.LAYOUT_INFLATER_SERVICE);
                final View customView = inflater.inflate(R.layout.inward_popup, null);
                TextView header =(TextView) customView.findViewById(R.id.header);
                final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.inward_recycler_view);
                header.setText(R.string.Brands);
                ImageButton closePopupBtn = (ImageButton) customView.findViewById(R.id.close);

                //instantiate popup window
                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                popupWindow = new PopupWindow(customView, width, height);
                popupWindow.setFocusable(true);

                //display the popup window
                popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);
                popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                //close the popup window on button click
                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });


                ListView list = (ListView) customView.findViewById(R.id.list);
                final List<String> cats = db.getallproductbrands(selectedCategory);
                final HashMap<String, String> catMap = db.getproductBrandMap(selectedCategory);

                ListAdapter arrayadapter = new BaseAdapter() {

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View mView = inflater.inflate(R.layout.adapter_cat_list, null);
                        final TextView textView = (TextView) mView
                                .findViewById(R.id.textView);
                        textView.setText(getItem(position));
                        textView.setTag(getItemid(position));
                        mView.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // Toast.makeText(getApplicationContext(),
                                // textView.getTag().toString(),
                                // Toast.LENGTH_SHORT).show();
                                selectedBrand = textView.getTag().toString();
                                selectedForm = "All";
                                selection = 0;
                                listPopulate();
                                popupWindow.dismiss();
                                recyclerView.setLayoutManager(new LinearLayoutManager(TabInward.this.getActivity().getApplicationContext()));
                                adapter = new InwardOutwardAdapter(TabInward.this.getActivity().getApplicationContext(), productids,productmap,qtymap,view,casemap,batchmap);
//                                adapter = new InwardOutwardAdapter(TabInward.this.getActivity().getApplicationContext(), productids,productmap,qtymap,view);
                                adapter.setClickListener(TabInward.this);
                                recyclerView.setAdapter(adapter);
                                brand.setBackgroundColor(getResources().getColor(R.color.btnback));
                                form.setBackgroundResource(R.drawable.filterbtn_border);
                                all.setBackgroundResource(R.drawable.filterbtn_border);
                                // settitle(brand, category, all, form);
                            }
                        });
                        return mView;
                    }

                    private String getItemid(int position) {
                        return cats.get(position);
                    }

                    @Override
                    public long getItemId(int position) {
                        return position;
                    }

                    @Override
                    public String getItem(int position) {
                        return catMap.get(cats.get(position));
                    }

                    @Override
                    public int getCount() {
                        return cats == null ? 0 : cats.size();
                    }
                };
                list.setAdapter(arrayadapter);

            }

        });
        form.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                LayoutInflater layoutInflaternflater = (LayoutInflater) view.(Context.LAYOUT_INFLATER_SERVICE);
                final View customView = inflater.inflate(R.layout.inward_popup, null);
                TextView header =(TextView) customView.findViewById(R.id.header);
                final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.inward_recycler_view);
                header.setText(R.string.Forms);
                ImageButton closePopupBtn = (ImageButton) customView.findViewById(R.id.close);

                //instantiate popup window
                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                popupWindow = new PopupWindow(customView, width, height);
                popupWindow.setFocusable(true);

                //display the popup window
                popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);
                popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                //close the popup window on button click
                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });


                ListView list = (ListView) customView.findViewById(R.id.list);
                final List<String> cats = db.getallproductforms(selectedCategory,selectedBrand);
                final HashMap<String, String> catMap = db.getproductFormMap( selectedCategory, selectedBrand);

                ListAdapter arrayadapter = new BaseAdapter() {

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View mView = inflater.inflate(R.layout.adapter_cat_list, null);
                        final TextView textView = (TextView) mView
                                .findViewById(R.id.textView);
                        textView.setText(getItem(position));
                        textView.setTag(getItemid(position));
                        mView.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // Toast.makeText(getApplicationContext(),
                                // textView.getTag().toString(),
                                // Toast.LENGTH_SHORT).show();
                                selectedForm = textView.getTag().toString();
                                selection = 0;
                                listPopulate();
                                popupWindow.dismiss();
                                recyclerView.setLayoutManager(new LinearLayoutManager(TabInward.this.getActivity().getApplicationContext()));
                                adapter = new InwardOutwardAdapter(TabInward.this.getActivity().getApplicationContext(), productids,productmap,qtymap,view,casemap,batchmap);
//                                adapter = new InwardOutwardAdapter(TabInward.this.getActivity().getApplicationContext(), productids,productmap,qtymap,view);
                                adapter.setClickListener(TabInward.this);
                                recyclerView.setAdapter(adapter);
//                                form.setBackgroundResource(R.drawable.cansel_corner);
                                form.setBackgroundColor(getResources().getColor(R.color.btnback));
                                all.setBackgroundResource(R.drawable.filterbtn_border);
                                // settitle(form, all, brand, category);
                            }
                        });
                        return mView;
                    }

                    private String getItemid(int position) {
                        return cats.get(position);
                    }

                    @Override
                    public long getItemId(int position) {
                        return position;
                    }

                    @Override
                    public String getItem(int position) {
                        return catMap.get(cats.get(position));
                    }

                    @Override
                    public int getCount() {
                        return cats == null ? 0 : cats.size();
                    }
                };
                list.setAdapter(arrayadapter);

            }

        });
        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.inward_recycler_view);
                selectedCategory = "All";
                selectedBrand = "All";
                selectedForm = "All";
                selection = 0;
                listPopulate();
                settitle(all, category, brand, form);
                recyclerView.setLayoutManager(new LinearLayoutManager(TabInward.this.getActivity().getApplicationContext()));
                adapter = new InwardOutwardAdapter(TabInward.this.getActivity().getApplicationContext(), productids,productmap,qtymap,view,casemap,batchmap);
//                                adapter = new InwardOutwardAdapter(TabInward.this.getActivity().getApplicationContext(), productids,productmap,qtymap,view);
                adapter.setClickListener(TabInward.this);
                recyclerView.setAdapter(adapter);

            }

        });
        //==========================

        return view;//inflater.inflate(R.layout.inward_or_outward, container, false);

    }
    @Override
    public void onItemClick(View view, int position) {
//        Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
    }
    private void setSelectedProductsHashMap(String productListString) {

        qtymap = new HashMap<>();
        casemap = new HashMap<>();
        batchmap = new HashMap<>();

        try{
            productListArray= new JSONArray(productListString);

            for ( int i=0;i<productListArray.length();i++) {
                String state=productListArray.getJSONObject(i).getString("transfer");
                if(!state.equals(getResources().getString(R.string.inward)) )
                {
                    continue;
                }
                else {
                    String prdid = productListArray.getJSONObject(i).getString("ProductId");
                    String Qty = productListArray.getJSONObject(i).getString("Qty");
                    String PackageTypeCode =productListArray.getJSONObject(i).getString("PackageTypeCode");
                    String packageType = productListArray.getJSONObject(i).getString("PackageType");
                    String batch = productListArray.getJSONObject(i).getString("batch");

                    qtymap.put(prdid, Qty);
                    casemap.put(prdid, PackageTypeCode);
                    batchmap.put(prdid, batch);
                }

            }}
        catch(Exception e){
            e.printStackTrace();
        }
    }
    public void listPopulate()
    {
        productids = db.getallproductids(selectedCategory, selectedBrand,
                selectedForm, searchString, selection);

        Calendar C = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String CurrentDate = df.format(C.getTime()) + "000000";
        String cats = catid;
        productmap = db.getproductmap(selectedCategory, selectedBrand,
                selectedForm, searchString, selection, CurrentDate, cats,cuttype,"");
        HashSet<String> uniquepids = new HashSet<>();
        productids.clear();
        List<String> temppids = new ArrayList<>();
        for (String key : productmap.keySet()) {
            if (uniquepids.contains(key))
                continue;
            uniquepids.add(key);
            temppids.add(key);
        }

        for (String id : productids) {
            if (!uniquepids.contains(id)) {
                temppids.add(id);
            }
        }
        productids = temppids;
        Product[] Product = new Product[productmap.size()];
        int i = 0;
        for (String key : productmap.keySet()) {
            Product[i] = productmap.get(key);
            i++;
        }

    }
    public void settitle(Button title, Button one, Button two,
                         Button three) {
//        title.setBackgroundResource(R.drawable.cansel_corner);
        one.setBackgroundResource(R.drawable.filterbtn_border);
        two.setBackgroundResource(R.drawable.filterbtn_border);
        three.setBackgroundResource(R.drawable.filterbtn_border);
        title.setBackgroundColor(getResources().getColor(R.color.btnback));
    }
    public void itemlist1(View v)  {

                productListArray = new JSONArray();

                state = getResources().getString(R.string.inward);
                adapter.getItemViewType(4);
                try {
                    if (qtymap.isEmpty()) {
                        Toast.makeText(TabInward.this.getContext().getApplicationContext(), R.string.toastinward, Toast.LENGTH_LONG).show();
                    } else {
                        selectedCategory = "All";
                        selectedBrand = "All";
                        selectedForm = "All";
                        searchString = "";
                        Calendar C = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                        String CurrentDate = df.format(C.getTime()) + "000000";
                        String cats = catid;
                        productmap = db.getproductmap(selectedCategory, selectedBrand,
                                selectedForm, searchString, selection, CurrentDate, cats,
                                cuttype, "");

                        for (Map.Entry<String, String> entry : qtymap.entrySet()) {
                            String key = entry.getKey();
                            String value = entry.getValue();
                            if (Float.parseFloat(value) > 0) {
                                JSONObject values = new JSONObject();

                                try {
                                    values.put("Rate", productmap.get(key).rate);
                                } catch (Exception e) {
                                    values.put("Rate", "0");
                                }
                                try {
                                    values.put("MRP", productmap.get(key).mrp);
                                } catch (Exception e) {
                                    values.put("MRP", "");
                                }
                                try {
                                    values.put("Name", productmap.get(key).name);
                                } catch (Exception e) {
                                    values.put("Name", "");

                                }
                                //				productmap.get(productids.get(position)).name
                                try {
                                    values.put("ProductId", key);
                                } catch (Exception e) {
                                    values.put("ProductId", "");
                                }
                                try {
                                    values.put("Qty", value);
                                } catch (Exception e) {
                                    values.put("Qty", "");
                                }

						/*try{
						values.put("UOM", casemap.get(key));
						}
						catch(Exception e){
							values.put("UOM", "");
						}
						try{
						values.put("upc", units.get(casemap.get(key)));
						}
						catch(Exception e){
							values.put("upc","");
						}*/

                                try {
                                    values.put("batch", batchmap.get(key));
                                } catch (Exception e) {
                                    values.put("batch", "");
                                }

                                try {
                                    values.put("PackageTypeCode", casemap.get(key));
                                } catch (Exception e) {
                                    values.put("PackageType", "");
                                }
                                try {
                                    values.put("PackageType", packageTypeMap.get(casemap.get(key)));
                                } catch (Exception e) {
                                    values.put("PackageType", "");
                                }
                                //				values.put("delflag", false);
                                productListArray.put(values);
                            }
                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(getActivity(),
                        InwardOutwardListActivity.class);
                intent.putExtra("transfer", state);
                intent.putExtra("productList", productListArray.toString());
                getActivity().finish();
//        overridePendingTransition(0, 0);
                startActivity(intent);

    }
//    @Override
//    protected void onRestart() {
//        getActivity().finish();
//        startActivity(getActivity().getIntent());
//        getActivity().overridePendingTransition(0, 0);
//        super.onRestart();
//    }
}
