package dsale.dhi.com.offroute;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.UUID;

import dsale.dhi.com.ariesdsale.NavMapActivity;
import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.customer.Locations;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.Scheduledata;
import dsale.dhi.com.objects.Scheduledetail;

public class CustSearchOffRouteRecyclerViewAdapter extends RecyclerView.Adapter<CustSearchOffRouteRecyclerViewAdapter.ViewHolder>{
    private JSONArray data;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context context;
    private static DatabaseHandler db;

    // data is passed into the constructor
    public CustSearchOffRouteRecyclerViewAdapter(Context context, JSONArray data) {
        this.mInflater = LayoutInflater.from(context);
        this.data = data;
        this.context=context;
        db = new DatabaseHandler(context);
    }
    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view = mInflater.inflate(R.layout.recyclerview_row_custsrch_offroute, parent, false);
        return new ViewHolder(view);
    }
    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try{
            JSONObject dataobj = data.getJSONObject(position);
            String custname = dataobj.getString("custname");
            String custadress = dataobj.getString("adress");
            String customercode = dataobj.getString("custcode");
            String customercategory = dataobj.getString("custcategory");
            String invoicenumber = dataobj.optString("invoicestatus", "");
            String locationlock = dataobj.optString("locationlock", "0");
            String closingday = dataobj.optString("closingday", "");
            String mobile = dataobj.optString("mobile", "");
            String custtype = dataobj.optString("custtype", "");
            int custid = Integer.parseInt(dataobj.getString("custid"));
            double custlat = 0.0;
            double custlong = 0.0;
            try {
                custlat = Double.parseDouble(dataobj.getString("custlat"));
                custlong = Double.parseDouble(dataobj.getString("custlong"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.CustomerName.setText(custname);
            holder.CustomerAddress.setText(custadress);
            holder.Customercategory.setText(customercategory);
            holder.CustCode.setText(customercode);
            if (invoicenumber.equals("")) {
                holder.addOffRouteCust.setImageResource(R.drawable.addcustomerweb);
            } else {
                holder.addOffRouteCust.setImageResource(R.drawable.invoicedcustomer);
            }
        }catch(JSONException e2){
            e2.printStackTrace();
        }
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return data.length();
    }
    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView CustomerName;
        TextView CustomerAddress;
        TextView Customercategory;
        TextView CustCode;
        ImageView addOffRouteCust;
        RecyclerView srchCustRecyclerview;
        LinearLayout loadpopup;
        ImageView imgNavigation;

        ViewHolder(View itemView) {
            super(itemView);
            CustomerName = itemView.findViewById(R.id.CustomerName);
            CustomerAddress = itemView.findViewById(R.id.CustomerAddress);
            Customercategory = itemView.findViewById(R.id.Customercategory);
            CustCode = itemView.findViewById(R.id.CustCode);
            addOffRouteCust=itemView.findViewById(R.id.addOffRouteCust);
            srchCustRecyclerview=itemView.findViewById(R.id.srchCustRecyclerview);
            loadpopup=itemView.findViewById(R.id.custListRowLayout);
            imgNavigation=itemView.findViewById(R.id.imgNavigation);

            imgNavigation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        final int pos = getAdapterPosition();
                        //popup
                        String storename="";
                        String storeaddress="";
                        String Lat="";
                        String Longi="";
                        try{
                            JSONObject dataobj = data.getJSONObject(pos);
                            storename = dataobj.getString("custname");
                            storeaddress = dataobj.getString("adress");
                            Lat = dataobj.getString("custlat");
                            Longi = dataobj.getString("custlong");
                        }catch(JSONException e){
                            e.printStackTrace();
                        }

                        if (!Lat.toString().equals("")
                                && !Lat.toString().equals("0")
                                && !Lat.toString().equals("0.0")) {
                            Intent intent = new Intent(view.getContext(),
                                    NavMapActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("offroute", true);
                            intent.putExtra("Lat", Lat);
                            intent.putExtra("Long", Longi);
                            intent.putExtra("storename", storename);
                            intent.putExtra("storeaddress", storeaddress);
                            view.getContext().startActivity(intent);



                        } else {
                            Toast.makeText(view.getContext(),
                                    R.string.no_loc_data_found, Toast.LENGTH_LONG)
                                    .show();
                        }
                    } catch (Exception ex) {

                        Toast.makeText(view.getContext(), R.string.no_loc_data_found,
                                Toast.LENGTH_LONG).show();

                    }
                }
            });

            // add customer
            addOffRouteCust.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick (View v){
                    final int pos = getAdapterPosition();
                    //popup
                    String cName="";
                    String cAddr="";
                    try{
                        JSONObject dataobj = data.getJSONObject(pos);
                        cName = dataobj.getString("custname");
                        cAddr = dataobj.getString("adress");
                    }catch(JSONException e){
                        e.printStackTrace();
                    }

                    View customView = mInflater.inflate(R.layout.popuplayout, null);
                    Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                    Button confirm = (Button) customView.findViewById(R.id.yes);
                    TextView title = (TextView) customView.findViewById(R.id.header);
                    title.setText(R.string.addcustomer);
                    TextView message = (TextView) customView.findViewById(R.id.message);
                    message.setText(cName+" , "+cAddr);
                    //instantiate popup window
                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
                    final PopupWindow popupWindow = new PopupWindow(customView, width, height);
                    popupWindow.setFocusable(true);
                    //display the popup window
                    new Handler().postDelayed(new Runnable(){
                        public void run() {
                            popupWindow.showAtLocation(loadpopup, Gravity.CENTER, 0, 0);
                        }
                    }, 200L);

                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    //close the popup window on button click
                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                            return;
                        }
                    });
                    confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                            try {
                                Addlocation( pos);
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
        }
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void Addlocation(int position) throws JSONException {
        JSONObject dataobj = data.getJSONObject(position);
        String custname = dataobj.getString("custname");
        String custadress = dataobj.getString("adress");
        String customercode = dataobj.getString("custcode");
        String customercategory = dataobj.getString("custcategory");
        String invoicenumber = dataobj.optString("invoicestatus", "");
        String locationlock = dataobj.optString("locationlock", "0");
        String closingday = dataobj.optString("closingday", "");
        String mobile = dataobj.optString("mobile", "");
        String custtype = dataobj.optString("custtype", "");
        int custid = Integer.parseInt(dataobj.getString("custid"));
        double custlat = 0.0;
        double custlong = 0.0;
        try {
            custlat = Double.parseDouble(dataobj.getString("custlat"));
            custlong = Double.parseDouble(dataobj.getString("custlong"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        HashSet<String> ids = db.getcurrentlocationids();
        if (ids.contains(String.valueOf(custid))) {
            Toast.makeText(context, R.string.cust_already_added,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        String schdulineguid = UUID.randomUUID().toString();
        Locations loctodb = new Locations();
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String crtime = dateTimeFormat.format(c.getTime());
        loctodb.Locationname = custname;
        loctodb.Locationadress = custadress;
        loctodb.Latitude = String.valueOf(custlat);
        loctodb.Longitude = String.valueOf(custlong);
        loctodb.Locationid = String.valueOf(custid);
        loctodb.createtime = crtime;
        loctodb.costomercode = customercode;
        loctodb.customercategory = customercategory;
        loctodb.tinnumber = "";
        loctodb.schdulineguid = schdulineguid;
        // /add location to location table
        db.addnewlocation(loctodb);
        // adding to schedule
        String schedheadid = db.isnewlocationsexistwebsearch();
        if (schedheadid.equals("0")) {
            Calendar c2 = Calendar.getInstance();
            DateFormat dateTimeFormat2 = new SimpleDateFormat("MM/dd/yyyy");
            String schdate = dateTimeFormat2.format(c2.getTime());
            String schbegintime = db.getschedulebegintime();
            if (schbegintime == null || schbegintime == "") {
                Calendar ct = Calendar.getInstance();
                DateFormat dateTimeFormat1 = new SimpleDateFormat(
                        "yyyyMMddHHmmss");
                schbegintime = dateTimeFormat1.format(ct.getTime());
                db.updatescheduleheaderbegintime(schbegintime);
                db.intialisesynctable(schbegintime);
                SharedPreferences pref = context.getSharedPreferences(
                        "Config", context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("Schbegintime", schbegintime);
                editor.commit();
            }
            SharedPreferences pref = context.getSharedPreferences("Config",
                    context.MODE_PRIVATE);
            Scheduledata headerdatatodb = new Scheduledata();
            headerdatatodb.Scheduleid = UUID.randomUUID().toString();
            headerdatatodb.Schedulename = context.getResources().getString(R.string.off_route_cust_hdr);
            headerdatatodb.Salespersonid = pref.getString("personid", "0");
            headerdatatodb.salespersonname = pref.getString("personname", "");
            headerdatatodb.Routenetworkid = "0";
            headerdatatodb.Headerstatus = "";
            headerdatatodb.Scheduledate = schdate;
            headerdatatodb.begintime = schbegintime;

            Scheduledetail detail = new Scheduledetail();

            detail.Scheduledetailid = schdulineguid;
            detail.locationid = String.valueOf(custid);
            detail.locationname = custname;
            detail.locationadress = custadress;
            detail.sequencenumber = "1";
            detail.status = "pending";
            detail.latitude = String.valueOf(custlat);
            detail.longitude = String.valueOf(custlong);
            detail.Customercatogory = customercategory;
            String cat = customercategory;
            if (!cat.equals(""))
                detail.CustomercatogoryId = db.getcustomercategoriesId(cat);
            detail.invoicenumber = "";
            detail.picklistnumber = "";
            detail.customercode = customercode;
            detail.locationlock = locationlock;
            detail.Closingday = closingday;
            detail.mobilenumber = mobile;
            detail.custtype = custtype;
            db.firstlocationadd(headerdatatodb, detail);
            Toast.makeText(context, R.string.customer_added, Toast.LENGTH_SHORT)
                    .show();
        }
        else {
            Scheduledetail detail = new Scheduledetail();
            detail.Scheduledetailid = schdulineguid;
            detail.locationid = String.valueOf(custid);
            detail.locationname = custname;
            detail.locationadress = custadress;
            detail.sequencenumber = "1";
            detail.status = "pending";
            detail.latitude = String.valueOf(custlat);
            detail.longitude = String.valueOf(custlong);
            detail.Customercatogory = customercategory;
            String cat = customercategory;
            if (!cat.equals(""))
                detail.CustomercatogoryId = db.getcustomercategoriesId(cat);
            detail.invoicenumber = "";
            detail.picklistnumber = "";
            detail.customercode = customercode;
            detail.locationlock = locationlock;
            detail.Closingday = closingday;
            detail.mobilenumber = mobile;
            detail.custtype = custtype;
            db.addtoexistingnewlocations(schedheadid, detail);
            Toast.makeText(context, R.string.customer_added, Toast.LENGTH_SHORT)
                    .show();
        }
    }
}
