package dsale.dhi.com.ariesdsale;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import dsale.dhi.com.customer.EditCustomerActivity;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.gps.GPSAccuracy;
import dsale.dhi.com.gps.PlayTracker;
import dsale.dhi.com.invoice.InvoiceMakePaymentActivity;
import dsale.dhi.com.objects.DateError;
import dsale.dhi.com.objects.Global;
import dsale.dhi.com.objects.Stacklogger;
import dsale.dhi.com.objects.SupportFunctions;
import dsale.dhi.com.objects.webconfigurration;
import dsale.dhi.com.order.OrderTakingActivity;


public class ShopActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_CODE_GPS_ACCURACY = 0;
    private DatabaseHandler db;
    private SharedPreferences pref;
    private String scheduledetailid;
    private String status;
    private ImageView photo;
    private ProgressBar progress;
    private ImageView imgNavigationcustdtl4;
    private ImageView imgNavigationcustdtl2;
    private ImageView imgNavigationcustdtl3;

    private TextView orderamt;
    private TextView orderAmtTv;
    private TextView collected;
    private TextView ordercount;
   // private String Rupeessymbol = "\u20B9";
    private int itemchoosen = -1;
    private ImageView locationView;
    private  int btnstatus ;
    private  TextView reasonbtn ;
    private double latitude = 0;
    private double longitude = 0;

    private static final int REQUEST_CODE_DATE_SETTING = 0;
    private static final int CAMERA_REQUEST = 1888;
    private static Uri mImageUri;
    private Bitmap  photoBitmap;
    private ImageView thumbnail;
    private CustomerVisibility visibility ;
    private String visibilityId ;
    private LinearLayout thumbnailBorder ;
    private ImageView camera;
    private String currencyCode ;
    private String currencySymbol ;
    private int roundoffPrecision ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        imgNavigationcustdtl2=(ImageView)findViewById(R.id.imgNavigationcustdtl2);
        imgNavigationcustdtl2.setOnClickListener(this);
        imgNavigationcustdtl3=(ImageView)findViewById(R.id.imgNavigationcustdtl3);
        imgNavigationcustdtl3.setOnClickListener(this);
        imgNavigationcustdtl4=(ImageView)findViewById(R.id.imgNavigationcustdtl4);
        imgNavigationcustdtl4.setOnClickListener(this);
        reasonbtn=(TextView)findViewById(R.id.reasonbtn);
        db = new DatabaseHandler(getApplicationContext());
        pref = getApplicationContext()
                .getSharedPreferences("Config", MODE_PRIVATE);
        Bundle bundle = getIntent().getExtras();
       /* String currencyType=pref.getString("currencyType","");
        currencyType=currencyType.toUpperCase();
        if(!currencyType.equalsIgnoreCase("inr"))
            Rupeessymbol=currencyType+": ";*/

        currencyCode=pref.getString("CurrencyCode","");
        currencySymbol =pref.getString("CurrencySymbol","");
        roundoffPrecision = pref.getInt("RoundOffPrecision",0);

        scheduledetailid = (String) bundle.get("scheduledetailid");
        photo = (ImageView) findViewById(R.id.photo);
        progress = (ProgressBar) findViewById(R.id.progress);

        /*
        Photo Visibility
         */
        camera=(ImageView) findViewById(R.id.visibilityPhoto);
        thumbnail =(ImageView) findViewById(R.id.thumbnail);
        thumbnailBorder = (LinearLayout) findViewById(R.id.thumbnailBorder);
        try {
            setvalues();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //append order dtls
        ordercount = (TextView) findViewById(R.id.ordercount);
        orderamt = (TextView) findViewById(R.id.orderamt);
        orderAmtTv=(TextView) findViewById(R.id.orderamtTV);
        collected = (TextView) findViewById(R.id.collected);
        try {
            JSONObject summary = db.getCustomerSummary(scheduledetailid);
            String orderid = db.getorderid(scheduledetailid);
        /*    double taxableAmnt = db.gettaxableamnt(orderid);
            double taxAmnt = db.gettaxtot(orderid);
            double ordAmnt = taxableAmnt+taxAmnt ;
            ordAmnt = Math.round(ordAmnt);*/
            BigDecimal taxableAmnt = db.getTotalTaxableAmnt(orderid, 5);
            BigDecimal taxAmnt = db.getTotalTaxAmount(orderid, 5);
            BigDecimal ordAmnt = taxableAmnt.add(taxAmnt);
            if (roundoffPrecision == 0){
                ordAmnt = ordAmnt.setScale(2,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
            }

            ordAmnt = ordAmnt.setScale(  roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/) ;
            //orderamt.setText(Rupeessymbol + ordAmnt);
            orderamt.setText(""+ ordAmnt );
            orderAmtTv.setText(currencyCode+": ");
            ordercount.setText(summary.optString("ordercount", "0")+" NOS");
            collected.setText(currencySymbol + summary.optString("collected", "0"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //check gps on
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!statusOfGPS) {

            AlertDialog.Builder builder = new AlertDialog.Builder(
                    ShopActivity.this);
            builder.setTitle(R.string.switch_on_gps);
            builder.setMessage(R.string.gps_off_alert_message);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            startActivityForResult(
                                    new Intent(
                                            Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                    0);
                        }
                    });
            builder.show();
            return;
        }

        // location color
        locationView = (ImageView) findViewById(R.id.imgNavcustdt);
        btnstatus = 0;
        try {
            btnstatus = choosebuttonstatus();
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

       setLocationButtonColor();


        visibilityId = db.getCustomerVisibilityId(scheduledetailid);

        if (!visibilityId.equals("0") ){
            visibility = db.getCustomerVisibilityPhoto(visibilityId);

            File rootsd = Environment.getExternalStorageDirectory();
            File direct = new File(rootsd.getAbsolutePath() + "/Aries");
            String filename = visibility.photoUUID + ".png";
            File file = new File(direct, filename);
            photoBitmap= BitmapFactory.decodeFile(file.getAbsolutePath());
            if (photoBitmap != null) {
                thumbnail.setImageBitmap(photoBitmap);
                thumbnailBorder.setVisibility(View.VISIBLE);
            }

           // camera.setEnabled(false);

        }


    }

    private void setLocationButtonColor() {

        if (btnstatus == 0) {
            locationView.setImageResource(R.drawable.nolatlongfound);
        }
        if (btnstatus == 1) {
            locationView.setImageResource(R.drawable.locationerrorinner);
        }
        if (btnstatus == 2) {
            locationView.setImageResource(R.drawable.locationcurrectinner);
        }
        if (btnstatus == 3) {
            locationView.setImageResource(R.drawable.locationcorrect);
        }

        if (btnstatus == 4) {
            locationView.setImageResource(R.drawable.locationerrorinner_locked);
        }
        if (btnstatus == 5) {
            locationView.setImageResource(R.drawable.locationcurrectinner_locked);
        }
    }

    public void setvalues() throws Exception {
        JSONObject cust = db.getallcustdata(scheduledetailid);
        ((TextView) findViewById(R.id.storename)).setText(cust.optString("CustName", ""));
        ((TextView) findViewById(R.id.custcode)).setText(cust.optString("CustCode", ""));
        ((TextView) findViewById(R.id.phone)).setText(cust.optString("CustPhone", ""));
        ((TextView) findViewById(R.id.address)).setText(cust.optString("CustAddress", ""));
        ((TextView) findViewById(R.id.custcat)).setText(cust.optString("CustCat", ""));
        String photouuid = cust.optString("PhotoUUID", "");
        /*String orderid=db.getorderid(scheduledetailid);
        Boolean approveOrder=false;
        Boolean approve=false;
        if(!(orderid.equalsIgnoreCase("")))
        {
            approveOrder= db.isorderapproved(orderid);
        }
        approve=db.isPaymentApproved(scheduledetailid);
        String custstatus = cust.optString("status", "");
        if (approve || approveOrder) {
            reasonbtn.setClickable(false);
        } else if (custstatus.equals("Completed")) {
            reasonbtn.setClickable(false);
        } else {
            reasonbtn.setClickable(true);
        }*/

        if (!photouuid.contentEquals("")) {
            File rootsd = Environment.getExternalStorageDirectory();
            File direct = new File(rootsd.getAbsolutePath() + "/Aries");
            String filename = "photo_" + photouuid + ".png";
            File file = new File(direct, filename);
            Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
            if (bmp != null) {
                photo.setImageBitmap(bmp);

            } else {
                photo.setImageResource(R.drawable.ic_dhi_photodownload);
                photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            searchForImage();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    private void searchForImage() throws Exception {
        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = new JSONObject(inputParamsStr);

        JSONObject updatestatus = null;
        try {
            webconfigurration.status = "ImageDownload";
            updatestatus = SupportFunctions.getstatusobject(getApplicationContext(), "ImageDownload", pref.getString("personid", ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputParamObj.put("updatestatus", updatestatus);
        JSONObject data = new JSONObject();
        data.put("operation", "DownloadImages")
                .put("slineid", scheduledetailid);
        inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        inputParamObj.getJSONObject("data")
                .put("personid", pref.getString("personid", ""))
                .put("PersonOrg", pref.getString("tmsOrgId", ""));
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        inputParamObj.put("auth", auth);
        new ImageDownloadOperation().execute(inputParamObj);
    }


    private class ImageDownloadOperation extends
            AsyncTask<JSONObject, Void, String> {
        protected void onPreExecute() {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    progress.setVisibility(View.VISIBLE);
                    photo.setVisibility(View.GONE);
                }
            });
        }

        ;

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.url;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setConnectTimeout(100000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(200000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responsefrom;
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
            Global.onSync = false;
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Log.e("reply", result);
                        JSONObject reply = new JSONObject(result);
                        if (reply.getString("Status").contentEquals("Success")) {
                            String photoString = reply.getString("photo");

                            byte[] decodedString = Base64.decode(photoString,
                                    Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(
                                    decodedString, 0, decodedString.length);
                            photo.setImageBitmap(decodedByte);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        progress.setVisibility(View.GONE);
                        photo.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgNavigationcustdtl4:
                Intent i =new Intent(ShopActivity.this, OrderTakingActivity.class);
                i.putExtra("complimentary",false);
                i.putExtra("parentInvoiceNo","");
                i.putExtra("scheduledetailid",scheduledetailid);
                finish();
                startActivity(i);
                overridePendingTransition(0,0);
                break;
            case R.id.imgNavigationcustdtl2:
                i =new Intent(ShopActivity.this, InvoiceMakePaymentActivity.class);
                i.putExtra("complimentary",false);
                i.putExtra("parentInvoiceNo","");
                i.putExtra("scheduledetailid",scheduledetailid);
                finish();
                overridePendingTransition(0,0);
                startActivity(i);
                break;
            case R.id.imgNavigationcustdtl3:
                i =new Intent(ShopActivity.this, EditCustomerActivity.class);
                i.putExtra("complimentary",false);
                i.putExtra("parentInvoiceNo","");
                i.putExtra("scheduledetailid",scheduledetailid);
                finish();
                startActivity(i);
                overridePendingTransition(0,0);
                break;

        }
    }

    @Override
    protected void onRestart() {
        finish();
        Intent intent = getIntent();
        intent.putExtra("scheduledetailid", scheduledetailid);
        intent.putExtra("status", status);
        // finish();
        startActivity(intent);
        overridePendingTransition(0, 0);
        super.onRestart();
    }

    public void updateStatus(View v){
        JSONArray resondat = new JSONArray();
        try {
            resondat = db.getreasons();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String[] reasons = new String[resondat.length()];
        final String[] reasoncode = new String[resondat.length()];
        String reasoncode_chooseen = db.getreasoncodechoosen(scheduledetailid);
        for (int i = 0; i < resondat.length(); i++) {
            JSONObject resonobj;
            try {
                resonobj = resondat.getJSONObject(i);
                reasons[i] = resonobj.getString("resondetail");
                reasoncode[i] = resonobj.getString("reasoncode");
                if (reasoncode_chooseen != null
                        && reasoncode_chooseen.equals(resonobj
                        .getString("reasoncode"))) {
                    itemchoosen = i;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder( ShopActivity.this);
        builder.setTitle(R.string.Choose_Reason);
        builder.setSingleChoiceItems(reasons, itemchoosen,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        itemchoosen = which;
                    }
                });
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {

                        if (itemchoosen == -1) {
                            Toast.makeText(ShopActivity.this,R.string.Please_choose_reason,Toast.LENGTH_SHORT).show();
                            builder.show();
                            return;
                        }
                        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                                "Config", MODE_PRIVATE);
                        String resonschoosen = reasoncode[itemchoosen];
                        resonschoosen = reasoncode[itemchoosen];
                        dialog.dismiss();


                        Calendar c1 = Calendar.getInstance();
                        DateFormat dateTimeFormat2 = new SimpleDateFormat(
                                "yyyyMMddHHmmss");

                        DateFormat dateTimeFormat_payment = new SimpleDateFormat(
                                "dd/MM/yyyy HH:mm:ss");

                        String createtime = dateTimeFormat2.format(c1
                                .getTime());
                        String paymentdate = dateTimeFormat_payment
                                .format(c1.getTime());

                        //GPS
                        Location loc =null;
                        final double lat ;
                        final double lng ;
                        final double latreson ;
                        final double lngreson ;
                        if (PlayTracker.getLastLocation() != null) {
                            loc = PlayTracker.getLastLocation();
                            lat =  loc.getLatitude();
                            lng = loc.getLongitude();
                            latreson =  loc.getLatitude();
                            lngreson = loc.getLongitude();
                        }
                        else
                        {
                            lat=0;
                            lng=0;
                            lngreson=0;
                            latreson=0;
                        }

                        db.updatenovisitstatus(scheduledetailid, resonschoosen,
                                paymentdate, createtime,
                                String.valueOf(lat),
                                String.valueOf(lng),
                                String.valueOf(latreson),
                                String.valueOf(lngreson));
                        try {
                            db.updateschedulestatus(scheduledetailid, "status");
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        finish();
                        overridePendingTransition(0, 0);
                        Intent intent =  getIntent();
                        startActivity(intent);
                    }
                });
        builder.setNeutralButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {

                        itemchoosen = -1;
                        dialog.dismiss();

                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public int choosebuttonstatus() throws JSONException {

        Location tracker = PlayTracker.getLastLocation();
        double latitudeCrnt_live = tracker.getLatitude();
        double longitudeCrnt_live = tracker.getLongitude();

        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        JSONObject latlong = db.getupdatedlatlong(scheduledetailid);
        String locstatus = db.getlocakstatus(scheduledetailid);
        double latcurrent = 0;
        double loncurrent = 0;

        if (latlong.has("latitude_current")
                && !latlong.getString("latitude_current").equals("")) {
            latcurrent = Double.parseDouble(latlong
                    .getString("latitude_current"));
            loncurrent = Double.parseDouble(latlong
                    .getString("longitude_current"));
        }
        double latnew = Double.parseDouble(latlong.getString("latitude_new"));

        latitude = latcurrent;
        longitude = loncurrent;

        if (latcurrent == 0 && latnew == 0) {
            return 0;
        }
        if (latnew != 0) {
            return 3;
        }

        float distance = distance(latcurrent, loncurrent, latitudeCrnt_live,
                longitudeCrnt_live);
        if (distance > 70) {

            if (locstatus.equals("1"))
                return 4;
            else
                return 1;

        } else {
            if (locstatus.equals("1"))
                return 5;
            else
                return 2;
        }


    }

    public static float distance(double lat1, double lng1, double lat2,
                                 double lng2) {
        double earthRadius = 6371000; // meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

    public void updateLocation(View v){

        if (!PlayTracker.intime()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ShopActivity.this);
            builder.setTitle(R.string.gps_unavailable);
            builder.setMessage(R.string.check_your_gps);

            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

            AlertDialog dialog = builder.create();
            builder.show();

            return;
        }
        if (locationaccuracy()!=3) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ShopActivity.this);
            builder.setTitle(R.string.enable_high_loc_accuracy);
            builder.setMessage(R.string.loc_acc_low);

            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    });

            builder.show();

            return;
        }

        Intent intent = new Intent(ShopActivity.this,
                GPSAccuracy.class);
        intent.putExtra("operation", "addlocation");
        startActivityForResult(intent, REQUEST_CODE_GPS_ACCURACY);
        overridePendingTransition(0, 0);

    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Intent i=new Intent(ShopActivity.this, CustomerListActivity.class);
        i.putExtra("scheduledetailid", scheduledetailid);
        finish();
        overridePendingTransition(0,0);
        startActivity(i);

    }

    private int locationaccuracy(){
        int locationMode=-1;
        try {
            locationMode = Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {

            e.printStackTrace();
        }
        return locationMode;
    }

    public void updatelalong(double latitude ,double longitude) {

        Calendar c = Calendar.getInstance();

        DateFormat dateTimeFormat = new SimpleDateFormat(
                "yyyyMMddHHmmss", Locale.US);

        DateFormat dateTimeFormat_readable = new SimpleDateFormat(
                "dd/MM/yyyy HH:mm:ss", Locale.US);
        String createttime = dateTimeFormat.format(c
                .getTime());
        String readabletime = dateTimeFormat_readable
                .format(c.getTime());



        DatabaseHandler db = new DatabaseHandler(
                getApplicationContext());
        db.updatelatlongcoustomertable(
                scheduledetailid, latitude+"", longitude+"",
                createttime, readabletime);
        Toast.makeText(getApplicationContext(),
                "Latlong updated", Toast.LENGTH_SHORT)
                .show();

        btnstatus = 0;
        try {
            btnstatus = choosebuttonstatus();
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        setLocationButtonColor();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_GPS_ACCURACY){

            if (resultCode == Activity.RESULT_OK) {
                Double latitude = data.getDoubleExtra("latit", 0);
                Double longitude = data.getDoubleExtra("longit", 0);
                Float accuracy = data.getFloatExtra("minacc", 0);
                String provider = data.getStringExtra("prov");
                String operation = data.getStringExtra("operation");

                if (latitude > 0 && longitude > 0)
                    updatelalong(latitude, longitude);
                else if (!webconfigurration.GPSMandatory){
                    updatelalong(latitude, longitude );
                }else
                    Toast.makeText(ShopActivity.this,
                            R.string.location_not_found,Toast.LENGTH_SHORT).show();
            }
            if (locationaccuracy()!=3) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ShopActivity.this);
                builder.setTitle(R.string.enable_high_loc_accuracy);
                builder.setMessage(R.string.loc_acc_low);


                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        });

                builder.show();

                return;
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(ShopActivity.this,
                        R.string.cancelled,Toast.LENGTH_SHORT).show();


            }
        }else  if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {

            Bundle extras =data.getExtras();
            Bitmap mImageBitmap=(Bitmap)extras.get("data");

            photoBitmap=getResizedBitmap(mImageBitmap,500,500);
            thumbnail.setImageBitmap(photoBitmap);
            thumbnailBorder.setVisibility(View.VISIBLE);

            saveVisibilityPhoto();



        }else if (requestCode == REQUEST_CODE_DATE_SETTING) {
            finish();
            startActivity(getIntent());
            overridePendingTransition(0, 0);
        }
    }


    /**
     * Option to Capture Visibility Photo during each visit.
     * Coded by :Reny
     * Done on :03-06-2019
     *
     */


    public void saveVisibilityPhoto() {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(
                "yyyyMMddHHmmss");
        String createTime = df.format(c.getTime());

        if (!visibilityId.equals("0") ){
            db.deleteVisibilityPhoto(visibilityId,createTime);
        }


        String uuid = "visibility_" + UUID.randomUUID().toString();
        if (photoBitmap != null) {
            String filename = uuid + ".png";
            createDirectoryAndsavetempfile(photoBitmap, filename);


            visibility = new CustomerVisibility(createTime, scheduledetailid,
                    uuid, "0", "0", "0");
            try {
                db.saveVisibilityPhoto(visibility);

                finish();
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            } catch (DateError e) {
                callDateTimeSetting();
            }

        }
/*

        if (photoBitmap == null) {
            Toast.makeText(getApplicationContext(),"Please retake photo !",Toast.LENGTH_LONG).show();
        }else {

            AlertDialog.Builder builder = new AlertDialog.Builder(
                    ShopActivity.this);
            builder.setTitle(R.string.savephoto_confirmation_title);
            builder.setMessage(R.string.savephoto_confirmation_message);
            builder.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            String uuid = "visibility_" + UUID.randomUUID().toString();
                            if (photoBitmap != null) {
                                String filename = uuid + ".png";
                                createDirectoryAndsavetempfile(photoBitmap, filename);

                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat df = new SimpleDateFormat(
                                        "yyyyMMddHHmmss");
                                String createTime = df.format(c.getTime());
                                visibility = new CustomerVisibility(createTime, scheduledetailid,
                                        uuid, "0", "0", "0");
                                try {
                                    db.saveVisibilityPhoto(visibility);

                                } catch (DateError e) {
                                    callDateTimeSetting();
                                }
                              */
/*  Intent i = new Intent(ShopActivity.this, ShopActivity.class);
                                i.putExtra("scheduledetailid",scheduledetailid);*//*

                             */
/*   finish();
                                startActivity(getIntent());
                                overridePendingTransition(0, 0);*//*

                            }

                        }
                    });
            builder.setNeutralButton("CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.show();

        }

*/

    }

    public void cancelVisibilityPhoto(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                ShopActivity.this);
        builder.setTitle(R.string.deletephoto_confirmation_title);
        builder.setMessage("");
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                        /*   Intent i=new Intent(VisibilityPhotoActivity.this, ShopActivity.class);*/
                        finish();
                        startActivity(getIntent());
                        overridePendingTransition(0,0);
                    }
                });
        builder.setNeutralButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    public void takePhoto(View view){

        if (ContextCompat.checkSelfPermission(ShopActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(ShopActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},CAMERA_REQUEST);
        }
        else
        {

            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            File photo;
            try {
                photo = createTemporaryFile("picture", ".jpg");
                photo.delete();

            } catch (Exception e) {
                Toast.makeText(ShopActivity.this, R.string.shot_is_impossible,
                        Toast.LENGTH_SHORT).show();
                return;
            }
            mImageUri = Uri.fromFile(photo);
            startActivityForResult(intent, CAMERA_REQUEST);

        }
    }

    private File createTemporaryFile(String part, String ext) throws Exception {

        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }


    private void createDirectoryAndsavetempfile(Bitmap imageToSave,
                                                String fileName) {

        File rootsd = Environment.getExternalStorageDirectory();
        File direct = new File(rootsd.getAbsolutePath() + "/Aries");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        File file = new File(direct, fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 75, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }

    private void callDateTimeSetting() {
        Toast.makeText(getApplicationContext(),
                R.string.correct_mob_date_time, Toast.LENGTH_SHORT)
                .show();
        Intent dateSetttingIntent = new Intent(android.provider.Settings.ACTION_DATE_SETTINGS);
        startActivityForResult(dateSetttingIntent, REQUEST_CODE_DATE_SETTING);
    }


}
