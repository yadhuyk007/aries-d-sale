package dsale.dhi.com.order;

import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.content.Context;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dsale.dhi.com.ariesdsale.Product;
import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.database.DatabaseHandler;


public class OrderTakingItemListAdapter extends RecyclerView.Adapter<OrderTakingItemListAdapter.ViewHolder> {


    private ItemClickListener mClickListener;
    Context context;
    private HashMap<String,Product> dataHashMap;
    private HashMap<String, OrderLine> savedData ;
    // List<String> keyList;

    private HashMap<Integer,String> productIdMap ;
    List<String> packageTypeList ;
    private SharedPreferences pref;
    private ItemFocusChangedListener itemFocusChangedListener ;
    String invoiceno ;
    private String scheduleLineId ;
    private DatabaseHandler db ;
    private  boolean compliment ;

    public OrderTakingItemListAdapter(Context context, HashMap<String,Product> dataHashMap,
                                      HashMap<String, OrderLine> savedData, List<String> packageTypeList,
                                      ItemClickListener itemClickListener,SharedPreferences pref,
            /* ItemFocusChangedListener focusChangedListener,*/String invoiceno,String scheduleLineId,boolean compliment){
        this.dataHashMap = dataHashMap ;
        this.context =context;
        this.savedData =savedData ;
        productIdMap = new HashMap<>();

        this.mClickListener = itemClickListener ;

        this.packageTypeList =packageTypeList;
        this.pref = pref ;
        //  this.itemFocusChangedListener =focusChangedListener ;
        this.invoiceno = invoiceno;
        this.scheduleLineId =scheduleLineId;
        this.compliment =compliment;
        db = new DatabaseHandler(context);
        insertProductIdMap () ;
    }

    private void insertProductIdMap() {


        int position = 0;
        for (Map.Entry<String, Product> entry : dataHashMap.entrySet()) {

            String productId = entry.getKey();

            productIdMap.put(position,productId);

            position++;
        }


    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem= layoutInflater.inflate(R.layout.adapter_order_taking_list, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

//        String productId = keyList.get(position);
        String productId = productIdMap.get(position);
        Product product = dataHashMap.get(productId);

        viewHolder.productName.setText(product.name );
        viewHolder.mrp.setText(context.getResources().getString(R.string.MRP_with_colon)+product.mrp);
        viewHolder.rate.setText(context.getResources().getString(R.string.Rate_with_colon)+product.rate);
        viewHolder.qtyEditText.setTag(productId);
        viewHolder.spinner.setTag(productId);


        //  DatabaseHandler db = new DatabaseHandler(context);


        OrderLine orderLine = savedData.get(productId);

        if (compliment){

            orderLine = savedData.get(productId+"_C");

        }

        double inventory = db.getavailableqty( productId ,pref.getString("tmsOrgId", ""),invoiceno);
        // viewHolder.mrp.setText("inventory : "+inventory);
        //  Toast.makeText(context,"Product Id :"+viewHolder.qtyEditText.getTag(),Toast.LENGTH_LONG).show();

        if (inventory <= 0 && (!db.isWithoutInventory())) {
            viewHolder.qtyEditText.setEnabled(false);
            //  viewHolder.qtyEditText.setBackgroundColor(Color.GRAY) ;
            viewHolder.qtyEditText.setBackgroundResource(R.color.light_gray);
        }else{
            viewHolder.qtyEditText.setEnabled(true);
            viewHolder.qtyEditText.setBackgroundResource(R.drawable.searchbox_border);
        }




        if(orderLine != null && (orderLine.getQty() >0) ) {
            double qty = orderLine.getQty();
            String packageType = orderLine.getPackageTypeCode();
            int pos = packageTypeList.indexOf(packageType);

            viewHolder.qtyEditText.setText(qty+"");
            viewHolder.spinner.setSelection(pos);
        }else{
            int pos = 0;
            if (packageTypeList.contains("PCS")) {
                pos = packageTypeList.indexOf("PCS");
            }
            viewHolder.qtyEditText.setText( "");
            viewHolder.spinner.setSelection(pos);
        }
    }

    @Override
    public int getItemCount() {

        return dataHashMap.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout itemLayout;
        TextView productName;
        TextView mrp ;
        TextView rate;
        Spinner spinner;
        EditText qtyEditText ;


        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.itemName);
            mrp = itemView.findViewById(R.id.itemMRP);
            rate = itemView.findViewById(R.id.itemRate);
            spinner = itemView.findViewById(R.id.packageTypeDropdown);

            itemLayout =(LinearLayout)itemView.findViewById(R.id.containerItem);
            itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onRecyclerViewItemClick(itemView,getItem(getAdapterPosition()),getAdapterPosition());
                }
            });

            SpinnerAdapter adapter =  new ArrayAdapter<String>(context,R.layout.spinner_additem,packageTypeList);
            spinner.setAdapter(adapter);

            qtyEditText = itemView.findViewById(R.id.qtyEditText);


            qtyEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {



                }

                @Override
                public void afterTextChanged(final Editable s) {





                    String productId = qtyEditText.getTag().toString();
                    double qty =0 ;
                    try{
                        if (s.length() == 0){
                            qty = 0;
                        }else {
                            qty = Double.parseDouble(s.toString());
                        }
                    }catch (Exception pe){

                        pe.printStackTrace();
                    }
                    if (qty >= 0) {
                        String packageType = spinner.getSelectedItem().toString();


                        addToHashMap(productId, packageType, qty);


                    }

                    if(context instanceof OrderTakingActivity){

                        ((OrderTakingActivity)context ).setSummaryText(savedData);
                    }




                }
            });


            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {


                    String productId = spinner.getTag().toString();
                    String packageType = spinner.getSelectedItem().toString();

                    OrderLine line = savedData.get(productId);
                    if (line != null) {
                        line.setPackageTypeCode(packageType);

                        if (compliment){
                            savedData.put(productId+"_C", line);
                        }else{
                            savedData.put(productId, line);
                        }



                           /* Toast.makeText(context,"PackageType :"+packageType,Toast.LENGTH_LONG).show();
                            Toast.makeText(context,"jsonObject :"+jsonObject.toString(),Toast.LENGTH_LONG).show();*/
                    }




                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {

                }
            });
        }
    }

    private void removeFromHashMap(String productId) {

        if (compliment){
            savedData.remove(productId+"_C");
        }else{
            savedData.remove(productId);
        }


    }

    private Product getItem(int position) {
        String productId = productIdMap.get(position);
        Product product = dataHashMap.get(productId);

        return product;
    }

    private void addToHashMap(String productId,String packageType,double qty) {


        Product product = dataHashMap.get(productId);
        String prodName = product.name ;



        OrderLine line = new OrderLine();
        line.setProductId(Integer.parseInt(productId));
        line.setQty(qty);
        line.setPackageTypeCode(packageType);
        line.setProductName(prodName);
        line.setRate(new BigDecimal(product.rate));
        // line.setTaxId(Long.parseLong(product.tax));

        if (compliment){
            line.setComplimentFlag(true);
            savedData.put(productId+"_C",line   );
        }else {
            line.setComplimentFlag(false);
            savedData.put(productId,line   );
        }


        // Toast.makeText(context,"rate :"+product.rate,Toast.LENGTH_LONG).show();
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onRecyclerViewItemClick(View view, Product product, int position);
    }

    public HashMap<String,OrderLine> getSavedData(){

        return savedData ;
    }

    public interface ItemFocusChangedListener{

        void onRecyclerViewFocusChangedListener(View v, Product product, int position);
    }

    void setFocusChangedListener(ItemFocusChangedListener focusChangedListener) {
        this.itemFocusChangedListener = focusChangedListener;
    }
}
