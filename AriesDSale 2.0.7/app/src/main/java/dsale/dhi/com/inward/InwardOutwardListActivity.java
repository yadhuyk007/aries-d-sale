package dsale.dhi.com.inward;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.WriteToFile;
import dsale.dhi.com.objects.webconfigurration;

public class InwardOutwardListActivity extends AppCompatActivity  {
    PopupWindow popupWindow;
    LinearLayout linearLayout1;
    ImageView img;
    private String productlist;
    private TextView nos;
    private TextView inward_outward;
    private Bundle bundle;
    HashMap<String, JSONObject> itemmap;
    private JSONArray productListArray ;
    private String prdid;
    private JSONObject productobj;
    private HashMap<String, String> batchmap;
    private DatabaseHandler db;
    private LinearLayout listcontent;
    private LayoutInflater inflater;
    TableLayout list;
    private TextView qtytxt;
    private double qtyval;
    private boolean approved=false;
    private Button listapprove;
    private Button deletepayment;
    private String state;
    private SharedPreferences pref;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inward_outward_list);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        nos = (TextView) findViewById(R.id.nos);
        inward_outward= (TextView) findViewById(R.id.inward_outward);
        listcontent = (LinearLayout) findViewById(R.id.listcontent);
        listapprove= (Button) findViewById(R.id.listapprove);
        deletepayment =(Button) findViewById(R.id.cancellist);
        inflater = getLayoutInflater();
        Intent inward=getIntent();
        state = inward.getStringExtra("transfer");
        inward_outward.setText(state);
        bundle = getIntent().getExtras();
        productlist = bundle.getString("productList");
        try {
            itemmap = converttohashmap(productlist);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        db = new DatabaseHandler(getApplicationContext());
        batchmap = new HashMap<>();
        batchmap = db.getBatches();
        try {
            listPopulate();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        listapprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemlistApprove();

        }
        });
        deletepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemlistDelete();
            }
        });
        }


    public HashMap<String, JSONObject> converttohashmap(String jsonstring) throws JSONException{


        itemmap = new HashMap<>();

        try{
            productListArray= new JSONArray(jsonstring);

            for ( int i=0;i<productListArray.length();i++) {
                prdid = productListArray.getJSONObject(i).getString("ProductId");
                productobj=productListArray.getJSONObject(i);


                itemmap.put(prdid, productobj);
            }}
        catch(Exception e){
            e.printStackTrace();
        }

        return itemmap;

    }

    public void listPopulate() throws JSONException {
        listcontent.removeAllViews();

        for (Map.Entry<String, JSONObject> entry : itemmap.entrySet()) {
            final String key = entry.getKey();
            final JSONObject value = entry.getValue();


            final String prdname=value.getString("Name");
            final String batch=value.getString("batch");

            final String qty=value.getString("Qty");
            final String mrp=value.getString("MRP");
            final String rate = value.getString("Rate");
			/*final String uom=value.getString("UOM");
			final String upcstr=value.getString("upc");
			final double upc = db.getupcofprod(prdid, upcstr);*/
            final String packageType=value.getString("PackageType");

            final View child = inflater.inflate(R.layout.inwardoutward_itemlist_adapter, null);
            TextView prdtxt = (TextView) child.findViewById(R.id.prdname);
            prdtxt.setText(prdname);
            qtytxt = (TextView) child.findViewById(R.id.prdqty);
            qtyval = 0;
            qtyval = Double.parseDouble(qty) ;
//			qtyval = Double.parseDouble(qty) * upc;
            qtytxt.setText(String.valueOf(qtyval));
            TextView mrptxt = (TextView) child.findViewById(R.id.prdmrp);
            mrptxt.setText(rate); //Changes to Rate. Done by Reny..

            TextView batchtxt = (TextView) child.findViewById(R.id.batch);
            batchtxt.setText(batch);
            final String packageTypeName = db.getPackageTypeCode(packageType);

            TextView uomtxt = (TextView) child.findViewById(R.id.uom);
//			uomtxt.setText(uom);
            uomtxt.setText(packageTypeName);


            child.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (approved==true)
                        child.setEnabled(false);
                    else{

//                        final TableLayout tab  =(TableLayout) findViewById(R.id.Inward_list);
                        final LinearLayout tab =(LinearLayout) findViewById(R.id.listcontent);
                        LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                        final View customView = layoutInflater.inflate(R.layout.edit_inwod_popup, null);
                        Button closePopupBtn = (Button) customView.findViewById(R.id.Delete);
                        Button confirm = (Button) customView.findViewById(R.id.yes);
                        ImageButton close =(ImageButton) customView.findViewById(R.id.closebtn);
                        int width = LinearLayout.LayoutParams.MATCH_PARENT;
                        int height = LinearLayout.LayoutParams.MATCH_PARENT;
                        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
                        popupWindow.setFocusable(true);
                        popupWindow.showAtLocation(tab, Gravity.CENTER, 0, 0);
                        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
//                        popupWindow.setOutsideTouchable(true);
//                        popupWindow .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                        TextView txt = (TextView) customView
                                .findViewById(R.id.prdtname);
                        txt.setText(prdname);
                        EditText txtt = (EditText) customView .findViewById(R.id.editqty);
                        ((TextView) customView.findViewById(R.id.Qty))
                                .setText(getResources().getString(R.string.qty)+" ("+getResources().getString(R.string.packagetype)+  packageTypeName  + ")");
                        txtt.setText(qty);

                        closePopupBtn.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                //							String productid = prdid;
                                //							Calendar c = Calendar.getInstance();
                                //							SimpleDateFormat df = new SimpleDateFormat(
                                //									"yyyyMMddHHmmss");
                                //							String createdtime = df.format(c.getTime());
                                ////							db.deleteproduct(orderid, key, createdtime);
                                //				int index=productlist.indexOf(prdid);
                                listcontent.removeView(child);//At(i);
                                itemmap.remove(key);
                                nos.setText(String.valueOf(itemmap.size()) + getResources().getString(R.string.nos));

                                //				Global.Toast(InwardOutwardItemList.this, itemmap.toString(),
                                //						Toast.LENGTH_SHORT, Font.Regular);
                                popupWindow.dismiss();
                                //							finish();
                                //							startActivity(getIntent());
                                //							overridePendingTransition(0, 0);
                            }
                        });
                        close.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                popupWindow.dismiss();
                            }
                        });
                        confirm.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {

                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        itemmap.remove(key);
                                        EditText txt = (EditText) customView .findViewById(R.id.editqty);
                                        String quantity = txt.getText().toString();
                                        quantity = quantity.contentEquals("") ? "0"
                                                : quantity;
                                        try {
                                            value.put("Qty", quantity);

                                            itemmap.put(key, value);

                                            listPopulate();
                                        } catch (JSONException e) {

                                            e.printStackTrace();
                                        }

                                        //									TextView t = (TextView) child.findViewById(R.id.prdqty);
                                        //									double childqtyval = 0;
                                        //									childqtyval = Double.parseDouble(quantity) * upc;
                                        //									t.setText(String.valueOf(childqtyval));
                                        //
                                        //										Global.Toast(InwardOutwardItemList.this, itemmap.toString(),
                                        //												Toast.LENGTH_SHORT, Font.Regular);

                                        //									String productid = prdid;
                                        //									Calendar c = Calendar.getInstance();
                                        //									SimpleDateFormat df = new SimpleDateFormat(
                                        //											"yyyyMMddHHmmss");
                                        //									String createdtime = df.format(c.getTime());
                                        ////									db.updateproduct(orderid, key, quantity,
                                        ////											createdtime);
                                        popupWindow.dismiss();

                                        //									finish();
                                        //									startActivity(getIntent());
                                        overridePendingTransition(0, 0);
                                    }
                                });
                            }
                        });

                    }
                }});

            listcontent.addView(child);
        }
        nos.setText(String.valueOf(itemmap.size()) + getResources().getString(R.string.nos));
        // DecimalFormat twoDForm = new DecimalFormat("#.#");
        // amtd = Double.valueOf(twoDForm.format(amtd));
        // amt.setText(amtd > 0 ? String.valueOf(amtd) + " INR" : "0.0 INR");
        //		String amot = db.getorderamt(orderid);
        //		amt.setText(amot == null ? "0.0 INR" : amot + " INR");
    }
    //=================Begin of Approve
    public void ItemlistApprove()
    {
        if(itemmap.isEmpty()){
            Toast.makeText(InwardOutwardListActivity.this, R.string.toastnoproduct, Toast.LENGTH_LONG).show();
            return;
        }
        final LinearLayout tab =(LinearLayout) findViewById(R.id.listcontent);
        final View customView = inflater.inflate(R.layout.popuplayout, null);
        TextView header =(TextView) customView.findViewById(R.id.header);
        TextView message =(TextView) customView.findViewById(R.id.message);
        header.setText(R.string.confirmapproveInward);
        message.setText(R.string.msgInwardconfirm);
        Button yes =(Button) customView.findViewById(R.id.yes);
        Button cancel =(Button) customView.findViewById(R.id.close);

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);

        popupWindow.showAtLocation(tab, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popupWindow.dismiss();
                Button appr = ((Button) findViewById(R.id.listapprove));
                appr.setEnabled(false);
                Button deletebtn = ((Button) findViewById(R.id.cancellist));
                deletebtn.setEnabled(false);

                //CREATED TIME:
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                String createdtime = df.format(c.getTime());

                //SALESPERSON ID:
                SharedPreferences pref = getSharedPreferences("Config", MODE_PRIVATE);
                String salespersonid = pref.getString("personid", "");
                String salespersoncode = pref.getString("usernamefor", "");

                //STOCK TRANSFER JSON FORMAT:
                JSONObject webobj=new JSONObject();

                try{
                    webobj.put("operation","Stock Transfer");
                    webobj.put("Stock Transfer",state);
                    webobj.put("SalesPersonId", salespersonid);
                    webobj.put("CreateTime", createdtime);
                    JSONArray productArray = new JSONArray();
                    for (Map.Entry<String, JSONObject> entry : itemmap.entrySet()) {
                        String key = entry.getKey();
                        JSONObject value = entry.getValue();
                        JSONObject webprodobj=new JSONObject();
                        webprodobj.put("Product", key);
                        webprodobj.put("Batch", batchmap.get(value.getString("batch")));
                        webprodobj.put("Qty",value.getString("Qty"));
                        webprodobj.put("Rate",value.getString("Rate"));
                        webprodobj.put("PackageType", value.getString("PackageType"));
                        productArray.put(webprodobj);
                    }

                    webobj.put("Products", productArray);



                    //WRITING TO INTERNAL STORAGE
                    WriteToFile w=new WriteToFile();
                    w.writeFile(state, createdtime, webobj.toString(), "AriesBackup");

                    webconfigurration C = new webconfigurration(getApplicationContext());
                    String inputParamsStr = "{\"data\": {\"User\": \""
                            + C.user
                            + "\",\"DBname\": \""
                            + C.dbname
                            + "\", \"BussinessUnit\": \""
                            + C.bu
                            + "\","
                            + "\"Operation\": \"Custom\","
                            + "\"columnlist\": [],"
                            + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"StockTransferMobile\",\"operationtype\": \"custom\",\"Value\": \"\","
                            + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                            + "\"appname\": \""
                            + C.appid
                            + "\","
                            + "\"data\": [],"
                            + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                            + C.orgid + "\"}," + "\"meta\": []}";

                    JSONObject inputParamObj = new JSONObject(inputParamsStr);
                    inputParamObj.getJSONObject("data").getJSONArray("data").put(webobj);

                    String auth = webconfigurration.auth;
                    auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
                    inputParamObj.put("auth", auth);

                    if(isNetworkAvailable()){
                        getdata(inputParamObj.toString());

                    }
                    else{
//                        Global.Toast(InwardOutwardItemList.this, "You Are Offline..Check Your Connection", Toast.LENGTH_SHORT, Font.Regular);
                        Toast.makeText(InwardOutwardListActivity.this, R.string.toastoffline, Toast.LENGTH_LONG).show();

                    }

                }
                catch(Exception e){
                    e.getStackTrace();
                }

            }
        });


    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public void setButtonVisibility(boolean approvedFlag ){

        if (approvedFlag) {
            approved=true;

            Button appr = ((Button) findViewById(R.id.listapprove));
            appr.setEnabled(false);
            appr.setBackgroundResource(R.color.dark_gray);
            appr.setText(getResources().getString(R.string.approvedbtn));
            Button deleteall = ((Button) findViewById(R.id.cancellist));
            deleteall.setEnabled(false);
            deleteall.setBackgroundResource(R.color.dark_gray);
        }else{

            approved=false;

            Button appr = ((Button) findViewById(R.id.listapprove));
            appr.setBackgroundResource(R.color.textDark);
            appr.setEnabled(true);
//            appr.setBackgroundResource(R.drawable.dhi_square_button);
            appr.setText(getResources().getString(R.string.approve));
            Button deleteall = ((Button) findViewById(R.id.cancellist));
            deleteall.setEnabled(true);
//            deleteall.setBackgroundResource(R.drawable.dhi_square_button);
        }

    }
    public void getdata(String urlParameters) {

        MyRunnable myRunnable = new MyRunnable(urlParameters);
        Thread t = new Thread(myRunnable);
        t.start();
    }

    public class MyRunnable implements Runnable {

        private String urlParameters;

        public MyRunnable(String urlParameters) {
            this.urlParameters = urlParameters;
        }

        @Override
        public void run() {

            try{

                webconfigurration C = new webconfigurration(getApplicationContext());




                String targetURL = C.stocktransferUrl;

                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-8");
                // conn.setReadTimeout(60000);
                conn.setConnectTimeout(10000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestMethod("POST");

                int totalLength = (urlParameters.getBytes().length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.writeBytes(urlParameters);
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();
                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                String responsefrom = response.toString();
                //				Log.d("return :", "returning :"+responsefrom);
                JSONObject webresp= new JSONObject(responsefrom);
                final String message= webresp.getString("message");

                if(message.equalsIgnoreCase("Success")){
                    boolean count=db.insertInventoryJrnl(webresp);
                    //                	Log.d("db count", "count :"+count);


                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            Toast.makeText(getApplicationContext(),R.string.inwardsapprove, Toast.LENGTH_SHORT).show();

                            setButtonVisibility(true);


                        }
                    });

                }
                else{
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),R.string.inwarsfaild +message, Toast.LENGTH_SHORT).show();
                            setButtonVisibility(false);
                        }
                    });



                }

                //				Log.d("post", responsefrom);
                rd.close();

                if (conn != null) {
                    conn.disconnect();
                }

            } catch (Exception e) {
                Log.d("post", getResources().getString(R.string.cannot_connect_to_server));
                e.printStackTrace();

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // update ui here
                        // display toast here

                        Toast.makeText(getApplicationContext(),R.string.cannot_connect_to_server, Toast.LENGTH_SHORT)  .show();
                        setButtonVisibility(false);
                    }
                });

            }

        }
    }
    //===================================End of approve button
    //===================================Start of delete button
    public void ItemlistDelete()
    {
        final LinearLayout tab =(LinearLayout) findViewById(R.id.listcontent);
        final View customView = inflater.inflate(R.layout.popuplayout, null);
        TextView header =(TextView) customView.findViewById(R.id.header);
        TextView message =(TextView) customView.findViewById(R.id.message);
        header.setText(getResources().getString(R.string.confirmdeleteInward));
        message.setText(getResources().getString(R.string.msgconfirmdelete));
        Button yes =(Button) customView.findViewById(R.id.yes);
        Button cancel =(Button) customView.findViewById(R.id.close);

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);

        popupWindow.showAtLocation(tab, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popupWindow.dismiss();
                listcontent.removeAllViews();
                itemmap.clear();
                nos.setText(String.valueOf(itemmap.size()) + getResources().getString(R.string.nos));
                Toast.makeText(getApplicationContext(),R.string.toastdeleteinward, Toast.LENGTH_SHORT).show();
            }
        });

    }
    //===========================End of delete button

    @Override
    public void onBackPressed() {



        pref = getSharedPreferences("Config", MODE_PRIVATE);

        if(approved){

            pref.edit().remove("InwardOutwardItemlist").commit();
            Intent intent=new Intent(InwardOutwardListActivity.this,CustomerListActivity.class);
            startActivity(intent);
            finish();
        }
        else{



            SharedPreferences.Editor editor = pref.edit();
            editor.putString("InwardOutwardItemlist", getProductList().toString());
            editor.putString("TabState", state);
            editor.commit();
            Intent intent=new Intent(InwardOutwardListActivity.this,InwardOutwardActivity.class);
            startActivity(intent);
            finish();
            super.onBackPressed();
            //	    		    finish();
        }

    }

    private JSONArray getProductList(){
        JSONArray productArray = new JSONArray();
        try {

            for (Map.Entry<String, JSONObject> entry : itemmap.entrySet()) {
                String key = entry.getKey();
                JSONObject value = entry.getValue();
                JSONObject webprodobj=entry.getValue();

                webprodobj.put("ProductId",key);
                webprodobj.put("Qty",value.getString("Qty"));
                webprodobj.put("PackageType",value.getString("PackageType"));
                webprodobj.put("batch",value.getString("batch"));
                webprodobj.put("transfer",state);
                productArray.put(webprodobj);



            }

        } catch (JSONException e) {

            e.printStackTrace();
        }
        return productArray;
    }

}
