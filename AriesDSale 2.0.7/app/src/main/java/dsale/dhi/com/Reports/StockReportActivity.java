package dsale.dhi.com.Reports;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.HashMap;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.Global;
import dsale.dhi.com.objects.webconfigurration;
import dsale.dhi.com.print.PrintActivity;
import dsale.dhi.com.print.UtilPrint;

public class StockReportActivity extends AppCompatActivity {
    private TableLayout list;
    private TableRow row;
    private DatabaseHandler db;
    private HashMap<String, JSONObject> hashmap;
    private TableRow.LayoutParams lp;
    private ImageButton prntbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stock_report);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        prntbtn = (ImageButton) findViewById(R.id.prntbtn);
        prntbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message1 = getReportForPrint();
                Intent intent=new Intent(StockReportActivity.this, PrintActivity.class);
                intent.putExtra("message1",message1 );
                intent.putExtra("message2","" );
                intent.putExtra("message3","" );
                finish();
                startActivity(intent);
                int i=0;
            }
        });

        list = (TableLayout) findViewById(R.id.list);

        lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        lp.setMargins(1, 0, 0, 1);
        SharedPreferences pref = getApplicationContext()
                .getSharedPreferences("Config", MODE_PRIVATE);
        db = new DatabaseHandler(getApplicationContext());
        try {
            String orgid=pref.getString("tmsOrgId", "");
            hashmap = db.getvansalesstockreport(orgid);

        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i=1 ;i <= hashmap.size(); i++) {
            String sno = String.valueOf(i);
            JSONObject value = hashmap.get(sno);

            String itemname = value.optString("productname", "");
            String availableqty = value.optString("availableqty", "");
            String unit = value.optString("unit", "KG");

            row = new TableRow(StockReportActivity.this);
//            row.setBackgroundResource(R.color.darkgray);
            row.setBackgroundResource(R.drawable.table_row);
            row.addView(addtext(sno,i));
            row.addView(addtext(itemname, i));
            row.addView(addtext(availableqty, i));
            row.addView(addtext(unit, i));
            list.addView(row);
        }


    }
//    Added by jithu on 15/05/2019 for adding menu for stock report print
//    public boolean onCreateOptionsMenu(Menu menu){
//        getMenuInflater().inflate(R.menu.menu_print_report, menu);
//        return true;
//    }

    //Added by Jithu on 15/05/2019 for adding menu for stock report print
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.printReport:
                String message1 = getReportForPrint();
                Intent intent=new Intent(StockReportActivity.this, PrintActivity.class);
                intent.putExtra("message1",message1 );
                intent.putExtra("message2","" );
                intent.putExtra("message3","" );
                finish();
                startActivity(intent);
                int i=0;
                return true;

            default :return super.onOptionsItemSelected(item);
        }
    }

    //Added by Jithu on 15/05/2019 for getting data for stock report print
    private String getReportForPrint() {


        String printmsg = "";
        try{
            webconfigurration c = new webconfigurration(getApplicationContext());
            String line = UtilPrint.getPrintFormat("------------------------", 24, 2);
            String companyName =c.companyNameShort ;
            String msg = UtilPrint.getPrintFormat(getResources().getString(R.string.stock_report_heading), 24, 2)
                    +UtilPrint.getPrintFormat(companyName.toUpperCase(), 24, 0)
                    +UtilPrint.getPrintFormat(c.companyAddressShort.toUpperCase(), 24, 0)
                    +UtilPrint.getPrintFormat(c.companyGSTIN.toUpperCase(), 24, 0)
                    +UtilPrint.getPrintFormat("ST CODE:32", 24, 0)
                    +line;
            String productHeads =UtilPrint.getPrintFormat(getResources().getString(R.string.stock_report_slno_heading),5, 0)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.stock_report_itemname_heading),8, 0)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.stock_report_qty_heading),7, 0)
                    +UtilPrint.getPrintFormat(getResources().getString(R.string.stock_report_unit_heading), 4, 1)
                    +line;
            String productString="";
            int slNo=1;
            for (int i=1 ;i <= hashmap.size(); i++) {
                String sno = String.valueOf(i);
                JSONObject value = hashmap.get(sno);

                String itemname = value.optString("productname", "");
                itemname=getShortenItemName(itemname,21);
                String availableqty = value.optString("availableqty", "0");
                double avlQtyInDble=Double.parseDouble(availableqty);
                if(avlQtyInDble<=0)
                    continue;
                String unit = value.optString("unit", "kilogram");
                String slNoStr="";
                if(String.valueOf(slNo).length()==1)
                    slNoStr="0"+String.valueOf(slNo);
                else
                    slNoStr=String.valueOf(slNo);
                productString =productString+UtilPrint.getPrintFormat(slNoStr, 4,0)
                        + UtilPrint.getPrintFormat(itemname, 20, 0)
                        +UtilPrint.getPrintFormat(availableqty, 17, 1)
                        +UtilPrint.getPrintFormat(unit, 7, 1);
                slNo++;

            }
            printmsg =  msg+productHeads+productString;

        }catch ( Exception e){
            e.printStackTrace();
        }
        return printmsg;
        //return null;

    }

    //Added by Jithu on 15/05/2019 for getting shortname of a given product
    private String getShortenItemName(String itemname, int possibleLength) {

        StringBuilder shortenName = new StringBuilder();

        if (itemname.length()<=possibleLength) {
            return itemname;
        }else {

            String[] itemNameSplited = itemname.split(" ");
            if (itemNameSplited.length > 3) {

                shortenName.append(itemNameSplited[0].length() < 3 ? itemNameSplited[0] : itemNameSplited[0].substring(0, 3));
                shortenName.append(".");
                for (int i = 1; i < itemNameSplited.length-1; i++) {

                    //itemNameSplited[i] = itemNameSplited[i].substring(itemNameSplited.length-2) + ".";
                    shortenName.append(itemNameSplited[i].length() < 1 ? itemNameSplited[i] : itemNameSplited[i].substring(0, 1));
                    shortenName.append(".");
                }
                shortenName.append(itemNameSplited[itemNameSplited.length-1]);
                return shortenName.toString() ;

            }else if (itemNameSplited.length == 3) {

                shortenName.append(itemNameSplited[0].length() < 3 ? itemNameSplited[0] : itemNameSplited[0].substring(0,3));
                shortenName.append(".");
                shortenName.append(itemNameSplited[1].length() < 2 ?
                        itemNameSplited[1] : itemNameSplited[1].substring(0, 2));
                shortenName.append(".");
                shortenName.append(itemNameSplited[itemNameSplited.length-1]);
                return shortenName.toString() ;
            }else if (itemNameSplited.length == 2) {

                shortenName.append(itemNameSplited[0].length() < 4 ? itemNameSplited[0] : itemNameSplited[0].substring(0, 4));
                shortenName.append(".");

                shortenName.append(itemNameSplited[itemNameSplited.length-1]);
                return shortenName.toString() ;
            }else if (itemNameSplited.length == 1) {

                shortenName.append(itemNameSplited[0].length() < possibleLength-2 ? itemNameSplited[0] : itemNameSplited[0].substring(0, possibleLength-2));
                shortenName.append(".");

//				shortenName.append(itemNameSplited[itemNameSplited.length-1]);
                return shortenName.toString() ;
            }


        }

        return shortenName.toString();

    }

    private TextView addtext(String string, int i) {
        TextView text = new TextView(StockReportActivity.this);
        text.setLayoutParams(lp);
        text.setText(string);
//        if (i % 2 == 0)
            text.setBackgroundResource(R.color.white);
//        else
//            text.setBackgroundResource(R.color.gray3);
        text.setPadding(10, 20, 20, 10);
//        a = new Font(VanSalesReport.this, text, Font.Regular);
        return text;
    }
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        String l = Global.setLastTime();
    }

    //Added by Jithu on 17/05/2019 for back press is redirected to customer list page
    @Override
    public void onBackPressed() {

        Intent intent=new Intent(StockReportActivity.this, CustomerListActivity.class);
        finish();
        startActivity(intent);
        super.onBackPressed();
    }
}
