package dsale.dhi.com.order;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.amazonaws.auth.policy.Resource;

import java.io.InputStream;

import dsale.dhi.com.ariesdsale.R;

class DownloadImageTask extends AsyncTask<String, Void, Bitmap>  {
    private final ImageView bmImage;
    private  Context mContext;
    public DownloadImageTask(ImageView bmImage, Context mContext) {
        this.bmImage = bmImage;
        this.mContext=mContext;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }
    protected void onPostExecute(Bitmap result) {
        if(result==null){
            Drawable d = mContext.getResources().getDrawable(R.drawable.filenotfound);
            bmImage.setImageDrawable(d);
        }
        else{
            bmImage.setImageBitmap(result);
        }
        //bmImage.setImageDrawable(R.drawable.fileNotFound);

    }
}
