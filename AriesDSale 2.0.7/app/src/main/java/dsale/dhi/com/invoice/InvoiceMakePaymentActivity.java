package dsale.dhi.com.invoice;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.ariesdsale.ShopActivity;
import dsale.dhi.com.customer.EditCustomerActivity;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.gps.PlayTracker;
import dsale.dhi.com.order.OrderTakingActivity;

public class InvoiceMakePaymentActivity extends AppCompatActivity implements View.OnClickListener  {
    InvoiceMakePaymentAdapter adapter;
    Spinner spinner;
    EditText dat;
    private static String sid;
    PopupWindow popupWindow;
    LinearLayout linearLayout1;
    private static String Schedulestatus = "";
    private String scheduledetailid = "";
    SparseArray<TextView> array;
    DatabaseHandler db;
    String[] banknames;
    String[] bankcodes;
    String cancelled = "Cancelled";
    Calendar myCalendar = Calendar.getInstance();
    private HashMap<String, String> banks;
    String[] amount;
    String[] balance;
    String[] invoicenumber;
    String[] customerid;
    String[] invoicedate;
    String[] status;
    String[] invoiceid;
    /*Double subtotal = 0.0;
    Double paidamount = 0.0;
    Double amountdue = 0.0;
    double tot_amt = 0.0;*/
    private static String checknumbervalue = "";
    private static String banknamevalue = "";
    private static String checkdatevalue = "";
    private static String banknamecode = "";
    private ImageView imgNavigationcustdtl3;
    private ImageView imgNavigationcustdtl1;
    private ImageView imgNavigationcustdtl4;
    final List<View> arr = new ArrayList<View>();
    final HashMap<String, String> arrText = new HashMap<String, String>();
    HashMap<Integer, String> selItems = new HashMap<Integer, String>();

    private int roundoffPrecision ;
    private String currencySymbol ;
    private String currencyCode ;

    private BigDecimal subtotal = new BigDecimal(0);
    private BigDecimal paidamount = new BigDecimal(0);
    private BigDecimal amountdue =new BigDecimal(0);
    private BigDecimal tot_amt = new BigDecimal(0);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_make_payment);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        imgNavigationcustdtl3 = (ImageView) findViewById(R.id.imgNavigationcustdtl3);
        imgNavigationcustdtl3.setOnClickListener(this);
        imgNavigationcustdtl1 =(ImageView) findViewById(R.id.imgNavigationcustdtl1);
        imgNavigationcustdtl1.setOnClickListener(this);
        imgNavigationcustdtl4 =(ImageView) findViewById(R.id.imgNavigationcustdtl4);
        imgNavigationcustdtl4.setOnClickListener(this);

        banks = new HashMap<>();
        try {
            ViewGroup viewGroup = ((ViewGroup) findViewById(R.id.parent));
            array = new SparseArray<TextView>();
            findAllEdittexts(viewGroup);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Bundle bundle = getIntent().getExtras();
        scheduledetailid = (String) bundle.get("scheduledetailid");
        sid = scheduledetailid;
        Schedulestatus = (String) bundle.get("status");
        spinner = (Spinner) findViewById(R.id.spinner2);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        String type = pref.getString("typeofschedule", "");
        db = new DatabaseHandler(getApplicationContext());
        try {
            String rsncode = db.getreasoncodechoosen(scheduledetailid);
            if (!rsncode.contentEquals("")) {
                Button sav = (Button) findViewById(R.id.save);
                sav.setEnabled(false);
                sav.setBackgroundResource(R.color.dark_gray);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //=====================================Bank data

        JSONArray bankdata;
        try {
            bankdata = db.getbankdata();

            bankcodes = new String[bankdata.length()];
            banknames = new String[bankdata.length()];
            // bankcodes[0] = "0";
            // banknames[0] = "SELECT";
            for (int i = 0; i < bankdata.length(); i++) {
                JSONObject dataobj = bankdata.getJSONObject(i);
                bankcodes[i] = dataobj.getString("bankcode");
                banknames[i] = dataobj.getString("bankcode") + "- "
                        + dataobj.getString("bankname");
                String name = dataobj.getString("bankcode") + "- "
                        + dataobj.getString("bankname");
                String code = dataobj.getString("bankcode");
                banks.put(name, code);

            }

        } catch (JSONException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        JSONArray inputdata = new JSONArray();
        try {
            inputdata = db.getallinvoicedata(sid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        amount = new String[inputdata.length()];
        balance = new String[inputdata.length()];
        invoicenumber = new String[inputdata.length()];
        customerid = new String[inputdata.length()];
        invoicedate = new String[inputdata.length()];
        status = new String[inputdata.length()];
        invoiceid = new String[inputdata.length()];
        for (int i = 0; i < inputdata.length(); i++) {
            amount[i] = inputdata.optJSONObject(i).optString("amount","0");
            balance[i] = inputdata.optJSONObject(i).optString("balance","0");
            invoicenumber[i] = inputdata.optJSONObject(i).optString(
                    "invoicenumber");
            customerid[i] = inputdata.optJSONObject(i).optString("customerid");
            invoicedate[i] = inputdata.optJSONObject(i)
                    .optString("invoicedate");
            status[i] = inputdata.optJSONObject(i).optString("status");
            invoiceid[i] = inputdata.optJSONObject(i).optString("invoiceid");
            try {
                /*Double amt = Double.parseDouble(amount[i]);
                Double bal = Double.parseDouble(balance[i]);
                Double paid = amt - bal;
                subtotal = subtotal + amt;
                paidamount = paidamount + paid;
                amountdue = amountdue + bal;*/

                BigDecimal amt =  new BigDecimal(amount[i]);
                BigDecimal bal = new BigDecimal(balance[i]);
                BigDecimal paid = amt.subtract(bal) ;
                subtotal = subtotal.add(amt) ;
                paidamount = paidamount.add(paid);
                amountdue = amountdue.add(bal);

                amountdue = amountdue.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/) ;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        pref = InvoiceMakePaymentActivity.this.getSharedPreferences("Config",InvoiceMakePaymentActivity.MODE_PRIVATE);
       /* String currencyType=pref.getString("currencyType","");
        currencyType=currencyType.toUpperCase();*/
        roundoffPrecision = pref.getInt("RoundOffPrecision",0);
        currencyCode=pref.getString("CurrencyCode","");
        currencySymbol =pref.getString("CurrencySymbol","");
        TextView subtotal= (TextView) findViewById(R.id.subtotal);
        TextView inrTV= (TextView) findViewById(R.id.inrTV);
        inrTV.setText(currencySymbol+" ");
        subtotal.setText(amountdue.toString());


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new InvoiceMakePaymentAdapter(this,sid);
        recyclerView.setAdapter(adapter);
        if (invoicenumber.length > 0) {
            TextView subtot = findViewById(R.id.subtotal);
            subtot.setText( amountdue.toString());
            pref = InvoiceMakePaymentActivity.this.getSharedPreferences("Config",InvoiceMakePaymentActivity.MODE_PRIVATE);
            Button makepayment =findViewById(R.id.makepayment);
            if (pref.getInt("dispayment", 0) > 0)
                makepayment.setVisibility(View.GONE);
            linearLayout1 = (LinearLayout) findViewById(R.id.popuplayout);
            makepayment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selItems= adapter.retitem();
//                    double total_payingamount = 0.0;
                    BigDecimal total_payingamount = new BigDecimal(0);
                    for (int i = 0; i < invoicenumber.length; i++) {
                        String singlebil_amount = selItems.get(i);
                        if (singlebil_amount.equals(""))
                            continue;

                        try {
                            total_payingamount = total_payingamount.add(new BigDecimal(singlebil_amount));
                           /* total_payingamount += Double
                                    .parseDouble(singlebil_amount);*/
                        } catch (Exception newx) {
                        }
                    }
//                    if(total_payingamount == 0)
//                    {
//                        Toast.makeText(InvoiceMakePaymentActivity.this, "Invalid Amount",Toast.LENGTH_SHORT).show();
//                        return;
//                    }
                    Spinner spiner_temp = (Spinner) findViewById(R.id.spinner2);
                    String paymentmode = spiner_temp.getSelectedItem() .toString();
                    LayoutInflater layoutInflater = (LayoutInflater) InvoiceMakePaymentActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View customView = layoutInflater.inflate(R.layout.popuplayout_makepayment, null);
                    TextView header =customView.findViewById(R.id.header);
                    TextView msg =(TextView) customView.findViewById(R.id.message);
                    Button yes =(Button) customView.findViewById(R.id.yes);
                    Button cancel = (Button) customView.findViewById(R.id.close);
                    header.setText(getResources().getString(R.string.submitpayment));

                    String chequenumber_temp = checknumbervalue;
                    String cheuedate_temp = checkdatevalue;
                    String bankname_temp = banknamevalue;

                    String confirmation_string = "";
                    total_payingamount = total_payingamount.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
                    tot_amt=total_payingamount;

                    if (paymentmode.equals("CDC") || paymentmode.equals("PDC")) {
                        confirmation_string = getResources().getString(R.string.totamt)
                                + total_payingamount + "\n" + getResources().getString(R.string.paymentmode)
                                + paymentmode + "\n" + getResources().getString(R.string.chno)
                                + chequenumber_temp + "\n" + getResources().getString(R.string.chdt)
                                + cheuedate_temp + "\n" + getResources().getString(R.string.chbnk)
                                + bankname_temp;

                    } else {
                        confirmation_string = getResources().getString(R.string.totamt)
                                + total_payingamount + "\n" + getResources().getString(R.string.paymentmode)
                                + paymentmode;
                    }
                    final String confirmation_final = confirmation_string;
                    msg.setText(confirmation_final);
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            Double tottal_amount_paid = 0.0;
                            BigDecimal tottal_amount_paid = new BigDecimal(0);
                            JSONArray invoicedata = new JSONArray();
                            for (int i = 0; i < invoicenumber.length; i++) {
                                String invamount = selItems.get(i);
                                if (invamount.equals(""))
                                    continue;
                                JSONObject obj = new JSONObject();
                                String amount = invamount;

                                String id = invoiceid[i];
                                String tottalinvoiceamount = InvoiceMakePaymentActivity.this.balance[i];
                                try {
                                    obj.put("amout", amount);
                                    try {
                                      /*  tottal_amount_paid += Double
                                                .parseDouble(amount);*/
                                        tottal_amount_paid = tottal_amount_paid.add(new BigDecimal(amount));
                                        tottal_amount_paid =
                                                tottal_amount_paid.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
                                    } catch (Exception newx) {
                                    }
                                    obj.put("invoiceid", id);
                                    obj.put("invoicenumber", invoicenumber[i]);
                                    obj.put("Tottalinvoiceamount",
                                            tottalinvoiceamount);
                                /*    Double currentamount_temp = Double
                                            .parseDouble(amount);
                                    Double tottalamount_temp = Double
                                            .parseDouble(tottalinvoiceamount);*/

                                    BigDecimal currentamount_temp = new BigDecimal(amount);
                                    BigDecimal tottalamount_temp =new BigDecimal(tottalinvoiceamount);

                                    Spinner mySpinner = (Spinner)findViewById(R.id.spinner2);
                                    String paymentmode = mySpinner
                                            .getSelectedItem()
                                            .toString();
//                                    Double balance;

                                    BigDecimal balance =new BigDecimal(0);
                                    if (paymentmode
                                            .contentEquals("Credit")){

                                         /*  balance = round(
                                                (tottalamount_temp), 2);*/
                                        balance = tottalamount_temp.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
                                    } else {

                                        balance =tottalamount_temp.subtract(currentamount_temp);
                                        balance = balance.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
                                       /* balance = round(
                                                (tottalamount_temp - currentamount_temp),
                                                2);*/
                                    }
                                    String status = "";
//                                    if (balance <= 0) {
                                    if (balance.compareTo(new BigDecimal(0))<= 0 ) {
                                        status = "paid";
                                        obj.put("balance",
                                                String.valueOf(balance));
                                    } else {
                                        status = "not paid";
                                        obj.put("balance",
                                                String.valueOf(balance));
                                    }
                                    obj.put("status", status);
                                    invoicedata.put(obj);

                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }

                            }
                            DatabaseHandler db = new DatabaseHandler(InvoiceMakePaymentActivity.this);

                            String paymentid = UUID.randomUUID().toString();
                            Calendar c1 = Calendar.getInstance();
                            DateFormat dateTimeFormat2 = new SimpleDateFormat("yyyyMMddHHmmss");
                            DateFormat dateTimeFormat_payment = new SimpleDateFormat( "dd/MM/yyyy HH:mm:ss");
                            String createtime = dateTimeFormat2 .format(c1.getTime());
                            String paymentdate = dateTimeFormat_payment .format(c1.getTime());

                            //GPS
                            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                            boolean statusOfGPS = manager
                                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
                            if (!statusOfGPS) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(
                                        InvoiceMakePaymentActivity.this);
                                builder.setTitle(getResources().getString(R.string.switch_on_gps));
                                builder.setMessage(getResources().getString(R.string.gps_off_alert_message));
                                builder.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                startActivityForResult(
                                                        new Intent(
                                                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                                        0);
                                            }
                                        });
                                builder.show();
                                return;
                            }
                            Location loc =null;
                            final double lat ;
                            final double lng ;
                            if (PlayTracker.getLastLocation() != null) {
                                loc = PlayTracker.getLastLocation();
                                lat =  loc.getLatitude();
                                lng = loc.getLongitude();
                            }
                            else
                            {
                                lat=0.0;
                                lng=0.0;
                            }

                            Spinner mySpinner = (Spinner) findViewById(R.id.spinner2);
                            String paymentmode = mySpinner
                                    .getSelectedItem().toString();

                            if (paymentmode.equals("CDC") || paymentmode.equals("PDC"))
                                paymentmode = "Cheque";
                            String actualmode = mySpinner.getSelectedItem().toString();
//                            if (invoicedata.length() < 1) {
//                                return;
//                            }

//                            if(tot_amt != 0.0) {
                            if(tot_amt.compareTo(new BigDecimal( 0 )) != 0) {
                                try {

                                    if (!paymentmode.equals("Cheque")) {
                                        checknumbervalue = "";
                                        banknamevalue = "";
                                        checkdatevalue = "";
                                        banknamecode = "";
                                    }
                                    db.insertpayments(
                                            sid,
                                            paymentmode,
                                            tottal_amount_paid.toString(),
                                           /* String.valueOf(round(
                                                    tottal_amount_paid, 2)),*/
                                            checknumbervalue,
                                            banknamevalue, createtime,
                                            paymentid, checkdatevalue,
                                            paymentdate, String
                                                    .valueOf(lat), String
                                                    .valueOf(lng),
                                            invoicedata, actualmode,
                                            banknamecode, true);

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }

                                try {
                                    db.updateschedulestatus(sid, "payment");
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }

                            Intent intent = new Intent(InvoiceMakePaymentActivity.this, InvoicePaymentApproveActivity.class);
                            intent.putExtra("scheduledetailid", scheduledetailid);
                            overridePendingTransition(0, 0);
                            finish();
                            startActivity(intent);
//                            DeliveryColllectionActivity.afterpayment = true;
                            popupWindow.dismiss();
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                        }
                    });
                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
                    popupWindow = new PopupWindow(customView, width, height);
                    popupWindow.setFocusable(true);

                    //display the popup window
                    popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);
                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                }
            });
        }
        else
        {
            LinearLayout lay =(LinearLayout) findViewById(R.id.popuplayout);
            lay.setVisibility(View.GONE);
            Button makepay =(Button) findViewById(R.id.makepayment);
            makepay.setBackgroundResource(R.color.textDark);
            makepay.setTextColor(getResources().getColor(R.color.textcolor1));
            makepay.setEnabled(false);
            TextView invcnt =(TextView) findViewById(R.id.noinv);
            invcnt.setVisibility(View.VISIBLE);

        }


        ArrayAdapter adapter1 = ArrayAdapter.createFromResource(getApplicationContext(), R.array.payment_mode, R.layout.spinner_item_payment);

        linearLayout1 = (LinearLayout) findViewById(R.id.popuplayout);
        spinner.setAdapter(adapter1);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {


                String text = spinner.getSelectedItem().toString();
                cancelled = "Cancelled";
                //Log.d("myTag", text);
                if(text.equals("CDC"))
                {
                    LayoutInflater layoutInflater = (LayoutInflater) InvoiceMakePaymentActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View customView = layoutInflater.inflate(R.layout.cdc_popup, null);
                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
                    popupWindow = new PopupWindow(customView, width, height);
                    Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                    Button yes= (Button) customView.findViewById(R.id.confirm);
                    TextView cdc=(TextView) customView.findViewById(R.id.cdcorpdc);
                    dat=(EditText) customView.findViewById(R.id.checquedate);
                    dat.setEnabled(false);
                    cdc.setText(getResources().getString(R.string.chequecdc));
                    Spinner spinnerbankmaster = (Spinner) customView.findViewById(R.id.banknamespinner);
                    SpinnerAdapter spinAdapter_bank = new ArrayAdapter<Object>(
                            getApplicationContext(), android.R.layout.simple_list_item_1,
                            banknames);
                    spinnerbankmaster.setAdapter(spinAdapter_bank);
                    final ImageButton datepick = (ImageButton) customView.findViewById(R.id.calendar);
                    final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // TODO Auto-generated method stub
                            myCalendar.set(Calendar.YEAR, year);
                            myCalendar.set(Calendar.MONTH, monthOfYear);
                            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            updateLabel();
                        }

                        private void updateLabel() {
                            String myFormat = "dd/MM/yyyy"; // In which you need put
                            // here
                            SimpleDateFormat sdf = new SimpleDateFormat(myFormat,
                                    Locale.US);
                            dat.setText(sdf.format(myCalendar.getTime()));
                        }
                    };
                    datepick.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            new DatePickerDialog(InvoiceMakePaymentActivity.this, date, myCalendar
                                    .get(Calendar.YEAR), myCalendar
                                    .get(Calendar.MONTH), myCalendar
                                    .get(Calendar.DAY_OF_MONTH)).show();
                        }
                    });

                        Calendar cal = Calendar.getInstance();
                        Date dateafter_2_days = cal.getTime();
                        String Cheque_date_choosen = "";
                        DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy");
                        Cheque_date_choosen = dateTimeFormat.format(dateafter_2_days);
                        dat.setText(Cheque_date_choosen);
                        dat.setEnabled(false);
                        datepick.setEnabled(false);



                    //instantiate popup window

                    popupWindow.setFocusable(true);

                    //display the popup window
                    popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);
                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    //close the popup window on button click
                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            spinner.setSelection(0);
                            popupWindow.dismiss();
                        }
                    });
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            EditText checknumber = (EditText) customView.findViewById(R.id.checkno);
                            String chknbr = checknumber.getText().toString();
                            Spinner bankname = (Spinner) popupWindow
                                    .getContentView().findViewById(
                                            R.id.banknamespinner);
                            String ifsccdval = bankname.getSelectedItem()
                                    .toString();

                            String selecteditemcode = bankcodes[bankname
                                    .getSelectedItemPosition()];
                            EditText chequedate = (EditText) customView.findViewById(R.id.checquedate);
                            String chequedatevalue = chequedate.getText().toString();
                            if (chknbr.equals("")) {
                                Toast.makeText(InvoiceMakePaymentActivity.this, getResources().getString(R.string.toastchequenoneeded),
                                        Toast.LENGTH_SHORT).show();

                                return;
                            }

                            if (ifsccdval.equalsIgnoreCase(getResources().getString(R.string.SELECT))) {
                                Toast.makeText(InvoiceMakePaymentActivity.this, getResources().getString(R.string.toastbanknameneeded),
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if (chequedatevalue.equals("")) {
                                Toast.makeText(InvoiceMakePaymentActivity.this, getResources().getString(R.string.toastdatenotvalid),
                                        Toast.LENGTH_SHORT).show();
                                return;

                            }

                            checknumbervalue = chknbr;
                            banknamevalue = ifsccdval;
                            banknamecode = selecteditemcode;
                            checkdatevalue = chequedatevalue;
                            cancelled = "OK";
                            popupWindow.dismiss();
                        }
                    });
                    popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                                @Override
                                public void onDismiss() {
                                    if (!(cancelled.contentEquals("OK"))) {
                                        spinner.setSelection(0);
                                        popupWindow.dismiss();
                                    }

                                }
                            });
                }
                else if(text.equals("PDC"))
                {
                    LayoutInflater layoutInflater = (LayoutInflater) InvoiceMakePaymentActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View customView = layoutInflater.inflate(R.layout.cdc_popup, null);
                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
                    popupWindow = new PopupWindow(customView, width, height);
                    Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                    Button yes= (Button) customView.findViewById(R.id.confirm);
                    TextView cdc=(TextView) customView.findViewById(R.id.cdcorpdc);
                    cdc.setText(getResources().getString(R.string.chequepdc));
                    ImageButton datp=(ImageButton) customView.findViewById(R.id.calendar);
                    dat=(EditText) customView.findViewById(R.id.checquedate);
                    dat.setEnabled(false);
                    Spinner spinnerbankmaster = (Spinner) customView.findViewById(R.id.banknamespinner);
                    SpinnerAdapter spinAdapter_bank = new ArrayAdapter<Object>(
                            InvoiceMakePaymentActivity.this, android.R.layout.simple_list_item_1,
                            banknames);
                    spinnerbankmaster.setAdapter(spinAdapter_bank);
                    datp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Calendar calendar = Calendar.getInstance();
                            int year = calendar.get(Calendar.YEAR);
                            int month = calendar.get(Calendar.MONTH);
                            final int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                            DatePickerDialog datePickerDialog = new DatePickerDialog(InvoiceMakePaymentActivity.this,
                                    new DatePickerDialog.OnDateSetListener() {
                                        @Override
                                        public void onDateSet(DatePicker datePicker, final int year, final int month, final int day) {
                                            Date d= new Date();
                                            myCalendar.set(Calendar.YEAR, year);
                                            myCalendar.set(Calendar.MONTH, month);
                                            myCalendar.set(Calendar.DAY_OF_MONTH, day);
                                            Date enterdate = myCalendar.getTime();
                                            if(enterdate.compareTo(d) > 0)
                                            {
                                                AlertDialog builder = new AlertDialog.Builder(InvoiceMakePaymentActivity.this)
                                                        .setTitle(getResources().getString(R.string.alert))
                                                        .setMessage(
                                                                getResources().getString(R.string.alertfuturedate))
                                                        .setPositiveButton(
                                                                android.R.string.yes,
                                                                new DialogInterface.OnClickListener() {
                                                                    public void onClick(
                                                                            DialogInterface dialog,
                                                                            int which) {
                                                                        updateLabel(day,month,year);
                                                                    }
                                                                })
                                                        .setNegativeButton(
                                                                android.R.string.no,
                                                                new DialogInterface.OnClickListener() {
                                                                    public void onClick(
                                                                            DialogInterface dialog,
                                                                            int which) {
                                                                    }
                                                                })
                                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                                        .show();

                                            }
//                                            dat.setText(new StringBuilder().append(day).append("/")
//                                                    .append(month).append("/").append(year));
                                            else
                                            {
                                                updateLabel(day,month,year);
                                            }
                                        }
                                    }, year, month, dayOfMonth);
                            datePickerDialog.show();

                        }
                        private void updateLabel(int day,int month,int year) {
                            String myFormat = "dd/MM/yyyy"; // In which you need put
                            // here
                            SimpleDateFormat sdf = new SimpleDateFormat(myFormat,
                                    Locale.US);
                            dat.setText(sdf.format(myCalendar.getTime()));

//                            dat.setText(new StringBuilder().append(day).append("/")
//                                    .append(month).append("/").append(year));
                        }
                    });

                    popupWindow.setFocusable(true);

                    //display the popup window
                    popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);
                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    //close the popup window on button click
                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                        }
                    });

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            EditText checknumber = (EditText) customView.findViewById(R.id.checkno);
                            String chknbr = checknumber.getText().toString();
                            Spinner bankname = (Spinner) popupWindow
                                    .getContentView().findViewById(
                                            R.id.banknamespinner);
                            String ifsccdval = bankname.getSelectedItem()
                                    .toString();

                            String selecteditemcode = bankcodes[bankname
                                    .getSelectedItemPosition()];
                            EditText chequedate = (EditText) customView.findViewById(R.id.checquedate);
                            String chequedatevalue = chequedate.getText().toString();
                            if (chknbr.equals("")) {
                                Toast.makeText(InvoiceMakePaymentActivity.this, getResources().getString(R.string.toastchequenoneeded),
                                        Toast.LENGTH_SHORT).show();

                                return;
                            }

                            if (ifsccdval.equalsIgnoreCase(getResources().getString(R.string.SELECT))) {
                                Toast.makeText(InvoiceMakePaymentActivity.this, getResources().getString(R.string.toastbanknameneeded),
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if (chequedatevalue.equals("")) {
                                Toast.makeText(InvoiceMakePaymentActivity.this, getResources().getString(R.string.toastdatenotvalid),
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Calendar cal = Calendar.getInstance();

                            Date currdate = cal.getTime();

                            DateFormat dateTimeFormat = new SimpleDateFormat(
                                    "dd/MM/yyyy");
                            Date Cheque_date_choosen = new Date();
                            try {
                                Cheque_date_choosen = dateTimeFormat
                                        .parse(chequedatevalue);
                            } catch (ParseException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                            if (Cheque_date_choosen.compareTo(currdate) <= 0) {
                                Toast.makeText(InvoiceMakePaymentActivity.this, getResources().getString(R.string.toastinvaliddate),
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }
                            checknumbervalue = chknbr;
                            banknamevalue = ifsccdval;
                            banknamecode = selecteditemcode;
                            checkdatevalue = chequedatevalue;
                            cancelled = "OK";
                            popupWindow.dismiss();
                        }
                    });

                    popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            if (!(cancelled.contentEquals("OK"))) {
                                spinner.setSelection(0);
                                popupWindow.dismiss();
                            }

                        }
                    });
                }
                else {
                    checknumbervalue = "";
                    banknamevalue = "";
                    checkdatevalue = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

    }
    private void findAllEdittexts(ViewGroup viewGroup) {
        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof ViewGroup)
                findAllEdittexts((ViewGroup) view);
            else if (view instanceof TextView) {
                TextView edittext = (TextView) view;
                array.put(edittext.getId(), edittext);
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(InvoiceMakePaymentActivity.this, CustomerListActivity.class);
        finish();
        startActivity(i);
        overridePendingTransition(0,0);
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.imgNavigationcustdtl3:
                Intent i = new Intent(InvoiceMakePaymentActivity.this, EditCustomerActivity.class);

                i.putExtra("scheduledetailid", scheduledetailid);
                finish();
                startActivity(i);
                overridePendingTransition(0, 0);
                break;
            case R.id.imgNavigationcustdtl4:
                i = new Intent(InvoiceMakePaymentActivity.this, OrderTakingActivity.class);

                i.putExtra("status", "");
                i.putExtra("scheduledetailid", scheduledetailid);
                finish();
                overridePendingTransition(0, 0);
                startActivity(i);
                break;
            case R.id.imgNavigationcustdtl1:
                i = new Intent(InvoiceMakePaymentActivity.this, ShopActivity.class);
                i.putExtra("scheduledetailid", scheduledetailid);
                finish();
                startActivity(i);
                overridePendingTransition(0, 0);
                break;

//            case R.id.showAllBtn:
//                showFilterPopup("All", v);
//
//                break;
//
//            case R.id.brandBtn:
//
//                showFilterPopup("Brand", v);
//                break;
//
//            case R.id.categoryBtn:
//                showFilterPopup("Category", v);
//                break;
//
//            case R.id.formBtn:
//                showFilterPopup("Form", v);
//                break;


        }
    }

    /*public double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }*/
}
