package dsale.dhi.com.order;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dsale.dhi.com.ariesdsale.Product;
import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.ariesdsale.ShopActivity;
import dsale.dhi.com.customer.EditCustomerActivity;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.invoice.InvoiceMakePaymentActivity;
import dsale.dhi.com.objects.DateError;
import dsale.dhi.com.objects.PlayLocation;
import dsale.dhi.com.objects.webconfigurration;


public class OrderTakingActivity extends AppCompatActivity implements OrderTakingItemListAdapter.ItemClickListener,
        View.OnClickListener {
    private static final String COMPLIMENT_CUSTOMER_NAME = "Compliment";
    private static final int REQUEST_CODE_DATE_SETTING = 0;
//    String invoiceno = "";
    String currentDate = "";
    private SharedPreferences pref;
    private String scheduleLineId;
    private boolean complimentaryFlag;
    private String parentOrderId ;
//    private String parentInvoiceNo;
    private DatabaseHandler db;
    private String orderId;
    private String customerCategoryId;
    private String customerId;
    private String customerType;
    private HashMap<String, String> packageTypeMap;
    private List<String> packageTypeList = new ArrayList<>();
    private boolean approved = false;
    private TextView customerNameTv;
    private HashMap<String, Product> productmap;
    private HashMap<String,Product>  finalProductMap ;

    /*   private HashMap<String, String> qtymap;
     private HashMap<String, JSONArray> qtybatchmap;
     private HashMap<String, String> casemap;*/
    private OrderTakingItemListAdapter adapter;
    private String selectedCategory;
    private String selectedBrand;
    private String selectedForm;
    private String searchString;
    private int selection;
    private OrderTakingItemListAdapter.ItemClickListener itemClickListener;
    //    private OrderTakingItemListAdapter.ItemFocusChangedListener focusChangedListener ;
    private HashMap<String, OrderLine> savedData;
    private EditText searchEditText;
    private ImageView srchbtn;
    private TextView itemCountTextView;
    private TextView amountTextView;
    private Button showAllFilterBtn;
    private Button categoryFilterBtn;
    private Button formFilterBtn;
    private Button brandFilterBtn;
    private LayoutInflater inflater;

    private ImageView imgNavigationcustdtl3;
    private ImageView imgNavigationcustdtl2;
    private ImageView imgNavigationcustdtl1;
    private ImageView imgNoteBtn;

    private int roundoffPrecision ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_taking);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);


        imgNavigationcustdtl3 = (ImageView) findViewById(R.id.imgNavigationcustdtl3);
        imgNavigationcustdtl3.setOnClickListener(this);
        imgNavigationcustdtl2 = (ImageView) findViewById(R.id.imgNavigationcustdtl2);
        imgNavigationcustdtl2.setOnClickListener(this);
        imgNavigationcustdtl1 = (ImageView) findViewById(R.id.imgNavigationcustdtl1);
        imgNavigationcustdtl1.setOnClickListener(this);


        customerNameTv = (TextView) findViewById(R.id.customerName);
        searchEditText = (EditText) findViewById(R.id.searchText);
        itemCountTextView = (TextView) findViewById(R.id.CustomerOrderStatusIconText);
        amountTextView = (TextView) findViewById(R.id.totalAmountTv);
        srchbtn = (ImageView) findViewById(R.id.searchButton);

        showAllFilterBtn = (Button) findViewById(R.id.showAllBtn);
        categoryFilterBtn = (Button) findViewById(R.id.categoryBtn);
        formFilterBtn = (Button) findViewById(R.id.formBtn);
        brandFilterBtn = (Button) findViewById(R.id.brandBtn);
        imgNoteBtn =(ImageView) findViewById(R.id.imgNoteBtn);
        imgNoteBtn.setOnClickListener(this);

        //GPS check
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!statusOfGPS) {

            AlertDialog.Builder builder = new AlertDialog.Builder(
                    OrderTakingActivity.this);
            builder.setTitle(R.string.switch_on_gps);
            builder.setMessage(R.string.gps_off_alert_message);
            builder.setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            startActivityForResult(
                                    new Intent(
                                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                    0);
                        }
                    });
            builder.show();
            return;
        }



        inflater = getLayoutInflater();
        Bundle bundle = getIntent().getExtras();
        scheduleLineId = bundle.getString("scheduledetailid");
        complimentaryFlag = bundle.getBoolean("complimentary");
        parentOrderId = bundle.getString("parentOrderId");
//        parentInvoiceNo = bundle.getString("parentInvoiceNo");
        pref = getSharedPreferences("Config", MODE_PRIVATE);

        roundoffPrecision = pref.getInt("RoundOffPrecision",0);
        Calendar C = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        currentDate = df.format(C.getTime()) + "000000";

        db = new DatabaseHandler(getApplicationContext());

        if (complimentaryFlag) {

//            orderId = db.getComplimentaryOrderId(scheduleLineId, parentInvoiceNo);
            orderId = db.getComplimentaryOrderId(scheduleLineId, parentOrderId);
            customerCategoryId = db.getcustcat(scheduleLineId);
            customerId = "0";
            customerType = db.getcusttype(scheduleLineId);

        } else {
            orderId = db.getorderid(scheduleLineId);
            customerCategoryId = db.getcustcat(scheduleLineId);
            customerId = db.getcustomerid(scheduleLineId);
            customerType = db.getcusttype(scheduleLineId);
        }

        if (complimentaryFlag) {

            customerNameTv.setText(COMPLIMENT_CUSTOMER_NAME);

        } else {
            String temp = db.getcustName(scheduleLineId);

            customerNameTv.setText(temp);

        }

        packageTypeMap = db.getPackageTypes();

        packageTypeList.addAll(packageTypeMap.keySet());

        if (orderId.contentEquals("0")) {
            approved = false;
            savedData = new HashMap<>();

            BigDecimal creditBalance = db.getCreditBalance(scheduleLineId);
//            Log.e("Credit Balance",creditBalance.toString());
            if (creditBalance.compareTo(new BigDecimal(0))==1){

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        OrderTakingActivity.this);
                builder.setTitle(R.string.credit_exists_confirmation_title);
                builder.setMessage( getResources().getString(R.string.credit_exists_confirmation_msg)+" "+ creditBalance.toString());
                builder.setPositiveButton(R.string.continue_caps,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                            }
                        });

                builder.setNegativeButton(R.string.cancelbtn,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i=new Intent(OrderTakingActivity.this,CustomerListActivity.class);
                                finish();
                                startActivity(i);
                                overridePendingTransition(0,0);

                            }
                        });
                builder.show();
               // return;
            }

        } else {

            savedData = db.getSavedOrderLineData(orderId);
            setSummaryText(savedData);

        }
        approved = db.isorderapproved(orderId);

//        invoiceno = db.isinvoicenogenerated(orderId);


        itemClickListener = this;
        //focusChangedListener = this ;
        showAllFilterBtn.setOnClickListener(this);
        brandFilterBtn.setOnClickListener(this);
        formFilterBtn.setOnClickListener(this);
        categoryFilterBtn.setOnClickListener(this);

        finalProductMap = db.getproductmap("All", "All", "All", "",
                0,currentDate,customerCategoryId,customerType,customerId);


        loadItemList("All", "All", "All", "");

        searchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                if (s.length() > 0) {
                    srchbtn.setImageResource(R.drawable.ic_dhi_deletetextn);
                } else {
                    srchbtn.setImageResource(R.drawable.ic_dhi_new_search);
                }

                loadItemList(selectedCategory, selectedForm, selectedBrand, s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finish();
        overridePendingTransition(0,0);
        Intent i=new Intent(OrderTakingActivity.this,CustomerListActivity.class);
        startActivity(i);

    }

    private void loadItemList(String categoryFilter, String formFilter, String brandFilter,
                              String searchText) {

        selectedCategory = categoryFilter;
        selectedForm = formFilter;
        selectedBrand = brandFilter;
        searchString = searchText;
        selection = 0;


        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.items);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        String invoiceno = db.getInvoiceNo(orderId) ;
        productmap = db.getproductmap(selectedCategory, selectedBrand,
                selectedForm, searchString, selection, currentDate, customerCategoryId,
                customerType, customerId);

        adapter = new OrderTakingItemListAdapter(OrderTakingActivity.this, productmap, savedData,
                packageTypeList, itemClickListener, pref, invoiceno,scheduleLineId,complimentaryFlag );

        recyclerView.setAdapter(adapter);


    }


    @Override
    public void onRecyclerViewItemClick(View view, Product product, int position) {
        Intent intent = new Intent(OrderTakingActivity.this,
                ItemDetailsActivity.class);
        //finish();
        Bundle bundle = getIntent().getExtras();
        scheduleLineId = bundle.getString("scheduledetailid");
        complimentaryFlag = bundle.getBoolean("complimentary");
        parentOrderId =bundle.getString("parentOrderId");
        String invoiceno = db.getInvoiceNo(orderId) ;
//        parentInvoiceNo = bundle.getString("parentInvoiceNo");
        String productId=product.id;
        String productName=product.name;
        String rate=product.rate;
        String mrp=product.mrp;
        intent.putExtra("scheduledetailid",scheduleLineId)
                .putExtra("complimentary",complimentaryFlag)
                .putExtra("parentOrderId",parentOrderId)
//                .putExtra("parentInvoiceNo",parentInvoiceNo)
                .putExtra("productId",productId)
                .putExtra("productName",productName)
                .putExtra("customerId",customerId)
                .putExtra("invoiceno",invoiceno)
                .putExtra("rate",rate)
                .putExtra("mrp",mrp)
                .putExtra("customerCategoryId",customerCategoryId);
        overridePendingTransition(0,0);
        startActivity(intent);
    }


    public void saveOrder(View v) {
        savedData = adapter.getSavedData();

       //If Order is already approved then go to Item List Screen
        if (approved) {
            goToItemListActivity();
            /*finish();
            overridePendingTransition(0,0);
            Intent intent = new Intent(OrderTakingActivity.this,
                    ItemListActivity.class);
            intent.putExtra("scheduledetailid", scheduleLineId);
//            intent.putExtra("invoiceno", invoiceno);
            startActivity(intent);*/

        } else {
            //If not approved then update OrderLine

//            String parentOrderId ="";
            HashMap<String,OrderLine> parentOrderLineMap =  new HashMap<>();



            if(complimentaryFlag){
//                parentOrderId = db.getorderid(scheduleLineId);
                parentOrderLineMap = db.getSavedOrderLineData(parentOrderId);
            }

            /**
             * Check van inventory if the user have inventory
             */
            if (!db.isWithoutInventory()) {


                HashMap<String,OrderLine> temp = new HashMap<>();
                for (Map.Entry<String, OrderLine> entry : savedData.entrySet()) {

                    String tempProductId = entry.getKey();
                    OrderLine line = entry.getValue();
                    double qty = line.getQty();

                    if(complimentaryFlag){
                        tempProductId =  entry.getKey().split("_")[0];
                        OrderLine parentOrderLine = parentOrderLineMap.get(tempProductId);

                        if (parentOrderLine != null){
                            qty =qty + parentOrderLine.getQty();
                        }
                    }

                    final String productId =tempProductId;
                    double availableqty = db.getavailableqty(productId,
                            pref.getString("tmsOrgId", ""), db.getInvoiceNo(orderId));

//                    double availableqty = db.getavailableqty(productId, pref.getString("tmsOrgId", ""), invoiceno);
                    //   String productname=db.getProductName(productId);


                    if (availableqty < qty) {

                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.Maximum_available_stock_for)
                                        +" "  + line.getProductName() +" "+
                                        getResources().getString(R.string.is)+" " + availableqty,
                                Toast.LENGTH_LONG).show();

                        if(complimentaryFlag){
                            return ;
                        }
                        //savedData.remove(productId);

                    }else{
                        temp.put(entry.getKey(),line);
                    }
                }

                savedData = new HashMap<>();
                savedData.putAll(temp);

            }


            if (savedData.size() <= 0) {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.No_Items_Added_to_the_Cart), Toast.LENGTH_LONG).show();
                /*finish();
                startActivity(getIntent());*/
                return ;

            }

            String personId = pref.getString("personid", "");


            //			Vehicletracker tracker = new Vehicletracker();
            PlayLocation tracker = new PlayLocation();
            Double lat = tracker.getlattitude();
            Double lng = tracker.getlongitude();
            if (orderId.contentEquals("0")) {

              /*  if (db.isInvoiceNoGenTableEmpty()){
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.invoice_no_not_configured),
                            Toast.LENGTH_LONG).show();
                    return ;
                }*/

                try {
                    webconfigurration web = new webconfigurration(getApplicationContext());
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                    String createdtime = df.format(c.getTime());
                    db.insertorder(scheduleLineId, personId, createdtime,
                            finalProductMap, savedData, packageTypeMap, String.valueOf(lat),
                            String.valueOf(lng), customerId, "",web.taxType,parentOrderId);

                    if (complimentaryFlag) {
                        orderId = db.getComplimentaryOrderId(scheduleLineId, parentOrderId);
//                        orderId = db.getComplimentaryOrderId(scheduleLineId, parentInvoiceNo);
                    } else {
                        orderId = db.getorderid(scheduleLineId);
                    }

                 //   Toast.makeText(getApplicationContext(), "orderId :" + orderId, Toast.LENGTH_LONG).show();
                  /*  nos.setText(String.valueOf(qtymap.size()) + " NOS ");
                    String amot = db.getorderamt(orderid);
                    amt.setText(amot == null ? "0.0 INR" : amot + " INR");*/
                } catch (DateError e) {
                    callDateTimeSetting();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {

               if(db.isPaymentApproved(scheduleLineId) ){

                    AlertDialog.Builder builder = new AlertDialog.Builder( OrderTakingActivity.this);
                    builder.setTitle(R.string.Already_Payment_Collected);
                    builder.setMessage(R.string.want_to_cancel_Payment);
                    builder.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                                    try {
                                        Calendar c = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat(
                                                "yyyyMMddHHmmss");
                                        String cancelTime = df.format(c.getTime());
                                        db.cancelPayment(scheduleLineId, orderId,cancelTime);

                                        updateAndLoadItemListActivity();

                                    } catch (DateError e) {
                                        callDateTimeSetting();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                    builder.setNeutralButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    goToItemListActivity();
                                }
                            });
                    builder.show();
               }else {

                   updateAndLoadItemListActivity();

              }
            }


        }
    }

    private void updateAndLoadItemListActivity(){

        try {

            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
            String createdtime = df.format(c.getTime());
            webconfigurration web = new webconfigurration(getApplicationContext());
            db.updateorder(orderId, createdtime, finalProductMap,
                    savedData, packageTypeMap, scheduleLineId,web.taxType);

            /*if (invoiceno.length() == 0) {
                invoiceno = db.invoicenogenerator(orderId);
            }*/

            if (complimentaryFlag) {
                db.updateComplimentaryFlag(orderId);
            }

            goToItemListActivity();


        } catch (DateError e) {
            callDateTimeSetting();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.error_try_again),
                    Toast.LENGTH_SHORT).show();

        }
    }

    public void goToItemListActivity(){
        finish();
        overridePendingTransition(0, 0);

        Intent intent = new Intent(OrderTakingActivity.this,
                ItemListActivity.class);
        intent.putExtra("scheduledetailid", scheduleLineId);
        startActivity(intent);
    }

    public void callDateTimeSetting() {


        Toast.makeText(getApplicationContext(), getResources().getString( R.string.correct_mob_date_time),
                Toast.LENGTH_LONG).show();

        Intent dateSetttingIntent = new Intent(android.provider.Settings.ACTION_DATE_SETTINGS);
        startActivityForResult(dateSetttingIntent, REQUEST_CODE_DATE_SETTING);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_DATE_SETTING) {
            finish();
            startActivity(getIntent());
            overridePendingTransition(0, 0);
        }

    }



    public void setSummaryText(HashMap<String, OrderLine> data) {


        if ((!approved) && db.isAnyPreviousPaymentPending(scheduleLineId)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    OrderTakingActivity.this);
            builder.setTitle(R.string.pending_payments);
            builder.setMessage(R.string.Previous_Invoice_Pending);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Intent i=new Intent(OrderTakingActivity.this,CustomerListActivity.class);
                            finish();
                            startActivity(i);
                            overridePendingTransition(0,0);

                        }
                    });


            builder.show();
        } else {



            this.savedData = data;

            BigDecimal totalAmount = new BigDecimal(0) ;
            int itemCount =0 ;

            for (Map.Entry<String, OrderLine> entry : savedData.entrySet()) {

                String tempProductId = entry.getKey();
                OrderLine line = entry.getValue();

                if(complimentaryFlag){
                    tempProductId =  entry.getKey().split("_")[0];
                }


                final String productId =tempProductId;
//                double baseunitRate = line.getRate();
                double qty = line.getQty();
//                double discountAmnt =line.getDiscountAmount();
//                double rate = baseunitRate;

                BigDecimal baseunitRate = line.getRate();
                BigDecimal discountAmnt = line.getDiscountAmount();
                BigDecimal rate = baseunitRate;
                BigDecimal amount = new BigDecimal(0) ;

                try {
                    JSONObject baseunitqtyObject = db.getBaseUnitQty(productId, packageTypeMap.get(line.getPackageTypeCode()) , qty);

                    double conFactor = baseunitqtyObject.optDouble("ConversionFactor", 1);

//                    rate = baseunitRate * conFactor;
                    rate = baseunitRate.multiply(new BigDecimal(conFactor));

                } catch (JSONException e) {

                    e.printStackTrace();
                }

                if (qty > 0){
                    itemCount = itemCount +1 ;
                }
               /* double amount = rate * qty;
                amount = amount - discountAmnt ;
                totalAmount = totalAmount + amount;*/

                amount = rate.multiply(new BigDecimal(qty)) ;
                amount = amount.subtract(discountAmnt);

                totalAmount = totalAmount.add(amount) ;

            }

            /*DecimalFormat twoDForm = new DecimalFormat("#.##");
            totalAmount = Double.valueOf(twoDForm.format(totalAmount));
            totalAmount = Math.round(totalAmount);*/

            if (roundoffPrecision == 0){
                totalAmount = totalAmount.setScale(2,BigDecimal.ROUND_HALF_UP /*ROUND_HALF_EVEN*/);
            }


            totalAmount = totalAmount.setScale(roundoffPrecision,BigDecimal.ROUND_HALF_UP);
            amountTextView.setText(totalAmount + "");
            itemCountTextView.setText(itemCount + "");
            if(complimentaryFlag){
                amountTextView.setText( "0");
            }

        }
    }

    

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.imgNavigationcustdtl3:
                Intent i = new Intent(OrderTakingActivity.this, EditCustomerActivity.class);

                i.putExtra("scheduledetailid", scheduleLineId);
                finish();
                startActivity(i);
                overridePendingTransition(0, 0);
                break;
            case R.id.imgNavigationcustdtl2:
                i = new Intent(OrderTakingActivity.this, InvoiceMakePaymentActivity.class);

                i.putExtra("status", "");
                i.putExtra("scheduledetailid", scheduleLineId);
                finish();
                overridePendingTransition(0, 0);
                startActivity(i);
                break;
            case R.id.imgNavigationcustdtl1:
                i = new Intent(OrderTakingActivity.this, ShopActivity.class);
                i.putExtra("scheduledetailid", scheduleLineId);
                finish();
                startActivity(i);
                overridePendingTransition(0, 0);
                break;

            case R.id.showAllBtn:
                showFilterPopup("All", v);

                break;

            case R.id.brandBtn:

                showFilterPopup("Brand", v);
                break;

            case R.id.categoryBtn:
                showFilterPopup("Category", v);
                break;

            case R.id.formBtn:
                showFilterPopup("Form", v);
                break;

            case R.id.imgNoteBtn :

                 showNotePopup(v);
                break;

        }
    }

    private void showNotePopup(View v) {

       // if (!orderId.contentEquals("0"))  {

            LayoutInflater layoutInflater = (LayoutInflater) OrderTakingActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View customView = layoutInflater.inflate(R.layout.forceclr_popuplayout, null);
            Button closePopupBtn = (Button) customView.findViewById(R.id.close);
            Button confirm = (Button) customView.findViewById(R.id.yes);
            TextView title = (TextView) customView.findViewById(R.id.header);
            TextView msg = (TextView) customView.findViewById(R.id.message);
            LinearLayout btnlayout=(LinearLayout) findViewById(R.id.btnlayout);

            final EditText pass = (EditText) customView.findViewById(R.id.forcepass);
            pass.setSingleLine(false);
            title.setText(R.string.Notes);
            msg.setText("");


            //instantiate popup window

            int width = LinearLayout.LayoutParams.MATCH_PARENT;
            int height = LinearLayout.LayoutParams.MATCH_PARENT;
            final PopupWindow popupWindow = new PopupWindow(customView, width, height);
            popupWindow.setFocusable(true);

            //display the popup window
            popupWindow.showAtLocation(btnlayout, Gravity.CENTER, 0, 0);
            popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            //close the popup window on button click
            closePopupBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                }
            });

            String Notedatas = db.readordernote(orderId);
            if(Notedatas != null)
            {
                if (Notedatas.length() >0){
                    pass.setText(Notedatas);
                }
            }



            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String note_value = pass.getText() .toString();
                    if (note_value != null) {
                        db.addordernote(orderId, note_value);
                    }

                    popupWindow.dismiss();

                }
            });



       // }
    }

    private void showFilterPopup(final String filterString, View v) {

        if (filterString.equals("All")) {

            loadItemList("All", "All", "All", searchString);

        } else {

            List<String> filterList = new ArrayList<>();
            HashMap<String, String> filterDataMap = new HashMap<>();

            if (filterString.equals("Category")) {

                filterList = db.getallproductcats();
                filterDataMap = db.getproductCatMap();


            } else if (filterString.equals("Brand")) {
                filterList = db.getallproductbrands(selectedCategory);
                filterDataMap = db.getproductBrandMap(selectedCategory);
            } else if (filterString.equals("Form")) {

                filterList = db.getallproductforms(selectedCategory, selectedBrand);
                filterDataMap = db.getproductFormMap(selectedCategory, selectedBrand);

            }

            final View customView = inflater.inflate(R.layout.popup_filter, null);
            TextView header = (TextView) customView.findViewById(R.id.header);

            header.setText(filterString);
            ImageButton closePopupBtn = (ImageButton) customView.findViewById(R.id.close);

            //instantiate popup window
            int width = LinearLayout.LayoutParams.MATCH_PARENT;
            int height = LinearLayout.LayoutParams.MATCH_PARENT;

            final PopupWindow popupWindow = new PopupWindow(customView, width, height);
            popupWindow.setFocusable(true);

            //display the popup window
            popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
            popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            //close the popup window on button click
            closePopupBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                }
            });


            ListView listView = (ListView) customView.findViewById(R.id.list);
            final List<String> list = filterList;
            final HashMap<String, String> hashMap = filterDataMap;
            ListAdapter arrayadapter = new BaseAdapter() {

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View mView = inflater.inflate(R.layout.adapter_cat_list, null);
                    final TextView textView = (TextView) mView
                            .findViewById(R.id.textView);
                    textView.setText(getItem(position));
                    textView.setTag(getItemid(position));
                    mView.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            if (filterString.equals("Category")) {
                                selectedCategory = textView.getTag().toString();
                                selectedBrand = "All";
                                selectedForm = "All";

//                                loadItemList(textView.getTag().toString(),"All","All",searchString);
                            } else if (filterString.equals("Brand")) {
                                selectedBrand = textView.getTag().toString();
                                selectedForm = "All";

//                                loadItemList(selectedCategory,"All",textView.getTag().toString(),searchString);
                            } else if (filterString.equals("Form")) {
                                selectedForm = textView.getTag().toString();
//                                loadItemList(selectedCategory,textView.getTag().toString(),selectedBrand,searchString);
                            }

                            loadItemList(selectedCategory, selectedForm, selectedBrand, searchString);
                            popupWindow.dismiss();

                            v.setBackgroundResource(R.drawable.cansel_corner);

                        }

                    });
                    return mView;
                }

                private String getItemid(int position) {
                    return list.get(position);
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public String getItem(int position) {
                    return hashMap.get(list.get(position));
                }

                @Override
                public int getCount() {
                    return list == null ? 0 : list.size();
                }
            };
            listView.setAdapter(arrayadapter);

        }


    }


    public void deleteText(View view) {

        searchEditText.setText("");

        loadItemList(selectedCategory, selectedForm, selectedBrand, "");

    }
}


