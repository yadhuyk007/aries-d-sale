package dsale.dhi.com.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import dsale.dhi.com.ariesdsale.R;

public class DashboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard2);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
    }
}
