package dsale.dhi.com.order;

import java.math.BigDecimal;

public class OrderLine {
    int orderId  = 0;
    int productId  = 0;
    int packageTypeId  = 0;
    String productName  = "";
    String packageTypeCode  = "";
    double qty  = 0;
    BigDecimal rate  =  new BigDecimal(0);
    BigDecimal discountPer  =  new BigDecimal(0);
    BigDecimal discountAmount  =  new BigDecimal(0);
    boolean complimentFlag  = false;
    BigDecimal cgstTaxAmount  = new BigDecimal(0);
    BigDecimal sgstTaxAmount  =  new BigDecimal(0);
    BigDecimal otherTaxAmount  =  new BigDecimal(0);
    long taxId  = 0;
    BigDecimal taxRate  =  new BigDecimal(0);
    BigDecimal totalTaxAmount  =  new BigDecimal(0);
    BigDecimal taxableAmount  =  new BigDecimal(0);
    BigDecimal totalAmount  =  new BigDecimal(0);
    int batchId =0;
    String batch ="DEFAULT";
    BigDecimal partyMargin =  new BigDecimal(0);

    public OrderLine(){

    }

    public OrderLine(int orderId, int productId,String productName, int packageTypeId, String packageTypeCode, double qty, BigDecimal rate) {
        this.orderId = orderId;
        this.productId = productId;
        this.packageTypeId = packageTypeId;
        this.packageTypeCode = packageTypeCode;
        this.qty = qty;
        this.rate = rate;
        this.productName = productName ;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getPackageTypeId() {
        return packageTypeId;
    }

    public void setPackageTypeId(int packageTypeId) {
        this.packageTypeId = packageTypeId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPackageTypeCode() {
        return packageTypeCode;
    }

    public void setPackageTypeCode(String packageTypeCode) {
        this.packageTypeCode = packageTypeCode;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    /*public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }*/

    public int getBatchId() {
        return batchId;
    }

    public void setBatchId(int batchId) {
        this.batchId = batchId;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

  /*  public double getDiscountPer() {
        return discountPer;
    }

    public void setDiscountPer(double discountPer) {
        this.discountPer = discountPer;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }*/

    public boolean isComplimentFlag() {
        return complimentFlag;
    }

    public void setComplimentFlag(boolean complimentFlag) {
        this.complimentFlag = complimentFlag;
    }

  /*  public double getCgstTaxAmount() {
        return cgstTaxAmount;
    }

    public void setCgstTaxAmount(double cgstTaxAmount) {
        this.cgstTaxAmount = cgstTaxAmount;
    }

    public double getSgstTaxAmount() {
        return sgstTaxAmount;
    }

    public void setSgstTaxAmount(double sgstTaxAmount) {
        this.sgstTaxAmount = sgstTaxAmount;
    }*/

    public long getTaxId() {
        return taxId;
    }

    public void setTaxId(long taxId) {
        this.taxId = taxId;
    }

   /* public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public double getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxAmount(double totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    public double getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(double taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }



    public double getPartyMargin() {
        return partyMargin;
    }

    public void setPartyMargin(double partyMarging) {
        this.partyMargin = partyMarging;
    }

    public double getOtherTaxAmount() {
        return otherTaxAmount;
    }

    public void setOtherTaxAmount(double otherTaxAmount) {
        this.otherTaxAmount = otherTaxAmount;
    }*/

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getDiscountPer() {
        return discountPer;
    }

    public void setDiscountPer(BigDecimal discountPer) {
        this.discountPer = discountPer;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getCgstTaxAmount() {
        return cgstTaxAmount;
    }

    public void setCgstTaxAmount(BigDecimal cgstTaxAmount) {
        this.cgstTaxAmount = cgstTaxAmount;
    }

    public BigDecimal getSgstTaxAmount() {
        return sgstTaxAmount;
    }

    public void setSgstTaxAmount(BigDecimal sgstTaxAmount) {
        this.sgstTaxAmount = sgstTaxAmount;
    }

    public BigDecimal getOtherTaxAmount() {
        return otherTaxAmount;
    }

    public void setOtherTaxAmount(BigDecimal otherTaxAmount) {
        this.otherTaxAmount = otherTaxAmount;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public BigDecimal getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxAmount(BigDecimal totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    public BigDecimal getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(BigDecimal taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getPartyMargin() {
        return partyMargin;
    }

    public void setPartyMargin(BigDecimal partyMargin) {
        this.partyMargin = partyMargin;
    }
}
