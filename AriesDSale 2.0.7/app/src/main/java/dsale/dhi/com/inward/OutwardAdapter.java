package dsale.dhi.com.inward;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dsale.dhi.com.ariesdsale.Product;
import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.database.DatabaseHandler;

public class OutwardAdapter extends RecyclerView.Adapter<OutwardAdapter.ViewHolder> {

    Context mcontext;
    View mview;
    private List<String> productids;
    private HashMap<String, Product> producthashmap;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private DatabaseHandler db;
    private HashMap<String, String> packageTypeMap;
    private HashMap<String, String> batchdb;
    private List<String> packageTypeList = new ArrayList<>();
    private List<String> batchlist = new ArrayList<>();
    private HashMap<String, String> qtymap;
    private HashMap<String, String> casemap;
    private HashMap<String, String> batchmap;
    private TextView nos;


    // data is passed into the constructor
//    InwardOutwardAdapter(Context context, List<String> data, HashMap<String, Product> data1,HashMap<String, String> qtymap,View v ) {
    OutwardAdapter(Context context, List<String> data, HashMap<String, Product> data1, HashMap<String, String> qtymap, View v, HashMap<String, String> casemap, HashMap<String, String> batchmap) {
        this.mInflater = LayoutInflater.from(context);
        Log log;
        this.producthashmap = data1;
        this.productids = data;
        this.mcontext = context;
        this.qtymap = qtymap;
        this.mview = v;
        this.casemap = casemap;
        this.batchmap = batchmap;
        db = new DatabaseHandler(context);
        packageTypeMap = db.getPackageTypes();
        for (String key : packageTypeMap.keySet()) {
            packageTypeList.add(key);
        }
        batchdb = db.getBatches();
        for (String key : batchdb.keySet()) {
            batchlist.add(key);
        }


    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.inward_outward_row, parent, false); // make_payment_row payment_approve_row
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String prdtid = productids.get(position);
//        String status = producthashmap.get(position);
//        Log.d("myTag", animal);
        //lin = itemView.findViewById(R.id.cashbtn);
        holder.myTextView.setText(getItemName(position));
        holder.rate.setText(getItemDisc(position));
        holder.qty.setTag(getItem(position));
        holder.packagespinner.setTag(getItem(position));
        holder.batchspinner.setTag(getItem(position));


        if (casemap.containsKey(holder.packagespinner.getTag().toString())) {
            // sp.setText(qtymap.get(edit.getTag().toString()));
            int pos = packageTypeList.indexOf(casemap.get(holder.packagespinner.getTag().toString()));
            holder.packagespinner.setSelection(pos);
        } else
            holder.packagespinner.setSelection(0);

        if (qtymap.containsKey(holder.qty.getTag().toString()))
            holder.qty.setText(qtymap.get(holder.qty.getTag().toString()));
        else
            holder.qty.setText("");
//
        if (batchmap.containsKey(holder.batchspinner.getTag().toString())) {

            int posi = batchlist.indexOf(batchmap.get(holder.batchspinner.getTag().toString()));
            holder.batchspinner.setSelection(posi);
        } else
            holder.batchspinner.setSelection(0);


    }

    private String getItemName(int position) {
        return producthashmap.get(productids.get(position)).name;
    }

    // total number of rowsholder.
    @Override
    public int getItemCount() {
        return productids.size();
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return productids.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    private Spanned getItemDisc(int position) {
        String val = "MRP: "
                + producthashmap.get(productids.get(position)).mrp
                + " Rate: "
                + producthashmap.get(productids.get(position)).rate;

        double dominqty = 0;
        try {
            dominqty = Double.parseDouble(producthashmap.get(productids
                    .get(position)).minqty);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (dominqty > 0) {
            val += producthashmap.get(productids.get(position)).minqty
                    .contentEquals("") ? "" : "<b> MOQ: "
                    + producthashmap.get(productids.get(position)).minqty
                    + "</b>" + " ";

        }
        return Html.fromHtml(val);
        // return val;
    }

    private SpinnerAdapter initializepackage() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mcontext, android.R.layout.simple_spinner_item, packageTypeList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#606060"));
                    tv.setTextSize(12);
                    String fontPath = "font/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            mcontext.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        dataAdapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        return dataAdapter;
    }

    private SpinnerAdapter initializebatch() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mcontext, android.R.layout.simple_spinner_item, batchlist) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#606060"));
                    tv.setTextSize(12);
                    String fontPath = "font/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            mcontext.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        dataAdapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        return dataAdapter;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        TextView rate;
        EditText qty;
        Spinner batchspinner;
        Spinner packagespinner;

        ViewHolder(final View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.itemname);
            rate = itemView.findViewById(R.id.mrp);
            qty = itemView.findViewById(R.id.qty);
            itemView.setOnClickListener(this);
            nos = mview.findViewById(R.id.nos);

            packagespinner = itemView.findViewById(R.id.packagespinner);
            batchspinner = itemView.findViewById(R.id.batchspinner);
            SpinnerAdapter packagesAdapter = initializepackage();
            SpinnerAdapter batchAdapter = initializebatch();
            packagespinner.setAdapter(packagesAdapter);
            batchspinner.setAdapter(batchAdapter);

            //=========================for nos calculation


//
            packagespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
                    // Global.Toast(CustomerDashboardActivity.this, sp
                    // .getAdapter().getItem(arg2).toString(),
                    // Toast.LENGTH_SHORT, Font.Regular);
                    casemap.put(packagespinner.getTag().toString(), packagespinner.getAdapter()
                            .getItem(arg2).toString());

                    //						if (!approved) {
                    //							// Global.Toast(CustomerDashboardActivity.this,
                    //							// "saving", Toast.LENGTH_SHORT,
                    //							// Font.Regular);
                    //							save();
                    //						}
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {

                }
            });
//
            batchspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
//                     Global.Toast(CustomerDashboardActivity.this, sp
//                     .getAdapter().getItem(arg2).toString(),
//                     Toast.LENGTH_SHORT, Font.Regular);

                    batchmap.put(batchspinner.getTag().toString(), batchspinner.getAdapter()
                            .getItem(arg2).toString());
//                    						if (!approved) {
//                    							 Global.Toast(CustomerDashboardActivity.this,
//                    							 "saving", Toast.LENGTH_SHORT,
//                    							 Font.Regular);
//                    							save();
//                    						}
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {

                }
            });

            qty.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    // Toast.makeText(getApplicationContext(),
                    // edit.getTag().toString(), Toast.LENGTH_SHORT)
                    // .show();


                    if (s.toString().length() > 0) {
                        qtymap.put(qty.getTag().toString(), s.toString());

                        //						   nos.setText(String.valueOf(qtymap.size()) + " NOS ");
                    } else {
                        qtymap.remove(qty.getTag().toString());
                    }
                    String batch = batchspinner.getSelectedItem().toString();
                    batchmap.put(batchspinner.getTag().toString(),batch);
                    String pack=packagespinner.getSelectedItem().toString();
                    casemap.put(packagespinner.getTag().toString(),pack);



                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {

                    nos.setText(String.valueOf(qtymap.size()) + " NOS ");
                }
            });
//
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

}