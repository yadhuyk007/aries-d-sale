package dsale.dhi.com.ariesdsale;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.Global;
import dsale.dhi.com.objects.Scheduledata;
import dsale.dhi.com.objects.Scheduledetail;
import dsale.dhi.com.objects.Stacklogger;
import dsale.dhi.com.objects.SupportFunctions;
import dsale.dhi.com.objects.product;
import dsale.dhi.com.objects.webconfigurration;

public class LoginActivity extends AppCompatActivity {
    private static String Routnetworkid = "";
    ProgressDialog mProgress;
    private SharedPreferences pref;
    private JSONArray query;
   // private DatabaseHandler db;
    private String paramDate;
    private String code;
    private String password;
    private int REQUEST_CODE_DATE_SETTING = 0;

    private Button salesSignIn ;
    RelativeLayout loginLayout ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginLayout = (RelativeLayout) findViewById(R.id.rightPanel);
        salesSignIn = (Button) findViewById(R.id.salesSignIn);
        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));
        pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);



        try {
            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(getApplicationContext());

            PackageManager manager = getApplicationContext()
                    .getPackageManager();
            PackageInfo info = manager.getPackageInfo(getApplicationContext()
                    .getPackageName(), 0);

            int versionCode = sharedPreferences.getInt("VERSION_CODE",
                    info.versionCode);
            String ver = info.versionName;
//            TextView tx = (TextView) findViewById(R.id.resetPassword2);
//            tx.setText("V " + ver);
            if (versionCode != info.versionCode) {
                onAppUpdated();
            }

            sharedPreferences.edit().putInt("VERSION_CODE", info.versionCode)
                    .apply();
        } catch (Exception e) {
            e.printStackTrace();
        }

        checkversion();

        Calendar c = Calendar.getInstance();
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        String date = sdf.format(c.getTime());
        if (Global.date.contentEquals(""))
            paramDate = "'" + date + "'";
        else {
            SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date dateParam = format1.parse(Global.date);
                String param = format2.format(dateParam);
                paramDate = "'" + param + "'";
            } catch (Exception e) {
                e.printStackTrace();
                paramDate = "'" + date + "'";
            }
        }

        PackageManager manager = getApplicationContext()
                .getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(getApplicationContext()
                    .getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String ver = info.versionName;
        TextView tx = (TextView) findViewById(R.id.resetPassword2);
        tx.setText("V " + ver);
        String loginstring = pref.getString("datafromlogin", "");
        Log.d("refre", loginstring);
        Intent login = getIntent();
        boolean flag = login.getBooleanExtra("refresh", false);
        String logdata = login.getStringExtra("prefer");
        try {
            if (flag == true) {

                try {
                    info = manager.getPackageInfo(getApplicationContext()
                            .getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                if(info.versionCode < pref.getInt("version",0)){
                    Intent intent = new Intent(LoginActivity.this, DownloadActivity.class);
                    finish();
                    overridePendingTransition(0,0);
                    startActivity(intent);
                    return;
                }

                loginLayout.setVisibility(View.GONE);
                ImageView icon = (ImageView) findViewById(R.id.icon);
                icon.setVisibility(View.GONE);
                //mProgress.dismiss();
                if (mProgress != null) {
                    mProgress.dismiss();
                }
                ProgressDialog mProgress = new ProgressDialog(LoginActivity.this);

                mProgress.setView(getLayoutInflater().inflate(R.layout.adapter_loading, null));
                mProgress.setTitle("");
                mProgress.setMessage("Loading...");
                mProgress.setCancelable(false);
                mProgress.setIndeterminate(true);
                mProgress.create();

                if (!mProgress.isShowing()){
                    mProgress.show();
                }



                Log.d("refresh", "refresh");
             //   db = new DatabaseHandler(getApplicationContext());
                getSchedule(logdata);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (!pref.getString("username", "").contentEquals("") && Global.refresh && !loginstring.contentEquals("")) {

                try {
                    info = manager.getPackageInfo(getApplicationContext()
                            .getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                if(info.versionCode < pref.getInt("version",0)){
                    Intent intent = new Intent(LoginActivity.this, DownloadActivity.class);
                    finish();
                    overridePendingTransition(0,0);
                    startActivity(intent);
                    return;
                }

                getSchedule(loginstring);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!pref.getString("username", "").contentEquals("")){
            gotonextactivity();
        }else{
            try {
                info = manager.getPackageInfo(getApplicationContext()
                        .getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            //if(Global.isNewVersionAvailable(getApplicationContext())) {
            if(info.versionCode < pref.getInt("version",0)){
                Intent intent = new Intent(LoginActivity.this, DownloadActivity.class);
                finish();
                overridePendingTransition(0,0);
                startActivity(intent);
                return;
            }
        }

       // db = new DatabaseHandler(getApplicationContext());

    }

    private void checkversion() {
        JSONObject params = new JSONObject();
        try {
            params.put("AppName", webconfigurration.appname);
            params.put("Version", "1");
            params.put("email", "");
            params.put("password", "");
            String auth = webconfigurration.auth;
            auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
            params.put("auth", auth);
            params.put("Operation", "VersionCheck");

            new LongOperation().execute(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gotonextactivity() {
        Intent intent = new Intent(LoginActivity.this, CustomerListActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }

    public void login(View v) throws Exception {
        // Toast.makeText(getApplicationContext(), "Login", Toast.LENGTH_SHORT).show();
        EditText salesCode = (EditText) findViewById(R.id.salesUserCode);
        EditText passCode = (EditText) findViewById(R.id.password);
        code = salesCode.getText().toString();
        password = passCode.getText().toString();
        String email = Base64.encodeToString(code.getBytes(), Base64.NO_WRAP);
        String sendpswd = Base64.encodeToString(password.getBytes(), Base64.NO_WRAP);
        JSONObject data = new JSONObject();
        data.put("email", email);
        data.put("password", sendpswd);
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        data.put("auth", auth);
        new LoginOperation().execute(data.toString());
        try {
            v.setEnabled(false);
            mProgress = new ProgressDialog(LoginActivity.this);
            mProgress.setView(getLayoutInflater().inflate(R.layout.adapter_loading, null));
            mProgress.setTitle("");
            mProgress.setMessage("Loading...");
            mProgress.setCancelable(false);
            mProgress.setIndeterminate(true);
            mProgress.create();

            if (!mProgress.isShowing()){
                mProgress.show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

//        mProgress.dismiss();
    }

    private void getSchedule(String responsefrom) throws Exception {
        JSONObject obj = new JSONObject(responsefrom);
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String formattedDate = dateTimeFormat.format(c.getTime());
        String code = ((EditText) findViewById(R.id.salesUserCode)).getText().toString();
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        String personid = obj.getString("personid");
        Intent login = getIntent();
        boolean flag = login.getBooleanExtra("refresh", false);
        try {
            if (flag == true) {
                webconfigurration.status = "Refresh";
                code = login.getStringExtra("UserName");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (flag)
            webconfigurration.status = "Refresh";
        else {
            webconfigurration.status = "Login";
        }


        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + code
                + "\",\"DBname\": \""
                + obj.getString("orgDb")
                + "\", \"BussinessUnit\": \""
                + obj.getString("BussinessUnit")
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + "app38"
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + obj.getString("orgId") + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = new JSONObject(inputParamsStr);

        JSONObject updatestatus = null;
        try {
            updatestatus = SupportFunctions.getstatusobject(getApplicationContext(), "Refresh", personid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputParamObj.put("updatestatus", updatestatus);
        String branch = obj.optString("tmsOrgId", "");
        JSONObject data = new JSONObject();

        data.put("operation", "GetSchedules").put("data", obj.getString("PersonCode"))
                .put("PassCode", obj.getString("PassCode")).put("currdate", formattedDate)
                .put("personrole", obj.optString("PersonRole", "Designation"))
                .put("sDate", Global.date).put("personid", personid)
                .put("branch", branch).put("needcustomers", true);

        inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        try {
            operations(obj);
            inputParamObj.getJSONObject("data").put("query", query);
            inputParamObj.getJSONObject("data").put("service", "GetQuery");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        inputParamObj.put("auth", auth);
        //Log.e("getsync", inputParamObj.toString());
        //Toast.makeText(getApplicationContext(), inputParamObj.toString(), Toast.LENGTH_SHORT).show();
        new GetScheduleOperation().execute(inputParamObj.toString(), responsefrom);
        //getdata(inputParamObj.toString());
    }

    private void operations(final JSONObject obj) {
        query = new JSONArray();
        String personid = obj.optString("personid", "0");
        String tmsOrgId = obj.optString("tmsOrgId", "");

        DatabaseHandler db = new DatabaseHandler(getApplicationContext()) ;
        String scheduleId = db.getactualscheduleid();

        boolean ordt = false;
        boolean van = false;
        JSONArray services = obj.optJSONArray("services");
        for (int i = 0; i < services.length(); i++) {
            JSONObject temp = services.optJSONObject(i);
            if (temp.optString("srvcname").contentEquals("OrderTaking"))
                ordt = true;
            if (temp.optString("srvcname").contentEquals("Van Sales"))
                van = true;
        }
        if (ordt || van) {
            try {
                JSONObject queryobj = new JSONObject();
                JSONArray param = new JSONArray();
                queryobj.put("name", "GetExpenseTypesMobileQuery").put("params", param);
                query.put(queryobj);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                JSONObject queryobj = new JSONObject();
                JSONArray param = new JSONArray();
                queryobj.put("name", "GetProductCatMobileQuery").put("params",
                        param);
                query.put(queryobj);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                JSONObject queryobj = new JSONObject();
                JSONArray param = new JSONArray();
                queryobj.put("name", "GetBrandMobileQuery")
                        .put("params", param);
                query.put(queryobj);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                JSONObject queryobj = new JSONObject();
                JSONArray param = new JSONArray();
                queryobj.put("name", "GetFormsMobileQuery")
                        .put("params", param);
                query.put(queryobj);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                JSONObject queryobj = new JSONObject();
                JSONArray param = new JSONArray();
                queryobj.put("name", "GetProductsMobileQuery").put("params",
                        param);
                query.put(queryobj);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                JSONObject queryobj = new JSONObject();
                JSONArray param = new JSONArray();
                JSONObject paramob = new JSONObject();
                paramob.put("Name", "sDate").put("Val", paramDate);
                param.put(paramob);
                queryobj.put("name", "GetFocusedMobileQuery").put("params",
                        param);
                query.put(queryobj);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                JSONObject queryobj = new JSONObject();
                JSONArray param = new JSONArray();
                JSONObject paramob = new JSONObject();
                paramob.put("Name", "sDate").put("Val", paramDate);
                param.put(paramob);
                queryobj.put("name", "GetMinQtyMobileQuery").put("params",
                        param);
                query.put(queryobj);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                JSONObject queryobj = new JSONObject();
                JSONArray param = new JSONArray();
                JSONObject paramob = new JSONObject();
                paramob.put("Name", "AsOfDate").put("Val", paramDate);
                param.put(paramob);
                JSONObject paramobj = new JSONObject();
                paramobj.put("Name", "SalesPerson").put("Val",
                        personid);
                param.put(paramobj);
                queryobj.put("name", "GetAggInventoryMobileQuery").put(
                        "params", param);

                query.put(queryobj);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                JSONObject queryobj = new JSONObject();
                JSONArray param = new JSONArray();
                JSONObject paramob = new JSONObject();
                paramob.put("Name", "sDate").put("Val", paramDate);
                param.put(paramob);
                queryobj.put("name", "getCreditNoteReasons").put("params",
                        param);
                query.put(queryobj);
            } catch (Exception e) {
                e.printStackTrace();
            }
            loadProductRates(obj);
        }
        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramob = new JSONObject();
            paramob.put("Name", "sDate").put("Val", paramDate);
            param.put(paramob);
            queryobj.put("name", "getMapUrl").put("params", param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Organization Wise Mapping
        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramob = new JSONObject();
            paramob.put("Name", "person").put("Val",
                    personid);
            param.put(paramob);
            queryobj.put("name", "OrganizationServiceMappingQuery").put(
                    "params", param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramob = new JSONObject();
            paramob.put("Name", "sDate").put("Val", paramDate);
            param.put(paramob);
            queryobj.put("name", "GetCustomerTypeMobileQuery").put("params",
                    param);
            query.put(queryobj);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramob = new JSONObject();
            paramob.put("Name", "sDate").put("Val", paramDate);
            param.put(paramob);
            paramob = new JSONObject();
            paramob.put("Name", "person").put("Val",
                    personid);
            param.put(paramob);
            queryobj.put("name", "GetRegionsOfPersonQuery")
                    .put("params", param);
            query.put(queryobj);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramob = new JSONObject();
            paramob.put("Name", "person").put("Val", personid);
            param.put(paramob);
            queryobj.put("name", "PreviousInvoiceGetQry")
                    .put("params", param);
            query.put(queryobj);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramob = new JSONObject();
            paramob.put("Name", "Organization")
                    .put("Val", tmsOrgId);
            param.put(paramob);
            queryobj.put("name", "GetCurrencyDetails").put("params",
                    param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramob = new JSONObject();
            paramob.put("Name", "ScheduleDate").put("Val", paramDate);
            param.put(paramob);
            paramob = new JSONObject();
            paramob.put("Name", "PersonId").put("Val", personid);
            param.put(paramob);
            queryobj.put("name", "getCreditBalanceInScheduleForMbl")
                    .put("params", param);
            query.put(queryobj);

        } catch (Exception e) {
            e.printStackTrace();
        }

    //    DatabaseHandler  db = new DatabaseHandler(getApplicationContext());
        //Log.e("DB-excustneeded()","------------------------------------------------------");
        if (db.excustneeded())
            try {

                JSONObject queryobj = new JSONObject();
                JSONArray param = new JSONArray();
                JSONObject paramob = new JSONObject();
                paramob.put("Name", "Organization")
                        .put("Val", tmsOrgId);
                param.put(paramob);
                queryobj.put("name", "fetchAllCustomerQuery").put("params",
                        param);
                query.put(queryobj);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void loadProductRates(JSONObject obj) {
        String personid = obj.optString("personid", "0");
        String tmsOrgId = obj.optString("tmsOrgId", "");
        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramobj = new JSONObject();

            param.put(paramobj);
            queryobj.put("name", "GetPackageMobileQuery").put("params", param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramobj = new JSONObject();

            param.put(paramobj);
            queryobj.put("name", "GetProductPackageMappingMobileQuery").put("params", param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramobj = new JSONObject();
            paramobj.put("Name", "sDate");
            paramobj.put("Val", paramDate);
            param.put(paramobj);
            queryobj.put("name", "GetConsumerOffMobileQuery").put("params",
                    param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramobj = new JSONObject();
            paramobj.put("Name", "sDate");
            paramobj.put("Val", paramDate);
            param.put(paramobj);
            queryobj.put("name", "GetPartyMarginNewMobileQuery").put("params",
                    param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramobj = new JSONObject();
            paramobj.put("Name", "sDate");
            paramobj.put("Val", paramDate);
            param.put(paramobj);
            queryobj.put("name", "GetAddlDiscountMobileQuery").put("params",
                    param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramobj = new JSONObject();
            paramobj.put("Name", "sDate");
            paramobj.put("Val", paramDate);
            param.put(paramobj);
            queryobj.put("name", "GetMRPMobileQuery").put("params", param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramobj = new JSONObject();
            paramobj.put("Name", "sDate");
            paramobj.put("Val", paramDate);
            param.put(paramobj);
            queryobj.put("name", "GetUnitQuery").put("params", param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramobj = new JSONObject();
            paramobj.put("Name", "sDate");
            paramobj.put("Val", paramDate);
            param.put(paramobj);
            queryobj.put("name", "GetProductBatchMobileQuery").put("params", param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            queryobj.put("name", "GetInventoryAccountMobileQuery").put("params", param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramobj = new JSONObject();
            paramobj.put("Name", "Organization");
            paramobj.put("Val", tmsOrgId);
            param.put(paramobj);
            queryobj.put("name", "GetInvoiceNoForGreenMount").put("params", param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramobj = new JSONObject();
            paramobj.put("Name", "sDate");
            paramobj.put("Val", paramDate);
            param.put(paramobj);
            queryobj.put("name", "GetTaxDtlsQry").put("params", param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramobj = new JSONObject();
            paramobj.put("Name", "zDate");
            paramobj.put("Val", paramDate);
            param.put(paramobj);
            JSONObject paramobj2 = new JSONObject();
            paramobj2.put("Name", "Organization");
            paramobj2.put("Val", tmsOrgId);
            param.put(paramobj2);
            queryobj.put("name", "AvailableStockForMob").put("params", param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject queryobj = new JSONObject();
            JSONArray param = new JSONArray();
            JSONObject paramobj = new JSONObject();
            paramobj.put("Name", "Date");
            paramobj.put("Val", paramDate);
            param.put(paramobj);
            queryobj.put("name", "GetOffersMobileQuery").put("params", param);
            query.put(queryobj);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void loadschedulesandproducts(String incomingdata)
            throws JSONException {

        final JSONObject inputdata = new JSONObject(incomingdata);

        if (inputdata.getJSONObject("schedules").has("message")) {
            callDateTimeSetting();
            return;
        }

        // new edits of ordertaking
        JSONObject response = new JSONObject(incomingdata);
        JSONObject dataobj = response.optJSONObject("GetQuery").optJSONObject(
                "data");
        try {
            DatabaseHandler  db = new DatabaseHandler(getApplicationContext());
            //Log.e("schedulesandproducts","------------------------------------------------------");
            if (dataobj != null) {


                JSONArray region = dataobj
                        .optJSONArray("GetRegionsOfPersonQuery");
                if (region != null) {
                    db.insertintoregions(region);
                }
                JSONArray customertype = dataobj
                        .optJSONArray("GetCustomerTypeMobileQuery");
                if (customertype != null) {
                    db.insertintocutomertype(customertype);
                }
                JSONArray productcats = dataobj
                        .optJSONArray("GetProductCatMobileQuery");
                if (productcats != null)
                    db.insertintoproductcats(productcats);
                JSONArray brands = dataobj.optJSONArray("GetBrandMobileQuery");
                if (brands != null)
                    db.insertintobrands(brands);
                JSONArray forms = dataobj.optJSONArray("GetFormsMobileQuery");
                if (forms != null)
                    db.insertintoforms(forms);
                JSONArray products = dataobj
                        .optJSONArray("GetProductsMobileQuery");
                if (products != null)
                    db.insertintoproducts(products);
                JSONArray focusedprds = dataobj
                        .optJSONArray("GetFocusedMobileQuery");
                if (focusedprds != null)
                    db.insertintofocsdproducts(focusedprds);
                JSONArray inventory = dataobj
                        .optJSONArray("GetAggInventoryMobileQuery");
                if (inventory != null)
                    db.insertintoinventory(inventory);

                JSONArray party = dataobj
                        .optJSONArray("GetPartyMarginNewMobileQuery");
                if (party != null)
                    db.insertintoparty(party);
                JSONArray adldisc = dataobj
                        .optJSONArray("GetAddlDiscountMobileQuery");
                if (adldisc != null)
                    db.insertintoaddis(adldisc);
                JSONArray mrp = dataobj.optJSONArray("GetMRPMobileQuery");
                if (mrp != null)
                    db.insertintomrp(mrp);
                JSONArray units = dataobj.optJSONArray("GetUnitQuery");
                if (units != null)
                    db.insertintounits(units);
                /*JSONArray unitsmap = dataobj
                        .optJSONArray("GetProductUomMapQuery");
				if (unitsmap != null)
					db.insertintounitsmap(unitsmap);*/

                JSONArray creditnotereasons = dataobj
                        .optJSONArray("getCreditNoteReasons");
                if (creditnotereasons != null)
                    db.insertintocreditnotereasons(creditnotereasons);
                JSONArray getMapUrl = dataobj.optJSONArray("getMapUrl");
                if (getMapUrl != null)
                    db.insertintoconstants(getMapUrl);
                JSONArray excust = dataobj
                        .optJSONArray("fetchAllCustomerQuery");
                if (excust != null)
                    db.insertintoexcust(excust);
                JSONArray minqty = dataobj.optJSONArray("GetMinQtyMobileQuery");
                if (minqty != null)
                    db.insertintominqty(minqty);

                JSONArray productBatch = dataobj.optJSONArray("GetProductBatchMobileQuery");

                if (productBatch != null && productBatch.length() > 0) {
               //     DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                    if (db.isProductBatchTableEmpty()) {
                        db.loadProductBatch(productBatch);
                    }
                }


                JSONArray inventoryAccount = dataobj.optJSONArray("GetInventoryAccountMobileQuery");

                if (inventoryAccount != null && inventoryAccount.length() > 0) {
               //     DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                    if (db.isInventoryAccountTableEmpty()) {
                        db.loadInventoryAccount(inventoryAccount);
                    }
                }

                JSONArray tax = dataobj.optJSONArray("GetTaxDtlsQry");

                if (tax != null && tax.length() > 0) {
               //     DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                    if (db.isTaxTableEmpty()) {
                        db.loadTaxTables(tax);
                    }
                }

                JSONArray invoicenogen = dataobj.optJSONArray("GetInvoiceNoForGreenMount");

                if (invoicenogen != null && invoicenogen.length() > 0) {
           //        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                    if (db.isInvoiceNoGenTableEmpty()) {
                        db.loadInvoiceNoGen(invoicenogen);
                    }
                }
                //AvailableStockReport

                JSONArray availableStock = dataobj.optJSONArray("AvailableStockForMob");

                if (availableStock != null && availableStock.length() > 0) {
           //         DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                    if (db.isInvJrnlTableEmpty()) {
                        db.loadAvailableStock(availableStock);
                    }
                }

                JSONArray offers = dataobj.optJSONArray("GetOffersMobileQuery");
                if (offers != null && offers.length() > 0) {
                    db.loadOffers(offers);
                }

                JSONArray expenseTypes = dataobj.optJSONArray("GetExpenseTypesMobileQuery");
                if (expenseTypes != null && expenseTypes.length() > 0) {
                    db.loadExpenseTypes(expenseTypes);
                }

                /**
                 * Package type implementation
                 */

                JSONArray packagetypes = dataobj.optJSONArray("GetPackageMobileQuery");
                if (packagetypes != null)
                    db.loadPackageTypes(packagetypes);

                JSONArray packagetypesmap = dataobj
                        .optJSONArray("GetProductPackageMappingMobileQuery");
                if (packagetypesmap != null)
                    db.loadPackageTypesMap(packagetypesmap);

                JSONArray invoicedata = dataobj.optJSONArray("PreviousInvoiceGetQry");
                if (invoicedata != null && invoicedata.length() > 0) {
                    if (db.isinvoicetableempty()) {
                        db.loadinvoicetable(invoicedata);
                    }
                }

                JSONArray creditBalance = dataobj.optJSONArray("getCreditBalanceInScheduleForMbl");
                if (creditBalance != null && creditBalance.length() > 0) {
                    db.updateCreditBalance(creditBalance);
                }

                JSONArray currencyDetails = dataobj.optJSONArray("GetCurrencyDetails");
                if (currencyDetails != null && currencyDetails.length() >0){

                    JSONObject currencyObject = currencyDetails.getJSONObject(0);
                    SharedPreferences pref = getApplicationContext()
                            .getSharedPreferences("Config", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();

                    editor.putString("CurrencyCode", currencyObject.optString("CurrencyCode", "INR"));

                    Currency currency = Currency.getInstance(currencyObject.optString("CurrencyCode", "INR"));
                    editor.putString("CurrencySymbol",currency.getSymbol());

                    String roundOffType = currencyObject.optString("RoundOffType", "NA");

                    if (roundOffType.equals("No decimals")){
                        editor.putInt("RoundOffPrecision",0  );
                    }else if (roundOffType.equals("1 decimals")){
                        editor.putInt("RoundOffPrecision",1  );
                    }else if (roundOffType.equals("2 decimals")){
                        editor.putInt("RoundOffPrecision",2  );
                    }else if (roundOffType.equals("3 decimals")){
                        editor.putInt("RoundOffPrecision",3  );
                    }
                    editor.commit();
                }

                // ---------------------sravan----------------------------
                boolean isOrderTaking = false;
                boolean isVanSales = false;
                JSONArray orgservicemap = dataobj.optJSONArray("OrganizationServiceMappingQuery");
                if (orgservicemap.length() > 0) {
                    String services = orgservicemap.getJSONObject(0).optString(
                            "Services", "0");
                    String flag = orgservicemap.getJSONObject(0).optString(
                            "Flag", "0");
                    if (services.equalsIgnoreCase("OrderTaking")
                            && flag.equals("1"))
                        isOrderTaking = true;

                    if (services.equalsIgnoreCase("Van Sales")
                            && flag.equals("1"))
                        isVanSales = true;
                }

                if (!isOrderTaking)
                    db.deleteService("OrderTaking");

                if (!isVanSales) {
                    db.deleteService("Van Sales");
                }


                // ----------------------end-----------------------------
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // endd
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String formattedDate = dateTimeFormat.format(c.getTime());

        editor.putString("Scheduletime", formattedDate);
        editor.putFloat("distancetravelled", 0);
        editor.commit();


        JSONArray schedules = inputdata.getJSONObject("schedules")
                .getJSONArray("data");

        if (schedules.length() > 0) {

            loadshedulestodb(schedules);
        } else {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    Toast.makeText(getApplicationContext(),
                            "Creating new Schedule..", Toast.LENGTH_SHORT).show();

                    Calendar c = Calendar.getInstance();
                    //Log.e("newschedule_create","------------------------------------------------------");
                    DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                    DateFormat schdateFormat = new SimpleDateFormat("MM/dd/yyyy");
                    DateFormat schbegintimeFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);

                    String schbegintime = schbegintimeFormat.format(c.getTime());
                    String schdate = schdateFormat.format(c.getTime());

                    SharedPreferences pref = getApplicationContext()
                            .getSharedPreferences("Config", MODE_PRIVATE);
                    Scheduledata headerdatatodb = new Scheduledata();
                    headerdatatodb.Scheduleid = UUID.randomUUID().toString();
                    headerdatatodb.Schedulename = "New Locations";
                    headerdatatodb.Salespersonid = pref.getString("personid", "0");
                    headerdatatodb.salespersonname = pref.getString("personname", "");
                    headerdatatodb.Routenetworkid = "0";
                    headerdatatodb.Headerstatus = "";
                    headerdatatodb.Scheduledate = schdate;
                    headerdatatodb.begintime = schbegintime;
                    db.createNewSchedule(headerdatatodb);

                    //Log.e("intialisesynctable","------------------------------------------------------");
                    db.intialisesynctable(schbegintime);
                    /*SharedPreferences pref = getApplicationContext()
                    .getSharedPreferences("Config",
							MODE_PRIVATE);*/

                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("Schbegintime", schbegintime);
                    editor.commit();
                    Toast.makeText(getApplicationContext(), "Schedule Started!", Toast.LENGTH_SHORT).show();
                    //Global.Toast(Loading.this, "Schedule Started!", Toast.LENGTH_SHORT, Font.Regular);
                    return;

                }
            });

        }

        JSONArray paymenttabledata = inputdata.getJSONObject("price")
                .getJSONArray("data");
        if (paymenttabledata.length() > 0) {
            //Log.e("paymenttabledata","------------------------------------------------------");
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.ispricetableempty()) {
                db.loadpricetable(paymenttabledata);
            }

        }

        // loading customer category
        JSONArray categorydata = inputdata.getJSONObject("CustomerCategory")
                .optJSONArray("data");
        if (categorydata != null && categorydata.length() > 0) {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.categorytableempty()) {
                db.loadcustomercategorys(categorydata);
            }
        }

        // loading invoice table and calculating tottal invoice balance
        JSONArray invoicedata = inputdata.getJSONObject("InvoiceDetails")
                .optJSONArray("data");
        if (invoicedata != null && invoicedata.length() > 0) {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.isinvoicetableempty()) {
                db.loadinvoicetable(invoicedata);
            }
            // calculatin tottal invoice balance

            Double tottal_invoicebalnce = 0.0;
            for (int i = 0; i < invoicedata.length(); i++) {
                try {
                    tottal_invoicebalnce += Double.parseDouble(invoicedata
                            .getJSONObject(i).getString("balance"));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            editor.putString("invoicebalnce",
                    String.valueOf(tottal_invoicebalnce));
            editor.commit();

        }

        JSONArray resons = inputdata.getJSONObject("Resons").optJSONArray(
                "data");
        if (resons != null && resons.length() > 0) {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.isreasontableempty()) {
                db.loadresons(resons);
            }
        }

        JSONArray branches = inputdata.getJSONObject("branches").optJSONArray(
                "data");
        if (branches != null && branches.length() > 0) {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.isbranchestableempty()) {
                db.loadbranches(branches);
            }
        }

        try {
            final JSONArray workingtime = inputdata
                    .getJSONObject("workingtime").optJSONArray("data");
            if (workingtime != null && workingtime.length() > 0) {
                try {
                    String begin = workingtime.getJSONObject(0).optString(
                            "begintime", "0");
                    String end = workingtime.getJSONObject(0).optString(
                            "endtime", "0");
                    begin = begin.replace(":", "");
                    end = end.replace(":", "");
                    editor.putInt("begin", Integer.parseInt(begin));
                    editor.putInt("end", Integer.parseInt(end));
                    editor.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            final JSONArray personservices = inputdata.getJSONObject(
                    "personservices").optJSONArray("data");
            if (personservices != null && personservices.length() > 0) {
                try {
                    String discustomer = personservices.getJSONObject(0)
                            .optString("discustomer", "0");
                    String disgps = personservices.getJSONObject(0).optString(
                            "disgps", "0");
                    String dispayment = personservices.getJSONObject(0)
                            .optString("dispayment", "0");
                    editor.putInt("discustomer", Integer.parseInt(discustomer));
                    editor.putInt("disgps", Integer.parseInt(disgps));
                    editor.putInt("dispayment", Integer.parseInt(dispayment));
                    editor.commit();

                    String flag = personservices.getJSONObject(0)
                            .optString("DisableInventory", "0");
                    DatabaseHandler  db = new DatabaseHandler(getApplicationContext());
                    db.updateInventoryFlag(flag);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // loading bank data
        JSONArray bankdata = inputdata.getJSONObject("bankmaster")
                .optJSONArray("data");
        if (bankdata != null && bankdata.length() > 0) {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.bankmastertableempty()) {
                db.loadbankmaster(bankdata);
            }
        }

        JSONArray products = inputdata.getJSONObject("products").optJSONArray(
                "data");
        if (products != null && products.length() > 0) {
            loadproductstodb(products);
        }

        JSONArray helpnumbers = inputdata.getJSONObject("helpline")
                .optJSONArray("data");
        if (helpnumbers != null && helpnumbers.length() > 0) {

            JSONObject obj = helpnumbers.getJSONObject(0);
            editor.putString("HelpLineEmail", obj.getString("HelpLineEmail"));
            editor.putString("HelpLinePerson", obj.getString("HelpLinePerson"));
            editor.putString("HelpLinePhone", obj.getString("HelpLinePhone"));
            editor.commit();

        }

        // loading checklist
        JSONArray checklistdata = inputdata.getJSONObject("customerChecklist")
                .optJSONArray("data");
        if (checklistdata != null && checklistdata.length() > 0) {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.ischecklisttableempty()) {
                db.loadchecklist(checklistdata);
            }
        }

        // loading towns
        JSONArray towndata = inputdata.getJSONObject("towns").optJSONArray(
                "data");
        if (towndata != null && towndata.length() > 0) {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.istowntableempty()) {
                db.loadtowns(towndata);
            }
        }

        JSONArray othercompany = inputdata.getJSONObject("othercompany")
                .optJSONArray("data");
        if (othercompany != null && othercompany.length() > 0) {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.isothercompanytableempty()) {
                db.loadothercompany(othercompany);
            }
        }

        JSONArray customersearchdata = inputdata.getJSONObject(
                "dseassignedcustomers").optJSONArray("data");

        if (customersearchdata != null && customersearchdata.length() > 0) {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.iscustomersearchtableempty()) {
                db.loadcustomersearchtable(customersearchdata);
            }
        }

        JSONArray denomdata = inputdata.getJSONObject("denominationdata")
                .optJSONArray("data");
        if (denomdata != null && denomdata.length() > 0) {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.denomtableableempty()) {
                db.loaddenomdata(denomdata);
            }
        }
        gotonextactivity();
        if (mProgress != null) {
            mProgress.dismiss();
        }
    }

    private void callDateTimeSetting() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                Toast.makeText(getApplicationContext(),
                        "Please correct Mobile Date !!", Toast.LENGTH_LONG).show();

                Intent dateSetttingIntent = new Intent(android.provider.Settings.ACTION_DATE_SETTINGS);
                startActivityForResult(dateSetttingIntent, REQUEST_CODE_DATE_SETTING);


            }

        });

    }

    public void loadshedulestodb(JSONArray data) throws JSONException {

        Routnetworkid = String.valueOf(data.getJSONObject(0).getInt(
                "Routenetworkid"));
        String role = data.getJSONObject(0).getString("Scheduletype");

        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Role", role);
        editor.commit();

        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        List<Scheduledata> schdulelist;
        schdulelist = new ArrayList<Scheduledata>();

        JSONArray data_schedules = data;

        for (int i = 0; i < data_schedules.length(); i++) {
            JSONObject schedeleobject = data_schedules.getJSONObject(i);
            Scheduledata schedule = new Scheduledata();
            schedule.Scheduleid = String.valueOf(schedeleobject
                    .getInt("Scheduleid"));
            schedule.Schedulename = schedeleobject.getString("Schedulename");
            schedule.Salespersonid = String.valueOf(schedeleobject
                    .getInt("Salespersonid"));
            schedule.salespersonname = schedeleobject
                    .getString("salespersonname");
            schedule.Salesrouteid = String.valueOf(schedeleobject
                    .getInt("Salesrouteid"));
            schedule.Scheduledate = schedeleobject.getString("Scheduledate");
            schedule.Headerstatus = schedeleobject.getString("Scheduletype");
            schedule.Routenetworkid = String.valueOf(schedeleobject
                    .getInt("Routenetworkid"));
            schedule.completionstatus = schedeleobject
                    .optString("completionstatus");
            JSONArray jscheduledetails = schedeleobject
                    .getJSONArray("Scheduledetails");
            for (int j = 0; j < jscheduledetails.length(); j++) {
                JSONObject scheduledetailobj = jscheduledetails
                        .getJSONObject(j);
                Scheduledetail detail = new Scheduledetail();
                detail.Scheduledetailid = String.valueOf(scheduledetailobj
                        .getInt("Scheduledetailid"));
                detail.locationid = String.valueOf(scheduledetailobj
                        .getInt("locationid"));
                detail.locationname = scheduledetailobj
                        .getString("locationname");
                detail.locationadress = scheduledetailobj
                        .getString("locationadress");
                detail.sequencenumber = scheduledetailobj
                        .getString("sequencenumber");
                detail.status = scheduledetailobj.getString("status");
                detail.latitude = scheduledetailobj.getString("latitude");
                detail.longitude = scheduledetailobj.getString("longitude");
                detail.Rettail = scheduledetailobj.getString("retail");
                detail.invoicenumber = "0";// scheduledetailobj.getString("InvoiceNo");
                detail.picklistnumber = scheduledetailobj
                        .getString("PickListNo");
                detail.Customercatogory = scheduledetailobj
                        .getString("customerCat");
                detail.CustomercatogoryId = scheduledetailobj
                        .getString("customerCatId");
                detail.customercode = scheduledetailobj.getString("CustCode");
                detail.locationlock = scheduledetailobj.optString(
                        "locationverified", "0");
                detail.Closingday = scheduledetailobj.optString("closingday",
                        "");

                detail.mobilenumber = scheduledetailobj.optString(
                        "customerNumber", "");

                detail.OTP = scheduledetailobj.optString("otp", "");
                detail.tinnumber = scheduledetailobj.optString("TINNumber", "");
                detail.pan = scheduledetailobj.optString("Pan", "");
                detail.photoUUID = scheduledetailobj.optString("PhotoUUID", "");
                detail.landmark = scheduledetailobj.optString("landmark", "");
                detail.town = scheduledetailobj.optString("town", "");
                detail.contactperson = scheduledetailobj.optString(
                        "contactperson", "");
                detail.othercompany = scheduledetailobj.optString(
                        "othercompany", "");
                detail.crates = scheduledetailobj.optString("crates", "");
                detail.alternatecustcode = scheduledetailobj.optString(
                        "AlternateCustcode", "");
                detail.custtype = scheduledetailobj.optString("CustType", "");

                detail.targetType = scheduledetailobj.optString("TargetType", "");
                detail.targetValue = scheduledetailobj.optString("TargetValue", "");
                detail.creditLimit = scheduledetailobj.optString("CreditLimit", "");

                detail.gsttype = scheduledetailobj.optString("GSTType", "");
                detail.fssi = scheduledetailobj.optString("FSSINumber", "");
                detail.stateCode = scheduledetailobj.optString("State", "");

                schedule.Scheduledetails.add(detail);

            }

            schdulelist.add(schedule);

        }

        db.insertscheduledata(schdulelist);

    }

    public void loadproductstodb(JSONArray data) throws JSONException {


        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        List<product> produts = new ArrayList<product>();
        JSONArray data_products = data;

        for (int i = 0; i < data_products.length(); i++) {
            JSONObject obj = data_products.getJSONObject(i);
            product product = new product();
            product.productid = String.valueOf(obj.getInt("ProductId"));
            product.productname = obj.getString("Name");
            product.productcode = obj.getString("Code");
            product.productdescrip = obj.getString("ProductDesc");
            product.weight = obj.getString("Wt");
            product.volume = obj.getString("Vol");
            product.tax = obj.getString("Tax_RateValue");
            product.unitprice = obj.getString("UnitPrice");
            produts.add(product);
        }

        if (db.isproducttableempty()) {
            db.insertproducts(produts);
        }

    }

    private class LoginOperation extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... urlParameters) {
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.loginurl;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-8");
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(10000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestMethod("POST");

                int totalLength = (urlParameters[0].getBytes().length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.writeBytes(urlParameters[0]);
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();
                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }

                final String responsefrom = response.toString();
                return responsefrom;
            } catch (final Exception e) {

                //Log.e("Error","---------------------------------------------------------");
                //Log.e("Error",e.getMessage());
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

                return "issue";
            }
        }

        @Override
        protected void onPostExecute(final String responsefrom) {
            super.onPostExecute(responsefrom);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //  Toast.makeText(getApplicationContext(), responsefrom, Toast.LENGTH_SHORT).show();
                }
            });
            if (responsefrom == null || responsefrom.contentEquals("issue")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mProgress != null) {
                            mProgress.dismiss();
                        }
                        salesSignIn.setEnabled(true);
                        Toast.makeText(getApplicationContext(), "Cannot connect to Server!", Toast.LENGTH_SHORT).show();
                    }
                });
                return;
            }
            if (responsefrom.contains("success")) {
                try {
                    getSchedule(responsefrom);
                } catch (Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (mProgress != null) {
                                mProgress.dismiss();
                            }
                            salesSignIn.setEnabled(true);
                            Toast.makeText(getApplicationContext(), "Cannot connect to Server!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return;
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mProgress != null) {
                            mProgress.dismiss();
                        }
                        salesSignIn.setEnabled(true);
                        Toast.makeText(getApplicationContext(), "Error in Username or Password!", Toast.LENGTH_SHORT).show();
                    }
                });
                return;
            }
        }
    }

    private class GetScheduleOperation extends AsyncTask<String, Void, String> {
        private String data;

        @Override
        protected String doInBackground(String... urlParameters) {
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.url;
            data = urlParameters[1];
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-8");
            //    conn.setReadTimeout(10000);
                conn.setConnectTimeout(10000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestMethod("POST");

                int totalLength = (urlParameters[0].getBytes().length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.writeBytes(urlParameters[0]);
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();
                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }

                final String responsefrom = response.toString();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        salesSignIn.setEnabled(true);
                        //Log.e("getsync", responsefrom);
                        // Toast.makeText(getApplicationContext(), responsefrom, Toast.LENGTH_SHORT).show();
                    }
                });

                return responsefrom;
            } catch (final Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        salesSignIn.setEnabled(true);
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

                return "{\"status\":\"issue\"}";
            }
        }

        @Override
        protected void onPostExecute(String responsefrom) {
            super.onPostExecute(responsefrom);
            try {

                JSONObject inputdata = new JSONObject(data);
                SharedPreferences pref = getApplicationContext()
                        .getSharedPreferences("Config", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("personname", inputdata.getString("PersonName"));

                Intent login = getIntent();
                boolean flag = login.getBooleanExtra("refresh", false);
                try {
                    if (flag == true) {

                        code = login.getStringExtra("UserName");

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                editor.putString("UserName", code);
                editor.putString("username", code);
                editor.putString("usernamefor", code);

                editor.putString("datafromlogin", data);
                Log.d("dta", data);
                code = inputdata.getString("PersonCode");
                password = inputdata.getString("PassCode");

                editor.putString("code", code);
                editor.putString("passcode", password);
             //   editor.putString("currencyType", inputdata.optString("currencyType", "inr"));
                //	    stopService(new Intent(this, Vehicletracker.class));
                editor.putString("tmsOrgId", inputdata.optString("tmsOrgId", ""));
                editor.putString("personid", inputdata.getString("personid"));
                editor.putString("personname", inputdata.getString("PersonName"));
                editor.putString("personrole",
                        inputdata.optString("PersonRole", "Designation"));
                editor.putString("pesronorg",
                        inputdata.optString("tmsOrgName", "Organisation"));

                editor.putString("code", code);
                editor.putString("passcode", password);

                editor.putString("BussinessUnit",
                        inputdata.getString("BussinessUnit"));
                editor.putString("orgId", inputdata.getString("orgId"));
                editor.putString("orgDb", inputdata.getString("orgDb"));
                editor.putString("appid", "app38");
                editor.commit();
                Log.d("afterpre", pref.getString("datafromlogin", ""));

                JSONArray services = inputdata.optJSONArray("services");
                if (services.length() > 0) {
                    DatabaseHandler db = new DatabaseHandler(
                            getApplicationContext());
                    db.insertservices(services);
                }
                loadschedulesandproducts(responsefrom);
            } catch (final Exception e) {
                e.printStackTrace();
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.commit();
//                if(mProgress != null) {
                mProgress.dismiss();
//                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), e.getMessage() + "Cannot connect to Server!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    private class LongOperation extends AsyncTask<JSONObject, Void, String> {
        // final Dialog dialog = new Dialog(MainActivity.this);
        protected void onPreExecute() {
            // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            // dialog.getWindow().clearFlags(
            // WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            // dialog.setContentView(R.layout.adapter_progressdialog);
            // TextView txt = (TextView) dialog.findViewById(R.id.text);
            // txt.setText("Please wait..");
            // dialog.getWindow().setBackgroundDrawable(
            // new ColorDrawable(android.graphics.Color.TRANSPARENT));
            // dialog.show();
            // dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.loginurl;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setConnectTimeout(100000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(200000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responsefrom;
        }

        protected void onPostExecute(final String response) {
            // dialog.dismiss();
            if (response != null) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        String replay = "";
                        try {
                            JSONObject resp = new JSONObject(response);
                            if (resp.optString("status").contentEquals(
                                    "success")) {
                                replay = resp.optString("Version","0");
                                int vcode = Integer.parseInt(replay);
                                int currnt = 0;
                                try {
                                    PackageManager manager = getApplicationContext()
                                            .getPackageManager();
                                    PackageInfo info = manager.getPackageInfo(
                                            getApplicationContext()
                                                    .getPackageName(), 0);
                                    currnt = info.versionCode;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (vcode > currnt) {
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putBoolean("versionavailable",true);
                                    editor.putInt("version",vcode);
                                    editor.commit();
                                }
                            } else
                                replay = resp.optString("Error");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(getApplicationContext(), replay,
                        // Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                // Toast.makeText(getApplicationContext(),
                // "Cannot connect to server", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onAppUpdated() {

        File myFile = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/temp_apk/"+webconfigurration.appname);
        if (myFile.exists())
            myFile.delete();
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.update_completed),
                Toast.LENGTH_LONG).show();
        // stopService(new Intent(MainActivity.this, Vehicletracker.class));
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        Global.force = true;
        db.Deletetabledata();
        Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }
}
