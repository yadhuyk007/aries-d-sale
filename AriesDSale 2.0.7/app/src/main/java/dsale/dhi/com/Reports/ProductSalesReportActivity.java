package dsale.dhi.com.Reports;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.HashMap;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.Global;

public class ProductSalesReportActivity extends AppCompatActivity {
    private TableLayout list;
    private TableRow row;
    private DatabaseHandler db;
    private HashMap<String, JSONObject> hashmap;
    private TableRow.LayoutParams lp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productsalesreport);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);


        list = (TableLayout) findViewById(R.id.list);
        lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        lp.setMargins(1, 0, 0, 1);
        SharedPreferences pref = getApplicationContext()
                .getSharedPreferences("Config", MODE_PRIVATE);
        db = new DatabaseHandler(getApplicationContext());

        try {

            String orgid=pref.getString("tmsOrgId", "");
            hashmap = db.getVansalesSaleReportData();

        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i=1 ;i <= hashmap.size(); i++) {
            String sno = String.valueOf(i);
            JSONObject value = hashmap.get(sno);

            String itemname = value.optString("productname", "");
            String availableqty = value.optString("availableqty", "");
            String unit = value.optString("unit", "kilogram");

            row = new TableRow(ProductSalesReportActivity.this);
            row.setBackgroundResource(R.drawable.table_row);
            row.addView(addtext(sno,i));
            row.addView(addtext(itemname, i));
            row.addView(addtext(availableqty, i));
            row.addView(addtext(unit, i));
            list.addView(row);
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        String l = Global.setLastTime();
    }

    private TextView addtext(String string, int i) {
        TextView text = new TextView(ProductSalesReportActivity.this);
        text.setLayoutParams(lp);
        text.setText(string);
//        if (i % 2 == 0)
            text.setBackgroundResource(R.color.white);
//        else
//            text.setBackgroundResource(R.color.gray3);
        text.setPadding(10, 20, 10, 10);
//        a = new Font(VanSalesReport.this, text, Font.Regular);
        return text;
    }

    //Added by jithu on 17/05/2019 for redirect to customer list when backpress worked
    @Override
    public void onBackPressed() {

        Intent intent=new Intent(ProductSalesReportActivity.this, CustomerListActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

}
