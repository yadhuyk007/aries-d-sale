package dsale.dhi.com.dashboard;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import dsale.dhi.com.ariesdsale.R;
import dsale.dhi.com.customerlist.CustomerListActivity;
import dsale.dhi.com.database.DatabaseHandler;
import dsale.dhi.com.objects.webconfigurration;

import static dsale.dhi.com.Reports.DailySalesReportActivity.round;

public class SummaryActivity extends AppCompatActivity {
    private DatabaseHandler db;
    private String arcollected;
    private SharedPreferences pref;
    TextView masalaper;
    TextView stpowper;
    TextView otherper;
    TextView distancetraveled;
    TextView qtytarget;
    TextView achieved;
    LinearLayout layout_masalaPer;
    LinearLayout layout_stpowPer;
    LinearLayout layout_otherPer;
    //    String Rupeessymbol = "\u20B9";
    private int roundoffPrecision ;
    private String currencySymbol ;
    private String currencyCode ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        db = new DatabaseHandler(getApplicationContext());
        pref = getApplicationContext()
                .getSharedPreferences("Config", MODE_PRIVATE);

        roundoffPrecision = pref.getInt("RoundOffPrecision",0);
        currencyCode=pref.getString("CurrencyCode","");
        currencySymbol =pref.getString("CurrencySymbol","");
        try {
            setvalues();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setvalues() throws ParseException {
        JSONObject summarydata = new JSONObject();
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        webconfigurration w = new webconfigurration(getApplicationContext());
        try {
            summarydata = db.getworksummary();
        } catch (Exception e) {
            e.printStackTrace();
            summarydata = new JSONObject();
        }
        masalaper=(TextView) findViewById(R.id.MasalaPer);
        stpowper=(TextView) findViewById(R.id.StpowPer);
        otherper=(TextView) findViewById(R.id.OtherPer);
        distancetraveled=(TextView) findViewById(R.id.distancetrsvelled);
        qtytarget=(TextView) findViewById(R.id.qtytarget);
        achieved=(TextView) findViewById(R.id.achieved);
        layout_masalaPer=(LinearLayout) findViewById(R.id.layout_MasalaPer);
        layout_stpowPer=(LinearLayout) findViewById(R.id.layout_StpowPer);
        layout_otherPer=(LinearLayout) findViewById(R.id.layout_OtherPer);
        double qtyTarget = 0;
        double totalQty= 0;
        double mqty= 0;
        double spqty =0;
        double otherqty=0;
        double mper=0;
        double sper=0;
        double othrper=0;
        if (w.showProdCatPer) {
            qtyTarget = db.getTargetQty();
        } else {
            qtyTarget = db.getTargetVal();
        }
        double distance_current = db.getdistance();

        String totalstores = String.valueOf(summarydata
                .optInt("tottalnumberofstores", 0));
        String visited = String.valueOf(summarydata
                .optInt("visitedstores", 0));
        String productive = String.valueOf(summarydata
                .optInt("productivestores", 0));
        String artarget = "";
        String arcollected = "";
        ((TextView) findViewById(R.id.totstoresbval)).setText(totalstores);
        ((TextView) findViewById(R.id.visitedstoresval)).setText(visited);
        ((TextView) findViewById(R.id.productiveval)).setText(productive);
        ((TextView) findViewById(R.id.artargetval)).setText(artarget);
        ((TextView) findViewById(R.id.arcollectval)).setText(arcollected);
        distancetraveled.setText(String.valueOf(round(
                distance_current, 2)) + " Km ");
        if (w.showProdCatPer) {
            qtytarget.setText(String.valueOf(qtyTarget) + " Kg ");
            HashMap<String, JSONObject> hashmap = new HashMap<>();
            try {
                hashmap = db.getDailySalesReport();
            } catch (Exception e) {
                e.printStackTrace();
            }
            JSONArray listArray = new JSONArray();
            for (String key : hashmap.keySet()) {
                listArray.put(hashmap.get(key));
            }
            for (int i = 0; i < listArray.length(); i++) {

                JSONObject item = listArray.optJSONObject(i);
                if (item != null) {
                    String category = item.optString("Category", "");
                    String qtyString = item.optString("Qty", "0");

                    try {
                        double qty = Double.parseDouble(qtyString);

                        totalQty += qty;

                        if (category.equalsIgnoreCase("Masala")) {
                            mqty = qty;

                        } else if (category
                                .equalsIgnoreCase("Straight Powder")) {
                            spqty = qty;

                        } else {
                            otherqty += qty;

                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
                mper = (mqty * 100) / totalQty;
                mper = round(mper, 1);
                sper = (spqty * 100) / totalQty;
                sper = round(sper, 1);
                othrper = (otherqty * 100) / totalQty;
                othrper = round(othrper, 1);
                masalaper.setText(twoDForm.format(mper) + " %");
                stpowper.setText(twoDForm.format(sper) + " %");
                otherper.setText(twoDForm.format(othrper) + " %");
                achieved.setText(twoDForm.format(totalQty) + " KG");

            }
        }
        else{
            qtytarget.setText(currencySymbol+" " + qtyTarget);
         /*   double taxableAmnt = db.getTotalTaxableAmount();
            double taxAmnt = db.getTotalTaxAmount();
            double ordAmnt = taxableAmnt + taxAmnt;
            ordAmnt = Math.round(ordAmnt);*/

        /*    BigDecimal taxableAmnt = db.getTotalTaxableAmount();
            BigDecimal taxAmnt = db.getTotalTaxAmount();
*/
            BigDecimal ordAmnt = db.getTotalOrderAmount(roundoffPrecision) ;


            achieved.setText(currencySymbol+" " +  ordAmnt.toString() );

            layout_masalaPer.setVisibility(View.INVISIBLE);
            layout_stpowPer.setVisibility(View.INVISIBLE);
            layout_otherPer.setVisibility(View.INVISIBLE);

        }
       /* if(!w.showProdCatPer){
            layout_masalaPer.setVisibility(View.GONE);
            layout_stpowPer.setVisibility(View.GONE);
            layout_otherPer.setVisibility(View.GONE);
        }*/
        String scheduledate = pref.getString("Schbegintime", "");
        Date datsched = new Date();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date curr = new Date();
        long diff = 0;
        long hours_tottal = 0;
        long mins_tottal = 0;
        int progress1;
        ProgressBar mProgress1=(ProgressBar) findViewById(R.id.performance_progress_bar);
        if (!scheduledate.contentEquals("")) {
            datsched = dateTimeFormat.parse(scheduledate);
            diff = curr.getTime() - datsched.getTime();
            hours_tottal = diff / (60 * 60 * 1000);
            mins_tottal = diff / (60 * 1000) % 60;
        }
        String sched_time = String.valueOf(hours_tottal) + "h "
                + String.valueOf(mins_tottal) + "min";
        ((TextView) findViewById(R.id.worktime)).setText(sched_time);

        // updating working hours in progrss bar and textview
        final String scheduledate_final = pref
                .getString("Schbegintime", "");
        double worktimemax = (((hours_tottal * 60) + mins_tottal) / (8.0 * 60.0)) * 100.0;
        progress1 = (int) worktimemax;
        animateProgress(mProgress1, progress1);
    }
    private void animateProgress(ProgressBar mProgress, int i) {
        ObjectAnimator animation = ObjectAnimator.ofInt(mProgress, "progress",
                0, i);
        animation.setDuration(800);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }

    //Added by Jithu on 17/05/2019 for back press is redirect to  customer list page
    @Override
    public void onBackPressed() {

        Intent intent=new Intent(SummaryActivity.this, CustomerListActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

}
